//
//  HZUser.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZJsonModel.h"
@interface HZUser : HZJsonModel
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *facePhotoUrl;
@property (nonatomic, copy) NSString *hospitalId;
@property (nonatomic, copy) NSString *number;// 员工编号
@property (nonatomic, copy) NSString *titleName;// 主任
@property (nonatomic, copy) NSString *departmentGeneralName;// 科室新
@property (nonatomic, copy) NSString *departmentGeneralId;// 科室Id
@property (nonatomic, copy) NSString *authLocalDoctor;// 0 不是
@property (nonatomic, copy) NSString *authRemoteExpert;// 0 不是
@property (nonatomic, copy) NSString *remoteAccount;
@property (nonatomic, copy) NSString *honor;
@property (nonatomic, copy) NSString *speciality;
@property (nonatomic, copy) NSString *phone;

//
@property (nonatomic, copy) NSString *remoteExpertLevel;// 级别Code
@property (nonatomic, copy) NSString *totalMoney;// price
@property (nonatomic, copy) NSString *openid;// openid
@property (nonatomic, copy) NSString *titleId;
@property (nonatomic, copy) NSString *userId;// userId
//
@property (nonatomic, copy) NSArray *departmentList;

//
@property (nonatomic, copy) NSString *authenticationState;// 认证状态:unauthenticated,authenticating, authenticated, rejected
//// 用户对应的名医联盟产品版本(数据对接的版本)
//@property (nonatomic, assign) NSString *dataButtedVersion; // PRIMARY: 基础版； PROFESSIONAL:专业版； ULTIMATE:旗舰版
//@property (nonatomic, assign) BOOL hasDataButted;
@end
