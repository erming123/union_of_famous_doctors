//
//  HZAccountSettingTableViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZAccountSettingViewController.h"
#import "HZActivityIndicatorView.h"
#import "HZAlertView.h"
#import <StoreKit/StoreKit.h>
#import "HZAccount.h"
#import "HZAccountTool.h"
#import "HZHttpsTool.h"
#import "HZDeviceAccountTool.h"
//
#import "SFHFKeychainUtils.h"

//
#import <WebKit/WebKit.h>
//#import "HZAppVersionInforCell.h"
@interface HZAccountSettingViewController ()<UITableViewDataSource, UITableViewDelegate, HZAlertViewDelegate, SKStoreProductViewControllerDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *tableView;
//** <#注释#>*/
@property (nonatomic, strong) UIImageView *logoImageView;
//** <#注释#>*/
@property (nonatomic, strong) UILabel *appNameLabel;
//** <#注释#>*/
@property (nonatomic, strong) UILabel *versionLabel;
//** <#注释#>*/
@property (nonatomic, strong) HZActivityIndicatorView *activityIndicatorView;
//** <#注释#>*/
@property (nonatomic, strong) UIView *cover;
//** <#注释#>*/
@property (nonatomic, strong) HZAlertView *alertView;
//** <#注释#>*/
@property (nonatomic, strong) UITableViewCell *cell;
@property (nonatomic, weak) UILabel *fileSizeLabel;
@property (nonatomic, weak) UILabel *currentAccountLabel;
@property (nonatomic, weak) UILabel *logoutLabel;
@end

@implementation HZAccountSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"帐号设置";
    
    
    //
    HZTableView *tableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorInset = UIEdgeInsetsMake(0, - 10, 0, 0);
    tableView.separatorColor = [UIColor colorWithHexString:@"dfdfdf" alpha:1.0];
    tableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    

    
    //
    HZActivityIndicatorView *activityIndicatorView = [[HZActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, kP(240), kP(240))];
    activityIndicatorView.center = self.view.center;
    activityIndicatorView.activityIndicatorText = @"正在退出";
    [self.view addSubview:activityIndicatorView];
    self.activityIndicatorView = activityIndicatorView;
    
   
    
    //
    UILabel *copyRightLabel = [UILabel new];
    copyRightLabel.text = @"Copyright @2017 WOWJOY All Rights Reserve";
    copyRightLabel.font = [UIFont systemFontOfSize:kP(24)];
    [copyRightLabel sizeToFit];
    copyRightLabel.x = self.view.width * 0.5 - copyRightLabel.width * 0.5;
    copyRightLabel.y = self.view.height - copyRightLabel.height - kP(46) - 64;
    copyRightLabel.textColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0];
    [self.tableView addSubview:copyRightLabel];
    //
    
    UIView *longPressBgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height - SafeAreaTopHeight - kP(150) - SafeAreaBottomHeight, self.view.width, kP(150))];
//    longPressBgView.backgroundColor = [UIColor redColor];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressToAlert:)];
    longPress.minimumPressDuration = 5.0f;//设置长按 时间
    [longPressBgView addGestureRecognizer:longPress];
    [self.view addSubview:longPressBgView];
}

- (NSString *)getBundleFileSize {
    //
    NSString *imagePathStr = [NSString stringWithFormat:@"%@/Library/Preferences/JCYPics/",NSHomeDirectory()];
    CGFloat fileSize = [imagePathStr fileSize];
    fileSize += [[SDImageCache sharedImageCache] getSize];
    NSString *fileSizeStr;
    //
    if (fileSize >= pow(1024, 3)) { // size >= 1GB
        fileSizeStr = [NSString stringWithFormat:@"%.1fGB", fileSize / pow(1024, 3)];
    } else if (fileSize >= pow(1024, 2)) { // 1GB > size >= 1MB
        fileSizeStr = [NSString stringWithFormat:@"%.1fMB", fileSize / pow(1024, 2)];
    } else if (fileSize >= 1024) { // 1MB > size >= 1KB
        fileSizeStr = [NSString stringWithFormat:@"%.fKB", fileSize / 1024];
    } else { // 1KB > size
        fileSizeStr = [NSString stringWithFormat:@"%zdB", fileSize];
    }
    return fileSizeStr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return section == 0 ? 2 : 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            cell.textLabel.font = [UIFont systemFontOfSize:kP(32)];
            cell.textLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.textLabel.text = @"当前帐号";
            //
            [self.currentAccountLabel removeFromSuperview];
            UILabel *currentAccountLabel = [[UILabel alloc] init];
            currentAccountLabel.text = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"phoneNumber"];
            currentAccountLabel.font = [UIFont systemFontOfSize:kP(32)];
            currentAccountLabel.textAlignment = NSTextAlignmentCenter;
            currentAccountLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
            
            CGSize currentAccountSize = [currentAccountLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(38)];
            currentAccountLabel.width = currentAccountSize.width;
            currentAccountLabel.height = currentAccountSize.height;
            currentAccountLabel.x = HZScreenW - currentAccountLabel.width - kP(28);
            currentAccountLabel.y = kP(100) * 0.5 - currentAccountLabel.height * 0.5;
            [cell addSubview:currentAccountLabel];
            self.currentAccountLabel = currentAccountLabel;
        } else {
            cell.textLabel.font = [UIFont systemFontOfSize:kP(32)];
            cell.textLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.textLabel.text = @"清除缓存";
            
            
            //
            [self.fileSizeLabel removeFromSuperview];
            UILabel *fileSizeLabel = [[UILabel alloc] init];
            fileSizeLabel.text = [self getBundleFileSize];
            fileSizeLabel.font = [UIFont systemFontOfSize:kP(32)];
            fileSizeLabel.textAlignment = NSTextAlignmentCenter;
            fileSizeLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
            
            CGSize currentAccountSize = [fileSizeLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(38)];
            fileSizeLabel.width = currentAccountSize.width;
            fileSizeLabel.height = currentAccountSize.height;
            fileSizeLabel.x = HZScreenW - fileSizeLabel.width - kP(28);
            fileSizeLabel.y = kP(100) * 0.5 - fileSizeLabel.height * 0.5;
            [cell addSubview:fileSizeLabel];
            self.fileSizeLabel = fileSizeLabel;
        }
        
    } else {
        
        [self.logoutLabel removeFromSuperview];
        UILabel *logoutLabel = [[UILabel alloc] init];
        logoutLabel.text = @"退出登录";
        logoutLabel.font = [UIFont boldSystemFontOfSize:kP(32)];
        logoutLabel.textAlignment = NSTextAlignmentCenter;
        logoutLabel.textColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
        
        CGSize logoutSize = [logoutLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(35)];
        logoutLabel.width = logoutSize.width;
        logoutLabel.height = logoutSize.height;
        logoutLabel.x = HZScreenW * 0.5 - logoutLabel.width * 0.5;
        logoutLabel.y = kP(100) * 0.5 - logoutLabel.height * 0.5;
        [cell addSubview:logoutLabel];
        self.logoutLabel = logoutLabel;
        return cell;
        
    }
    
    return cell;
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(100);
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(416);
    }
    return kP(12);
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kP(0.1);
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
        if (section != 0) return 0;
    
        UIView *bgView = [UIView new];
        //
        CGFloat logoImageViewX = kP(294);
        CGFloat logoImageViewY = kP(90);
        CGFloat logoImageViewWH = self.view.width - 2 * logoImageViewX;
        UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(logoImageViewX, logoImageViewY, logoImageViewWH, logoImageViewWH)];
        logoImageView.image = [UIImage imageNamed:@"logo"];
        logoImageView.layer.cornerRadius = kP(30);
        logoImageView.clipsToBounds = YES;
        [bgView addSubview:logoImageView];
        self.logoImageView = logoImageView;
        
        //
        UILabel *appNameLabel = [UILabel new];
        appNameLabel.text= @"名医联盟";
        appNameLabel.font = [UIFont systemFontOfSize:kP(28)];
        [appNameLabel sizeToFit];
        appNameLabel.x = self.view.width * 0.5 - appNameLabel.width * 0.5;
        appNameLabel.y = CGRectGetMaxY(self.logoImageView.frame) + kP(12);
        [bgView addSubview:appNameLabel];
        self.appNameLabel = appNameLabel;
        
        //
        UILabel *versionLabel = [UILabel new];
        versionLabel.font = [UIFont systemFontOfSize:kP(28)];
        versionLabel.text = [NSString stringWithFormat:@"版本号 %@", kCurrentBundleVersion];
        [versionLabel sizeToFit];
        versionLabel.x = self.view.width * 0.5 - versionLabel.width * 0.5;
        versionLabel.y = CGRectGetMaxY(appNameLabel.frame) + kP(14);
        versionLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
        [bgView addSubview:versionLabel];
        self.versionLabel = versionLabel;
        
        return bgView;
}
#pragma mark -- 点击cell进入各个界面
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    
    if ([indexPath isEqual:[NSIndexPath indexPathForRow:1 inSection:0]]) {
        
//        if ([self.fileSizeLabel.text isEqualToString:@"0B"]) return;
        NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"提示" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kP(32)],NSForegroundColorAttributeName:[UIColor blackColor]}];
        
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"是否清除当前的缓存数据？" preferredStyle:UIAlertControllerStyleAlert];
        [alertCtr setValue:attributedTitle forKey:@"attributedTitle"];
        
        UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //      // 删除移动影像图片
            NSString *imagePathStr = [NSString stringWithFormat:@"%@/Library/Preferences/JCYPics/",NSHomeDirectory()];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            [fileManager removeItemAtPath:imagePathStr error:nil];
            //        NSArray *contents = [fileManager contentsOfDirectoryAtPath:imagePathStr error:NULL];
            //        NSError *error;
            //        for (NSString *path in contents) {
            //            if ([path containsString:@"com.wowjoy.SLFDA.plist"] ) {
            //                continue;
            //            }
            //
            //            [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@%@",imagePathStr,path] error:&error];
            //        }
            //        NSLog(@"error = %@",error);
            ////
            //        // 删除图片路径数组ImageArr
            NSString *pathStrKey = [NSString stringWithFormat:@"%@Key", @"648860CT"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:pathStrKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //
            //        // 删除sdWebCache图片
            [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
            [[SDImageCache sharedImageCache] clearMemory];
            
            //
            if (kCurrentSystemVersion >= 9.0) {
                [self clearWebAllMemory];
            }
            
            
            [self.tableView reloadData];
        }];
        if (kCurrentSystemVersion >= 9.0) {
            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            [cancelAlertAction setValue:[UIColor colorWithHexString:@"878787" alpha:1.0] forKey:@"titleTextColor"];
        }
        
        [alertCtr addAction:cancelAlertAction];
        [alertCtr addAction:ensureAlertAction];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];

    }
    
    if (indexPath.section == 1) {
//        BOOL isIMOnLine = [[NSUserDefaults standardUserDefaults] boolForKey:@"isIMOnLine"];
        NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"退出确认" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kP(32)],NSForegroundColorAttributeName:[UIColor blackColor]}];
        
        //        NSString *str = isIMOnLine ? @"在线" : @"离线";
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:attributedTitle.string message:@"是否确认退出当前帐号？" preferredStyle:UIAlertControllerStyleAlert];
        [alertCtr setValue:attributedTitle forKey:@"attributedTitle"];
        
        UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        
        
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            // 推出
            self.activityIndicatorView.hidden = NO;
            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
            
            // 去除头像
            NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
            if (!userDict) return;
            NSString *userOpenId = userDict[@"openid"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:userOpenId];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // 判断是否容联云在线
            NSLog(@"-------容联云在线，推出容联云和本地服务器");
            NSLog(@"退出成功!");
            self.activityIndicatorView.hidden = YES;
            
            // 查看
            NSString *deviceAccount = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceAccount"];
            
            //
            BOOL isRegisted = [[NSUserDefaults standardUserDefaults] boolForKey:@"deviceAccountIsAssociated"];
            
            if (isRegisted) {
                [HZDeviceAccountTool disassociateDeviceAccountWithDeviceAccount:deviceAccount success:^(id  _Nullable responseObject) {
                    NSLog(@"----responseObject-----%@", responseObject);
                    if ([responseObject[@"state"] isEqual:@200]) {
                        
                        //推出容联云
                        while ([[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"] != nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"basic_token"] != nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"refresh_token"] != nil) {
                            [self clearAppMemory];
                        }
                        
                        // 选择页面
                        [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                    } else {
                        
                        
                        //推出容联云
                        while ([[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"] != nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"basic_token"] != nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"refresh_token"] != nil) {
                            [self clearAppMemory];
                        }
                        
                        // 选择页面
                        [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                    }
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"----error-----%@", error);
                    
                    //推出容联云
                    while ([[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"] != nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"basic_token"] != nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"refresh_token"] != nil) {
                        [self clearAppMemory];
                    }
                    // 选择页面
                    [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                }];
            } else {
                
                [self clearAppMemory];
                
                //
                //                [[NSUserDefaults standardUserDefaults] synchronize];
                // 选择页面
                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
            }
            
        }];
        
        if (kCurrentSystemVersion >= 9.0) {
            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            [cancelAlertAction setValue:[UIColor colorWithHexString:@"878787" alpha:1.0] forKey:@"titleTextColor"];
        }
        
        [alertCtr addAction:ensureAlertAction];
        [alertCtr addAction:cancelAlertAction];
        [self presentViewController:alertCtr animated:YES completion:nil];
    }
    
}


- (void)clearAppMemory {
    
    NSUserDefaults *defatluts = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictionary = [defatluts dictionaryRepresentation];
    
    //1.
    NSArray *allkeysArr = [dictionary allKeys];
    //2.
    NSDictionary *orderIdJCYBIdDict = [HZOrderIdJCYBIdDictTool shareInstance].orderIdJCYBIdDict;
    
    NSArray *keyBigArr = [orderIdJCYBIdDict allValues];
    NSLog(@"-----keyBigArr----%@", keyBigArr);
    NSMutableArray *keysArr = [NSMutableArray array];
    for (NSArray *keyArr in keyBigArr) {
        
        for (NSString *key in keyArr) {
            
            [keysArr addObject:key];
        }
        
    }
    
    //3.需要留下的
    NSMutableSet * leftKeySet = [[NSMutableSet alloc] initWithArray:keysArr];
    
    // 4. 全部的key
    NSMutableSet *allKeySet = [[NSMutableSet alloc] initWithArray:allkeysArr];
    
    [allKeySet minusSet:leftKeySet];
    
    NSArray *orderIdJCYBIdArr = [allKeySet allObjects];
    //
    NSLog(@"-----orderIdJCYBIdArr----%@", orderIdJCYBIdArr);
    for(NSString *key in orderIdJCYBIdArr){

        if ([key containsString:kMyAppKey] || [key isEqualToString:@"648860CTKey"] || [key isEqualToString:@"测试网络环境"]) {
            
            continue;
        }
        [defatluts removeObjectForKey:key];
        [defatluts synchronize];
    }

    //
    [SFHFKeychainUtils deleteItemForUsername:@"HZ" andServiceName:@"WOWJOY" error:nil];
    
    //
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
    
    [[SDImageCache sharedImageCache] clearMemory];//可不写
    
    //
    [[YYCache cacheWithName:@"MyInfo_YYcache"] removeAllObjects];
}


- (void)clearWebAllMemory {
    /////////-----------WKWebsiteDataStore------------
    NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
    
    //// Date from
    
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    
    //// Execute
    
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
        
        // Done
        NSLog(@"WKWebView清除缓存成功!");
    }];
}

#pragma mark -- longPressToAlert
- (void)longPressToAlert:(UILongPressGestureRecognizer *)longPress {
    BOOL isTextNet = [[NSUserDefaults standardUserDefaults] boolForKey:@"测试网络环境"];
    NSString *msg = [NSString stringWithFormat:@"当前是%@网络环境,是否要切换为%@网络环境", isTextNet ? @"测试" : @"生产", isTextNet ? @"生产" : @"测试"];
    typedef void (^PresentAlertBlock)();
    PresentAlertBlock b = ^(){
        [[NSUserDefaults standardUserDefaults] setBool:!isTextNet forKey:@"测试网络环境"];
    };
    showMessage(msg, self, b);
}
@end
