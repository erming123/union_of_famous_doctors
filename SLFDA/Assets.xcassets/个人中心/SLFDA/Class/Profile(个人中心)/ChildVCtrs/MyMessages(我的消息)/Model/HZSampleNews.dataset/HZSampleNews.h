//
//  HZSampleNews.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/8/28.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZSampleNews : NSObject
@property (nonatomic, copy) NSString *userData;
@property (nonatomic, copy) NSString *openId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *titleName;
@property (nonatomic, copy) NSString *unReadNewsCountStr;
@property (nonatomic, copy) NSString *newsTimeStr;
@property (nonatomic, copy) NSString *lastNews;
@property (nonatomic, copy) NSArray *imgPathArr;
@end
