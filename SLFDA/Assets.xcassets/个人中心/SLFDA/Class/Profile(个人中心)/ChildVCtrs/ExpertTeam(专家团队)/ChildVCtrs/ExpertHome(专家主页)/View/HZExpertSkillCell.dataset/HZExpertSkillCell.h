//
//  HZExpertSkillCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZExpertSkillCellDelegate <NSObject>

- (void)setCellHeightWithIsSkillUnFold:(BOOL)isUnFold cellHeight:(CGFloat)cellHeight;

@end
@interface HZExpertSkillCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) id<HZExpertSkillCellDelegate>delegate;
@property (nonatomic, copy) NSString *expertSkillStr;
@end
