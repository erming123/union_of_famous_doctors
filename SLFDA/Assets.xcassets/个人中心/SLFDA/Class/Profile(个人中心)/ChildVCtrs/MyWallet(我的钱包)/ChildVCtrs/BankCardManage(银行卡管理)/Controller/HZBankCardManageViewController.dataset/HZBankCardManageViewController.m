//
//  HZBankCardManageViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/7.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZBankCardManageViewController.h"
#import "HZSelBankNameCell.h"
#import "HZWallet.h"
#import "HZWalletHttpsTool.h"
//
//银行卡扫描
#import "HZScanToolViewController.h"
//解决IQKeyBoard多次textField调用代理
#import "IQUIView+Hierarchy.h"
#import "IQKeyboardManager.h"
@interface HZBankCardManageViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
//** <#注释#>*/
@property (nonatomic, weak) UIButton *editBtn;
//** <#注释#>*/
@property (nonatomic, weak) HZTableView *bankCardManageTableView;
//** <#注释#>*/
@property (nonatomic, strong) UITextField *bankTextField;
@property (nonatomic, strong) UITextField *bankCardTextField;
@property (nonatomic, strong) UITextField *openNetTextField;
//
//** <#注释#>*/
@property (nonatomic, copy) NSString *bankStr;

////** <#注释#>*/
@property (nonatomic, weak) UIView *bgCover;
//** <#注释#>*/
@property (nonatomic, weak) UITableView *bankNameListTableView;

@property (nonatomic, copy) NSArray *selNameArr;

//** <#注释#>*/
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
//** 当前状态*/
@property (nonatomic,assign) BOOL nowIsEdit;

@end

@implementation HZBankCardManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    self.navigationItem.title = @"银行卡管理";
    
    
    
    //1. 设置页面
    [self setUpSubViews];
    
    
    [self.bankCardTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //2. 数据处理
    NSLog(@"-----ffffggggggg----%@", [HZWallet shareInstance].bankOwnerName);
    
    self.nowIsEdit = NO;
}



- (HZWallet *)wallet {
    if (!_wallet) {
        _wallet = [HZWallet shareInstance];
    }
    
    return _wallet;
}

#pragma mark -- 设置页面
- (void)setUpSubViews {
    //
    UIButton *editBtn = [UIButton new];
    [editBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [editBtn setTitle:@"保存" forState:UIControlStateSelected];
    [editBtn sizeToFit];
    editBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [editBtn setTitleColor:[UIColor colorWithHexString:@"#4A4A4A" alpha:1.0] forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(editBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:editBtn];
    UIBarButtonItem *itemSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    itemSpacer.width = - kP(8);
    self.navigationItem.rightBarButtonItems = @[itemSpacer, leftItem];
    self.editBtn = editBtn;
    
    //
    HZTableView *bankCardManageTableView = [[HZTableView alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, HZScreenH) style:UITableViewStyleGrouped];
    bankCardManageTableView.backgroundColor = [UIColor clearColor];
    bankCardManageTableView.dataSource = self;
    bankCardManageTableView.delegate = self;
    [self.view addSubview:bankCardManageTableView];
    self.bankCardManageTableView = bankCardManageTableView;
    
    //
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    
    // 开户行
    UITextField *bankTextField = [[UITextField alloc] init];
    //    bankTextField.backgroundColor = [UIColor greenColor];
    bankTextField.textAlignment = NSTextAlignmentRight;
    bankTextField.userInteractionEnabled = NO;
    bankTextField.textColor = [UIColor lightGrayColor];
    bankTextField.text = self.wallet.depositBank;
    bankTextField.font = [UIFont systemFontOfSize:kP(32)];
    bankTextField.delegate = self;
    //    [bankTextField addTarget:self action:@selector(textValueChanged:) forControlEvents:UIControlEventEditingDidEndOnExit];
    self.bankTextField = bankTextField;
    
    // 银行卡号
    UITextField *bankCardTextField = [[UITextField alloc] init];
    //    bankCardTextField.backgroundColor = [UIColor greenColor];
    bankCardTextField.textAlignment = NSTextAlignmentRight;
    bankCardTextField.userInteractionEnabled = NO;
    bankCardTextField.textColor = [UIColor lightGrayColor];
    bankCardTextField.text = self.wallet.bankAccount;
    bankCardTextField.keyboardType = UIKeyboardTypeNumberPad;
    bankCardTextField.font = [UIFont systemFontOfSize:kP(32)];
    bankCardTextField.delegate = self;
    // [bankCardTextField addTarget:self action:@selector(textValueChanged:) forControlEvents:UIControlEventEditingDidEndOnExit];
    self.bankCardTextField = bankCardTextField;
    
    // 开户网点
    UITextField *openNetTextField = [[UITextField alloc] init];
    openNetTextField.frame = CGRectMake(kP(200), kP(20), HZScreenW - kP(200) - kP(28), kP(50));
    //    openNetTextField.backgroundColor = [UIColor greenColor];
    openNetTextField.textAlignment = NSTextAlignmentRight;
    openNetTextField.userInteractionEnabled = NO;
    openNetTextField.textColor = [UIColor lightGrayColor];
    openNetTextField.text = [self.wallet.bankAddress stringByReplacingEmojiCheatCodesWithUnicode];
    openNetTextField.font = [UIFont systemFontOfSize:kP(32)];
    openNetTextField.delegate = self;
    //    [openNetTextField addTarget:self action:@selector(textValueChanged:) forControlEvents:UIControlEventEditingDidEndOnExit];
    self.openNetTextField = openNetTextField;
    
    //
    //    UITextView *alertTextView = [[UITextView alloc] initWithFrame:CGRectMake(kP(28), HZScreenH - kP(80) - kP(200) - 64, HZScreenW - 2 * kP(28), kP(200))];
    //    alertTextView.text = @"重要提示\n.为了确保顺利提现，填写的银行卡开户人姓名必须和本人一致\n.银行卡号仅支持储蓄卡，不要填写信用卡";
    //    alertTextView.font = [UIFont systemFontOfSize:kP(28)];
    //    alertTextView.textColor = [UIColor lightGrayColor];
    //    alertTextView.backgroundColor = [UIColor clearColor];
    //    alertTextView.userInteractionEnabled = NO;
    //    [self.bankCardManageTableView addSubview:alertTextView];
    
    UITextView *alertTextView = [[UITextView alloc] initWithFrame:CGRectMake(kP(28), self.view.height - kP(80) - kP(200), HZScreenW - 2 * kP(28), kP(200))];
    alertTextView.text = @"重要提示\n.为了确保顺利提现，填写的银行卡开户人姓名必须和本人一致\n.银行卡号仅支持储蓄卡，不要填写信用卡";
    alertTextView.font = [UIFont systemFontOfSize:kP(28)];
    alertTextView.textColor = [UIColor lightGrayColor];
    alertTextView.backgroundColor = [UIColor clearColor];
    alertTextView.userInteractionEnabled = NO;
    [self.view addSubview:alertTextView];
    //
    //    HZBankListView *bankListView = [[HZBankListView alloc] initWithFrame:self.view.bounds];
    //    [self.view addSubview:bankListView];
    //    self.bankListView = bankListView;
    UIView *bgCover = [[UIView alloc] initWithFrame:self.view.bounds];
    bgCover.backgroundColor = [UIColor grayColor];
    bgCover.alpha = 0.8;
    bgCover.hidden = YES;
    //    [self.view addSubview:bgCover];
    [[UIApplication sharedApplication].keyWindow addSubview:bgCover];
    [bgCover addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissTheBankList)]];
    self.bgCover = bgCover;
    
    
    CGFloat bankNameListTableViewW = kP(508);
    CGFloat bankNameListTableViewH = kP(876);
    CGFloat bankNameListTableViewX = HZScreenW * 0.5 - bankNameListTableViewW * 0.5;
    CGFloat bankNameListTableViewY = HZScreenH * 0.5 - bankNameListTableViewH * 0.5;
    UITableView *bankNameListTableView = [[UITableView alloc] initWithFrame:CGRectMake(bankNameListTableViewX, bankNameListTableViewY, bankNameListTableViewW, bankNameListTableViewH) style:UITableViewStylePlain];
    bankNameListTableView.dataSource = self;
    bankNameListTableView.delegate = self;
    bankNameListTableView.layer.cornerRadius = kP(6);
    bankNameListTableView.clipsToBounds = YES;
    bankNameListTableView.hidden = YES;
    bankNameListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    [self.view addSubview:bankNameListTableView];
    [[UIApplication sharedApplication].keyWindow addSubview:bankNameListTableView];
    self.bankNameListTableView = bankNameListTableView;
    
    //
    self.selNameArr = @[@"中国银行", @"中国农业银行", @"中国建设银行", @"中国工商银行", @"中国民生银行", @"兴业银行", @"上海浦东发展银行", @"中国邮政储蓄银行", @"中信银行", @"招商银行", @"交通银行"];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ([tableView isEqual:self.bankNameListTableView]) {
        return 1;
    } else {
        return 2;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([tableView isEqual:self.bankNameListTableView]) {
        return self.selNameArr.count;
    } else {
        if (section == 0) {
            return 3;
        }
        return 1;
    }
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:self.bankNameListTableView]) {
        
        return [self setBankListCellWithTableView:tableView cellForRowAtIndexPath:indexPath];
    } else {
        return [self setCellWithTableView:tableView cellForRowAtIndexPath:indexPath];
    }
    
}

- (UITableViewCell *)setBankListCellWithTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HZSelBankNameCell *selBankNameCell = [HZSelBankNameCell cellWithTableView:tableView];
    
    selBankNameCell.selBankName = self.selNameArr[indexPath.row];
    
    return selBankNameCell;
}


- (UITableViewCell *)setCellWithTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:kP(32)];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:kP(32)];
    
    //即将绘制cell的时候 (防止reload 重复添加 内存💥)
    for (UITextField *texV in cell.contentView.subviews) {
        [texV removeFromSuperview];
    }
    
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0: {
                cell.textLabel.text = @"真实姓名";
                NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
                if (userDict) {
                    cell.detailTextLabel.text = userDict[@"userName"];
                }
                
            }
                break;
                
            case 1:
                
                
                cell.textLabel.attributedText = [self getAttributedStringShowIconOrNot:self.nowIsEdit];
                cell.textLabel.userInteractionEnabled = self.nowIsEdit;
                [cell.textLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(scanTheBankCard)]];
                self.bankCardTextField.frame = CGRectMake(kP(30)+CGRectGetMaxX(cell.textLabel.frame), cell.height * 0.5 - kP(50) * 0.5, HZScreenW - kP(14)-CGRectGetMaxX(cell.textLabel.frame) - kP(28), kP(50));
                [cell.contentView addSubview:self.bankCardTextField];
                break;
                
            case 2:
                
                cell.textLabel.text = @"开户行";
                self.bankTextField.frame = CGRectMake(kP(150), cell.height * 0.5 - kP(50) * 0.5, HZScreenW - kP(150) - kP(28), kP(50));
                [cell.contentView addSubview:self.bankTextField];
                break;
                
        }
        
    } else {
        cell.textLabel.text = @"开户网点";
        self.openNetTextField.frame = CGRectMake(kP(170), cell.height * 0.5 - kP(50) * 0.5, HZScreenW - kP(150) - kP(28), kP(50));
        [cell.contentView addSubview:self.openNetTextField];
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        
    }else {
        cell.textLabel.textColor = self.editBtn.selected ? [UIColor blackColor] : [UIColor lightGrayColor];
        cell.detailTextLabel.textColor = self.editBtn.selected ? [UIColor blackColor] : [UIColor lightGrayColor];
    }
    
    return cell;
    
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if ([tableView isEqual:self.bankNameListTableView]) {
        return 0.1;
    } else {
        if (section == 0) {
            return kP(20);
        }
        return kP(10);
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kP(0.1);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //    self.bankCardManageTableView.userInteractionEnabled = YES;
    //    self.editBtn.userInteractionEnabled = YES;
    if (![tableView isEqual:self.bankNameListTableView]) return;
    
    self.bgCover.hidden = YES;
    self.bankNameListTableView.hidden = YES;
    
    //
    HZSelBankNameCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *bankNameStr = cell.selBankNameLabel.text;
    self.bankTextField.text = bankNameStr;
}

#pragma mark -- editBtnClick

- (void)editBtnClick:(UIButton *)editBtn {
    
    self.editBtn.selected = !editBtn.selected;
    
    self.nowIsEdit = editBtn.selected;
    //
    if (editBtn.selected) {
        self.bankTextField.textColor = [UIColor blackColor];
        self.bankCardTextField.textColor = [UIColor blackColor];
        self.openNetTextField.textColor = [UIColor blackColor];
        self.bankTextField.userInteractionEnabled = YES;
        self.bankCardTextField.userInteractionEnabled = YES;
        self.openNetTextField.userInteractionEnabled = YES;
        self.bankTextField.placeholder = @"请选择银行";
        self.bankCardTextField.placeholder = @"请输入银行卡号";
        self.openNetTextField.placeholder = @"请填写支行或分行(选填)";
        
        [self.bankCardManageTableView reloadData];
        
    } else {
        self.bankTextField.textColor = [UIColor lightGrayColor];
        self.bankCardTextField.textColor = [UIColor lightGrayColor];
        self.openNetTextField.textColor = [UIColor lightGrayColor];
        self.bankTextField.userInteractionEnabled = NO;
        self.bankCardTextField.userInteractionEnabled = NO;
        self.openNetTextField.userInteractionEnabled = NO;
        self.bankTextField.placeholder = @"";
        self.bankCardTextField.placeholder = @"";
        self.openNetTextField.placeholder = @"";
        [self postToSave];
    }
    
    
    //
    //    [self.bankCardManageTableView reloadData];
    
}


#pragma mark -- 保存数据到服务器
- (void)postToSave {
    
    
    
    //    // 1. 判断是否内容有更新
    //    NSLog(@"---------BOOL------%@", [self textValueChanged:self.bankTextField] == YES ? @"YES":@"NO");
    //    NSLog(@"---------BOOL------%@", [self textValueChanged:self.bankCardTextField] == YES ? @"YES":@"NO");
    //    NSLog(@"---------BOOL------%@", [self textValueChanged:self.openNetTextField] == YES ? @"YES":@"NO");
    //    if ([self textValueChanged:self.bankTextField] == NO && [self textValueChanged:self.bankCardTextField] == NO && [self textValueChanged:self.openNetTextField] == NO) return;
    
    if (self.bankCardTextField.text.length < 15) {
        [MBProgressHUD showError:@"请输入正确银行卡号" toView:nil];
        [self postError];
        return;
    }
    
    
    //2.
    if (self.bankTextField.text.length == 0 || self.bankCardTextField.text.length == 0) {
        
        //
        //self.editBtn.selected = YES;
        [self justAlert];
        [self postError];
        
    } else {
        
        self.wallet.depositBank = self.bankTextField.text;
        self.wallet.bankAccount = self.bankCardTextField.text;
        NSString *openNetStr = self.openNetTextField.text;
        // 判断是否含有emoji
        BOOL isContainsEmoji = [NSString stringContainsEmoji:openNetStr];
        // 转emojiStr成unicodeStr
        if (isContainsEmoji) {
            openNetStr = [self getUnicodeStrWithEmojiStr:openNetStr];
        }
        self.wallet.bankAddress = openNetStr;
        
        [self.activityIndicator startAnimating];
        [HZWalletHttpsTool updateWalletInforWithWallet:self.wallet success:^(id responseObject) {
            NSLog(@"---------更新成功!-----%@", responseObject);
            
            NSDictionary *dataDict = responseObject;
            NSString *stateCode = dataDict[@"state"];
            if (stateCode.integerValue != 200) {
                [self.activityIndicator stopAnimating];
                [MBProgressHUD showOnlyTextToView:nil title:@"请检查您的银行账号或者开户行是否输入有误"];
                [self postError];
                return;
            }
            
            
            // 保存成功在保存;
//            //  对象转字典
            NSDictionary *orignDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"walletDict"];
            // 改变有变动的!!!
            NSMutableDictionary *oriSubDict = [orignDict mutableCopy];

            //
            NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
            //
            [oriSubDict setValue:self.wallet.bankAccount forKey:@"bankAccount"];
            [oriSubDict setValue:self.wallet.bankAddress forKey:@"bankAddress"];
            [oriSubDict setValue:userDict[@"userName"] forKey:@"bankOwnerName"];
            [oriSubDict setValue:self.wallet.depositBank forKey:@"depositBank"];

            [[NSUserDefaults standardUserDefaults] setObject:oriSubDict forKey:@"walletDict"];
            [self.bankCardManageTableView reloadData];
            [self.activityIndicator stopAnimating];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            
        }];
    }
    
    
}

-(void)postError{
    self.editBtn.selected = YES;
    self.nowIsEdit = YES;
    self.bankTextField.userInteractionEnabled = YES;
    self.bankCardTextField.userInteractionEnabled = YES;
    self.openNetTextField.userInteractionEnabled = YES;
    self.bankTextField.textColor = [UIColor blackColor];
    self.bankCardTextField.textColor = [UIColor blackColor];
    self.openNetTextField.textColor = [UIColor blackColor];
    self.wallet.bankAccount = nil;
    self.wallet.bankAddress = nil;
    self.wallet.depositBank = nil;
    [self.bankCardManageTableView reloadData];
}

- (void)justAlert {
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"输入的内容不能为空" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
    }
    [alertCtr addAction:ensureAlertAction];
    [self presentViewController:alertCtr animated:YES completion:nil];
    [self.activityIndicator stopAnimating];
}
#pragma mark -- UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField == self.bankTextField){
        if (textField.isAskingCanBecomeFirstResponder == NO) {
            //NSLog(@"do something...2");
            self.bgCover.hidden = NO;
            self.bankNameListTableView.hidden = NO;
            [self.view endEditing:YES];
        }
        return NO;
    }else{
        
        return YES;
    }
    
}

-(void)textFieldDidChange:(UITextField *)textField
{
    CGFloat maxLength = 24;
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    if (!position || !selectedRange)
    {
        if (toBeString.length > maxLength)
        {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1)
            {
                textField.text = [toBeString substringToIndex:maxLength];
            }
            else
            {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}


#pragma mark -- 退去键盘
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark -- getUnicodeStrWithEmojiStr
- (NSString *)getUnicodeStrWithEmojiStr:(NSString *)emojiStr {
    
    return [emojiStr stringByReplacingEmojiUnicodeWithCheatCodes];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//#pragma mark -- textValueChanged
//
//- (BOOL)textValueChanged:(UITextField *)textField {
//
////        if ([textField isEqual:self.bankTextField]) {
////
////        if ([self.bankTextField.text isEqualToString:self.bankStr]) {// 无变化
////            self.bankStr = self.bankTextField.text;
////            NSLog(@"---------+++++++++++++++++++++++无变化+++++***********************=====%@", textField.text);
////
////            return NO;
////        } else {//  有变化
////             self.bankStr = self.bankTextField.text;
////            NSLog(@"---------+++++++++++++++++++++++++有变化+++***********************=====%@", textField.text);
////
////            return YES;
////        }
////
////
////
////        } else {
////
////
////             return YES;
////    }
//    if ([textField.text isEqualToString:self.bankStr]) {// 无变化
//                    self.bankStr = textField.text;
//                    NSLog(@"---------+++++++++++++++++++++++无变化+++++***********************=====%@", textField.text);
//
//                    return NO;
//                } else {//  有变化
//                     self.bankStr = textField.text;
//                    NSLog(@"---------+++++++++++++++++++++++++有变化+++***********************=====%@", textField.text);
//
//                    return YES;
//                }
//
//
//
//}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)dismissTheBankList{
    
    self.bgCover.hidden = YES;
    self.bankNameListTableView.hidden = YES;
}

-(NSMutableAttributedString*)getAttributedStringShowIconOrNot:(BOOL)show{
    
    NSMutableAttributedString* attributeString = [[NSMutableAttributedString alloc] initWithString:@"银行卡号  "];
    
    if (show) {
        
        NSTextAttachment* textAttachment = [[NSTextAttachment alloc] init];
        textAttachment.image = [UIImage imageNamed:@"cardScan"];
        textAttachment.bounds = CGRectMake(0, -0.5, kP(28), kP(28));  // 微调图片位置
        
        NSAttributedString* imageAttachment = [NSAttributedString attributedStringWithAttachment:textAttachment];
        [attributeString insertAttributedString:imageAttachment atIndex:attributeString.length]; // 插入图片
        
    }
    
    
    return attributeString;
    
}

#pragma mark -
#pragma mark--scanBankCard--
-(void)scanTheBankCard{
    
    // 跳转;
    NSLog(@"跳转银行卡扫描");
    HZScanToolViewController *codeVC = [[HZScanToolViewController alloc]initWithScanType:scanTypeBankCard];
    [self.navigationController pushViewController:codeVC animated:YES];
    codeVC.scanResultInfoBlock = ^(NSString *barCode, NSDictionary *bankInfo) {
        
        //        self.bankTextField.text = bankInfo[@"bankName"];
        self.bankCardTextField.text = bankInfo[@"bankNumber"];
        
    };
    
    
    
}

/**校验是否是银行卡号*/
//- (BOOL) checkCardNo:(NSString*) cardNo{
//    int oddsum = 0;     //奇数求和
//    int evensum = 0;    //偶数求和
//    int allsum = 0;
//    int cardNoLength = (int)[cardNo length];
//    int lastNum = [[cardNo substringFromIndex:cardNoLength-1]intValue];
//
//    cardNo = [cardNo substringToIndex:cardNoLength -1];
//    for (int i = cardNoLength -1; i>=1;i--) {
//        NSString *tmpString = [cardNo substringWithRange:NSMakeRange(i-1,1)];
//        int tmpVal = [tmpString intValue];
//        if (cardNoLength % 2 ==1) {
//            if((i % 2) == 0){
//                tmpVal *= 2;
//                if(tmpVal>=10)
//                    tmpVal -= 9;
//                evensum += tmpVal;
//            }else{
//                oddsum += tmpVal;
//            }
//        }else{
//            if((i % 2) == 1){
//                tmpVal *= 2;
//                if(tmpVal>=10)
//                    tmpVal -= 9;
//                evensum += tmpVal;
//            }else{
//                oddsum += tmpVal;
//            }
//        }
//    }
//
//    allsum = oddsum + evensum;
//    allsum += lastNum;
//    if((allsum % 10) == 0)
//        return YES;
//    else
//        return NO;
//}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar=YES;
}


@end
