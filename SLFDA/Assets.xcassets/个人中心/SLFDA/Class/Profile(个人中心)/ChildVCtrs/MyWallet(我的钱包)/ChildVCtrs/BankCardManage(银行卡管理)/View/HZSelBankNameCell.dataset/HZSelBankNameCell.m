//
//  HZSelBankNameCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/12.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZSelBankNameCell.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZSelBankNameCell ()
@end
NS_ASSUME_NONNULL_END
@implementation HZSelBankNameCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    UILabel *selBankNameLabel = [[UILabel alloc] init];
    [selBankNameLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
//    selBankNameLabel.numberOfLines = 0;
    [self addSubview:selBankNameLabel];
    self.selBankNameLabel = selBankNameLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
   //
    [self setSubViewContent];
    
    //
    CGFloat selBankNameLabelX = kP(56);
    CGFloat maxWidth = self.width - 2 * selBankNameLabelX;
    CGSize selBankNameSize = [self.selBankNameLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(35)];
    
    if (selBankNameSize.width > maxWidth) {
        self.selBankNameLabel.font = [UIFont systemFontOfSize:kP(28)];
    }
    CGFloat selBankNameLabelW = selBankNameSize.width;
    CGFloat selBankNameLabelH = selBankNameSize.height;
    CGFloat selBankNameLabelY = self.height * 0.5 - selBankNameLabelH * 0.5;
    self.selBankNameLabel.frame = CGRectMake(selBankNameLabelX, selBankNameLabelY, selBankNameLabelW, selBankNameLabelH);
    
    
}


- (void)setSubViewContent {
    self.selBankNameLabel.text = _selBankName;
}

//- (void)setSelBankNameLabel:(UILabel *)selBankNameLabel {
//    _selBankNameLabel.text = _selBankName;
//    
//    
//}
//
- (void)setSelBankName:(NSString *)selBankName {
    _selBankName = selBankName;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}


@end
