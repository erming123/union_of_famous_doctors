//
//  HZMyWalletViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZMyWalletViewController.h"
#import "HZWalletTopView.h"
#import "HZMyIncomeCell.h"
#import "MJRefresh.h"
#import "HZWallet.h"
#import "HZBankCardManageViewController.h"
#import "MJExtension.h"
#import "HZCapital.h"
#import "UIViewController+PanBack.h"
#import "HZWalletHttpsTool.h"
@interface HZMyWalletViewController ()<UITableViewDataSource, UITableViewDelegate, HZWalletTopViewDelegate>

//** <#注释#>*/
@property (nonatomic, strong) HZTableView *myIncomeListTableView;
//** <#注释#>*/
@property (nonatomic, strong) HZWallet *wallet;
//** <#注释#>*/
@property (nonatomic, strong) HZWalletTopView *walletTopView;

//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *walletArr;
//** <#注释#>*/
@property (nonatomic, strong) UIImageView *noBillsImageView;
@property (nonatomic, copy) NSArray *capitalArr;
////** <#注释#>*/
//@property (nonatomic, strong) UILabel *errorLabel;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation HZMyWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor greenColor];
    [self setViewPanBack];
    //
    HZWalletTopView *walletTopView = [[HZWalletTopView alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, HZScreenH == 812.0 ? kP(452) : kP(432))];
    walletTopView.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    walletTopView.delegate = self;

    NSDictionary *walletDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"walletDict"];
    if ([walletDict isKindOfClass:[NSDictionary class]]) {
        HZWallet *wallet = [HZWallet mj_objectWithKeyValues:walletDict];
       // wallet = [HZWallet mj_objectWithKeyValues:[wallet propertyValueDict]];
        walletTopView.wallet = wallet;
    }

    [self.view addSubview:walletTopView];
    self.walletTopView = walletTopView;
    
//    //
//    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activityIndicator.center = walletTopView.center;
//    activityIndicator.color = [UIColor grayColor];
//    [walletTopView addSubview:activityIndicator];
//    self.activityIndicator = activityIndicator;

    
    [self getWalletInforData:walletTopView withBlock:^(HZWallet *wallet){
        
        walletTopView.wallet = wallet;
        NSLog(@"----wallet2-----%@", wallet);
    }];


    //
    HZTableView *myIncomeListTableView = [[HZTableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.walletTopView.frame), HZScreenW, HZScreenH - CGRectGetMaxY(self.walletTopView.frame)) style:UITableViewStylePlain];
    myIncomeListTableView.dataSource = self;
    myIncomeListTableView.delegate = self;
    myIncomeListTableView.separatorColor = [UIColor colorWithHexString:@"#D8D8D8" alpha:1.0];
//    myIncomeListTableView.backgroundColor = [UIColor colorWithHexString:@"#FAFBFC" alpha:1.0];
    myIncomeListTableView.tableFooterView = [UIView new];
    myIncomeListTableView.separatorInset = UIEdgeInsetsMake(kP(0), kP(28), kP(0), kP(0));
    [self.view addSubview:myIncomeListTableView];
    self.myIncomeListTableView = myIncomeListTableView;
    
    NSArray *arr = [[NSUserDefaults standardUserDefaults] objectForKey:@"bills"];
    self.capitalArr = [HZCapital mj_objectArrayWithKeyValuesArray:arr];
    
    
    //
    // 没有资金明细的时候
    UIImageView *noBillsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(390), kP(434))];
    noBillsImageView.image = [UIImage imageNamed:@"no_list"];
    noBillsImageView.center = self.view.center;
    noBillsImageView.y = kP(700);
    [self.view addSubview:noBillsImageView];
    self.noBillsImageView = noBillsImageView;
    
    noBillsImageView.center = self.myIncomeListTableView.center;

//    //3. 网络出现问题的时候
//    UILabel *errorLabel = [[UILabel alloc] init];
//    errorLabel.text = @"暂未获取到数据";
//    [errorLabel sizeToFit];
//    errorLabel.center = self.view.center;
//    errorLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
//    errorLabel.hidden = YES;
//    [self.view addSubview:errorLabel];
//    self.errorLabel = errorLabel;
//
    
    
    //
    [self pushToRefresh];
    
    
    // 获取数据
//    [self getWalletInforData];
//    walletTopView.wallet = self.wallet;
    
//     NSLog(@"------222222222--%@", self.wallet);
    
    
    
}


- (void)pushToRefresh {
    
    //下拉刷新
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        
        [self refreshWalletData];
    }];
    self.myIncomeListTableView.mj_header = refreshHeader;
    //
//    //下拉刷新
//    self.myIncomeListTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
//
//
//        [self refreshWalletData];
//
//        // 结束刷新
//        [self.myIncomeListTableView.mj_header endRefreshing];
//
//    }];
//
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.myIncomeListTableView.mj_header.automaticallyChangeAlpha = YES;
//
//    // 上拉刷新
//    self.myIncomeListTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
//
//
//        [self refreshWalletData];
//        // 结束刷新
//        [self.myIncomeListTableView.mj_footer endRefreshing];
//
//    }];
}

#pragma mark -- 获取钱包信息和资金明细数据
- (void)getWalletInforData:(HZWalletTopView *)walletTopView withBlock:(void(^)(HZWallet *wallet))block {
        [HZWalletHttpsTool getWalletInforWithSuccess:^(id responseObject) {
            
            
            NSDictionary *walletDict = responseObject[@"results"];
            HZWallet *wallet = [HZWallet mj_objectWithKeyValues:walletDict[@"wallet"]];
           // wallet = [HZWallet mj_objectWithKeyValues:[wallet propertyValueDict]];
            [[NSUserDefaults standardUserDefaults] setObject:walletDict[@"wallet"] forKey:@"walletDict"];
            
            if (block) {
                block(wallet);
            }
        } failure:nil];
}

#pragma mark -- <#class#> 

- (NSArray *)capitalArr {
    if (!_capitalArr) {
        _capitalArr = [NSArray array];
    }
    
    return _capitalArr;
}
#pragma mark -- <#class#>
- (void)refreshWalletData {
    //
//    self.errorLabel.hidden = YES;
    
    
    //
    [HZWalletHttpsTool getWalletInforWithSuccess:^(id responseObject) {
        NSDictionary *walletDict = responseObject[@"results"];
        HZWallet *wallet = [HZWallet mj_objectWithKeyValues:walletDict[@"wallet"]];
        wallet = [HZWallet mj_objectWithKeyValues:[wallet propertyValueDict]];
        self.wallet = wallet;
        [self.myIncomeListTableView.mj_header endRefreshing];
    } failure:nil];
    
    //
    [HZWalletHttpsTool getCapitalListWithSuccess:^(id responseObject) {
        
        NSLog(@"---------sdfc------------:%@", responseObject);
        
        NSDictionary *resultDict = responseObject[@"results"];
        NSArray *billsArr = resultDict[@"bills"];

        
        self.capitalArr = [HZCapital mj_objectArrayWithKeyValuesArray:billsArr];
        [self.myIncomeListTableView reloadData];
        [[NSUserDefaults standardUserDefaults] setObject:billsArr forKey:@"bills"];
        [self.myIncomeListTableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //        [self.activityIndicator stopAnimating];
        //        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取订单信息" preferredStyle:UIAlertControllerStyleAlert];
        //        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        //        [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
        //        [alertCtr addAction:ensureAlertAction];
        //        [self presentViewController:alertCtr animated:YES completion:nil];
//        self.errorLabel.hidden = NO;
        [self.myIncomeListTableView.mj_header endRefreshing];
    }];

}
#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.capitalArr.count != 0) {
        
        self.noBillsImageView.hidden = YES;
        
    } else {
        return self.noBillsImageView.hidden ? 1: 0;
    }
    
    return self.capitalArr.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HZMyIncomeCell *incomeCell = [HZMyIncomeCell cellWithTableView:tableView];
    
    if (self.capitalArr.count > 0) {
        incomeCell.captital = self.capitalArr[indexPath.row];
    } else {
        incomeCell.captital = nil;
    }
    
    return incomeCell;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(120);
}

#pragma mark -- HZWalletTopViewDelegate

// 返回上一个页面
- (void)rebackToLastView {
    
    [self.navigationController popViewControllerAnimated:YES];
}

// 去银行卡管理页面
- (void)enterToBankCardManageView {
     HZWallet *wallet;
     NSDictionary *walletDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"walletDict"];
    if ([walletDict isKindOfClass:[NSDictionary class]]) {
        wallet = [HZWallet mj_objectWithKeyValues:walletDict];
       // wallet = [HZWallet mj_objectWithKeyValues:[wallet propertyValueDict]];
    }
    //
    HZBankCardManageViewController *bankCardMgrVCtr = [HZBankCardManageViewController new];
    wallet.bankOwnerName = self.wallet.bankOwnerName;
    bankCardMgrVCtr.wallet = wallet;
    [self.navigationController pushViewController:bankCardMgrVCtr animated:YES];

}


- (void)setWalllet:(HZWallet *)walllet {
    _wallet = walllet;
    
}

// 去提现页面
- (void)enterToDepositView {
    
//    [self.navigationController popViewControllerAnimated:YES];
}

// 弹出提示框
- (void)popDepositAlertView {
    
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"什么是自动提现" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kP(30)],NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:attributedTitle.string message:@"如果您已经绑定银行，系统每个月会自动将钱提现到您的银行卡上(要求余额大于100元)" preferredStyle:UIAlertControllerStyleAlert];
    [alertCtr setValue:attributedTitle forKey:@"attributedTitle"];
    
    UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleDefault handler:nil];
    
    if (kCurrentSystemVersion >= 9.0) {
        [cancelAlertAction setValue:[UIColor colorWithHexString:@"21b8c6" alpha:1.0] forKey:@"titleTextColor"];
    }

    [alertCtr addAction:cancelAlertAction];
    [self presentViewController:alertCtr animated:YES completion:nil];
    
}

#pragma mark -- <#class#>
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshWalletData];
//    self.navigationController.navigationBarHidden = NO;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
   [self.navigationController setNavigationBarHidden:NO animated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
