//
//  HZWalletTopView.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/7.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZWallet;
@protocol HZWalletTopViewDelegate <NSObject>

- (void)rebackToLastView;
- (void)enterToBankCardManageView;
- (void)popDepositAlertView;
@end
@interface HZWalletTopView : UIView
@property (nonatomic, weak) id <HZWalletTopViewDelegate> delegate;
//** <#注释#>*/
@property (nonatomic, strong) HZWallet *wallet;

@end
