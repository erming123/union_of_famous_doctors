//
//  YHScanView.m
//  chartTest
//
//  Created by coder_xu on 2017/8/14.
//  Copyright © 2017年 coder_xu. All rights reserved.
//

#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ScreenHeight [UIScreen mainScreen].bounds.size.height

#import "HZBarScanView.h"
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>



@interface HZBarScanView ()<AVCaptureMetadataOutputObjectsDelegate,AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic,strong) AVCaptureSession *session;

@property (nonatomic,weak) UIImageView *centerView;

@property (nonatomic,weak) UIImageView *line;

@property (nonatomic, strong) UILabel *QRCodeTipLabel;

@property (nonatomic,strong)AVCaptureDeviceInput *input;
@property (nonatomic,strong) AVCaptureDevice *device;
@property (nonatomic,strong) AVCaptureVideoDataOutput *outPut2;




//视图层
@property (nonatomic, weak) AVCaptureVideoPreviewLayer* preView;
@property (nonatomic, strong) AVCaptureMetadataOutput* CaptureOutPut;

@property (nonatomic,strong) UIButton *lightBtn;


@end

@implementation HZBarScanView

-(UIButton *)lightBtn{
    if (!_lightBtn) {
        _lightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_lightBtn setImage:[UIImage imageNamed:@"bulb"] forState:UIControlStateNormal];
        [_lightBtn setImage:[UIImage imageNamed:@"bulbSel"] forState:UIControlStateSelected];
        [_lightBtn addTarget:self action:@selector(lightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _lightBtn.frame = CGRectMake(self.centerView.center.x-20, CGRectGetMaxY(self.centerView.frame)-40, 40, 40);
        
        [self addSubview:_lightBtn];
        
    }
    return _lightBtn;
}

- (UILabel *)QRCodeTipLabel {
    if (!_QRCodeTipLabel) {
        _QRCodeTipLabel = [[UILabel alloc] init];
        _QRCodeTipLabel.font = [UIFont systemFontOfSize:12];
        _QRCodeTipLabel.backgroundColor = [UIColor clearColor];
        _QRCodeTipLabel.textAlignment = NSTextAlignmentCenter;
        _QRCodeTipLabel.textColor = [UIColor whiteColor];
        _QRCodeTipLabel.numberOfLines = 0;
        //旋转90度
        _QRCodeTipLabel.transform = CGAffineTransformMakeRotation(M_PI_2);
        _QRCodeTipLabel.frame = CGRectMake(0, 0, 20, self.bounds.size.height);
        
    }
    return _QRCodeTipLabel;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        [self setUp];
    
    }
    
    return self;
}

-(void)setUp {
    
    
    self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    self.input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:nil];
    self.CaptureOutPut = [[AVCaptureMetadataOutput alloc]init];
    [self.CaptureOutPut setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    
    self.outPut2 = [[AVCaptureVideoDataOutput alloc] init];
    [self.outPut2 setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    

    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    
    if (self.input) {
        
        [_session addInput:self.input];
    }
    
    if (self.CaptureOutPut) {
        
        [_session addOutput:self.CaptureOutPut];
        NSMutableArray *a = [[NSMutableArray alloc] init];
        //接受类型 (只要条码)
//        if ([output.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeQRCode]) {
//            [a addObject:AVMetadataObjectTypeQRCode];
//        }
        if ([self.CaptureOutPut.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeEAN13Code]) {
            [a addObject:AVMetadataObjectTypeEAN13Code];
        }
        if ([self.CaptureOutPut.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeEAN8Code]) {
            [a addObject:AVMetadataObjectTypeEAN8Code];
        }
        if ([self.CaptureOutPut.availableMetadataObjectTypes containsObject:AVMetadataObjectTypeCode128Code]) {
            [a addObject:AVMetadataObjectTypeCode128Code];
        }
        self.CaptureOutPut.metadataObjectTypes=a;
    }
    
    if (self.outPut2) {
        [_session addOutput:self.outPut2];
    }
    
    
    //AVCaptureAutoFocusRangeRestrictionNear ios7版本新增的属性，允许我们使用一个范围的约束对这个功能进行定制，我们扫描的大部分条码距离都不远，所以可以通过缩小扫描区域来提升识别的成功率。检测是否支持该功能
    if (_device.autoFocusRangeRestrictionSupported) {
        if ([_device lockForConfiguration:nil]) {
            _device.autoFocusRangeRestriction =
            AVCaptureAutoFocusRangeRestrictionNear;
            [_device unlockForConfiguration];
        }
    }

    
    //视图层
    AVCaptureVideoPreviewLayer * layer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    layer.videoGravity=AVLayerVideoGravityResizeAspectFill;
    layer.frame=self.layer.bounds;
    self.preView = layer;
    
    [self setOverlayView];
    
    self.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.4];
    
    //默认宽度
  //  _scanWidth = 250;
  
}

-(void)setTips:(NSString *)tips{
    _tips = tips;
    
    self.QRCodeTipLabel.text = tips;
}

-(void)setScanWidth:(int)scanWidth{
    
    _scanWidth = scanWidth;
    
    [self layoutSubviews];
    
    
}

-(void)setOverlayView{
    


    UIImageView *centerView = [[UIImageView alloc]init];
    //扫描框图片的拉伸，拉伸中间一块区域
    UIImage *scanImage = [UIImage imageNamed:@"QR"];
    CGFloat top = 34*0.5-1; // 顶端盖高度
    CGFloat bottom = top ; // 底端盖高度
    CGFloat left = 34*0.5-1; // 左端盖宽度
    CGFloat right = left; // 右端盖宽度
    UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
    scanImage = [scanImage resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
    
    centerView.image = scanImage;
   // centerView.contentMode = UIViewContentModeScaleAspectFit;
    centerView.backgroundColor = [UIColor clearColor];
    [self addSubview:centerView];
    _centerView = centerView;

    
    //扫描线
    UIImageView *line = [[UIImageView alloc]init];
    line.image = [UIImage imageNamed:@"scanline"];
    line.contentMode = UIViewContentModeScaleAspectFill;
    line.backgroundColor = [UIColor clearColor];
    line.clipsToBounds = YES;
    [self addSubview:line];
    _line = line;
    
    [self addSubview:self.QRCodeTipLabel];
    
    
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    //中间框
     _centerView.frame = CGRectMake((ScreenWidth - self.scanWidth)*0.5 + 15,(ScreenHeight - self.scanWidth - 150)*0.5 , self.scanWidth, self.scanWidth+150);
    //扫描线
    _line.frame = CGRectMake(CGRectGetMaxX(_centerView.frame)-3,CGRectGetMinY(_centerView.frame) -2 , 3, _centerView.frame.size.height -4);
    
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(contextRef, self.backgroundColor.CGColor);
    CGContextFillRect(contextRef, rect);
    CGFloat tipLabelPadding = 50.0f;
    CGRect QRCodeTipLabelFrame = self.QRCodeTipLabel.frame;
    QRCodeTipLabelFrame.origin.x = CGRectGetMinX(self.centerView.frame) - tipLabelPadding;
    self.QRCodeTipLabel.frame = QRCodeTipLabelFrame;
    
    CGContextClearRect(contextRef, self.centerView.frame);
    CGContextSaveGState(contextRef);
   
}



/**
 *  开始扫码
 */
- (void)showInView:(UIView*)view{
    
    [view.layer addSublayer:self.preView];
    [view addSubview:self];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"Function: %s,line : %d 开启相机",__FUNCTION__,__LINE__);
        [self.session startRunning];
        [self setScanRect];
    });
     [self addAnimation];
    

}


/**
 * 设置扫描区域
 */
-(void)setScanRect{
    
    CGRect intertRect = [self.preView metadataOutputRectOfInterestForRect:self.centerView.frame];
    
    CGRect layerRect = [self.preView rectForMetadataOutputRectOfInterest:intertRect];
    
    NSLog(@"%@,  %@",NSStringFromCGRect(intertRect),NSStringFromCGRect(layerRect));
    
    self.CaptureOutPut.rectOfInterest = intertRect;
    

    
}

/**
 *  结束扫码
 */
- (void)stopRunning{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        NSLog(@"Function: %s,line : %d 关闭相机",__FUNCTION__,__LINE__);
        if ([self.session isRunning]) {
            [self.session stopRunning];
            [self removeAnimation];
        }
    });
    [self removeAnimation];
    
    

}




/**
 *  添加扫码动画
 */
- (void)addAnimation{
    

    self.line.hidden = NO;
    CABasicAnimation *animation = [self moveYTime:2 fromY:[NSNumber numberWithFloat:CGRectGetMaxX(_centerView.frame)-2] toY:[NSNumber numberWithFloat:CGRectGetMinX(_centerView.frame)+2] rep:OPEN_MAX];
    [self.line.layer addAnimation:animation forKey:@"LineAnimation"];
}

- (CABasicAnimation *)moveYTime:(float)time fromY:(NSNumber *)fromY toY:(NSNumber *)toY rep:(int)rep{
    
    CABasicAnimation *animationMove = [CABasicAnimation animationWithKeyPath:@"position.x"];
    [animationMove setFromValue:fromY];
    [animationMove setToValue:toY];
    animationMove.duration = time;
    animationMove.repeatCount  = rep;
    animationMove.fillMode = kCAFillModeForwards;
    animationMove.removedOnCompletion = NO;
    animationMove.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return animationMove;
}


/**
 *  去除扫码动画
 */
- (void)removeAnimation{
    
    [self.line.layer removeAnimationForKey:@"LineAnimation"];
    self.line.hidden = YES;
}



/**
 *  移除监听
 */
- (void)dealloc{
    
    
#if !TARGET_IPHONE_SIMULATOR
    [_session stopRunning];
#endif
    [_session removeInput:self.input];
    [_session removeOutput:self.CaptureOutPut];
    [_session removeOutput:self.outPut2];
    
}


  

#pragma mark - AVCaptureMetadataOutputObjectsDelegate Delagate

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection {
    

    BOOL isAvailable = YES;
    if (metadataObjects.count > 0 && isAvailable == YES) {
        isAvailable = NO;
        NSString *metadataString = nil;
        AudioServicesPlaySystemSound(1360);
        AVMetadataMachineReadableCodeObject *MetadataObject = [metadataObjects objectAtIndex:0];
        metadataString = MetadataObject.stringValue;
        [self stopRunning];
        self.successScanBlock(metadataString);
        
    }
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection{
    
    //检测光线明暗
    
    CFDictionaryRef metadataDict = CMCopyDictionaryOfAttachments(NULL,sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    NSDictionary *metadata = [[NSMutableDictionary alloc] initWithDictionary:(__bridge NSDictionary*)metadataDict];
    CFRelease(metadataDict);
    NSDictionary *exifMetadata = [[metadata objectForKey:(NSString *)kCGImagePropertyExifDictionary] mutableCopy];
    float brightnessValue = [[exifMetadata objectForKey:(NSString *)kCGImagePropertyExifBrightnessValue] floatValue];
    
    //NSLog(@"brightnessValue==%f",brightnessValue);
    
    // 根据brightnessValue的值来打开和关闭闪光灯
    BOOL result = [self.device hasTorch];// 判断设备是否有闪光灯
    if ((brightnessValue < 0) && result) {// 可以打开闪光灯
        
        self.lightBtn.hidden = NO;
        
    }else if((brightnessValue > 0) && result) {// 不需要闪光灯
        //灯打开后不隐藏
        self.lightBtn.hidden = self.lightBtn.selected?NO:YES;
       
        
    }
    

}

-(void)lightBtnClick:(UIButton*)btn{
    btn.selected = !btn.selected;
    btn.selected?[self openFlashLight]:[self offFlashLight];
    
}

-(void)openFlashLight{
    
    [self.device lockForConfiguration:nil];
    [self.device setTorchMode: AVCaptureTorchModeOn];//开
    [self.device unlockForConfiguration];
    
    
}
-(void)offFlashLight{
    
    [self.device lockForConfiguration:nil];
    [self.device setTorchMode: AVCaptureTorchModeOff];//关
    [self.device unlockForConfiguration];
    
}



@end
