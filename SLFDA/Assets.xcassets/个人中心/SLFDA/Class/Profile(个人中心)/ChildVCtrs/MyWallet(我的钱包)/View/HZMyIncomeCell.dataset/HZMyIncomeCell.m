//
//  HZMyIncomeCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/7.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZMyIncomeCell.h"
#import "HZCapital.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZMyIncomeCell ()
@property (nonatomic, strong) UILabel *OPNameLabel;
@property (nonatomic, strong) UILabel *OPTimeLabel;
@property (nonatomic, strong) UILabel *incomeCountLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZMyIncomeCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
//        self.backgroundColor = [UIColor colorWithHexString:@"#FAFBFC" alpha:1.0];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UILabel *OPNameLabel = [[UILabel alloc] init];
    [OPNameLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    [self addSubview:OPNameLabel];
    self.OPNameLabel = OPNameLabel;
    
    UILabel *OPTimeLabel = [[UILabel alloc] init];
    [OPTimeLabel setTextFont:kP(28) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:OPTimeLabel];
    self.OPTimeLabel = OPTimeLabel;
    
    
    UILabel *incomeCountLabel = [[UILabel alloc] init];
    [incomeCountLabel setTextFont:kP(40) textColor:@"#4A4A4A" alpha:1.0];
    incomeCountLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:incomeCountLabel];
    self.incomeCountLabel = incomeCountLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    [self setLabelsContent];
    
    
    CGSize OPNameSize = [self.OPNameLabel.text getTextSizeWithMaxWidth:self.width textFontSize:kP(35)];
    CGFloat OPNameLabelW = OPNameSize.width;
    CGFloat OPNameLabelH = OPNameSize.height;
    CGFloat OPNameLabelX = kP(28);
    CGFloat OPNameLabelY = kP(20);
    self.OPNameLabel.frame = CGRectMake(OPNameLabelX, OPNameLabelY, OPNameLabelW, OPNameLabelH);
    
    
    CGSize OPTimeSize = [self.OPTimeLabel.text getTextSizeWithMaxWidth:self.width textFontSize:kP(30)];
    CGFloat OPTimeLabelW = OPTimeSize.width;
    CGFloat OPTimeLabelH = OPTimeSize.height;
    CGFloat OPTimeLabelX = OPNameLabelX;
    CGFloat OPTimeLabelY = CGRectGetMaxY(self.OPNameLabel.frame) + kP(20);
    self.OPTimeLabel.frame = CGRectMake(OPTimeLabelX, OPTimeLabelY, OPTimeLabelW, OPTimeLabelH);

    
    CGSize incomeCountSize = [self.incomeCountLabel.text getTextSizeWithMaxWidth:self.width textFontSize:kP(45)];
    CGFloat incomeCountLabelW = incomeCountSize.width;
    CGFloat incomeCountLabelH = incomeCountSize.height;
    CGFloat incomeCountLabelX = self.width - incomeCountLabelW - kP(28);
    CGFloat incomeCountLabelY = self.height * 0.5 - incomeCountLabelH * 0.5;
    self.incomeCountLabel.frame = CGRectMake(incomeCountLabelX, incomeCountLabelY, incomeCountLabelW, incomeCountLabelH);
}



- (void)setLabelsContent {
    
    if ([self.captital.bill_type isEqualToString:@"001"]) {// +
        
        NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
        if (!userDict) return;
        NSString *userId = userDict[@"openid"];
    
        self.OPNameLabel.text = [NSString stringWithFormat:@"%@的%@", self.captital.outpatient_name, [userId isEqualToString:self.captital.expert_openid] ? @"远程门诊" : @"本地陪诊"];
        self.OPTimeLabel.text = self.captital.treatment_date_time;
        self.incomeCountLabel.text = [NSString stringWithFormat:@"+%@",self.captital.sum_of_money];

    } else {// -
        self.OPNameLabel.text = [NSString stringWithFormat:@"余额提现(%@)", self.captital.cash_state_value];
        self.OPTimeLabel.text = self.captital.cash_time;
        self.incomeCountLabel.text = [NSString stringWithFormat:@"-%@",self.captital.sum_of_money];
    }
    
}

- (void)setCaptital:(HZCapital *)captital {
    _captital = captital;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
