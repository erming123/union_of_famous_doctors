//
//  QRCodeViewController.m
//  chartTest
//
//  Created by coder_xu on 2017/8/14.
//  Copyright © 2017年 coder_xu. All rights reserved.
//

#import "HZScanToolViewController.h"
#import "HZBarScanView.h"

#import "ZBarSDK.h"
#import "HZBankCardScanView.h"

#import <AVFoundation/AVFoundation.h>

@interface HZScanToolViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic,weak) HZBarScanView *barCodeScanV;

@property (nonatomic,weak) UIButton *NavBarRightBtn;

@property (nonatomic,assign) scanType type;

@end

@implementation HZScanToolViewController


-(instancetype)initWithScanType:(scanType)type{
    if (self = [super init]) {
        
        _type = type;
    }
    
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //暂时不需要
    //[self setNavBarItem];
    self.view.backgroundColor = [UIColor blackColor];
    self.title = @"扫一扫";
    //权限获取
    [self requestPermission];
}


- (void)loadScanView{
    
    NSThread *currentThred = [NSThread currentThread];
    if ([currentThred isMainThread]) {
        switch (self.type) {
            case scanTypeBarCode:
                [self scanBarCode];
                break;
            case scanTypeBankCard:
                [self scanBankCard2];
                break;
                
            default:
                break;
        }

    
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            switch (self.type) {
                case scanTypeBarCode:
                    [self scanBarCode];
                    break;
                case scanTypeBankCard:
                    [self scanBankCard2];
                    break;
                    
                default:
                    break;
            }

        });
        
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 扫描条码
-(void)scanBarCode{
        //暂不支持 从相册里面选 暂时关闭
        self.NavBarRightBtn.hidden = YES;
        HZBarScanView *scanV = [[HZBarScanView alloc]initWithFrame:self.view.bounds];
        scanV.scanWidth = kP(400);
        scanV.tips = @"条形码放入框内，即可自动扫描";
        [scanV showInView:self.view];
        self.barCodeScanV = scanV;
        WeakSelf(weakSelf)
       scanV.successScanBlock = ^(NSString *info) {
           
        StrongSelf(strongSelf)
        NSLog(@"扫描内容===%@",info);
        if (info) {
            //返回上层
            [strongSelf.navigationController popViewControllerAnimated:YES];
             strongSelf.scanResultInfoBlock(info, nil);
        }
    };
    
    
}



-(void)scanBankCard2{
    
    self.NavBarRightBtn.hidden = YES;
    HZBankCardScanView *preV = [[HZBankCardScanView alloc]initWithWithFrame:self.view.bounds ScanSize:CGSizeMake(kP(450), kP(718)) Andtips:@"请将扫描线对准银行卡号并对齐左右"];
    [preV startRunningCamera];
    [self.view addSubview:preV];
    WeakSelf(weakSelf)
    preV.successScanBlock = ^(NSString *bankName, NSString *bankNumber) {
        
        StrongSelf(strongSelf)
        [strongSelf.navigationController popViewControllerAnimated:YES];
        if ([bankName containsString:@"信用卡"]) {
            [MBProgressHUD showOnlyTextToView:nil title:@"暂不支持信用卡提现,请使用储蓄卡"];
            return ;
        }
        strongSelf.scanResultInfoBlock(nil, @{@"bankName":bankName,@"bankNumber":bankNumber});
    };
    
}


-(void)setNavBarItem{
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"相册" forState:UIControlStateNormal];
    rightBtn.frame = CGRectMake(0, 0, 40, 40);
    [rightBtn addTarget:self action:@selector(jumpTheAlbum) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    UIBarButtonItem *barItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = barItem;
    self.NavBarRightBtn = rightBtn;
    
}

-(void)jumpTheAlbum{
    
    
    ZBarReaderController *imagePicker = [ZBarReaderController new];
    imagePicker.showsHelpOnFail = NO; // 禁止显示读取失败页面
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];

    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    
    for(symbol in results) {
        
        break;
    }
    
    //因为有可能是二维码(过滤)
    //正则匹配(^[0-9]*$) 全是数字
    if (![self TheResultIsBarCodeOrQRcode:symbol.data]) {
        //从相册里面选取的不是条码
        return;
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //二维码字符串
        NSString *QRCodeString =  symbol.data;
        // 触发回调
        NSLog(@"内容是==%@",QRCodeString);
        [self.navigationController popViewControllerAnimated:YES];
        self.scanResultInfoBlock(QRCodeString, nil);
    }];
    
}
// 相册读取失败回调
-(void)readerControllerDidFailToRead:(ZBarReaderController *)reader withRetry:(BOOL)retry{
    
    if (retry) { //retry == 1 选择图片为非二维码。
        [reader dismissViewControllerAnimated:YES completion:^{
            
           self.scanResultInfoBlock(nil, nil);
            
        }];
        
    }
    return;
    
} 

- (BOOL)TheResultIsBarCodeOrQRcode:(id)code{
    
    NSString *zhenZeStr = @"^[0-9]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",zhenZeStr];
    return [predicate evaluateWithObject:code];
    
}

// 获取授权
- (void)requestPermission {
    
    
    {
        AVAuthorizationStatus status =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        switch (status) {
            case AVAuthorizationStatusNotDetermined: {
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if (granted) {
                        
                       // NSLog(@"已经授权");
                        //加载View;
                        [self loadScanView];
                        
                    } else {
                        
                        NSLog(@"拒绝访问");
                        [self showHUDErrorInMainThred];
                        
                    }
                }];
                break;
            }
            case AVAuthorizationStatusRestricted:
            case AVAuthorizationStatusDenied: {
              //  NSLog(@"未授权访问相机");
                [self showHUDErrorInMainThred];
                break;
            }
            case AVAuthorizationStatusAuthorized: {
               // permission = YES;
                //加载View;
                [self loadScanView];
                break;
            }
        }
        
    }
    
   
}

-(void) showHUDErrorInMainThred{
    NSThread *currentThred = [NSThread currentThread];
    if ([currentThred isMainThread]) {
        [MBProgressHUD showTextToView:nil title:@"请点击设置,授权使用相机" buttonTitle:@"设置" AndAction:^(UIButton *btn) {
            
            if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0f) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Privacy&path=CAMERA"]];
            }
        }];
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showTextToView:nil title:@"请点击设置,授权使用相机" buttonTitle:@"设置" AndAction:^(UIButton *btn) {
                
                if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0f) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                } else {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Privacy&path=CAMERA"]];
                }
            }];
            
        });
    }
    
}

-(void)dealloc{
    
    NSLog(@"扫码控制器销毁了");
    
}

@end
