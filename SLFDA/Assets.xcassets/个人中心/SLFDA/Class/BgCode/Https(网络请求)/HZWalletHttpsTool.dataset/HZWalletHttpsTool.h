//
//  HZWalletHttpsTool.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/25.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HZWallet;
@interface HZWalletHttpsTool : NSObject
+ (void)getWalletInforWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
+ (void)updateWalletInforWithWallet:(HZWallet *)wallet success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
+ (void)getCapitalListWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
////
- (void)getBankListWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
