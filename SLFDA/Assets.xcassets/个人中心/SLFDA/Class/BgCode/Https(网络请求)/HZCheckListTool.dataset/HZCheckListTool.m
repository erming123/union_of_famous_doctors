//
//  HZCheckListTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/12/2.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZCheckListTool.h"
#import "HZHttpsTool.h"
#import "HZPatient.h"
//#define checkListBasicStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/his/report"
@implementation HZCheckListTool
+ (void)getCheckListWithBookingOrderId:(NSString *)bookingOrderId patient:(HZPatient *)patient userHospitalId:(NSString *)userHospitalId success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *checkListStr;
    if (bookingOrderId != nil) {
        NSString *checkListBasicStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/his/report");
        checkListStr =[NSString stringWithFormat:@"%@?bookingOrderId=%@",checkListBasicStr, bookingOrderId];
    } else {
        NSString *checkListBasicStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/his/report");
        checkListStr =[NSString stringWithFormat:@"%@?outpatientRecordNumber=%@&identitycard=%@&hospitalId=%@&name=%@",checkListBasicStr, patient.patientRecordId, patient.idCard, userHospitalId, patient.name];
    }

    NSLog(@"-----checkListStr:---------:%@", checkListStr);

    [HZHttpsTool GET:checkListStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)getBiochemistryCheckListWithCheckModelId:(NSString *)checkModelId patientHospitalId:(NSString *)patientHospitalId success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    

    NSString *checkListBasicStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/his/report");
    NSString *biochemistryCheckListStr =[NSString stringWithFormat:@"%@/bioChemReport?jcybid=%@&hospitalId=%@",checkListBasicStr, checkModelId, patientHospitalId ? patientHospitalId : @""];
    
    NSLog(@"---------sssaaaawwwwwweeee---%@", biochemistryCheckListStr);
    [HZHttpsTool GET:biochemistryCheckListStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
                   if (success) {
                success(responseObject);
            }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];

}
@end
