//
//  HZUserTool.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/18.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZUserTool.h"
#import "HZHttpsTool.h"
#import "HZAccountTool.h"
#import "HZUser.h"
#import "JPUSHService.h"
//#define OrderUrlStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/user/info/get"
//#define AvatarUrlStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/doctor/photo/openid?openid="
// 别名,或者标签 设置状态码
static NSMutableDictionary *jpushSetCode;

@implementation HZUserTool
+ (void)getUserWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSString *OrderUrlStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/user/info/get");
    [HZHttpsTool GET:OrderUrlStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {

//        NSLog(@"--getUser成功-------%@", responseObject);
        
//        // 成功后设置 用户的 别名与 标签
//        [HZUserTool setUpJPUSHalisAndTagsWithUser:responseObject];
        
                if (success) {
                    success(responseObject);
                }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---getUser失败--%@", error);
        
        if (failure) {
            failure(task, error);
        }
    }];
    

}


+ (void)getUserDetailInforWithOpenId:(NSString *)openId success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *openIdUrlStr = [NSString stringWithFormat:@"cfdu/v2/doctor/elastic/%@", openId];
    [HZHttpsTool GET:kGatewayUrlStr(openIdUrlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---getDetailUserInfor失败--%@", error);
        
        if (failure) {
            failure(task, error);
        }
    }];
}

//+ (HZUser *)getUserInfor {
//    NSDictionary *dataDict = [[HZDB shareInstance] getBaseDataUserInforJsonDict];
//    NSDictionary *resultDict = dataDict[@"results"];
//    NSDictionary *userDict = resultDict[@"user"];
//    NSLog(@"------dsdsdsdsd---%@", userDict);
//    HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
// 
//    return user;
//}


+ (void)getAvatarWithAvatarOpenId:(NSString *)openId success:(void(^)(UIImage *image))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *AvatarUrlStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/photo/openid?openid=");
    NSString *avatarUrlStr = [NSString stringWithFormat:@"%@%@",AvatarUrlStr,openId];
    [HZHttpsTool GET:avatarUrlStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSString *avatarStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        avatarStr = [[avatarStr componentsSeparatedByString:@"base64,"] lastObject];
        NSData *_decodedImageData   = [[NSData alloc] initWithBase64EncodedString:avatarStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage *image = [UIImage imageWithData:_decodedImageData];
//        NSLog(@"------dsdsdssddsdsdsdssdsdsdsdsdssddsdsdsdsd---%@", image);
        
        if (success) {
            success(image);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        NSLog(@"------ddddddsssssssssss---%@", error);
        if (failure) {
            failure(task, error);
        }
    }];
}


//

+ (void)getAvatarWithFaceUrl:(NSString *)faceUrl success:(void(^)(UIImage *image))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v1/openapi/json/doctor/photo/get?url=%@", faceUrl];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSString *avatarStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        avatarStr = [[avatarStr componentsSeparatedByString:@"base64,"] lastObject];
        NSData *_decodedImageData   = [[NSData alloc] initWithBase64EncodedString:avatarStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage *image = [UIImage imageWithData:_decodedImageData];
        if (success) {
            success(image);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        NSLog(@"-----sssaa----%@", error);
        if (failure) {
            failure(task, error);
        }
    }];

}

#pragma mark -- 获取个人简介和擅长-
+ (void)getMyIntroduceWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZHttpsTool GET:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/honor") headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        
        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        if (failure) {
            failure(task, error);
        }
    }];
}
+ (void)getMySkillWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZHttpsTool GET:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/speciality") headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        if (failure) {
            failure(task, error);
        }
    }];
}

#pragma mark -- 上传个人简介和擅长-
+ (void)upLoadMyIntroduceWithIntroduceStr:(NSString *)introduceStr userOpenid:(NSString *)userOpenid success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
//    NSLog(@"---123456------%@", introduceStr);
    NSDictionary *paramDict = @{@"honor" : introduceStr, @"openid" : userOpenid};
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor?_method=PUT") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)upLoadMySkillWithSkillStr:(NSString *)skillStr userOpenid:(NSString *)userOpenid success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSDictionary *paramDict = @{@"speciality" : skillStr, @"openid" : userOpenid};
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor?_method=PUT") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
//        NSLog(@"----upLoadSkill成功-----%@", responseObject);
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"-----upLoadSkill失败----%@", error);
        
        if (failure) {
            failure(task, error);
        }
    }];

}


#pragma mark -- 上传头像和证书
+ (void)upLoadMyAvatarWithAvatarImage:(UIImage *)avatarImage userOpenid:(NSString *)userOpenid progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZHttpsTool Upload:kGatewayUrlStr(@"doctor/aws/s3/upload") headDomainValue:nil uploadImage:avatarImage parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress);
        }
    } success:^(id responseObject) {
        NSString *imageUrlStr;
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSArray *filesArr = resultsDict[@"files"];
            imageUrlStr  = filesArr[0];
        }
        
        if ([NSString isBlankString:imageUrlStr]) return;
        
        NSDictionary *paramDict = @{@"facePhotoUrl" : imageUrlStr, @"openid" : userOpenid};
        [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor?_method=PUT") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if (failure) { //
                failure(task, error);
            }
        }];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}
+ (void)upLoadMyAptitudeWithAptitudeImage:(UIImage *)aptitudeImage userId:(NSString *)userId progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"doctor/v1/doctor/%@/authentication", userId];
    [HZHttpsTool Upload:kGatewayUrlStr(urlStr) headDomainValue:nil uploadImage:aptitudeImage parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress);
        }
    } success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}


//+(void)setUpJPUSHalisAndTagsWithUser:(id)userInfo{
//
//    NSString *hospitalID = userInfo[@"results"][@"user"][@"hospitalId"];
//    NSString *departName = userInfo[@"results"][@"user"][@"departmentGeneralName"];
//    NSString *userID = userInfo[@"results"][@"user"][@"openid"];
//    //空值校验-----
//    if (!departName && !hospitalID ) return;
//    NSArray *arr = @[hospitalID,departName];
//    NSSet *tagsSet = [NSSet setWithArray:arr];
//
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        jpushSetCode = [NSMutableDictionary dictionaryWithDictionary:@{@"tag":@(-1),@"alia":@(-1)}];
//    });
//
//
//    // 防止,反反复复 调用
//    NSNumber *tag = jpushSetCode[@"tag"];
//    NSNumber *alia = jpushSetCode[@"alia"];
//    if (tag.intValue != 0) {
//        [JPUSHService addTags:tagsSet completion:^(NSInteger iResCode, NSSet *iTags, NSInteger seq) {
//            NSLog(@"绑定标签----添加不是覆盖--状态:%zd",iResCode);
//            [jpushSetCode setValue:@(iResCode) forKey:@"tag"];
//        } seq:111111];
//    }
//
//    if (alia.intValue != 0) {
//        //设置别名
//        [JPUSHService setAlias:userID completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
//            NSLog(@"绑定别名-----状态:%zd",iResCode);
//            [jpushSetCode setValue:@(iResCode) forKey:@"alia"];
//        } seq:222222];
//    }
//
//
//}


+ (void)getUserDataButtedVersionWithHospitalId:(NSString *)hospitalId success:(void(^_Nonnull)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v2/systems/versions/hospitals/%@", hospitalId];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}
@end
