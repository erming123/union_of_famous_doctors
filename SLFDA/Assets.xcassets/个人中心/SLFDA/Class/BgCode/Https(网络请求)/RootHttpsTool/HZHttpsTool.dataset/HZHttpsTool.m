//
//  HZHttpsTool.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/10/28.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZHttpsTool.h"
#import "AFNetworking.h"
#import "HZAccountTool.h"
#import "HZAccount.h"

#import "EMSDWebImageManager.h"
//
//#import "HZErrorHelper.h"
//#import "HZUser.h"
//static UILabel *alertLabel;
//static NSError *getError;
//static NSError *postError;
//static BOOL isOffLineNet;
@implementation HZHttpsTool

#pragma mark -- GET
+ (void)GET:(NSString *)URLString  headDomainValue:(NSString *)headDomainValue parameters:(id)parameters progress:(void (^)(NSProgress * _Nonnull downloadProgress))progress
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [self getCurrentNetStatus];
    
//    if (isOffLineNet) {
//        return;
//    }
    // 防止崩溃
    URLString = [URLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"`#%^{}\"[]|\\<> "].invertedSet];
    
    // 1.获得请求管理者
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    //    mgr.securityPolicy.validatesDomainName = NO;
    // 2.
    //    NSString *str = [NSString stringWithFormat:@"%@---", kAccess_token]; // 过测使用的
    //    if ([NSString isBlankString:headDomainValue]) headDomainValue = [NSString stringWithFormat:@"Bearer %@",str];
    if ([NSString isBlankString:headDomainValue]) headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
    NSLog(@"-----RealkAccess_token----%@", kAccess_token);
    
    NSLog(@"---headDomainValue------%@", headDomainValue);
    // 2.
    if ([URLString containsString:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/photo/openid?openid=")] || [URLString containsString:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/photo/get")] || [URLString containsString:@"fileName="] || [URLString containsString:@"pay/qrImageString"]) {
        [self setAvatarHttpsToolWithManager:mgr HeadDomainValue:headDomainValue parameters:parameters];
    } else {
        
        [self setHttpsToolWithManager:mgr HeadDomainValue:headDomainValue parameters:parameters];
    }
    
    
    //--------
    //3.
    [mgr GET:URLString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        if (progress) {
            progress(downloadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        responseObject = [NSDictionary changeType:responseObject];
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            
            failure(task, error);
            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            NSInteger statusCode = [response statusCode];
            
            
            BOOL getUserInfo =  [task.currentRequest.URL.absoluteString containsString:@"cfdu/v1/openapi/json/doctor/photo/get"];
            NSLog(@"--get-----statusCode:%ld:-------urlString:%@", statusCode, URLString);
            if (statusCode == 0 && !getUserInfo) {
                [MBProgressHUD showOnlyTextToView:nil title:@"服务器或网络异常"];
                [HZHttpsTool hideTheActiveLoading];
                return;
            }

//            
//            if (![error isEqual:getError] && error.code != - 1009 && error.code != -1004) {
//                //
//                [[HZErrorHelper shareInstance].errorArr addObject:error];
//                //
//                getError = error;
//            }
//            if (isOffLineNet) {
//                return;
//            }
            //         根据过期access_token的refresh_token 请求获取 新的access_token，并存储
            if (statusCode == 401) {// access_token 过期
                
                NSLog(@"-----GET-access_token---过期---:%@", URLString);
                NSString *refresh_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"refresh_token"];
                NSLog(@"-----GET-access_token过期-----refresh_token---:%@", refresh_token);
                //
                [HZAccountTool getNewAccessTokenWithRefreshToken:refresh_token success:^(id responseObject) {

                    NSLog(@"--re--responseObject-----%@", responseObject);
                    
                    NSString *access_token = responseObject[@"access_token"];
                    NSString *refresh_token = responseObject[@"refresh_token"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:access_token forKey:@"access_token"];
                    [[NSUserDefaults standardUserDefaults] setObject:refresh_token forKey:@"refresh_token"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSLog(@"---------URLString:%@----headDomainValue:%@------parameters:%@", URLString, headDomainValue, parameters);
                    //
                    [self GET:URLString headDomainValue:headDomainValue parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
                        if (progress) {
                            progress(downloadProgress);
                        }
                    } success:^(id responseObject) {
                        
                        if (success) {
                            success(responseObject);
                        }
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        if (failure) {
                            failure(task,error);
                        }
                    }];
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                    if (failure) {
                        failure(task, error);
                    }
                    
                }];
                
            } else if (statusCode == 0) {
                
                NSLog(@"---GET报错-----URLString:%@--------headDomainValue:%@------parameters:%@", URLString, headDomainValue, parameters);
                [self GET:URLString headDomainValue:headDomainValue parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
                    if (progress) {
                        progress(downloadProgress);
                    }
                } success:^(id responseObject) {
                    NSLog(@"-----GET再刷成功----%@------", responseObject);
                    if (success) {
                        success(responseObject);
                    }
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"-----GET再刷失败----%@------", error);
                    NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
                    NSInteger statusCode = [response statusCode];
                    
                    
                    
                    if (statusCode == 0) {
                        
                        NSString *errorCodeStr = [NSString stringWithFormat:@"网络连接失败:%ld", statusCode];
                        //                        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:errorCodeStr preferredStyle:UIAlertControllerStyleAlert];
                        //                        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
                        //                        if (kCurrentSystemVersion >= 9.0) {
                        //                            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                        //                        }
                        //                        [alertCtr addAction:ensureAlertAction];
                        //                        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
//                        [MBProgressHUD showOnlyTextToView:nil title:errorCodeStr];
                        [[UIApplication sharedApplication].keyWindow makeToast:errorCodeStr duration:1.5f position:CSToastPositionCenter];
                        if (failure) {
                            failure(task, error);
                        }
                        
                        
                        //                        NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
                        //
                        //                        HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                        //
                        //                        NSLog(@"---user.userId123----%@", user.userId);
                    }
                    
                    
                    
                }];
                
                
            } else if (statusCode == 404) {
                
                if (failure) {
                    failure(task, error);
                }
                
            } else {
                
                
                NSLog(@"----报错接口总-----URLString:%@-----error:%@", URLString, error);
                failure(task, error);
                if ([URLString containsString:@"cfdu/v1/app/function/state?function_name"] || [URLString containsString:@"https://gateway.rubikstack.com/user/certificate/"]) return;
               
                NSLog(@"----报错接口内-----URLString:%@-----error:%@", URLString, error);
                NSString *errorCodeStr = [NSString stringWithFormat:@"网络连接失败:%ld", statusCode];
                //                UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:errorCodeStr preferredStyle:UIAlertControllerStyleAlert];
                //                UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
                //                if (kCurrentSystemVersion >= 9.0) {
                //                    [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                //                }
                //                [alertCtr addAction:ensureAlertAction];
                //                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
                
                [MBProgressHUD showOnlyTextToView:nil title:errorCodeStr];
                
                if (failure) {
                    failure(task, error);
                }
                //                NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
                //
                //                HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                //
                //                NSLog(@"---user.userId123----%@", user.userId);
                
            }
        }
    }];
}


#pragma mark -- POST
// ---------------------获取token-------
+ (void)POST:(NSString *)URLString headDomainValue:(NSString *)headDomainValue parameters:(id)parameters progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [self getCurrentNetStatus];
    
//    if (isOffLineNet) {
//        return;
//    }
    // 1.获得请求管理者
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    //    mgr.securityPolicy.validatesDomainName = NO;
    //    // 2.
    if ([NSString isBlankString:headDomainValue]) headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
    NSLog(@"-----1233333333kAccess_token----%@", kAccess_token);
    [self setHttpsToolWithManager:mgr HeadDomainValue:headDomainValue parameters:parameters];
    
    
    // 3.发送POST请求
    [mgr POST:URLString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        responseObject = [NSDictionary changeType:responseObject];
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"-------GETError:%@", error);
        
        if (failure) {
            failure(task, error);
        }
        
        //
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSInteger statusCode = [response statusCode];
//        
//        if (![error isEqual:postError] && error.code != - 1009 && error.code != -1004) {
//            //
//            [[HZErrorHelper shareInstance].errorArr addObject:error];
//            //
//            postError = error;
//        }
        
        
        //         根据过期access_token的refresh_token 请求获取 新的access_token，并存储
        
//        if (isOffLineNet) {
//            return;
//        }
        NSLog(@"--post-----statusCode:%ld:-------urlString:%@", statusCode, URLString);
        if (statusCode == 0) {
           [MBProgressHUD showOnlyTextToView:nil title:@"服务器或网络异常"];
            [HZHttpsTool hideTheActiveLoading];
            return;
        }
        if (statusCode == 401) {// access_token 过期
            
            NSLog(@"----POST--access_token---过期---URLString:%@", URLString);
            NSLog(@"----POST--access_token---过期---Access_token:%@", kAccess_token);
            
            
            if ([URLString containsString:@"oauth/token?grant_type=refresh_token&refresh_token="]) {// refresh_token过期
                if (failure) {
                    failure(task, error);
                }
                
            } else {//  access_token 过期
                //                NSString *refresh_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"refresh_token"];
                NSString *refresh_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"refresh_token"];
                //                 refresh_token = [NSString stringWithFormat:@"%@---", refresh_token]; // 过测使用的
                [HZAccountTool getNewAccessTokenWithRefreshToken:refresh_token success:^(id responseObject) {
                    
                    
                    NSLog(@"--re--responseObject-----%@", responseObject);
                    //
                    NSString *access_token = responseObject[@"access_token"];
                    NSString *refresh_token = responseObject[@"refresh_token"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:access_token forKey:@"access_token"];
                    [[NSUserDefaults standardUserDefaults] setObject:refresh_token forKey:@"refresh_token"];
                    
                    NSLog(@"---------URLString:%@----headDomainValue:%@------parameters:%@", URLString, headDomainValue, parameters);
                    //
                    [self POST:URLString headDomainValue:headDomainValue parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
                        if (progress) {
                            progress(uploadProgress);
                        }
                    } success:^(id responseObject) {
                        if (success) {
                            success(responseObject);
                        }
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        if (failure) {
                            failure(task,error);
                        }
                    }];
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                    if (failure) {
                        failure(task, error);
                    }
                    
                    
                }];
                
            }
            
        } else if (statusCode == 0){
            
            NSLog(@"---POST报错-----URLString:%@--------headDomainValue:%@------parameters:%@", URLString, headDomainValue, parameters);
            
            
            
            [self POST:URLString headDomainValue:headDomainValue parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
                if (progress) {
                    progress(uploadProgress);
                }
            } success:^(id responseObject) {
                NSLog(@"-----POST再刷成功----%@------", responseObject);
                if (success) {
                    success(responseObject);
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"-----POST再刷失败----%@------", error);
                NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
                NSInteger statusCode = [response statusCode];
                
                //                if (failure) {
                //                    failure(task, error);
                //                }
                
                if (statusCode == 0) {
                    NSString *errorCodeStr = [NSString stringWithFormat:@"网络连接失败:%ld", statusCode];
                    //                    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:errorCodeStr preferredStyle:UIAlertControllerStyleAlert];
                    //                    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
                    //                    if (kCurrentSystemVersion >= 9.0) {
                    //                        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                    //                    }
                    //                    [alertCtr addAction:ensureAlertAction];
                    //                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
//                    [MBProgressHUD showOnlyTextToView:nil title:errorCodeStr];
                    [[UIApplication sharedApplication].keyWindow makeToast:errorCodeStr duration:1.5f position:CSToastPositionCenter];
                }
                
            }];
            
            
            
        } else {
            NSLog(@"---POST报错-----URLString:%@-----error:%@", URLString, error);
            //            NSString *errorCodeStr = [NSString stringWithFormat:@"网络连接失败:%ld", statusCode];
            //            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:errorCodeStr preferredStyle:UIAlertControllerStyleAlert];
            //            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
            //            if (kCurrentSystemVersion >= 9.0) {
            //                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            //            }
            //            [alertCtr addAction:ensureAlertAction];
            //            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
            
            
            
        }
        
        
    }];
    
}


+ (void)Delete:(NSString *)URLString headDomainValue:(NSString *)headDomainValue parameters:(id)parameters progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress
       success:(void (^)(id responseObject))success
       failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [self getCurrentNetStatus];
    
//    if (isOffLineNet) {
//        return;
//    }
    // 1.获得请求管理者
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    //    mgr.securityPolicy.validatesDomainName = NO;
    //    // 2.
    if ([NSString isBlankString:headDomainValue]) headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
    NSLog(@"-----1233333333kAccess_token----%@", kAccess_token);
    [self setHttpsToolWithManager:mgr HeadDomainValue:headDomainValue parameters:parameters];
    
    //
    [mgr DELETE:URLString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        responseObject = [NSDictionary changeType:responseObject];
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"-------GETError:%@", error);
        
        
        //
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSInteger statusCode = [response statusCode];
        //
        //        if (![error isEqual:postError] && error.code != - 1009 && error.code != -1004) {
        //            //
        //            [[HZErrorHelper shareInstance].errorArr addObject:error];
        //            //
        //            postError = error;
        //        }
        
//        if (isOffLineNet) {
//            return;
//        }
        NSLog(@"--Delete-----statusCode:%ld-----------------:urlString----------:%@", statusCode, URLString);
        
        if (statusCode == 0) {
            [MBProgressHUD showOnlyTextToView:nil title:@"服务器或网络异常"];
            [HZHttpsTool hideTheActiveLoading];
            return;
        }
        //         根据过期access_token的refresh_token 请求获取 新的access_token，并存储
        
        if (statusCode == 401) {// access_token 过期
            
            NSLog(@"----POST--access_token---过期---URLString:%@", URLString);
            NSLog(@"----POST--access_token---过期---Access_token:%@", kAccess_token);
            
            
            if ([URLString containsString:@"oauth/token?grant_type=refresh_token&refresh_token="]) {// refresh_token过期
                if (failure) {
                    failure(task, error);
                }
                
            } else {//  access_token 过期
                //                NSString *refresh_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"refresh_token"];
                NSString *refresh_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"refresh_token"];
                //                 refresh_token = [NSString stringWithFormat:@"%@---", refresh_token]; // 过测使用的
                [HZAccountTool getNewAccessTokenWithRefreshToken:refresh_token success:^(id responseObject) {
                    
                    
                    NSLog(@"--re--responseObject-----%@", responseObject);
                    //
                    NSString *access_token = responseObject[@"access_token"];
                    NSString *refresh_token = responseObject[@"refresh_token"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:access_token forKey:@"access_token"];
                    [[NSUserDefaults standardUserDefaults] setObject:refresh_token forKey:@"refresh_token"];
                    
                    NSLog(@"---------URLString:%@----headDomainValue:%@------parameters:%@", URLString, headDomainValue, parameters);
                    //
                    [self Delete:URLString headDomainValue:headDomainValue parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
                        if (progress) {
                            progress(uploadProgress);
                        }
                    } success:^(id responseObject) {
                        if (success) {
                            success(responseObject);
                        }
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        if (failure) {
                            failure(task,error);
                        }
                    }];
                    
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                    if (failure) {
                        failure(task, error);
                    }
                    
                    
                }];
                
            }
            
        } else if (statusCode == 0){
            
            NSLog(@"---POST报错-----URLString:%@--------headDomainValue:%@------parameters:%@", URLString, headDomainValue, parameters);
            
            
            
            [self Delete:URLString headDomainValue:headDomainValue parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
                if (progress) {
                    progress(uploadProgress);
                }
            } success:^(id responseObject) {
                NSLog(@"-----POST再刷成功----%@------", responseObject);
                if (success) {
                    success(responseObject);
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"-----POST再刷失败----%@------", error);
                NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
                NSInteger statusCode = [response statusCode];
                
                //                if (failure) {
                //                    failure(task, error);
                //                }
                
                if (statusCode == 0) {
                    NSString *errorCodeStr = [NSString stringWithFormat:@"网络连接失败:%ld", statusCode];
                    //                    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:errorCodeStr preferredStyle:UIAlertControllerStyleAlert];
                    //                    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
                    //                    if (kCurrentSystemVersion >= 9.0) {
                    //                        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                    //                    }
                    //                    [alertCtr addAction:ensureAlertAction];
                    //                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
//                    [MBProgressHUD showOnlyTextToView:nil title:errorCodeStr];
                    
                    [[UIApplication sharedApplication].keyWindow makeToast:errorCodeStr duration:1.5f position:CSToastPositionCenter];
                }
                
            }];
            
            
            
        } else {
            NSLog(@"---POST报错-----URLString:%@-----error:%@", URLString, error);
            //            NSString *errorCodeStr = [NSString stringWithFormat:@"网络连接失败:%ld", statusCode];
            //            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:errorCodeStr preferredStyle:UIAlertControllerStyleAlert];
            //            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
            //            if (kCurrentSystemVersion >= 9.0) {
            //                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            //            }
            //            [alertCtr addAction:ensureAlertAction];
            //            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
            
            if (failure) {
                failure(task, error);
            }
            
        }
        
        
    }];

    
       
}

#pragma mark -- Upload
+ (void)Upload:(NSString *)URLString headDomainValue:(NSString *)headDomainValue uploadImage:(UIImage *)image parameters:(id)parameters progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress
       success:(void (^)(id responseObject))success
       failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    [self getCurrentNetStatus];
    
//    if (isOffLineNet) {
//        return;
//    }
    // 1.获得请求管理者
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    //    mgr.securityPolicy.validatesDomainName = NO;
    //    // 2.
    if ([NSString isBlankString:headDomainValue]) headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
    NSLog(@"-----1233333333kAccess_token----%@", kAccess_token);
    [self setHttpsToolWithManager:mgr HeadDomainValue:headDomainValue parameters:parameters];
    
    [mgr POST:URLString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *data = UIImagePNGRepresentation(image);
        
        
        // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
        // 要解决此问题，
        // 可以在上传时使用当前的系统事件作为文件名
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        // 设置时间格式
        formatter.dateFormat = @"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
        
        //上传
        /*
         此方法参数
         1. 要上传的[二进制数据]
         2. 对应网站上[upload.php中]处理文件的[字段"file"]
         3. 要保存在服务器上的[文件名]
         4. 上传文件的[mimeType]
         */
        [formData appendPartWithFileData:data name:@"file" fileName:fileName mimeType:@"image/png"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
        if (progress) {
            progress(uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
            
            //
//            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            //            NSInteger statusCode = [response statusCode];
            
//            if (![error isEqual:postError] && error.code != - 1009 && error.code != -1004) {
//                //
//                [[HZErrorHelper shareInstance].errorArr addObject:error];
//                //
//                postError = error;
//            }
            
        }
    }];
}


+ (void)setHttpsToolWithManager:(AFHTTPSessionManager *)mgr HeadDomainValue:(NSString *)headDomainValue parameters:(id)parameters {
    
    
    // 2.申明返回的结果是text/html类型
    mgr.requestSerializer = [AFJSONRequestSerializer serializer];// 先请求，在返回
    AFJSONResponseSerializer *response = [AFJSONResponseSerializer serializer];
    mgr.responseSerializer= response;
    //    response.removesKeysWithNullValues = YES;
    //
    mgr.requestSerializer.timeoutInterval = 10;
    
    //-------- 注意： head域名不能加冒号
    [mgr.requestSerializer setValue:headDomainValue forHTTPHeaderField:@"Authorization"];
    [mgr.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //--------
    
}


+ (void)setAvatarHttpsToolWithManager:(AFHTTPSessionManager *)mgr HeadDomainValue:(NSString *)headDomainValue parameters:(id)parameters {
    
    // 2.申明返回的结果是text/html类型
    mgr.requestSerializer = [AFHTTPRequestSerializer serializer];// 先请求，在返回
    mgr.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFJSONResponseSerializer *response = [AFJSONResponseSerializer serializer];
    response.removesKeysWithNullValues = YES;
    //
    mgr.requestSerializer.timeoutInterval = 10;
    //    [mgr.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    //    mgr.requestSerializer.timeoutInterval = 10;
    //    [mgr.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    
    //-------- 注意： head域名不能加冒号
    [mgr.requestSerializer setValue:headDomainValue forHTTPHeaderField:@"Authorization"];
    //    [mgr.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //--------
    mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/plain", @"text/json",  nil];
    
}



+ (void)getCurrentNetStatus {
    
//    if (alertLabel == nil) {
//        NSLog(@"-----alertLabel---");
//        alertLabel = [UILabel new];
//        alertLabel.textAlignment = NSTextAlignmentCenter;
//        alertLabel.font = [UIFont systemFontOfSize:kP(26)];
//        alertLabel.textColor = [UIColor lightGrayColor];
//        alertLabel.text = @"请您检查网络连接状态";
//        [alertLabel sizeToFit];
//        alertLabel.width += kP(20);
//        alertLabel.height += kP(20);
//        alertLabel.x = HZScreenW * 0.5 - alertLabel.width * 0.5;
//        alertLabel.y = HZScreenH * 0.5 - alertLabel.height * 0.5;
//        alertLabel.layer.cornerRadius = kP(5);
//        alertLabel.clipsToBounds = YES;
//        alertLabel.backgroundColor = [UIColor blackColor];
//        alertLabel.alpha = 0.5;
//        alertLabel.hidden = YES;
//
//    }
//
//    if (alertLabel) {
//        [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:alertLabel];
//    }
//
    //1.创建网络状态监测管理者
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    
    //2.监听改变
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"2G,3G,4G...的网络");
//                isOffLineNet = NO;
                
                // 设置容联云在线状态
                // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIMOnLine"];
//                alertLabel.hidden = YES;
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"wifi的网络");
//                isOffLineNet = NO;
                // 设置容联云在线状态
                // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIMOnLine"];
//                alertLabel.hidden = YES;
                break;
            default:
//                alertLabel.hidden = NO;
                NSLog(@"没有网络");
//                isOffLineNet = YES;
//                [MBProgressHUD showOnlyTextToView:nil title:@"请您检查网络连接状态"];
                break;
        }
    }];
    
    [manager startMonitoring];
}
// 隐藏到处出现的菊花
+ (void)hideTheActiveLoading {
    
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    if (!window) {
        return;
    }
    UIView *tempView;
    for (UIView *subview in window.subviews) {
        if ([[subview.classForCoder description] isEqualToString:@"UILayoutContainerView"]) {
            tempView = subview;
            break;
        }
    }
    if (!tempView) {
        tempView = [window.subviews lastObject];
    }
    
    id nextResponder = [tempView nextResponder];
    while (![nextResponder isKindOfClass:[UIViewController class]] || [nextResponder isKindOfClass:[UINavigationController class]] || [nextResponder isKindOfClass:[UITabBarController class]]) {
        tempView =  [tempView.subviews firstObject];
        
        if (!tempView) {
            return;
        }
        nextResponder = [tempView nextResponder];
    }
    UIViewController *currentVC = (UIViewController*)nextResponder;
    for (UIView *view in currentVC.view.subviews) {
        if ([view isKindOfClass:[UIActivityIndicatorView class]]) {
            UIActivityIndicatorView *actV = (UIActivityIndicatorView*)view;
            [actV stopAnimating];
        }
    }
    
}
/**
 * 图片接口
 */
+ (void)GET:(NSString *_Nullable)URLString success:(void (^_Nullable)(id _Nullable responseObject,NSURLSessionDataTask * _Nonnull task))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure{
    
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    NSString *headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
    [self setAvatarHttpsToolWithManager:mgr HeadDomainValue:headDomainValue parameters:nil];
    [mgr GET:URLString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        responseObject = [NSDictionary changeType:responseObject];
        if (success) {
            success(responseObject,task);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task,error);
        }
        
    }];
    
}


@end
