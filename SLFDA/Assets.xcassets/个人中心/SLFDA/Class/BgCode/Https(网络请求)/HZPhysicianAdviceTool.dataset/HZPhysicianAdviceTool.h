//
//  HZPhysicanAdviceTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/12.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZPhysicianAdviceTool : NSObject
// 根据身份证号,是否当日, 医嘱类型(医疗/用药), 医嘱时长(长期/临时)获取医嘱数据
+ (void)getPhysicanAdviceDataWithHodiernalStr:(NSString *)hodiernalStr adviceTypeStr:(NSString *_Nullable)adviceTypeStr adviceCycleTimeStr:(NSString *_Nullable)adviceCycleTimeStr cardId:(NSString *_Nullable)cardId success:(void(^_Nonnull)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
// 根据身份证号获取医嘱数据
+ (void)getPhysicanAdviceDataWithCardId:(NSString *_Nullable)cardId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
