//
//  HZExpertSearchTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertSearchTool.h"
#import "HZHttpsTool.h"
@implementation HZExpertSearchTool
+ (void)getExpertArrWithDoctorName:(NSString *)expertName dictDepartmentId:(NSString *)dictDepartmentId isHotExpertSearch:(BOOL)isHotExpertSearch page:(NSInteger)page countPerPage:(NSInteger)countPerPage success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    expertName = [NSString isBlankString:expertName] ?  @"" : expertName;
    dictDepartmentId = [NSString isBlankString:dictDepartmentId] ? @"" : dictDepartmentId;
    
    NSNumber *pageNumber = [NSNumber numberWithInteger:page];
    NSNumber *countPerPageNumber = [NSNumber numberWithInteger:countPerPage];
    
    NSMutableDictionary *doctorParamDict;
    if (isHotExpertSearch) {
        doctorParamDict = [NSMutableDictionary dictionaryWithDictionary:@{@"page": pageNumber,
                                                                          @"size" : countPerPageNumber,
                                                                          @"doctorName_should_match" : expertName,
                                                                          @"doctorName.raw_should_match" : expertName,
                                                                          @"departmentGeneralId_must_match": dictDepartmentId,
                                                                          //@"departmentGeneralId.raw_should_match": dictDepartmentId,
                                                                          @"authRemoteExpert_must_match": @"001",
                                                                          @"hot_should_match" : @true}];
    } else {
        doctorParamDict = [NSMutableDictionary dictionaryWithDictionary:@{@"page": pageNumber,
                                                                          @"size" : countPerPageNumber,
                                                                          @"doctorName_should_match" : expertName,
                                                                          @"doctorName.raw_should_match" : expertName,
                                                                          @"departmentGeneralId_must_match": dictDepartmentId,
                                                                          //@"departmentGeneralId.raw_should_match": dictDepartmentId,
                                                                          @"authRemoteExpert_should_match": @"001"}];
    }


    if ([NSString isBlankString:expertName]) {
        [doctorParamDict removeObjectForKey:@"doctorName_should_match"];
        [doctorParamDict removeObjectForKey:@"doctorName.raw_should_match"];
    }

    // cfdu/v1/openapi/json/doctor/esearch  || cfdu/es/user_doctor/accurate_search
    NSLog(@"-----------doctorParamDict--------------:%@", [doctorParamDict getDictJsonStr]);
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/es/user_doctor/accurate_search") headDomainValue:nil parameters:doctorParamDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}


+ (void)getPriceListSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZHttpsTool GET:kGatewayUrlStr(@"cfdu/v1/openapi/json/proportion/getPriceList") headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}
@end
