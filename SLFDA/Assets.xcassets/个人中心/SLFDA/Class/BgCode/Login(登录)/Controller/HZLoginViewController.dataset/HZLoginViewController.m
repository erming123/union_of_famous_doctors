//
//  NSLoginViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZLoginViewController.h"
#import "AFNetWorking.h"
//#import "HZHomeViewController.h"
#import "HZTabBarController.h"
#import "HZTimerTool.h"

#import "HZActivityIndicatorView.h"

//
#import "HZAccountTool.h"
#import "HZAccount.h"
#import "HZHttpsTool.h"

#import "HZUser.h"
#import "HZUserTool.h"

//
#import "HZCompleteInformationViewController.h"
//
//#import "HZAlertView.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZLoginViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UIView *firstHLine;
@property (nonatomic, strong) UILabel *verityLabel;
@property (nonatomic, strong) UIView *secondHLine;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, assign) NSInteger timeCount;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) HZActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) HZActivityIndicatorView *phoneVerityIndicatorView;
@property (nonatomic, copy) NSString *previousTextFieldContent;
@property (nonatomic, strong) UITextRange *previousSelection;

@property (nonatomic, strong) HZTableView *loginTableView;
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *verityTextField;
@property (nonatomic, strong) UIButton *verityCodeBtn;
@property (nonatomic, strong) UIButton *loginBtn;
@property (nonatomic, strong) UIButton *enterRegistBtn;
@end
NS_ASSUME_NONNULL_END
@implementation HZLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"---------888888888888888888&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&$$$$$$$$$$$$$$$$$**************************");
    
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.navigationItem.title = @"医生登录";
    
    
    //
    //    [NSUserDefaults standardUserDefaults];
    
    
    //
    [self setUpSubViews];
    
    
    //
    HZActivityIndicatorView *activityIndicatorView = [[HZActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, kP(240), kP(240))];
    activityIndicatorView.center = self.view.center;
    activityIndicatorView.activityIndicatorText = @"请稍候...";
    [self.view addSubview:activityIndicatorView];
    self.activityIndicatorView = activityIndicatorView;
    
    
    HZActivityIndicatorView *phoneVerityIndicatorView = [[HZActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, kP(240), kP(240))];
    phoneVerityIndicatorView.center = self.view.center;
    phoneVerityIndicatorView.activityIndicatorText = @"正在验证...";
    [self.view addSubview:phoneVerityIndicatorView];
    self.phoneVerityIndicatorView = phoneVerityIndicatorView;
}

#pragma mark -- 添加子视图
- (void)setUpSubViews {
    
    //1.tableView
    HZTableView *loginTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    loginTableView.dataSource = self;
    loginTableView.delegate = self;
    loginTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    loginTableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    [self.view addSubview:loginTableView];
    self.loginTableView = loginTableView;
    
    //2.手机号输入框
    UITextField *phoneTextField = [[UITextField alloc] init];
    phoneTextField.placeholder = @"请输入手机号";
    phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
    phoneTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    phoneTextField.font = [UIFont systemFontOfSize:kP(36)];
    phoneTextField.delegate = self;
//    [phoneTextField becomeFirstResponder];
//
//     BOOL is =  [[NSUserDefaults standardUserDefaults] boolForKey:@"showKeyBoard"];
    
//    NSLog(@"---------BOOL------%@", is == YES ? @"YES":@"NO");
    [phoneTextField addTarget:self action:@selector(reformatphoneTextFieldContent:) forControlEvents:UIControlEventEditingChanged];
    self.phoneTextField = phoneTextField;
    
    //3. 验证码输入框
    UITextField *verityTextField = [[UITextField alloc] init];
    verityTextField.placeholder = @"请输入验证码";
    //    verityTextField.backgroundColor = [UIColor redColor]
    verityTextField.keyboardType = UIKeyboardTypeNumberPad;
    verityTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    verityTextField.font = [UIFont systemFontOfSize:kP(36)];
    verityTextField.delegate = self;
    [verityTextField addTarget:self action:@selector(checkVerityTextFieldContent:) forControlEvents:UIControlEventEditingChanged];
    self.verityTextField = verityTextField;
    
    //4. 验证码按钮
    UIButton *verityCodeBtn = [[UIButton alloc] init];
    [verityCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    verityCodeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
    //    [verityCodeBtn setTitleColor:[UIColor colorWithHexString:@"21b8c6" alpha:1.0] forState:UIControlStateNormal];
    [verityCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    verityCodeBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    verityCodeBtn.layer.cornerRadius = kP(16);
    verityCodeBtn.clipsToBounds = YES;
    
    verityCodeBtn.alpha = 0.5;
    verityCodeBtn.userInteractionEnabled = NO;
    [verityCodeBtn addTarget:self action:@selector(verityCodeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.verityCodeBtn = verityCodeBtn;
    
    //5.登陆按钮
    UIButton *loginBtn = [[UIButton alloc] init];
    loginBtn.x = kP(28);
    loginBtn.height = kP(106);
    loginBtn.y = kP(350);
    loginBtn.width = self.view.width - 2 * loginBtn.x;
    [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    loginBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    loginBtn.layer.cornerRadius = kP(16);
    loginBtn.clipsToBounds = YES;
    loginBtn.alpha = 0.5;
    loginBtn.userInteractionEnabled = NO;
    [loginBtn addTarget:self action:@selector(loginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [loginTableView addSubview:loginBtn];
    self.loginBtn = loginBtn;
    
    //6.去注册按钮
    UIButton *enterRegistBtn = [[UIButton alloc] init];
    enterRegistBtn.x = kP(260);
    enterRegistBtn.height = kP(50);
    enterRegistBtn.y = CGRectGetMaxY(loginBtn.frame) + kP(28);
    enterRegistBtn.width = self.view.width - 2 * enterRegistBtn.x;
    [enterRegistBtn setTitle:@"新用户注册" forState:UIControlStateNormal];
    enterRegistBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [enterRegistBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];//
    [enterRegistBtn addTarget:self action:@selector(enterRegistBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [loginTableView addSubview:enterRegistBtn];
    self.enterRegistBtn = enterRegistBtn;
    
    
    //7.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endViewEditing:)];
    [self.view addGestureRecognizer:tap];
}


#pragma mark -- 进入AppStore更新下载
- (void)loadAppStoreController {
    NSString *appStoreUrl = @"https://itunes.apple.com/us/app/ming-yi-lian-meng/id1173508999?l=zh&ls=1&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreUrl] options:@{} completionHandler:nil];
}


#pragma mark -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0) {
        
        //
        CGFloat phoneTextFieldX = kP(28);
        CGFloat phoneTextFieldY = kP(120) * 0.5 - kP(50) * 0.5;
        CGFloat phoneTextFieldW = HZScreenW - phoneTextFieldX - kP(50);
        CGFloat phoneTextFieldH = kP(50);
        self.phoneTextField.frame = CGRectMake(phoneTextFieldX, phoneTextFieldY, phoneTextFieldW, phoneTextFieldH);
        [cell.contentView addSubview:self.phoneTextField];
        
    } else {
        
        //
        CGFloat verityTextFieldW = kP(350);
        CGFloat verityTextFieldH = kP(50);
        CGFloat verityTextFieldX = kP(28);
        CGFloat verityTextFieldY = kP(120) * 0.5 - kP(50) * 0.5;
        self.verityTextField.frame = CGRectMake(verityTextFieldX, verityTextFieldY, verityTextFieldW, verityTextFieldH);
        [cell.contentView addSubview:self.verityTextField];
        
        //
        self.verityCodeBtn.width = kP(214);
        self.verityCodeBtn.height = kP(60);
        self.verityCodeBtn.x = HZScreenW - self.verityCodeBtn.width - kP(28);
        self.verityCodeBtn.y = kP(120) * 0.5 - self.verityCodeBtn.height * 0.5;
        [cell.contentView addSubview:self.verityCodeBtn];
    }
    
    return cell;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(120);
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(42);
    }
    return kP(2);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kP(0.1);
}


#pragma mark -- 褪去键盘
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (void)endViewEditing:(UITapGestureRecognizer *)tap {
    [UIView animateWithDuration:0.25f animations:^{
        [self.view endEditing:YES];
    }];
    
}
#pragma mark -- 获取验证码
- (void)verityCodeBtnClick:(UIButton *)verityCodeBtn {
    
    self.phoneVerityIndicatorView.hidden = NO;
    self.view.userInteractionEnabled = NO;
    if ([self getReCoverPhoneNumber].length != 11)  return;
    
//    [HZAccountTool getBasicTokenSuccess:nil failure:nil];
    
    [self performSelector:@selector(getVerifyCode) withObject:self afterDelay:0];
}

#pragma mark -- 获取手机号
- (NSString *)getReCoverPhoneNumber{
    return  [self.phoneTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}

#pragma mark -- 获取验证码
- (void)getVerifyCode {
    
    NSLog(@"---------sdddss----");
   
   
    [HZAccountTool getVerifyCodeWithUserPhoneNumber:[self getReCoverPhoneNumber] success:^(id responseObject) {
       
        self.phoneVerityIndicatorView.hidden = YES;
        self.view.userInteractionEnabled = YES;
        // 直接点击验证码输入框
        [self.verityTextField becomeFirstResponder];
        
        // 开始倒计时
        self.loginTableView.scrollEnabled = NO;
        //
        self.verityCodeBtn.enabled = NO;
        _timeCount = 60;
        [self.verityCodeBtn setTitle:@"获取验证码" forState:UIControlStateDisabled];
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        self.timer = timer;
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
       
        self.phoneVerityIndicatorView.hidden = YES;
        NSLog(@"--------获取验证码失败---%@", error);
        self.phoneVerityIndicatorView.hidden = YES;
        self.view.userInteractionEnabled = YES;
//        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"服务器或网络异常" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            self.phoneVerityIndicatorView.hidden = YES;
//            self.view.userInteractionEnabled = YES;
//        }];
//        if (kCurrentSystemVersion >= 9.0) {
//            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
//        }
//        [alertCtr addAction:ensureAlertAction];
//        [self presentViewController:alertCtr animated:YES completion:nil];
    }];
}

#pragma mark -- 倒计时
- (void)timerFired:(NSTimer *)timer {
    
    self.timeCount -= 1;
    if (self.timeCount >= 0) {
        self.verityTextField.enabled = YES;
        NSString *str = [NSString stringWithFormat:@"重新获取(%ld)", (long)self.timeCount];
        [self.verityCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
        [self.verityCodeBtn setTitle:str forState:UIControlStateDisabled];
        self.verityCodeBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        self.verityCodeBtn.backgroundColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0];
        if (self.timeCount == 0) {
            
            //
            self.loginTableView.scrollEnabled = YES;
            [self.verityCodeBtn setTitle:@"获取验证码" forState:UIControlStateDisabled];
            [self.verityCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
            self.verityCodeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
            self.verityCodeBtn.titleLabel.textAlignment = NSTextAlignmentRight;
            self.verityCodeBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
            self.phoneTextField.enabled = YES;
            self.verityCodeBtn.enabled = NO;
        }
        
        
    } else if (self.timeCount < 0) {
        //        self.timeCount = 0;
        //
        self.loginTableView.scrollEnabled = YES;
        //
        [timer invalidate];
        self.verityCodeBtn.enabled = YES;
        [self.verityCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.verityCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
        self.verityCodeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
        self.verityCodeBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        self.verityCodeBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
        self.phoneTextField.enabled = YES;
        //        self.getTestCodeBtn.enabled = NO;
    }
    
//    NSLog(@"5555555%ld", self.timeCount);
}

#pragma mark -- 停止倒计时
- (void)viewWillDisappear:(BOOL)animated {
    [self.timer invalidate];
    self.activityIndicatorView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self viewDidLoad];
}

#pragma mark -- 登录
- (void)loginBtnClick:(UIButton *)loginBtn{
    
    [self.phoneTextField resignFirstResponder];
    [self.verityTextField resignFirstResponder];
    
    self.activityIndicatorView.hidden = NO;
    loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
    loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
    
    self.view.userInteractionEnabled = NO;
    
    NSLog(@"----self.verityTextField.text-----%@", self.verityTextField.text);
    [HZAccountTool getBasicTokenSuccess:^(id responseObject) {
        //  验证 验证码
        [HZAccountTool justVerifyCodeWithPhoneNumber:[self getReCoverPhoneNumber] verifyCode:self.verityTextField.text success:^(id responseObject) {
            
            
            NSDictionary *resultsDict = responseObject[@"results"];
            if ([responseObject[@"state"] isEqual:@200] && [resultsDict[@"checkCode"] isEqual:@YES] && [resultsDict[@"checkUser"] isEqual:@YES]) {
                
                NSLog(@"----验证验证码成功-----%@", responseObject);
                
                // 获取access_token
                [HZAccountTool getLoginCodeWithUserPhoneNumber:[self getReCoverPhoneNumber] andLoginVerifyCode:self.verityTextField.text success:^(id responseObject) {// 获取access_token有回应
                    
                    
                    NSLog(@"------登录反馈123---%@", responseObject);
                    
                    NSString *access_token = responseObject[@"access_token"];
                    NSString *refresh_token = responseObject[@"refresh_token"];
                    //            NSString *expires_in = responseObject[@"expires_in"];
                    //            NSString *jti = responseObject[@"jti"];
                    
                    //
                    [[NSUserDefaults standardUserDefaults] setObject:self.verityTextField.text forKey:@"verityCode"];
                    
                    
                    //
                    //                    [[NSUserDefaults standardUserDefaults] setValue:@YES forKeyPath:@"isLogin"];
                    //------------------------------------------------------
                    
                    [[NSUserDefaults standardUserDefaults] setValue:access_token forKeyPath:@"access_token"];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setValue:refresh_token forKeyPath:@"refresh_token"];
                    [[NSUserDefaults standardUserDefaults] setValue:[self getReCoverPhoneNumber] forKey:@"phoneNumber"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSLog(@"-----kAccess_token对对对----%@", kAccess_token);
                    
                    if (kAccess_token) {
                        
                        [HZUserTool getUserWithSuccess:^(id responseObject) {
                            
                            NSLog(@"------登录获取用户信息---%@", responseObject);
                            if ([responseObject[@"state"] isEqual:@200]) {// 信息完善
                                
                                NSLog(@"----dddssssaaaaaa-----%@", responseObject);
                                
                                NSLog(@"获取用户信息成功");
                                self.activityIndicatorView.hidden = YES;
                                loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                
                                NSDictionary *dataDict = responseObject;
                                NSDictionary *resultDict = dataDict[@"results"];
                                NSDictionary *userDict = resultDict[@"user"];
                                HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                
                                //1.
                                [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                
//                                HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                homeVCtr.user = user;
//                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                
                                HZTabBarController *tabBarCtr = [HZTabBarController new];
//                                tabBarCtr.user = user;
                                [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                self.view.userInteractionEnabled = YES;
                                
                                
                            } else if ([responseObject[@"state"] isEqual:@4001]){ // 信息不完善
                                
                                HZCompleteInformationViewController *completeInformationVCtr = [HZCompleteInformationViewController new];
                                [self.navigationController pushViewController:completeInformationVCtr animated:YES];
                                
                                //
                                self.view.userInteractionEnabled = YES;
                                //                        //
                            } else {
                                
                                
                                
                                NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                                
                                if (!userDict) {
                                    
                                    
                                    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                                        loginBtn.userInteractionEnabled = YES;
                                        
//                                        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
//                                        [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                        self.phoneVerityIndicatorView.hidden = YES;
                                        // ------------- 去掉accesstoken，回到 ------------
                                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                        self.view.userInteractionEnabled = YES;
                                        
                                        //
                                        self.activityIndicatorView.hidden = YES;
                                        loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                        loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                        
                                    }];
                                    if (kCurrentSystemVersion >= 9.0) {
                                        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                                    }
                                    [alertCtr addAction:ensureAlertAction];
                                    [self presentViewController:alertCtr animated:YES completion:nil];
                                    return;
                                }
                                //
                                self.activityIndicatorView.hidden = YES;
                                loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                //
                                HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                //1
                                [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                
//                                HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                homeVCtr.user = user;
//                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                
                                HZTabBarController *tabBarCtr = [HZTabBarController new];
//                                tabBarCtr.user = user;
                                [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                self.view.userInteractionEnabled = YES;
                            }
                            
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            
                            
                            NSLog(@"----rrrrrrrrrrrr-----%@", error);
                            
                            
                            
                            NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                            
                            if (!userDict) {
                                
                                self.activityIndicatorView.hidden = YES;
                                loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    //
                                    self.activityIndicatorView.hidden = YES;
                                    loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                    loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                    
//                                    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
//                                    [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                    self.phoneVerityIndicatorView.hidden = YES;
                                    // ------------- 去掉accesstoken，回到 ------------
                                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    self.view.userInteractionEnabled = YES;
                                    
                                }];
                                if (kCurrentSystemVersion >= 9.0) {
                                    [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                                }
                                [alertCtr addAction:ensureAlertAction];
                                [self presentViewController:alertCtr animated:YES completion:nil];
                                return;
                            }
                            
                            //
                            self.activityIndicatorView.hidden = YES;
                            loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                            loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                            //
                            HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                            user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                            //1
                            [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                            
//                            HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                            homeVCtr.user = user;
//                            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                            HZTabBarController *tabBarCtr = [HZTabBarController new];
//                            tabBarCtr.user = user;
                            [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                            self.view.userInteractionEnabled = YES;
                        }];
                        
                        
                        
                    } else {
                        NSLog(@"---------没有access_token");
                    }
                    
                    
                    
                    
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {// 获取access_toke有问题
                    
                    self.activityIndicatorView.hidden = YES;
                    
                    
                    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"验证码输入错误" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        self.phoneVerityIndicatorView.hidden = YES;
                        loginBtn.userInteractionEnabled = YES;
                        loginBtn.userInteractionEnabled = YES;
                        loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                        self.view.userInteractionEnabled = YES;
                    }];
                    if (kCurrentSystemVersion >= 9.0) {
                        [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
                    }
                    [alertCtr addAction:ensureAlertAction];
                    [self presentViewController:alertCtr animated:YES completion:nil];
                    
                    //                }
                    self.activityIndicatorView.hidden = YES;
                }];
                
                
                
                
            } else if ([responseObject[@"state"] isEqual:@200] && [resultsDict[@"checkUser"] isEqual:@YES] && [resultsDict[@"checkCode"] isEqual:@NO]) {
                
                
                self.activityIndicatorView.hidden = YES;
                UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"验证码输入错误" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    loginBtn.userInteractionEnabled = YES;
                    loginBtn.userInteractionEnabled = YES;
                    loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                    
                    
                    //                self.phoneVerityIndicatorView.hidden = YES;
                    
                    self.activityIndicatorView.hidden = YES;
                    self.view.userInteractionEnabled = YES;
                }];
                if (kCurrentSystemVersion >= 9.0) {
                    [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                }
                [alertCtr addAction:ensureAlertAction];
                [self presentViewController:alertCtr animated:YES completion:nil];
            } else if ([responseObject[@"state"] isEqual:@200] && [resultsDict[@"checkUser"] isEqual:@NO]) { // 去注册
                NSLog(@"---------注册成用户12----");
                
                
                
                
                //注册成用户
                NSString *phoneNumberStr = [self getReCoverPhoneNumber];
                NSString *pwdStr = [phoneNumberStr substringWithRange:NSMakeRange(phoneNumberStr.length - 6, 6)];
                [HZAccountTool registAccountWithPhoneNumber:phoneNumberStr passWord:pwdStr type:@"phone" success:^(id responseObject) {
                    
                    //15618705866
                    NSLog(@"-----responseObject注册成功----%@", responseObject);
                    
                    if ([responseObject[@"state"] isEqual:@200]) {// 注册成功
                        
                        NSDictionary *resultDict = responseObject[@"results"];
                        NSLog(@"----userId-----%@", resultDict[@"userId"]);
                        
                        
                        
                        // 再次获取验证码
                        // 获取access_token
                        [HZAccountTool getLoginCodeWithUserPhoneNumber:[self getReCoverPhoneNumber] andLoginVerifyCode:self.verityTextField.text success:^(id responseObject) {// 获取access_token有回应
                            
                            
                            NSLog(@"------登录反馈123---%@", responseObject);
                            
                            NSString *access_token = responseObject[@"access_token"];
                            NSString *refresh_token = responseObject[@"refresh_token"];
                            //                        NSString *expires_in = responseObject[@"expires_in"];
                            //                        NSString *jti = responseObject[@"jti"];
                            
                            //
                            [[NSUserDefaults standardUserDefaults] setObject:self.verityTextField.text forKey:@"verityCode"];
                            self.activityIndicatorView.hidden = YES;
                            //
                            //
                            //                    [[NSUserDefaults standardUserDefaults] setValue:@YES forKeyPath:@"isLogin"];
                            
                            //------------------------------------------------------
                            [[NSUserDefaults standardUserDefaults] setValue:access_token forKeyPath:@"access_token"];
                            [[NSUserDefaults standardUserDefaults] setValue:refresh_token forKeyPath:@"refresh_token"];
                            [[NSUserDefaults standardUserDefaults] setValue:[self getReCoverPhoneNumber] forKey:@"phoneNumber"];
                            
                            NSLog(@"-----kAccess_token对对对----%@", kAccess_token);
                            
                            if (kAccess_token) {
                                
                                
                                
                                [HZUserTool getUserWithSuccess:^(id responseObject) {
                                    
                                    NSLog(@"-----user123456----%@", responseObject);
                                    if ([responseObject[@"state"] isEqual:@200]) {// 信息完善
                                        
                                        NSLog(@"----dddssssaaaaaa-----%@", responseObject);
                                        
                                        NSLog(@"获取用户信息成功");
                                        self.activityIndicatorView.hidden = YES;
                                        loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                        loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                        
                                        NSDictionary *dataDict = responseObject;
                                        NSDictionary *resultDict = dataDict[@"results"];
                                        NSDictionary *userDict = resultDict[@"user"];
                                        HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                        user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                        //1
                                        [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                        
//                                        HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                        homeVCtr.user = user;
//                                        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                        [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                        
                                        HZTabBarController *tabBarCtr = [HZTabBarController new];
//                                        tabBarCtr.user = user;
                                        [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                        self.view.userInteractionEnabled = YES;
                                        
                                    } else if ([responseObject[@"state"] isEqual:@4001]){ // 信息不完善
                                        
                                        
                                        
                                        HZCompleteInformationViewController *completeInformationVCtr = [HZCompleteInformationViewController new];
                                        [self.navigationController pushViewController:completeInformationVCtr animated:YES];
                                        self.view.userInteractionEnabled = YES;
                                        //                        //
                                    } else {
                                        
                                        
                                        NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                                        if (!userDict) {
                                            
                                            
                                            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                //
                                                self.activityIndicatorView.hidden = YES;
                                                loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                                loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                                
//                                                //
//                                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
//                                                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                                
                                                self.phoneVerityIndicatorView.hidden = YES;
                                                //------------- 去掉accesstoken，回到 ------------
                                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                self.view.userInteractionEnabled = YES;
                                                
                                                
                                                
                                            }];
                                            if (kCurrentSystemVersion >= 9.0) {
                                                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                                            }
                                            [alertCtr addAction:ensureAlertAction];
                                            [self presentViewController:alertCtr animated:YES completion:nil];
                                            return;
                                        }
                                        
                                        //
                                        self.activityIndicatorView.hidden = YES;
                                        loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                        loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                        
                                        HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                        user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                        //1
                                        [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                        
//                                        HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                        homeVCtr.user = user;
//                                        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                        [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                        
                                        HZTabBarController *tabBarCtr = [HZTabBarController new];
//                                        tabBarCtr.user = user;
                                        [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                        self.view.userInteractionEnabled = YES;
                                        
                                    }
                                    
                                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                    
                                    
                                    NSLog(@"----rrrrrrrrrrrr-----%@", error);
                                    
                                    
                                    
                                    
                                    NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                                    
                                    if (!userDict) {
                                        self.activityIndicatorView.hidden = YES;
                                        loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                        loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                        
                                        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                                        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                            //
                                            self.activityIndicatorView.hidden = YES;
                                            loginBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                            loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                            
//                                            //                                   //
//                                            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
//                                            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                            
                                            self.phoneVerityIndicatorView.hidden = YES;
                                            //------------- 去掉accesstoken，回到 ------------
                                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                            [[NSUserDefaults standardUserDefaults] synchronize];
                                            self.view.userInteractionEnabled = YES;
                                            
                                            
                                        }];
                                        
                                        
                                        
                                        if (kCurrentSystemVersion >= 9.0) {
                                            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                                        }
                                        [alertCtr addAction:ensureAlertAction];
                                        [self presentViewController:alertCtr animated:YES completion:nil];
                                        return;
                                    }
                                    //
                                    
                                    
                                    
                                }];
                                
                                
                                
                            } else {
                                NSLog(@"---------没有access_token");
                            }
                            
                            
                            
                            
                            
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {// 获取access_toke有问题
                            
                            self.activityIndicatorView.hidden = YES;
                            
                            
                            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"验证码输入错误" preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                
                                self.phoneVerityIndicatorView.hidden = YES;
                                loginBtn.userInteractionEnabled = YES;
                                loginBtn.userInteractionEnabled = YES;
                                loginBtn.alpha = loginBtn.userInteractionEnabled ? 1:0.5;
                                self.view.userInteractionEnabled = YES;
                                self.activityIndicatorView.hidden = YES;
                            }];
                            if (kCurrentSystemVersion >= 9.0) {
                                [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
                            }
                            [alertCtr addAction:ensureAlertAction];
                            [self presentViewController:alertCtr animated:YES completion:nil];
                            
                            //                }
                            
                        }];
                        
                        
                        
                    } else {// 注册失败
                        NSLog(@"---------注册失败");
                        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"注册成用户失败，请您从新获取验证码" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            self.phoneVerityIndicatorView.hidden = YES;
                        }];
                        if (kCurrentSystemVersion >= 9.0) {
                            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                        }
                        [alertCtr addAction:ensureAlertAction];
                        [self presentViewController:alertCtr animated:YES completion:nil];
                        
                        
                    }
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"--------responseObject注册失败---%@", error);
                }];
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"---验证验证码失败------%@", error);
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---验证basickToken失败------%@", error);
    }];



//    // 本地服务器登陆
//
}


#pragma mark -- enterRegistBtnClick
- (void)enterRegistBtnClick:(UIButton *)enterRegistBtn {
    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZRegistViewController") new]];
    [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
}


#pragma mark -- 设置手机号的格式
- (void)reformatphoneTextFieldContent:(UITextField *)phoneTextField {
    /**
     *  判断正确的光标位置
     */
    NSUInteger targetCursorPostion = [phoneTextField offsetFromPosition:phoneTextField.beginningOfDocument toPosition:phoneTextField.selectedTextRange.start];
    NSString *phoneNumberWithoutSpaces = [self removeNonDigits:phoneTextField.text andPreserveCursorPosition:&targetCursorPostion];
    
    
    if([phoneNumberWithoutSpaces length] > 11) {
        /**
         *  避免超过11位的输入
         */
        
        [phoneTextField setText:_previousTextFieldContent];
        phoneTextField.selectedTextRange = _previousSelection;
        
        return;
    }
    
    
    NSString *phoneNumberWithSpaces = [self insertSpacesEveryFourDigitsIntoString:phoneNumberWithoutSpaces andPreserveCursorPosition:&targetCursorPostion];
    
    phoneTextField.text = phoneNumberWithSpaces;
    UITextPosition *targetPostion = [phoneTextField positionFromPosition:phoneTextField.beginningOfDocument offset:targetCursorPostion];
    [phoneTextField setSelectedTextRange:[phoneTextField textRangeFromPosition:targetPostion toPosition:targetPostion]];
    
    
    
    // 设置login按钮的状态
    if (phoneTextField.text.length != 0 && self.verityTextField.text.length != 0) {
        self.loginBtn.alpha = 1;
        self.loginBtn.userInteractionEnabled = YES;
    } else {
        self.loginBtn.alpha = 0.5;
        self.loginBtn.userInteractionEnabled = NO;
    }
    
    if (phoneTextField.text.length == 13) {
        self.verityCodeBtn.alpha = 1;
        self.verityCodeBtn.userInteractionEnabled = YES;
    } else {
        self.verityCodeBtn.alpha = 0.5;
        self.verityCodeBtn.userInteractionEnabled = NO;
    }
    
    
    
}


- (NSString *)removeNonDigits:(NSString *)string andPreserveCursorPosition:(NSUInteger *)cursorPosition {
    NSUInteger originalCursorPosition =*cursorPosition;
    NSMutableString *digitsOnlyString = [NSMutableString new];
    
    for (NSUInteger i = 0; i < string.length; i++) {
        unichar characterToAdd = [string characterAtIndex:i];
        
        if(isdigit(characterToAdd)) {
            NSString *stringToAdd = [NSString stringWithCharacters:&characterToAdd length:1];
            [digitsOnlyString appendString:stringToAdd];
        }
        else {
            if(i<originalCursorPosition) {
                (*cursorPosition)--;
            }
        }
    }
    return digitsOnlyString;
}


- (NSString *)insertSpacesEveryFourDigitsIntoString:(NSString *)string andPreserveCursorPosition:(NSUInteger *)cursorPosition{
    NSMutableString *stringWithAddedSpaces = [NSMutableString new];
    NSUInteger cursorPositionInSpacelessString = *cursorPosition;
    
    for (NSUInteger i=0; i<string.length; i++) {
        if(i>0)
        {
            if(i==3 || i==7) {
                [stringWithAddedSpaces appendString:@" "];
                
                if(i<cursorPositionInSpacelessString) {
                    (*cursorPosition)++;
                }
            }
        }
        
        unichar characterToAdd = [string characterAtIndex:i];
        NSString *stringToAdd = [NSString stringWithCharacters:&characterToAdd length:1];
        [stringWithAddedSpaces appendString:stringToAdd];
    }
    return stringWithAddedSpaces;
}


#pragma mark --  检查密码输入框的内容

- (void)checkVerityTextFieldContent:(UITextField *)verityTextField {
    
    if (verityTextField.text.length != 0 && self.phoneTextField.text.length != 0) {
        self.loginBtn.alpha = 1;
        self.loginBtn.userInteractionEnabled = YES;
    } else {
        self.loginBtn.alpha = 0.5;
        self.loginBtn.userInteractionEnabled = NO;
    }
    
    if([verityTextField.text length] > 6) {
        /**
         *  避免超过11位的输入
         */
        
        [verityTextField setText:_previousTextFieldContent];
        verityTextField.selectedTextRange = _previousSelection;
        
        return;
    }
    
}


#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.phoneTextField) {
        _previousSelection = textField.selectedTextRange;
        _previousTextFieldContent = textField.text;
        
        if(range.location==0) {
            if(string.integerValue >1)
            {
                return NO;
            }
        }
        
        
    } else if (textField == self.verityTextField){
        
        if (range.length == 1 && string.length == 0) {
            return YES;
        }
        else if (self.verityTextField.text.length >= 6) {
            self.verityTextField.text = [textField.text substringToIndex:6];
            return NO;
        }
        
    }
    
    return YES;
    
}


#pragma mark -- refreshHomeSubViews

- (void)refreshHomeSubViews {
    [self viewDidLoad];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
