//
//  HZSearchBar.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZSearchBar.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZSearchBar ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextField *textField;
//
@property (nonatomic, strong) UIView *leftBgView;
@property (nonatomic, strong) UIImageView *leftImageView;
//
@property (nonatomic, strong) UIButton *cancelBtn;

@property (nonatomic, assign) CGFloat keyBoardH;
@end
NS_ASSUME_NONNULL_END
@implementation HZSearchBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}


- (void)setUpSubViews {
    
    //1.
    UITextField *textField = [[UITextField alloc] init];
    textField.delegate = self;
    [textField addTarget:self action:@selector(textChaged:) forControlEvents:UIControlEventEditingChanged];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.returnKeyType = UIReturnKeySearch;
    
    
    //
    UIView *leftBgView = [[UIView alloc] init];
    UIImageView *leftImageView = [[UIImageView alloc] init];
    leftImageView.image = [UIImage imageNamed:@"search.png"];
    [leftBgView addSubview:leftImageView];
    //
    textField.font = [UIFont systemFontOfSize:kP(28)];
    textField.leftView = leftBgView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    //    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.backgroundColor = [UIColor colorWithHexString:@"#030303" alpha:0.09];
    [textField becomeFirstResponder];
     [self addSubview:textField];
    self.leftBgView = leftBgView;
    self.leftImageView = leftImageView;
    self.textField = textField;
    
    
    //2.
    UIButton *cancelBtn = [[UIButton alloc] init];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(returnBack:) forControlEvents:UIControlEventTouchUpInside];
//    cancelBtn.backgroundColor = [UIColor redColor];
    
    //
    [self addSubview:cancelBtn];
    self.cancelBtn = cancelBtn;
    
    

//    //增加监听，当键盘出现或改变时收出消息
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
//    
//    //增加监听，当键退出时收出消息
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];


}


- (void)layoutSubviews {
    [super layoutSubviews];
 
    //1.
    CGFloat textFieldX = kP(0);
    CGFloat textFieldY = kP(0);
    CGFloat textFieldW = self.width -kP(80);
    CGFloat textFieldH = self.height;
    self.textField.frame = CGRectMake(textFieldX, textFieldY, textFieldW, textFieldH);
    self.textField.placeholder = self.placeholder;
    
    //2.1
    CGFloat leftBgViewX = kP(0);
    CGFloat leftBgViewY = kP(0);
    CGFloat leftBgViewW = kP(42);
    CGFloat leftBgViewH = textFieldH;
    self.leftBgView.frame = CGRectMake(leftBgViewX, leftBgViewY, leftBgViewW, leftBgViewH);
    
    
    //2.2
    CGFloat leftImageViewW = kP(26);
    CGFloat leftImageViewH = kP(28);
    CGFloat leftImageViewX = kP(16);
    CGFloat leftImageViewY = textFieldH * 0.5 - leftImageViewH * 0.5;
    self.leftImageView.frame = CGRectMake(leftImageViewX, leftImageViewY, leftImageViewW, leftImageViewH);
    
    
    //3.
    CGFloat cancelBtnX = CGRectGetMaxX(self.textField.frame) + kP(5);
    CGFloat cancelBtnW = self.width - cancelBtnX;
    CGFloat cancelBtnH = kP(50);
    CGFloat cancelBtnY = textFieldH * 0.5 - cancelBtnH * 0.5;
    self.cancelBtn.frame = CGRectMake(cancelBtnX, cancelBtnY, cancelBtnW, cancelBtnH);
    
    //
    //4.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification:) name:UIKeyboardWillShowNotification object:nil];
    
}

- (NSString *)textFieldText {
    return self.textField.text;
}

- (void)setIsBecomeFirstResponder:(BOOL)isBecomeFirstResponder {
    isBecomeFirstResponder ? [self.textField becomeFirstResponder] : [self.textField resignFirstResponder];
}

#pragma mark -- delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    
//    [textField resignFirstResponder];
//    if ([NSString isBlankString:textField.text]) return NO;
    if (self.delegate && [self.delegate respondsToSelector:@selector(getSearchResult:)]) {
        [self.delegate getSearchResult:self];
    }
    return YES;
}

- (void)returnBack:(UIButton *)cancelBtn {
    [self.textField resignFirstResponder];
  
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelBtnClick:)]) {
        [self.delegate cancelBtnClick:self];
    }
}

#pragma mark -- textChaged
- (void)textChaged:(UITextField *)textField {
    
    NSLog(@"----text--%@", textField.text);
    
    NSLog(@"-----fffdd----%@",textField.markedTextRange);
    if (textField.markedTextRange == nil) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(changeText:)]) {
            [self.delegate changeText:textField.text];
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"------self.keyBoardH---111-----%lf", self.keyBoardH);
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeTableViewHeight:)]) {
        [self.delegate changeTableViewHeight:self.keyBoardH];
    }
    return YES;
}


#pragma mark -- keyboardWillChangeFrameNotification
- (void)keyboardWillChangeFrameNotification:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyBoardH = keyboardRect.size.height;
    
    NSLog(@"------dds---%lf", self.keyBoardH);
}

// 点击return键
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ([textField.text isEqualToString:@"\n"]) {
        
       
        [textField resignFirstResponder];
    
        
        //
//        if (self.delegate && [self.delegate respondsToSelector:@selector(passTextViewStateClickReturn)]) {
//            [self.delegate passTextViewStateClickReturn];
//        }
        return NO;
    }
    
    return YES;
}

////当键盘出现或改变时调用
//- (void)keyboardWillShow:(NSNotification *)aNotification
//{
//    //获取键盘的高度
//    NSDictionary *userInfo = [aNotification userInfo];
//    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardRect = [aValue CGRectValue];
//    CGFloat height = keyboardRect.size.height;
//    
//    
//    NSLog(@"-----2323232323111117767767676----%lf", height);
//    
//    self.keyBoardH = height;
//}  malloc_history 1102 0x7f93896c7790 |grep 0x7f93896c7790
//
////当键退出时调用
//- (void)keyboardWillHide:(NSNotification *)aNotification
//{
//    
//}

- (void)dealloc {
//    self.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
