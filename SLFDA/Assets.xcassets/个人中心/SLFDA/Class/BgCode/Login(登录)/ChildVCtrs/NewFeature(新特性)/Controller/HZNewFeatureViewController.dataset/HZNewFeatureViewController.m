//
//  HZNewFeatureViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZNewFeatureViewController.h"
#import "HZAlertView.h"
@interface HZNewFeatureViewController ()<UIScrollViewDelegate, HZAlertViewDelegate>
//** <#注释#>*/
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, copy) NSMutableArray *btnArr;

//** <#注释#>*/
@property (nonatomic, strong) UIButton *shareBtn;

//** <#注释#>*/
@property (nonatomic, strong) UIButton *enterBtn;

//** <#注释#>*/
@property (nonatomic, strong) UIButton *registBtn;

//** <#注释#>*/
@property (nonatomic, strong) HZAlertView *alertView;
//** <#注释#>*/
@property (nonatomic, strong) UIView *cover;
@end

@implementation HZNewFeatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(kP(0), kP(100), kP(750), kP(900))];
//    scrollView.backgroundColor = [UIColor yellowColor];
    [scrollView setContentSize:CGSizeMake(kP(750) * 2, 0)];
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    
    
    //
    UIView *scrView1 = [[UIView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(750), kP(800))];
//    scrView1.backgroundColor = [UIColor cyanColor];
    [scrollView addSubview:scrView1];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(750), kP(548))];
//    imageView1.backgroundColor = [UIColor redColor];
    imageView1.image = [UIImage imageNamed:@"newFeature1.png"];
    [scrView1 addSubview:imageView1];
    
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.text = @"互联网门诊";
    [label1 setTextFont:kP(36) textColor:@"#2DBED8" alpha:1.0];
    [label1 sizeToFit];
    label1.x = self.view.width * 0.5 - label1.width * 0.5;
    label1.y = CGRectGetMaxY(imageView1.frame) + kP(115);
    label1.textAlignment = NSTextAlignmentCenter;
     [scrView1 addSubview:label1];
    
    UILabel *label12 = [[UILabel alloc] init];
    label12.text = @"手机上即可进行互联网门诊，随时随地为患者提供服务";
    [label12 setTextFont:kP(32) textColor:@"#999999" alpha:1.0];
    label12.numberOfLines = 0;
    label12.x = kP(108);
    label12.y = CGRectGetMaxY(imageView1.frame) + kP(85);
    label12.width = self.view.width - 2 * label12.x;
    label12.height = kP(300);
//    label12.backgroundColor = [UIColor blueColor];
    label12.textAlignment = NSTextAlignmentCenter;
    [scrView1 addSubview:label12];


    
    //
    UIView *scrView2 = [[UIView alloc] initWithFrame:CGRectMake(kP(750), kP(0), kP(750), kP(800))];
//    scrView2.backgroundColor = [UIColor purpleColor];
    [scrollView addSubview:scrView2];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(750), kP(548))];
    imageView2.image = [UIImage imageNamed:@"newFeature2.png"];
    [scrView2 addSubview:imageView2];
    
    UILabel *label2 = [[UILabel alloc] init];
    label2.text = @"患者跟踪";
    [label2 setTextFont:kP(36) textColor:@"#2DBED8" alpha:1.0];
    [label2 sizeToFit];
    label2.x = self.view.width * 0.5 - label2.width * 0.5;
    label2.y = CGRectGetMaxY(imageView2.frame) + kP(115);
    label2.textAlignment = NSTextAlignmentCenter;
    [scrView2 addSubview:label2];
    
    UILabel *label22 = [[UILabel alloc] init];
    label22.text = @"患者病情实时追踪，体检报告一键同步至手机";
    [label22 setTextFont:kP(32) textColor:@"#999999" alpha:1.0];
    label22.numberOfLines = 0;
    label22.x = kP(108);
    label22.y = CGRectGetMaxY(imageView2.frame) + kP(85);
    label22.width = self.view.width - 2 * label22.x;
    label22.height = kP(300);
    label22.textAlignment = NSTextAlignmentCenter;
    [scrView2 addSubview:label22];


    //
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    
    
    //
    CGRect scrollRect = [self.view convertRect:imageView2.frame fromView:scrView2];
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(self.view.width * 0.5 - kP(560) * 0.5, CGRectGetMaxY(scrollRect) + kP(60), kP(560), kP(12))];
    for (int i = 0; i < 2; i++) {
        
        UIButton *btn = [UIButton buttonWithType:0];
        CGFloat btnW = kP(60);
        CGFloat btnH = kP(12);
//        CGFloat marginW = (bgView.width - btnW * 2) / 3.0;
        CGFloat marginW = kP(24);
        CGFloat marginX = (bgView.width - kP(24) - 2 * btnW) / 2.0;
        //
        CGFloat btnX = btnW * i + marginW * i + marginX;
        CGFloat btnY = bgView.height * 0.5 - btnH * 0.5;
        
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        btn.backgroundColor = [UIColor colorWithHexString:@"#D4D4D4" alpha:1.0];
        btn.layer.cornerRadius = kP(6);
        btn.clipsToBounds = YES;
        
        if (i == 0) {
            btn.backgroundColor = [UIColor lightGrayColor];
            [self selectBtn:btn];
        }
        
        //
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:btn];
        
        [self.btnArr addObject:btn];
    }
   
    
//    bgView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:bgView];
    
    
    CGFloat btnWidth = (self.view.width - 2 * kP(64) - kP(52)) * 0.5;
    //
    UIButton *registBtn = [[UIButton alloc] initWithFrame:CGRectMake(kP(64), HZScreenH - kP(106) - 34, btnWidth, kP(106))];
    [registBtn setTitle:@"注册" forState:UIControlStateNormal];
    registBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    [registBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    registBtn.layer.cornerRadius = kP(16);
    registBtn.clipsToBounds = YES;
    [registBtn addTarget:self action:@selector(enterRegistVC:) forControlEvents:UIControlEventTouchUpInside];
//    enterBtn.hidden = YES;
//    enterBtn.alpha = 0;
    [self.view addSubview:registBtn];
    self.registBtn = registBtn;
    
    
    //
    UIButton *enterBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(registBtn.frame) + kP(52), HZScreenH - kP(106) - 34, btnWidth, kP(106))];
    [enterBtn setTitle:@"登录" forState:UIControlStateNormal];
    enterBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    [enterBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    //
    enterBtn.backgroundColor = [UIColor whiteColor];
    enterBtn.layer.borderColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0].CGColor;
    enterBtn.layer.borderWidth = kP(2);
    enterBtn.layer.cornerRadius = kP(16);
    enterBtn.clipsToBounds = YES;
    [enterBtn addTarget:self action:@selector(enterLoginVC:) forControlEvents:UIControlEventTouchUpInside];
    //    enterBtn.hidden = YES;
    //    enterBtn.alpha = 0;
    [self.view addSubview:enterBtn];
    self.enterBtn = enterBtn;
    //2.显示/更新版本
//    [self setUpNewVersion];
    
}


#pragma mark -- setUpNewVersion
- (void)setUpNewVersion {
    [HZAccountTool getAppStoreNewVersionWithSuccess:^(id responseObject) {
        NSLog(@"-----AppStoreNewVersionsetUpNewVersion----%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultDict = responseObject[@"results"];
            NSDictionary *versionDict = resultDict[@"version"];
            
            
            //
            NSString *forceUpVersion = versionDict[@"ForceUpVersion"];
            
            
            
            
            NSString *message = versionDict[@"message"];
            NSString *newVersion = versionDict[@"newVersion"];
            
            
            NSLog(@"---message:%@---newVersion---%@", message, newVersion);
            message = [message stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
            //        //
            //        newVersion = @"1.0.1";
            //        forceUpVersion = @"1.0.1";
            //        message = @"本次更新增加了专家钱包的提现功能，钱包里的钱能随时发起提现!钱包里的钱能随";
            // 1. 获取当前版本号
//            NSString *currentVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
//            currentVersion = @"1.0";
            //  判断是否更新
            if ([kCurrentBundleVersion compare:newVersion options:NSNumericSearch] == NSOrderedSame || [kCurrentBundleVersion compare:newVersion options:NSNumericSearch] == NSOrderedDescending) return;// currentVersion >= newVersion
            
            // 弹出提示框
            UIWindow *myWindow = [UIApplication sharedApplication].keyWindow;
            UIView *cover = [[UIView alloc] initWithFrame:myWindow.bounds];
            cover.backgroundColor = [UIColor blackColor];
            cover.alpha = 0.3;
            //        cover.hidden = YES;
            [myWindow addSubview:cover];
            self.cover = cover;
            
            HZAlertView *alertView = [[HZAlertView alloc] initWithFrame:CGRectMake(kP(2), kP(0), kP(556), kP(556))];
            alertView.center = myWindow.center;
            alertView.backgroundColor = [UIColor lightGrayColor];
            alertView.layer.cornerRadius = 10;
            alertView.clipsToBounds = YES;
            alertView.delegate = self;
            alertView.alertString = message;
            alertView.isForceUp = [kCurrentBundleVersion compare:forceUpVersion options:NSNumericSearch] == NSOrderedAscending;
            [myWindow addSubview:alertView];
            self.alertView = alertView;
            
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------AppStoreNewVersion---error:%@", error);
    }];
    
}

#pragma mark -- HZAlertViewDelegate
- (void)enterToAppStore {
    self.cover.hidden = NO;
    self.alertView.hidden = NO;
    [self loadAppStoreController];
}

- (void)cancelToDismissWithIsForceUp:(BOOL)isForceUp {
    self.cover.hidden = YES;
    self.alertView.hidden = YES;
    
    //
    if (!isForceUp) return;
    //
    exit(0);
}


#pragma mark -- 进入AppStore更新下载

- (void)loadAppStoreController {
    NSString *appStoreUrl = @"https://itunes.apple.com/us/app/ming-yi-lian-meng/id1173508999?l=zh&ls=1&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreUrl] options:@{} completionHandler:nil];
}


#pragma mark -- UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 获取当前角标
    NSInteger i = scrollView.contentOffset.x / scrollView.width;
    UIButton *selBtn = self.btnArr[i];
    [self selectBtn:selBtn];
    
//    if (i == 1) {
//        [self addEnterBtn];
//    }

    
}




- (void)selectBtn:(UIButton *)selBtn {
    self.shareBtn.selected = NO;
    
    self.shareBtn.backgroundColor = [UIColor colorWithHexString:@"#D4D4D4" alpha:1.0];
    selBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    self.shareBtn = selBtn;
    
}

- (void)btnClick:(UIButton *)btn{
    [self selectBtn:btn];
    
    // 子视图控制器的View滚动到可视区域
    NSInteger i = [self.btnArr indexOfObject:btn];
    CGFloat x = i * self.scrollView.width;
    [self.scrollView setContentOffset:CGPointMake(x, 0) animated:NO];
//    if (i == 1) {
//        [self addEnterBtn];
//    }
}


//- (void)addEnterBtn {
//    [UIView animateWithDuration:2 animations:^{
//        self.enterBtn.alpha = 1;
//    }];
//}

- (NSMutableArray *)btnArr {
    if (_btnArr == nil) {
        _btnArr = [NSMutableArray array];
    }
    return _btnArr;
}

- (void)enterRegistVC:(UIButton *)registBtn {
    
    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZRegistViewController") new]];
    [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
}

- (void)enterLoginVC:(UIButton *)loginBtn {
    
    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
    [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
