//
//  HZAlertView.m
//  HZAlertViewDIYDemo
//
//  Created by 季怀斌 on 2016/11/15.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZAlertView.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZAlertView ()
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UILabel *alertLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZAlertView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setSubViews];
    }
    return self;
}

- (void)setSubViews {

    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), self.width, self.height - kP(104))];
    bgImageView.backgroundColor = [UIColor grayColor];
    bgImageView.image = [UIImage imageNamed:@"new"];
    [self addSubview:bgImageView];
    self.bgImageView = bgImageView;

    UILabel *alertLabel = [[UILabel alloc] init];
//    alertLabel.backgroundColor = [UIColor redColor];
    alertLabel.textAlignment = NSTextAlignmentLeft;
    [bgImageView addSubview:alertLabel];
    self.alertLabel = alertLabel;

    //
    UIImage *norImage = [UIImage createImageWithColor:[UIColor whiteColor]];
    UIImage *selImage = [UIImage createImageWithColor:[UIColor lightGrayColor]];

    //
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    CGFloat cancelBtnX = kP(0);
    CGFloat cancelBtnY = CGRectGetMaxY(bgImageView.frame) + kP(2);
    CGFloat cancelBtnW = self.width * 0.5 -  0.5 * kP(2);
    CGFloat cancelBtnH = self.height - cancelBtnY;
    cancelBtn.frame = CGRectMake(cancelBtnX, cancelBtnY, cancelBtnW, cancelBtnH);
    cancelBtn.backgroundColor = [UIColor whiteColor];
    [cancelBtn setTitle:@"下次再说" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [cancelBtn setBackgroundImage:selImage forState:UIControlStateHighlighted];
    [cancelBtn setBackgroundImage:norImage forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
    [cancelBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];


    UIButton *ensureBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    CGFloat ensureBtnW = cancelBtnW;
    CGFloat ensureBtnX = self.width - ensureBtnW;
    CGFloat ensureBtnY = cancelBtnY;
    CGFloat ensureBtnH = cancelBtnH;
    ensureBtn.frame = CGRectMake(ensureBtnX, ensureBtnY, ensureBtnW, ensureBtnH);
    ensureBtn.backgroundColor = [UIColor whiteColor];
    [ensureBtn setTitle:@"立即更新" forState:UIControlStateNormal];
    [ensureBtn setTitleColor:[UIColor colorWithHexString:@"#05B1C2" alpha:1.0] forState:UIControlStateNormal];
    
    [ensureBtn setBackgroundImage:selImage forState:UIControlStateHighlighted];
    [ensureBtn setBackgroundImage:norImage forState:UIControlStateNormal];
    
    ensureBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
    [ensureBtn addTarget:self action:@selector(ensure:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:ensureBtn];
}



- (void)layoutSubviews {
    [super layoutSubviews];
    
//    @"本次更新增加了专家钱包的提现功能，钱包里的钱能随时发起提现!"
    
    self.alertLabel.text = self.alertString;
    self.alertLabel.numberOfLines = 0;
    [self.alertLabel setTextFont:kP(32) textColor:@"#666666" alpha:1.0];
    self.alertLabel.x = kP(44);
    self.alertLabel.y = kP(288);
    self.alertLabel.width = self.bgImageView.width - 2 * self.alertLabel.x;
    self.alertLabel.height = kP(120);
}


- (void)cancel:(UIButton *)btn {
    NSLog(@"--------cancel");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelToDismissWithIsForceUp:)]) {
        [self.delegate cancelToDismissWithIsForceUp:_isForceUp];
    }

}


- (void)ensure:(UIButton *)btn {
    NSLog(@"---------ensure");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterToAppStore)]) {
        [self.delegate enterToAppStore];
    }

}


-(void)setIsForceUp:(BOOL)isForceUp {
    _isForceUp = isForceUp;
}

-(void)setAlertString:(NSString *)alertString {
    _alertString = alertString;
}
@end
