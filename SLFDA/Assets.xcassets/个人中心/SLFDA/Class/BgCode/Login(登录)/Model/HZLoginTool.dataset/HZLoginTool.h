//
//  NSLoginTool.h
//  SLIOP
//
//  Created by 季怀斌 on 16/9/28.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSLoginTool : NSObject
@property (nonatomic, assign) BOOL isLogin;
+(instancetype)shareLoginTool;
@end
