//
//  HZTimerTool.h
//  LoginDemo
//
//  Created by 季怀斌 on 16/6/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZTimerTool : NSObject
@property (nonatomic, assign) NSInteger time;

+(instancetype)shareTimerTool;
-(void) startTimer;
@end
