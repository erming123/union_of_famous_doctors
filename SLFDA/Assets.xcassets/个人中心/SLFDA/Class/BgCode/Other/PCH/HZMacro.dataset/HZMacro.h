//
//  HZMacro.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/12.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#ifndef HZMacro_h
#define HZMacro_h

/**
 测试环境
 */
#define kGatewayUrlStr(str) (NSString *)[NSString stringWithFormat:@"https://test-gateway.rubikstack.com/%@", str]
#define kOauthUrlStr(str) (NSString *)[NSString stringWithFormat:@"https://test-oauth.rubikstack.com/%@", str]

/**
// 生产环境
// */
//#define kGatewayUrlStr(str) (NSString *)[NSString stringWithFormat:@"https://gateway.rubikstack.com/%@", str]
//#define kOauthUrlStr(str) (NSString *)[NSString stringWithFormat:@"https://oauth.rubikstack.com/%@", str]

/**
  网络环境
// */
//////// 1.应用层的网络环境
//#define kGatewayUrlStr(str) [[NSUserDefaults standardUserDefaults] boolForKey:@"测试网络环境"] == YES ? (NSString *)[NSString stringWithFormat:@"https://test-gateway.rubikstack.com/%@", str] : (NSString *)[NSString stringWithFormat:@"https://gateway.rubikstack.com/%@", str]
//
//// 2.帐号层的网络环境
//#define kOauthUrlStr(str) [[NSUserDefaults standardUserDefaults] boolForKey:@"测试网络环境"] == YES ? (NSString *)[NSString stringWithFormat:@"https://test-oauth.rubikstack.com/%@", str] : (NSString *)[NSString stringWithFormat:@"https://oauth.rubikstack.com/%@", str]

/**
    调试打印
 */
//#ifdef DEBUG // 调试9527
//#define NSLog(...)  NSLog(__VA_ARGS__)
//#else // 发布
//#define NSLog(...)
//#endif

// 为了配合三方的NSLog函数。取消(请勿删除)上面的定义方式。
#ifdef DEBUG
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...)
#endif
/**
    控件属性
 */
#define kGreenColor [UIColor colorWithHexString:@"21b8c6" alpha:1.0]
#define kAccess_token [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"access_token"]
#define kBasic_token [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"basic_token"]
#define kMyChatId [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"myChatId"]
#define kUnReadMsgCount @"unReadMsgCount"
#define kEnterForeground @"kEnterForeground"
#define kCurrentSystemVersion ([[[UIDevice currentDevice] systemVersion] floatValue])
#define kCurrentBundleVersion [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"]

#pragma mark -- 像素转点的屏幕适配
#define HZScreenW [UIScreen mainScreen].bounds.size.width
#define HZScreenH [UIScreen mainScreen].bounds.size.height
#define HZMargin 10

#define kP(px) (CGFloat)(px * 0.5 * CGRectGetWidth([[UIScreen mainScreen] bounds]) / 375)
#define WeakSelf(weakSelf)      __weak __typeof(&*self)   weakSelf  = self;
#define StrongSelf(strongSelf)  __strong __typeof(&*self) strongSelf = weakSelf;

#define SafeAreaTopHeight (HZScreenH == 812.0 ? 88 : 64)
#define SafeAreaBottomHeight (HZScreenH == 812.0 ? 34 : 0)

#pragma mark -- basicClientId
#define kMyBasicClientId @"46db8ec6526f4794ac11be2e95369870"


#pragma mark -- 容联云
#define kMyAccountSID @"8aaf0708566d0df601566d72a51500cc"
#define kMyAppKey @"8a216da8584bf8cf01585c7bbea80b09"
#define kMyAppToken @"96ee48273df53448fad47c7f6d25c295"
#define kOrganGraftingUserData @"push_type_add_pat"
#pragma mark -- Bugly
#define kMyBuglyAppId @"5d742489d8"
//#define kMyAppKey @"8a216da8584bf8cf01585c7bbea80b09"
//#define kMyAppToken @"96ee48273df53448fad47c7f6d25c295"

/**
  方法
 */

#define showMessage(MESSAGE,TARGET, BLOCK) \
UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:MESSAGE preferredStyle:UIAlertControllerStyleAlert]; \
UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil]; \
[alertController addAction:cancelAction]; \
UIAlertAction *ensureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:BLOCK]; \
[alertController addAction:ensureAction]; \
[TARGET presentViewController:alertController animated:YES completion:nil];\


#endif /* HZMacro_h */
