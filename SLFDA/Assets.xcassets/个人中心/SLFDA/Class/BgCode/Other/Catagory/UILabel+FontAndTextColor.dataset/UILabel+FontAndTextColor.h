//
//  UILabel+FontAndTextColor.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (FontAndTextColor)
- (void)setTextFont:(CGFloat)font textColor:(NSString *)hexString alpha:(CGFloat)alpha;
@end
