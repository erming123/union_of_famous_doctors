//
//  HZChatHelper.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/12/18.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZChatHelper.h"
#import "HZMessageDB.h"
#import "HZMessageViewController.h"
#import "HZUser.h"
#import "SFHFKeychainUtils.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZChatHelper ()
@property (nonatomic, strong) HZMessageDB *messageDB;
@property (nonatomic, assign) long long lastTimeStamp;
@property (nonatomic, assign) BOOL isTimeShow;
@end
NS_ASSUME_NONNULL_END
@implementation HZChatHelper
+(HZChatHelper*)sharedInstance {
    static HZChatHelper*chatHelper;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        chatHelper = [[self alloc] init];
    });
    
    return chatHelper;
}
#pragma mark -- 初始化图片下载的队列
-(NSOperationQueue *)downLoadImgQueue{
    if (!_downLoadImgQueue) {
        _downLoadImgQueue = [[NSOperationQueue alloc]init];
        //最大并发数
        _downLoadImgQueue.maxConcurrentOperationCount = 4;
    }
    return _downLoadImgQueue;
}

#pragma mark -- 初始化数据库
- (HZMessageDB *)messageDB {
    if (_messageDB == nil) {
        _messageDB = [HZMessageDB initMessageDB];
    }
    return _messageDB;
}
//
#pragma mark -- 发送信息
- (void)sendMessage:(ECMessage *)message withImagePath:(NSString *)imagePath {
    
    [[ECDevice sharedInstance].messageManager sendMessage:message progress:nil completion:^(ECError *error, ECMessage *message) {
        if (error.errorCode == ECErrorType_NoError) {
            
            //1.
            NSLog(@"发送成功：－－－－－－－－－－－－－－－－－－timestamp%@", message.timestamp);
            NSLog(@"\n-----from:%@\n------to:%@\n----messageId:%@\n-----messageBody:%@\n---timestamp:%@\n---userData:%@\n---sessionId:%@\n----isGroup:%d\n---messageState:%ld\n----isRead:%d\n----groupSenderName:%@", message.from, message.to, message.messageId, message.messageBody, message.timestamp, message.userData, message.sessionId, message.isGroup, message.messageState, message.isRead, message.groupSenderName);
            
            //            NSLog(@"-----kMyChatId----%@", kMyChatId);
            
            //2.发通知存数据，把error，message传出去
            if (message.messageBody.messageBodyType == MessageBodyType_Text) {
                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onMesssageSend object:message];
            } else if(message.messageBody.messageBodyType == MessageBodyType_Image){
                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onMesssageSend object:message userInfo:@{@"imagePath":imagePath}];
            }
            
            
        } else if (error.errorCode == ECErrorType_Have_Forbid || error.errorCode == ECErrorType_File_Have_Forbid) {
            NSLog(@"您被群组禁言");
        } else {
            NSLog(@"发送失败：-----%ld", error.errorCode);
            
            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"发送失败，请检查网络" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self loginChat];
            }];
            if (kCurrentSystemVersion >= 9.0) {
                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            }
            [alertCtr addAction:ensureAlertAction];
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
            
        }
    }];
}


- (void)loginChat{
    
    //    BOOL iSIMlogin = [[NSUserDefaults standardUserDefaults] boolForKey:@"isIMOnLine"];
    //
    //    if (iSIMlogin) return;
    ECLoginInfo * loginInfo = [[ECLoginInfo alloc] init];
    loginInfo.username = kMyChatId;
    loginInfo.appKey = kMyAppKey;
    loginInfo.appToken = kMyAppToken;
    loginInfo.authType = LoginAuthType_NormalAuth;//默认方式登录
    loginInfo.mode = LoginMode_InputPassword;
    
    
    // 容联云登录
    [[ECDevice sharedInstance] login:loginInfo completion:^(ECError *error) {
        if (error.errorCode == ECErrorType_NoError) {
            
            NSLog(@"---------容联云登录成功");
            //            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIMOnLine"];
            
            //            [[UIApplication sharedApplication].keyWindow.rootViewController.view makeToast:@"容联云登录成功"
            //                        duration:1.5f
            //                        position:CSToastPositionCenter];
        } else {
            NSLog(@"---------容联云登录失败");
            //            [self loginChat];
            
            //            [[UIApplication sharedApplication].keyWindow.rootViewController.view makeToast:@"容联云登录失败"
            //                                                                                  duration:1.5f
            //                                                                                  position:CSToastPositionCenter];
        }
    }];
}
#pragma mark -- 接收信息
- (void)onReceiveMessage:(ECMessage *)message {
    
    
    
    //  NSLog(@"----zxc------%@---------%@", message.from, message.to);
    //    [self.messgeArr addObject:message];
    int messageType = message.messageBody.messageBodyType;
    switch (messageType) {
            
        case MessageBodyType_Text: {
            //            ECTextMessageBody *msgBody = (ECTextMessageBody *)message.messageBody;
            
            if ([message.userData containsString:kOrganGraftingUserData]) {
                [self recieveThePushMessageHandle:message];
            }else{
                message = [self setMyReceiveUnReadMsg:(ECMessage *)message];
                [self addToMesageDBWithMessage:message withLocalImagePath:nil];
            }

            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onMesssageReceive object:message];
            break;
        }
        case MessageBodyType_Image: {
            // ECImageMessageBody *msgBody = (ECImageMessageBody *)message.messageBody;
            
            //            NSLog(@"收到的信息是：--------%@", msgBody.remotePath);
            //            NSLog(@"收到的信息是：--------%@", msgBody.thumbnailRemotePath);
            message = [self setMyReceiveUnReadMsg:(ECMessage *)message];
            [self addToMesageDBWithMessage:message withLocalImagePath:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onMesssageReceive object:message];
            break;
        }
            
    }
}



#pragma mark -- <#class#>


/**
 @该通知回调接口在代理类里面
 @brief 接收离线消息代理函数
 @param message 接收的消息
 */
-(void)onReceiveOfflineMessage:(ECMessage*)message {
    
    // 在数据库中查找是否已读,是否已有 (有问题的--)
    NSNumber *boolNum = [HZMessageDB initMessageDB].needUpdateReadStateArray[message.messageId];
    message.isRead = boolNum.boolValue;
    NSLog(@"是否已读----%zd,",message.isRead);
    
    //屏蔽掉测试环境账号信息 (正式环境与测试环境 用的是荣连云同一个消息环境所以有问题)
    if ([message.userData isEqualToString:@"e0944258c0a04057b08dd466483b7225,季怀斌-c,主治医师"]) return;
    
    
    if (message.isRead) return;
    
    //    NSLog:(@"收到%@的消息,属于%@会话", message.from, message.sessionId);
    
    switch(message.messageBody.messageBodyType){
        case MessageBodyType_Text:{
            ECTextMessageBody *msgBody = (ECTextMessageBody *)message.messageBody;
            NSLog(@"收到的是离线文本消息------%@",msgBody.text);
            if ([message.userData containsString:kOrganGraftingUserData]) {
                [self recieveThePushMessageHandle:message];
            }else{
                [self addToMesageDBWithMessage:message withLocalImagePath:nil];
                [self setMyReceiveUnReadMsg:(ECMessage *)message];
            }
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onMesssageOffLineReceive object:message];
            
            break;
        }
            
        case MessageBodyType_Image:{
            ECImageMessageBody *msgBody = (ECImageMessageBody *)message.messageBody;
            NSLog(@"图片文件remote路径------%@",msgBody. remotePath);
            NSLog(@"缩略图片文件remote路径------%@",msgBody. thumbnailRemotePath);
            [self addToMesageDBWithMessage:message withLocalImagePath:msgBody.remotePath];
            [self setMyReceiveUnReadMsg:(ECMessage *)message];
            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onMesssageOffLineReceive object:message];
            
            break;
        }
            
            
    }
    
    
    
    
}

- (NSMutableArray *)unReadNewsArr {
    if (!_unReadNewsArr) {
        _unReadNewsArr = [NSMutableArray array];
    }
    
    return _unReadNewsArr;
}


- (ECMessage*)setMyReceiveUnReadMsg:(ECMessage *)message {
    
    
//    if (![message.userData containsString:@","]) {
//        return message;
//    }
//
    UIViewController *currentVC = [self getCurrentVC];
    if ([currentVC isKindOfClass:[HZMessageViewController class]]) {
        HZMessageViewController *messageVC =(HZMessageViewController*) currentVC;
        NSString *key = [NSString stringWithFormat:@"%@,%@,%@",messageVC.chatter.openid, messageVC.chatter.userName, messageVC.chatter.titleName];
        
        if ([key isEqualToString:message.userData]){
            message.isRead = YES;
            return message;
        }
    }
    
    [self.unReadNewsArr addObject:message];
    
    if (self.unReadNewsArr.count == 0) {
        return message;
    }
    
    //    NSMutableArray *newsListArr = [NSMutableArray array];
    NSMutableArray *userDataArr = [NSMutableArray array];
    
    for (ECMessage *message in self.unReadNewsArr) {
        
        NSLog(@"----xde------:%@", message.userData);
        
        NSString *userData = (NSString *)message.userData;
        [userDataArr addObject:userData];
    }
    
    NSLog(@"----qas------:%@", userDataArr);
    
    NSStringCompareOptions comparisonOptions = NSCaseInsensitiveSearch|NSNumericSearch|
    NSWidthInsensitiveSearch|NSForcedOrderingSearch;
    NSComparator sort = ^(NSString *obj1,NSString *obj2){
        NSRange range = NSMakeRange(0,obj1.length);
        return [obj1 compare:obj2 options:comparisonOptions range:range];
    };
    NSArray *tempUserDataArr = [userDataArr sortedArrayUsingComparator:sort];
    
    
    NSLog(@"----zxcvb----------------%@", tempUserDataArr);
    
   NSMutableArray *resultArr = [self getOrderArrWithArr:tempUserDataArr];
    

    
    
    NSMutableDictionary *newsDict;
    NSDictionary *myDict  = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
    if (myDict) {
        newsDict = [NSMutableDictionary dictionaryWithDictionary:myDict];
    } else {
        newsDict = [NSMutableDictionary dictionary];
    }
    
    
    //
    NSLog(@"----rtyu----------------%@", resultArr);
    NSLog(@"---qsc--:%@", newsDict);
    
    
    for (NSArray *arr in resultArr) {
        
        NSString *keyStr = (NSString *)[arr firstObject];
        //IM
        if (![keyStr isEqualToString:kOrganGraftingUserData]) {
            NSString *countStr = [NSString stringWithFormat:@"%d", (int)arr.count];
            //        NSString *countStr = @"10";
            
            NSString *lastCount = [newsDict objectForKey:keyStr];
            countStr = [NSString stringWithFormat:@"%zd",countStr.integerValue+lastCount.integerValue];
            
            NSLog(@"--:%lu----:%lu", countStr.length, (unsigned long)keyStr.length);
            //        [newsDict setObject:countStr  forKey:keyStr];
            
            [newsDict setValue:countStr forKey:keyStr];
            break;
        }

    }
    
    NSLog(@"-----------adf------:%@", newsDict);
    NSDictionary *myNewDict = [NSDictionary dictionaryWithDictionary:newsDict];
    [[NSUserDefaults standardUserDefaults] setObject:myNewDict forKey:@"profileVCtrNews"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // 设置完后清空一下
    [self.unReadNewsArr removeAllObjects];
    //
    return message;
}


#pragma mark -- getOrderArrWithArr: 把杂乱的userdata分类, 找到每个userdata对应的个数(消息未读数)
- (NSMutableArray *)getOrderArrWithArr:(NSArray *)originArr {
        NSMutableArray *resultArr = [NSMutableArray array];
    
        NSString *lastC = @"a";
        NSMutableArray *dataArr;
    
    
        for (NSString *user in originArr) {
            if (![user isEqualToString:lastC]) {
                lastC = user;
    
    
                if (dataArr && dataArr.count > 0) {
                    [resultArr addObject:dataArr];
                }
    
    
                dataArr = [[NSMutableArray alloc] init];
                [dataArr addObject:user];
    
            } else { // 此时此字符等于上一个字符， 相同的字符。
                [dataArr addObject:user];
            }
    
        }
    
    
    
    
        // 1. 把最后一个数组放进去
        if (dataArr && dataArr.count > 0) {
            [resultArr addObject:dataArr];
        }
    
    return resultArr;
}

-(NSInteger) onGetOfflineMessage
{
    return -1;
}



-(void)onReceiveOfflineCompletion:(BOOL)isCompletion {
    //离线消息接收完成
    NSLog(@"---------离线消息接收完成");
    
}


- (void)onConnectState:(ECConnectState)state failed:(ECError *)error {
    NSLog(@"------dddwwwqqq---%ld", state);
    
    
    
    //    switch (state) {
    //        case State_ConnectSuccess:
    //            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onConnected object:[ECError errorWithCode:ECErrorType_NoError]];
    //            break;
    //        case State_Connecting:
    //            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onConnected object:[ECError errorWithCode:ECErrorType_Connecting]];
    //            break;
    //        case State_ConnectFailed:
    //            [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onConnected object:error];
    //            break;
    //        default:
    //            break;
    //    }
    
    
    
    //    if (error.errorCode == ECErrorType_KickedOff) {
    //        NSLog(@"--------被异地登录!");
    //    }
    
    if (error.errorCode == ECErrorType_KickedOff) {// 被异地登录
        
        
        
        
        NSLog(@"被异地登录!");
        
        
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"被异地登录!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //            // 去除头像
            //            NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
            //            if (!userDict) return;
            //            NSString *userOpenId = userDict[@"openid"];
            //            [[NSUserDefaults standardUserDefaults] removeObjectForKey:userOpenId];
            //
            //
            //
            //            //
            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
            //            //        self.activityIndicatorView.hidden = YES;
            //
            //            //推出容联云
            //            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isIMOnLine"];
            //            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"myChatId"];
            //            // 推出本地服务器, 置空token
            //            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
            //            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user"];
            //
            //            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self setDelate];
            
            
            // 选择页面
            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
            
        }];
        if (kCurrentSystemVersion >= 9.0) {
            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        }
        [alertCtr addAction:ensureAlertAction];
        
        //        UIViewController *alertVCtr = [UIApplication sharedApplication].keyWindow.rootViewController;
        //
        //        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
        //
        //        if ([alertVCtr isEqual:navCtr]) return;
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
        
    }
}
-(void)onOfflineMessageCount:(NSUInteger)count {
    NSLog(@"onOfflineMessageCount=========-------------------=========%lu",(unsigned long)count);
    
    NSString *offLineMsgCount = [NSString stringWithFormat:@"%lu", (unsigned long)count];
    [[NSUserDefaults standardUserDefaults] setObject:offLineMsgCount forKey:@"offLineMsgCount"];
}

- (void)setDelate {
    NSUserDefaults *defatluts = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictionary = [defatluts dictionaryRepresentation];
    
    //1.
    NSArray *allkeysArr = [dictionary allKeys];
    //2.
    NSDictionary *orderIdJCYBIdDict = [HZOrderIdJCYBIdDictTool shareInstance].orderIdJCYBIdDict;
    
    NSArray *keyBigArr = [orderIdJCYBIdDict allValues];
    NSLog(@"-----keyBigArr----%@", keyBigArr);
    NSMutableArray *keysArr = [NSMutableArray array];
    for (NSArray *keyArr in keyBigArr) {
        
        for (NSString *key in keyArr) {
            
            [keysArr addObject:key];
        }
        
    }
    
    //3.需要留下的
    NSMutableSet * leftKeySet = [[NSMutableSet alloc] initWithArray:keysArr];
    
    // 4. 全部的key
    NSMutableSet *allKeySet = [[NSMutableSet alloc] initWithArray:allkeysArr];
    
    [allKeySet minusSet:leftKeySet];
    
    NSArray *orderIdJCYBIdArr = [allKeySet allObjects];
    //
    NSLog(@"-----orderIdJCYBIdArr----%@", orderIdJCYBIdArr);
    for(NSString *key in orderIdJCYBIdArr){
        
        if ([key isEqualToString:@"648860CTKey"] ) {
            
            continue;
        }
        [defatluts removeObjectForKey:key];
        [defatluts synchronize];
    }
    
    //
    [SFHFKeychainUtils deleteItemForUsername:@"HZ" andServiceName:@"WOWJOY" error:nil];
    
    //
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
    
    [[SDImageCache sharedImageCache] clearMemory];//可不写
    
    //
    [[YYCache cacheWithName:@"MyInfo_YYcache"] removeAllObjects];
}

//#pragma mark -- saveWithMessage
//
//- (void)saveWithMessage:(ECMessage *)message{
//    //
//    int type = message.messageBody.messageBodyType;
//    switch(type){
//        case MessageBodyType_Text:{
//            //            ECTextMessageBody *msgBody = (ECTextMessageBody *)message.messageBody;
//            [self addToMesageDBWithMessage:message withLocalImagePath:nil];
//            break;
//        }
//
//        case MessageBodyType_Image:{
//            ECImageMessageBody *msgBody = (ECImageMessageBody *)message.messageBody;
////            NSLog(@"图片文件remote路径------%@",msgBody. remotePath);
////            NSLog(@"缩略图片文件remote路径------%@",msgBody. thumbnailRemotePath);
//
//            [self addToMesageDBWithMessage:message withLocalImagePath:msgBody.remotePath];
//            break;
//        }
//
//
//    }
//
//}


#pragma mark -- 模型转字典保存到数据库
- (void)addToMesageDBWithMessage:(ECMessage *)message withLocalImagePath:(NSString *)imageLocalPath {
    
    
    NSMutableArray *receivedMsgArr = [self.messageDB getReceivedMsgUnReadCountArrWithOrderId:message.userData];
    NSString *unReadCountStr = [receivedMsgArr lastObject];
    //        NSLog(@"--dddsssaaaaa22222-------%@", unReadCountStr);
    
    if (receivedMsgArr.count == 0) {
        unReadCountStr = @"1";
    } else {
        unReadCountStr = [NSString stringWithFormat:@"%d", [[receivedMsgArr lastObject] intValue] + 1];
    }
    
    //    NSLog(@"---sssssaaaaa------%@", unReadCountStr);
    //1. 设置时间间隔
    NSString *timeStr;
    if (message.messageBody.messageBodyType == MessageBodyType_Image) {
        if (message.timestamp) {
            if (message.timestamp.length > 3) {
                timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
            }
        }
        //        NSLog(@"=========save2=======%@", message);
    } else {
        
        
        //                if ([message.from isEqualToString:self.IMOrder.remoteAccount]) {
        
        if (message.timestamp) {
            if (message.timestamp.length > 3) {
                timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
            }
        }
        //                } else {
        //                    timeStr = message.timestamp;
        //
        //        }
        
    }
    
    //
    self.isTimeShow = [timeStr integerValue] - self.lastTimeStamp <= 60;
    
    self.lastTimeStamp = [timeStr integerValue];
    
    //3. 保存
    if (message.messageBody.messageBodyType == MessageBodyType_Text) {
        
        ECTextMessageBody *textMsgBody = (ECTextMessageBody *)message.messageBody;
        NSDictionary *messageDict = @{
                                      @"from":message.from,
                                      @"to":message.to,
                                      @"messageId":message.messageId,
                                      @"timestamp":timeStr,
                                      @"messageBodyType":@(message.messageBody.messageBodyType),
                                      @"text":textMsgBody.text,
                                      @"userData":message.userData,
                                      @"sessionId":message.sessionId,
                                      @"isGroup":@(message.isGroup),
                                      @"messageState":@(message.messageState),
                                      @"isRead":@(message.isRead),
                                      @"groupSenderName":@"暂无",
                                      @"isTimeShow":@(self.isTimeShow)
                                      };
        [self.messageDB addMessageDict:messageDict withUnReadCount:[unReadCountStr intValue]];
        
        
        
    } else if (message.messageBody.messageBodyType == MessageBodyType_Image) {
        
        ECImageMessageBody *imageMsgBody = (ECImageMessageBody *)message.messageBody;
        
        
        NSDictionary *messageDict = @{
                                      @"from":message.from,
                                      @"to":message.to,
                                      @"messageId":message.messageId,
                                      @"timestamp":timeStr,
                                      @"messageBodyType":@(message.messageBody.messageBodyType),
                                      @"thumbnailDownloadStatus":@(imageMsgBody.thumbnailDownloadStatus),
                                      @"remotePath":imageMsgBody.remotePath,
                                      @"thumbnailRemotePath":imageMsgBody.thumbnailRemotePath,
                                      @"userData":message.userData,
                                      @"sessionId":message.sessionId,
                                      @"isGroup":@(message.isGroup),
                                      @"messageState":@(message.messageState),
                                      @"isRead":@(message.isRead),
                                      @"groupSenderName":@"暂无",
                                      @"isTimeShow":@(self.isTimeShow)
                                      };
        [self.messageDB addMessageDict:messageDict withUnReadCount:[unReadCountStr intValue]];
        
        
        
    }
}

- (UIViewController *)getCurrentVC {
    
    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    if (!window) {
        return nil;
    }
    UIView *tempView;
    for (UIView *subview in window.subviews) {
        if ([[subview.classForCoder description] isEqualToString:@"UILayoutContainerView"]) {
            tempView = subview;
            break;
        }
    }
    if (!tempView) {
        tempView = [window.subviews lastObject];
    }
    
    id nextResponder = [tempView nextResponder];
    while (![nextResponder isKindOfClass:[UIViewController class]] || [nextResponder isKindOfClass:[UINavigationController class]] || [nextResponder isKindOfClass:[UITabBarController class]]) {
        tempView =  [tempView.subviews firstObject];
        
        if (!tempView) {
            return nil;
        }
        nextResponder = [tempView nextResponder];
    }
    return  (UIViewController *)nextResponder;
}

#pragma mark - 推送接收的消息处理
/**
 * 收到推送
 */
- (void)recieveThePushMessageHandle:(ECMessage*)message{
    
    if (![message.userData containsString:kOrganGraftingUserData]) return;
    
    // 推送(必定是文字推送)
    NSDictionary *userDataDict = message.userData.mj_JSONObject;
    NSMutableArray *receivedMsgArr = [self.messageDB getReceivedMsgUnReadCountArrWithOrderId:userDataDict[@"msgType"]];
    NSString *unReadCountStr = [receivedMsgArr lastObject];
    //        NSLog(@"--dddsssaaaaa22222-------%@", unReadCountStr);
    
    if (receivedMsgArr.count == 0) {
        unReadCountStr = @"1";
    } else {
        unReadCountStr = [NSString stringWithFormat:@"%d", [[receivedMsgArr lastObject] intValue] + 1 ];
    }
    NSString *timeStr;
    if (message.timestamp) {
        if (message.timestamp.length > 3) {
            timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
        }
    }
    
   // ECTextMessageBody *textMsgBody = (ECTextMessageBody *)message.messageBody;
    NSArray *arr = userDataDict[@"patients"];
    NSMutableArray *messageArray = [NSMutableArray array];
    for (int i = 0; i<arr.count; ++i) {
     NSDictionary *dictJson = arr[i];
     NSString *jsonText = dictJson.mj_JSONString;
     NSDictionary *messageDict = @{
                                  @"from":message.from,
                                  @"to":message.to,
                                  @"messageId":[NSString stringWithFormat:@"%@_%zd",message.messageId,i],
                                  @"timestamp":timeStr,
                                  @"messageBodyType":@(message.messageBody.messageBodyType),
                                  @"text":jsonText,
                                  @"userData":userDataDict[@"msgType"],
                                  @"sessionId":message.sessionId,
                                  @"isGroup":@(message.isGroup),
                                  @"messageState":@(message.messageState),
                                  @"isRead":@(message.isRead),
                                  @"groupSenderName":@"暂无"
                                  };
        ECMessage *subMessage = [[ECMessage alloc]init];
        subMessage.from = message.from;
        subMessage.to = message.to;
        subMessage.messageId = [NSString stringWithFormat:@"%@_%zd",message.messageId,i];
        subMessage.timestamp = message.timestamp;
        subMessage.userData = userDataDict[@"msgType"];
        subMessage.sessionId = message.sessionId;
        ECTextMessageBody *textMessageBody = [[ECTextMessageBody alloc]init];
        textMessageBody.text = jsonText;
        subMessage.messageBody = textMessageBody;
        [messageArray addObject:subMessage];
        int unreadCount = [unReadCountStr intValue];
        unreadCount++;
     [self.messageDB addMessageDict:messageDict withUnReadCount:unreadCount];
    }
    // 设置未读数
    [self setReceivePushUnReadMsg:messageArray];
    
}


-(void)setReceivePushUnReadMsg:(NSArray*)messageArray{
    
    [self.unReadNewsArr addObjectsFromArray:messageArray];
    
    if (self.unReadNewsArr.count == 0) return;
    
    //    NSMutableArray *newsListArr = [NSMutableArray array];
    NSMutableArray *userDataArr = [NSMutableArray array];
    
    for (ECMessage *message in self.unReadNewsArr) {
        
        NSLog(@"----xde------:%@", message.userData);
        
        NSString *userData = (NSString *)message.userData;
        [userDataArr addObject:userData];
    }
    
    NSLog(@"----qas------:%@", userDataArr);
    
    NSStringCompareOptions comparisonOptions = NSCaseInsensitiveSearch|NSNumericSearch|
    NSWidthInsensitiveSearch|NSForcedOrderingSearch;
    NSComparator sort = ^(NSString *obj1,NSString *obj2){
        NSRange range = NSMakeRange(0,obj1.length);
        return [obj1 compare:obj2 options:comparisonOptions range:range];
    };
    NSArray *tempUserDataArr = [userDataArr sortedArrayUsingComparator:sort];
    
    
    NSLog(@"----zxcvb----------------%@", tempUserDataArr);
    
    
    
    NSMutableArray *resultArr = [NSMutableArray array];
    
    NSString *lastC = @"a"; // 随便定义一个字符而已
    NSMutableArray *dataArr;
    
    
    for (NSString *user in tempUserDataArr) {
        
        // 首字母
        
        
        // 是否是英文字母,ewewew, fdffd
        
        
        if (![user isEqualToString:lastC]) { // 是英文字母， 如果字符属性位置不是'1'的， 检索首字母一样的
            lastC = user; //  lastC = 's'
            
            // 不进，此刻没有数组
            if (dataArr && dataArr.count > 0) { // 把上一个数组放进最终大数组里
                [resultArr addObject:dataArr];
            }
            
            // 创建一个新的小数组， 把user放进去
            dataArr = [[NSMutableArray alloc] init];
            [dataArr addObject:user];
            
        } else { // 此时此字符等于上一个字符， 相同的字符。
            [dataArr addObject:user];
        }
        
    }
    
    
    
    
    // 1. 把最后一个数组放进去
    if (dataArr && dataArr.count > 0) {
        [resultArr addObject:dataArr];
    }
    
    
    NSMutableDictionary *newsDict;
    NSDictionary *myDict  = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
//    NSString *initNumber = @"0";
    if (myDict) {
        newsDict = [NSMutableDictionary dictionaryWithDictionary:myDict];
    } else {
        newsDict = [NSMutableDictionary dictionary];
    }
    
    NSLog(@"----rtyu----------------%@", resultArr);
    NSLog(@"---qsc--:%@", newsDict);
    
    
    for (NSArray *arr in resultArr) {
        
        // push message
        NSString *keyStr = (NSString *)[arr firstObject];
        if ([keyStr containsString:kOrganGraftingUserData]) {
            
            NSString *countStr = [NSString stringWithFormat:@"%d", (int)arr.count];
            //        NSString *countStr = @"10";
            
            NSString *lastCount = [newsDict objectForKey:keyStr];
            countStr = [NSString stringWithFormat:@"%zd",lastCount.integerValue+countStr.integerValue];
            NSLog(@"--:%lu----:%lu", countStr.length, (unsigned long)keyStr.length);
            //        [newsDict setObject:countStr  forKey:keyStr];
            [newsDict setValue:countStr forKey:keyStr];
            // 设置一次就可以了
            break;
        }
        
    }
    
    NSLog(@"CHAThELPER-SET:%@", newsDict);
    NSDictionary *myNewDict = [NSDictionary dictionaryWithDictionary:newsDict];
    [[NSUserDefaults standardUserDefaults] setObject:myNewDict forKey:@"profileVCtrNews"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // 设置完后清空一下
    [self.unReadNewsArr removeAllObjects];
    
}
@end

