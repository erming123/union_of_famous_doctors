//
//  HZIMChatViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZIMChatViewController.h"
#import "HZKeyboardView.h"
#import "HZMessageDB.h"
#import "HZChatCell.h"
#import <AVFoundation/AVFoundation.h>
#import "CommonTools.h"
#import "UIImageView+WebCache.h"

//
#import "TZImagePickerController.h"
#import "UIView+Layout.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "TZImageManager.h"
#import "TZVideoPlayerController.h"

//

//
#import "HZOrder.h"
#import "HZUserTool.h"

#import "IQKeyboardManager.h"
#import "SDPhotoBrowser.h"
//#import "HZLocalOrderDetailViewController.h"
//#import "HZRemoteOrderDetailViewController.h"
//#import "HZExampleRemoteOrderDetailViewController.h"
//
#define DefaultThumImageHigth 90.0f
#define DefaultPressImageHigth 960.0f
@interface HZIMChatViewController ()<UITableViewDataSource, UITableViewDelegate, HZKeyboardViewDelegate,HZChatCellDelegate,TZImagePickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, SDPhotoBrowserDelegate>

//** <#注释#>*/
@property (nonatomic, strong) HZTableView *chatTableView;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) HZKeyboardView *keyboardView;
@property (nonatomic, strong) NSMutableArray *messageArr;
//** <#注释#>*/
@property (nonatomic, assign) SystemSoundID receiveSound;
@property (nonatomic, assign) SystemSoundID sendSound;
//** <#注释#>*/
@property (nonatomic, copy) NSString *imagePath;
//** <#注释#>*/
@property (nonatomic, strong) HZMessageDB *messageDB;
//
//** <#注释#>*/
@property (nonatomic, strong) NSString *callID;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *receiveBtn;

//** <#注释#>*/
@property (nonatomic, strong) NSDictionary *photoMessageDict;

@property (nonatomic, assign) long long lastTimeStamp;
@property (nonatomic, assign) BOOL isTimeShow;


//
//** <#注释#>*/
@property (nonatomic, assign) CGFloat cellMaxY;
@property (nonatomic, assign) CGRect keyboardF;
//** <#注释#>*/
@property (nonatomic, strong) NSDictionary *userInfoDict;

/**
 *  ----------------------
 */
@property (nonatomic, strong) TZImagePickerController *imagePickerVc;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *selectedAssets;


//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *imagePathArr;

@property (nonatomic, copy) NSString *chatterID;
@property (nonatomic,strong) UIImage *photoNeedImage;
@end

@implementation HZIMChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"f4f4f6" alpha:1.0];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    //1. 设置页面
    [self setUpSubViews];
    
    //2. 即时通讯业务
    //    [self makeChat];
    
    //3.播放音效
    [self playSound];
    
    //4.
    self.selectedPhotos = [NSMutableArray array];
    self.selectedAssets = [NSMutableArray array];
    
    
    //6.
    //    [self setUpChatAvatar];
    
    //7.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendMessageSuccess:) name:KNOTIFICATION_onMesssageSend object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMessageSuccess:) name:KNOTIFICATION_onMesssageReceive object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetTableViewHeight) name:UIKeyboardDidHideNotification object:nil];
    
    //
}

-(void)resetTableViewHeight{
    
    self.chatTableView.height = self.keyboardView.y;
    
}



- (void)sendMessageSuccess:(NSNotification *)notification {
    ECMessage *message = notification.object;
    NSString *imagePath = notification.userInfo[@"imagePath"];
    [self addToMesageDBWithMessage:message withLocalImagePath:imagePath];
}

- (void)receiveMessageSuccess:(NSNotification *)notification {
    //2.
    ECMessage *message = notification.object;
    if ([message.userData isEqualToString:self.IMOrder.bookingOrderId]) {
        // 收到同一个人
        // [self.messageDB upDateUnReadMsgCountWithOrderId:self.saveUserData];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"offLineMsgCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        ECTextMessageBody *textMsgBody;
        ECImageMessageBody *msgBody;
        NSString *timeStr;
        switch (message.messageBody.messageBodyType) {
            case MessageBodyType_Text:
                textMsgBody = (ECTextMessageBody *)message.messageBody;
                
                if (message.timestamp) {
                    if (message.timestamp.length > 3) {
                        timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
                    }
                }
                
                break;
            case MessageBodyType_Image:
                msgBody = (ECImageMessageBody *)message.messageBody;
                if (message.timestamp) {
                    if (message.timestamp.length > 3) {
                        timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
                    }
                }
                break;
            default:
                break;
        }
        
        NSDictionary *dict = @{
                               @"from":message.from,
                               @"to":message.to,
                               @"messageId":message.messageId,
                               @"timestamp":timeStr,
                               @"text":textMsgBody.text,
                               @"messageBodyType":@(message.messageBody.messageBodyType),
                               @"thumbnailDownloadStatus":msgBody?@(msgBody.thumbnailDownloadStatus):[NSNull null],
                               @"remotePath":msgBody?@(msgBody.thumbnailDownloadStatus):[NSNull null],
                               @"thumbnailRemotePath":msgBody?msgBody.thumbnailRemotePath:[NSNull null],
                               @"userData":message.userData,
                               @"sessionId":message.sessionId,
                               @"isGroup":@(message.isGroup),
                               @"messageState":@(message.messageState),
                               @"isRead":@(message.isRead),
                               @"groupSenderName":@"暂无",
                               @"isTimeShow":@(self.isTimeShow)
                               };
        
        [self.messageArr addObject:dict];
        //   self.messageArr = [self.messageArr mutableCopy];
        [self.chatTableView reloadData];
        [self scrollowToBottom];
    }
    
}

//
- (void)setIMOrder:(HZOrder *)IMOrder {
    _IMOrder = IMOrder;
}


#pragma mark -- refesh
- (void)refreshMyTable {
    
    //1. 未读数全部置为零
    [self.messageDB upDateUnReadMsgCountWithOrderId:self.IMOrder.bookingOrderId];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"offLineMsgCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.messageArr = [self.messageDB getMessageArrWithOrderId:self.IMOrder.bookingOrderId];
    //    NSLog(@"-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==%@", self.messageArr);
    
    
    //    NSMutableArray *unReadMsgCountArr = [NSMutableArray array];
    //    for (NSDictionary *msgDict in self.messageArr) {
    //
    //        if (!msgDict[@"isRead"] && [msgDict[@"from"] isEqualToString:self.IMOrder.remoteAccount]) {
    //            NSDictionary *dict = msgDict;
    //            msgDict[@"isRead"] = YES;
    //            [unReadMsgCountArr addObject:msgDict];
    //        }
    //    }
    
    
    [self.chatTableView reloadData];
    [self scrollowToBottom];
    
    
    
    
    //    [[NSUserDefaults standardUserDefaults] setObject:unReadMsgCountArr forKey:kUnReadMsgCount];
    
}

#pragma mark -- 获取信息数组

- (NSMutableArray *)messgeArr {
    if (_messageArr == nil) {
        _messageArr = [NSMutableArray array];
    }
    return _messageArr;
}

#pragma mark -- 初始化数据库
- (HZMessageDB *)messageDB {
    if (_messageDB == nil) {
        _messageDB = [HZMessageDB initMessageDB];
    }
    return _messageDB;
}



//#pragma mark -- <#class#>
//- (void)setUpChatAvatar{
//    NSString *sendAvatarId = [self.IMOrder.orderType isEqualToString:@"01"] ? self.IMOrder.localDocotorOpenid : self.IMOrder.expertOpenid;
//    NSString *receiveAvatarId = [self.IMOrder.orderType isEqualToString:@"01"] ? self.IMOrder.expertOpenid : self.IMOrder.localDocotorOpenid;
//
//    [self setSendAvatarWithOpenId:sendAvatarId];
//    [self setReceiveAvatarWithOpenId:receiveAvatarId];
//
//}



//- (void)setSendAvatarWithOpenId:(NSString *)sendAvatarId {
//
//    NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:sendAvatarId];
//    if (!imageData) {
//        //
//        [HZUserTool getAvatarWithAvatarOpenId:sendAvatarId success:^(UIImage *image) {
//        NSData *imageData = UIImagePNGRepresentation(image);//把image归档为NSData
//        [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:sendAvatarId];
//
//        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSLog(@"---------%@", error);
//    }];
//
//}
//}


//- (void)setReceiveAvatarWithOpenId:(NSString *)receiveAvatarId {
//
//    NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:receiveAvatarId];
//    if (!imageData) {
//    [HZUserTool getAvatarWithAvatarOpenId:receiveAvatarId success:^(UIImage *image) {
//        NSData *imageData = UIImagePNGRepresentation(image);//把image归档为NSData
//        [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:receiveAvatarId];
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSLog(@"---------%@", error);
//        }];
//
//}
//
//}


#pragma mark -- 设置页面

- (void)setUpSubViews {
    
    //
    self.navigationItem.title = [self.IMOrder.orderType isEqualToString:@"01"] ? self.IMOrder.expertName : self.IMOrder.localDoctorName;
    
    //
    self.chatterID = [self getFriendChatIdWithIMOrder:self.IMOrder];
    
//    self.view.backgroundColor = [UIColor colorWithHexString:@"#eeeeee" alpha:1.0];
    
    HZTableView *chatTableView = [[HZTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - kP(101) - SafeAreaTopHeight - SafeAreaBottomHeight) style:UITableViewStylePlain];
    chatTableView.delegate = self;
    chatTableView.dataSource = self;
    chatTableView.tableFooterView = [UIView new];
    chatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    chatTableView.backgroundColor = [UIColor redColor];
    //    chatTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:chatTableView];
    self.chatTableView = chatTableView;
    
    
    HZKeyboardView *keyboardView = [[HZKeyboardView alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, kP(101))];
    //    keyboardView.isInInquiryCtr = self.chatInInquiryCtr;
    keyboardView.y = self.chatTableView.height;
    // 设置通知改变键盘的位置
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];
    keyboardView.delegate = self;
    [self.view addSubview:keyboardView];
    [self.view bringSubviewToFront:keyboardView];
    self.keyboardView = keyboardView;
    
    //
    //    self.keyboardView.isLessThanChatTableViewCellMaxY = self.messageDB.DBCellMaxY < kP(600);
    //    self.keyboardView.isLessThanChatTableViewCellMaxY = self.messageDB.DBCellMaxY < ;
}

#pragma mark -- 键盘弹出与收回
- (void)keyboardWillChangeFrameNotification:(NSNotification *)notification {
    CGRect keyboardF = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardF = keyboardF;
    
    if (keyboardF.origin.y < HZScreenH ) {// 键盘弹出
        
        NSLog(@"弹出键盘-- %@",NSStringFromCGRect(keyboardF));
        //        if (self.messageDB.DBCellMaxY < kP(600)) {
        NSDictionary *userInfoDict = notification.userInfo;
        self.userInfoDict = userInfoDict;
        CGFloat animationDuration = [userInfoDict[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        UIViewAnimationCurve curve = [userInfoDict[UIKeyboardAnimationCurveUserInfoKey] integerValue];
        
        
        NSLog(@"----小于kP(600)-----");
        [UIView animateWithDuration:animationDuration delay:0 options:(curve << 16| UIViewAnimationOptionBeginFromCurrentState) animations:^{
            
            self.keyboardView.y = self.view.height - keyboardF.size.height -self.keyboardView.height;
            self.chatTableView.height = self.keyboardView.y;
            
        } completion:^(BOOL finished) {
            
        }];
        
    } else {// 键盘收回
        
        NSLog(@"弹出收起来-- ");
        
        self.chatTableView.height = HZScreenH - self.keyboardView.height - SafeAreaTopHeight - SafeAreaBottomHeight;
        self.keyboardView.y = self.chatTableView.height;
    }
    
    
    [self scrollowToBottom];
}

#pragma mark -- 播放音效
- (void)playSound {
    
    //1.获得音效文件的全路径
    NSString *receiveSoundPath = [[NSBundle mainBundle] pathForResource:@"receive_msg"
                                                                 ofType:@"caf"];
    NSURL *receiveSoundURL = [NSURL fileURLWithPath:receiveSoundPath];
    
    OSStatus receiveErr = AudioServicesCreateSystemSoundID((__bridge CFURLRef)receiveSoundURL,
                                                           &_receiveSound);
    if (receiveErr != kAudioServicesNoError) {
        NSLog(@"Could not load %@, error code: %d", receiveSoundURL, (int)receiveErr);
    }
    //1.获得音效文件的全路径
    NSString *sendSoundPath = [[NSBundle mainBundle] pathForResource:@"send_msg"
                                                              ofType:@"caf"];
    NSURL *sendSoundURL = [NSURL fileURLWithPath:sendSoundPath];
    
    OSStatus sendErr = AudioServicesCreateSystemSoundID((__bridge CFURLRef)sendSoundURL,
                                                        &_sendSound);
    if (sendErr != kAudioServicesNoError) {
        NSLog(@"Could not load %@, error code: %d", sendSoundURL, (int)sendErr);
    }
    
}

- (void)dealloc {
    
    AudioServicesDisposeSystemSoundID(_receiveSound);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messgeArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    NSDictionary *modelDict = self.messgeArr[indexPath.row];
    NSString *cellID = [HZChatCell cellIdentifierWithModel:modelDict];
    HZChatCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[HZChatCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    //    NSLog(@"-----sdsdsdsdsdsdsdsdsdsds*****--111--%@", self.IMOrder);
    cell.chatMessageDict = modelDict;
    cell.IMOrder = self.IMOrder;
    cell.delegate = self;
    return cell;
}


#pragma mark -- UITableViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [self.view endEditing:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    NSArray *modelDictArr = [self.messageDB getMessageArrWithOrderId:self.IMOrder.bookingOrderId];
    
    //    if (modelDictArr) {
    //        <#statements#>
    //    }
    NSDictionary *modelDict = self.messgeArr[indexPath.row];
    return [HZChatCell cellHeightWithModel:modelDict];
}

#pragma mark -- HZKeyboardViewDelegate
#pragma mark -- 发送文本信息
- (void)sendTextMessage:(NSString *)textMessage{
    
    ECTextMessageBody *messageBody = [[ECTextMessageBody alloc] initWithText:textMessage];
    
    //-----------------------------------------
    NSString *friendChatId = [self.IMOrder.orderType isEqualToString:@"01"] ? self.IMOrder.remoteAccount : self.IMOrder.localAccount;
    ECMessage *message = [[ECMessage alloc] initWithReceiver:friendChatId body:messageBody];
    
    NSLog(@"-----ddd----%@", self.IMOrder);
    // 其本地时间
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval tmp = [date timeIntervalSince1970];
    message.timestamp = [NSString stringWithFormat:@"%lld", (long long)tmp];
    
    // ------------------userData-------------
    message.userData = self.IMOrder.bookingOrderId;
    
    //
    [[HZChatHelper sharedInstance] sendMessage:message withImagePath:nil];
    
    NSLog(@"-----s----from:%@---------to:%@--------%@", message.to, message.from, self.IMOrder.bookingOrderId);
    
    //发送后 修改 tableView 高度
    self.chatTableView.height = HZScreenH - kP(101) - SafeAreaTopHeight - self.keyboardF.size.height - SafeAreaBottomHeight;
    
}


#pragma mark -- 相册与相机

- (void)pickImageFromAlbum:(UIButton *)albumBtn {
    
    [self pushImagePickerController];
    
}


- (void)pickImageFromCamera:(UIButton *)cameraBtn {
    BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
    
    if (!isCamera) {
        [self setUpAlertViewWithTitle:@"没有摄像头"];
        return;
    }
    self.imagePickerController = [[UIImagePickerController alloc] init];
    self.imagePickerController.delegate = self;
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    self.imagePickerController.allowsEditing = YES;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}


- (void)setUpAlertViewWithTitle:(NSString *)title {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -- UIImagePickerControllerDelegate
// 相机拍照发送
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    // 隐藏picker
    [picker dismissViewControllerAnimated:YES completion:nil];
    // 取出图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    // 发送图片
    [self sendSelectedImage:image];
}

// 相册获取发送
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    for (UIImage *image in photos) {
        [self sendSelectedImage:image];
    }
}


- (void)sendSelectedImage:(UIImage *)image {
    
    if ([self.chatterID isEqualToString:kMyChatId]) {
        [[UIApplication sharedApplication].keyWindow.rootViewController.view makeToast:@"请不能给自己发信息"
                                                                              duration:1.5f
                                                                              position:CSToastPositionCenter];
        return;
    }
    NSString *imagePath = [self saveToDocument:image];
    NSMutableArray *imagePathArr = [NSMutableArray array];
    [imagePathArr addObject:imagePath];
    self.imagePathArr = imagePathArr;
    NSMutableArray *messageArr;
    //
    for (NSString *imagePath in self.imagePathArr) {
        NSString *friendChatId = self.chatterID;
        ECImageMessageBody *mediaBody = [[ECImageMessageBody alloc] initWithFile:imagePath displayName:imagePath.lastPathComponent];
        ECMessage *message = [[ECMessage alloc] initWithReceiver:friendChatId body:mediaBody];
        message.userData = self.IMOrder.bookingOrderId;
        NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
        NSTimeInterval tmp =[date timeIntervalSince1970]*1000;
        message.timestamp = [NSString stringWithFormat:@"%lld", (long long)tmp];
        
        messageArr = [NSMutableArray array];
        [messageArr addObject:message];
    }
    
    
    
    //---------------------------上面执行完毕再执行下面的多线程-----------------------------
#warning 入库前设置本地时间，以本地时间排序和以本地时间戳获取本地数据库缓存数据
    for (NSInteger i = 0; i < messageArr.count; i++) {
        NSString *imagePath = self.imagePathArr[i];
        ECMessage *message = messageArr[i];
        [self sendMessage:message withLocalImagePath:imagePath];
    }
}


- (void)sendMessage:(ECMessage *)message withLocalImagePath:(NSString *)imagePath {
    
    //1. userData
    message.userData = self.IMOrder.bookingOrderId;
    //2.
    [[HZChatHelper sharedInstance] sendMessage:message withImagePath:imagePath];
}

-(NSString*)saveToDocument:(UIImage*)image {
    
    
    
    
    //    NSLog(@"=========save=======%@", image);
    UIImage* fixImage = [self fixOrientation:image];
    
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString* fileName = [NSString stringWithFormat:@"%@.jpg", [formater stringFromDate:[NSDate date]]];
    
    //    NSString* filePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:fileName];
    //
    //    //图片按0.5的质量压缩－》转换为NSData
    CGSize pressSize = CGSizeMake((DefaultPressImageHigth/fixImage.size.height) * fixImage.size.width, DefaultPressImageHigth);
    UIImage * pressImage = [CommonTools compressImage:fixImage withSize:pressSize];
    NSData *imageData = UIImageJPEGRepresentation(pressImage, 0.5);
    // [imageData writeToFile:filePath atomically:YES];
    //
    //    CGSize thumsize = CGSizeMake((kP(253)/fixImage.size.height) * fixImage.size.width, kP(253));
    //    UIImage * thumImage = [CommonTools compressImage:fixImage withSize:thumsize];
    //    NSData * photo = UIImageJPEGRepresentation(thumImage, 0.5);
    //    NSString * thumfilePath = [NSString stringWithFormat:@"%@.jpg_thum", filePath];
    //    [photo writeToFile:thumfilePath atomically:YES];
    
    // [[SDImageCache sharedImageCache] storeImage:fixImage forKey:fileName toDisk:YES];
    NSString *imageCache = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
    NSString *imageCachePath = [NSString stringWithFormat:@"%@/default/com.hackemist.SDWebImageCache.default/%@",imageCache,fileName];
    [imageData writeToFile:imageCachePath atomically:YES];
    
    return imageCachePath;
    
}



- (UIImage *)fixOrientation:(UIImage *)aImage {
    // No-op if the orientation is already correct
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform     // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,CGImageGetBitsPerComponent(aImage.CGImage), 0,CGImageGetColorSpace(aImage.CGImage),CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
        default:              CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);              break;
    }       // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

#pragma mark -- 模型转字典保存到数据库
- (void)addToMesageDBWithMessage:(ECMessage *)message withLocalImagePath:(NSString *)imageLocalPath {
    
    
    
    //1. 设置时间间隔
    NSString *timeStr;
    if (message.messageBody.messageBodyType == MessageBodyType_Image) {
        
        if (message.timestamp) {
            if (message.timestamp.length > 3) {
                timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
            }
        }
        
        //        NSLog(@"=========save2=======%@", message);
    } else {
        
        
        if ([message.from isEqualToString:self.IMOrder.remoteAccount]) {// 本地发的
            
            
            if (message.timestamp) {
                if (message.timestamp.length > 3) {
                    timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
                }
            }
        } else {// 接收的
            timeStr = message.timestamp;
            
        }
        
    }
    self.isTimeShow = [timeStr integerValue] - self.lastTimeStamp <= 60;
    
    self.lastTimeStamp = [timeStr integerValue];
    
    //2. 设置orderId
    message.userData = self.IMOrder.bookingOrderId;
    //3. 保存
    NSDictionary *messageDict;
    if (message.messageBody.messageBodyType == MessageBodyType_Text) {
        
        
        if ([message.from isEqualToString:kMyChatId]) {// 本地发的
            ECTextMessageBody *textMsgBody = (ECTextMessageBody *)message.messageBody;
            
            messageDict = @{
                            @"from":message.from,
                            @"to":message.to,
                            @"messageId":message.messageId,
                            @"timestamp":message.timestamp,
                            @"messageBodyType":@(message.messageBody.messageBodyType),
                            @"text":textMsgBody.text,
                            @"userData":message.userData,
                            @"sessionId":message.sessionId,
                            @"isGroup":@(message.isGroup),
                            @"messageState":@(message.messageState),
                            @"isRead":@(message.isRead),
                            @"groupSenderName":@"暂无",
                            @"isTimeShow":@(self.isTimeShow)
                            };
            [self.messageDB addMessageDict:messageDict withUnReadCount:0];
            
        } else {// 接收的
            
            
            ECTextMessageBody *textMsgBody = (ECTextMessageBody *)message.messageBody;
            messageDict = @{
                            @"from":message.from,
                            @"to":message.to,
                            @"messageId":message.messageId,
                            @"timestamp":timeStr,
                            @"messageBodyType":@(message.messageBody.messageBodyType),
                            @"text":textMsgBody.text,
                            @"userData":message.userData,
                            @"sessionId":message.sessionId,
                            @"isGroup":@(message.isGroup),
                            @"messageState":@(message.messageState),
                            @"isRead":@(message.isRead),
                            @"groupSenderName":@"暂无",
                            @"isTimeShow":@(self.isTimeShow)
                            };
            [self.messageDB addMessageDict:messageDict withUnReadCount:0];
        }
        
        
    } else if (message.messageBody.messageBodyType == MessageBodyType_Image) {
        
        ECImageMessageBody *imageMsgBody = (ECImageMessageBody *)message.messageBody;
        
        if ([message.from isEqualToString:kMyChatId]) {// 本地发的
            messageDict = @{
                            @"from":message.from,
                            @"to":message.to,
                            @"messageId":message.messageId,
                            @"timestamp":timeStr,
                            @"messageBodyType":@(message.messageBody.messageBodyType),
                            @"thumbnailDownloadStatus":@(imageMsgBody.thumbnailDownloadStatus),
                            @"imagePath":imageLocalPath,
                            @"userData":message.userData,
                            @"sessionId":message.sessionId,
                            @"isGroup":@(message.isGroup),
                            @"messageState":@(message.messageState),
                            @"isRead":@(message.isRead),
                            @"groupSenderName":@"暂无",
                            @"isTimeShow":@(self.isTimeShow)
                            };
            
            NSLog(@"=========save5=======%@", messageDict[@"userData"]);
            [self.messageDB addMessageDict:messageDict withUnReadCount:0];
            
        } else {
            
            messageDict = @{
                            @"from":message.from,
                            @"to":message.to,
                            @"messageId":message.messageId,
                            @"timestamp":timeStr,
                            @"messageBodyType":@(message.messageBody.messageBodyType),
                            @"thumbnailDownloadStatus":@(imageMsgBody.thumbnailDownloadStatus),
                            @"remotePath":imageMsgBody.remotePath,
                            @"thumbnailRemotePath":imageMsgBody.thumbnailRemotePath,
                            @"userData":message.userData,
                            @"sessionId":message.sessionId,
                            @"isGroup":@(message.isGroup),
                            @"messageState":@(message.messageState),
                            @"isRead":@(message.isRead),
                            @"groupSenderName":@"暂无",
                            @"isTimeShow":@(self.isTimeShow)
                            };
            [self.messageDB addMessageDict:messageDict withUnReadCount:0];
            
        }
        
    }
    
    
    
    if ([message.from isEqualToString:kMyChatId]) {// 本地发的
        AudioServicesPlaySystemSound(_sendSound);// 播放音效
    } else {
        AudioServicesPlaySystemSound(_receiveSound);// 播放音效
    }
    
    
    [self.messageArr addObject:messageDict];
    [self.chatTableView reloadData];
    [self scrollowToBottom];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageComplete" object:nil];
    
    //--------------------------------获取当前订单的数组
    //    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    //        NSMutableArray *arr = [self.messageDB getMessageArrWithOrderId:self.IMOrder.bookingOrderId];
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            self.messageArr = [arr mutableCopy];
    //            //            NSLog(@"-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==%@", self.messageArr);
    //            [self.chatTableView reloadData];
    //            [self scrollowToBottom];
    //        });
    //    });
    //    //--------------------------------获取当前订单的数组
    //    self.messageArr = [self.messageDB getMessageArrWithOrderId:self.IMOrder.bookingOrderId];
    //    [self.chatTableView reloadData];
    //    [self scrollowToBottom];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- 字典转模型获取到数组中
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self refreshMyTable];
    [IQKeyboardManager sharedManager].enable = NO;
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [IQKeyboardManager sharedManager].enable = YES;
    
    //    [self.messageDB deleteAllMessage];
}

#pragma mark -- 滚到底部
- (void)scrollowToBottom {
    if (self.messageArr.count != 0) {
        
        //        self.chatTableView.contentSize = CGSizeMake(HZScreenW, 0);
        
        if (self.chatTableView && self.messageArr.count > 0) {
            NSIndexPath *bottomIndexPath = [NSIndexPath indexPathForRow:self.messageArr.count - 1 inSection:0];
            [self.chatTableView scrollToRowAtIndexPath:bottomIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
        
        //        if (self.chatTableView && self.chatTableView.contentSize.height > self.chatTableView.frame.size.height) {//
        //
        //
        //            NSLog(@"---------9999999------");
        //            CGPoint offset = CGPointMake(0, self.chatTableView.contentSize.height - self.chatTableView.frame.size.height);
        //            [self.chatTableView setContentOffset:offset animated:YES];
        //        }
    }
}

#pragma mark - TZImagePickerController
- (void)pushImagePickerController {
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:5 columnNumber:4 delegate:self];
    imagePickerVc.selectedAssets = nil;
    imagePickerVc.allowTakePicture = NO; // 在内部显示拍照按钮
    imagePickerVc.allowPickingVideo = YES;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingOriginalPhoto = YES;
    imagePickerVc.sortAscendingByModificationDate = YES;// 4. 照片排列按修改时间升序
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if ([picker isKindOfClass:[UIImagePickerController class]]) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}



#pragma mark -- HZChatCellDelegate
- (void)chatCellWithMessageDict:(NSDictionary *)messageDict withImageView:(UIImageView *)imageView sender:(UIGestureRecognizer *)sender {
    
    
    NSLog(@"----dsdsdsdsdsdsds-----%@", messageDict);
    
    
    if ([imageView.image isEqual:[UIImage imageNamed:@"no_pic"]]) return;
    
    
    self.photoNeedImage = imageView.image;
    SDPhotoBrowser *photoBrowser = [SDPhotoBrowser new];
    photoBrowser.backgroundColor = [UIColor colorWithHexString:@"#333333"];
    photoBrowser.delegate = self;
    photoBrowser.currentImageIndex = 0;
    photoBrowser.imageCount = 1;
    photoBrowser.sourceImagesContainerView =imageView.superview;
    photoBrowser.showIndexLabel = NO;
    [photoBrowser show];
    
}

- (void)showAndHideStatusBarWithIsBig:(BOOL)isBig {
    //    self.isClicked = !self.isClicked;
    [self.parentViewController prefersStatusBarHidden];
    [self.parentViewController setNeedsStatusBarAppearanceUpdate];
}

#pragma mark photo-Delegate
- (UIImage *)photoBrowser:(SDPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return self.photoNeedImage;
}


#pragma mark -- <#class#>
- (NSString *)getFriendChatIdWithIMOrder:(HZOrder *)IMOrder {
    return [IMOrder.orderType isEqualToString:@"01"] ? IMOrder.remoteAccount : IMOrder.localAccount;
}



@end

