//
//  HZMessageDB.h
//  SLIOP
//
//  Created by 季怀斌 on 16/9/1.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZMessageDB : NSObject
+ (instancetype)initMessageDB;
- (void)addMessageDict:(NSDictionary *)messageDict withUnReadCount:(int)unReadCount;
- (NSMutableArray *)getMessageArrWithOrderId:(NSString *)orderId;
- (NSMutableArray *)getPushMessageArrWithOrderId:(NSString *)orderId;
- (NSMutableArray *)getReceivedMsgUnReadCountArrWithOrderId:(NSString *)orderId;
- (void)upDateUnReadMsgCountWithOrderId:(NSString *)orderId;
- (void)deleteAllMessageWithOrderId:(NSString *)orderId;
- (void)deleteAllMessage;
- (NSMutableArray *)getMyAllMessageUserDataArr:(NSString*)orderId;
- (NSMutableArray *)getMsgImgPathArrWithOrderId:(NSString *)orderId;

-(void)changeTheMessageReadState:(BOOL)isRead with:(NSString*)orderId andMessageId:(NSString*)messageId;
-(void)changePushMessageReadState:(BOOL)isRead withMessageId:(NSString*)messageId;

-(NSMutableArray*)getAllUnReadMessage;

@property (nonatomic, assign) CGFloat DBCellMaxY;
@property (nonatomic, strong) NSMutableArray *msgImgPathArr;
@property (nonatomic, strong) NSMutableArray *messageArr;
@property (nonatomic,strong) NSMutableArray *IMmessageArr;
@property (nonatomic,strong) NSMutableArray *PushMessageArr;
// 全局 串行队列
@property (nonatomic,strong) dispatch_queue_t GlobSerieaQueue;
@property (nonatomic,strong) NSMutableDictionary *needUpdateReadStateArray;
@end

