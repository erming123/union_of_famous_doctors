//
//  HZDeviceAccountTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/19.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZDeviceAccountTool.h"
#import "HZHttpsTool.h"
#import "SFHFKeychainUtils.h"
#import "HZMD5Tool.h"
@implementation HZDeviceAccountTool
#pragma mark -- getMD5SaltWithKey
// 获取盐值
+ (void)getMD5SaltWithAccountKey:(NSString *_Nullable)AccountKey accountPassWordKey:(NSString *_Nullable)accountPassWordKey success:(void(^_Nullable)(NSDictionary * _Nullable saltDict))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v1/app/encode/salt?key=%@,%@", AccountKey, accountPassWordKey];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSLog(@"------获取盐值---%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            
            NSDictionary *dict = responseObject[@"results"];
            
            success(dict);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
        NSLog(@"----获取盐值--失败-----%@" , error);
    }];
    
}

#pragma mark -- getDeviceAccountAndDeviceAccountPassWordWithDeviceSingleId
+ (void)getDeviceAccountAndDeviceAccountPassWordWithDeviceAccountSalt:(NSString *)deviceAccountSalt deviceAccountPassWordSalt:(NSString *)deviceAccountPassWordSalt {
    
    // 查看
    NSString *deviceAccount = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceAccount"];
    NSString *deviceAccountPassWord = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceAccountPassWord"];
    NSLog(@"-----check----deviceAccount:%@-------deviceAccountPassWord:%@", deviceAccount, deviceAccountPassWord);
    if (![NSString isBlankString:deviceAccount] && ![NSString isBlankString:deviceAccountPassWord]) return;
    
    //1. UUIDStr
    NSString *UUIDStr = [self getUUIDStr];
    
    //2. 加密生成deviceAccount
    if (![NSString isBlankString:deviceAccountSalt]) {
            NSLog(@"---AccountSalt------%@", deviceAccountSalt);
            NSString *deviceAccount = [HZMD5Tool MD5ForLower16Bate:UUIDStr saltStr:deviceAccountSalt];
            [[NSUserDefaults standardUserDefaults] setObject:deviceAccount forKey:@"deviceAccount"];
        }

    //3. 加密生成deviceAccountPassWord
    if (![NSString isBlankString:deviceAccountPassWordSalt]) {
           NSString *deviceAccountPassWord = [HZMD5Tool MD5ForLower16Bate:UUIDStr saltStr:deviceAccountPassWordSalt];
            [[NSUserDefaults standardUserDefaults] setObject:deviceAccountPassWord forKey:@"deviceAccountPassWord"];
    }
   
}


// 获取uuid
+ (NSString *)getUUIDStr {
    
    // 查
    NSString * UUIDStr = [SFHFKeychainUtils getPasswordForUsername:@"HZ" andServiceName:@"WOWJOY" error:nil];
    if ([NSString isBlankString:UUIDStr]) {
        // 存
        BOOL isSuccessSaved =  [SFHFKeychainUtils storeUsername:@"HZ" andPassword:[self uuid] forServiceName:@"WOWJOY" updateExisting:YES error:nil];
        NSLog(@"---------BOOL------%@", isSuccessSaved == YES ? @"YES":@"NO");
    }
    
    // 查
    UUIDStr = [SFHFKeychainUtils getPasswordForUsername:@"HZ" andServiceName:@"WOWJOY" error:nil];
    NSLog(@"-----UUIDStr----%@", UUIDStr);
    return UUIDStr;
}


// 生成uuid
+ (NSString*)uuid {
    CFUUIDRef puuid = CFUUIDCreate(nil);
    CFStringRef uuidString = CFUUIDCreateString(nil, puuid);
    NSString * result = (NSString *)CFBridgingRelease(CFStringCreateCopy(NULL, uuidString));
    CFRelease(puuid);
    CFRelease(uuidString);
    return result;
}



#pragma mark -- justDeviceAccountLoginFunctionAvailableSuccess
//1.设备号登录功能是否开启
+ (void)justDeviceAccountLoginFunctionAvailableSuccess:(void(^_Nullable)(BOOL isAvailable))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v1/app/function/state?function_name=%@", @"equipmentLogin"];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSLog(@"---just设备号登录功能是否开启--成功-----%@" , responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            
            NSDictionary *resultsDict = responseObject[@"results"];
            BOOL isAvailable = resultsDict[@"available"];
            success(isAvailable);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
        NSLog(@"----just设备号登录功能是否开启--失败-----%@" , error);
    }];
}


#pragma mark -- justDeviceAccountRegistedWithDeviceAccount
//2.设备号是否已经注册
+ (void)justDeviceAccountRegistedWithDeviceAccount:(NSString *_Nullable)deviceAccount success:(void(^_Nullable)(NSString * _Nullable userId))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"user/certificate/%@", deviceAccount];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSLog(@"---just设备号是否已经注册--成功-----%@" , responseObject);
        NSString *userId;
        if ([responseObject[@"state"] isEqual:@200]) {// YES
            
            NSDictionary *dict = responseObject[@"results"];
            userId = dict[@"userId"];
            success(userId);
        } else {// NO
            success(userId);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
        NSLog(@"----just设备号是否已经注册--失败-----%@" , error);
    }];
}


#pragma mark -- registDeviceAccountWithDeviceAccount
//3.设备号注册
+ (void)registDeviceAccountWithDeviceAccount:(NSString *_Nullable)deviceAccount deviceAccountPassWord:(NSString *_Nullable)deviceAccountPassWord success:(void(^_Nullable)(NSString * _Nullable userId))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
//    NSLog(@"---------deviceAccount:%@-------deviceAccountPassWord:%@-------", deviceAccount, deviceAccountPassWord);
    //
    NSDictionary *paramDict = @{@"certificate" : deviceAccount, @"password" : deviceAccountPassWord, @"type" : @"equipment"};
    
    NSLog(@"----paramDict-----%@", paramDict);
    [HZHttpsTool POST:kGatewayUrlStr(@"user/register") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        
        NSString *userId;
        if ([responseObject[@"state"] isEqual:@200]) {
            
            NSDictionary *dict = responseObject[@"results"];
            userId = dict[@"userId"];
            success(dict[@"userId"]);
        } else {
            success(userId);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
        NSLog(@"----设备号注册--失败-----%@" , error);
    }];

}


#pragma mark -- associateDeviceAccountWithDeviceAccount
//4.设备号绑定
+ (void)associateDeviceAccountWithDeviceAccount:(NSString *_Nullable)deviceAccount deviceAccountPassWord:(NSString *_Nullable)deviceAccountPassWord success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    //
    NSDictionary *paramDict = @{@"certificate" : deviceAccount, @"password" : deviceAccountPassWord};
    [HZHttpsTool POST:kGatewayUrlStr(@"user/association") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        
        NSLog(@"---设备号绑定--成功-----%@" , responseObject);
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
        NSLog(@"----设备号绑定--失败-----%@" , error);
    }];

}


#pragma mark -- disassociateDeviceAccountWithDeviceAccount
//5.设备号解绑
+ (void)disassociateDeviceAccountWithDeviceAccount:(NSString *_Nullable)deviceAccount success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"user/association/%@?_method=%@", deviceAccount, @"delete"];
    [HZHttpsTool POST:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSLog(@"---设备号解绑--成功-----%@" , responseObject);
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
        NSLog(@"----设备号解绑--失败-----%@" , error);
    }];

}
@end
