//
//  HZAccountTool.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/9.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZAccountTool.h"
#import "AFNetworking.h"
#import "HZHttPsTool.h"

#import "HZAccount.h"
#import "MJExtension.h"
//
#import "HZLoginViewController.h"
//#define Access_tokenURLStr @"https://oauth.rubikstack.com/oauth/token?grant_type=client_credentials&scope=openid"
//#define VerifyCode @"https://gateway.rubikstack.com/rest/login/code"
//#define Login_tokenURLStr @"https://gateway.rubikstack.com/rest/login"
//

#define CZAccountFileName [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"account.data"]
@implementation HZAccountTool

// 类方法一般用静态变量代替成员属性

static HZAccount *_account;
+ (void)saveAccount:(HZAccount *)account {
    [NSKeyedArchiver archiveRootObject:account toFile:CZAccountFileName];
}

+ (HZAccount *)account {
    if (_account == nil) {
        
        _account = [NSKeyedUnarchiver unarchiveObjectWithFile:CZAccountFileName];
        
        // 判断下账号是否过期，如果过期直接返回Nil
        // 2015 < 2017
        
        //        NSDate *date = [NSDate date];
        //        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        //        NSInteger interval = [zone secondsFromGMTForDate: date];
        //        NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
        //
        //
        //        NSLog(@"---------%@---------%@", localeDate, _account.expires_date);
        //        if ([localeDate compare:_account.expires_date] != NSOrderedAscending) { // 过期
        //
        //            NSLog(@"---------的点点滴滴");
        
        
        //            return nil;
        //            [self accountWithCode:Access_tokenURLStr success:nil failure:nil];
    } else {
        NSDate *date = [NSDate date];
        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        NSInteger interval = [zone secondsFromGMTForDate: date];
        NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
        
        
        //        NSLog(@"---------%@---------%@", localeDate, _account.expires_date);
        if ([localeDate compare:_account.expires_date] != NSOrderedAscending) { // 过期
            
            NSLog(@"---------的点点滴滴");
            //            [self getBasicTokenSuccess:nil failure:nil];
            //            [self account];
        }
        
    }
    
    return _account;
}


// 获取basic token
+ (void)getBasicTokenSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
//    if (![NSString isBlankString:kBasic_token]) return;
    
    NSString *basicHeadDomainValue = @"Basic NDZkYjhlYzY1MjZmNDc5NGFjMTFiZTJlOTUzNjk4NzA6MThlOWRlZDg0YTFmNDk5ZGJjNGZkODkzOTRmZjMyMjA=";
    [HZHttpsTool POST:kOauthUrlStr(@"oauth/token?grant_type=client_credentials&scope=openid") headDomainValue:basicHeadDomainValue parameters:nil progress:nil success:^(id responseObject) {
        
        //  存储basic_token
        [[NSUserDefaults standardUserDefaults] setValue:responseObject[@"access_token"] forKeyPath:@"basic_token"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"--网络获取到--basic_token----%@", responseObject[@"access_token"]);
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
        
        NSLog(@"---获取basic token失败-----%@" , error);
        
    }];
    
    
}

// 获取 验证码
+ (void)getVerifyCodeWithUserPhoneNumber:(NSString *)userPhoneNumber success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    //    NSString *basic_token = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"basic_token"];
    //
    //    //#15	0x00000001000ec524 in main at /Users/jihuaibin/Desktop/SLFDA测试环境的副本自助注册/SLFDA/Class/Other/main.m:14
    //
    //    NSLog(@"basic_token:---%@--------userPhoneNumber：----%@", basic_token, userPhoneNumber);
    
    //    NSDictionary *vedrifyParam = @{@"client_id":@"46db8ec6526f4794ac11be2e95369870",@"client_secret":@"18e9ded84a1f499dbc4fd89394ff3220",@"phone":userPhoneNumber,@"phone":userPhoneNumber};
    NSString *basicHeadDomainValue = @"Basic NDZkYjhlYzY1MjZmNDc5NGFjMTFiZTJlOTUzNjk4NzA6MThlOWRlZDg0YTFmNDk5ZGJjNGZkODkzOTRmZjMyMjA=";
    //
    NSDictionary *vedrifyParam = @{@"phone":userPhoneNumber};
    //
    [HZHttpsTool POST:kOauthUrlStr(@"oauth/dynamic_password") headDomainValue:basicHeadDomainValue parameters:vedrifyParam progress:nil success:^(id responseObject) {
        
        
        //        if ([responseObject[@"msg"]  isEqualToString:@"success"]) {
        //
        //            NSLog(@"-----获取验证码成功----%@", responseObject);
        
        if (success) {
            success(responseObject);
        }
        
        //        } else {
        //            NSLog(@"-----获取失败----%@", responseObject);
        //        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
        
        NSLog(@"----链接失败-----%@" , error);
        
    }];
    
    
}

// 验证 验证吗
+ (void)justVerifyCodeWithPhoneNumber:(NSString *)phoneNumber verifyCode:(NSString *)verifyCode success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSString *basicHeadDomainValue = @"Basic NDZkYjhlYzY1MjZmNDc5NGFjMTFiZTJlOTUzNjk4NzA6MThlOWRlZDg0YTFmNDk5ZGJjNGZkODkzOTRmZjMyMjA=";
    
    NSDictionary *vedrifyParam = @{@"phone":phoneNumber , @"code" : verifyCode};
    [HZHttpsTool POST:kOauthUrlStr(@"oauth/check_dynamic_password") headDomainValue:basicHeadDomainValue parameters:vedrifyParam progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
        
        //        } else {
        //            NSLog(@"-----获取失败----%@", responseObject);
        //        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
        
        NSLog(@"----链接失败-----%@" , error);
        
    }];
}

// 获取 access_token
+ (void)getLoginCodeWithUserPhoneNumber:(NSString *)userPhoneNumber andLoginVerifyCode:(NSString *)loginVerifyCode success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    //    NSString *basic_token = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"basic_token"];
    //
    NSLog(@"---userPhoneNumber：----%@-----loginVerifyCode:%@", userPhoneNumber, loginVerifyCode);
    
    //
    NSString *basicHeadDomainValue = @"Basic NDZkYjhlYzY1MjZmNDc5NGFjMTFiZTJlOTUzNjk4NzA6MThlOWRlZDg0YTFmNDk5ZGJjNGZkODkzOTRmZjMyMjA=";
    //    NSDictionary *vedrifyParam = @{@"" : @"", @"" : , @"" : };
    NSString *urlStr = [NSString stringWithFormat:@"oauth/token?grant_type=dynamic_password&username=%@&dynamic_password=%@", userPhoneNumber, loginVerifyCode];
    
    NSLog(@"----kOauthUrlStr(urlStr)-----%@", kOauthUrlStr(urlStr));
    [HZHttpsTool POST:kOauthUrlStr(urlStr) headDomainValue:basicHeadDomainValue parameters:nil progress:nil success:^(id responseObject) {
        
        
        
        // 字典转模型
        //        [[NSUserDefaults standardUserDefaults] setValue:responseObject[@"access_token"] forKeyPath:@"access_token"];        //
        //        NSLog(@"-----登录成功--access_token:--%@", responseObject[@"access_token"]);
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
        
    }];
    
}




// 获取App版本号
+ (void)getAppStoreNewVersionWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {

    
         
         [self getBasicTokenSuccess:^(id responseObject) {
             
             
             NSLog(@"------获取BasicToken---%@", responseObject);
             
             
             
             
             NSString *basicHeadDomainValue = [NSString stringWithFormat:@"Bearer %@", kBasic_token];
             [HZHttpsTool GET:kGatewayUrlStr(@"cfdu/v1/openapi/json/versioncontroller/version/ios") headDomainValue:basicHeadDomainValue parameters:nil progress:nil success:^(id responseObject) {
                 
                 
                 NSLog(@"---------d---------------------");
                 if (success) {
                     success(responseObject);
                 }
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 if (failure) {
                     failure(task, error);
                 }
             }];
             
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSLog(@"-----BasicToken--error--%@", error);
         }];

//     }

}

// 注册
+ (void)registAccountWithPhoneNumber:(NSString *)phoneNumber passWord:(NSString *)passWord type:(NSString *)type success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *headDomainValue = [NSString stringWithFormat:@"Bearer %@",kBasic_token];
    NSDictionary *paramDict = @{@"certificate":phoneNumber,@"password":passWord,@"type":type};
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:paramDict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *dictStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    NSLog(@"-----dd----%@", dictStr);
    NSLog(@"-----qq--paramDict:%@-----headDomainValue:%@", dictStr, headDomainValue);
    //
    [HZHttpsTool POST:kGatewayUrlStr(@"user/register") headDomainValue:headDomainValue parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

// 刷新access_token
+ (void)getNewAccessTokenWithRefreshToken:(NSString *)refreshToken success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSString *basicHeadDomainValue = @"Basic NDZkYjhlYzY1MjZmNDc5NGFjMTFiZTJlOTUzNjk4NzA6MThlOWRlZDg0YTFmNDk5ZGJjNGZkODkzOTRmZjMyMjA=";
    //    NSDictionary *paramDict = @{@"grant_type" : @"refresh_token", @"refresh_token" : refreshToken};
    
    //    NSLog(@"-----1----%@", refreshToken);
    //    refreshToken = [NSString stringWithFormat:@"%@---", refreshToken];
    //    NSLog(@"-----2----%@", refreshToken);
    NSString *urlStr = [NSString stringWithFormat:@"oauth/token?grant_type=refresh_token&refresh_token=%@", refreshToken];
    [HZHttpsTool POST:kOauthUrlStr(urlStr) headDomainValue:basicHeadDomainValue parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        //        NSLog(@"--re--error-----%@", error);
        
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSInteger statusCode = [response statusCode];
        
        
        
        if (statusCode == 401) { // refresh_token 过期
            
            NSLog(@"----POST401--refresh_token---过期---");
            
            
            // 是否开启设备账号
            BOOL isRegisted = [[NSUserDefaults standardUserDefaults] boolForKey:@"deviceAccountIsAssociated"];
            
            NSString *deviceAccount = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceAccount"];
            NSString *deviceAccountPassWord = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceAccountPassWord"];
            
            
            
            if (isRegisted) {
                
                NSLog(@"---------设备账号刷新");
                if ([NSString isBlankString:deviceAccount] || [NSString isBlankString:deviceAccountPassWord]) return;
                [self getNewAccessTokenWithDeviceAccount:deviceAccount deviceAccountPassWord:deviceAccountPassWord success:^(id  _Nullable responseObject) {
                    
                    NSString *access_token = responseObject[@"access_token"];
                    NSString *refresh_token = responseObject[@"refresh_token"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:access_token forKey:@"access_token"];
                    [[NSUserDefaults standardUserDefaults] setObject:refresh_token forKey:@"refresh_token"];
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                    NSLog(@"---------弹出提示框");
                    [self setUpAlertViewController];
                }];
                
            } else {
                
                NSLog(@"---------弹出提示框");
                [self setUpAlertViewController];
            }
            
            
            
        } else {
            
            NSString *errorCodeStr = [NSString stringWithFormat:@"连接网络错误，错误码:%ld", error.code];
            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:errorCodeStr preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
            if (kCurrentSystemVersion >= 9.0) {
                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            }
            [alertCtr addAction:ensureAlertAction];
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
            
        }
    }];
}

// setUpAlertViewController
+ (void)setUpAlertViewController {
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"登录信息超时,请重新登录" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        HZLoginViewController *loginVCtr = [HZLoginViewController new];
        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:loginVCtr];
        
        
        NSString *access_token = kAccess_token;
        if (!access_token || [NSString isBlankString:access_token]) {
            
            NSLog(@"---------没有，重选:%@", kAccess_token);
            // 选择页面
            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
        } else {
            
            NSLog(@"---------有，重选:%@", kAccess_token);
            
            // 去除头像
            NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
            if (userDict)  {
                NSString *userOpenId = userDict[@"openid"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:userOpenId];
                 [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            // 判断是否容联云在线
            BOOL isIMOnLine = [[NSUserDefaults standardUserDefaults] boolForKey:@"isIMOnLine"];
            if (isIMOnLine) { // 在线
                NSLog(@"-------容联云在线，推出容联云和本地服务器");
                NSLog(@"退出成功!");
                
                //推出容联云
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isIMOnLine"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"myChatId"];
                // 推出本地服务器, 置空token
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                // 选择页面
                //                    loginVCtr.view.y = 64;
                //                    [loginVCtr refreshHomeSubViews];
                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
            } else {
                NSLog(@"--------容联云不在线，不用推出容联云, 推出本地服务器");
                // 推出本地服务器, 置空token
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                // 选择页面
                //                    loginVCtr.view.y = 64;
                //                    [loginVCtr refreshHomeSubViews];
                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
            }
            
        }
        
        //
    }];
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
    }
    [alertCtr addAction:ensureAlertAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
    
}

#pragma mark -- getNewAccessTokenWithDeviceAccount
// 用设备账号和密码获取新的Access_token
+ (void)getNewAccessTokenWithDeviceAccount:(NSString *_Nullable)deviceAccount deviceAccountPassWord:(NSString *_Nullable)deviceAccountPassWord success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    //
    NSString *basicHeadDomainValue = @"Basic NDZkYjhlYzY1MjZmNDc5NGFjMTFiZTJlOTUzNjk4NzA6MThlOWRlZDg0YTFmNDk5ZGJjNGZkODkzOTRmZjMyMjA=";
    NSString *urlStr = [NSString stringWithFormat:@"https://oauth.shulan.com/oauth/token?grant_type=password&username=%@&password=%@", deviceAccount, deviceAccountPassWord];
    [HZHttpsTool POST:urlStr headDomainValue:basicHeadDomainValue parameters:nil progress:nil success:^(id responseObject) {
        
        NSLog(@"----用设备账号和密码获取新的Access_token成功-----%@", responseObject);
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"----用设备账号和密码获取新的Access_token失败-----%@", error);
        if (failure) {
            failure(task, error);
        }
    }];
    
}

@end
