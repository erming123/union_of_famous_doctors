
//
//  HZCellHightCounter.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZCellHightCounter.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZCellHightCounter ()
@property (nonatomic, strong) UILabel *countLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZCellHightCounter

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.countLabel = [UILabel new];
    }
    return self;
}

- (CGFloat)cellHeightWithStrFont:(UIFont *)strFont maxWidth:(CGFloat)maxWidth countStr:(NSString *)countStr {
    self.countLabel.text = countStr;
    self.countLabel.font = strFont;
    self.countLabel.numberOfLines = 0;
    self.countLabel.width = maxWidth;
    [self.countLabel sizeToFit];
    return self.countLabel.height;
}
@end
