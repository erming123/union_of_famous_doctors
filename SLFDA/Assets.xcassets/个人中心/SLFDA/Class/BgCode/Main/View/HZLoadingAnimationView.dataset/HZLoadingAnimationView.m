//
//  HZLoadingAnimationView.m
//  SLIOP
//
//  Created by 季怀斌 on 16/9/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZLoadingAnimationView.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZLoadingAnimationView ()
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *cover;
@property (nonatomic, strong) UILabel *textLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZLoadingAnimationView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}


- (void)setUpSubViews {
    
    self.backgroundColor = [UIColor blackColor];
    self.layer.cornerRadius = kP(10);
    self.clipsToBounds = YES;
    self.alpha = 0.53;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(68), kP(40), kP(104), kP(108))];
    imageView.image = [UIImage imageNamed:@"logo"];
    [self addSubview:imageView];
    self.imageView = imageView;
    
    
    
    UIView *cover = [[UIView alloc] initWithFrame:CGRectMake(0, - kP(40), kP(120), kP(160))];
    cover.backgroundColor = [UIColor blackColor];
    cover.layer.shadowColor = [UIColor blackColor].CGColor;
    cover.layer.shadowOpacity = 1;
    cover.layer.shadowRadius = kP(10);
    cover.layer.shadowOffset = CGSizeMake(0, kP(20));
    [imageView addSubview:cover];
    self.cover = cover;
    
    
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.font = [UIFont systemFontOfSize:kP(28)];
    textLabel.text = @"正在加载...";
    textLabel.textColor = [UIColor whiteColor];
    [textLabel sizeToFit];
    textLabel.x = self.width * 0.5 - textLabel.width * 0.5;
    textLabel.y = CGRectGetMaxY(imageView.frame) + kP(25);
    [self addSubview:textLabel];
    
}

- (void)startLoadingAnimation {
    
    [self animation];
}
- (void)stopLoadingAnimation {
    [self removeFromSuperview];
}

- (void)animation {
    [UIView animateWithDuration:1.5f animations:^{
        self.cover.height = kP(2);
        self.cover.layer.shadowOffset = CGSizeMake(0, kP(20));
    } completion:^(BOOL finished) {
        
        if (finished) {
            self.cover.height = kP(240);
            self.cover.layer.shadowOffset = CGSizeZero;
        }
        [self performSelector:@selector(animation) withObject:nil afterDelay:0.15f];
        
    }];
}

@end
