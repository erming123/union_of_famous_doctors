//
//  HZPatientViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientViewController.h"
#import "HZSynchronizateToolCell.h"
#import "HZSearchPatientToolCell.h"
#import "HZPatientListCell.h"

#import "HZPatient.h"
#import "HZContactDataHelper.h"
//
#import "HZCheckListViewController.h"
#import "HZPatientSearchViewController.h"
#define kIndexH 18

<<<<<<< HEAD
=======
typedef struct sectionY {
    float minY;
    float maxY;
} sectionY;
>>>>>>> ef3d851ae440d03db36bc89e72b78bebaefad56c
@interface HZPatientViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) UITableView *patientTableView;


//** 展示第几个的大label*/
@property (nonatomic, weak) UILabel *indexShowLabel;
@property (nonatomic, strong) NSMutableArray *indexArr;


////** <#注释#>*/
//@property (nonatomic, strong) UIView *searchIndexView;
//** 索引的背景视图*/
@property (nonatomic, weak) UIView *bgView;
@property (nonatomic, strong) NSMutableArray *indexLabelArr;
//** 索引的label*/
@property (nonatomic, strong) UILabel *indexLabel;


// 数组
@property (nonatomic, strong) NSMutableArray *patientsOriginArr;
// 含有分区的数组
@property (strong,nonatomic) NSMutableArray *patientStructArr;

@property (nonatomic,strong) NSMutableArray *offsetSectionArray;

@end





@implementation HZPatientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"患者管理";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpSubViews];
    
    [self getMyData];
    
    
}


- (void)setUpSubViews {
    
    UITableView *patientTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    patientTableView.dataSource = self;
    patientTableView.delegate = self;
    patientTableView.tableFooterView = [UIView new];
    patientTableView.separatorInset = UIEdgeInsetsZero;
    patientTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:patientTableView];
    self.patientTableView = patientTableView;
}

- (void)getMyData {
    
    
    self.patientStructArr = [NSMutableArray array];
    
    self.patientsOriginArr = [NSMutableArray array];
    
    self.indexArr = [NSMutableArray array];
    NSArray *dictArr = @[@{@"portrait":@"1",@"name":@"1"},@{@"portrait":@"2",@"name":@"花几把"},@{@"portrait":@"3",@"name":@"东方不败"},@{@"portrait":@"4",@"name":@"任我行"},@{@"portrait":@"5",@"name":@"逍遥王"},@{@"portrait":@"6",@"name":@"阿"},@{@"portrait":@"13",@"name":@"百草堂"},@{@"portrait":@"8",@"name":@"三味书屋"},@{@"portrait":@"9",@"name":@"彩彩"},@{@"portrait":@"10",@"name":@"陈晨"},@{@"portrait":@"11",@"name":@"多多"},@{@"portrait":@"12",@"name":@"峨嵋山"},@{@"portrait":@"7",@"name":@"哥哥"},@{@"portrait":@"14",@"name":@"林俊杰"},@{@"portrait":@"15",@"name":@"足球"},@{@"portrait":@"16",@"name":@"58赶集"},@{@"portrait":@"17",@"name":@"搜房网"},@{@"portrait":@"18",@"name":@"欧弟"},@{@"portrait":@"1",@"name":@"3"},@{@"portrait":@"2",@"name":@"花无缺"},@{@"portrait":@"3",@"name":@"东方积极"},@{@"portrait":@"4",@"name":@"任嘎嘎"},@{@"portrait":@"5",@"name":@"逍遥疯"},@{@"portrait":@"6",@"name":@"阿Q"},@{@"portrait":@"13",@"name":@"蛋草堂"},@{@"portrait":@"8",@"name":@"三味真火"},@{@"portrait":@"9",@"name":@"彩BI"},@{@"portrait":@"10",@"name":@"陈歌"},@{@"portrait":@"11",@"name":@"拼多多"},@{@"portrait":@"12",@"name":@"太山"},@{@"portrait":@"7",@"name":@"弟弟"},@{@"portrait":@"14",@"name":@"周杰伦"},@{@"portrait":@"15",@"name":@"篮球"},@{@"portrait":@"16",@"name":@"我的老家"},@{@"portrait":@"17",@"name":@"哈哈哈"},@{@"portrait":@"18",@"name":@"吃鸡"},@{@"portrait":@"18",@"name":@"徐艺恒"},@{@"portrait":@"18",@"name":@"吃鸡"}];
    
    
    for (NSDictionary *dict in dictArr) {
        HZPatient *p = [HZPatient new];
        p.BRDAXM = dict[@"name"];
        int x = arc4random() % 2;
        p.IsAttention = x;
        //        HZLog(@"------ddssaaa-----:%@", p.namePinyinStr);
        [self.patientsOriginArr addObject:p];
    }
    
    
    
    [self changeAttentionReloadData];
    
    
}


- (void)changeAttentionReloadData{
    
    self.patientStructArr = [HZContactDataHelper getFriendListDataBy:self.patientsOriginArr];
    self.indexArr = [HZContactDataHelper getFriendListSectionBy:self.patientStructArr];// 0--8
 
    CGFloat bgViewW = kIndexH;
    CGFloat bgViewH = kIndexH * (self.indexArr.count);
    CGFloat bgViewX = self.view.width - bgViewW;
    CGFloat bgViewY = (self.view.height - 64) * 0.5 - bgViewH * 0.5 + 64;
    
    
    if(!self.bgView){
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH)];
        bgView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:bgView];
        self.bgView = bgView;
        
    }
    // 更新 height;和Y值
    self.bgView.height = bgViewH;
    self.bgView.y = bgViewY;
    [self.indexLabelArr removeAllObjects];
    
    //添加前先移除
    for (UILabel*label in self.bgView.subviews) {
        [label removeFromSuperview];
    }
    
    for (int i = 0; i < self.indexArr.count; i++) {
        
        CGFloat indexlabelX = kIndexH * 0.1;
        CGFloat indexlabelY = i * kIndexH;
        CGFloat indexlabelWH = kIndexH - indexlabelX * 2;
        
        UILabel *indexlabel = [[UILabel alloc] initWithFrame:CGRectMake(indexlabelX, indexlabelY, indexlabelWH, indexlabelWH)];
        //        UILabel *indexlabel = [[UILabel alloc] initWithFrame:indexBgView.bounds];
        indexlabel.textAlignment = NSTextAlignmentCenter;
        indexlabel.font = [UIFont boldSystemFontOfSize:kP(20)];
        //        indexlabel.backgroundColor = [UIColor greenColor];
        
        NSString *str = self.indexArr[i];
        //  if ([str isEqualToString:@"{search}"]) continue;
        indexlabel.text = str;
        //        indexlabel.backgroundColor = [UIColor greenColor];
        indexlabel.layer.cornerRadius = indexlabelWH * 0.5;
        indexlabel.clipsToBounds = YES;
        [self.bgView addSubview:indexlabel];
        [self.indexLabelArr addObject:indexlabel];
        
    }
    

    //
    if (!self.indexShowLabel) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kP(200), kP(200), kP(160), kP(160))];
        label.backgroundColor = [UIColor lightGrayColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:50];
        label.textColor = [UIColor whiteColor];
        label.center = self.view.center;
        label.alpha = 0.0;
        [self.view addSubview:label];
        self.indexShowLabel = label;
    }
    
    
    // 获得偏移y值区间
    [self getIndexOffsetSize];

    
    
    
    
    
}



- (NSMutableArray *)indexLabelArr {
    if (!_indexLabelArr) {
        _indexLabelArr = [NSMutableArray array];
    }
    return _indexLabelArr;
}

#pragma mark -- touches
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self setIndexViewWithIsTouchesEnd:NO touches:touches];
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self setIndexViewWithIsTouchesEnd:NO touches:touches];
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self setIndexViewWithIsTouchesEnd:YES touches:touches];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.patientsOriginArr.count == 0) {
        return 1;
    }
    return self.patientStructArr.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if (self.patientsOriginArr.count == 0) {
        return 1;
    }
    
    if (section == 0) {
        return 2;
    }
    
    NSArray *rowArr = self.patientStructArr[section - 1];
    return rowArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {// 同步数据
            HZSynchronizateToolCell *synchronizateToolCell = [HZSynchronizateToolCell cellWithTableView:tableView];
            cell = synchronizateToolCell;
        } else { // 搜索数据
            HZSearchPatientToolCell *searchPatientToolCell = [HZSearchPatientToolCell cellWithTableView:tableView];
            cell = searchPatientToolCell;
        }
        
    } else {
        HZPatientListCell *patientListCell = [HZPatientListCell cellWithTableView:tableView];
        
        //        if (indexPath.section != 0) {
        NSArray *rowArr = self.patientStructArr[indexPath.section - 1];
        
        patientListCell.patient = rowArr[indexPath.row];
        
        //关注与否点击
        
        WeakSelf
        patientListCell.attentionClick = ^(BOOL IsAttention) {
            
    
            // 情况太多 索性 从走一边 数据 修改数据模型 重新刷新数据
            
            for (int i = 0; i<weakSelf.patientsOriginArr.count; ++i) {
                 HZPatient *model =  weakSelf.patientsOriginArr[i];
                 HZPatient *model2 = rowArr[indexPath.row];
                if ([model.BRDAXM isEqual:model2.BRDAXM]) {
                    model.IsAttention = IsAttention;
                    break;
                }
                
            }

            //从新计算
            [weakSelf changeAttentionReloadData];
            [tableView reloadData];
            [weakSelf scrollViewDidScroll:tableView];
    
        };
        
        cell = patientListCell;
    }
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return nil;
    }
    
    id label = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerView"];
    if (!label) {
        label = [[UILabel alloc] init];
        [label setFont:[UIFont systemFontOfSize:kP(30)]];
        [label setTextColor:[UIColor grayColor]];
        [label setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    }
    
    
    [label setText:[NSString stringWithFormat:@" %@",_indexArr[section-1]]];
    return label;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return section == 0 ? 0.1 :22.0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

#pragma mark -- setIndexViewWithIsTouchesEnd
- (void)setIndexViewWithIsTouchesEnd:(BOOL)isTouchesEnd touches:(NSSet<UITouch *> *)touches{
    

    CGPoint point = [touches.anyObject locationInView:self.bgView];
    int index = (point.y / kIndexH);
    if (index >= self.patientStructArr.count && index > 0) {
        index = self.patientStructArr.count - 1;
    }else if (index < 0){
        index = -1;
    }
    self.indexShowLabel.alpha = isTouchesEnd ? 0.0 : 1.0;
    
    if (index >= 0) {
        self.indexShowLabel.text = [NSString stringWithFormat:@"%@", self.indexArr[index]];
    }
    [self.patientTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index+1] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        switch (indexPath.row) {
            case 0: {
                HZLog(@"--------同步数据");
            }
                break;
                
            case 1: {
                HZLog(@"--------搜索数据");
                
                HZPatientSearchViewController *patientSearchViewVCtr = [HZPatientSearchViewController new];
                [self.navigationController pushViewController:patientSearchViewVCtr animated:YES];
            }
                break;
        }
        
    } else {
        
        HZLog(@"------------点击了cell--------------");
        
    
    }
    
    
    
}
#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(120);
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self changeTheIndexInBackgroundWithOffsetY:scrollView.contentOffset.y+64];
    
}


-(void)changeTheIndexInBackgroundWithOffsetY:(CGFloat)offsetY{
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        
        NSInteger index = -1;
        for (int i= 0; i<self.offsetSectionArray.count; ++i) {
            sectionY min_max;
            NSValue *value = self.offsetSectionArray[i];
            [value getValue:&min_max];
            if (offsetY < min_max.maxY && offsetY >= min_max.minY) {
                index = i;
                break;
            }
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.indexLabel.backgroundColor = [UIColor clearColor];
            if (index >=0) {
                
                UILabel *indexLabel;
                indexLabel = self.indexLabelArr[index];
                indexLabel.backgroundColor = kGreenColor;
                self.indexLabel = indexLabel;
            }
            
            
        });
        
        
    });
    
    
    
    
    
}

- (NSInteger)indexOfSectionWithPoint:(CGFloat)offsetY{
    NSInteger index = -1;
    for (int i= 0; i<self.offsetSectionArray.count; ++i) {
        sectionY min_max;
        NSValue *value = self.offsetSectionArray[i];
        [value getValue:&min_max];
        if (offsetY < min_max.maxY && offsetY >= min_max.minY) {
            
            index = i;
            break;
        }
    }
    
    return index;
    
}


- (void)getIndexOffsetSize{
    
    
    sectionY min_Max;
    CGFloat cellHeight = kP(120);
    CGFloat headerHeight = 22;
    CGFloat initHeight = 2 * cellHeight;
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i<self.patientStructArr.count; ++i) {
        NSArray *modelArr = self.patientStructArr[i];
        
        min_Max.minY = initHeight;
        min_Max.maxY = initHeight + headerHeight +modelArr.count*cellHeight;
        initHeight = min_Max.maxY;
        
        NSValue *min_MaxValue = [NSValue valueWithBytes:&min_Max objCType:@encode(struct sectionY)];
        [arr addObject:min_MaxValue];
        
    }
    
    self.offsetSectionArray = arr;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
