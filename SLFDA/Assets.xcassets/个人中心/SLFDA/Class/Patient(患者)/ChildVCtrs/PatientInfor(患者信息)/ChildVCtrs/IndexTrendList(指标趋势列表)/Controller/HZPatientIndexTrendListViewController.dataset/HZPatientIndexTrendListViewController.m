//
//  HZPatientIndexTrendListViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientIndexTrendListViewController.h"
#import "HZSectionHeadView.h"
//
#import "HZIndexTrendDetailViewController.h"
@interface HZPatientIndexTrendListViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) UITableView *indexTrendTableView;
@end

@implementation HZPatientIndexTrendListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.isShouldPanToPopBack = NO;
    [self setUpSubViews];
}


- (void)setUpSubViews {
    UITableView *indexTrendTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    indexTrendTableView.dataSource = self;
    indexTrendTableView.delegate = self;
    [self.view addSubview:indexTrendTableView];
    self.indexTrendTableView = indexTrendTableView;
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    cell.textLabel.text = @"sdsdsdsdsdds";
    return cell;
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kP(60);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    HZSectionHeadView *headView = [[HZSectionHeadView alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, kP(60))];
    headView.leftStr = @"指标名";
    headView.midStr = @"结果";
    headView.rightStr = @"范围";
//    headView.backgroundColor = [UIColor redColor];
    return headView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HZIndexTrendDetailViewController *indexTrendDetailVCtr = [HZIndexTrendDetailViewController new];
    [self.navigationController pushViewController:indexTrendDetailVCtr animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
