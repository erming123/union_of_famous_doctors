//
//  HZPatientPhysicianAdviceViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZViewController.h"
@class HZPatient;
@interface HZPatientPhysicianAdviceViewController : HZViewController
//**患者*/
@property (nonatomic, strong) HZPatient *patient;
@property (nonatomic, copy) void(^setSiftBtnNormalBlock)();
@end
