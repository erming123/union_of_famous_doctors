//
//  HZPatientBasicInforViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZHospitalizedPatientBasicInforViewController.h"
#import "HZHospitalizePatientBasicInforCell.h"
#import "HZCellHightCounter.h"
#import "HZPatientTool.h"
#import "HZPatient.h"
@interface HZHospitalizedPatientBasicInforViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) HZTableView *patientBasicInforTableView;
@property (nonatomic, strong) HZCellHightCounter *cellHightCounter;
@property (nonatomic, strong) NSArray *patientBasicInforArr;
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
@end

@implementation HZHospitalizedPatientBasicInforViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"住院信息";
    
    [self setUpSubViews];
    
    //
    HZCellHightCounter *cellHightCounter  = [HZCellHightCounter new];
    self.cellHightCounter = cellHightCounter;
    
    //
    self.patientBasicInforArr = @[@"姓名", @"床号",@"年龄", @"主治医生", @"血型", @"病历号", @"住院状态", @"入院天数",@"入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断入院诊断"];
    [self getPatientBasicInfor];
}

- (void)setPatient:(HZPatient *)patient {
    _patient = patient;
}

- (void)getPatientBasicInfor {
    [self.activityIndicator startAnimating];
    NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    [HZPatientTool getHospitalizePatientBasicInforWithHospitalId:userDict[@"hospitalId"] patientCardId:self.patient.hisInHospitalNum success:^(id  _Nullable responseObject) {
        NSLog(@"--------ssqqqqq---------:%@", responseObject);
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSDictionary *inpatientInfoDict = resultsDict[@"inpatientInfo"];
            self.patientBasicInforArr =  @[inpatientInfoDict[@"patientName"],// 姓名
                                           inpatientInfoDict[@"bedNum"],// 床号
                                           inpatientInfoDict[@"patientAge"], // 年龄
                                           inpatientInfoDict[@"treatmentDoctor"], // 主治医生
                                           inpatientInfoDict[@"bloodGroup"], //血型
                                           inpatientInfoDict[@"cardId"], // 病历号
                                           inpatientInfoDict[@"inHospitalStatus"],// 住院状态
                                           inpatientInfoDict[@"inHospitalDays"], // 入院天数
                                           inpatientInfoDict[@"inHospitalReason"] // 入院诊断
                                           ];
            
        } else {
             [self.view makeToast:responseObject[@"msg"] duration:1.5 position:CSToastPositionBottom];
        }
        [self.patientBasicInforTableView reloadData];
        [self.activityIndicator stopAnimating];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----sssdddddddd-----%@", error);
        [self.activityIndicator stopAnimating];
    }];
}

- (void)setUpSubViews {
    HZTableView *patientBasicInforTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    patientBasicInforTableView.dataSource = self;
    patientBasicInforTableView.delegate = self;
    patientBasicInforTableView.separatorInset = UIEdgeInsetsZero;
    patientBasicInforTableView.tableFooterView = [UIView new];
    [self.view addSubview:patientBasicInforTableView];
    self.patientBasicInforTableView = patientBasicInforTableView;
    
    //
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.y -= kP(92);
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
}


#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *myTitleArr = @[@"姓名", @"床号",@"年龄", @"主治医生", @"血型", @"病历号", @"住院状态", @"入院天数",@"入院诊断"];
    HZHospitalizePatientBasicInforCell *patientBasicInforCell = [HZHospitalizePatientBasicInforCell cellWithTableView:tableView];
    patientBasicInforCell.myTitleStr = myTitleArr[indexPath.row];
    patientBasicInforCell.myTitleContentStr = self.patientBasicInforArr[indexPath.row];
    return patientBasicInforCell;
}
#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = [self.cellHightCounter cellHeightWithStrFont:[UIFont systemFontOfSize:kP(32)] maxWidth:HZScreenW - kP(268) countStr:self.patientBasicInforArr[indexPath.row]];
    return cellHeight + 2 * kP(20);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
