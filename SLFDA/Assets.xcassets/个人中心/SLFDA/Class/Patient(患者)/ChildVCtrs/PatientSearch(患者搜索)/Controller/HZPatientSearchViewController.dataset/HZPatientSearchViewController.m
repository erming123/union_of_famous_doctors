//
//  HZPatientSearchViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/6.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientSearchViewController.h"
#import "HZSearchBar.h"
#import "HZOrderSubmitViewController.h"
//#import "HZExpertSiftCell.h"
#import "HZPatientListCell.h"
#import "HZExpertSearchTool.h"
#import "HZUser.h"
#import "HZUserTool.h"
//
#import "HZExpertHomeViewController.h"
#import "HZPatient.h"
//
#import "IQKeyboardManager.h"
//
#import "HZPatientCheckListViewController.h"
//
#import "HZPatientTool.h"
#import "HZUser.h"
@interface HZPatientSearchViewController ()<HZSearchBarDelegate, UITableViewDataSource, UITableViewDelegate, NSCacheDelegate>
//** <#注释#>*/
@property (nonatomic, weak) HZSearchBar *searchBar;
//** <#注释#>*/
@property (nonatomic, weak) HZTableView *searchTableView;
//** <#注释#>*/
@property (nonatomic, weak) HZPatientListCell *patientListCell;
//** <#注释#>*/
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
//
@property (nonatomic, strong) NSMutableArray *nameArr;
@property (nonatomic, strong) NSMutableArray *bedNumArr;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *searchResultStrArr;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *searchResultPatientArr;
@property (nonatomic, assign) BOOL isSearched;
@property (nonatomic, assign) BOOL isChangeTextNil;
//** <#注释#>*/
@property (nonatomic, weak) UIImageView *noDoctImageView;
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
@end

@implementation HZPatientSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpMySubViews];

}


#pragma mark -- setUpMySubViews
- (void)setUpMySubViews {
    
    CGFloat searBarX = kP(20);
    CGFloat searBarY = kP(14);
    CGFloat searBarW = HZScreenW - searBarX - kP(26);
    CGFloat searBarH = kP(56);
    HZSearchBar *searchBar = [[HZSearchBar alloc] initWithFrame:CGRectMake(searBarX, searBarY, searBarW, searBarH)];
    searchBar.delegate = self;
    searchBar.placeholder = @"请输入患者姓名或者床号";
    self.navigationItem.titleView = searchBar;
    self.searchBar = searchBar;
    
    //
    HZTableView *searchTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    searchTableView.dataSource = self;
    searchTableView.delegate = self;
    searchTableView.tableFooterView = [UIView new];
    [self.view addSubview:searchTableView];
    self.searchTableView = searchTableView;
    
    //
    CGFloat noDoctImageViewW = kP(340);
    CGFloat noDoctImageViewH = kP(376);
    CGFloat noDoctImageViewX = self.view.width * 0.5 - noDoctImageViewW * 0.5;
    CGFloat noDoctImageViewY = kP(600);
    UIImageView *noDoctImageView = [[UIImageView alloc] initWithFrame:CGRectMake(noDoctImageViewX, noDoctImageViewY, noDoctImageViewW, noDoctImageViewH)];
    noDoctImageView.image = [UIImage imageNamed:@"noDoct.png"];
    noDoctImageView.hidden = YES;
    [self.view addSubview:noDoctImageView];
    self.noDoctImageView = noDoctImageView;
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    //
    
    if (self.patientOriginArr.count == 0) return;
    
    self.nameArr = [NSMutableArray array];
    self.bedNumArr = [NSMutableArray array];
    for (HZPatient *patient in self.patientOriginArr) {
        [self.nameArr addObject:patient.name];
    }
    
    for (HZPatient *patient in self.patientOriginArr) {
        [self.bedNumArr addObject:patient.bedNum];
    }
    
    //
    NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
    self.user = user;
    
    //
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification1:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

#pragma mark -- setPatientOriginArr
- (void)setPatientOriginArr:(NSMutableArray *)patientOriginArr {
    _patientOriginArr = patientOriginArr;
}


#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchResultPatientArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HZPatientListCell *patientListCell = [HZPatientListCell cellWithTableView:tableView];
    
    HZPatient *patient = self.searchResultPatientArr[indexPath.row];
    patientListCell.isShowStarBtn = NO;
    //
    patientListCell.patient = patient;
    return patientListCell;
    
}

- (NSMutableArray *)searchResultStrArr {
    
    if (!_searchResultStrArr) {
        _searchResultStrArr = [NSMutableArray array];
    }
    
    return _searchResultStrArr;
}


- (NSMutableArray *)searchResultPatientArr {
    if (!_searchResultPatientArr) {
        _searchResultPatientArr = [NSMutableArray array];
    }
    
    return _searchResultPatientArr;
}

#pragma mark -- keyboardWillChangeFrameNotification1
- (void)keyboardWillChangeFrameNotification1:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    NSLog(@"------wo---%lf", keyboardRect.size.height);
    
    
    if (keyboardRect.origin.y == HZScreenH) {
        self.searchTableView.height = HZScreenH;
    } else {
        self.searchTableView.height = HZScreenH - keyboardRect.size.height;
    }
    
}

#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(120);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HZPatientCheckListViewController *patientCheckListVCtr = [HZPatientCheckListViewController new];
    patientCheckListVCtr.patient = self.searchResultPatientArr[indexPath.row];
    [self.navigationController pushViewController:patientCheckListVCtr animated:YES];
}

#pragma mark -- 去掉返回按钮
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationItem.hidesBackButton = YES;
    if (self.searchResultPatientArr.count == 0) {
        self.searchBar.isBecomeFirstResponder = YES;
    }
    
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    self.searchBar.isBecomeFirstResponder = NO;
}

#pragma mark -- HZSearchBarDelegate
- (void)cancelBtnClick:(HZSearchBar *)searBar {
    [self.navigationController popViewControllerAnimated:NO];
}

//- (void)getSearchResult:(HZSearchBar *)searBar {
//    
////    //
////    [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
////    
////    [[SDImageCache sharedImageCache] clearMemory];//可不写
////    
////    if ([NSString isBlankString:searBar.textFieldText] && self.expertArr.count != 0) {
////        searBar.isBecomeFirstResponder = NO;
////        //        return;
////    } else if (![NSString isBlankString:searBar.textFieldText]){
////        searBar.isBecomeFirstResponder = NO;
////        [self.activityIndicator startAnimating];
////        
////        NSLog(@"---------%@", searBar.textFieldText);
////        
////        
////        [HZExpertSearchTool getExpertArrWithDoctorName:searBar.textFieldText dictDepartmentId:nil isHotExpertSearch:NO page:1 countPerPage:1000 success:^(id responseObject) {
////            [self.activityIndicator stopAnimating];
////            
////            NSLog(@"-------dsf-----:%@", responseObject);
////            
////            if ([responseObject[@"msg"] isEqualToString:@"success"]) {
////                NSDictionary *resultDict = responseObject[@"results"];
////                self.expertArr = [HZUser mj_objectArrayWithKeyValuesArray:resultDict[@"doctors"]];
////                
////                //            HZUser *user = self.expertArr;
////                //                            NSLog(@"---------搜索成功----%@", self.expertArr);
////                //            NSLog(@"---------getDictJsonStr搜索成功----%@", self.expertArr[0]);
////                
////                self.noDoctImageView.hidden = self.expertArr.count != 0;
////                [self.searchTableView reloadData];
////            } else {
////                NSLog(@"---------搜索不成功----%@", responseObject[@"msg"]);
////            }
////        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
////            NSLog(@"-----搜索失败专家搜索----%@", error);
////        }];
////        
////    }
//    
//    
//}

- (void)changeText:(NSString *)changeText {
    
    if (self.patientOriginArr.count == 0) return;
    
    //    NSLog(@"------------+++++++++++++++++++==");
    
    //
    NSString *searchString = changeText;
    NSPredicate *preicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[c] %@", searchString];
    // 清空以前的数据
    if (self.searchResultStrArr!= nil) {
        [self.searchResultStrArr removeAllObjects];
    }
    if (self.searchResultPatientArr!= nil) {
        [self.searchResultPatientArr removeAllObjects];
    }
    
    //过滤数据
    if ([self isPureNumandCharacters:changeText]) { // 纯数字
        self.searchResultStrArr = [NSMutableArray arrayWithArray:[self.bedNumArr filteredArrayUsingPredicate:preicate]];
        NSLog(@"-----纯数字------------:%@", self.patientOriginArr);
        
        for (NSString *resultStr in self.searchResultStrArr) {
            
            
            //
            for (HZPatient *resultPatient in self.patientOriginArr) {
                
                if ([resultStr isEqualToString:resultPatient.bedNum]) {
                    [self.searchResultPatientArr addObject:resultPatient];
                    NSLog(@"9999------------");
                }
            }
            
            
        }

    } else {
        self.searchResultStrArr = [NSMutableArray arrayWithArray:[self.nameArr filteredArrayUsingPredicate:preicate]];
        NSLog(@"-----非纯数字------------:%@", self.searchResultStrArr);
        
        for (NSString *resultStr in self.searchResultStrArr) {
            
            for (HZPatient *resultPatient in self.patientOriginArr) {
                
                if ([resultStr isEqualToString:resultPatient.name]) {
                    [self.searchResultPatientArr addObject:resultPatient];
                }
            }
        }

    }
    
    NSLog(@"-----dddsss------------:%@", self.searchResultPatientArr);
    
    //刷新表格
    [self.searchTableView reloadData];
}

//- (void)changeTableViewHeight:(CGFloat)keyBoardH {
//    self.searchTableView.height = self.view.height - kP(600);
////    [self.searchTableView reloadData];
//}
- (BOOL)isPureNumandCharacters:(NSString *)string
{
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(string.length > 0)
    {
        return NO;
    }
    return YES;
}


#pragma mark -- dealloc
- (void)dealloc {
    //    [self.searchBar removeFromSuperview];
    //    self.searchBar = nil;
    self.searchBar.delegate = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
