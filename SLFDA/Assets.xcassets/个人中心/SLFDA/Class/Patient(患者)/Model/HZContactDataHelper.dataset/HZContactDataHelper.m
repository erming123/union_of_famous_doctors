//
//  HZContactDataHelper.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/5.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZContactDataHelper.h"
#import "HZPatient.h"
@implementation HZContactDataHelper

+ (NSMutableArray *) getStructuredPatientListDataBy:(NSMutableArray *)array {
    
    
    // 1. 先排序
    //resultArr
    NSMutableArray *resultArr = [[NSMutableArray alloc] init];
    
    NSArray *serializeArray = [(NSArray *)array sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        
        //1. 排序
        int i;
        NSString *strA = ((HZPatient *)obj1).namePinyinStr;
        NSString *strB = ((HZPatient *)obj2).namePinyinStr;
        
        
        
        // @"dsds", @"nihao", @"aiyou"...
        for (i = 0; i < strA.length && i < strB.length; i++) {
            char a = [strA characterAtIndex:i];
            char b = [strB characterAtIndex:i];
            
            //
            if (a > b) {
                return (NSComparisonResult)NSOrderedDescending;//上升 A在后面
            }
            else if (a < b) {
                return (NSComparisonResult)NSOrderedAscending;//下降 A在前面
            }
        }
        
        // 如果a和b的较少的所有元素比较，完全一致，那么谁的长，谁就靠后
        
        //
        if (strA.length > strB.length) {
            return (NSComparisonResult)NSOrderedDescending; //上升 A在后面
        }else if (strA.length < strB.length){
            return (NSComparisonResult)NSOrderedAscending; //下降 A在前面
        }else{
            return (NSComparisonResult)NSOrderedSame;// A,B相同
        }
    }];
    
    
    
    // 2. 排序结束后，再把拍好的数组分组
    //
    char lastC = '1'; // 随便定义一个字符而已
    NSMutableArray *dataArr;
    NSMutableArray *otherArr = [[NSMutableArray alloc] init];// 只有一个otherArr
    NSMutableArray *attention = [NSMutableArray array];
    
    for (HZPatient *patient in serializeArray) {
        
        // 首字母
        char capChar = [patient.namePinyinStr characterAtIndex:0];
        
        // 我的关注
        if (patient.starred) {
            [attention addObject:patient];
            continue;
        }
        
        // 是否是英文字母,ewewew, fdffd
        if (!isalpha(capChar)) { // 不是英文字母
            [otherArr addObject:patient];
        } else {
            
            if (capChar != lastC) { // 是英文字母， 如果字符属性位置不是'1'的， 检索首字母一样的
                lastC = capChar; //  lastC = 's'
                
                // 不进，此刻没有数组
                if (dataArr && dataArr.count > 0) { // 把上一个数组放进最终大数组里
                    [resultArr addObject:dataArr];
                }
                
                // 创建一个新的小数组， 把patient放进去
                dataArr = [[NSMutableArray alloc] init];
                [dataArr addObject:patient];
                
            } else if (capChar == lastC) { // 此时此字符等于上一个字符， 相同的字符。
                [dataArr addObject:patient];
            }
            
        }
    }
    
    // 把关注 放进去
    if (attention.count >0) {
        [resultArr insertObject:attention atIndex:0];
    }
    
    // 1. 把最后一个数组放进去
    if (dataArr && dataArr.count > 0) {
        [resultArr addObject:dataArr];
    }
    
    //2. 把最终的非英文首字母的数组放进去
    if (otherArr.count > 0) {
        [resultArr addObject:otherArr];
    }
    return resultArr;
}

+ (NSMutableArray *) getPatientListSectionBy:(NSMutableArray *)array {
    
    //
    NSMutableArray *section = [[NSMutableArray alloc] init];
    //    [section addObject:UITableViewIndexSearch];
    
    //
    for (NSArray *item in array) {
        
        
        // 筛选出我的关注
        HZPatient *patient = [item objectAtIndex:0];
        if (patient.starred) {
            
            [section addObject:@"☆"];
            continue;
        }
        
        // 筛选出首字母不是拼音字母的
        char c = [patient.namePinyinStr characterAtIndex:0];
        if (!isalpha(c)) {
            c = '#'; // 不是英文字符的就是#
        }
        // toupper: 大些字母， tolower，小写字母
        [section addObject:[NSString stringWithFormat:@"%c", toupper(c)]];
    }
    return section;
}

+ (NSMutableArray *) getSampleOrderPatientListBy:(NSMutableArray *)array orderType:(HZOrderType)orderType {
    
    
    //
    NSArray *orderPatientList;
    if (orderType == HZOrderTypeAscending) { // 升序
       orderPatientList  = [(NSArray*)array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            
            NSInteger bedNumA = [((HZPatient *)obj1).bedNum integerValue];
            NSInteger bedNumB = [((HZPatient *)obj2).bedNum integerValue];
            
            if (bedNumA > bedNumB) {
                return (NSComparisonResult)NSOrderedDescending; //上升 A在后面
            }else if (bedNumA < bedNumB){
                return (NSComparisonResult)NSOrderedAscending; //下降 A在前面
            }else{
                return (NSComparisonResult)NSOrderedSame;// A,B相同
            }
        }];
    } else if (orderType == HZOrderTypeDescending) {
        orderPatientList = [(NSArray*)array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            
            NSInteger bedNumA = [((HZPatient *)obj1).bedNum integerValue];
            NSInteger bedNumB = [((HZPatient *)obj2).bedNum integerValue];
            
            if (bedNumA > bedNumB) {
                return (NSComparisonResult)NSOrderedAscending;//下降 A在前面
            }else if (bedNumA < bedNumB){
                return (NSComparisonResult)NSOrderedDescending; //上升 A在后面
            }else{
                return (NSComparisonResult)NSOrderedSame;// A,B相同
            }
        }];
    }
   
    
    // 2. 排序结束后，再把拍好的数组分组
//    char lastC = '1'; // 随便定义一个字符而已
    
    NSMutableArray *otherArr = [[NSMutableArray alloc] init];// 只有一个otherArr
    NSMutableArray *attentionArr = [NSMutableArray array];
    
    for (HZPatient *patient in orderPatientList) {
        
      
        // 我的关注
        if (patient.starred) {
            [attentionArr addObject:patient];
            continue;
        }
        
        [otherArr addObject:patient];
      
    }
    
    
    NSMutableArray *resultArr = [NSMutableArray array];
    if (attentionArr.count) {
        [resultArr addObject:attentionArr];
    }
    
    if (otherArr.count) {
        [resultArr addObject:otherArr];
    }
    
    
    
//    // 把关注 放进去
//    if (attention.count >0) {
//        [resultArr insertObject:attention atIndex:0];
//    }
    
//    // 1. 把最后一个数组放进去
//    if (dataArr && dataArr.count > 0) {
//        [resultArr addObject:dataArr];
//    }
    
//    //2. 把最终的非英文首字母的数组放进去
//    if (otherArr.count > 0) {
//        [resultArr addObject:otherArr];
//    }
    return resultArr;
   
//    return orderPatientList;
}


+ (NSMutableArray *)getUserPatientListWithUserName:(NSString *)userName originDataList:(NSMutableArray *)array {
    
    NSMutableArray *resultArr = [NSMutableArray array];
    
    for (HZPatient *patient in array) {
        
        if ([patient.docName isEqualToString:userName]) {
            [resultArr addObject:patient];
        }
    }
    
    return resultArr;
}
@end
