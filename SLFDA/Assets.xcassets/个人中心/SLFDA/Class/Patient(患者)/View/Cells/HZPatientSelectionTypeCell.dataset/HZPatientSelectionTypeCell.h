//
//  HZPatientSelectionTypeCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/16.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZPatientSelectionTypeCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, copy) NSString *selTypeStr;
@end
