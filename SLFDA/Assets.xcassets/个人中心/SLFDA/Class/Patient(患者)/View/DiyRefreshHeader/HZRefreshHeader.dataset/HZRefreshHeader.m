//
//  HZRefreshHeader.m
//  SLFDA
//
//  Created by coder_xu on 2017/9/28.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZRefreshHeader.h"

@implementation HZRefreshHeader

#pragma mark - 重写方法
#pragma mark 基本设置
- (void)prepare
{
    [super prepare];
    
    // 设置普通状态的动画图片
    NSMutableArray *idleImages = [NSMutableArray array];
    for (NSUInteger i = 0; i<=13; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"Comp 2_0000%zd", i]];
        [idleImages addObject:image];
    }
    [self setImages:idleImages forState:MJRefreshStateIdle];
    
    UIImage *image =  [UIImage imageNamed:@"Comp 2_000013"];
    NSArray *state2 = @[image];
    [self setImages:state2 forState:MJRefreshStatePulling];
    
    // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
    NSMutableArray *refreshingImages = [NSMutableArray array];
    for (NSUInteger i = 13; i<=24; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"Comp 2_0000%zd", i]];
        [refreshingImages addObject:image];
    }
    // 设置正在刷新状态的动画图片
    [self setImages:refreshingImages forState:MJRefreshStateRefreshing];
    
    //
    [self setTitle:@"下拉刷新页面" forState:MJRefreshStateIdle];
    [self setTitle:@"松开立即刷新" forState:MJRefreshStatePulling];
    [self setTitle:@"页面正在刷新" forState:MJRefreshStateRefreshing];
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.gifView.x = kP(70);
    self.stateLabel.x = CGRectGetMaxX(self.gifView.frame) + kP(16);
    self.stateLabel.width = HZScreenW - self.gifView.x-self.gifView.width - kP(16);
    self.stateLabel.textAlignment = NSTextAlignmentLeft;
    self.lastUpdatedTimeLabel.hidden = YES;
   
    // Set font
    self.stateLabel.font = [UIFont systemFontOfSize:kP(32)];
    // Set textColor
    self.stateLabel.textColor = [UIColor colorWithHexString:@"#2DBED8"];
}


@end
