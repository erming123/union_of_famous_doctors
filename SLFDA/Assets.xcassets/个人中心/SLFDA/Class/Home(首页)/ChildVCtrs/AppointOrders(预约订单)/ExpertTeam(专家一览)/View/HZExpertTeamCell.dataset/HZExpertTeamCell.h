//
//  HZExpertTeamCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/21.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;
@interface HZExpertTeamCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//** <#注释#>*/
@property (nonatomic, strong) HZUser *expert;
@property (nonatomic, copy) NSArray *priceList;
@end
