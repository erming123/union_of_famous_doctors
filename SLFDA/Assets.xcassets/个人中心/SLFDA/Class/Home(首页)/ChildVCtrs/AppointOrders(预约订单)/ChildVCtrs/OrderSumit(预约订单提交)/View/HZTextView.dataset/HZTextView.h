//
//  HZTextView.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZTextViewDelegate <NSObject>
- (void)passTextViewStateDidBegin:(CGFloat)keyBoardH;
- (void)passTextViewStateClickReturn:(NSString *)textViewText;
@end
@interface HZTextView : UIView
@property (nonatomic, weak) id<HZTextViewDelegate>delegate;
@property (nonatomic, assign) BOOL becomeFirstResponder;
@end
