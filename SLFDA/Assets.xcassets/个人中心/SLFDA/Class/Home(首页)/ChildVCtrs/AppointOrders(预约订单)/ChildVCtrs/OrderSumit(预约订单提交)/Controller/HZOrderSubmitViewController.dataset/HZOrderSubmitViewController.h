//
//  HZOrderSubmitViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;
@interface HZOrderSubmitViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZUser *expert;
@property (nonatomic, copy) NSArray *priceList;
@end
