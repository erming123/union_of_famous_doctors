//
//  HZPatientSummitInforCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientSummitInforCell.h"
#import "HZPatient.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZPatientSummitInforCell () <UITextFieldDelegate>
@property (nonatomic, weak) UILabel *patientNameLabel;
@property (nonatomic, weak) UITextField *patientNameTextField;
@property (nonatomic, weak) UILabel *patientGenderLabel;
@property (nonatomic, weak) UITextField *patientGenderTextField;
@property (nonatomic, weak) UILabel *patientAgeLabel;
@property (nonatomic, weak) UITextField *patientAgeTextField;
@end
NS_ASSUME_NONNULL_END
@implementation HZPatientSummitInforCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    
    //
    UILabel *patientNameLabel = [UILabel new];
    patientNameLabel.attributedText = [@"姓名 *" changeContentColorWithString:@"*" color:[UIColor redColor]];
//    patientNameLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:patientNameLabel];
    self.patientNameLabel = patientNameLabel;
    //
    UITextField *patientNameTextField = [UITextField new];
//    patientNameTextField.backgroundColor = [UIColor greenColor];
    patientNameTextField.textAlignment = NSTextAlignmentRight;
    patientNameTextField.placeholder = @"请输入患者的姓名";
    patientNameTextField.delegate = self;
    
//    [patientNameTextField addTarget:self action:@selector(typeInTextField:) forControlEvents:UIControlEventEditingDidBegin];
    [self.contentView addSubview:patientNameTextField];
    self.patientNameTextField = patientNameTextField;
    
    
    //
    UILabel *patientGenderLabel = [UILabel new];
    patientGenderLabel.attributedText = [@"性别 *" changeContentColorWithString:@"*" color:[UIColor redColor]];
//    patientGenderLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:patientGenderLabel];
    self.patientGenderLabel = patientGenderLabel;
    //
    UITextField *patientGenderTextField = [UITextField new];
//    patientGenderTextField.backgroundColor = [UIColor greenColor];
    patientGenderTextField.textAlignment = NSTextAlignmentRight;
    patientGenderTextField.placeholder = @"请输入患者的性别";
    patientGenderTextField.delegate = self;
    
//    [patientGenderTextField addTarget:self action:@selector(typeInTextField:) forControlEvents:UIControlEventEditingDidBegin];
    [self.contentView addSubview:patientGenderTextField];
    self.patientGenderTextField = patientGenderTextField;
    
    //
    UILabel *patientAgeLabel = [UILabel new];
    patientAgeLabel.attributedText = [@"年龄 *" changeContentColorWithString:@"*" color:[UIColor redColor]];
//    patientAgeLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:patientAgeLabel];
    self.patientAgeLabel = patientAgeLabel;
    //
    UITextField *patientAgeTextField = [UITextField new];
//    patientAgeTextField.backgroundColor = [UIColor greenColor];
    patientAgeTextField.textAlignment = NSTextAlignmentRight;
    patientAgeTextField.placeholder = @"请输入患者的年龄";
    patientAgeTextField.delegate = self;
    
//    [patientAgeTextField addTarget:self action:@selector(typeInTextField:) forControlEvents:UIControlEventEditingDidBegin];
    [self.contentView addSubview:patientAgeTextField];
    self.patientAgeTextField = patientAgeTextField;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];

    //2.
    self.patientNameLabel.x = kP(28);
    self.patientNameLabel.y = kP(29);
    [self.patientNameLabel sizeToFit];
    
    self.patientNameTextField.x = CGRectGetMaxX(self.patientNameLabel.frame) + kP(30);
    self.patientNameTextField.y = self.patientNameLabel.y;
    self.patientNameTextField.width = HZScreenW - self.patientNameTextField.x - self.patientNameLabel.x;
    self.patientNameTextField.height = self.patientNameLabel.height;
    
    //3.
    self.patientGenderLabel.x = self.patientNameLabel.x;
    self.patientGenderLabel.y = CGRectGetMaxY(self.patientNameLabel.frame) + 2 * self.patientNameTextField.y;
    [self.patientGenderLabel sizeToFit];
    
    self.patientGenderTextField.x = self.patientNameTextField.x;
    self.patientGenderTextField.y = self.patientGenderLabel.y;
    self.patientGenderTextField.width = self.patientNameTextField.width;
    self.patientGenderTextField.height = self.patientNameTextField.height;
    
    //4.
    self.patientAgeLabel.x = self.patientGenderLabel.x;
    self.patientAgeLabel.y = CGRectGetMaxY(self.patientGenderTextField.frame) + 2 * self.patientNameLabel.y;
    [self.patientAgeLabel sizeToFit];
    
    self.patientAgeTextField.x = self.patientGenderTextField.x;
    self.patientAgeTextField.y = self.patientAgeLabel.y;
    self.patientAgeTextField.width = self.patientGenderTextField.width;
    self.patientAgeTextField.height = self.patientGenderTextField.height;
}

- (void)setSubViewsContent {
    self.patientNameTextField.text = self.patient.BRDAXM;
    if (self.patient.BRDAXB.length == 0) {
        self.patientGenderTextField.text = @"";
    } else {
        self.patientGenderTextField.text = [self.patient.BRDAXB isEqualToString:@"1"] ? @"男" : @"女";
    }
    self.patientAgeTextField.text = [self getAgeStr];
}


- (NSString *)getAgeStr {
    //1.
    NSDate *now = [NSDate date];
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"yyyy"];//
    
    NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
    [df2 setDateFormat:@"MMdd"];//MMdd
    
    NSString *yearStrNew = [df1 stringFromDate:now];
    NSString *monthStrNew = [df2 stringFromDate:now];
    
    //
    NSString *ageStr;
    NSInteger year = 0;
    NSInteger month = 0;
    if (self.patient.BRCSRQ.length == 0) {
        ageStr = @"";
    } else {
        NSString *yearStr = self.patient.BRCSRQ.length > 0 ? [self.patient.BRCSRQ substringWithRange:NSMakeRange(0, 4)] : @"";
        NSString *monthStr = self.patient.BRCSRQ.length > 0 ? [self.patient.BRCSRQ substringWithRange:NSMakeRange(5, 2)] : @"";
        NSString *dayStr = self.patient.BRCSRQ.length > 0 ? [self.patient.BRCSRQ substringWithRange:NSMakeRange(8, 2)] : @"";
        NSString *MDStr = [NSString stringWithFormat:@"%@%@", monthStr, dayStr];
        year = [yearStrNew intValue] - [yearStr intValue];
        month = [monthStrNew intValue] - [MDStr intValue];
        if (month < 0) {
            year -= 1;
        }
        ageStr = [NSString stringWithFormat:@"%ld岁", year];
    }
    
    return ageStr;
}


- (void)setPatient:(HZPatient *)patient {
    _patient = patient;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"PatientSummitInforCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (self.patientNameTextField.text.length == 0){
        [self.patientNameTextField becomeFirstResponder];
    } else if (self.patientGenderTextField.text.length == 0) {
        [self.patientGenderTextField becomeFirstResponder];
    }else if (self.patientAgeTextField.text.length == 0){
        [self.patientAgeTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    if (self.patientNameTextField.text.length != 0 && self.patientGenderTextField.text.length != 0 && self.patientAgeTextField.text.length != 0) {
        
        NSDictionary *paramDict = @{@"patientName" : self.patientNameTextField.text,
                                    @"patientGender" : self.patientGenderTextField.text,
                                    @"patientAge" : [NSString stringWithFormat:@"%@岁", self.patientAgeTextField.text]
                                    };
        [[NSNotificationCenter defaultCenter] postNotificationName:@"submitBtnShouldEnable" object:paramDict];
    }
    return YES;
}


@end
