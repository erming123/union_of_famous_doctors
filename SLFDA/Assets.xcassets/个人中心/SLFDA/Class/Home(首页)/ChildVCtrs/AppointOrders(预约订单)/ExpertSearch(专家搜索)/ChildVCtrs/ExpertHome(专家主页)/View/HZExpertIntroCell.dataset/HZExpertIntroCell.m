//
//  HZExpertIntroCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertIntroCell.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZExpertIntroCell ()
@property (nonatomic, weak) UIView *bgView;
@property (nonatomic, weak) UIImageView *introImageView;
@property (nonatomic, weak) UILabel *introLabel;
@property (nonatomic, weak) UIButton *unFoldBtn;
@property (nonatomic, weak) UILabel *unFoldLabel;
@property (nonatomic, assign) CGFloat cellHeight;
@end
NS_ASSUME_NONNULL_END
@implementation HZExpertIntroCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"f3f3f3" alpha:1.0];
    }
    
    return self;
}

- (void)setUpSubViews {
    
    //1.
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    self.bgView = bgView;
    
    
    //2.
    UIImageView *introImageView = [UIImageView new];
//    introImageView.backgroundColor = [UIColor greenColor];
    [bgView addSubview:introImageView];
    self.introImageView = introImageView;
    
    
    //3.
    UILabel *introLabel = [UILabel new];
//    introLabel.backgroundColor = [UIColor yellowColor];
    [bgView addSubview:introLabel];
    self.introLabel = introLabel;
    
    
    //4.
    UIButton *unFoldBtn = [UIButton new];
//    unFoldBtn.backgroundColor = [UIColor redColor];
    unFoldBtn.transform = CGAffineTransformMakeRotation(M_PI_2);
    [unFoldBtn addTarget:self action:@selector(unFoldBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:unFoldBtn];
    self.unFoldBtn = unFoldBtn;
    
    
    //5.
    UILabel *unFoldLabel = [UILabel new];
//    unFoldLabel.backgroundColor = [UIColor yellowColor];
    unFoldLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
    [bgView addSubview:unFoldLabel];
    self.unFoldLabel = unFoldLabel;
    
   
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    //
    CGFloat introImageViewX = kP(28);
    CGFloat introImageViewY = kP(44);
    CGFloat introImageViewWH = kP(40);
    self.introImageView.frame = CGRectMake(introImageViewX, introImageViewY, introImageViewWH, introImageViewWH);
    
    self.introLabel.x = CGRectGetMaxX(self.introImageView.frame) + kP(14);
    self.introLabel.y = CGRectGetMidY(self.introImageView.frame) - self.introLabel.height * 0.5;
    
    //
    CGFloat unFoldBtnW = kP(100);
    CGFloat unFoldBtnH = kP(100);
    CGFloat unFoldBtnX = self.width - kP(42) - unFoldBtnW;
    CGFloat unFoldBtnY = CGRectGetMidY(self.introLabel.frame) - unFoldBtnH * 0.5;
    self.unFoldBtn.frame = CGRectMake(unFoldBtnX, unFoldBtnY, unFoldBtnW, unFoldBtnH);
    
    //
    //
    self.unFoldLabel.numberOfLines = 0;//5,r
    CGFloat unFoldLabelX = self.introLabel.x;
    CGFloat unFoldLabelY = CGRectGetMaxY(self.introLabel.frame) + kP(26);
    
    NSLog(@"----unFoldLabelY-----%lf", unFoldLabelY);
    //
    CGFloat unFoldLabelW = self.width - 2 * unFoldLabelX + kP(25);
    CGFloat unFoldLabelH = [self.unFoldLabel sizeThatFits:CGSizeMake(unFoldLabelW, MAXFLOAT)].height;
    
    //
    NSLog(@"-----wwww----%lf", unFoldLabelH);
    if (unFoldLabelH > kP(32) * 5) {
        NSLog(@"---------wwwweeeeeeooooo大于");
        
        self.unFoldBtn.hidden = NO;
        
        unFoldLabelH = self.unFoldBtn.selected ? unFoldLabelH :  kP(32) * 5;
        self.unFoldLabel.numberOfLines = self.unFoldBtn.selected ? 0 : 4;
        
        unFoldLabelY = self.unFoldBtn.selected ? unFoldLabelY : unFoldLabelY - (kP(4));
        
        
    } else {
        NSLog(@"---------wwwweeeeeeooooo小于");
        self.unFoldBtn.hidden = YES;
        unFoldLabelY = unFoldLabelY - kP(5);
        self.unFoldLabel.numberOfLines = 0;
    }
    //
    self.unFoldLabel.frame = CGRectMake(unFoldLabelX, unFoldLabelY, unFoldLabelW, unFoldLabelH);
    
    
    //2.
    CGFloat bgViewX = kP(0);
    CGFloat bgViewY = kP(0);
    CGFloat bgViewW = self.width;
    CGFloat bgViewH = CGRectGetMaxY(self.unFoldLabel.frame) + kP(26);
    
    //    if (self.unFoldBtn.hidden) {
    //        bgViewH = kP(32) * 5 + kP(26);
    //    }
    self.bgView.frame = CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH);
    
   
    //
    self.cellHeight = CGRectGetMaxY(self.bgView.frame) + kP(20);
}



- (void)setSubViewsContent {
    
    self.introImageView.image = [UIImage imageNamed:@"expertIntro"];
    self.introLabel.text = @"简介";
    self.introLabel.font = [UIFont systemFontOfSize:kP(32)];
    [self.introLabel sizeToFit];
    //
    [self.unFoldBtn setImage:[UIImage imageNamed:@"allExpertEnter"] forState:UIControlStateNormal];
    self.unFoldLabel.text = self.expertIntroStr;
    self.unFoldLabel.font = [UIFont systemFontOfSize:kP(32)];
}

#pragma mark -- set
-(void)setExpertIntroStr:(NSString *)expertIntroStr {
    _expertIntroStr = expertIntroStr;
}

#pragma mark -- unFoldBtnClick

- (void)unFoldBtnClick:(UIButton *)unFoldBtn {
    
    unFoldBtn.selected = !unFoldBtn.selected;
    [self layoutSubviews];
    NSLog(@"---------BOOL------%@", unFoldBtn.selected == YES ? @"YES":@"NO");
    
//    self.isTapped = !self.isTapped;
    unFoldBtn.transform = unFoldBtn.selected ? CGAffineTransformMakeRotation(- M_PI_2) : CGAffineTransformMakeRotation(M_PI_2);
    
//    NSLog(@"----self.cellHeight-----%lf", self.cellHeight);
    if (self.delegate && [self.delegate respondsToSelector:@selector(setCellHeightWithIsIntrolUnFold: cellHeight:)]) {
        [self.delegate setCellHeightWithIsIntrolUnFold:unFoldBtn.selected cellHeight:self.cellHeight];
    }
    
}
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"IntroCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
