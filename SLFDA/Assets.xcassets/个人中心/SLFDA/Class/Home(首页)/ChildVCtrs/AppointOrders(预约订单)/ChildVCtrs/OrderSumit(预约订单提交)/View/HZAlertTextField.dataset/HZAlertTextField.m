//
//  HZAlertTextField.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZAlertTextField.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZAlertTextField ()<UITextFieldDelegate>
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *topBgView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UIButton *ensureBtn;
@property (nonatomic, assign) CGFloat keyBoardH;
@property (nonatomic, weak) UIButton *scanBtn;
@end
NS_ASSUME_NONNULL_END
@implementation HZAlertTextField

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews {
    
    //0.
    self.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.5
                            ];
    
    //1.
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithHexString:@"#030303" alpha:0.05];
    [self addSubview:bgView];
    self.bgView = bgView;
    
    //3.
    UIView *topBgView = [UIView new];
    topBgView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    [self.bgView addSubview:topBgView];
    self.topBgView = topBgView;
    
    //3.
    UILabel *titleLabel = [UILabel new];
    [self.topBgView addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    //4.
    UITextField *textField = [UITextField new];
    textField.backgroundColor = [UIColor colorWithHexString:@"#030303" alpha:0.09];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.inputAccessoryView = [[UIView alloc] init];
    [self.topBgView addSubview:textField];
    textField.delegate = self;
    self.textField = textField;
    
    
    //
    UIImage *norImage = [UIImage createImageWithColor:[UIColor colorWithWhite:1.0 alpha:0.9]];
    UIImage *selImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#F6F6F6" alpha:0.1]];
    //5.
    UIButton *cancelBtn = [UIButton new];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setBackgroundImage:norImage forState:UIControlStateNormal];
    [cancelBtn setBackgroundImage:selImage forState:UIControlStateSelected];
    [cancelBtn setTitleColor:[UIColor colorWithHexString:@"#666666" alpha:1.0] forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:cancelBtn];
    self.cancelBtn = cancelBtn;
    
    //6.
    UIButton *ensureBtn = [UIButton new];
    [ensureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [ensureBtn setBackgroundImage:norImage forState:UIControlStateNormal];
    [ensureBtn setBackgroundImage:selImage forState:UIControlStateSelected];
    [ensureBtn setTitleColor:[UIColor colorWithHexString:@"#17B5CC" alpha:1.0] forState:UIControlStateNormal];
    ensureBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [ensureBtn addTarget:self action:@selector(ensureBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:ensureBtn];
    self.ensureBtn = ensureBtn;
    
    //扫描按钮
    UIButton *scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [scanBtn setImage:[UIImage imageNamed:@"cardScan"] forState:UIControlStateNormal];
    [scanBtn addTarget:self action:@selector(scanBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.textField.rightView = scanBtn;
    self.textField.rightViewMode = UITextFieldViewModeUnlessEditing;
    self.scanBtn = scanBtn;
    //4.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeAlertFrameNotification:) name:UIKeyboardWillShowNotification object:nil];
    
    
    
}

-(void)setNeedScan:(BOOL)needScan{
    _needScan = needScan;
    self.textField.rightViewMode = needScan?UITextFieldViewModeUnlessEditing:UITextFieldViewModeNever;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //0.
    [self setSubViewsContent];
    
    
    //1.
    self.size = [UIScreen mainScreen].bounds.size;
    
    
    //2.
    
    NSLog(@"-------------------%lf", self.keyBoardH);
    CGFloat bgViewX = kP(90);
    CGFloat bgViewY = kP(398);
    CGFloat bgViewW = self.width - 2 * bgViewX;
    CGFloat bgViewH = kP(334);
    self.bgView.frame = CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH);
    self.bgView.layer.cornerRadius = kP(12);
    self.bgView.clipsToBounds = YES;
    
    //3.
    CGFloat topBgViewX = 0;
    CGFloat topBgViewY = 0;
    CGFloat topBgViewW = self.bgView.width;
    CGFloat topBgViewH = kP(238);
    self.topBgView.frame = CGRectMake(topBgViewX, topBgViewY, topBgViewW, topBgViewH);
    
    //4.
    self.titleLabel.x = self.topBgView.width * 0.5 - self.titleLabel.width * 0.5;
    self.titleLabel.y = kP(38);
    
    //5.
    CGFloat textFieldX = kP(64);
    CGFloat textFieldY = CGRectGetMaxY(self.titleLabel.frame) + kP(40);
    CGFloat textFieldW = self.topBgView.width - 2 * textFieldX;
    CGFloat textFieldH = kP(70);
    self.textField.frame = CGRectMake(textFieldX, textFieldY, textFieldW, textFieldH);
    
    
    //6.
    CGFloat cancelBtnX = 0;
    CGFloat cancelBtnY = CGRectGetMaxY(self.topBgView.frame) + kP(2);
    CGFloat cancelBtnW = self.bgView.width * 0.5 - kP(2) * 0.5;
    CGFloat cancelBtnH = self.bgView.height - self.topBgView.height - kP(2);
    self.cancelBtn.frame = CGRectMake(cancelBtnX, cancelBtnY, cancelBtnW, cancelBtnH);
    
    
    //2.
    CGFloat ensureBtnX = CGRectGetMaxX(self.cancelBtn.frame) + kP(2);
    CGFloat ensureBtnY = cancelBtnY;
    CGFloat ensureBtnW = cancelBtnW;
    CGFloat ensureBtnH = cancelBtnH;
    self.ensureBtn.frame = CGRectMake(ensureBtnX, ensureBtnY, ensureBtnW, ensureBtnH);
    
    //scanBtnFrame
    self.scanBtn.frame = CGRectMake(0, 0, self.textField.height, self.textField.height);
    
}

- (void)setSubViewsContent {
    
    self.titleLabel.text = _title;
    [self.titleLabel sizeToFit];
    //    self.<#label#>.text = @"<##>";
    //    self.<#label#>.text = @"<##>";
}
// 重写set方法
-(void)setTextFieldText:(NSString *)textFieldText{
    
    self.textField.text = textFieldText;
}


- (void)setTitle:(NSString *)title {
    _title = title;
}

- (NSString *)textFieldText {
    return self.textField.text;
}


- (void)setIsKeyboardNumberType:(BOOL)isKeyboardNumberType {
    self.textField.keyboardType = isKeyboardNumberType ? UIKeyboardTypeNumberPad : UIKeyboardTypeDefault;
}
#pragma mark -- isBecomeFirstResponder

- (void)setIsBecomeFirstResponder:(BOOL)isBecomeFirstResponder {
    
    NSLog(@"----self.textField-----%lf", self.textField.y);
    if (self.textField) {
        isBecomeFirstResponder ? [self.textField becomeFirstResponder] : [self.textField resignFirstResponder];
    }
    
}


#pragma mark -- keyboardWillChangeFrameNotification
- (void)keyboardWillChangeAlertFrameNotification:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //    self.keyBoardH = keyboardRect.size.height;
    
    NSLog(@"------yui---%lf", keyboardRect.size.height);
    
    
    //    NSLog(@"---------BOOL------%@", kP(398) >= self.height - keyboardRect.size.height == YES ? @"YES":@"NO");
    
    //    self.keyBoardH = keyboardRect.size.height;
    
    if (CGRectGetMaxY(self.bgView.frame) >= self.height - keyboardRect.size.height) {
        
        NSLog(@"--00=-----");
        self.bgView.y = self.height - keyboardRect.size.height - kP(334) - kP(20);
    }
    //    else {
    //        self.bgView.y = kP(398);
    //
    //        NSLog(@"--11=-----");
    //    }
    
    
    
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -- cancelBtnClick
- (void)cancelBtnClick:(UIButton *)cancelBtn {
    //    cancelBtn.selected = !cancelBtn.selected;
    //    NSLog(@"---------asasa");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(hideAlertTextField:)]) {
        [self.delegate hideAlertTextField:self];
        [self.textField resignFirstResponder];
    }
}
#pragma mark -- ensureBtnClick

- (void)ensureBtnClick:(UIButton *)ensureBtn {
    //    ensureBtn.selected = !ensureBtn.selected;
    //    NSLog(@"---------asaaaa");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(getPatientInfor:)]) {
        [self.delegate getPatientInfor:self];
        [self.textField resignFirstResponder];
    }
}
#pragma mark -
#pragma mark--scanBtnClick--
-(void)scanBtnClick:(UIButton*)scanbtn{
    
    //跳转
    //事件传出去;
    if ([self.delegate respondsToSelector:@selector(scanBtnClickAction)]) {
        [self.delegate scanBtnClickAction];
    }
    
    
}


@end
