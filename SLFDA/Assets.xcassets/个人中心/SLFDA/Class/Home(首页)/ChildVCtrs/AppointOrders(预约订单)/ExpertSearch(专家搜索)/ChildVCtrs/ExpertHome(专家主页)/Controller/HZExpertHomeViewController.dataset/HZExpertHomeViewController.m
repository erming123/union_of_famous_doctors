//
//  HZExpertHomeViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertHomeViewController.h"
#import "HZExpertSkillCell.h"
#import "HZExpertIntroCell.h"
#import "HZOrderSubmitViewController.h"
#import "HZMessageViewController.h"
#import "HZUser.h"

//
@interface HZExpertHomeViewController ()<UITableViewDataSource, UITableViewDelegate, HZExpertSkillCellDelegate, HZExpertIntrolCellDelegate, UIGestureRecognizerDelegate, UINavigationControllerDelegate>
////** <#注释#>*/
@property (nonatomic, strong) UIImageView *bgImageView;
////** <#注释#>*/
@property (nonatomic, strong) HZTableView *expertInforTableView;
////** <#注释#>*/
@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;
////** <#注释#>*/
//@property (nonatomic, strong) HZExpertSkillCell *expertSkillCell;
////** <#注释#>*/
//@property (nonatomic, strong) HZExpertIntroCell *expertIntroCell;
@property (nonatomic, assign) BOOL isSkillUnFold;
@property (nonatomic, assign) BOOL isIntroUnFold;
@property (nonatomic, assign) CGFloat skillDefaultCellHeight;
@property (nonatomic, assign) CGFloat introDefaultCellHeight;
@property (nonatomic, assign) CGFloat skillCellHeight;
@property (nonatomic, assign) CGFloat introCellHeight;
//** 会诊*/
@property (nonatomic, strong) UIButton *consultBtn;
//** 留言*/
@property (nonatomic, strong) UIButton *messageBtn;
//
////** <#注释#>*/
@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactiveTransition;
@end

@implementation HZExpertHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //
    
    self.view.backgroundColor = [UIColor whiteColor];
    //    //
    self.expert.speciality = [self.expert.speciality stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.expert.honor = [self.expert.honor stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.skillDefaultCellHeight = [self getDefaultCellheightWithText:self.expert.speciality];
    self.introDefaultCellHeight = [self getDefaultCellheightWithText:self.expert.honor];
    
    NSLog(@"---------skillDefaultCellHeight:%lf-----introDefaultCellHeight:%lf", self.skillDefaultCellHeight, self.introDefaultCellHeight);
    
    NSLog(@"--qwrt-:%@", self.expert.authenticationState);
    
    //---------------------------------------------------------------------------
    [self setUpHeadTopSubViews];
    //
    [self setUpExpertInforTableView];
    //
    [self setUpBottomBtnsSubViews];
    //
    
}


#pragma mark -- set
- (void)setExpert:(HZUser *)expert {
    _expert = expert;
}

-(void)setExpertAvatar:(UIImage *)expertAvatar {
    _expertAvatar = expertAvatar;
}

- (void)setUser:(HZUser *)user {
    _user = user;
}
#pragma mark -- setUpHeadTopSubViews
- (void)setUpHeadTopSubViews {
    
    //1.bgImageView
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), self.view.width, kP(344))];
    bgImageView.image = [UIImage imageNamed:@"expertBg.png"];
    bgImageView.userInteractionEnabled = YES;
    //    bgImageView.backgroundColor = [UIColor redColor];
    [self.view addSubview:bgImageView];
    self.bgImageView = bgImageView;
    
    //2.
    CGFloat backBtnX = kP(5);
    CGFloat backBtnY = kP(76);
    CGFloat backBtnW = kP(80);
    CGFloat backBtnH = kP(75);
    UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(backBtnX, backBtnY, backBtnW, backBtnH)];
    [backBtn setImage:[self imageWithColor:[UIColor whiteColor] AndImage:[UIImage imageNamed:@"back_black.png"]] forState:UIControlStateNormal];
    [backBtn setImageEdgeInsets:UIEdgeInsetsMake(kP(0), kP(30), kP(0), kP(0))];
    // backBtn.backgroundColor = [UIColor yellowColor];
    [backBtn addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    //3.
    CGFloat expertAvatarImageViewWH = kP(124);
    CGFloat expertAvatarImageViewX = self.view.width * 0.5 - expertAvatarImageViewWH * 0.5;
    CGFloat expertAvatarImageViewY = kP(72);
    UIImageView *expertAvatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(expertAvatarImageViewX, expertAvatarImageViewY, expertAvatarImageViewWH, expertAvatarImageViewWH)];
    expertAvatarImageView.backgroundColor = [UIColor whiteColor];
    expertAvatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    expertAvatarImageView.layer.cornerRadius = expertAvatarImageViewWH * 0.5;
    expertAvatarImageView.layer.borderWidth = kP(2);
    expertAvatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    expertAvatarImageView.clipsToBounds = YES;
    
    //    expertAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
    [expertAvatarImageView getPictureWithFaceUrl:self.expert.facePhotoUrl AndPlacehoderImage:@"expertPlaceImg"];
    [self.view addSubview:expertAvatarImageView];
    
    //4.
    UILabel *nameLabel = [UILabel new];
    nameLabel.text = [NSString isBlankString:self.expert.userName] ? @"" : self.expert.userName;
    nameLabel.font = [UIFont systemFontOfSize:kP(32)];
    nameLabel.textColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    [nameLabel sizeToFit];
    nameLabel.x = self.view.width * 0.5 - nameLabel.width * 0.5;
    nameLabel.y = CGRectGetMaxY(expertAvatarImageView.frame) + kP(20);
    [self.view addSubview:nameLabel];
    
    //5.
    UILabel *subjectLabel = [UILabel new];
    subjectLabel.text = [NSString isBlankString:self.expert.departmentGeneralName] ? @"" : self.expert.departmentGeneralName;
    subjectLabel.font = [UIFont systemFontOfSize:kP(26)];
    subjectLabel.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    subjectLabel.backgroundColor = [UIColor colorWithHexString:@"#F59322"];
    [subjectLabel sizeToFit];
    subjectLabel.height += kP(10);
    subjectLabel.width = kP(124);
    subjectLabel.y = CGRectGetMaxY(nameLabel.frame) + kP(24);
    subjectLabel.textAlignment = NSTextAlignmentCenter;
    subjectLabel.layer.cornerRadius = subjectLabel.height * 0.5;
    subjectLabel.clipsToBounds = YES;
    [self.view addSubview:subjectLabel];
    
    //6.
    UILabel *jobLabel = [UILabel new];
    jobLabel.text = [NSString isBlankString:self.expert.titleName] ? @"" : self.expert.titleName;
    jobLabel.font = [UIFont systemFontOfSize:kP(26)];
    jobLabel.backgroundColor = [UIColor colorWithHexString:@"#228CF5"];
    jobLabel.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    [jobLabel sizeToFit];
    jobLabel.height += kP(10);
    jobLabel.width += jobLabel.height;
    jobLabel.y = subjectLabel.y;
    jobLabel.textAlignment = NSTextAlignmentCenter;
    jobLabel.layer.cornerRadius = jobLabel.height * 0.5;
    jobLabel.clipsToBounds = YES;
    [self.view addSubview:jobLabel];
    
    //
    subjectLabel.x = (self.view.width - subjectLabel.width - jobLabel.width - kP(24)) * 0.5;
    jobLabel.x = CGRectGetMaxX(subjectLabel.frame) + kP(24);
}


#pragma mark -- setUpExpertInforTableView
- (void)setUpExpertInforTableView {
    
    HZTableView *expertInforTableView = [[HZTableView alloc] initWithFrame:CGRectMake(kP(0), CGRectGetMaxY(self.bgImageView.frame), self.view.width, self.view.height - kP(344)) style:UITableViewStylePlain];
    expertInforTableView.dataSource = self;
    expertInforTableView.delegate = self;
    expertInforTableView.tableFooterView = [UIView new];
    expertInforTableView.backgroundColor = [UIColor colorWithHexString:@"f3f3f3" alpha:1.0];
    expertInforTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:expertInforTableView];
    self.expertInforTableView = expertInforTableView;
    self.view.backgroundColor = expertInforTableView.backgroundColor;
}


#pragma mark -- setUpBottomBtnsSubViews
- (void)setUpBottomBtnsSubViews {
    
    //
    CGFloat btnBgViewW = self.view.width;
    CGFloat btnBgViewH = kP(128);
    CGFloat btnBgViewX = kP(0);
    CGFloat btnBgViewY = HZScreenH - btnBgViewH - SafeAreaBottomHeight;
    UIView *btnBgView = [[UIView alloc] initWithFrame:CGRectMake(btnBgViewX, btnBgViewY, btnBgViewW, btnBgViewH)];
    btnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    btnBgView.layer.shadowOffset = CGSizeMake(0, - kP(2));
    btnBgView.layer.shadowOpacity = 0.1;
    btnBgView.layer.shadowRadius = kP(2);
    btnBgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:btnBgView];
    
    
    BOOL isLocalDoctor = YES;
    
    //
    CGFloat messageBtnX = kP(30);
    CGFloat messageBtnY = kP(20);
    CGFloat messageBtnW = (self.view.width - 3 * messageBtnX) * 0.5;
    CGFloat messageBtnH = btnBgView.height - 2 * messageBtnY;
    UIButton *messageBtn = [[UIButton alloc] initWithFrame:CGRectMake(messageBtnX, messageBtnY, messageBtnW, messageBtnH)];
    messageBtn.hidden = !isLocalDoctor;
    messageBtn.backgroundColor = [UIColor whiteColor];
    messageBtn.layer.cornerRadius = kP(10);
    messageBtn.layer.borderWidth = kP(2);
    messageBtn.layer.borderColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0].CGColor;
    messageBtn.clipsToBounds = YES;
    [messageBtn setTitle:@"在线留言" forState:UIControlStateNormal];
    [messageBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    messageBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    [messageBtn addTarget:self action:@selector(messageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnBgView addSubview:messageBtn];
    self.messageBtn = messageBtn;
    
    
    //
    //    CGFloat consultBtnX = kP(30);
    //    CGFloat consultBtnY = kP(20);
    //    CGFloat consultBtnW = (self.view.width - 3 * consultBtnX) * 0.5;
    //    CGFloat consultBtnH = btnBgView.height - 2 * consultBtnY;
    //    UIButton *consultBtn = [[UIButton alloc] initWithFrame:CGRectMake(consultBtnX, consultBtnY, consultBtnW, consultBtnH)];
    
    CGFloat consultBtnX = self.messageBtn.hidden ? kP(30) : CGRectGetMaxX(self.messageBtn.frame) + messageBtnX;
    CGFloat consultBtnY = kP(20);
    CGFloat consultBtnW = self.messageBtn.hidden ? self.view.width - 2 * consultBtnX : messageBtnW;
    CGFloat consultBtnH = btnBgView.height - 2 * consultBtnY;
    UIButton *consultBtn = [[UIButton alloc] initWithFrame:CGRectMake(consultBtnX, consultBtnY, consultBtnW, consultBtnH)];
    consultBtn.layer.cornerRadius = kP(10);
    consultBtn.layer.borderWidth = kP(2);
    consultBtn.layer.borderColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0].CGColor;
    consultBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    consultBtn.clipsToBounds = YES;
    [consultBtn setTitle:@"预约会诊" forState:UIControlStateNormal];
    [consultBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    consultBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    //
    [consultBtn addTarget:self action:@selector(consultBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnBgView addSubview:consultBtn];
    self.consultBtn = consultBtn;
    
    self.expertInforTableView.height = self.view.height - kP(344) -btnBgView.height;
}


#pragma mark -- backBtnClick:
- (void)backBtnClick:(UIButton *)backBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setPriceArr:(NSArray *)priceArr {
    _priceArr = priceArr;
}


#pragma mark -- messageBtnClick
- (void)messageBtnClick:(UIButton *)messageBtn {
    
    //
    if ([self.user.authenticationState isEqualToString:@"UNAUTHENTICATED"] || [self.user.authenticationState isEqualToString:@"REJECTED"]) { // 尚未审核 或者 审核失败
        [self setAuthenticationStateCheckFailAlertWithAlertStr:@"需要医生资质才能继续，是否立即认证?"];
        return;
    } else if([self.user.authenticationState isEqualToString:@"AUTHENTICATING"]){ // 审核中
        [self setAuthenticationStateCheckingAlertWithAlertStr:@"您的医生资质认证尚未通过,无法使用此功能"];
        return;
    }
    
    //
    HZMessageViewController *messageVCtr = [HZMessageViewController new];
    messageVCtr.chatter = self.expert;
    messageVCtr.user = self.user;
    [self.navigationController pushViewController:messageVCtr animated:YES];
}
#pragma mark -- consultBtnClick
- (void)consultBtnClick:(UIButton *)consultBtn {
    
    NSLog(@"------xxx-------%@", self.user);
    if (![self.user.authLocalDoctor isEqualToString:@"001"]) {
        [self setConsultAlertWithAlertStr:@"您尚未拥有预约专家的权限，请联系客服开通"];
        return;
    }
    
    //
//    consultBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
//    [consultBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    self.messageBtn.backgroundColor = [UIColor whiteColor];
//    [self.messageBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    //
    HZOrderSubmitViewController *orderSubmitVCtr = [HZOrderSubmitViewController new];
    orderSubmitVCtr.expert = self.expert;
    //
    orderSubmitVCtr.priceList = self.priceArr;
    [self.navigationController pushViewController:orderSubmitVCtr animated:YES];
}

//
#pragma mark -- setSwipeBack
- (void)setSwipeBack {
    self.navigationController.delegate = self; // 设置navigationController的代理为self,并实现其代理方法
    
    id target = self.navigationController.interactivePopGestureRecognizer.delegate;
    
    // handleNavigationTransition:为系统私有API,即系统自带侧滑手势的回调方法，我们在自己的手势上直接用它的回调方法
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    panGesture.delegate = self; // 设置手势代理，拦截手势触发
    [self.view addGestureRecognizer:panGesture];
    self.panGesture = panGesture;
    
    // 一定要禁止系统自带的滑动手势
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1)
    {
        return NO;
    }
    
    return YES;
}


#pragma mark -- handleNavigationTransition

- (void)handleNavigationTransition:(UIPanGestureRecognizer *)panGesture {
    NSLog(@"---------handleNavigationTransition");
}
#pragma mark -- getDefaultCellheight
- (CGFloat)getDefaultCellheightWithText:(NSString *)text {
    
  
    UILabel *heightLabel = [UILabel new];
    heightLabel.numberOfLines = 4;
    heightLabel.font = [UIFont systemFontOfSize:kP(32)];
    heightLabel.text = text;
    CGFloat unFoldLabelW = self.view.width - 2 * kP(82) + kP(25);
    CGFloat unFoldLabelH = [heightLabel sizeThatFits:CGSizeMake(unFoldLabelW, MAXFLOAT)].height;
    NSLog(@"-----unFoldLabelHunFoldLabelH----%lf", unFoldLabelH);
    unFoldLabelH += kP(93.5);
    return unFoldLabelH;
}
#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        HZExpertSkillCell *expertSkillCell = [HZExpertSkillCell cellWithTableView:tableView];
        expertSkillCell.expertSkillStr = self.expert.speciality;
        expertSkillCell.delegate = self;
        //        self.expertSkillCell = expertSkillCell;
        return expertSkillCell;
    } else {
        HZExpertIntroCell *expertIntroCell = [HZExpertIntroCell cellWithTableView:tableView];
        expertIntroCell.expertIntroStr = self.expert.honor;
        expertIntroCell.delegate = self;
        [self addLineTo:expertIntroCell];
        //        self.expertIntroCell = expertIntroCell;
        return expertIntroCell;
    }
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if (indexPath.row == 0) {
        
        NSLog(@"----self.skillCellHeight-----%lf", self.skillDefaultCellHeight);
        
        //        CGFloat cellLowH = [@"擅长擅长擅长擅长擅长擅长擅长" ] + kP(93.5) + kP(26);
        //        HZExpertSkillCell *expertSkillCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        //        NSLog(@"-------expertSkillCell--BOOL------%@", expertSkillCell.unFoldBtnHidden == YES ? @"YES":@"NO");
        //        NSLog(@"-----skillDefaultCellHeight----%lf", self.skillDefaultCellHeight);
        CGFloat cellHeight = 0.f;
        if (self.skillDefaultCellHeight > kP(80)) {
            cellHeight = self.isSkillUnFold ? self.skillCellHeight : self.skillDefaultCellHeight;
        } else {
            
            cellHeight = self.skillDefaultCellHeight;
            cellHeight = cellHeight + kP(20);
        }
        
        return cellHeight;
    } else {
        CGFloat cellHeight = 0.f;
        if (self.introDefaultCellHeight > kP(80)) {
            cellHeight = self.isIntroUnFold ? self.introCellHeight : self.introDefaultCellHeight;
        } else {
            cellHeight = self.introDefaultCellHeight;
            cellHeight = cellHeight + kP(93.5) + kP(26);
        }
        
        return cellHeight;
    }

}

#pragma mark -- HZExpertSkillCellDelegate
- (void)setCellHeightWithIsSkillUnFold:(BOOL)isUnFold cellHeight:(CGFloat)cellHeight {
    self.isSkillUnFold = isUnFold;
    self.skillCellHeight = cellHeight;
    [self.expertInforTableView reloadData];
}


#pragma mark -- expertIntroCellDelegate
- (void)setCellHeightWithIsIntrolUnFold:(BOOL)isUnFold cellHeight:(CGFloat)cellHeight {
    
    self.isIntroUnFold = isUnFold;
    self.introCellHeight = cellHeight;
    [self.expertInforTableView reloadData];
}
#pragma mark -- barHidden
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
//
//
//- (void)dealloc {
//    self.expertInforTableView.delegate = nil;
//    self.navigationController.delegate = nil;
//    self.panGesture.delegate = nil;
//    self.expertSkillCell.delegate = nil;
//    self.expertIntroCell.delegate = nil;
//}
#pragma mark -- setMessageAlertWithAlertStr
- (void)setAuthenticationStateCheckFailAlertWithAlertStr:(NSString *)alertStr {
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"提示" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kP(32)],NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:attributedTitle.string message:alertStr preferredStyle:UIAlertControllerStyleAlert];
    [alertCtr setValue:attributedTitle forKey:@"attributedTitle"];
    
    UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"认证" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self.navigationController pushViewController:[NSClassFromString(@"HZAptitudeImageGetViewController") new] animated:NO];
    }];
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        [cancelAlertAction setValue:[UIColor colorWithHexString:@"878787" alpha:1.0] forKey:@"titleTextColor"];
    }
    
    [alertCtr addAction:cancelAlertAction];
    [alertCtr addAction:ensureAlertAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
}


#pragma mark -- setMessageAlertWithAlertStr
- (void)setAuthenticationStateCheckingAlertWithAlertStr:(NSString *)alertStr {
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"提示" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kP(32)],NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:attributedTitle.string message:alertStr preferredStyle:UIAlertControllerStyleAlert];
    [alertCtr setValue:attributedTitle forKey:@"attributedTitle"];
    
    //
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:nil];
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
    }
    [alertCtr addAction:ensureAlertAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
}

#pragma mark -- setConsultAlertWithAlertStr
- (void)setConsultAlertWithAlertStr:(NSString *)alertStr {
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"提示" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kP(32)],NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:attributedTitle.string message:[NSString stringWithFormat:@"%@(工作日08:30—17:30)", alertStr] preferredStyle:UIAlertControllerStyleAlert];
    [alertCtr setValue:attributedTitle forKey:@"attributedTitle"];
    
    UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"联系客服" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"---------拨打");
        
        UIWebView*callWebview = [[UIWebView alloc] init];
        NSURL *telURL = [NSURL URLWithString:@"tel:057156757315"];// 貌似tel:// 或者 tel: 都行
        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
        //记得添加到view上
        [self.view addSubview:callWebview];
    }];
    
    
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        [cancelAlertAction setValue:[UIColor colorWithHexString:@"878787" alpha:1.0] forKey:@"titleTextColor"];
    }
    
    [alertCtr addAction:cancelAlertAction];
    [alertCtr addAction:ensureAlertAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)addLineTo:(UIView*)view{
    
    for (UIView *lastV in view.subviews) {
        if (lastV.tag == 66) {
            [lastV removeFromSuperview];
        }
    }
    UIView *lineV = [[UIView alloc] initWithFrame:CGRectMake(kP(40), kP(20), HZScreenW - kP(40), 1)];
    lineV.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE" alpha:1.0];
    lineV.tag = 66;
    [view addSubview:lineV];
    
}


//改变图片颜色
- (UIImage *)imageWithColor:(UIColor *)color AndImage:(UIImage*)image
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage*newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
