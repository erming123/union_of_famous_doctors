//
//  HZPayOrder.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/7/18.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZJsonModel.h"
@interface HZPayOrder : HZJsonModel
@property (nonatomic, copy) NSString *payState;
@property (nonatomic, copy) NSString *bookingOrderId;
@property (nonatomic, copy) NSString *goodsId;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *endTime;
@property (nonatomic, copy) NSString *wechatPayInfoId;
@property (nonatomic, copy) NSString *aliPayInfoId;
@end
