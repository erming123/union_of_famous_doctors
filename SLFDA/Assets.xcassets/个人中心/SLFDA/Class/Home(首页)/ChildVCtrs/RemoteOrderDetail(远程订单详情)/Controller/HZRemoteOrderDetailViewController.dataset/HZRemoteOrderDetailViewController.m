//
//  HZRemoteOrderDetailViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZRemoteOrderDetailViewController.h"
#import "HZRemoteOrderStatusCell.h"
#import "HZRemoteOrderStartTimeCell.h"
//#import "HZLocalDoctorInforCell.h"
#import "HZLocalDoctorInforContentCell.h"
#import "HZRemotePatientInforCell.h"
#import "HZOrderPatientInforContentCell.h"
#import "MJRefresh.h"
#import "HZOrder.h"
#import "HZOrderPatientInforViewController.h"
#import "HZUser.h"
#import "HZIMChatViewController.h"
#import "HZCheckListTool.h"


//
//#import "HZRemoteExpertInforContentCell.h"
//
#import "HZRoomViewController.h"
#import "HZSettingViewController.h"
#import "HZMessageDB.h"
//
#import "HZOrderHttpsTool.h"
//
#define kRemoteUnReadCountStr @"remoteUnReadCountStr"
@interface HZRemoteOrderDetailViewController ()<UITableViewDataSource, UITableViewDelegate, HZRemotePatientInforCellDelegate, HZLocalDoctorInforContentCellDelegate, HZRoomViewControllerDelegate, HZSettingViewControllerDelegate, HZRemoteOrderStatusCellDelegate>
@property (nonatomic, strong) UIButton *enterVideoBtn;

//
@property (assign, nonatomic) AgoraRtcVideoProfile videoProfile;

//** <#注释#>*/
@property (nonatomic, strong) HZMessageDB *messageDB;

//@property (nonatomic, copy) NSString *unReadCountStr;
//** <#注释#>*/
@property (nonatomic, strong) UIView *btnBgView;
@end

@implementation HZRemoteOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //1.
    self.navigationItem.title = @"订单详情";
    
    
    //2.
    HZTableView *remoteOrderDetailTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    remoteOrderDetailTableView.dataSource = self;
    remoteOrderDetailTableView.delegate = self;
    remoteOrderDetailTableView.showsVerticalScrollIndicator = NO;
    remoteOrderDetailTableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    remoteOrderDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:remoteOrderDetailTableView];
    self.remoteOrderDetailTableView = remoteOrderDetailTableView;
    
    
    //
//    //下拉刷新
//    self.remoteOrderDetailTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//       
//
//            // 结束刷新
//            [self.remoteOrderDetailTableView.mj_header endRefreshing];
//        
//    }];
//    
//    // 设置自动切换透明度(在导航栏下面自动隐藏)
//    self.remoteOrderDetailTableView.mj_header.automaticallyChangeAlpha = YES;
//    
//    // 上拉刷新
//    self.remoteOrderDetailTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//                  [self.remoteOrderDetailTableView.mj_footer endRefreshing];
//    
//    }];
    
    
    
    
    NSLog(@"-----www----%@", self.remoteOrder);
    
    
    //1.
    [self setUpVideoBtn];
    
    //2.
    self.videoProfile = AgoraRtc_VideoProfile_360P;

    //
    [self getCheckList];
    
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveRemoteMessageSuccess:) name:KNOTIFICATION_onMesssageReceive object:nil];

    
}

- (void)receiveRemoteMessageSuccess:(NSNotification *)notifucation {
    NSLog(@"-----dsdsds2222222----%@", notifucation.object);
    [self getCheckList];
}


- (HZMessageDB *)messageDB {
    if (!_messageDB) {
        _messageDB = [HZMessageDB initMessageDB];
    }
    
    return _messageDB;
}

#pragma mark -- getCheckList
- (void)getCheckList {
    
    
    [HZCheckListTool getCheckListWithBookingOrderId:self.remoteOrder.bookingOrderId  patient:nil userHospitalId:nil success:^(id responseObject) {
        [[NSUserDefaults standardUserDefaults] setObject:responseObject forKey:self.remoteOrder.bookingOrderId];
        
        
        //
        NSMutableArray *receivedMsgArr = [self.messageDB getReceivedMsgUnReadCountArrWithOrderId:self.remoteOrder.bookingOrderId];
        NSString *unReadCountStr = [receivedMsgArr lastObject];
        [[NSUserDefaults standardUserDefaults] setObject:unReadCountStr forKey:kRemoteUnReadCountStr];
        [self.remoteOrderDetailTableView reloadData];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------rrrrrr---%@", error);
    }];
}

- (void)setUpVideoBtn {
    
    [self.btnBgView removeFromSuperview];
    UIView *btnBgView = [[UIView alloc] initWithFrame:CGRectMake(kP(0), self.view.height - kP(130), self.view.width, kP(130))];
    btnBgView.backgroundColor = [UIColor whiteColor];
    btnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    btnBgView.layer.shadowOffset = CGSizeMake(0, - kP(2));
    btnBgView.layer.shadowOpacity = 0.1;
    btnBgView.layer.shadowRadius = kP(2);
    UIButton *enterVideoBtn = [[UIButton alloc] initWithFrame:CGRectMake(kP(32), kP(15), btnBgView.width - 2 * kP(32), btnBgView.height - 2 * kP(15))];
    enterVideoBtn.backgroundColor = kGreenColor;
    enterVideoBtn.layer.cornerRadius = kP(10);
    enterVideoBtn.clipsToBounds = YES;
    [enterVideoBtn setTitle:@"进入视频诊间" forState:UIControlStateNormal];
    [enterVideoBtn setTitle:@"尚未到达开诊时间" forState:UIControlStateDisabled];
    
//    if ([self.remoteOrder.bookingStateCode isEqualToString:@"003"]) {
//        enterVideoBtn.enabled = NO;
//        enterVideoBtn.alpha = 0.5;
//    } else if ([self.remoteOrder.bookingStateCode isEqualToString:@"004"] || [self.remoteOrder.bookingStateCode isEqualToString:@"006"]) {
//        enterVideoBtn.enabled = YES;
//    } else if ([self.remoteOrder.bookingStateCode isEqualToString:@"005"] || [self.remoteOrder.bookingStateCode isEqualToString:@"007"] || [self.remoteOrder.bookingStateCode isEqualToString:@"008"]) {
//        enterVideoBtn.hidden = YES;
//        btnBgView.hidden = YES;
//    }
    
     if ([self.remoteOrder.bookingStateCode isEqualToString:@"003"] || [self.remoteOrder.bookingStateCode isEqualToString:@"004"] || [self.remoteOrder.bookingStateCode isEqualToString:@"006"]) {
         enterVideoBtn.enabled = YES;
         self.remoteOrderDetailTableView.height = self.view.height - kP(130);
    } else if ([self.remoteOrder.bookingStateCode isEqualToString:@"005"] || [self.remoteOrder.bookingStateCode isEqualToString:@"007"] || [self.remoteOrder.bookingStateCode isEqualToString:@"008"]) {
        enterVideoBtn.hidden = YES;
        btnBgView.hidden = YES;
        self.remoteOrderDetailTableView.height = self.view.height;
    } else {
        self.remoteOrderDetailTableView.height = self.view.height - kP(130);
    }

    [enterVideoBtn addTarget:self action:@selector(enterVideoRoom:) forControlEvents:UIControlEventTouchUpInside];
    [btnBgView addSubview:enterVideoBtn];
    [self.view addSubview:btnBgView];
    self.enterVideoBtn = enterVideoBtn;
    self.btnBgView = btnBgView;
}

//- (HZOrder *)remoteOrder {
//    if (!_remoteOrder) {
//        _remoteOrder = [HZOrder new];
//    }
//    
//    return _remoteOrder;
//}


- (void)setRemoteOrder:(HZOrder *)remoteOrder {
    
    _remoteOrder = remoteOrder;
}
//- (void)setRemoteUnReadCount:(int)remoteUnReadCount {
//    _remoteUnReadCount = remoteUnReadCount;
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self refresh];
}



- (void)refresh {
    
    NSLog(@"--------2233");
    
    if (![self.remoteOrder.bookingStateCode isEqualToString:@"006"]) {
        [self reFreshRemoteOrderDetail];
    }
    
    
    [HZCheckListTool getCheckListWithBookingOrderId:self.remoteOrder.bookingOrderId  patient:nil userHospitalId:nil success:^(id responseObject) {
        NSDictionary *resultsDict = responseObject[@"results"];
        NSArray *localCheckList = resultsDict[@"info"];
        [[NSUserDefaults standardUserDefaults] setObject:localCheckList forKey:self.remoteOrder.bookingOrderId];
    
        //
        NSMutableArray *receivedMsgArr = [self.messageDB getReceivedMsgUnReadCountArrWithOrderId:self.remoteOrder.bookingOrderId];
        NSString *unReadCountStr = [receivedMsgArr lastObject];
        [[NSUserDefaults standardUserDefaults] setObject:unReadCountStr forKey:kRemoteUnReadCountStr];
        [self.remoteOrderDetailTableView reloadData];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}


#pragma mark -- reFreshRemoteOrderDetail
- (void)reFreshRemoteOrderDetail {
    
    [HZOrderHttpsTool getOrderDetailWithOrderID:self.remoteOrder.bookingOrderId success:^(id responseObject) {
        NSLog(@"------getOrderDetailWithOrderID---%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSDictionary *bookingOrderDict = resultsDict[@"bookingOrder"];
            
            HZOrder *remoteOrder = [HZOrder mj_objectWithKeyValues:bookingOrderDict];
            remoteOrder.orderType = @"02";
            self.remoteOrder = [HZOrder mj_objectWithKeyValues:[remoteOrder propertyValueDict]];
            
            [self setUpVideoBtn];
            [self.remoteOrderDetailTableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"--------2233----wwww");
    }];
}
#pragma mark -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 3) {
        return 2;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    [cell.textLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    //
    switch (indexPath.section) {
        case 0: {
            
            HZRemoteOrderStatusCell *remoteOrderStatusCell = [HZRemoteOrderStatusCell cellWithTableView:tableView];
            remoteOrderStatusCell.remoteOrder = self.remoteOrder;
            remoteOrderStatusCell.delegate = self;
            return remoteOrderStatusCell;
        }
            break;
        case 1: {
            
//            if (indexPath.row == 0) {
            
                HZRemoteOrderStartTimeCell *remoteOrderStartTimeCell = [HZRemoteOrderStartTimeCell cellWithTableView:tableView];
                return remoteOrderStartTimeCell;
//            } else {
//                cell.textLabel.text = self.remoteOrder.treatmentDateTime;
//                cell.selectionStyle = UITableViewCellSelectionStyleNone;
//                return cell;
//            }
            
        }
            break;
        case 2: {
            
//            if (indexPath.row == 0) {
//                
//                HZLocalDoctorInforCell *doctorInforCell = [HZLocalDoctorInforCell cellWithTableView:tableView];
//                
//                return doctorInforCell;
//            } else {
                HZLocalDoctorInforContentCell *doctorInforContentCell = [HZLocalDoctorInforContentCell cellWithTableView:tableView];
                doctorInforContentCell.delegate = self;
                doctorInforContentCell.remoteOrder = self.remoteOrder;
                
                //
//                int offLineMsgCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"offLineMsgCount"] intValue];
                int unReadMsgCount = [[[NSUserDefaults standardUserDefaults] objectForKey:kRemoteUnReadCountStr] intValue];
//                self.remoteUnReadCount = ;
                //
                doctorInforContentCell.commingNewsCount = unReadMsgCount;
                //
                return doctorInforContentCell;
                
//            }
        }
        case 3: {
            
            if (indexPath.row == 0) {
                HZRemotePatientInforCell *patientInforCel = [HZRemotePatientInforCell cellWithTableView:tableView];
                patientInforCel.delegate = self;
                patientInforCel.remoteOrder = self.remoteOrder;
                return patientInforCel;
            } else {
                
                HZOrderPatientInforContentCell *patientInforContentCell = [HZOrderPatientInforContentCell cellWithTableView:tableView];
                self.remoteOrder.isInOrderDetail = YES;
                patientInforContentCell.order = self.remoteOrder;
                patientInforContentCell.isInOrderDetail = YES;
                return patientInforContentCell;
            }
        }
            
    }
    
    return cell;
}

#pragma mark -- viewForHeaderInSection
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0 || section == 1) return 0;
    
    HZDashLine *dashLine = [[HZDashLine alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 1)];
    dashLine.backgroundColor = [UIColor whiteColor];
    return dashLine;
}

#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = 0;
    switch (indexPath.section) {
        case 0:
            cellHeight = [HZRemoteOrderStatusCell cellHeightWithRemoteOrder:self.remoteOrder];
            break;
        case 1: {
            
//            if (indexPath.row == 0) {
//                return kP(72);
//            } else {
                cellHeight = kP(158);
//            }
            
        }
            break;
        case 2: {
            

                cellHeight = 3 * kP(32) + kP(40) + kP(128);

            
        }
            break;
        case 3: {
            
            if (indexPath.row == 0) {
                cellHeight = kP(72);
            } else {
                
           cellHeight =  [HZOrderPatientInforContentCell cellHeightWithOrder:self.remoteOrder];
            }
            
        }
            break;
    }
    return cellHeight;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(0.01);
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(20);
    }
    return kP(0.1);
}


#pragma mark -- HZRemotePatientInforCellDelegate
- (void)enterMorePatientInfor{
    HZOrderPatientInforViewController *patientInforVCtr = [HZOrderPatientInforViewController new];
    patientInforVCtr.basicOrder = self.remoteOrder;
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
}

#pragma mark -- HZLocalDoctorInforContentCellDelegate
// --- 进入IM页面-----
- (void)enterIMVCtr {
    NSLog(@"---=====%lu",  self.commingNewsCount);
    
    [self pushToIMVCWithAnimated:YES];
}

#pragma mark -- 进入Video
- (void)enterVideoRoom:(UIButton *)btn {
    HZRoomViewController *romeVCtr = [[HZRoomViewController alloc] init];
    romeVCtr.delegate = self;
    romeVCtr.videoProfile = AgoraRtc_VideoProfile_360P;
    romeVCtr.roomName = self.remoteOrder.bookingOrderId;
    romeVCtr.remoteOrder = self.remoteOrder;
    romeVCtr.isComeFromPodfileVCtr = NO;
    [self.navigationController pushViewController:romeVCtr animated:YES];
}


- (void)pushToIMVCWithAnimated:(BOOL)animation {
    //
    HZIMChatViewController *IMChatVCtr = [HZIMChatViewController new];
    IMChatVCtr.IMOrder = self.remoteOrder;
    [self.navigationController pushViewController:IMChatVCtr animated:animation];

}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -- pay

- (void)enterToPay {
    
    NSLog(@"enterToPay");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
