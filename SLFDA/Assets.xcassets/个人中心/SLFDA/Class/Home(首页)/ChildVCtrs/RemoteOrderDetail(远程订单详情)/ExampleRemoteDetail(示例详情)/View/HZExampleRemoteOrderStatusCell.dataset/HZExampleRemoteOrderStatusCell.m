//
//  HZRemoteOrderStatusCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZExampleRemoteOrderStatusCell.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZExampleRemoteOrderStatusCell ()
@property (nonatomic, strong) UIImageView *orderStatusImageView;
@property (nonatomic, strong) UILabel *orderStatusLabel;
@property (nonatomic, strong) UILabel *orderStatusContentLabel;
//@property (nonatomic, strong) UIView *VSepLine;
//@property (nonatomic, strong) UIButton *enterToPayBtn;
@end
NS_ASSUME_NONNULL_END
@implementation HZExampleRemoteOrderStatusCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    
    UIImageView *orderStatusImageView = [UIImageView new];
    [self addSubview:orderStatusImageView];
    self.orderStatusImageView = orderStatusImageView;
    
    //
    UILabel *orderStatusLabel = [[UILabel alloc] init];
    [orderStatusLabel setTextFont:kP(34) textColor:@"#2DBED8" alpha:1.0];
    [self addSubview:orderStatusLabel];
    self.orderStatusLabel = orderStatusLabel;
    
    
    //
    UILabel *orderStatusContentLabel = [[UILabel alloc] init];
    [orderStatusContentLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    orderStatusContentLabel.numberOfLines = 0;
    [self addSubview:orderStatusContentLabel];
    self.orderStatusContentLabel = orderStatusContentLabel;
    
//    //
//    UIView *VSepLine = [UIView new];
//    VSepLine.backgroundColor = [UIColor colorWithHexString:@"#e6e6e6" alpha:1.0];
//    [self addSubview:VSepLine];
//    self.VSepLine = VSepLine;
//    
//    //
//    UIButton *enterToPayBtn = [UIButton new];
//    [enterToPayBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
//    enterToPayBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
//    [self addSubview:enterToPayBtn];
//    self.enterToPayBtn = enterToPayBtn;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    //
    [self setSubViewsContent];
    
    //
    CGFloat orderStatusImageViewWH = kP(40);
    CGFloat orderStatusImageViewXY = kP(32);
    self.orderStatusImageView.frame = CGRectMake(orderStatusImageViewXY, orderStatusImageViewXY, orderStatusImageViewWH, orderStatusImageViewWH);
    
    //
    CGSize orderStatusSize = [self.orderStatusLabel.text getTextSizeWithMaxWidth:kP(500) textFontSize:kP(38)];
    CGFloat orderStatusLabelW = orderStatusSize.width;
    CGFloat orderStatusLabelH = orderStatusSize.height;
    CGFloat orderStatusLabelX = CGRectGetMaxX(self.orderStatusImageView.frame) + kP(20);
    CGFloat orderStatusLabelY = CGRectGetMidY(self.orderStatusImageView.frame) - orderStatusLabelH * 0.5;
    self.orderStatusLabel.frame = CGRectMake(orderStatusLabelX, orderStatusLabelY, orderStatusLabelW, orderStatusLabelH);
    
//    //
//    self.enterToPayBtn.width = kP(180);
//    self.enterToPayBtn.height = kP(28);
////    self.enterToPayBtn.backgroundColor = [UIColor redColor];
//    self.enterToPayBtn.x = self.width - self.enterToPayBtn.width - kP(32);
//    self.enterToPayBtn.y = self.height * 0.5 - self.enterToPayBtn.height * 0.5;
//    
//    
//    //
//    self.VSepLine.x = self.enterToPayBtn.x - kP(32);
//    self.VSepLine.y = kP(45);
//    self.VSepLine.width = 1.0;
//    self.VSepLine.height = self.height - 2 * self.VSepLine.y;
    
    //
    self.orderStatusContentLabel.x = orderStatusLabelX;
    self.orderStatusContentLabel.y = CGRectGetMaxY(self.orderStatusLabel.frame) + kP(25);
    self.orderStatusContentLabel.width = self.width - self.orderStatusContentLabel.x - kP(32);
    self.orderStatusContentLabel.height = [self.orderStatusContentLabel.text getTextHeightWithMaxWidth:self.orderStatusContentLabel.width textFontSize:kP(32)];
}


- (void)setSubViewsContent {
    
    self.orderStatusImageView.image = [UIImage imageNamed:@"patientInfor"];
    self.orderStatusLabel.text = @"即将开始";

    self.orderStatusContentLabel.text = @"请进入视频诊间, 与陪诊医生交流";
    
//    [self.enterToPayBtn setTitle:@"打开支付页面" forState:UIControlStateNormal];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"remoteOrderStatusCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

+ (CGFloat)cellHeightWithRemoteOrder:(HZOrder *)order {
    CGFloat maxWidth = HZScreenW - 2 * kP(32) - kP(60);
    CGFloat height = [@"门诊尚未开始，请勿错过开诊时间" getTextHeightWithMaxWidth:maxWidth textFontSize:kP(33)];
    return height + kP(116);
}
@end
