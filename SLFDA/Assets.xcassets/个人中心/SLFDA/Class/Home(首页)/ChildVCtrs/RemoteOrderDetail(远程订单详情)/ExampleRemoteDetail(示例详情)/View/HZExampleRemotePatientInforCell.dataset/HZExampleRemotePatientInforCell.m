//
//  HZRemotePatientInforCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZExampleRemotePatientInforCell.h"
#import "HZOrder.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZExampleRemotePatientInforCell ()
@property (nonatomic, strong) UIImageView *patientInforImageView;
@property (nonatomic, strong) UILabel *patientInforLabel;
//** <#注释#>*/
@property (nonatomic, strong) UIView *enterBgView;
@property (nonatomic, strong) UILabel *morePatientInforLabel;
@property (nonatomic, strong) UIImageView *enterIndicatorImageView;
//
@property (nonatomic, strong) UILabel *patientNameLabel;
@property (nonatomic, strong) UILabel *patientNameContentLabel;
@property (nonatomic, strong) UILabel *illNameLabel;
@property (nonatomic, strong) UILabel *illNameContentLabel;
@property (nonatomic, strong) UILabel *illDetailLabel;
@property (nonatomic, strong) UILabel *illDetailContentLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZExampleRemotePatientInforCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UIImageView *patientInforImageView = [[UIImageView alloc] init];
    [self addSubview:patientInforImageView];
    self.patientInforImageView = patientInforImageView;
    
    UILabel *patientInforLabel = [[UILabel alloc] init];
    [patientInforLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    [self addSubview:patientInforLabel];
    self.patientInforLabel = patientInforLabel;
    
    
    
    UIView *enterBgView = [[UIView alloc] init];
    [self addSubview:enterBgView];
    self.enterBgView = enterBgView;
    
    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeDelegateEnterMorePatientInfor:)];
    [self.enterBgView addGestureRecognizer:tap];
    
    
    //
    UILabel *morePatientInforLabel = [[UILabel alloc] init];
    [morePatientInforLabel setTextFont:kP(32) textColor:@"#2DBED8" alpha:1.0];
    [self.enterBgView addSubview:morePatientInforLabel];
    self.morePatientInforLabel = morePatientInforLabel;
    
    UIImageView *enterIndicatorImageView = [[UIImageView alloc] init];
    [self.enterBgView addSubview:enterIndicatorImageView];
    self.enterIndicatorImageView = enterIndicatorImageView;
    
    //
    UILabel *patientNameLabel = [UILabel new];
    [patientNameLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self.contentView addSubview:patientNameLabel];
    //    patientNameLabel.backgroundColor = [UIColor redColor];
    self.patientNameLabel = patientNameLabel;
    
    UILabel *patientNameContentLabel = [UILabel new];
    [patientNameContentLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    patientNameContentLabel.numberOfLines = 0;
    //    patientNameContentLabel.backgroundColor = [UIColor greenColor];
    [self.contentView addSubview:patientNameContentLabel];
    self.patientNameContentLabel = patientNameContentLabel;
    
    
    UILabel *illNameLabel = [UILabel new];
    [illNameLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self.contentView addSubview:illNameLabel];
    self.illNameLabel = illNameLabel;
    
    
    UILabel *illNameContentLabel = [UILabel new];
    
    [illNameContentLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    illNameContentLabel.numberOfLines = 0;
    [self.contentView addSubview:illNameContentLabel];
    self.illNameContentLabel = illNameContentLabel;
    
    
    UILabel *illDetailLabel = [UILabel new];
    illDetailLabel.text = @"病情描述:";
    [illDetailLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self.contentView addSubview:illDetailLabel];
    self.illDetailLabel = illDetailLabel;
    
    
    
    UILabel *illDetailContentLabel = [UILabel new];
    [illDetailContentLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    illDetailContentLabel.numberOfLines = 0;
    //    illDetailContentLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:illDetailContentLabel];
    self.illDetailContentLabel = illDetailContentLabel;

}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    
    //2.
    CGFloat patientInforImageViewWH = kP(40);
    CGFloat patientInforImageViewXY = kP(32);
    self.patientInforImageView.frame = CGRectMake(patientInforImageViewXY, patientInforImageViewXY, patientInforImageViewWH, patientInforImageViewWH);
    
    
    [self.patientInforLabel sizeToFit];
    self.patientInforLabel.x = CGRectGetMaxX(self.patientInforImageView.frame) + kP(20);
    self.patientInforLabel.y = CGRectGetMidY(self.patientInforImageView.frame) - self.patientInforLabel.height * 0.5;
    
    //
    CGFloat enterBgViewW = kP(160);
    CGFloat enterBgViewH = kP(40);
    CGFloat enterBgViewX = self.width - enterBgViewW - kP(32);
    CGFloat enterBgViewY = CGRectGetMidY(self.patientInforLabel.frame) - enterBgViewH * 0.5;
    self.enterBgView.frame = CGRectMake(enterBgViewX, enterBgViewY, enterBgViewW, enterBgViewH);
    
    
    [self.morePatientInforLabel sizeToFit];
    self.morePatientInforLabel.x = 0;
    self.morePatientInforLabel.y = 0;
    
    CGFloat enterIndicatorImageViewW = kP(20);
    CGFloat enterIndicatorImageViewH = kP(32);
    CGFloat enterIndicatorImageViewX = CGRectGetMaxX(self.morePatientInforLabel.frame)+kP(20);
    CGFloat enterIndicatorImageViewY = CGRectGetMidY(self.morePatientInforLabel.frame) -  enterIndicatorImageViewH * 0.5;
    self.enterIndicatorImageView.frame = CGRectMake(enterIndicatorImageViewX, enterIndicatorImageViewY, enterIndicatorImageViewW, enterIndicatorImageViewH);
     self.enterIndicatorImageView.contentMode = UIViewContentModeCenter;
    
    //
    //1.
    CGSize patientNameSize = [self.patientNameLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(33)];
    CGFloat patientNameLabelW = kP(150);
    CGFloat patientNameLabelH = patientNameSize.height;
    CGFloat patientNameLabelX = self.patientInforLabel.x;
    CGFloat patientNameLabelY = CGRectGetMaxY(self.patientInforLabel.frame) + kP(20);
    self.patientNameLabel.frame = CGRectMake(patientNameLabelX, patientNameLabelY, patientNameLabelW, patientNameLabelH);
    
    //
    CGFloat maxWidth = self.width - CGRectGetMaxX(self.patientNameLabel.frame) - patientNameLabelX;
    
    //
    CGSize patientNameContentSize = [self.patientNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(33)];
    CGFloat patientNameContentLabelX = CGRectGetMaxX(self.patientNameLabel.frame) + kP(20);
    CGFloat patientNameContentLabelY = patientNameLabelY;
    CGFloat patientNameContentLabelW = maxWidth;
    CGFloat patientNameContentLabelH = patientNameContentSize.height;
    self.patientNameContentLabel.frame = CGRectMake(patientNameContentLabelX, patientNameContentLabelY, patientNameContentLabelW, patientNameContentLabelH);
    
    //2.
    
    CGSize illNameSize = [self.illNameLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(33)];
    CGFloat illNameLabelW = patientNameLabelW;
    CGFloat illNameLabelH = illNameSize.height;
    CGFloat illNameLabelX = patientNameLabelX;
    CGFloat illNameLabelY =  MAX(CGRectGetMaxY(self.patientNameLabel.frame), CGRectGetMaxY(self.patientNameContentLabel.frame)) + kP(20);
    self.illNameLabel.frame = CGRectMake(illNameLabelX, illNameLabelY, illNameLabelW, illNameLabelH);
    
    
    CGSize illNameContentSize = [self.illNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(33)];
    CGFloat illNameContentLabelW = maxWidth;
    CGFloat illNameContentLabelH = illNameContentSize.height;
    CGFloat illNameContentLabelX = CGRectGetMaxX(self.illNameLabel.frame) + kP(20);
    CGFloat illNameContentLabelY = illNameLabelY;
    self.illNameContentLabel.frame = CGRectMake(illNameContentLabelX, illNameContentLabelY, illNameContentLabelW, illNameContentLabelH);
    
    
    //3.
    CGSize illDetailSize = [self.illDetailLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(33)];
    CGFloat illDetailLabelW = patientNameLabelW;
    CGFloat illDetailLabelH = illDetailSize.height;
    CGFloat illDetailLabelX = illNameLabelX;
    CGFloat illDetailLabelY = MAX(CGRectGetMaxY(self.illNameLabel.frame), CGRectGetMaxY(self.illNameContentLabel.frame)) + kP(20);
    self.illDetailLabel.frame = CGRectMake(illDetailLabelX, illDetailLabelY, illDetailLabelW, illDetailLabelH);
    
    CGSize illDetailContentSize = [self.illDetailContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(33)];
    CGFloat illDetailContentLabelW = maxWidth;
    CGFloat illDetailContentLabelH =  illDetailContentSize.height;
    CGFloat illDetailContentLabelX = CGRectGetMaxX(self.illDetailLabel.frame) + kP(20);
    CGFloat illDetailContentLabelY = illDetailLabelY;
    self.illDetailContentLabel.frame = CGRectMake(illDetailContentLabelX, illDetailContentLabelY, illDetailContentLabelW, illDetailContentLabelH);
}

- (void)setSubViewsContent {
    self.patientInforImageView.image = [UIImage imageWithOriginalName:@"patientInfor"];
    self.patientInforLabel.text = @"患者资料";
    self.morePatientInforLabel.text = @"更多资料";
    self.enterIndicatorImageView.image = [UIImage imageNamed:@"arrow"];
    
    //
    self.patientNameLabel.text = @"患者姓名:";
    self.patientNameContentLabel.text = @"陈某某(男, 38岁)";
    self.illNameLabel.text = @"疾病名称:";
    self.illNameContentLabel.text = @"系膜增生性肾炎";
    self.illDetailContentLabel.text = @"面部及两下肢浮肿2个月，伴腰膝酸软，体重增加，纳呆，恶心及重度蛋白尿。无血尿及尿路感染刺激征。发病前一月内无发热、咽痛或关节痛史。收缩期血压稍增高，心肺正常，肝脾不大；轻度贫血，尿素氮及肌酐不增加，总胆固醇显著增多";
}


+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"remotePatientInforCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

+ (CGFloat)cellHeightWithRemoteOrder:(HZOrder *)order {
    CGFloat maxWidth = HZScreenW - kP(150) - 2 * kP(28) - kP(92);
    CGFloat height1 = [@"陈某某(男, 38岁)" getTextHeightWithMaxWidth:maxWidth textFontSize:kP(33)];
    CGFloat height2 = [@"系膜增生性肾炎" getTextHeightWithMaxWidth:maxWidth textFontSize:kP(33)];
    CGFloat height3 = [@"面部及两下肢浮肿2个月，伴腰膝酸软，体重增加，纳呆，恶心及重度蛋白尿。无血尿及尿路感染刺激征。发病前一月内无发热、咽痛或关节痛史。收缩期血压稍增高，心肺正常，肝脾不大；轻度贫血，尿素氮及肌酐不增加，总胆固醇显著增多" getTextHeightWithMaxWidth:maxWidth textFontSize:kP(33)];
    return height1 +height2 + height3 + kP(168);
}

- (void)makeDelegateEnterMorePatientInfor:(UITapGestureRecognizer *)tap {
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterExampleMorePatientInfor)]) {
        [self.delegate enterExampleMorePatientInfor];
    }
}
@end
