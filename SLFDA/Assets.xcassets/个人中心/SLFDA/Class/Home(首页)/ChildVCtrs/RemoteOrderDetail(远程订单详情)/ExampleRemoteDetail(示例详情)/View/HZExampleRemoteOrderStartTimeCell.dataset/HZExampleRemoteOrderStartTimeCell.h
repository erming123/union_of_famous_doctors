//
//  HZExampleRemoteOrderStartTimeCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZExampleRemoteOrderStartTimeCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
