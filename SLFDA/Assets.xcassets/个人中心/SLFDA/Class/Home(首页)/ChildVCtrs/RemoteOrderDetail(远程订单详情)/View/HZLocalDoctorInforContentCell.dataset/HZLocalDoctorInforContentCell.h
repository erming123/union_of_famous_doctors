//
//  HZLocalDoctorInforContentCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@protocol HZLocalDoctorInforContentCellDelegate <NSObject>

- (void)enterIMVCtr;

@end
@interface HZLocalDoctorInforContentCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) id <HZLocalDoctorInforContentCellDelegate> delegate;
@property (nonatomic, assign) BOOL isIMBtnHidden;
@property (nonatomic, assign) int commingNewsCount;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *remoteOrder;
@end
