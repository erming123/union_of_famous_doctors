//
//  HZRemoteExpertInforContentCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>


@class HZOrder;
@protocol HZRemoteExpertInforContentCellDelegate <NSObject>

- (void)enterIMVCtr;

@end
@interface HZRemoteExpertInforContentCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) id <HZRemoteExpertInforContentCellDelegate> delegate;
@property (nonatomic, assign) BOOL isIMBtnHidden;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *myLocalOrder;
@property (nonatomic,assign) int unReadMsgCount;
@end
