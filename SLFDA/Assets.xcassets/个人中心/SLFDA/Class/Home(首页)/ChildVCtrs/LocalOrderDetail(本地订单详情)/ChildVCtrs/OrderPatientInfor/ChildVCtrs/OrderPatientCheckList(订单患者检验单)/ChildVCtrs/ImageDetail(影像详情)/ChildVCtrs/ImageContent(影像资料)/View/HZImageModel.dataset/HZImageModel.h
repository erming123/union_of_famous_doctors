//
//  HZImageModel.h
//  HZScrollViewDemo
//
//  Created by 季怀斌 on 2017/6/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface HZImageModel : NSObject
@property (nonatomic, strong) UIImage *showImage;
@property (nonatomic, assign) BOOL isSelectedShowImage;
@property (nonatomic, copy) NSString *downloadPath;
@end
