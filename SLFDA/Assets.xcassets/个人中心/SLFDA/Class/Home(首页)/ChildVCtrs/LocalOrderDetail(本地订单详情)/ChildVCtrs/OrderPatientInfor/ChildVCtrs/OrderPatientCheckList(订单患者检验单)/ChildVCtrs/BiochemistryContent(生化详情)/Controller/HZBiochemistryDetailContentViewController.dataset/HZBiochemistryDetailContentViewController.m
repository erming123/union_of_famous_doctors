//
//  HZBiochemistryDetailContentViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZBiochemistryDetailContentViewController.h"
#import "HZCheckBiochemistryBaseInforCell.h"
#import "HZBiochemistryCheckContentCell.h"
#import "HZSectionHeadView.h"
#import "HZCheckModel.h"
#import "HZBiochemistryListModel.h"
#import "MJRefresh.h"
#import "HZCheckListTool.h"
#import "HZOrder.h"
@interface HZBiochemistryDetailContentViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) HZTableView *tableView;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *biochemistryCheckListArr;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation HZBiochemistryDetailContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"生化检验详情";
    
    HZTableView *tableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
    if (kCurrentSystemVersion >= 11.0) {
        tableView.y = 64;
        tableView.height -= 64;
    }
    
    tableView.dataSource = self;
    tableView.delegate = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    //
    if (self.biochemistryCheckModel.isExample) {
        HZBiochemistryListModel *biochemistryListModel1 = [HZBiochemistryListModel new];
        biochemistryListModel1.jcxmzw = @"总蛋白(TP)";
        biochemistryListModel1.jcjgbz = @"66.0 ";
        biochemistryListModel1.ybjcjg = @"";
        biochemistryListModel1.xmjgfw = @"66-83";
        biochemistryListModel1.xmjldw = @"g/L";
        
        HZBiochemistryListModel *biochemistryListModel2 = [HZBiochemistryListModel new];
        biochemistryListModel2.jcxmzw = @"白蛋白(A|b)";
        biochemistryListModel2.jcjgbz = @"32.1 ";
        biochemistryListModel2.ybjcjg = @"";
        biochemistryListModel2.xmjgfw = @"35-55";
        biochemistryListModel2.xmjldw = @"g/L";
        
        HZBiochemistryListModel *biochemistryListModel3 = [HZBiochemistryListModel new];
        biochemistryListModel3.jcxmzw = @"球蛋白(GLB)";
        biochemistryListModel3.jcjgbz = @"34.9 ";
        biochemistryListModel3.ybjcjg = @"";
        biochemistryListModel3.xmjgfw = @"20-35";
        biochemistryListModel3.xmjldw = @"g/L";
        
        HZBiochemistryListModel *biochemistryListModel4 = [HZBiochemistryListModel new];
        biochemistryListModel4.jcxmzw = @"白/球蛋白比(A/G)";
        biochemistryListModel4.jcjgbz = @"0.9 ";
        biochemistryListModel4.ybjcjg = @"";
        biochemistryListModel4.xmjgfw = @"1.5-2.5";
        biochemistryListModel4.xmjldw = @"";
        
        HZBiochemistryListModel *biochemistryListModel5 = [HZBiochemistryListModel new];
        biochemistryListModel5.jcxmzw = @"谷丙酸氨基转移酶(ALT)";
        biochemistryListModel5.jcjgbz = @"14 ";
        biochemistryListModel5.ybjcjg = @"";
        biochemistryListModel5.xmjgfw = @"5-35";
        biochemistryListModel5.xmjldw = @"U/L";
        
        HZBiochemistryListModel *biochemistryListModel6 = [HZBiochemistryListModel new];
        biochemistryListModel6.jcxmzw = @"天门冬氨基转移酶(AST)";
        biochemistryListModel6.jcjgbz = @"64 ";
        biochemistryListModel6.ybjcjg = @"";
        biochemistryListModel6.xmjgfw = @"8-40";
        biochemistryListModel6.xmjldw = @"U/L";
        
        self.biochemistryCheckListArr = @[biochemistryListModel1, biochemistryListModel2, biochemistryListModel3, biochemistryListModel4, biochemistryListModel5, biochemistryListModel6];
    }
    
    //
    [self getBiochemistryCheckListData];
    //下拉上拉时候才刷新
    [self pushToRefreshData];
}



#pragma mark -- 刷新数据
- (void)pushToRefreshData {
    
    if (self.biochemistryCheckModel.isExample) return;
    if (!self.biochemistryCheckModel.jcybid) {
        return;
    }
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        [self  refresh];
        
    }];
    self.tableView.mj_header = refreshHeader;
    
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.tableView.mj_header.automaticallyChangeAlpha = YES;
    //    [self.tableView reloadData];
    
}




- (NSArray *)biochemistryCheckListArr {
    if (!_biochemistryCheckListArr) {
        _biochemistryCheckListArr = [NSArray array];
    }
    
    return _biochemistryCheckListArr;
}


- (void)getBiochemistryCheckListData {
    
    
    
    if (self.biochemistryCheckModel.isExample) {
        return;
    }
    
    if (!self.biochemistryCheckModel.jcybid) {
        return;
    }
    NSDictionary *biochemistryCheckDict = [[NSUserDefaults standardUserDefaults] objectForKey:self.biochemistryCheckModel.jcybid];
    
    
    if (biochemistryCheckDict) {// 有保存
        NSString *stateStr =  [NSString stringWithFormat:@"%@", biochemistryCheckDict[@"state"]];
        if ([stateStr isEqualToString:@"200"] || [stateStr isEqualToString:@"0"]) { // 有列表
            
            NSDictionary *resultDict = biochemistryCheckDict[@"results"];
            NSArray *biochemistryCheckList = resultDict[@"info"];
            self.biochemistryCheckListArr = [HZBiochemistryListModel mj_objectArrayWithKeyValuesArray:biochemistryCheckList];
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
            
        } else { // 没有列表
            
            //            NSString *alertStr = biochemistryCheckDict[@"msg"];
            
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
            
        }
        
    } else { // 没有保存
        [self.activityIndicator startAnimating];
        
        
        [HZCheckListTool getBiochemistryCheckListWithCheckModelId:self.biochemistryCheckModel.jcybid patientHospitalId:self.biochemistryDetailPassOrder.hospitalId success:^(id responseObject) {
            
            
            
            // 展示
            NSString *stateStr =  [NSString stringWithFormat:@"%@", responseObject[@"state"]];
            if ([stateStr isEqualToString:@"200"] || [stateStr isEqualToString:@"0"]) {
                NSDictionary *resultDict = responseObject[@"results"];
                NSArray *biochemistryCheckList = resultDict[@"info"];
                
                
                //
                self.biochemistryCheckListArr = [HZBiochemistryListModel mj_objectArrayWithKeyValuesArray:biochemistryCheckList];
                [self.tableView reloadData];
                [self.activityIndicator stopAnimating];
                
            } else {
                NSString *alertStr = responseObject[@"msg"];
                NSLog(@"---alertStr********------%@", alertStr);
                [self.tableView reloadData];
                [self.activityIndicator stopAnimating];
            }
            
            
            
            
            // 保存
            [[NSUserDefaults standardUserDefaults] setObject:responseObject forKey:self.biochemistryCheckModel.jcybid];
            
        } failure:nil];
        
    }
    
    
    
    
    
}


- (void)refresh {
    
    
    [self.activityIndicator startAnimating];
    [HZCheckListTool getBiochemistryCheckListWithCheckModelId:self.biochemistryCheckModel.jcybid  patientHospitalId:self.biochemistryDetailPassOrder.hospitalId success:^(id responseObject) {
        
        [[NSUserDefaults standardUserDefaults] setObject:responseObject forKey:self.biochemistryCheckModel.jcybid];
        [self.tableView reloadData];
        [self.activityIndicator stopAnimating];
        [self.tableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self.activityIndicator stopAnimating];
        [self.tableView.mj_header endRefreshing];
    }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)setBiochemistryCheckModel:(HZCheckModel *)biochemistryCheckModel {
    
    
    _biochemistryCheckModel = biochemistryCheckModel;
}

- (void)setBiochemistryListModel:(HZBiochemistryListModel *)biochemistryListModel {
    _biochemistryListModel = biochemistryListModel;
}

- (void)setBiochemistryDetailPassOrder:(HZOrder *)biochemistryDetailPassOrder {
    _biochemistryDetailPassOrder = biochemistryDetailPassOrder;
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    
    
    if (section == 0) {
        return 1;
    }
    return self.biochemistryCheckListArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        HZCheckBiochemistryBaseInforCell *checkBiochemistryBaseInforCell = [HZCheckBiochemistryBaseInforCell cellWithTableView:tableView];
        checkBiochemistryBaseInforCell.biochemistryCheckModel = self.biochemistryCheckModel;
        return checkBiochemistryBaseInforCell;
    } else {
        //
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        HZBiochemistryCheckContentCell *biochemistryCheckContentCell = [HZBiochemistryCheckContentCell cellWithTableView:tableView];
        HZBiochemistryListModel *biochemistryListModel = self.biochemistryCheckListArr[indexPath.row];
        biochemistryCheckContentCell.biochemistryListModel = biochemistryListModel;
        return biochemistryCheckContentCell;
    }
    
}


#pragma mark -- delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        return [HZCheckBiochemistryBaseInforCell cellHeightWithBiochemistryCheckModel:self.biochemistryCheckModel];
        
    }
    
    return [HZBiochemistryCheckContentCell cellHeightWithBiochemistryCheckModel:self.biochemistryCheckListArr[indexPath.row]];
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(0.1);
    }
    
    return kP(60);
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(30);
    }
    
    return 0.1;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
   
    HZSectionHeadView *headView = [[HZSectionHeadView alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, kP(60))];
    headView.leftStr = @"名称";
    headView.midStr = @"结果";
    headView.rightStr = @"参考范围";
    if (section == 0) {
        return nil;
    }
    
    return headView;
}

@end

