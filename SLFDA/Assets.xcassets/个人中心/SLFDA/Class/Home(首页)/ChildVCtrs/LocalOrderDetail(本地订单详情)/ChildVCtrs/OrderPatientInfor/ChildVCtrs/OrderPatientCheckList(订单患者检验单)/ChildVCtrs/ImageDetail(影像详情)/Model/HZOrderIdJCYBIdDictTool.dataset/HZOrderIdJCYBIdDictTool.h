//
//  HZOrderIdJCYBIdDictTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/7/5.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZOrderIdJCYBIdDictTool : NSObject
@property (nonatomic, strong) NSMutableDictionary *orderIdJCYBIdDict;
+ (instancetype)shareInstance;
- (void)addBookingOrderIdKey:(NSString *)bookingOrderIdKey JCYBIdValue:(NSString *)JCYBIdValue;
- (void)removeJCYBIdValuesForBookingOrderIdKey:(NSString *)bookingOrderIdKey;
@end
