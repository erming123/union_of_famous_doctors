//
//  HZImageCell.m
//  HZScrollViewDemo
//
//  Created by 季怀斌 on 2017/6/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZImageCell.h"
#import "HZImageModel.h"
#define kP(px) (CGFloat)(px * 0.5 * CGRectGetWidth([[UIScreen mainScreen] bounds]) / 375)
#import "UIColor+HexString.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZImageCell ()
@property (nonatomic, strong) UIImageView *myImageView;
@end
NS_ASSUME_NONNULL_END
@implementation HZImageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpSubViews];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    
    UIImageView *myImageView = [UIImageView new];
    myImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
    [self addSubview:myImageView];
    self.myImageView = myImageView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    CGFloat myImageViewX = kP(0);
    CGFloat myImageViewY = kP(9);
    CGFloat myImageViewWH = kP(120);
    self.myImageView.frame = CGRectMake(myImageViewX, myImageViewY, myImageViewWH, myImageViewWH);
    
    //3.
    self.myImageView.layer.borderWidth = kP(4);
    if (self.imageModel.isSelectedShowImage) {
        self.myImageView.layer.borderColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0].CGColor;
    } else {
        self.myImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
}

- (void)setSubViewsContent {

    self.myImageView.image = self.imageModel.showImage;
}


//
- (void)setImageModel:(HZImageModel *)imageModel {
    _imageModel = imageModel;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
