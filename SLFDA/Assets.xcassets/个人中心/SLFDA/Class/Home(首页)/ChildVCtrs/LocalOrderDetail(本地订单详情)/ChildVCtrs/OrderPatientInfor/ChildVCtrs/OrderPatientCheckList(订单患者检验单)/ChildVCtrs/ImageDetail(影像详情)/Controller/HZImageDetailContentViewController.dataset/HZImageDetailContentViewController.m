//
//  HZImageDetailContentViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZImageDetailContentViewController.h"
#import "HZCheckImageBaseInforCell.h"
#import "HZImageCheckContentCell.h"
#import "HZCheckModel.h"
//
#import "HZImageContentViewController.h"

//
#import "SSZipArchive.h"
#import "AFNetworking.h"
//
#import "HZImageDataTool.h"

//
#import "HZImageModel.h"
@interface HZImageDetailContentViewController ()<UITableViewDataSource, UITableViewDelegate, SSZipArchiveDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *imageTableView;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *imageBtn;

//** 下载任务*/
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
//** 大小数组*/
@property (nonatomic, copy) NSMutableArray *mutableBigArray;
@property (nonatomic, copy) NSMutableArray *mutableSmallArray;
//** <#注释#>*/
@property (nonatomic, strong) HZImageModel *imageModel;
@end

@implementation HZImageDetailContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.title = @"影像检验详情";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    HZTableView *imageTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    imageTableView.dataSource = self;
    imageTableView.delegate = self;
    imageTableView.showsVerticalScrollIndicator = NO;
    imageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:imageTableView];
    self.imageTableView = imageTableView;
    
    NSLog(@"---ddddddddddddddddd------%@", self.imageCheckModel.jcybid);

    NSString *imageKey = [NSString stringWithFormat:@"%@Key", self.imageCheckModel.jcybid];
    NSArray *imageArr = [[NSUserDefaults standardUserDefaults] objectForKey:imageKey];
    
    if (imageArr && imageArr.count > 0) {
        [self setMyImageLoadBtn];
    } else {
        [self getImageBaseInfor];
    }
    
    
    HZImageModel *imageModel = [HZImageModel new];
    self.imageModel = imageModel;
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadDone) name:@"downLoadDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloading:) name:@"downLoading" object:nil];
}


- (void)downloadDone {
    NSLog(@"dddddd-----------------------------");
    
    
    //
    
    
   
    
//    NSArray *imageArr = [[NSUserDefaults standardUserDefaults] objectForKey:imageKey];
//    
//    if (imageArr && imageArr.count > 0) {
    
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"imageBtnDisabled"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //
        [self.imageBtn setTitle:@"打开影像" forState:UIControlStateNormal];
        
        
        self.imageBtn.userInteractionEnabled = YES;
//    }
    
    
   
    
}


- (void)downloading:(NSNotification *)notification {
    NSLog(@"gggg------------------------------%@", notification.object);
    [self.imageBtn setTitle:notification.object forState:UIControlStateNormal];
    self.imageBtn.userInteractionEnabled = NO;
}
- (void)dealloc {
   [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -- getImageBaseInfor
- (void)getImageBaseInfor {
    
//    self.imageCheckModel.jcybid = @"648860CT";
    
    NSLog(@"---------------------wer-%@", self.imageCheckModel.jcybid);
    //
    [HZImageDataTool getImageBaseInforWithJCYBID:self.imageCheckModel.jcybid patientHospitalId:self.patientHospitalId success:^(id  _Nullable responseObject) {
        NSLog(@"-----v下载----%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            
            
            NSDictionary *dataDict = responseObject[@"data"];
            NSString *downloadPath = dataDict[@"downloadPath"];
            
            if ([downloadPath hasSuffix:@"zip"]) {
                
                //
                self.imageModel.downloadPath = downloadPath;
                
//                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                
                    [self setMyImageLoadBtn];
//                });
                
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----下载w-----%@", error);
    }];
}

#pragma mark -- setMyImageLoadBtn

- (void)setMyImageLoadBtn {
    
    self.imageTableView.height -= kP(130);
    //
    CGFloat imageBtnBgViewX = kP(0);
    CGFloat imageBtnBgViewH = kP(130);
    CGFloat imageBtnBgViewY = HZScreenH - imageBtnBgViewH - SafeAreaBottomHeight;
    CGFloat imageBtnBgViewW = HZScreenW;
    
    UIView *imageBtnBgView = [[UIView alloc] initWithFrame:CGRectMake(imageBtnBgViewX, imageBtnBgViewY, imageBtnBgViewW, imageBtnBgViewH)];
    //
    imageBtnBgView.backgroundColor = [UIColor whiteColor];
    imageBtnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    imageBtnBgView.layer.shadowOffset = CGSizeMake(kP(0), - kP(2));
    imageBtnBgView.layer.shadowOpacity = 0.1;
    //
    imageBtnBgView.layer.shadowRadius = kP(2);
    [self.view addSubview:imageBtnBgView];
    
    //
    NSString *imageKey = [NSString stringWithFormat:@"%@Key", self.imageCheckModel.jcybid];
    NSArray *imageArr = [[NSUserDefaults standardUserDefaults] objectForKey:imageKey];
    //
    CGFloat imageBtnX = kP(32);
    CGFloat imageBtnY = kP(15);
    CGFloat imageBtnW = HZScreenW - 2 * imageBtnX;
    CGFloat imageBtnH = imageBtnBgViewH - 2 * imageBtnY;
    
    UIButton *imageBtn = [[UIButton alloc] initWithFrame:CGRectMake(imageBtnX, imageBtnY, imageBtnW, imageBtnH)];
    imageBtn.backgroundColor = kGreenColor;
    
    //    imageBtn.enabled = ;
    
    //
    
    if (imageArr) {
        [imageBtn setTitle:@"打开影像" forState:UIControlStateNormal];
    } else {
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"imageBtnDisabled"]) {
            [imageBtn setTitle:@"下载中..." forState:UIControlStateNormal];
        } else {
            [imageBtn setTitle:@"查看影像" forState:UIControlStateNormal];
        }
        
        
    }
    
    
    [imageBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    imageBtn.layer.cornerRadius = kP(10);
    imageBtn.clipsToBounds = YES;
    [imageBtn addTarget:self action:@selector(imageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [imageBtnBgView addSubview:imageBtn];
    self.imageBtn = imageBtn;

}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        HZCheckImageBaseInforCell *checkImageBaseInforCell = [HZCheckImageBaseInforCell cellWithTableView:tableView];
        checkImageBaseInforCell.imageCheckModel = self.imageCheckModel;
        return checkImageBaseInforCell;
    } else {
        HZImageCheckContentCell *imageCheckContentCell = [HZImageCheckContentCell cellWithTableView:tableView];
        imageCheckContentCell.imageCheckModel = self.imageCheckModel;
        return imageCheckContentCell;
        
    }
}



-(void)setImageCheckModel:(HZCheckModel *)imageCheckModel {
    _imageCheckModel = imageCheckModel;
}

- (void)setBookingOrderId:(NSString *)bookingOrderId {
    _bookingOrderId = bookingOrderId;
}

- (void)setPatientHospitalId:(NSString *)patientHospitalId {
    _patientHospitalId = patientHospitalId;
}
#pragma mark -- delgate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return [HZCheckImageBaseInforCell cellHeightWithImageCheckModel:self.imageCheckModel];
    }
    
    return [HZImageCheckContentCell cellHeightWithImageCheckModel:self.imageCheckModel];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kP(0.1);
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kP(1);
}



#pragma mark -- imageBtnClick
- (void)imageBtnClick:(UIButton *)imageBtn {
    
    
    NSString *imageKey = [NSString stringWithFormat:@"%@Key", self.imageCheckModel.jcybid];
    NSArray *imageArr = [[NSUserDefaults standardUserDefaults] objectForKey:imageKey];
    NSLog(@"----imageBtnClick--imageArr:%@", imageArr);
    if (imageArr) {
        HZImageContentViewController *imageContentVCtr = [HZImageContentViewController new];
        imageContentVCtr.imageArr = imageArr;
        imageContentVCtr.imageCheckModel = self.imageCheckModel;
        imageContentVCtr.bookingOrderId = self.bookingOrderId;
        [self.navigationController pushViewController:imageContentVCtr animated:YES];
    } else if (![[NSUserDefaults standardUserDefaults] boolForKey:@"imageBtnDisabled"]) {
        [self downloadImageArr];
    } else {
        [self setAlertWithAlertStr:@"订单影像找不到"];
//        NSLog(@"---------------------------ss-----------------------------------");
    }

}

- (void)downloadImageArr {
    
    
//    self.imageBtn.enabled = NO;
    [self.imageBtn setTitle:@"下载中..." forState:UIControlStateNormal];
    self.imageBtn.userInteractionEnabled = NO;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"imageBtnDisabled"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    //
//    self.imageModel.downloadPath = @"230010.3.1.2.140713912.7060.1500253569.1049.zip";
    //
    NSString *imageZipUrl = [NSString stringWithFormat:@"hocpacs/v1/tool/download?key=%@&fileName=%@", self.imageModel.downloadPath, @"ddd.zip"];
    NSURL *zipURL = [NSURL URLWithString:kGatewayUrlStr(imageZipUrl)];
    [self loadZipWithZipUrl:zipURL];

}


#pragma mark -- loadZipWithZipUrl

- (void)loadZipWithZipUrl:(NSURL *)zipUrl {
    
    //默认配置
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    //
    NSString *authorizationStr = [NSString stringWithFormat:@"Bearer %@", kAccess_token];
    
    NSLog(@"------mykAccess_token---%@", kAccess_token);
    
    [configuration setHTTPAdditionalHeaders:@{@"Authorization": authorizationStr}];
    //AFN3.0+基于封住URLSession的句柄
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    //请求
    NSURLRequest *request = [NSURLRequest requestWithURL:zipUrl];
    //下载Task操作
    _downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        // 下载进度
        
//        NSString *progressStr = [NSString stringWithFormat:@"下载中:%@", downloadProgress.];
//        [self.imageBtn setTitle:@"" forState:UIControlStateNormal];
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        //- block的返回值, 要求返回一个URL, 返回的这个URL就是文件的位置的路径
        NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
        NSString *path = [cachesPath stringByAppendingPathComponent:response.suggestedFilename];
        return [NSURL fileURLWithPath:path];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        //设置下载完成操作
        // filePath就是你下载文件的位置，你可以解压，也可以直接拿来使用
        NSString *imgFilePath = [filePath path];// 将NSURL转成NSString
        NSLog(@"imgFilePath = %@",imgFilePath);
        //
        //        self.progressView.statusStr = @"点点滴滴";
        //
        NSArray *documentArray =  NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *path = [[documentArray lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"Preferences/JCYPics/%@", self.imageCheckModel.jcybid]];
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            //            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsZipDone];
            
            // 解压
            [self releaseZipFilesWithUnzipFileAtPath:imgFilePath Destination:path];
        });
        
    }];
    
    [manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        NSLog(@"setDownloadTaskDidWriteDataBlock: %lld %lld %lld", bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *progressStr = [NSString stringWithFormat:@"下载中:%.2fM", totalBytesWritten / (1024 * 1024.0)];
//            [self.imageBtn setTitle:progressStr forState:UIControlStateNormal];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"downLoading" object:progressStr];
        });
        
    }];
    
    [_downloadTask resume];
}

////

// 解压
- (void)releaseZipFilesWithUnzipFileAtPath:(NSString *)zipPath Destination:(NSString *)unzipPath{
    NSError *error;
    
    if ([SSZipArchive unzipFileAtPath:zipPath toDestination:unzipPath overwrite:YES password:nil error:&error delegate:self]) {
        
        
        //
        NSLog(@"success");
        NSLog(@"unzipPath = %@",unzipPath);
        
        
        
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        
        
                BOOL bRet = [fileMgr fileExistsAtPath:zipPath];
        
                if (bRet) {
        
                    //
        
                    NSError *err;
        
                    [fileMgr removeItemAtPath:zipPath error:&err];
        
                }


       NSData *data = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/sort.json", unzipPath]];
//
        NSArray *chaArr = [self toArrayOrNSDictionary:data];

        NSMutableArray *mutableArray2 = [NSMutableArray array];
        NSArray *temp = [NSArray array];
        
        NSLog(@"---qwe---:%@", chaArr);
        for (int i = 0; i < chaArr.count; i++) {
            
            NSMutableArray *mutableArray1 = [NSMutableArray array];
            temp = chaArr[i];
            NSLog(@"-----123asd--------:%@", temp);
            int firstCount = [[temp firstObject] intValue];
            
            NSLog(@"-----rty--------:%d", firstCount);
            for (int j = firstCount; j < temp.count + firstCount; j++) {
                
                NSString *unzipPath1 = unzipPath;
                unzipPath1  = [unzipPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.jpg", j]];
                [mutableArray1 addObject:unzipPath1];
            }
            
            [mutableArray2 addObject:mutableArray1];
            
            
        }
        
        NSLog(@"-----解压mutableArray2--------:%@", mutableArray2);
        //
        self.mutableBigArray = mutableArray2;
        NSString *imageKey = [NSString stringWithFormat:@"%@Key", self.imageCheckModel.jcybid];
        [[NSUserDefaults standardUserDefaults] setObject:self.mutableBigArray forKey:imageKey];
        [[NSUserDefaults standardUserDefaults] synchronize];

        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (self.mutableBigArray == nil || self.mutableBigArray.count == 0) return;
            
            if (![self.imageCheckModel.jcybid isEqualToString:@"648860CT"]) {
                 [[HZOrderIdJCYBIdDictTool shareInstance] addBookingOrderIdKey:self.bookingOrderId JCYBIdValue:self.imageCheckModel.jcybid];
            }
           
       
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"downLoadDone" object:nil];

            
            
            
        });
        
//         self.imageBtn.enabled = YES;
        NSLog(@"-----temp----:%@", self.mutableBigArray);
    } else {
        
        [self setAlertWithAlertStr:@"订单影像找不到"];
//        [self.imageBtn setTitle:@"打开影像" forState:UIControlStateNormal];
        self.imageBtn.userInteractionEnabled = YES;
        NSLog(@"-----------------------------dd---------------------------------");
    }
    
}



- (id)toArrayOrNSDictionary:(NSData *)jsonData{
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData
                                                    options:NSJSONReadingAllowFragments
                                                      error:nil];
    
    if (jsonObject != nil && error == nil){
        return jsonObject;
    }else{
        // 解析错误
        return nil;
    }
    
}


- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *err;
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    
    if(err) {
        NSLog(@"json解析失败：%@",err);
        
        return nil;
    }
    
    return dic;
    
}

#pragma mark -- setAlertWithAlertStr
- (void)setAlertWithAlertStr:(NSString *)alertStr {
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:alertStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
    }
    [alertCtr addAction:ensureAlertAction];
    [self.navigationController presentViewController:alertCtr animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
