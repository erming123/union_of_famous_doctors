//
//  HZOrderPatientBasicInforViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZOrderPatientBasicInforViewController.h"
#import "HZOrderPatientInforContentCell.h"
#import "HZOrder.h"
#import "HZOrderHttpsTool.h"
@interface HZOrderPatientBasicInforViewController ()<UITableViewDataSource, UITableViewDelegate>

//** <#注释#>*/
@property (nonatomic, strong) HZTableView *patientBasicInforTableView;
@end

@implementation HZOrderPatientBasicInforViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //
    HZTableView *patientBasicInforTableView = [[HZTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - kP(128) - 44 ) style:UITableViewStylePlain];
    patientBasicInforTableView.dataSource = self;
    patientBasicInforTableView.delegate = self;
//    patientBasicInforTableView.backgroundColor = [UIColor redColor];
    patientBasicInforTableView.showsVerticalScrollIndicator = NO;
    patientBasicInforTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:patientBasicInforTableView];
    self.patientBasicInforTableView = patientBasicInforTableView;
    
    if (!self.basicOrder.isExampleOrder) {
        [self getData];
    }
    
}


#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HZOrderPatientInforContentCell *patientBasicInforCell = [HZOrderPatientInforContentCell cellWithTableView:tableView];
    self.basicOrder.isInOrderDetail = NO;
    patientBasicInforCell.order = self.basicOrder;
    patientBasicInforCell.isInOrderDetail = NO;
    return patientBasicInforCell;
}


- (void)setBasicOrder:(HZOrder *)basicOrder {
    _basicOrder = basicOrder;
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [HZOrderPatientInforContentCell cellHeightWithOrder:self.basicOrder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**获取资料图片数据*/
- (void)getData {
    if (self.basicOrder.laboratoryReports != nil) return;
    
    [HZOrderHttpsTool getOrderDetailWithOrderID:self.basicOrder.bookingOrderId success:^(id responseObject) {
       
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSDictionary *bookingOrderDict = resultsDict[@"bookingOrder"];
            self.basicOrder = [HZOrder mj_objectWithKeyValues:bookingOrderDict];
            [self.patientBasicInforTableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          // NSLog(@"--------2233----wwww");
    }];

    
}

@end
