//
//  HZBiochemistryCheckContentCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZBiochemistryListModel;
@interface HZBiochemistryCheckContentCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//** <#注释#>*/
@property (nonatomic, strong) HZBiochemistryListModel *biochemistryListModel;
+ (CGFloat)cellHeightWithBiochemistryCheckModel:(HZBiochemistryListModel *)biochemistryListModel;
@end
