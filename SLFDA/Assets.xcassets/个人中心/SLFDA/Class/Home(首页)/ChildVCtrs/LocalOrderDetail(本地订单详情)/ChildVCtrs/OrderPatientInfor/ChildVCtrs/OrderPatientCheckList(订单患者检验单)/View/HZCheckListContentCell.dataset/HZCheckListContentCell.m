//
//  HZCheckListContentCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZCheckListContentCell.h"
#import "HZCheckModel.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZCheckListContentCell ()
@property (nonatomic, strong) UILabel *assayContentLabel;
@property (nonatomic, strong) UIImageView *tagImageView;
@property (nonatomic, strong) UILabel *orderTimeLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZCheckListContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    UILabel *assayContentLabel = [[UILabel alloc] init];
    [self addSubview:assayContentLabel];
    self.assayContentLabel = assayContentLabel;
    
    UIImageView *tagImageView = [[UIImageView alloc] init];
    [self addSubview:tagImageView];
    self.tagImageView = tagImageView;
    
    UILabel *orderTimeLabel = [[UILabel alloc] init];
    [self addSubview:orderTimeLabel];
    self.orderTimeLabel = orderTimeLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    //
    [self setUpSubViewsContent];
    
    //
    [self.assayContentLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    CGFloat assayContentLabelW = [self.assayContentLabel.text getTextWidthWithMaxWidth:kP(400) textFontSize:kP(35)];
    CGFloat assayContentLabelH = kP(30);
    CGFloat assayContentLabelX = kP(28);
    CGFloat assayContentLabelY = self.height * 0.5 - assayContentLabelH * 0.5;
    self.assayContentLabel.frame = CGRectMake(assayContentLabelX, assayContentLabelY, assayContentLabelW, assayContentLabelH);
    
    //
    [self.tagImageView sizeToFit];
    self.tagImageView.x = CGRectGetMaxX(self.assayContentLabel.frame);
    self.tagImageView.y = CGRectGetMidY(self.assayContentLabel.frame) - self.tagImageView.height * 0.5;
    
    //
    [self.orderTimeLabel setTextFont:kP(28) textColor:@"#9B9B9B" alpha:1.0];
    CGFloat orderTimeLabelW = [self.orderTimeLabel.text getTextWidthWithMaxWidth:kP(300) textFontSize:kP(30)];
    CGFloat orderTimeLabelH = kP(20);
    CGFloat orderTimeLabelX = self.width - orderTimeLabelW - kP(28);
    CGFloat orderTimeLabelY = CGRectGetMidY(self.assayContentLabel.frame) - orderTimeLabelH * 0.5;
    self.orderTimeLabel.frame = CGRectMake(orderTimeLabelX, orderTimeLabelY, orderTimeLabelW, orderTimeLabelH);
    
}


- (void)setUpSubViewsContent {
    if ([self.checkModel.flag isEqualToString:@"0"]) {// 生化
        
        self.tagImageView.image = [UIImage imageNamed:@"tag_inspection"];
        NSString *assayPostStr = self.checkModel.jcmdmc;
        
        //
        if (assayPostStr) {
            if (assayPostStr.length > 0) {
                if ([[assayPostStr substringFromIndex:assayPostStr.length - 1] isEqualToString:@","]) {
                    assayPostStr = [assayPostStr stringByReplacingCharactersInRange:NSMakeRange(assayPostStr.length - 1, 1) withString:@""];
                }
            }
        }
        
        self.assayContentLabel.text = assayPostStr;
        
        NSString *timePostStr = self.checkModel.ybjgsj;
        NSString *timeStr;
        if (timePostStr) {
            if (timePostStr.length >= 11) {
                timeStr = [timePostStr stringByReplacingCharactersInRange:NSMakeRange(10, timePostStr.length - 10) withString:@""];
            }
            
        }
        
        
        self.orderTimeLabel.text = timeStr;
    } else {// 影像
        self.tagImageView.image = [UIImage imageNamed:@"tag_check"];
        self.assayContentLabel.text = self.checkModel.jcxmmc;
        
        NSString *str = self.checkModel.jcdjsj;
        
        NSString *timeStr;
        if (str) {
            if (str.length >= 11) {
                timeStr = [str stringByReplacingCharactersInRange:NSMakeRange(10, str.length - 10) withString:@""];
            }
            
        }
        
        self.orderTimeLabel.text = timeStr;
    }
    
    
}


- (void)setCheckModel:(HZCheckModel *)checkModel {
    _checkModel = checkModel;
}
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"checkCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

@end

