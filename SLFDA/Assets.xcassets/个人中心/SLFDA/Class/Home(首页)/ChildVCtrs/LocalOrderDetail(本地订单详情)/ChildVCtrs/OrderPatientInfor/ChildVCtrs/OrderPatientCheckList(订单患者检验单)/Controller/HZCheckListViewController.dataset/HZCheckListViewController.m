//
//  HZCheckListViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZCheckListViewController.h"
#import "HZCheckListContentCell.h"
#import "HZBiochemistryDetailContentViewController.h"
#import "HZImageDetailContentViewController.h"
#import "HZOrder.h"
#import "HZCheckModel.h"
#import "HZCheckListTool.h"
#import "MJRefresh.h"
@interface HZCheckListViewController ()<UITableViewDataSource, UITableViewDelegate>

//** <#注释#>*/
@property (nonatomic, strong) UIImageView *noCheckListImageView;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *showDemoBtn;
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *assayListContentTableView;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *checkListArr;
//** <#注释#>*/
//@property (nonatomic, strong) UILabel *errorLabel;
@property (nonatomic, assign) BOOL isExample;
//** <#注释#>*/
@property (nonatomic, strong) HZCheckModel *checkModel;
@end

@implementation HZCheckListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.isShouldPanToPopBack = NO;
    
    self.view.backgroundColor = [UIColor blueColor];
    HZTableView *assayListContentTableView = [[HZTableView alloc] initWithFrame:CGRectMake(kP(0), kP(0), self.view.width, self.view.height - kP(220)) style:UITableViewStylePlain];
    assayListContentTableView.dataSource = self;
    assayListContentTableView.delegate = self;
    assayListContentTableView.tableFooterView = [UIView new];
    [self.view addSubview:assayListContentTableView];
    self.assayListContentTableView = assayListContentTableView;
    
    //
    //2. 没有订单的时候
    UIImageView *noCheckListImageView = [[UIImageView alloc] init];
    noCheckListImageView.x = self.view.width * 0.5 - kP(340) * 0.5;
    noCheckListImageView.y = (self.view.height - kP(300)) * 0.5 - kP(380) * 0.5;
    noCheckListImageView.width = kP(340);
    noCheckListImageView.height = kP(382);
    noCheckListImageView.hidden = YES;
    noCheckListImageView.image = [UIImage imageNamed:@"noReport"];
    [self.view addSubview:noCheckListImageView];
    self.noCheckListImageView = noCheckListImageView;
    noCheckListImageView.centerY = self.view.centerY - kP(150);
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = noCheckListImageView.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    
//    //3. 网络出现问题的时候
//    UILabel *errorLabel = [[UILabel alloc] init];
//  //  errorLabel.text = @"暂未获取到数据";
//    [errorLabel sizeToFit];
//    errorLabel.center = self.view.center;
//    errorLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
//    errorLabel.hidden = YES;
//    [self.view addSubview:errorLabel];
//    self.errorLabel = errorLabel;
    
    
    self.isExample = [self.checkListOrder.bookingOrderId isEqualToString:@"iOSTestBookingOrderId:example123456"];
    
    //
    if (self.isExample) {
        HZCheckModel *checkModel1 = [HZCheckModel new];
        checkModel1.flag = @"0";
        checkModel1.jcmdmc = @"肝肾脂肪电解质+镁";
        checkModel1.ybjgsj = @"2016-11-06";
        checkModel1.jcybid = @"648860CT";
        checkModel1.kdysxm = @"张郁文";
        checkModel1.isExample = YES;
        
        HZCheckModel *checkModel2 = [HZCheckModel new];
        checkModel2.flag = @"1";
        checkModel2.jcxmmc = @"CT全腹平扫+增强";
        checkModel2.jcdjsj = @"2017-06-07 18:03";
        checkModel2.jcybid = @"648860CT";
        checkModel2.sjbrxm = @"许顺良";
        checkModel2.yxsjnr = @"肝脏略大，肝脏轮廓欠光整，平扫肝内不规则片状密度略低影，增强后肝内多发大小不一类圆形、椭圆形环形强化灶，最大者长径约为39mm。肝内血管走行未见异常，肝内外胆管无扩张。胆囊不大。   脾不大，胰腺大小形态正常，胰管无扩张。胃充盈，胃壁未见明显增厚。肠道未见明显异常。  \n     左肾下极不规则团块状影向下方延伸，肿块形态不规则，与左侧腰大肌分界不清，肿块最大径约为82mm，平扫不均匀低密度影、其内散在数个点状钙化灶，增强后呈明显不均匀强化，左肾脂肪囊模糊，片絮状强化， 左侧肾前筋膜增厚，周及左侧结肠旁沟少量积液。肿块与左侧下腹部肠管分界欠清。右侧下腹部腹膜处多个结节状软组织影明显强化。     右肾大小形态未见异常，增强后未见明显异常强化灶。双侧输尿管未见扩张。膀胱轻度充盈，壁无增厚。双侧精囊腺对称，前列腺未见异常。盆腔少量积液。\n     腹膜后腹主动脉旁、左肾动脉水平见多个明显增大淋巴结影，淋巴结呈不规则环形强化、中央密度较低。右侧耻骨、坐骨见骨质破坏并软组织肿块影。      另见：两肺多发团块状影，双侧胸腔中等量积液；两肺下叶片状密度增高影。";
        checkModel2.yxzdnr = @"1.左肾癌侵犯周围组织，腹主动脉旁多发淋巴结转移。2.肝脏多发转移性肿瘤，腹膜多发转移性肿瘤，少量腹水及盆腔积液。3.右侧耻坐骨转移性肿瘤。4.另见：两肺多发转移性肿瘤，胸腔积液并两肺下叶节段性不张。";
        checkModel2.isExample = YES;
        self.checkListArr = [NSMutableArray arrayWithObjects:checkModel1, checkModel2, nil];
    }
    
    //
    [self getCheckList];
    
    if (self.noCheckListImageView.hidden) {
        //
        NSLog(@"--------sadadsdasdsddsaddfsdfsdfsdsfds________________________-");
        [self refreshCheckList];
    }
    [self pushToRefreshData];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -- 刷新数据
- (void)pushToRefreshData {
    
    if (self.isExample) return;

    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        
        
        [self refreshCheckList];
        // 结束刷新
        [self.assayListContentTableView.mj_header endRefreshing];
        
    }];
    self.assayListContentTableView.mj_header = refreshHeader;
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.assayListContentTableView.mj_header.automaticallyChangeAlpha = YES;
    
}
- (void)setCheckListOrder:(HZOrder *)checkListOrder {
    _checkListOrder = checkListOrder;
}

- (void)getCheckList {
    
    
    
//    self.errorLabel.hidden = YES;
    
    if (self.isExample) {
        return;
    }
    
    
    //
    NSArray *checkListArr = [[NSUserDefaults standardUserDefaults] objectForKey:self.checkListOrder.bookingOrderId];
    if (checkListArr.count == 0) {
        
        [self.activityIndicator startAnimating];
        [HZCheckListTool getCheckListWithBookingOrderId:self.checkListOrder.bookingOrderId  patient:nil userHospitalId:nil success:^(id responseObject) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSArray *localCheckList = resultsDict[@"info"];
            //
            NSArray *checkModelArr = [HZCheckModel mj_objectArrayWithKeyValuesArray:localCheckList];
            self.checkListArr = [NSMutableArray arrayWithArray:checkModelArr];
            [[NSUserDefaults standardUserDefaults] setObject:localCheckList forKey:self.checkListOrder.bookingOrderId];
            self.noCheckListImageView.hidden = self.checkListArr.count != 0;
            [self.assayListContentTableView reloadData];
            [self.activityIndicator stopAnimating];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"------rrrrrr---%@", error);
            [self.activityIndicator stopAnimating];
//            self.errorLabel.hidden = NO;
        }];
        
    } else {
        [self.activityIndicator stopAnimating];
        NSArray *checkModelArr = [HZCheckModel mj_objectArrayWithKeyValuesArray:checkListArr];
        self.checkListArr = [NSMutableArray arrayWithArray:checkModelArr];
        
        [self.assayListContentTableView reloadData];
    }
    
}



- (void)refreshCheckList {
    
    
    NSLog(@"---------++++++++++++++++++++____________________-");
//    self.errorLabel.hidden = YES;
    if (self.isExample) {
        return;
    }
    [self.activityIndicator startAnimating];
    
    [HZCheckListTool getCheckListWithBookingOrderId:self.checkListOrder.bookingOrderId  patient:nil userHospitalId:nil success:^(id responseObject) {
        
        NSLog(@"----ddsa-----%@", [responseObject getDictJsonStr]);
        
        NSDictionary *resultsDict = responseObject[@"results"];
        NSArray *localCheckList = resultsDict[@"info"];
        //
        NSArray *checkModelArr = [HZCheckModel mj_objectArrayWithKeyValuesArray:localCheckList];
        self.checkListArr = [NSMutableArray arrayWithArray:checkModelArr];
        [[NSUserDefaults standardUserDefaults] setObject:localCheckList forKey:self.checkListOrder.bookingOrderId];
        self.noCheckListImageView.hidden = self.checkListArr.count != 0;
        [self.assayListContentTableView reloadData];
        [self.activityIndicator stopAnimating];
        // 结束刷新
        [self.assayListContentTableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------rrrrrr-错--%@", error);
        [self.activityIndicator stopAnimating];
//        self.errorLabel.hidden = NO;
        // 结束刷新
        [self.assayListContentTableView.mj_header endRefreshing];
    }];
    
}
- (NSMutableArray *)checkListArr {
    if (!_checkListArr) {
        _checkListArr = [NSMutableArray array];
    }
    
    return _checkListArr;
}


#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return  self.isExample ? 2 : self.checkListArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    HZCheckListContentCell *checkListContentCell = [HZCheckListContentCell cellWithTableView:tableView];
    checkListContentCell.checkModel = self.checkListArr[indexPath.row];
    return checkListContentCell;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(100);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HZBiochemistryDetailContentViewController *asssayDetailContentVCtr = [[HZBiochemistryDetailContentViewController alloc] init];
    HZImageDetailContentViewController *imageDetailContentVCtr = [HZImageDetailContentViewController new];
    
    HZCheckModel *checkModel = self.checkListArr[indexPath.row];
    BOOL isBiochemistry = [checkModel.flag isEqualToString:@"0"];
    
    
    NSLog(@"-----tutututtututututu----%@", checkModel.jcybid);
    if (isBiochemistry) { // 生化
        
        [self setBiochemistryDetailDataWithCheckId:checkModel.jcybid];// 生化列表
        asssayDetailContentVCtr.biochemistryCheckModel = checkModel;// 顶部数据
        asssayDetailContentVCtr.biochemistryDetailPassOrder = self.checkListOrder;
        [self.navigationController pushViewController:asssayDetailContentVCtr animated:YES];
    } else { // 影像
        imageDetailContentVCtr.imageCheckModel = checkModel;
        imageDetailContentVCtr.bookingOrderId = self.checkListOrder.bookingOrderId;
        imageDetailContentVCtr.patientHospitalId = self.checkListOrder.hospitalId;
        [self.navigationController pushViewController:imageDetailContentVCtr animated:YES];
    }
    
}


- (void)setBiochemistryDetailDataWithCheckId:(NSString *)checkId {
    
    if (!checkId) {
        return;
    }
    [HZCheckListTool getBiochemistryCheckListWithCheckModelId:checkId patientHospitalId:self.checkListOrder.hospitalId success:^(id responseObject) {
        NSLog(@"---ddsdsdsdsds0------%@", responseObject);
        [[NSUserDefaults standardUserDefaults] setObject:responseObject forKey:checkId];
    } failure:nil];
    
}

@end
