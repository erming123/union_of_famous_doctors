//
//  HZLocalOrderStartTimeCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZLocalOrderStartTimeCell.h"
#import "HZOrder.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZLocalOrderStartTimeCell ()
@property (nonatomic, strong) UIImageView *orderStartImageView;
@property (nonatomic, strong) UILabel *orderStartLabel;
@property (nonatomic, strong) UILabel *orderStartTimeLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZLocalOrderStartTimeCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UIImageView *orderStartImageView = [[UIImageView alloc] init];
    //    orderStartImageView.backgroundColor = [UIColor greenColor];
    orderStartImageView.image = [UIImage imageWithOriginalName:@"startTime"];
    [self addSubview:orderStartImageView];
    self.orderStartImageView = orderStartImageView;
    
    UILabel *orderStartLabel = [[UILabel alloc] init];
    [orderStartLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    [self addSubview:orderStartLabel];
    self.orderStartLabel = orderStartLabel;
    
    UILabel *orderStartTimeLabel = [[UILabel alloc] init];
    [orderStartTimeLabel setTextFont:kP(32) textColor:@"#666666" alpha:1.0];
    [self addSubview:orderStartTimeLabel];
    self.orderStartTimeLabel = orderStartTimeLabel;
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //
    [self setSubViewsContent];
    
    
    //
    CGFloat orderStartImageViewWH = kP(40);
    CGFloat orderStartImageViewX = kP(32);
    CGFloat orderStartImageViewY = kP(28);
    
    self.orderStartImageView.frame = CGRectMake(orderStartImageViewX, orderStartImageViewY, orderStartImageViewWH, orderStartImageViewWH);
    
    //
    [self.orderStartLabel sizeToFit];
    self.orderStartLabel.x = CGRectGetMaxX(self.orderStartImageView.frame) + kP(20);
    self.orderStartLabel.centerY = self.orderStartImageView.centerY;
    
    //
    [self.orderStartTimeLabel sizeToFit];
    self.orderStartTimeLabel.x = CGRectGetMinX(self.orderStartLabel.frame);
    self.orderStartTimeLabel.y = CGRectGetMaxY(self.orderStartImageView.frame)+kP(22);
    
}

- (void)setSubViewsContent {
  self.orderStartLabel.text = @"开诊时间";
  self.orderStartTimeLabel.text = self.localOrder.treatmentDateTime;
}

- (void)setLocalOrder:(HZOrder *)localOrder {
    _localOrder = localOrder;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"localOrderStartTimeCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
