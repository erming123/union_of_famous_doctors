//
//  HZOrderPatientInforViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZOrderPatientInforViewController.h"
#import "HZOrderPatientBasicInforViewController.h"
#import "HZCheckListViewController.h"
#import "HZOrder.h"
#import "HZCheckModel.h"
#import "MJExtension.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZOrderPatientInforViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIView *seperateLine;
@property (nonatomic, strong) UIView *indicator;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) UIButton *selectedTitleBtn;
@property (nonatomic, copy) NSMutableArray *titleBtnArr;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *postArr;
//** <#注释#>*/
@property (nonatomic, strong)  HZCheckListViewController *checkListVCtr;
@end
NS_ASSUME_NONNULL_END
@implementation HZOrderPatientInforViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationItem.title = @"患者资料";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    //1.创建titleView
    [self setUpTitleView];

    //2.添加titleView最下面的分割线
    [self setUpSeperateLine];

    //3.添加titleView标题指示条
    [self setUpTitleIndicator];
//
    
    
    //4.创建ContentScrollView
    [self setUpContentScrollView];

    //5.添加子视图控制器
    [self setUpAllChildViewControllers];

    //6.添加标题
    [self setUpAllTitles];

    //7.解决系统自动处理滚动视图的问题
    self.automaticallyAdjustsScrollViewInsets = NO;
//
//    //8.标题的点击

    NSLog(@"-------------------rrrre-----------------");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selCheckReportBtn) name:@"enterCheckReportVCtr" object:nil];
}

- (void)selCheckReportBtn {
    
    UIButton *checkReportBtn = [(UIButton *)[UIView alloc] viewWithTag:1];
    [self selectTitleBtn:checkReportBtn];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (NSMutableArray *)postArr {
    if (!_postArr) {
        _postArr = [NSMutableArray array];
    }
    
    return _postArr;
}

-(void)setBasicOrder:(HZOrder *)basicOrder {
    _basicOrder = basicOrder;
}


- (void)setLocalCheckListArr:(NSArray *)localCheckListArr {
    _localCheckListArr = localCheckListArr;
}



#pragma mark -- 初始化标题按钮数组
- (NSMutableArray *)titleBtnArr {
    if (_titleBtnArr == nil) {
        _titleBtnArr = [NSMutableArray array];
    }
    
    return _titleBtnArr;
}


#pragma mark -- 创建titleView
- (void)setUpTitleView {
    UIView *titleView = [[UIView alloc] init];
    //    CGFloat titleViewY = self.navigationController.navigationBarHidden ? kP(40) : kP(128);
    CGFloat titleViewY = self.parentViewController ? SafeAreaTopHeight: self.titleViewY;
    titleView.frame = CGRectMake(0, titleViewY, HZScreenW, kP(100));
    //    titleView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:titleView];
    self.titleView = titleView;
}

#pragma mark -- 添加titleView最下面的分割线
- (void)setUpSeperateLine {
    UIView *seperateLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleView.frame), HZScreenW, kP(2))];
    seperateLine.backgroundColor = [UIColor colorWithHexString:@"d9d9d9" alpha:1.0];
    [self.view addSubview:seperateLine];
    self.seperateLine = seperateLine;
}
//
#pragma mark -- 添加titleView标题指示条
- (void)setUpTitleIndicator {
    
    UIView *indicator = [[UIView alloc] init];
    indicator.frame = CGRectMake(0, CGRectGetMaxY(self.seperateLine.frame) - kP(4), HZScreenW / 2., kP(4));
    indicator.backgroundColor = kGreenColor;
    [self.view addSubview:indicator];
    self.indicator = indicator;
}

#pragma mark -- 创建ContentScrollView
- (void)setUpContentScrollView {
    
    UIScrollView *contentScrollView = [[UIScrollView alloc] init];
    CGFloat contentScrollViewY = CGRectGetMaxY(self.seperateLine.frame);
    contentScrollView.frame = CGRectMake(0, contentScrollViewY, HZScreenW, self.view.height - contentScrollViewY);
    // 设置效果
    contentScrollView.bounces = NO;
    contentScrollView.pagingEnabled = YES;
    contentScrollView.showsHorizontalScrollIndicator = NO;
    
    
    // 设置代理
    contentScrollView.delegate = self;
    [self.view addSubview:contentScrollView];
    self.contentScrollView = contentScrollView;
    
    
}

#pragma mark -- 添加子视图控制器
- (void)setUpAllChildViewControllers {
    // 基本资料
    HZOrderPatientBasicInforViewController *basicsVCtr = [[HZOrderPatientBasicInforViewController alloc] init];
    basicsVCtr.title = @"病情资料";
    basicsVCtr.basicOrder = self.basicOrder;
    [self addChildViewController:basicsVCtr];
    // 检验资料
    HZCheckListViewController *checkListVCtr = [[HZCheckListViewController alloc] init];
    checkListVCtr.title = @"院内报告";
    checkListVCtr.checkListOrder = self.basicOrder;
    [self addChildViewController:checkListVCtr];
    self.checkListVCtr = checkListVCtr;
}


#pragma mark -- 添加标题
- (void)setUpAllTitles {
    // 添加所有变体按钮
    
    NSInteger btnCount = self.childViewControllers.count;
    
    CGFloat titleBtnW = HZScreenW / 2.;
    CGFloat titleBtnH = kP(100);
    for (NSInteger i = 0; i < btnCount; i++) {
        UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        titleBtn.frame = CGRectMake(i * titleBtnW, 5, titleBtnW, titleBtnH);
        
        // 添加标题
        UIViewController *VCtr = self.childViewControllers[i];
        [titleBtn setTitle:VCtr.title forState:UIControlStateNormal];
        [titleBtn setTitleColor:[UIColor colorWithHexString:@"262626" alpha:1.0] forState:UIControlStateNormal];
        titleBtn.titleLabel.font = [UIFont systemFontOfSize:kP(31)];
        
//        if (kCurrentSystemVersion < 9) {
////            titleBtn.titleLabel ;
//        } else {
//            titleBtn.titleLabel.adjustsFontForContentSizeCategory = YES;
//        }
        
        // 添加tag
        titleBtn.tag = i;
        [self.titleView addSubview:titleBtn];
        
        // 监听按钮的点击
        [titleBtn addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        // 把标题按钮保存到数组
        [self.titleBtnArr addObject:titleBtn];
        
        // 默认选中第一个
        if (i == 0 && self.isReportBtnSel == NO) {
            [self titleBtnClick:titleBtn];
        } else if (i == 1 && self.isReportBtnSel == YES) {
            [self titleBtnClick:titleBtn];
        }
    }
    
    // 让ContentScrollView滚动
    [self.contentScrollView setContentSize:CGSizeMake(HZScreenW * btnCount, 0)];
}


#pragma mark -- 处理titleBtn的点击
- (void)titleBtnClick:(UIButton *)titleBtn {
    
    NSInteger titleIndex = titleBtn.tag;
    // 选中的字体颜色
    [self selectTitleBtn:titleBtn];
    // 横线偏移
    [self reSetIndicatorOrigin:titleIndex];
    
    
    // 添加子视图控制器的View
    [self setUpOneViewController:titleIndex];
    // 子视图控制器的View滚动到可视区域
    CGFloat x = titleIndex * HZScreenW;
    [self.contentScrollView setContentOffset:CGPointMake(x, 0) animated:NO];
}


#pragma mark -- 横线偏移
- (void)reSetIndicatorOrigin:(NSInteger)titleIndex {
    [UIView animateWithDuration:0.2f animations:^{
        self.indicator.x = titleIndex * self.indicator.width;
    }];
}
#pragma mark -- 选中标题
- (void)selectTitleBtn:(UIButton *)titleBtn {
    [self.selectedTitleBtn setTitleColor:[UIColor colorWithHexString:@"262626" alpha:1.0] forState:UIControlStateNormal];
    [titleBtn setTitleColor:kGreenColor forState:UIControlStateNormal];
    self.selectedTitleBtn = titleBtn;
}


#pragma mark -- 添加子视图控制器的View

- (void)setUpOneViewController:(NSInteger)titleIndex{
    
    
    // 判断是否已经加载，YES->不再加载，NO->加载
    
    UIViewController *VCTr = self.childViewControllers[titleIndex];
    if (VCTr.view.superview) {
        return;
    }
    
    
    UIViewController *VCtr = self.childViewControllers[titleIndex];
    CGFloat x = titleIndex * HZScreenW;
    VCtr.view.frame = CGRectMake(x, 0, HZScreenW, self.contentScrollView.height);
    [self.contentScrollView addSubview:VCtr.view];
    
}

#pragma mark -- UIScrollViewDelegate

// 滚动完成
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    
    // 获取当前角标
    NSInteger i = scrollView.contentOffset.x / HZScreenW;
    
    //1.选中标题
    UIButton *titleBtn = self.titleBtnArr[i];
    [self selectTitleBtn:titleBtn];
    
    //2.横线偏移
    [self reSetIndicatorOrigin:i];
    
    //3.添加子视图控制器的View
    [self setUpOneViewController:i];
    
//    //4.不是病情交流的页面，取消键盘的第一响应
//    if (i != 3) {
//        [self.view endEditing:YES];
//    }
    
}



- (void)rebackToRootViewAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (BOOL)prefersStatusBarHidden {
//    return self.adviceVCtr.isClicked;
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
