//
//  HZOrderPatientInforContentCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/10/8.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@interface HZOrderPatientInforContentCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *order;

+ (CGFloat)cellHeightWithOrder:(HZOrder *)order;
@property (nonatomic, assign) BOOL isInOrderDetail;
@end
