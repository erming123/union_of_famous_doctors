//
//  HZImageContentViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZCheckModel;
@interface HZImageContentViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZCheckModel *imageCheckModel;
@property (nonatomic, copy) NSArray *imageArr;
@property (nonatomic, copy) NSString *bookingOrderId;
@end
