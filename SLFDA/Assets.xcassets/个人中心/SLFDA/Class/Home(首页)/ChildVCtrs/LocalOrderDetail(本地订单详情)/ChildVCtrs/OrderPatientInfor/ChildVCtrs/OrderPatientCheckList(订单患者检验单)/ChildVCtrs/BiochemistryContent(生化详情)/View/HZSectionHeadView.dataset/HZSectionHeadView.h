//
//  HZSectionHeadView.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZSectionHeadView : UIView
@property (nonatomic, copy) NSString *leftStr;
@property (nonatomic, copy) NSString *midStr;
@property (nonatomic, copy) NSString *rightStr;
@end
