//
//  HZExampleCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExampleCell.h"
#import "HZOrder.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZExampleCell ()
@property (nonatomic, weak) UIView *bgView;
//
@property (nonatomic, weak) UIView *baseInforBgView;
@property (nonatomic, weak) UIImageView *IDImageView;
@property (nonatomic, weak) UILabel *IDLabel;
@property (nonatomic, weak) UILabel *leftTimeLabel;
//
@property (nonatomic, weak) UIView *sepLine;
//
@property (nonatomic, weak) UIView *HSepLine;
@property (nonatomic, weak) UILabel *illNameLabel;
@property (nonatomic, weak) UILabel *patientInforLabel;
@property (nonatomic, weak) UIImageView *exampleImageView;

//
@property (nonatomic, weak) UIView *baseInforOperateBgView;
@property (nonatomic, weak) UIView *baseInfoBgView;
@property (nonatomic, weak) UIButton *patientInforBtn;
@property (nonatomic, weak) UIView *VSepLine;
@property (nonatomic, weak) UIButton *checkReportBtn;
//@property (nonatomic, weak) UIImageView *comingNewsImgView;
@end
NS_ASSUME_NONNULL_END
@implementation HZExampleCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    }
    
    return self;
}


- (void)setUpSubViews {
    
    
    //
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor colorWithHexString:@"#D9D9D9" alpha:1.0];
    bgView.layer.cornerRadius = kP(16);
    bgView.layer.masksToBounds = NO;
    bgView.layer.shadowColor = [UIColor grayColor].CGColor;
    bgView.layer.shadowOffset = CGSizeMake(kP(2), kP(2));
    bgView.layer.shadowOpacity = 0.3;
    [self.contentView addSubview:bgView];
    self.bgView = bgView;
    
    UIView *baseInforBgView = [UIView new];
    baseInforBgView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    [self.bgView addSubview:baseInforBgView];
    self.baseInforBgView = baseInforBgView;
    //
    UIImageView *IDImageView = [[UIImageView alloc] init];
    [self.baseInforBgView addSubview:IDImageView];
    self.IDImageView = IDImageView;
    
    //
    UILabel *IDLabel = [[UILabel alloc] init];
    [IDLabel setTextFont:kP(28) textColor:@"#888888" alpha:1.0];
    [self.baseInforBgView addSubview:IDLabel];
    self.IDLabel = IDLabel;
    
    //
    UILabel *leftTimeLabel = [[UILabel alloc] init];
    leftTimeLabel.textAlignment = NSTextAlignmentCenter;
    [leftTimeLabel setTextFont:kP(30) textColor:@"#2DBED8" alpha:1.0];
    [self.baseInforBgView addSubview:leftTimeLabel];
    self.leftTimeLabel = leftTimeLabel;
    
    
    //
    UIView *HSepLine = [[UIView alloc] init];
    HSepLine.backgroundColor = [UIColor colorWithHexString:@"#F0F0F0" alpha:1.0];
    [self.bgView addSubview:HSepLine];
    self.HSepLine = HSepLine;
    
    
    //
    UILabel *illNameLabel = [UILabel new];
    [illNameLabel setTextFont:kP(34) textColor:@"#333333" alpha:1.0];
    [self.bgView addSubview:illNameLabel];
    self.illNameLabel = illNameLabel;
    
    
    
    
    //
    UILabel *patientInforLabel = [UILabel new];
    [patientInforLabel setTextFont:kP(30) textColor:@"#666666" alpha:1.0];
    [self.bgView addSubview:patientInforLabel];
    self.patientInforLabel = patientInforLabel;
    
    
    //
    //    UILabel *comingNewsLabel = [[UILabel alloc] init];
    //    comingNewsLabel.text = @"新消息";
    //    [comingNewsLabel setTextFont:kP(24) textColor:@"#FFFFFF" alpha:1.0];
    //    comingNewsLabel.textAlignment = NSTextAlignmentCenter;
    //    comingNewsLabel.layer.cornerRadius = kP(20);
    //    comingNewsLabel.layer.masksToBounds = YES;
    //    comingNewsLabel.backgroundColor = [UIColor redColor];
    //    [self.bgView addSubview:comingNewsLabel];
    //    self.comingNewsLabel = comingNewsLabel;
    
    
//    UIImageView *comingNewsImgView = [UIImageView new];
//    comingNewsImgView.image = [UIImage imageNamed:@"redMsg.png"];
//    [self addSubview:comingNewsImgView];
//    self.comingNewsImgView = comingNewsImgView;
    
    //
    UIView *baseInforOperateBgView = [UIView new];
    baseInforOperateBgView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    [self.bgView addSubview:baseInforOperateBgView];
    self.baseInforOperateBgView = baseInforOperateBgView;
    //
    
    UIButton *patientInforBtn = [UIButton new];
    //    patientInforBtn.backgroundColor = [UIColor greenColor];
    [patientInforBtn addTarget:self action:@selector(sickInfoBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [patientInforBtn setImage:[UIImage imageNamed:@"sickInfo"] forState:UIControlStateNormal];
    [patientInforBtn setTitle:@"病情资料" forState:UIControlStateNormal];
    [patientInforBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    [patientInforBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, kP(20))];
    [patientInforBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, kP(20), 0, 0)];
    patientInforBtn.titleLabel.font = [UIFont systemFontOfSize:kP(30)];
    [self.baseInforOperateBgView addSubview:patientInforBtn];
    self.patientInforBtn = patientInforBtn;
    //
    UIView *VSepLine = [UIView new];
    VSepLine.backgroundColor = [UIColor colorWithHexString:@"#D9D9D9" alpha:1.0];
    [self.baseInforOperateBgView addSubview:VSepLine];
    self.VSepLine = VSepLine;
    //
    UIButton *checkReportBtn = [UIButton new];
    //    checkReportBtn.backgroundColor = [UIColor blueColor];
    [checkReportBtn addTarget:self action:@selector(checkReportBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [checkReportBtn setImage:[UIImage imageNamed:@"checkReport"] forState:UIControlStateNormal];
    [checkReportBtn setTitle:@"检查报告" forState:UIControlStateNormal];
    [checkReportBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    [checkReportBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, kP(20))];
    [checkReportBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, kP(20), 0, 0)];
    checkReportBtn.titleLabel.font = [UIFont systemFontOfSize:kP(30)];
    [self.baseInforOperateBgView addSubview:checkReportBtn];
    self.checkReportBtn = checkReportBtn;
    //
   
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    //
    [self setSubViewsContent];
    
    
    
    
    //
    CGFloat bgViewX = kP(30);
    CGFloat bgViewY = kP(25);
    CGFloat bgViewW = self.width - bgViewX * 2;
    CGFloat bgViewH = self.height - bgViewY * 2;
    self.bgView.frame = CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH);
    
    //
    CGFloat baseInfoBgViewX = kP(0);
    CGFloat baseInfoBgViewY = kP(0);
    CGFloat baseInfoBgViewW = bgViewW;
    CGFloat baseInfoBgViewH = bgViewH - kP(80);
    self.baseInforBgView.frame = CGRectMake(baseInfoBgViewX, baseInfoBgViewY, baseInfoBgViewW, baseInfoBgViewH);
    [self.baseInforBgView cornerSideType:UIRectCornerTopLeft | UIRectCornerTopRight withCornerRadius:kP(16)];
    
    //
    CGFloat IDImageViewX = kP(30);
    CGFloat IDImageViewY = kP(30);
    CGFloat IDImageViewWH = kP(44);
    self.IDImageView.frame = CGRectMake(IDImageViewX, IDImageViewY, IDImageViewWH, IDImageViewWH);
    
    //
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    CGFloat baseInforOperateBgViewW = bgViewW;
    CGFloat baseInforOperateBgViewH = kP(78);
    CGFloat baseInforOperateBgViewX = 0;
    CGFloat baseInforOperateBgViewY = self.bgView.height - baseInforOperateBgViewH;
    self.baseInforOperateBgView.frame = CGRectMake(baseInforOperateBgViewX, baseInforOperateBgViewY, baseInforOperateBgViewW, baseInforOperateBgViewH);
    [self.baseInforOperateBgView cornerSideType:UIRectCornerBottomLeft | UIRectCornerBottomRight withCornerRadius:kP(16)];
    
    // 病情资料
    CGFloat patientInforBtnX = 0;
    CGFloat patientInforBtnY = 0;
    CGFloat patientInforBtnW = (self.baseInforOperateBgView.width - kP(1)) * 0.5;
    CGFloat patientInforBtnH = self.baseInforOperateBgView.height;
    self.patientInforBtn.frame = CGRectMake(patientInforBtnX, patientInforBtnY, patientInforBtnW, patientInforBtnH);
    
    //
    CGFloat VSepLineX = CGRectGetMaxX(self.patientInforBtn.frame);
    CGFloat VSepLineY = kP(20);
    CGFloat VSepLineW = kP(2);
    CGFloat VSepLineH = self.baseInforOperateBgView.height - 2 * VSepLineY;
    self.VSepLine.frame = CGRectMake(VSepLineX, VSepLineY, VSepLineW, VSepLineH);
    
    // 检查报告
    CGFloat checkReportBtnX = CGRectGetMaxX(self.VSepLine.frame);
    CGFloat checkReportBtnY = 0;
    CGFloat checkReportBtnW = self.patientInforBtn.width;
    CGFloat checkReportBtnH = self.patientInforBtn.height;
    self.checkReportBtn.frame = CGRectMake(checkReportBtnX, checkReportBtnY, checkReportBtnW, checkReportBtnH);
    
    //
    CGSize IDSize = [self.IDLabel.text getTextSizeWithMaxWidth:kP(420) textFontSize:kP(38)];
    CGFloat IDLabelW = IDSize.width;
    CGFloat IDLabelH = IDSize.height;
    CGFloat IDLabelX = CGRectGetMaxX(self.IDImageView.frame) + kP(24);
    CGFloat IDLabelY = CGRectGetMidY(self.IDImageView.frame) - IDLabelH * 0.5;
    self.IDLabel.frame = CGRectMake(IDLabelX, IDLabelY, IDLabelW, IDLabelH);
    
    
    
    //
    CGFloat HSepLineX = 0;
    CGFloat HSepLineY = CGRectGetMaxY(self.IDImageView.frame) + kP(30);
    CGFloat HSepLineW = bgViewW;
    CGFloat HSepLineH = kP(2);
    self.HSepLine.frame = CGRectMake(HSepLineX, HSepLineY, HSepLineW, HSepLineH);
    
    
    //    CGSize illNameSize = [self.illNameLabel.text getTextSizeWithMaxWidth:kP(520) textFontSize:kP(38)];
    CGFloat illNameLabelH = kP(36);
    CGFloat illNameLabelX = kP(100);
    CGFloat illNameLabelW = self.width - 2 * illNameLabelX;
    CGFloat illNameLabelY = CGRectGetMaxY(self.HSepLine.frame) + kP(20);
    self.illNameLabel.frame = CGRectMake(illNameLabelX, illNameLabelY, illNameLabelW, illNameLabelH);
    
    
    CGSize patientInforSize = [self.patientInforLabel.text getTextSizeWithMaxWidth:kP(500) textFontSize:kP(38)];
    CGFloat patientInforLabelW = patientInforSize.width;
    CGFloat patientInforLabelH = patientInforSize.height;
    CGFloat patientInforLabelX = illNameLabelX;
    CGFloat patientInforLabelY = CGRectGetMaxY(self.illNameLabel.frame) + kP(24);
    self.patientInforLabel.frame = CGRectMake(patientInforLabelX, patientInforLabelY, patientInforLabelW, patientInforLabelH);
    
    //
    
//    CGFloat comingNewsImgViewWH = kP(40);
//    CGFloat comingNewsImgViewX = self.baseInforBgView.width - comingNewsImgViewWH - kP(54);
//    CGFloat comingNewsImgViewY = self.baseInforBgView.height - comingNewsImgViewWH - kP(54);
//    self.comingNewsImgView.frame = CGRectMake(comingNewsImgViewX, comingNewsImgViewY, comingNewsImgViewWH, comingNewsImgViewWH);
    //    CGSize comingNewsSize = [self.comingNewsImgView.text getTextSizeWithMaxWidth:kP(400) textFontSize:kP(32)];
    //    self.comingNewsLabel.width = comingNewsSize.width + kP(10);
    //    self.comingNewsLabel.height = comingNewsSize.height + kP(2);
    //    self.comingNewsLabel.x = self.bgView.width - self.comingNewsLabel.width - kP(28);
    //    self.comingNewsLabel.y = CGRectGetMidY(self.patientInforLabel.frame) - self.comingNewsLabel.height * 0.5;
}



- (void)setSubViewsContent {
    self.IDImageView.image = [UIImage imageNamed:@"remoteOrder"];
    self.IDLabel.text = @"树兰医院互联网门诊";
    self.leftTimeLabel.text = @"即将开始";
    self.illNameLabel.text = @"系膜增生性肾炎";
    self.patientInforLabel.text = @"陈某某  男  38岁";
    self.exampleImageView.image = [UIImage imageNamed:@"example.png"];
}


- (void)sickInfoBtnClick:(UIButton *)sickInfoBtn {
    NSLog(@"-------enterSickInfo-qwe-----------%@", self.exampleOrder);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterExampleSickInfoVCtr:)]) {
        [self.delegate enterExampleSickInfoVCtr:self.exampleOrder];
    }
    
}

- (void)checkReportBtnClick:(UIButton *)checkReportBtn {
    NSLog(@"-------enterCheckReport------------");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterExampleCheckReportVCtr:)]) {
        [self.delegate enterExampleCheckReportVCtr:self.exampleOrder];
    }
    
}


- (HZOrder *)exampleOrder {
    
    if (!_exampleOrder) {
        _exampleOrder = [HZOrder new];
        _exampleOrder.remoteAccount = @"iOSTestRemoteAccount:example123456";
        _exampleOrder.localAccount = @"iOSTestLocalAccount:example123456";
        _exampleOrder.bookingOrderId = @"iOSTestBookingOrderId:example123456";
        _exampleOrder.expertOpenid = @"iOSTestExpertOpenid:example123456";
        _exampleOrder.orderType = @"02";
        //    exampleRemoteOrder.localAccount = @"";
        _exampleOrder.localDoctorName = @"夏惠民";
        _exampleOrder.outpatientName = @"陈某某(男, 38岁)";
        _exampleOrder.icd10Value = @"系膜增生性肾炎";
        _exampleOrder.sicknessRemark = @"面部及两下肢浮肿2个月，伴腰膝酸软，体重增加，纳呆，恶心及重度蛋白尿。无血尿及尿路感染刺激征。发病前一月内无发热、咽痛或关节痛史。收缩期血压稍增高，心肺正常，肝脾不大；轻度贫血，尿素氮及肌酐不增加，总胆固醇显著增多";
        HZIllnessDataImg *illnessDataImg = [HZIllnessDataImg new];
        illnessDataImg.reportImageUri = @"9eb95dd6bab349f3b16642fe4e36f0de.png";
        _exampleOrder.laboratoryReports = @[illnessDataImg];
        HZIllnessDataImg *illnessDataImg1 = [HZIllnessDataImg new];
         illnessDataImg1.reportImageUri = @"7f660bef8f694e0991632d532a65e014.png";
        _exampleOrder.examReports = @[illnessDataImg1];
        //调试
    }
    
    return _exampleOrder;
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"exampleCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

@end
