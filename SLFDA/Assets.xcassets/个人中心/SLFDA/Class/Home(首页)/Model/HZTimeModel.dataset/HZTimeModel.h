//
//  HZTimeModel.h
//  HZTimeDownerDemo
//
//  Created by 季怀斌 on 2016/11/30.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZTimeModel : NSObject
@property (nonatomic, assign) int timeCount;
+ (instancetype)timeModelWithTime:(int)time;
- (void)countDown;
- (NSString *)currentTimeString;
@end
