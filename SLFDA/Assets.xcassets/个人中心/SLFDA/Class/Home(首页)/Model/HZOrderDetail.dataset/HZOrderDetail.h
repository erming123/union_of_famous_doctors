//
//  HZOrderDetail.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//




#import "HZJsonModel.h"
@class HZLocalPatient, HZRemoteExpert;
@interface HZOrderDetail : HZJsonModel
//
@property (nonatomic, strong) HZLocalPatient *localPatient;
@property (nonatomic, strong) HZRemoteExpert *remoteExpert;
@end
