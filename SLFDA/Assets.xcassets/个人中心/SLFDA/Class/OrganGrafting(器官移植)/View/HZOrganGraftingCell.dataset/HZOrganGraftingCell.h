//
//  HZOrganGraftingCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface HZOrganGraftingCell : UITableViewCell
@property (nonatomic, strong) NSDictionary *patientAddDict;
- (void)setNewsLabelhidden;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
