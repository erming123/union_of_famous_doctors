//
//  HZActivityIndicatorView.h
//  HZLoadingViewDemo
//
//  Created by 季怀斌 on 16/9/27.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZActivityIndicatorView : UIView
@property (nonatomic, copy) NSString *activityIndicatorText;
@end
