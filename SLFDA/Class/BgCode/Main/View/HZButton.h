//
//  HZButton.h
//  HZVideoDemo
//
//  Created by 季怀斌 on 16/7/21.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZButton : UIButton
@property (nonatomic,assign) CGRect titleRect;
@property (nonatomic,assign) CGRect imageRect;
@end
