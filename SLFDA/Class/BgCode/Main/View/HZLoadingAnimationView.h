//
//  HZLoadingAnimationView.h
//  SLIOP
//
//  Created by 季怀斌 on 16/9/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZLoadingAnimationView : UIView
- (void)startLoadingAnimation;
- (void)stopLoadingAnimation;
@end
