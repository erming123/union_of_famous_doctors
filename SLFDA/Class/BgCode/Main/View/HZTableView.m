//
//  HZTableView.m
//  SLIOP
//
//  Created by 季怀斌 on 16/8/18.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZTableView.h"

@implementation HZTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style {
    if (self = [super initWithFrame:frame style:style]) {
        self.separatorColor = [UIColor colorWithHexString:@"dfdfdf" alpha:1.0];
        self.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
        
        self.estimatedRowHeight = 0;
        self.estimatedSectionFooterHeight = 0;
        self.estimatedSectionHeaderHeight = 0;
    }

    return self;
}


@end
