//
//  HZButton.m
//  HZVideoDemo
//
//  Created by 季怀斌 on 16/7/21.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZButton.h"
@implementation HZButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor= [UIColor redColor];
    }
    return self;
}



- (CGRect)titleRectForContentRect:(CGRect)contentRect{
    if (!CGRectIsEmpty(self.titleRect) && !CGRectEqualToRect(self.titleRect, CGRectZero)) {
        return self.titleRect;
    }
    return [super titleRectForContentRect:contentRect];
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect{
    
    if (!CGRectIsEmpty(self.imageRect) && !CGRectEqualToRect(self.imageRect, CGRectZero)) {
        return self.imageRect;
    }
    return [super imageRectForContentRect:contentRect];
}

@end
