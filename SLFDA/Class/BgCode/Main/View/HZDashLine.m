//
//  HZDashLine.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/7/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZDashLine.h"

@implementation HZDashLine

- (void)drawRect:(CGRect)rect {
    
    CGContextRef cont = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(cont, [UIColor colorWithHexString:@"e6e6e6" alpha:1.0].CGColor);
    
    CGContextSetLineWidth(cont, self.height);// 2:虚线高度
    CGFloat lengths[] = {5,5};
    CGContextSetLineDash(cont, 0, lengths, 2);  //画虚线
    CGContextBeginPath(cont);
    CGContextMoveToPoint(cont, 0.0, self.height * 0.5);    //开始画线
    CGContextAddLineToPoint(cont, self.width, self.height * 0.5);
    CGContextStrokePath(cont);
}
@end
