//
//  HZActivityIndicatorView.m
//  HZLoadingViewDemo
//
//  Created by 季怀斌 on 16/9/27.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZActivityIndicatorView.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZActivityIndicatorView ()
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) UILabel *textLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZActivityIndicatorView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews {

    self.backgroundColor = [UIColor blackColor];
    self.alpha = 0.53;
    self.layer.cornerRadius = kP(20);
    self.clipsToBounds = YES;
    self.hidden = YES;
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicatorView setHidesWhenStopped:YES];
    [self addSubview:activityIndicatorView];
    [self bringSubviewToFront:activityIndicatorView];
    [activityIndicatorView startAnimating];
    self.activityIndicatorView = activityIndicatorView;
    
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.font = [UIFont systemFontOfSize:kP(28)];
    textLabel.textColor = [UIColor whiteColor];
    textLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:textLabel];
    self.textLabel = textLabel;

}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.activityIndicatorView.width = kP(72);
    self.activityIndicatorView.height = kP(72);
    self.activityIndicatorView.x = self.width * 0.5 - self.activityIndicatorView.width * 0.5;
    self.activityIndicatorView.y = kP(58);


    [self.textLabel sizeToFit];
    self.textLabel.x = self.width * 0.5 - self.textLabel.width * 0.5;
    self.textLabel.y = self.activityIndicatorView.bottom + kP(48);
}

- (void)setActivityIndicatorText:(NSString *)activityIndicatorText {
    _activityIndicatorText = activityIndicatorText;
    self.textLabel.text = activityIndicatorText;
}

@end
