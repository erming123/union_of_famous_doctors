//
//  HZLabel.m
//  SLIOP
//
//  Created by 季怀斌 on 16/8/18.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZLabel.h"

@implementation HZLabel
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    }
    return self;
}
@end
