//
//  HZCellHightCounter.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZCellHightCounter : NSObject
- (CGFloat)cellHeightWithStrFont:(UIFont *)strFont maxWidth:(CGFloat)maxWidth countStr:(NSString *)countStr;
@end
