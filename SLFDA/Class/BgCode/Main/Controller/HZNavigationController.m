//
//  HZNavigationController.m
//  SLIOP
//
//  Created by 季怀斌 on 16/8/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZNavigationController.h"
#import "HZIMChatViewController.h"
#import "HZWebImageContentViewController.h"
@interface HZNavigationController ()
@property (nonatomic, assign) BOOL shouldLightConent;
@end

@implementation HZNavigationController

+ (void)initialize {
    
    if (self == [HZNavigationController class]) {
        [self setUpNavBarTitleFont];
        [self setUpNavBarButton];
    }
    
    
    
}


+ (void)setUpNavBarTitleFont {
    UINavigationBar *navigationBar;
//    if (kCurrentSystemVersion < 9) {
//        navigationBar  = [UINavigationBar appearanceWhenContainedIn:self, nil];
//    } else {
        navigationBar = [UINavigationBar appearance];
//    }

//    UINavigationBar *navigationBar = [UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[self]];
    [navigationBar setTintColor:[UIColor colorWithHexString:@"#4a4a4a"]];// 左右的字体颜色
    NSDictionary *dict = [NSDictionary dictionary];
    dict = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:kP(36)], NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#4a4a4a"]};// 中间的字体颜色
    [navigationBar setBarTintColor:[UIColor colorWithHexString:@"#FAFBFC"]];// 背景渲染
}

+ (void)setUpNavBarButton {
     UIBarButtonItem *item;
//    if (kCurrentSystemVersion < 10) {
//        item  = [UIBarButtonItem appearanceWhenContainedIn:self, nil];
//    } else {
//        item = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[self]];
//    }
    item = [UIBarButtonItem appearance];

    
//    UIBarButtonItem *item = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[self]];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//    dict[NSForegroundColorAttributeName] = [UIColor whiteColor];
//    dict[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];// 只要是系统的就会起作用，不然就不起作用
    dict[NSForegroundColorAttributeName] = [UIColor redColor];// 只要是系统的就会起作用，不然就不起作用
    dict[NSFontAttributeName] = [UIFont systemFontOfSize:kP(32)];
    [item setTitleTextAttributes:dict forState:UIControlStateNormal];
}

/**
 *  重写push方法，设置子vc的导航条
 *
 *  @param viewController <#viewController description#>
 *  @param animated       <#animated description#>
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if (self.childViewControllers.count) {// 不是根控制器
       // viewController.navigationController.navigationBarHidden = NO;
        viewController.hidesBottomBarWhenPushed = YES;
        [viewController preferredStatusBarStyle];
    } else {
        viewController.hidesBottomBarWhenPushed = NO;
    }

   
    
    if (self.childViewControllers.count) {// 不是根控制器
       // viewController.navigationController.navigationBarHidden = NO;
        
        if ([viewController isKindOfClass:[NSClassFromString(@"HZExpertSearchViewController") class]] || [viewController isKindOfClass:[NSClassFromString(@"HZPatientSearchViewController") class]] || [viewController isKindOfClass:[NSClassFromString(@"HZHospitalSearchViewController") class]] || [viewController isKindOfClass:[NSClassFromString(@"HZSickNameSearchViewController") class]]) {
            [super pushViewController:viewController animated:animated];
            return;
        }
        
        
        //
        UIButton *popBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        btn.backgroundColor = [UIColor yellowColor];
        
        //        btn.titleLabel.font = [UIFont systemFontOfSize:kP(34)];
        //
        if ([viewController isKindOfClass:[NSClassFromString(@"HZWebImageContentViewController") class]]) {
            //
//            HZWebImageContentViewController *VCtr = (HZWebImageContentViewController *)viewController;
            self.shouldLightConent = YES;
            [popBtn setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
             [popBtn addTarget:self action:@selector(webPopBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            
            [popBtn setImage:[UIImage imageNamed:@"back_black"] forState:UIControlStateNormal];
             [popBtn addTarget:self action:@selector(backBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            //
            self.shouldLightConent = NO;
        }

       
        popBtn.contentHorizontalAlignment  = UIControlContentHorizontalAlignmentLeft;
        popBtn.imageEdgeInsets = UIEdgeInsetsMake(0, kP(0), 0, 0);
        UIView *leftView = [[UIView alloc] init];
//        leftView.backgroundColor = [UIColor redColor];
        leftView.frame = CGRectMake(0, 0, kP(200), kP(100));
        popBtn.frame = leftView.bounds;
        [leftView addSubview:popBtn];
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftView];
        //        space.width = - kP(100);
        //        viewController.navigationItem.leftBarButtonItems = @[space, leftItem];
        viewController.navigationItem.leftBarButtonItem = leftItem;
        
        
       
        //===============================
        [self preferredStatusBarStyle];
        //
    }
    
    [super pushViewController:viewController animated:animated];
    

}

#pragma mark -- 改变状态栏颜色
-(UIStatusBarStyle)preferredStatusBarStyle {
    if (self.shouldLightConent) {
        return UIStatusBarStyleLightContent;
    }
    return UIStatusBarStyleDefault;
}




#pragma mark -- 去掉分割线

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
//    if ([self.navigationController.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
//    NSLog(@"--------list-------------%@", list);

   
}

//-(void)setShadow{
//
//    self.navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.navigationBar.layer.shadowOpacity = 0.25;
//    self.navigationBar.layer.shadowOffset = CGSizeMake(0, kP(1));
//}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setShadow];
    // Do any additional setup after loading the view.
//    NSArray *list = self.navigationBar.subviews;
//    for (UIView *view in list) {
//        for (UIView *subView in view.subviews) {
//            if ([subView isKindOfClass:[UIImageView class]]) {
//                subView.hidden = YES;
//                return;
//            }
//        }
//    }
    
//    //改变tabbar 线条颜色
//    CGRect rect = CGRectMake(0, 0, HZScreenW, 1);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//
//    CGContextSetFillColorWithColor(context,
//                                   [UIColor colorWithHexString:@"#EEEEEE" alpha:1.0].CGColor);
//    CGContextFillRect(context, rect);
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    [self.tabBar setShadowImage:img];
//    [self.tabBar setBackgroundImage:[[UIImage alloc] init]];
    
//    UIImage *bgImage = [UIImage createImageWithColor:[UIColor clearColor] withSize:CGSizeMake(self.view.frame.size.width, 0.5)];
    UIImage *shadowImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#d9d9d9"] withSize:CGSizeMake(self.view.frame.size.width, 1)];
//    [self.navigationBar setBackgroundImage:bgImage forBarMetrics:UIBarMetricsDefault];

    [self.navigationBar setShadowImage:shadowImage];
    
    self.navigationBar.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark ----------------- backBtnClicked ------------------
- (void)backBtnClicked:(UIButton *)btn {
    
    if (self.backItemBlock != nil) {
        self.backItemBlock();
        return;
    }
    [self popViewControllerAnimated:YES];
}


#pragma mark ----------------- webPopBtnClicked ------------------
- (void)webPopBtnClicked:(UIButton *)webPopBtn {
    self.shouldLightConent = NO;
    [self preferredStatusBarStyle];
    [self popViewControllerAnimated:YES];
}
- (void)returnViewController:(BackItemBlock)backItemBlock {
    self.backItemBlock = backItemBlock;
}
@end
