//
//  HZNavigationController.h
//  SLIOP
//
//  Created by 季怀斌 on 16/8/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
//
@interface HZNavigationController : UINavigationController
typedef void(^BackItemBlock)();
@property (nonatomic, copy) BackItemBlock backItemBlock;
- (void)returnViewController:(BackItemBlock)backItemBlock;
@end
