//
//  HZTabBarController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZTabBarController.h"
#import "HZNavigationController.h"
#import "HZHomeViewController.h"
#import "HZExpertViewController.h"
#import "HZPatientViewController.h"
#import "HZProfileViewController.h"
@interface HZTabBarController () <UITabBarControllerDelegate>

@end

@implementation HZTabBarController

+ (void)initialize {
    UITabBarItem *item =  [UITabBarItem appearance];
    [item setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0]} forState:UIControlStateSelected];
    [item setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} forState:UIControlStateNormal];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpAllChildViewController];
    //    self.selectedIndex = 2;
    //    HBLog(@"%@", self.tabBar.subviews);
//    // 添加自定义的tabBar
//    HBTabBar *tabBar = [[HBTabBar alloc] initWithFrame:self.tabBar.frame];
//    [self setValue:tabBar forKeyPath:@"tabBar"];
//    //    HBLog(@"%@", self.tabBar.subviews);
//    tabBar.barTintColor = [UIColor whiteColor];
//    tabBar.translucent = YES;
    self.selectedIndex = 0;// ps: 放在上面不行。因为tabbar还没赋值。
    self.delegate = self;
    
    // 设置一个自定义 View,大小等于 tabBar 的大小
    UIView *bgView = [[UIView alloc] initWithFrame:self.tabBar.bounds];
    // 给自定义 View 设置颜色
    bgView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    // 将自定义 View 添加到 tabBar 上
    [self.tabBar insertSubview:bgView atIndex:0];
    
    //
    //改变tabbar 线条颜色
    CGRect rect = CGRectMake(0, 0, HZScreenW, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context,
                                   [UIColor colorWithHexString:@"#EEEEEE" alpha:1.0].CGColor);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.tabBar setShadowImage:img];
    [self.tabBar setBackgroundImage:[[UIImage alloc] init]];

}

//- (void)viewWillAppear:(BOOL)animated {
//    self.tabBar.hidden = NO;
//}

//- (void)setUser:(HZUser *)user {
//    _user = user;
//}

- (void)setUpAllChildViewController {
    
  
    // 会诊
    HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
    NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
//    user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
    //                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:user.openid];
    
//    NSLog(@"----user.remoteAccount-----%@", user.remoteAccount);
    
    homeVCtr.user = user;
    
    [self setUpOneChildViewController:homeVCtr imageName:@"home" selImageName:@"homeSel" title:@"会诊"];
    
    // 专家
    HZExpertViewController *expertViewCtr = [[HZExpertViewController alloc] init];
    [self setUpOneChildViewController:expertViewCtr imageName:@"expert" selImageName:@"expertSel" title:@"专家"];
    
    // 患者
    HZPatientViewController *patientVCtr = [[HZPatientViewController alloc] init];
    [self setUpOneChildViewController:patientVCtr imageName:@"patient" selImageName:@"patientSel" title:@"患者"];
    
    
    // 我的
    HZProfileViewController *profileVCtr = [[HZProfileViewController alloc] init];
    profileVCtr.user = user;
    [self setUpOneChildViewController:profileVCtr imageName:@"profile" selImageName:@"profileSel" title:@"我的"];
}



- (void)setUpOneChildViewController:(UIViewController *)VCtr  imageName:(NSString *)imageName selImageName:(NSString *)selImageName title:(NSString *)title {
    
    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:VCtr];
    [self addChildViewController:navCtr];
    
    //
    navCtr.tabBarItem.title = title;
    navCtr.tabBarItem.image = [UIImage imageWithOriginalName:imageName];
    navCtr.tabBarItem.selectedImage = [UIImage imageWithOriginalName:selImageName];
}


- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
    //    NSLog(@"-----viewController----%@", viewController);
    
    UINavigationController *normalNavCtr = (UINavigationController *)viewController;
    if (tabBarController.childViewControllers[0] == (HZNavigationController *)viewController) {
        //        NSLog(@"---------sdfsfds");
        //        [normalNavCtr.navigationBar setTintColor:[UIColor colorWithHexString:@"#4a4a4a" alpha:1.0]];// 左右的字体颜色
        //        NSDictionary *dict = [NSDictionary dictionary];
        //        dict = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:kP(36)], NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#4a4a4a" alpha:1.0]};// 中间的字体颜色
        //        [normalNavCtr.navigationBar setBarTintColor:[UIColor colorWithHexString:@"34C6B2" alpha:1.0]];// 背景渲染 34C6B2
        
        
        //        NSLog(@"------eee---%@", normalNavCtr.childViewControllers);
        //
        //        if ([normalNavCtr.childViewControllers[0] isKindOfClass:NSClassFromString(@"HZWardViewController")]) {
       // normalNavCtr.navigationBarHidden = YES;
        
        //        } else {
        //            normalNavCtr.navigationBarHidden = NO;
        //        }
        
    }
}

//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
//   
//    if (tabBarController.childViewControllers[0] == (HZNavigationController *)viewController) {
//        
//         UINavigationController *normalNavCtr = (UINavigationController *)viewController;
//         normalNavCtr.navigationBarHidden = YES;
//        return YES;
//    }
//    
//    return YES;
//}


//
//
//- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
//
////    [self imageAnimation];
//
//}
//
//
//
//


//- (void)viewDidAppear:(BOOL)animated {
//    
//    for (UIView *lineView in self.tabBar.subviews) {
//        
//        if ([lineView isKindOfClass:NSClassFromString(@"UIImageView")] && lineView.height <= 1) {
//            //                UIImageView *line = (UIImageView *)lineView;
//            //                line.backgroundColor = [UIColor redColor];
//            lineView.backgroundColor = [UIColor lightGrayColor];
//            
//            //                lineView.hidden = YES;
//            [self.tabBar sendSubviewToBack:lineView];
//        }
//    }
//    
//    //    [self.tabBarController.tabBar setShadowImage:[UIImage imageNamed:@"line"]];
//    //    HBLog(@"dffffdfd%@", self.tabBar.subviews);
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
