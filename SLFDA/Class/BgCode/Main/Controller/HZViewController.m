//
//  HZViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/10/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZViewController.h"
#import <objc/message.h>

@interface HZViewController ()<UIGestureRecognizerDelegate>

@end

@implementation HZViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // default
    
    self.isShouldPanToPopBack = YES;
    
    if ([self isKindOfClass:NSClassFromString(@"HZWebImageContentViewController")]) return;
    
    id target = self.navigationController.interactivePopGestureRecognizer.delegate;
    
    // handleNavigationTransition:为系统私有API,即系统自带侧滑手势的回调方法，我们在自己的手势上直接用它的回调方法
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    panGesture.delegate = self; // 设置手势代理，拦截手势触发
    [self.view addGestureRecognizer:panGesture];
    
    // 一定要禁止系统自带的滑动手势
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    //
    self.view.backgroundColor = [UIColor whiteColor];
}



// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    
    
    
    // 当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1)
    {
        return NO;
    }
    
    
    if ([gestureRecognizer isKindOfClass:NSClassFromString(@"UIPanGestureRecognizer")]) {
        UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer *)gestureRecognizer;
        //
        CGPoint translatedPoint = [panGesture translationInView:self.view];
        
        NSLog(@"----------s-------:%lf", translatedPoint.x);
        //
        if(translatedPoint.x < 0 && ![self isKindOfClass:NSClassFromString(@"HZImageContentViewController")])// 向左
        {
            return NO;
        }
    }
    
    return self.isShouldPanToPopBack;
    
}


- (void)setIsShouldPanToPopBack:(BOOL)isShouldPanToPopBack {
    _isShouldPanToPopBack = isShouldPanToPopBack;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end


