//
//  HZOrderHttpsTool.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/10/28.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HZOrder;
@class HZPayOrder;
@interface HZOrderHttpsTool : NSObject

//
+ (void)getOrdersWithPages:(NSInteger)pages countPerPage:(NSInteger)count stateCode:(NSString *)stateCode success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)getOrderDetailWithOrderID:(NSString *)orderID success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)getBookingOrderStateStrWithVersion:(NSString *)version  success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
+ (NSString *)getBookingOrderStateAlertStringWithVersion:(NSString *)version order:(HZOrder *)order;

//
+ (void)getPayParamsWithBookingOrderId:(NSString *)bookingOrderId success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//
+ (void)getWeChatPayIdWithPayOrder:(HZPayOrder *)payOrder success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//
+ (void)getAliPayIdWithPayOrder:(HZPayOrder *)payOrder success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//
+ (void)getQRImageWithRelationId:(NSString *)relationId success:(void(^)(NSData *QRImageData))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)getOrderPayStateWithPayWay:(NSString *)payWay OutTradeNo:(NSString *)outTradeNo success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)getOrderPayDetailWithOrderId:(NSString *)OrderId openOrderId:(NSString *)openOrderId success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
////
+ (void)endLocaOrderWithBookingOrderId:(NSString *_Nullable)bookingOrderId success:(void (^_Nullable)(id _Nullable responseObject))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
