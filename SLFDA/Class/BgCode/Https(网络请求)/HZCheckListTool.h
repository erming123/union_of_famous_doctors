//
//  HZCheckListTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/12/2.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HZPatient;
@interface HZCheckListTool : NSObject
+ (void)getCheckListWithBookingOrderId:(NSString *)bookingOrderId patient:(HZPatient *)patient userHospitalId:(NSString *)userHospitalId success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
+ (void)getBiochemistryCheckListWithCheckModelId:(NSString *)checkModelId patientHospitalId:(NSString *)patientHospitalId success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
