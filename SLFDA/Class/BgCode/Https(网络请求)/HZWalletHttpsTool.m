//
//  HZWalletHttpsTool.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/25.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZWalletHttpsTool.h"
#import "HZHttpsTool.h"
#import "HZWallet.h"
//#define BasicwalletStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/wallet/"
//#define CapitalListStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/wallet/bills/get"
@implementation HZWalletHttpsTool
+ (void)getWalletInforWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *BasicwalletStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/wallet/");
    NSString *getWalletInforStr = [NSString stringWithFormat:@"%@info/get", BasicwalletStr];
    [HZHttpsTool GET:getWalletInforStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
//        NSLog(@"-----wallet----%@", responseObject);
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"-----walletyy----%@", error);
        if (failure) {
            failure(task, error);
        }
    }];
}



+ (void)updateWalletInforWithWallet:(HZWallet *)wallet success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    //
    //  对象转字典
    NSDictionary *walletDict = @{@"depositBank":wallet.depositBank, @"bankAccount":wallet.bankAccount, @"bankAddress":wallet.bankAddress};
    NSString *BasicwalletStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/wallet/");
     NSString *updateWalletInforStr = [NSString stringWithFormat:@"%@update", BasicwalletStr];
    [HZHttpsTool POST:updateWalletInforStr headDomainValue:nil parameters:walletDict progress:nil success:^(id responseObject) {
        NSLog(@"-----更新成功----%@", responseObject);
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---------%@", error);
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)getCapitalListWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *CapitalListStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/wallet/bills/get");
    [HZHttpsTool GET:CapitalListStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"-----Listyy----%@", error);
        if (failure) {
            failure(task, error);
        }
    }];
    
}

- (void)getBankListWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *bankListStr = kGatewayUrlStr(@"dictionary/dict/batchcommon?params={typeVersion:[{\"type\":\"deposit_bank\",\"version\":\"1\"}]}");
    [HZHttpsTool GET:bankListStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSLog(@"-----ewewewrerwerwerwerwerwerwerw----%@", responseObject);
        if (success) {
            
            
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(task, error);
        }
        
    }];
}
@end
