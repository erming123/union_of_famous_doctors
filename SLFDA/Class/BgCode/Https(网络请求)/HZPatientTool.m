//
//  HZPatientTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/18.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientTool.h"
#import "HZHttpsTool.h"
@implementation HZPatientTool

+ (void)getPatientsWithHospitalId:(NSString *)hospitalId staffNum:(NSString *_Nullable)staffNum success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v2/patients/hospitals/%@/doctors/%@", hospitalId, staffNum];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}


+ (void)starPatientWithStaffNum:(NSString *)staffNum patientRecordId:(NSString *_Nullable)patientRecordId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v2/patients/doctors/%@/starreds/%@", staffNum, patientRecordId];
    [HZHttpsTool POST:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}


+ (void)unstarPatientWithStaffNum:(NSString *)staffNum patientRecordId:(NSString *_Nullable)patientRecordId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v2/patients/doctors/%@/starreds/%@", staffNum, patientRecordId];
    [HZHttpsTool Delete:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}


// 获取患者的住院信息
+ (void)getHospitalizePatientBasicInforWithHospitalId:(NSString *_Nullable)hospitalId patientCardId:(NSString *_Nullable)patientCardId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSLog(@"--ssss-------:%@----dddd----------:%@", hospitalId, patientCardId);
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v2/patients/hospitals/%@/%@", hospitalId, patientCardId];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (failure) {
                failure(task, error);
            }
        }];
}
@end
