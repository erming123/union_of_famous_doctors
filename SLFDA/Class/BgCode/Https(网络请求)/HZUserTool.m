//
//  HZUserTool.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/18.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZUserTool.h"
#import "HZHttpsTool.h"
#import "HZAccountTool.h"
#import "HZUser.h"
#import "JPUSHService.h"
//#define OrderUrlStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/user/info/get"
//#define AvatarUrlStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/doctor/photo/openid?openid="
// 别名,或者标签 设置状态码
static NSMutableDictionary *jpushSetCode;

@implementation HZUserTool
+ (void)getUserWithSuccess:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSString *OrderUrlStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/user/info/get");
    [HZHttpsTool GET:OrderUrlStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {

//        NSLog(@"--getUser成功-------%@", responseObject);
        
//        // 成功后设置 用户的 别名与 标签
//        [HZUserTool setUpJPUSHalisAndTagsWithUser:responseObject];
        
                if (success) {
                    success(responseObject);
                }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---getUser失败--%@", error);
        
        if (failure) {
            failure(task, error);
        }
    }];
    

}


+ (void)getUserDetailInforWithOpenId:(NSString *_Nullable)openId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *openIdUrlStr = [NSString stringWithFormat:@"cfdu/v2/doctor/elastic/%@", openId];
    [HZHttpsTool GET:kGatewayUrlStr(openIdUrlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---getDetailUserInfor失败--%@", error);
        
        if (failure) {
            failure(task, error);
        }
    }];
}


+ (void)getAvatarWithAvatarOpenId:(NSString *_Nullable)openId success:(void(^_Nullable)(UIImage *_Nullable image))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *AvatarUrlStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/photo/openid?openid=");
    NSString *avatarUrlStr = [NSString stringWithFormat:@"%@%@",AvatarUrlStr,openId];
    [HZHttpsTool GET:avatarUrlStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSString *avatarStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        avatarStr = [[avatarStr componentsSeparatedByString:@"base64,"] lastObject];
        NSData *_decodedImageData   = [[NSData alloc] initWithBase64EncodedString:avatarStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage *image = [UIImage imageWithData:_decodedImageData];
//        NSLog(@"------dsdsdssddsdsdsdssdsdsdsdsdssddsdsdsdsd---%@", image);
        
        if (success) {
            success(image);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        NSLog(@"------ddddddsssssssssss---%@", error);
        if (failure) {
            failure(task, error);
        }
    }];
}


//

//+ (void)getAvatarWithFaceUrl:(NSString *_Nullable)faceUrl success:(void(^_Nullable)(UIImage *_Nullable image,NSString * _Nullable urlKey))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
//    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v1/openapi/json/doctor/photo/get?url=%@", faceUrl];
//    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
//        
//        NSString *avatarStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//        avatarStr = [[avatarStr componentsSeparatedByString:@"base64,"] lastObject];
//        NSData *_decodedImageData   = [[NSData alloc] initWithBase64EncodedString:avatarStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
//        UIImage *image = [UIImage imageWithData:_decodedImageData];
//        if (success) {
//            success(image);
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        
//        
//        NSLog(@"-----sssaa----%@", error);
//        if (failure) {
//            failure(task, error);
//        }
//    }];
//
//}

#pragma mark -- 获取个人简介和擅长-
+ (void)getMyIntroduceWithSuccess:(void(^_Nullable)(id _Nonnull responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZHttpsTool GET:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/honor") headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        
        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        if (failure) {
            failure(task, error);
        }
    }];
}
+ (void)getMySkillWithSuccess:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZHttpsTool GET:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/speciality") headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        if (failure) {
            failure(task, error);
        }
    }];
}

#pragma mark -- 上传个人简介和擅长-
+ (void)upLoadMyIntroduceWithIntroduceStr:(NSString *_Nullable)introduceStr userOpenid:(NSString *_Nullable)userOpenid success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
//    NSLog(@"---123456------%@", introduceStr);
    NSDictionary *paramDict = @{@"honor" : introduceStr, @"openid" : userOpenid};
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor?_method=PUT") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)upLoadMySkillWithSkillStr:(NSString *_Nullable)skillStr userOpenid:(NSString *_Nullable)userOpenid success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSDictionary *paramDict = @{@"speciality" : skillStr, @"openid" : userOpenid};
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor?_method=PUT") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
//        NSLog(@"----upLoadSkill成功-----%@", responseObject);
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"-----upLoadSkill失败----%@", error);
        
        if (failure) {
            failure(task, error);
        }
    }];

}


#pragma mark -- 上传头像和证书
+ (void)upLoadMyAvatarWithAvatarImage:(UIImage *_Nullable)avatarImage userOpenid:(NSString *_Nullable)userOpenid progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progress success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZHttpsTool Upload:kGatewayUrlStr(@"doctor/aws/s3/upload") headDomainValue:nil uploadImage:avatarImage parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress);
        }
    } success:^(id responseObject) {
        NSString *imageUrlStr;
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSArray *filesArr = resultsDict[@"files"];
            imageUrlStr  = filesArr[0];
        }
        
        if ([NSString isBlankString:imageUrlStr]) return;
        
        NSDictionary *paramDict = @{@"facePhotoUrl" : imageUrlStr, @"openid" : userOpenid};
        [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor?_method=PUT") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if (failure) { //
                failure(task, error);
            }
        }];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}


+ (void)upLoadMyAptitudeWithAptitudeImage:(UIImage *_Nullable)aptitudeImage userId:(NSString *_Nullable)userId progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progress success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"doctor/v1/doctor/%@/authentication", userId];
    [HZHttpsTool Upload:kGatewayUrlStr(urlStr) headDomainValue:nil uploadImage:aptitudeImage parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress);
        }
    } success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

// 产看用户的数据是否已经对接。
+ (void)getUserDataButtedVersionWithHospitalId:(NSString *_Nullable)hospitalId success:(void(^_Nonnull)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v2/systems/versions/hospitals/%@", hospitalId];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}
@end
