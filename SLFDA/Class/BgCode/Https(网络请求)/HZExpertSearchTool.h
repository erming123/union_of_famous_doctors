//
//  HZExpertSearchTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface HZExpertSearchTool : NSObject
+ (void)getExpertArrWithDoctorName:(NSString *)expertName dictDepartmentId:(NSString *)dictDepartmentId isHotExpertSearch:(BOOL)isHotExpertSearch page:(NSInteger)page countPerPage:(NSInteger)countPerPage success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
+ (void)getPriceListSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
