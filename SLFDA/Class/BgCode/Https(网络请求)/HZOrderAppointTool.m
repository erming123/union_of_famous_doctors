//
//  HZOrderAppointTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZOrderAppointTool.h"
#import "HZHttpsTool.h"

#import "HZUser.h"
#import "HZPatient.h"
#import "HZSickNess.h"
@implementation HZOrderAppointTool
+ (void)getPatientInforWithOutPatientCode:(NSString *)outPatientCode hospitalId:(NSString *)hospitalId success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v1/openapi/json/his/patient/getByJid?jid=%@&hospital=%@", outPatientCode, hospitalId];
    
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)getSickNameWithSickNameKeyStr:(NSString *)sickNameKeyStr success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"dictionary/search/disease?params=%@", sickNameKeyStr];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)justPrivilegeNumberEnAbledWithPrivilegeNumber:(NSString *)privilegeNumber success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSDictionary *paramDict = @{@"code" : privilegeNumber};
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/coupon/code/validate") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)creatOrderWithExpert:(HZUser *)expert patient:(HZPatient *)patient sickNess:(HZSickNess *)sickNess sickNessDes:(NSString *)sickNessDes privilegeNumberStr:(NSString *)privilegeNumberStr success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSLog(@"-----expert----%@", expert);
    NSLog(@"------patient---%@", patient);
    NSLog(@"------patientSickNess---%@", sickNess);
    
    //
    expert.remoteExpertLevel =  ![NSString isBlankString:expert.remoteExpertLevel] ? expert.remoteExpertLevel : @"";
    expert.departmentGeneralName = ![NSString isBlankString:expert.departmentGeneralName] ? expert.departmentGeneralName : @"";
    expert.totalMoney =  ![NSString isBlankString:expert.totalMoney] ? expert.totalMoney : @"";
    patient.BRDHZC = ![NSString isBlankString:patient.BRDHZC] ? patient.BRDHZC : @"";
    patient.BRDAXM = ![NSString isBlankString:patient.BRDAXM] ? patient.BRDAXM : @"";
    patient.BRJZHM = ![NSString isBlankString:patient.BRJZHM] ? patient.BRJZHM : @"";
    patient.BRSFZH = ![NSString isBlankString:patient.BRSFZH] ? patient.BRSFZH : @"";
    patient.BRJZLX = ![NSString isBlankString:patient.BRJZLX] ? patient.BRJZLX : @"";
    expert.openid = ![NSString isBlankString:expert.openid] ? expert.openid : @"";
    expert.openid = ![NSString isBlankString:expert.openid] ? expert.openid : @"";
    
    
    NSString *genderValue = [patient.BRDAXB isEqualToString:@"1"] ? @"男" : @"女";
    NSString *age;
    if ([patient.BRCSRQ containsString:@"岁"]) {
        age = [patient.BRCSRQ stringByReplacingOccurrencesOfString:@"岁" withString:@""];
    } else {
        age = [self getAgeWithIDCardNumber:patient];
    }
    
    
    age = ![NSString isBlankString:age]? age : @"";
    patient.BRDAXB = ![NSString isBlankString:patient.BRDAXB] ? patient.BRDAXB : @"";
    genderValue = ![NSString isBlankString:genderValue] ? genderValue : @"";
    //
    sickNess.diseaseName = ![NSString isBlankString:sickNess.diseaseName] ? sickNess.diseaseName : @"";
    sickNess.diseaseCode = ![NSString isBlankString:sickNess.diseaseCode] ? sickNess.diseaseCode : @"";
    sickNessDes = ![NSString isBlankString:sickNessDes] ? sickNessDes : @"";
    //
    expert.totalMoney = [expert.totalMoney stringByReplacingOccurrencesOfString:@"¥" withString:@""];
    privilegeNumberStr = ![NSString isBlankString:privilegeNumberStr] ? privilegeNumberStr : @"";
    NSString *payTypeCodeStr = ![NSString isBlankString:privilegeNumberStr] ? @"002" : @"";
    //-------
    
    
    
    NSDictionary *paramDict = @{@"expertOpenid": expert.openid,
                                @"orderLevelCode": expert.remoteExpertLevel,
                                @"expertTitleCode": expert.titleId,
                                @"experDeptValue": expert.departmentGeneralName,// 注意：接口的问题，单词少个t
                                @"outpatientFee": expert.totalMoney,
                                @"outpatientPhone": patient.BRDHZC,
                                @"outpatientName": patient.BRDAXM,
                                @"outpatientRecordNumber": patient.BRJZHM,
                                @"outpatientIdcard": patient.BRSFZH,
                                @"outpatientGenderCode": patient.BRDAXB,
                                @"outpatientGenderValue": genderValue,
                                @"outpatientAge": age,
                                @"outpatientTypeCode" : patient.BRJZLX,
                                @"icd10Value": sickNess.diseaseName,
                                @"icd10Code": sickNess.diseaseCode,
                                @"sicknessRemark":sickNessDes,
                                @"payTypeCode" : payTypeCodeStr,
                                @"paySerialNumber" : privilegeNumberStr
                                };
    
    
    
    NSLog(@"-----dd----%@", [paramDict getDictJsonStr]);
    //
    //cfdu/v1/openapi/json/booking/order/addorupdate
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v2/order") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

//
+ (NSString *)getAgeWithIDCardNumber:(HZPatient *)patient {
    
    NSDate *now = [NSDate date];
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"yyyy"];//
    
    NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
    [df2 setDateFormat:@"MMdd"];//MMdd
    
    NSString *yearStrNew = [df1 stringFromDate:now];
    NSString *monthStrNew = [df2 stringFromDate:now];
    
    
    NSString *yearStr = patient.BRCSRQ.length > 0 ? [patient.BRCSRQ substringWithRange:NSMakeRange(0, 4)] : @"";
    NSString *monthStr = patient.BRCSRQ.length > 0 ? [patient.BRCSRQ substringWithRange:NSMakeRange(5, 2)] : @"";
    NSString *dayStr = patient.BRCSRQ.length > 0 ? [patient.BRCSRQ substringWithRange:NSMakeRange(8, 2)] : @"";
    NSString *MDStr = [NSString stringWithFormat:@"%@%@", monthStr, dayStr];
    NSInteger year = [yearStrNew intValue] - [yearStr intValue];
    NSInteger month = [monthStrNew intValue] - [MDStr intValue];
    
    if (month < 0) {
        year -= 1;
    }
    NSString *ageStr = [NSString stringWithFormat:@"%ld", year];
    return ageStr;
}



@end
