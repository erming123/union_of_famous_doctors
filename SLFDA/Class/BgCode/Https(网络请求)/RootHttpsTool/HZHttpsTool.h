//
//  HZHttpsTool.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/10/28.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZHttpsTool : NSObject
+ (void)GET:(NSString *)URLString  headDomainValue:(NSString *)headDomainValue parameters:(id)parameters progress:(void (^)(NSProgress * _Nonnull downloadProgress))progress
    success:(void (^)(id responseObject))success
    failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)POST:(NSString *)URLString headDomainValue:(NSString *)headDomainValue parameters:(id)parameters progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)Delete:(NSString *)URLString headDomainValue:(NSString *)headDomainValue parameters:(id)parameters progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)Upload:(NSString *)URLString headDomainValue:(NSString *)headDomainValue uploadImage:(UIImage *)image parameters:(id)parameters progress:(void (^)(NSProgress * _Nonnull uploadProgress))progress
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;



//+ (void)GET:(NSString *_Nullable)URLString  headDomainValue:(NSString *_Nullable)headDomainValue parameters:(id _Nullable )parameters
//    success:(void (^_Nullable)(id _Nullable responseObject))success
//    failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error, NSInteger statusCode))failure;
//
//+ (void)POST:(NSString *_Nullable)URLString headDomainValue:(NSString *_Nullable)headDomainValue parameters:(id _Nullable )parameters
//     success:(void (^_Nullable)(id _Nullable responseObject))success
//     failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error, NSInteger statusCode))failure;


//+ (void)get:(NSString *)url params:(NSDictionary *)params success:(void (^)(id d))success failure:(void (^)(NSError * s))failure;


//+ (AFSecurityPolicy*)customSecurityPolicy;
// 图片接口请求
+ (void)GET:(NSString *_Nullable)URLString success:(void (^_Nullable)(id _Nullable responseObject,NSURLSessionDataTask * _Nonnull task))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
