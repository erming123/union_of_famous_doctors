//
//  HZInforCompleteTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZInforCompleteTool : NSObject

//1.医院数组
+ (void)getHospitalArrWithHospitalName:(NSString *)hospitalName success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
//2.学科数组
+ (void)getSubjectArrWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
//3.职称数组
+ (void)getProfessionArrWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//4.上传到服务器
+ (void)upLoadUserInforWithHospital:(NSString *)hospitalStr subjectIdArr:(NSArray *)subjectIdArr subjectName:(NSString *)subjectName professionId:(NSString *)professionId userName:(NSString *)userName phoneNumber:(NSString *)phoneNumber inviteCode:(NSString *)inviteCodeStr  success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
