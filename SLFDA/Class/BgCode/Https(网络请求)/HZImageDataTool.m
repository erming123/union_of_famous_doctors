//
//  HZImageDataTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/23.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZImageDataTool.h"
#import "HZHttpsTool.h"
#import "AFNetworking.h"
@implementation HZImageDataTool
+ (void)getImageBaseInforWithJCYBID:(NSString *_Nullable)JCYBID patientHospitalId:(NSString *)patientHospitalId  success:(void (^_Nullable)(id _Nullable responseObject))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSString *imageBaseInforUrl = [NSString stringWithFormat:@"%@?jcybid=%@&hospitalId=%@",kGatewayUrlStr(@"hocpacs/metadata/findToJCYBID"), JCYBID, patientHospitalId];
    [HZHttpsTool GET:imageBaseInforUrl headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        
        if (failure) {
            failure(task, error);
        }
    }]; //
}

//+ (void)getImageZipWithZipDownloadPath:(NSString *_Nullable)zipDownloadPath fileName:(NSString *_Nullable)fileName success:(void (^_Nullable)(id _Nullable responseObject))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
//    
//    NSString *imageZipUrl = [NSString stringWithFormat:@"%@?key=%@&fileName=%@",kGatewayUrlStr(@"hocpacs/v1/tool/download"), zipDownloadPath, fileName];
//    
//    [HZHttpsTool GET:imageZipUrl headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
//        
//        
//        if (success) {
//            success(responseObject);
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        
//        
//        if (failure) {
//            failure(task, error);
//        }
//    }];
//}
@end
