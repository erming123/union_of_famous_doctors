//
//  HZPhysicanAdviceTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/12.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPhysicianAdviceTool.h"
#import "HZHttpsTool.h"
@implementation HZPhysicianAdviceTool
// 根据身份证号,是否当日, 医嘱类型(医疗/用药), 医嘱时长(长期/临时)获取医嘱数据
+ (void)getPhysicanAdviceDataWithHodiernalStr:(NSString *)hodiernalStr adviceTypeStr:(NSString *_Nullable)adviceTypeStr adviceCycleTimeStr:(NSString *_Nullable)adviceCycleTimeStr cardId:(NSString *_Nullable)cardId success:(void(^_Nonnull)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    //
    NSString *urlStr = kGatewayUrlStr(@"cfdu/v2/doctor_advice/hospitals/34/query");
    //
    NSDictionary *paramDict;
    //
    BOOL isHodiernal = [hodiernalStr isEqualToString:@"今日医嘱"];
    //
    NSString *adviceType = @"";
    if ([adviceTypeStr isEqualToString:@"诊疗医嘱"]) {
        adviceType = @"CLINIC";
    } else if ([adviceTypeStr isEqualToString:@"用药医嘱"]) {
        adviceType = @"OTHERS";
    }
    
    //
    NSString *adviceCycleTime = @"";
    if ([adviceCycleTimeStr isEqualToString:@"长期医嘱"]) {
        adviceCycleTime = @"LONG_TERM";
    } else if ([adviceCycleTimeStr isEqualToString:@"临时医嘱"]) {
        adviceCycleTime = @"TEMPORARY";
    }
    
    if (isHodiernal) {
        paramDict = @{@"queryStartTime" : [NSString stringWithFormat:@"%@ 00:00:00", [NSString getCurrentTimeWithFormatter:nil]],
                      @"queryEndTime" : [NSString stringWithFormat:@"%@ 23:59:59", [NSString getCurrentTimeWithFormatter:nil]],
                      @"adviceType" : adviceType,
                      @"adviceCycleTime" : adviceCycleTime,
                      @"cardId" : cardId
                      };
    } else {
        paramDict = @{@"adviceType" : adviceType,
                      @"adviceCycleTime" : adviceCycleTime,
                      @"cardId" : cardId
                      };
    }
    
    NSMutableDictionary *paramMutableDict = [NSMutableDictionary dictionaryWithDictionary:paramDict];
    
    if ([NSString isBlankString:adviceType]) {
        [paramMutableDict removeObjectForKey:@"adviceType"];
    }
    
    if ([NSString isBlankString:adviceCycleTime]) {
        [paramMutableDict removeObjectForKey:@"adviceCycleTime"];
    }
    //
    NSLog(@"----paramDict-----%@", [paramMutableDict getDictJsonStr]);

    //
    [HZHttpsTool POST:urlStr headDomainValue:nil parameters:paramMutableDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}


// 根据身份证号获取医嘱数据
+ (void)getPhysicanAdviceDataWithCardId:(NSString *_Nullable)cardId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    //
    NSString *urlStr = kGatewayUrlStr(@"cfdu/v2/doctor_advice/hospitals/34/query");
    //
    NSDictionary *paramDict = @{@"cardId" : cardId};
    NSLog(@"------paramDict--------:%@", [paramDict getDictJsonStr]);
    [HZHttpsTool POST:urlStr headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

@end
