//
//  HZInforCompleteTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZInforCompleteTool.h"
#import "HZHttpsTool.h"
@implementation HZInforCompleteTool
//1.医院数组
+ (void)getHospitalArrWithHospitalName:(NSString *)hospitalName success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
    NSDictionary *paramDict = @{@"hospitalname" : hospitalName};
    
    NSLog(@"-----sss----%@", paramDict);
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v1/openapi/json/dict/hospital") headDomainValue:headDomainValue parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];

}
//2.科室数组
+ (void)getSubjectArrWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
    [HZHttpsTool GET:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor/depts") headDomainValue:headDomainValue parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}
//3.职称数组
+ (void)getProfessionArrWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    NSString *headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
    [HZHttpsTool GET:kGatewayUrlStr(@"cfdu/v1/openapi/json/dict/23") headDomainValue:headDomainValue parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

//4.上传到服务器
+ (void)upLoadUserInforWithHospital:(NSString *)hospitalStr subjectIdArr:(NSArray *)subjectIdArr subjectName:(NSString *)subjectName professionId:(NSString *)professionId userName:(NSString *)userName phoneNumber:(NSString *)phoneNumber inviteCode:(NSString *)inviteCodeStr  success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];

    
    //
    NSDictionary *paramDict = @{@"hospitalName" : hospitalStr,// 医院名称
                                @"doctorDictDepartment" : subjectIdArr,// 学科Id数组
                                @"departmentGeneralName" : subjectName,
                                @"titleId" : professionId,// 职称Id
                                @"userName" : userName,// 姓名
                                @"phone" : phoneNumber,// 手机号
                                @"applyOpenExpert" : @"1" //
                                };
    NSLog(@"-----dd----%@", [paramDict getDictJsonStr]);
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v1/openapi/json/doctor") headDomainValue:headDomainValue parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}
@end
