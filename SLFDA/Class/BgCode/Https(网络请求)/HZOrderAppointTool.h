//
//  HZOrderAppointTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HZUser, HZPatient, HZSickNess;
@interface HZOrderAppointTool : NSObject

//1. 获取就诊卡号
+ (void)getPatientInforWithOutPatientCode:(NSString *)outPatientCode hospitalId:(NSString *)hospitalId success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//2. 获取疾病列表
+ (void)getSickNameWithSickNameKeyStr:(NSString *)sickNameKeyStr success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
//3. 获取优惠码
+ (void)justPrivilegeNumberEnAbledWithPrivilegeNumber:(NSString *)privilegeNumber success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
//4. 提交预约单
+ (void)creatOrderWithExpert:(HZUser *)expert patient:(HZPatient *)patient sickNess:(HZSickNess *)sickNess sickNessDes:(NSString *)sickNessDes privilegeNumberStr:(NSString *)privilegeNumberStr success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
