//
//  HZPatientTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/18.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZPatientTool : NSObject
+ (void)getPatientsWithHospitalId:(NSString *)hospitalId staffNum:(NSString *_Nullable)staffNum success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)starPatientWithStaffNum:(NSString *)staffNum patientRecordId:(NSString *_Nullable)patientRecordId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

+ (void)unstarPatientWithStaffNum:(NSString *)staffNum patientRecordId:(NSString *_Nullable)patientRecordId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

// 获取患者的住院信息
+ (void)getHospitalizePatientBasicInforWithHospitalId:(NSString *_Nullable)hospitalId patientCardId:(NSString *_Nullable)patientCardId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
