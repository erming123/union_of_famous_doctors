//
//  HZImageDataTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/23.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZImageDataTool : NSObject
+ (void)getImageBaseInforWithJCYBID:(NSString *_Nullable)JCYBID patientHospitalId:(NSString *)patientHospitalId  success:(void (^_Nullable)(id _Nullable responseObject))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//+ (void)getImageZipWithZipDownloadPath:(NSString *_Nullable)zipDownloadPath fileName:(NSString *_Nullable)fileName success:(void (^_Nullable)(id _Nullable responseObject))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end

