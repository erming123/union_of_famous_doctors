//
//  HZUserTool.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/18.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HZUser;
@interface HZUserTool : NSObject


/**
 获取用户的全部信息
 */
+ (void)getUserWithSuccess:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;


/**
 获取用户的详细信息

 @param openId 用户的openid
 */
+ (void)getUserDetailInforWithOpenId:(NSString *_Nullable)openId success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;


/**
 获取用户头像
 @param openId 用户的openId
 */
+ (void)getAvatarWithAvatarOpenId:(NSString *_Nullable)openId success:(void(^_Nullable)(UIImage *_Nullable image))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
//+ (void)getAvatarWithFaceUrl:(NSString *_Nullable)faceUrl success:(void(^_Nullable)(UIImage *_Nullable image,NSString * _Nullable urlKey))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;


/**
 获取个人简介和擅长
 */
+ (void)getMyIntroduceWithSuccess:(void(^_Nullable)(id _Nonnull responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
+ (void)getMySkillWithSuccess:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;


/**
 上传个人简介和擅长
 @param introduceStr 用户的简介文本
 @param userOpenid 用户的openId
 */
+ (void)upLoadMyIntroduceWithIntroduceStr:(NSString *_Nullable)introduceStr userOpenid:(NSString *_Nullable)userOpenid success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
+ (void)upLoadMySkillWithSkillStr:(NSString *_Nullable)skillStr userOpenid:(NSString *_Nullable)userOpenid success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;


/**
 上传头像和证书

 @param avatarImage 占位图
 @param userOpenid 用户的openId
 @param progress 进度
 */
+ (void)upLoadMyAvatarWithAvatarImage:(UIImage *_Nullable)avatarImage userOpenid:(NSString *_Nullable)userOpenid progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progress success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
+ (void)upLoadMyAptitudeWithAptitudeImage:(UIImage *_Nullable)aptitudeImage userId:(NSString *_Nullable)userId progress:(void (^_Nullable)(NSProgress * _Nonnull uploadProgress))progress success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;


/**
 查看用户的数据是否已经对接
 @param hospitalId 用户的hospitalId
 */
+ (void)getUserDataButtedVersionWithHospitalId:(NSString *_Nullable)hospitalId success:(void(^_Nonnull)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
