//
//  HZOrderHttpsTool.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/10/28.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZOrderHttpsTool.h"
#import "HZHttpsTool.h"
#import "HZAccountTool.h"
#import "MJExtension.h"
#import "HZOrder.h"
#import "HZPayOrder.h"
//#define BasicOrderURLStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/booking/order/get"
@implementation HZOrderHttpsTool
+ (void)getOrdersWithPages:(NSInteger)pages countPerPage:(NSInteger)count stateCode:(NSString *)stateCode success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {

    if (pages == 0) pages = 1;
    if (count == 0) count = 10;
    
    // /openapi/json/booking  v2
    NSString *OrderURLStr;
    NSString *BasicOrderURLStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/booking/order/get");
    if ([stateCode isEqualToString:@"008"]) { // 完成
        
        OrderURLStr = [NSString stringWithFormat:@"%@?page=%ld&rp=%ld&stateCode=%@", BasicOrderURLStr, pages, count, stateCode];
    } else {// 全部
       OrderURLStr = [NSString stringWithFormat:@"%@?page=%ld&rp=%ld", BasicOrderURLStr, pages, count];
    }

    //
    [HZHttpsTool GET:OrderURLStr  headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];


}


+ (void)getOrderDetailWithOrderID:(NSString *)orderID success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
//
//    NSString *BasicOrderURLStr = kGatewayUrlStr(@"cfdu/v1/openapi/json/booking/order/get");
    NSString *BasicOrderURLStr = kGatewayUrlStr(@"/cfdu/v2/order/");
//    NSString *OrderDetailByIDStr = [NSString stringWithFormat:@"%@?bookingOrderId=%@", BasicOrderURLStr, orderID];
     NSString *OrderDetailByIDStr = [NSString stringWithFormat:@"%@%@", BasicOrderURLStr, orderID];
    [HZHttpsTool GET:OrderDetailByIDStr  headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];
    
}

//+ (void)getBookingOrderStateStrWithVersion:(NSString *)version  success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
//    
//    
////    static NSArray *orderStateStr;
//    NSString *bookingOrderStateUrlStr = [NSString stringWithFormat:@"https://gateway.rubikstack.com/dictionary/dict/batchcommon?params={typeVersion:[{\"type\":\"booking_state\",\"version\":%@}]}", version];
//    [HZHttpsTool GET:bookingOrderStateUrlStr parameters:nil success:^(id responseObject) {
//        
//        
//        if ([responseObject[@"msg"] isEqualToString:@"success"]) {
//            if (success) {
//                success(responseObject);
//            }
//
//            
////            NSDictionary *resultArrDict = responseObject[@"results"];
////            orderStateStr = resultArrDict[@"booking_state"];
//            
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        
//        if (failure) {
//            failure(task, error);
//        }
//    }];
//}


+ (NSString *)getBookingOrderStateAlertStringWithVersion:(NSString *)version order:(HZOrder *)order {
    
    NSString *orderStateCode = order.bookingStateCode;
    NSString *str;
    
    if ([order.orderType isEqualToString:@"01"]) { // 本地订单
        if ([orderStateCode isEqualToString:@"001"]) {
            str = @"请提醒就诊人付费";
        } else if ([orderStateCode isEqualToString:@"002"]) {
            str = @"医生助理正在和专家确定开诊时间";
        } else if ([orderStateCode isEqualToString:@"003"]) {
            str = @"请按时进入视频诊间，与专家交流";
        }else if ([orderStateCode isEqualToString:@"004"]) {
            str = @"请进入视频诊间，与专家交流";
        }else if ([orderStateCode isEqualToString:@"005"]) {
            str = @"本次门诊未按规定时间开始，客服处理中";
        }else if ([orderStateCode isEqualToString:@"006"]) {
            str = @"如无需更多咨询，请结束本次门诊";
        }else if ([orderStateCode isEqualToString:@"007"]) {
            str = @"门诊超时未结束，请等待客服处理";
        }else if ([orderStateCode isEqualToString:@"008"]) {
            str = @"本次门诊已经结束";
        }    

        
    } else { // 远程订单
        if ([orderStateCode isEqualToString:@"001"]) {
            str = @"";
        } else if ([orderStateCode isEqualToString:@"002"]) {
            str = @"";
        } else if ([orderStateCode isEqualToString:@"003"]) {
            str = @"请按时进入视频诊间，与陪诊医生交流";
        }else if ([orderStateCode isEqualToString:@"004"]) {
            str = @"请进入视频诊间，与陪诊医生交流";
        }else if ([orderStateCode isEqualToString:@"005"]) {
            str = @"本次门诊未按规定时间开始，客服处理中";
        }else if ([orderStateCode isEqualToString:@"006"]) {
            str = @"请等待对方结束门诊";
        }else if ([orderStateCode isEqualToString:@"007"]) {
            str = @"订单超时未结束，客服处理中";
        }else if ([orderStateCode isEqualToString:@"008"]) {
            str = @"本次门诊已经结束";
        }

    }
    
        return str;
}

//+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
//    if (jsonString == nil) {
//        return nil;
//    }
//    
//    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *err;
//    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                        options:NSJSONReadingMutableContainers
//                                                          error:&err];
//    if(err) {
//        NSLog(@"json解析失败：%@",err);
//        return nil;
//    }
//    return dic;
//}


+ (void)getPayParamsWithBookingOrderId:(NSString *)bookingOrderId success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v2/openapi/json/booking/order/process/findPayInfo?id=%@", bookingOrderId];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)getWeChatPayIdWithPayOrder:(HZPayOrder *)payOrder success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSDictionary *paramDict = @{@"payInfoId" : payOrder.wechatPayInfoId,
                                @"orderFrom" : @"iOS名医联盟",
                                @"clientId" : kMyBasicClientId,
                                @"accountId" : @"1",
                                
                                @"body" : @"iOS端门诊微信支付",
                                @"orderAmount" : payOrder.price,
//                                @"orderAmount" : @"0.01" ,
                                @"timeExpire" : payOrder.endTime,
                                @"productId" : payOrder.goodsId,
                                @"timeStart" : payOrder.startTime,
                                @"outTradeNo" : payOrder.bookingOrderId,
                                @"notifyQueueName" : @"ms-pay-cfdu",
                                @"productName" : @"诊间支付"};
    NSLog(@"---WeChatPayparamDict--%@", paramDict.getDictJsonStr);
    [HZHttpsTool POST:kGatewayUrlStr(@"pay/wxpay/toQrPay") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)getAliPayIdWithPayOrder:(HZPayOrder *)payOrder success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSDictionary *paramDict = @{
                                @"payInfoId" : payOrder.aliPayInfoId,
                                @"orderFrom" : @"iOS名医联盟",
                                @"clientId" : kMyBasicClientId,
                                @"accountId" : @"1",
                                
                                @"body" : @"iOS端门诊支付宝支付",
                                @"orderAmount" : payOrder.price,
//                                @"orderAmount" : @"0.01" ,
                                @"timeExpire" : payOrder.endTime,
                                @"productId" : payOrder.goodsId,
                                @"timeStart" : payOrder.startTime,
                                @"outTradeNo" : payOrder.bookingOrderId,
                                @"notifyQueueName" : @"ms-pay-cfdu",
                                @"productName" : @"诊间支付"};
    NSLog(@"---AliPayparamDict--%@", paramDict.getDictJsonStr);
    [HZHttpsTool POST:kGatewayUrlStr(@"pay/alipay/toQrPay") headDomainValue:nil parameters:paramDict progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)getQRImageWithRelationId:(NSString *)relationId success:(void(^)(NSData *QRImageData))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *urlStr = [NSString stringWithFormat:@"pay/qrImageString/%@", relationId];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        NSString *avatarStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        avatarStr = [[avatarStr componentsSeparatedByString:@"base64,"] lastObject];
        NSData *QRImageData = [[NSData alloc] initWithBase64EncodedString:avatarStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
//        UIImage *QRImage = [UIImage imageWithData:_decodedImageData];
        if (success) {
            success(QRImageData);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)getOrderPayStateWithPayWay:(NSString *)payWay OutTradeNo:(NSString *)outTradeNo success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    


    NSString *urlStr = [NSString stringWithFormat:@"pay/order/query?accountId=%@&payWay=%@&outTradeNo=%@",@"1", payWay, outTradeNo];
    
//    NSLog(@"-----sdfg-----%@", urlStr);
    [HZHttpsTool GET:kGatewayUrlStr(urlStr) headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)getOrderPayDetailWithOrderId:(NSString *)OrderId openOrderId:(NSString *)openOrderId success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    //
    NSString *urlStr = [NSString stringWithFormat:@"cfdu/v1/openapi/json/booking/order/%@?openid=%@", OrderId, openOrderId];
    [HZHttpsTool GET:kGatewayUrlStr(urlStr)  headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];
}

+ (void)endLocaOrderWithBookingOrderId:(NSString *_Nullable)bookingOrderId success:(void (^_Nullable)(id _Nullable responseObject))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    NSDictionary *paramsDict = @{@"bookingOrderId" : bookingOrderId};
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v2/openapi/json/booking/order/process/finish") headDomainValue:nil parameters:paramsDict progress:nil success:^(id responseObject) {
        NSLog(@"-----结束本地订单成功----%@", responseObject);
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(task, error);
        }
    }];
}
@end
