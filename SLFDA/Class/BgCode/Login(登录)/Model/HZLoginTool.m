//
//  NSLoginTool.m
//  SLIOP
//
//  Created by 季怀斌 on 16/9/28.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZLoginTool.h"

@implementation NSLoginTool

+(instancetype)shareLoginTool {
    static NSLoginTool* loginTool;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
            loginTool = [[NSLoginTool alloc] init];
    });
    return loginTool;
}

- (void)setIsLogin:(BOOL)isLogin {
    _isLogin = isLogin;
}

@end
