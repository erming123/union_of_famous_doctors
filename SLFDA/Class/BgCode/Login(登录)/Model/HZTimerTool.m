//
//  HZTimerTool.m
//  LoginDemo
//
//  Created by 季怀斌 on 16/6/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZTimerTool.h"

@implementation HZTimerTool
+(instancetype)shareTimerTool {
    static HZTimerTool* timerTool;
    if(timerTool == nil) {
        timerTool = [[HZTimerTool alloc] init];
        
        [timerTool initData];
    }
    return timerTool;
}

- (void)initData {
    self.time = 60;
}
-(void) startTimer{
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timePlus) userInfo:nil repeats:YES];
}

- (void)timePlus {
    [HZTimerTool shareTimerTool].time--;
    
    if ([HZTimerTool shareTimerTool].time <= 0) {
        
        [HZTimerTool shareTimerTool].time = 0;
        
    }

}
         
@end
