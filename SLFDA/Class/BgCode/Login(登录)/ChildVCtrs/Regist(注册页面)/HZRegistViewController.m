//
//  HZRegistViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/5/8.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZRegistViewController.h"
#import "HZActivityIndicatorView.h"
#import "HZUserTool.h"
#import "HZUser.h"
//#import "HZHomeViewController.h"
#import "HZTabBarController.h"
#import "HZCompleteInformationViewController.h"
@interface HZRegistViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
//@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UIView *firstHLine;
@property (nonatomic, strong) UILabel *verityLabel;
//@property (nonatomic, strong) UITextField *verityTextField;
@property (nonatomic, strong) UIView *secondHLine;
//@property (nonatomic, strong) UIButton *verityCodeBtn;
@property (nonatomic, strong) UIView *bgView;
//@property (nonatomic, strong) UIButton *loginBtn;


@property (nonatomic, assign) NSInteger timeCount;
@property (nonatomic, strong) NSTimer *timer;


//** <#注释#>*/
@property (nonatomic, strong) HZActivityIndicatorView *activityIndicatorView;
//** <#注释#>*/
@property (nonatomic, strong) HZActivityIndicatorView *phoneVerityIndicatorView;


//** <#注释#>*/
@property (nonatomic, copy) NSString *previousTextFieldContent;
@property (nonatomic, strong) UITextRange *previousSelection;





//** <#注释#>*/
@property (nonatomic, strong) HZTableView *registTableView;
@property (nonatomic, strong) UITextField *phoneTextField;
@property (nonatomic, strong) UITextField *verityTextField;
@property (nonatomic, strong) UIButton *verityCodeBtn;
@property (nonatomic, strong) UIButton *nextBtn;
@property (nonatomic, strong) UIButton *enterLoginBtn;

@end

@implementation HZRegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.navigationItem.title = @"新用户注册";
    
    
    //
    //    [NSUserDefaults standardUserDefaults];
    
    
    //
    [self setUpSubViews];
    
    
    //
    HZActivityIndicatorView *activityIndicatorView = [[HZActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, kP(240), kP(240))];
    activityIndicatorView.center = self.view.center;
    activityIndicatorView.activityIndicatorText = @"请稍候...";
    [self.view addSubview:activityIndicatorView];
    self.activityIndicatorView = activityIndicatorView;
    
    
    HZActivityIndicatorView *phoneVerityIndicatorView = [[HZActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, kP(240), kP(240))];
    phoneVerityIndicatorView.center = self.view.center;
    phoneVerityIndicatorView.activityIndicatorText = @"正在验证...";
    [self.view addSubview:phoneVerityIndicatorView];
    self.phoneVerityIndicatorView = phoneVerityIndicatorView;
}

#pragma mark -- 添加子视图
- (void)setUpSubViews {
    
    //1.tableView
    HZTableView *registTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    registTableView.dataSource = self;
    registTableView.delegate = self;
    registTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    registTableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    [self.view addSubview:registTableView];
    self.registTableView = registTableView;
    
    //2.手机号输入框
    UITextField *phoneTextField = [[UITextField alloc] init];
    phoneTextField.placeholder = @"请输入手机号";
    phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
    phoneTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    phoneTextField.font = [UIFont systemFontOfSize:kP(36)];
    phoneTextField.delegate = self;
    //    [phoneTextField becomeFirstResponder];
    //
    //     BOOL is =  [[NSUserDefaults standardUserDefaults] boolForKey:@"showKeyBoard"];
    
    //    NSLog(@"---------BOOL------%@", is == YES ? @"YES":@"NO");
    [phoneTextField addTarget:self action:@selector(reformatphoneTextFieldContent:) forControlEvents:UIControlEventEditingChanged];
    self.phoneTextField = phoneTextField;
    
    //3. 验证码输入框
    UITextField *verityTextField = [[UITextField alloc] init];
    verityTextField.placeholder = @"请输入验证码";
    //    verityTextField.backgroundColor = [UIColor redColor]
    verityTextField.keyboardType = UIKeyboardTypeNumberPad;
    verityTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    verityTextField.font = [UIFont systemFontOfSize:kP(36)];
    verityTextField.delegate = self;
    [verityTextField addTarget:self action:@selector(checkVerityTextFieldContent:) forControlEvents:UIControlEventEditingChanged];
    self.verityTextField = verityTextField;
    
    //4. 验证码按钮
    UIButton *verityCodeBtn = [[UIButton alloc] init];
    [verityCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    verityCodeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
//    [verityCodeBtn setTitleColor:[UIColor colorWithHexString:@"21b8c6" alpha:1.0] forState:UIControlStateNormal];
    [verityCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    verityCodeBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    verityCodeBtn.layer.cornerRadius = kP(16);
    verityCodeBtn.clipsToBounds = YES;

    verityCodeBtn.alpha = 0.5;
    verityCodeBtn.userInteractionEnabled = NO;
    [verityCodeBtn addTarget:self action:@selector(verityCodeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.verityCodeBtn = verityCodeBtn;
    
    //5.下一步按钮
    UIButton *nextBtn = [[UIButton alloc] init];
    nextBtn.x = kP(28);
    nextBtn.height = kP(106);
    nextBtn.y = kP(350);
    nextBtn.width = self.view.width - 2 * nextBtn.x;
    [nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
    nextBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];//
    nextBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    nextBtn.layer.cornerRadius = kP(16);
    nextBtn.clipsToBounds = YES;
    nextBtn.alpha = 0.5;
    nextBtn.enabled = NO;
    [nextBtn addTarget:self action:@selector(nextBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [registTableView addSubview:nextBtn];
    self.nextBtn = nextBtn;
    
    //6.去登录按钮
    UIButton *enterLoginBtn = [[UIButton alloc] init];
    enterLoginBtn.x = kP(260);
    enterLoginBtn.height = kP(50);
    enterLoginBtn.y = nextBtn.bottom + kP(28);
    enterLoginBtn.width = self.view.width - 2 * enterLoginBtn.x;
    [enterLoginBtn setTitle:@"已有账号登录" forState:UIControlStateNormal];
    enterLoginBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [enterLoginBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];//
    [enterLoginBtn addTarget:self action:@selector(enterLoginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [registTableView addSubview:enterLoginBtn];
    self.enterLoginBtn = enterLoginBtn;
    //7.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endViewEditing:)];
    [self.view addGestureRecognizer:tap];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0) {
        
        //
        CGFloat phoneTextFieldX = kP(28);
        CGFloat phoneTextFieldY = kP(120) * 0.5 - kP(50) * 0.5;
        CGFloat phoneTextFieldW = HZScreenW - phoneTextFieldX - kP(50);
        CGFloat phoneTextFieldH = kP(50);
        self.phoneTextField.frame = CGRectMake(phoneTextFieldX, phoneTextFieldY, phoneTextFieldW, phoneTextFieldH);
        [cell.contentView addSubview:self.phoneTextField];
        
    } else {
        
        //
        CGFloat verityTextFieldW = kP(350);
        CGFloat verityTextFieldH = kP(50);
        CGFloat verityTextFieldX = kP(28);
        CGFloat verityTextFieldY = kP(120) * 0.5 - kP(50) * 0.5;
        self.verityTextField.frame = CGRectMake(verityTextFieldX, verityTextFieldY, verityTextFieldW, verityTextFieldH);
        [cell.contentView addSubview:self.verityTextField];
        
        //
        self.verityCodeBtn.width = kP(214);
        self.verityCodeBtn.height = kP(60);
        self.verityCodeBtn.x = HZScreenW - self.verityCodeBtn.width - kP(28);
        self.verityCodeBtn.y = kP(120) * 0.5 - self.verityCodeBtn.height * 0.5;
        [cell.contentView addSubview:self.verityCodeBtn];
    }
    
    return cell;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(120);
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(42);
    }
    return kP(2);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kP(0.1);
}

#pragma mark -- 设置手机号的格式
- (void)reformatphoneTextFieldContent:(UITextField *)phoneTextField {
    /**
     *  判断正确的光标位置
     */
    NSUInteger targetCursorPostion = [phoneTextField offsetFromPosition:phoneTextField.beginningOfDocument toPosition:phoneTextField.selectedTextRange.start];
    NSString *phoneNumberWithoutSpaces = [self removeNonDigits:phoneTextField.text andPreserveCursorPosition:&targetCursorPostion];
    
    
    if([phoneNumberWithoutSpaces length] > 11) {
        /**
         *  避免超过11位的输入
         */
        
        [phoneTextField setText:_previousTextFieldContent];
        phoneTextField.selectedTextRange = _previousSelection;
        
        return;
    }
    
    
    NSString *phoneNumberWithSpaces = [self insertSpacesEveryFourDigitsIntoString:phoneNumberWithoutSpaces andPreserveCursorPosition:&targetCursorPostion];
    
    phoneTextField.text = phoneNumberWithSpaces;
    UITextPosition *targetPostion = [phoneTextField positionFromPosition:phoneTextField.beginningOfDocument offset:targetCursorPostion];
    [phoneTextField setSelectedTextRange:[phoneTextField textRangeFromPosition:targetPostion toPosition:targetPostion]];
    
    
    
    // 设置login按钮的状态
    if (phoneTextField.text.length != 0 && self.verityTextField.text.length != 0) {
        self.nextBtn.enabled = YES;
        self.nextBtn.alpha = 1;
    } else {
        
        self.nextBtn.enabled = NO;
        self.nextBtn.alpha = 0.5;
    }
    
    if (phoneTextField.text.length == 13) {
        self.verityCodeBtn.alpha = 1;
        self.verityCodeBtn.userInteractionEnabled = YES;
    } else {
        self.verityCodeBtn.alpha = 0.5;
        self.verityCodeBtn.userInteractionEnabled = NO;
    }
    
    
    
}


- (NSString *)removeNonDigits:(NSString *)string andPreserveCursorPosition:(NSUInteger *)cursorPosition {
    NSUInteger originalCursorPosition =*cursorPosition;
    NSMutableString *digitsOnlyString = [NSMutableString new];
    
    for (NSUInteger i = 0; i < string.length; i++) {
        unichar characterToAdd = [string characterAtIndex:i];
        
        if(isdigit(characterToAdd)) {
            NSString *stringToAdd = [NSString stringWithCharacters:&characterToAdd length:1];
            [digitsOnlyString appendString:stringToAdd];
        }
        else {
            if(i<originalCursorPosition) {
                (*cursorPosition)--;
            }
        }
    }
    return digitsOnlyString;
}


- (NSString *)insertSpacesEveryFourDigitsIntoString:(NSString *)string andPreserveCursorPosition:(NSUInteger *)cursorPosition{
    NSMutableString *stringWithAddedSpaces = [NSMutableString new];
    NSUInteger cursorPositionInSpacelessString = *cursorPosition;
    
    for (NSUInteger i=0; i<string.length; i++) {
        if(i>0)
        {
            if(i==3 || i==7) {
                [stringWithAddedSpaces appendString:@" "];
                
                if(i<cursorPositionInSpacelessString) {
                    (*cursorPosition)++;
                }
            }
        }
        
        unichar characterToAdd = [string characterAtIndex:i];
        NSString *stringToAdd = [NSString stringWithCharacters:&characterToAdd length:1];
        [stringWithAddedSpaces appendString:stringToAdd];
    }
    return stringWithAddedSpaces;
}


#pragma mark --  检查密码输入框的内容

- (void)checkVerityTextFieldContent:(UITextField *)verityTextField {
    
    if (verityTextField.text.length != 0 && self.phoneTextField.text.length != 0) {
       
        self.nextBtn.enabled = YES;
        self.nextBtn.alpha = 1;
    } else {
        
        self.nextBtn.enabled = NO;
        self.nextBtn.alpha = 0.5;
    }
    
    if([verityTextField.text length] > 6) {
        /**
         *  避免超过11位的输入
         */
        
        [verityTextField setText:_previousTextFieldContent];
        verityTextField.selectedTextRange = _previousSelection;
        
        return;
    }
    
}


#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.phoneTextField) {
        _previousSelection = textField.selectedTextRange;
        _previousTextFieldContent = textField.text;
        
        if(range.location==0) {
            if(string.integerValue >1)
            {
                return NO;
            }
        }
        
        
    } else if (textField == self.verityTextField){
        
        if (range.length == 1 && string.length == 0) {
            return YES;
        }
        else if (self.verityTextField.text.length >= 6) {
            self.verityTextField.text = [textField.text substringToIndex:6];
            return NO;
        }
        
    }
    
    return YES;
    
}


#pragma mark -- 褪去键盘
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (void)endViewEditing:(UITapGestureRecognizer *)tap {
    [UIView animateWithDuration:0.25f animations:^{
        [self.view endEditing:YES];
    }];
    
}

#pragma mark -- 获取验证码
- (void)verityCodeBtnClick:(UIButton *)verityCodeBtn {
    
    self.phoneVerityIndicatorView.hidden = NO;
    
    if ([self getReCoverPhoneNumber].length != 11)  return;
    
    //    [HZAccountTool getBasicTokenSuccess:nil failure:nil];
    
    [self performSelector:@selector(getVerifyCode) withObject:self afterDelay:0];
}

#pragma mark -- 获取手机号
- (NSString *)getReCoverPhoneNumber{
    return  [self.phoneTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
}

#pragma mark -- 获取验证码
- (void)getVerifyCode {
    
    NSLog(@"---------sdddss----");
    
    [HZAccountTool getVerifyCodeWithUserPhoneNumber:[self getReCoverPhoneNumber] success:^(id responseObject) {
        self.phoneVerityIndicatorView.hidden = YES;
        // 直接点击验证码输入框
        [self.verityTextField becomeFirstResponder];
        
        // 开始倒计时
        self.registTableView.scrollEnabled = NO;
        //
        self.verityCodeBtn.enabled = NO;
        _timeCount = 60;
        [self.verityCodeBtn setTitle:@"获取验证码" forState:UIControlStateDisabled];
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        self.timer = timer;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.phoneVerityIndicatorView.hidden = YES;
        NSLog(@"--------获取验证码失败---%@", error);
//        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"服务器或网络异常" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            self.phoneVerityIndicatorView.hidden = YES;
//        }];
//        if (kCurrentSystemVersion >= 9.0) {
//            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
//        }
//        [alertCtr addAction:ensureAlertAction];
//        [self presentViewController:alertCtr animated:YES completion:nil];
    }];
    
    
    
}

#pragma mark -- 倒计时
- (void)timerFired:(NSTimer *)timer {
    
    self.timeCount -= 1;
    if (self.timeCount >= 0) {
        self.verityTextField.enabled = YES;
        NSString *str = [NSString stringWithFormat:@"重新获取(%ld)", (long)self.timeCount];
        [self.verityCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
        [self.verityCodeBtn setTitle:str forState:UIControlStateDisabled];
        self.verityCodeBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        self.verityCodeBtn.backgroundColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0];
        if (self.timeCount == 0) {
            
            //
            self.registTableView.scrollEnabled = YES;
            [self.verityCodeBtn setTitle:@"获取验证码" forState:UIControlStateDisabled];
            [self.verityCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
            self.verityCodeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
            self.verityCodeBtn.titleLabel.textAlignment = NSTextAlignmentRight;
            self.verityCodeBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
            self.phoneTextField.enabled = YES;
            self.verityCodeBtn.enabled = NO;
        }
        
        
    } else if (self.timeCount < 0) {
        //        self.timeCount = 0;
        //
        self.registTableView.scrollEnabled = YES;
        //
        [timer invalidate];
        self.verityCodeBtn.enabled = YES;
        [self.verityCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.verityCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
        self.verityCodeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
        self.verityCodeBtn.titleLabel.textAlignment = NSTextAlignmentRight;
        self.verityCodeBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
        self.phoneTextField.enabled = YES;
        //        self.getTestCodeBtn.enabled = NO;
    }
    
    //    NSLog(@"5555555%ld", self.timeCount);
}

#pragma mark -- 停止倒计时
- (void)viewWillDisappear:(BOOL)animated {
    [self.timer invalidate];
//    self.activityIndicatorView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self viewDidLoad];
}

#pragma mark -- nextBtnClick
- (void)nextBtnClick:(UIButton *)nextBtn {
    
    [self.phoneTextField resignFirstResponder];
    [self.verityTextField resignFirstResponder];
    
    self.activityIndicatorView.hidden = NO;
    nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
    nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
    
    
    
    NSLog(@"----self.verityTextField.text-----%@", self.verityTextField.text);
    
    
    [HZAccountTool getBasicTokenSuccess:^(id responseObject) {
        //  验证 验证码
        [HZAccountTool justVerifyCodeWithPhoneNumber:[self getReCoverPhoneNumber] verifyCode:self.verityTextField.text success:^(id responseObject) {
            
            
            NSDictionary *resultsDict = responseObject[@"results"];
            if ([responseObject[@"state"] isEqual:@200] && [resultsDict[@"checkCode"] isEqual:@YES] && [resultsDict[@"checkUser"] isEqual:@YES]) {
                
                NSLog(@"----验证验证码成功-----%@", responseObject);
                
                // 获取access_token
                [HZAccountTool getLoginCodeWithUserPhoneNumber:[self getReCoverPhoneNumber] andLoginVerifyCode:self.verityTextField.text success:^(id responseObject) {// 获取access_token有回应
                    
                    
                    NSLog(@"------登录反馈123---%@", responseObject);
                    
                    NSString *access_token = responseObject[@"access_token"];
                    NSString *refresh_token = responseObject[@"refresh_token"];
                    //            NSString *expires_in = responseObject[@"expires_in"];
                    //            NSString *jti = responseObject[@"jti"];
                    
                    //
                    [[NSUserDefaults standardUserDefaults] setObject:self.verityTextField.text forKey:@"verityCode"];
                    
                    //
                    
                    
                    //
                    //                    [[NSUserDefaults standardUserDefaults] setValue:@YES forKeyPath:@"isLogin"];
                    //------------------------------------------------------
                    
                    [[NSUserDefaults standardUserDefaults] setValue:access_token forKeyPath:@"access_token"];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setValue:refresh_token forKeyPath:@"refresh_token"];
                    [[NSUserDefaults standardUserDefaults] setValue:[self getReCoverPhoneNumber] forKey:@"phoneNumber"];
                    
                    NSLog(@"-----kAccess_token对对对----%@", kAccess_token);
                    
                    if (kAccess_token) {
                        
                        
                        
                        [HZUserTool getUserWithSuccess:^(id responseObject) {
                            
                            
                            if ([responseObject[@"state"] isEqual:@200]) {// 信息完善
                                
                                NSLog(@"----dddssssaaaaaa-----%@", responseObject);
                                
                                NSLog(@"获取用户信息成功");
                                self.activityIndicatorView.hidden = YES;
                                nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                
                                NSDictionary *dataDict = responseObject;
                                NSDictionary *resultDict = dataDict[@"results"];
                                NSDictionary *userDict = resultDict[@"user"];
                                HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                //1
                                [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                
//                                HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                homeVCtr.user = user;
//                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                
                                HZTabBarController *tabBarCtr = [HZTabBarController new];
                                [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                            } else if ([responseObject[@"state"] isEqual:@4001]){ // 信息不完善
                                
                                
                                
                                HZCompleteInformationViewController *completeInformationVCtr = [HZCompleteInformationViewController new];
                                [self.navigationController pushViewController:completeInformationVCtr animated:YES];
                                
                                //                        //
                            } else {
                                
                                NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                                
                                if (!userDict) {
                                    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        self.activityIndicatorView.hidden = YES;
                                        nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                        nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                        
                                        //                                    //
                                        //                                    HZCompleteInformationViewController *completeInformationVCtr = [HZCompleteInformationViewController new];
                                        //                                    [self.navigationController pushViewController:completeInformationVCtr animated:YES];
                                        //
                                        //                                    self.phoneVerityIndicatorView.hidden = YES;
                                        //------------- 去掉accesstoken，回到 ------------
                                        //                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                        
                                    }];
                                    if (kCurrentSystemVersion >= 9.0) {
                                        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                                    }
                                    [alertCtr addAction:ensureAlertAction];
                                    [self presentViewController:alertCtr animated:YES completion:nil];
                                    return;
                                }
                                
                                //
                                self.activityIndicatorView.hidden = YES;
                                nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                
                                HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                //1
                                [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                
//                                HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                homeVCtr.user = user;
//                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                HZTabBarController *tabBarCtr = [HZTabBarController new];
                                [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                
                            }
                            
                            
                            
                            
                            
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            
                            
                            NSLog(@"----rrrrrrrrrrrr-----%@", error);
                            
                            
                            
                            //
                            NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                            
                            if (!userDict) {
                                self.activityIndicatorView.hidden = YES;
                                nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                                UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    self.activityIndicatorView.hidden = YES;
                                    nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                    nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                    
                                    //
                                    HZCompleteInformationViewController *completeInformationVCtr = [HZCompleteInformationViewController new];
                                    [self.navigationController pushViewController:completeInformationVCtr animated:YES];
                                    
                                    self.phoneVerityIndicatorView.hidden = YES;
                                    //------------- 去掉accesstoken，回到 ------------
                                    //                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                    
                                }];
                                if (kCurrentSystemVersion >= 9.0) {
                                    [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                                }
                                [alertCtr addAction:ensureAlertAction];
                                [self presentViewController:alertCtr animated:YES completion:nil];
                                return;
                            }
                            
                            //
                            self.activityIndicatorView.hidden = YES;
                            nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                            nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                            
                            HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                            user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                            //1
                            [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                            
//                            HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                            homeVCtr.user = user;
//                            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                            
                            HZTabBarController *tabBarCtr = [HZTabBarController new];
                            [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                            
                        }];
                        
                        
                        
                    } else {
                        NSLog(@"---------没有access_token");
                    }
                    
                    
                    
                    
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {// 获取access_toke有问题
                    
                    //                self.activityIndicatorView.hidden = YES;
                    //
                    //
                    //                UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"获取access_token失败" preferredStyle:UIAlertControllerStyleAlert];
                    //                UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    //
                    //                    self.phoneVerityIndicatorView.hidden = YES;
                    //                    nextBtn.userInteractionEnabled = YES;
                    //                    nextBtn.userInteractionEnabled = YES;
                    //                    nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                    //                }];
                    //                if (kCurrentSystemVersion >= 9.0) {
                    //                    [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
                    //                }
                    //                [alertCtr addAction:ensureAlertAction];
                    //                [self presentViewController:alertCtr animated:YES completion:nil];
                    //
                    //                //                }
                    //                self.activityIndicatorView.hidden = YES;
                }];
                
                
                
                
            } else if ([responseObject[@"state"] isEqual:@200] && [resultsDict[@"checkUser"] isEqual:@YES] && [resultsDict[@"checkCode"] isEqual:@NO]) {
                
                
                self.activityIndicatorView.hidden = YES;
                UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"验证码输入错误" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    
                    nextBtn.userInteractionEnabled = YES;
                    nextBtn.userInteractionEnabled = YES;
                    nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                    
                    
                    //                self.phoneVerityIndicatorView.hidden = YES;
                    
                    self.activityIndicatorView.hidden = YES;
                }];
                if (kCurrentSystemVersion >= 9.0) {
                    [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                }
                [alertCtr addAction:ensureAlertAction];
                [self presentViewController:alertCtr animated:YES completion:nil];
            } else if ([responseObject[@"state"] isEqual:@200] && [resultsDict[@"checkUser"] isEqual:@NO]) { // 去注册
                
                
                //注册成用户
                NSString *phoneNumberStr = [self getReCoverPhoneNumber];
                NSString *pwdStr = [phoneNumberStr substringWithRange:NSMakeRange(phoneNumberStr.length - 6, 6)];
                [HZAccountTool registAccountWithPhoneNumber:phoneNumberStr passWord:pwdStr type:@"phone" success:^(id responseObject) {
                    
                    //15618705866
                    
                    NSLog(@"-----responseObject注册成功----%@", responseObject);
                    
                    if ([responseObject[@"state"] isEqual:@200]) {// 注册成功
                        
                        NSDictionary *resultDict = responseObject[@"results"];
                        NSLog(@"----userId-----%@", resultDict[@"userId"]);
                        
                        
                        
                        // 再次获取验证码
                        // 获取access_token
                        [HZAccountTool getLoginCodeWithUserPhoneNumber:[self getReCoverPhoneNumber] andLoginVerifyCode:self.verityTextField.text success:^(id responseObject) {// 获取access_token有回应
                            
                            
                            NSLog(@"------登录反馈123---%@", responseObject);
                            
                            NSString *access_token = responseObject[@"access_token"];
                            NSString *refresh_token = responseObject[@"refresh_token"];
                            //                        NSString *expires_in = responseObject[@"expires_in"];
                            //                        NSString *jti = responseObject[@"jti"];
                            
                            //
                            [[NSUserDefaults standardUserDefaults] setObject:self.verityTextField.text forKey:@"verityCode"];
                            
                            //
                            //
                            //                    [[NSUserDefaults standardUserDefaults] setValue:@YES forKeyPath:@"isLogin"];
                            
                            //------------------------------------------------------
                            [[NSUserDefaults standardUserDefaults] setValue:access_token forKeyPath:@"access_token"];
                            [[NSUserDefaults standardUserDefaults] setValue:refresh_token forKeyPath:@"refresh_token"];
                            [[NSUserDefaults standardUserDefaults] setValue:[self getReCoverPhoneNumber] forKey:@"phoneNumber"];
                            
                            NSLog(@"-----kAccess_token对对对----%@", kAccess_token);
                            
                            if (kAccess_token) {
                                
                                
                                
                                [HZUserTool getUserWithSuccess:^(id responseObject) {
                                    
                                    NSLog(@"-----user----%@", responseObject);
                                    if ([responseObject[@"state"] isEqual:@200]) {// 信息完善
                                        
                                        NSLog(@"----dddssssaaaaaa-----%@", responseObject);
                                        
                                        NSLog(@"获取用户信息成功");
                                        self.activityIndicatorView.hidden = YES;
                                        nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                        nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                        
                                        NSDictionary *dataDict = responseObject;
                                        NSDictionary *resultDict = dataDict[@"results"];
                                        NSDictionary *userDict = resultDict[@"user"];
                                        HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                        user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                        //1
                                        [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                        
//                                        HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                        homeVCtr.user = user;
//                                        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                        [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                        HZTabBarController *tabBarCtr = [HZTabBarController new];
                                        [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                        
                                    } else if ([responseObject[@"state"] isEqual:@4001]){ // 信息不完善
                                        
                                        
                                        
                                        HZCompleteInformationViewController *completeInformationVCtr = [HZCompleteInformationViewController new];
                                        [self.navigationController pushViewController:completeInformationVCtr animated:YES];
                                        
                                        //                        //
                                    } else {
                                        
                                        
                                        NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                                        
                                        if (!userDict) {
                                            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                self.activityIndicatorView.hidden = YES;
                                                nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                                nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                                
//                                                //
//                                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
//                                                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                                
                                                self.phoneVerityIndicatorView.hidden = YES;
                                                //------------- 去掉accesstoken，回到 ------------
                                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                
                                            }];
                                            if (kCurrentSystemVersion >= 9.0) {
                                                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                                            }
                                            [alertCtr addAction:ensureAlertAction];
                                            [self presentViewController:alertCtr animated:YES completion:nil];
                                            return;
                                        }
                                        
                                        //
                                        self.activityIndicatorView.hidden = YES;
                                        nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                        nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                        
                                        HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                        user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                        //1
                                        [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                        
//                                        HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                        homeVCtr.user = user;
//                                        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                        [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                        
                                        HZTabBarController *tabBarCtr = [HZTabBarController new];
                                        [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                    }
                                    
                                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                    
                                    
                                    NSLog(@"----rrrrrrrrrrrr-----%@", error);
                                    
                                    
                                    
                                    NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                                    
                                    if (!userDict) {
                                        self.activityIndicatorView.hidden = YES;
                                        nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                        nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                        //
                                        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                                        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                            self.activityIndicatorView.hidden = YES;
                                            nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                            nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                            
//                                            //
//                                            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
//                                            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                            
                                            self.phoneVerityIndicatorView.hidden = YES;
                                            //------------- 去掉accesstoken，回到 ------------
                                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                            [[NSUserDefaults standardUserDefaults] synchronize];
                                            
                                        }];
                                        if (kCurrentSystemVersion >= 9.0) {
                                            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                                        }
                                        [alertCtr addAction:ensureAlertAction];
                                        [self presentViewController:alertCtr animated:YES completion:nil];
                                        return;
                                    }
                                    
                                    //
                                    self.activityIndicatorView.hidden = YES;
                                    nextBtn.userInteractionEnabled = self.activityIndicatorView.hidden;
                                    nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                                    
                                    HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                    user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                                    //1
                                    [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                                    
//                                    HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//                                    homeVCtr.user = user;
//                                    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                    [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                    
                                    HZTabBarController *tabBarCtr = [HZTabBarController new];
                                    [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
                                }];
                                
                                
                                
                            } else {
                                NSLog(@"---------没有access_token");
                            }
                            
                            
                            
                            
                            
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {// 获取access_toke有问题
                            
                            //                        self.activityIndicatorView.hidden = YES;
                            //                        
                            //                        
                            //                        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"获取access_token失败" preferredStyle:UIAlertControllerStyleAlert];
                            //                        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            //                            
                            //                            self.phoneVerityIndicatorView.hidden = YES;
                            //                            nextBtn.userInteractionEnabled = YES;
                            //                            nextBtn.userInteractionEnabled = YES;
                            //                            nextBtn.alpha = nextBtn.userInteractionEnabled ? 1:0.5;
                            //                        }];
                            //                        if (kCurrentSystemVersion >= 9.0) {
                            //                            [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
                            //                        }
                            //                        [alertCtr addAction:ensureAlertAction];
                            //                        [self presentViewController:alertCtr animated:YES completion:nil];
                            //                        
                            //                        //                }
                            //                        self.activityIndicatorView.hidden = YES;
                        }];
                        
                        
                        
                    } else {// 注册失败
                        NSLog(@"---------注册失败");
                        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"注册成用户失败，请您从新获取验证码" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            self.phoneVerityIndicatorView.hidden = YES;
                        }];
                        if (kCurrentSystemVersion >= 9.0) {
                            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                        }
                        [alertCtr addAction:ensureAlertAction];
                        [self presentViewController:alertCtr animated:YES completion:nil];
                        
                        
                    }
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"--------responseObject注册失败---%@", error);
                }];
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"---验证验证码失败------%@", error);
        }];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---验证basickToken失败------%@", error);
    }];
    
}
#pragma mark -- enterLoginBtnClick
- (void)enterLoginBtnClick:(UIButton *)enterLoginBtn {
    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
    [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
