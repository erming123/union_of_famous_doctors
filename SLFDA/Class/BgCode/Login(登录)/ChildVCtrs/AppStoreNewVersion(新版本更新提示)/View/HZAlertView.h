//
//  HZAlertView.h
//  HZAlertViewDIYDemo
//
//  Created by 季怀斌 on 2016/11/15.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZAlertViewDelegate <NSObject>
- (void)enterToAppStore;
- (void)cancelToDismissWithIsForceUp:(BOOL)isForceUp;
@end
@interface HZAlertView : UIView
@property (nonatomic, weak) id <HZAlertViewDelegate> delegate;
@property (nonatomic, assign) BOOL isForceUp;
@property (nonatomic, copy) NSString *alertString;
@end
