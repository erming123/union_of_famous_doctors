//
//  HZCompleteInformationViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZCompleteInformationViewController.h"
#import "HZCompleteDoneTableViewController.h"

//
#import "HZHospitalSearchViewController.h"
#import "HZSubjectSelectionViewController.h"
#import "HZProfessionSelectionViewController.h"

//
#import "HZInforCompleteTool.h"
//
#import "IQKeyboardManager.h"
@interface HZCompleteInformationViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, HZHospitalSearchViewControllerDeleagte, HZProfessionSelectionViewControllerDelegate, HZSubjectSelectionViewControllerDeleagte>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *completeInformationTableView;
//** <#注释#>*/
@property (nonatomic, strong) UITextField *nameTextField;
//** <#注释#>*/
//@property (nonatomic, strong) UITextField *inviteCodeTextField;

@property (nonatomic, copy) NSString *hospitalStr;
//** <#注释#>*/
@property (nonatomic, strong) NSDictionary *subjectDict;
@property (nonatomic, copy) NSString *subjectId;
@property (nonatomic, copy) NSString *subjectName;
//** <#注释#>*/
@property (nonatomic, strong) NSDictionary *professionDict;
@property (nonatomic, copy) NSString *professionId;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, assign) CGFloat keyBoardH;
@end

@implementation HZCompleteInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //
    self.navigationItem.title = @"资料完善";
    self.view.backgroundColor = [UIColor whiteColor];
    
    HZTableView *completeInformationTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    completeInformationTableView.dataSource = self;
    completeInformationTableView.delegate = self;
    [self.view addSubview:completeInformationTableView];

    completeInformationTableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    //    self.tableView.backgroundColor = [UIColor blueColor];
    completeInformationTableView.separatorInset = UIEdgeInsetsZero;
    self.completeInformationTableView = completeInformationTableView;

    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    // 提交btn
    //
    CGFloat btnBgViewH = kP(128);
    CGFloat btnBgViewW = self.view.width;
    CGFloat btnBgViewX = 0;
    CGFloat btnBgViewY = self.view.height - btnBgViewH;
    UIView *btnBgView = [[UIView alloc] initWithFrame:CGRectMake(btnBgViewX, btnBgViewY, btnBgViewW, btnBgViewH)];
    btnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    btnBgView.layer.shadowOffset = CGSizeMake(0, - kP(2));
    btnBgView.layer.shadowOpacity = 0.1;
    btnBgView.layer.shadowRadius = kP(2);
    
    btnBgView.backgroundColor = [UIColor whiteColor];
    
    
    //
    CGFloat submitBtnX = kP(34);
    CGFloat submitBtnW = self.view.width - 2 * submitBtnX;
    CGFloat submitBtnY = kP(16);
    CGFloat submitBtnH = btnBgViewH - 2 * submitBtnY;
    UIButton *submitBtn = [[UIButton alloc] initWithFrame:CGRectMake(submitBtnX, submitBtnY, submitBtnW, submitBtnH)];
    [submitBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#CBCBCB" alpha:1.0]] forState:UIControlStateDisabled];
    [submitBtn setBackgroundImage:[UIImage createImageWithColor:kGreenColor] forState:UIControlStateNormal];
    [submitBtn setTitle:@"资料提交" forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateNormal];
    submitBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    submitBtn.layer.cornerRadius = kP(10);
    submitBtn.clipsToBounds = YES;
    [submitBtn addTarget:self action:@selector(saveMyInformation:) forControlEvents:UIControlEventTouchUpInside];
    submitBtn.enabled = NO;
    [btnBgView addSubview:submitBtn];
    [self.view addSubview:btnBgView];
    self.submitBtn = submitBtn;
    
    //
    CGFloat nameTextFieldH = kP(110);
    CGFloat nameTextFieldX = kP(35);
    CGFloat nameTextFieldY = kP(120) * 0.5 - nameTextFieldH * 0.5;
    CGFloat nameTextFieldW = HZScreenW - 1.0 * nameTextFieldX;
    UITextField *nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(nameTextFieldX, nameTextFieldY, nameTextFieldW, nameTextFieldH)];
    nameTextField.placeholder = @"请输入姓名";
    nameTextField.font = [UIFont systemFontOfSize:kP(32)];
    nameTextField.returnKeyType = UIReturnKeyDone;
    nameTextField.delegate = self;
    nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.nameTextField = nameTextField;
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 4;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"completeInforCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:kP(32)];
        
        switch (indexPath.row) {
            case 0: {
                //                        cell.textLabel.text = @"请填写执业医院";
                //                        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

                
                NSString *hospitalStr = self.hospitalStr;
                NSLog(@"-----dddsssaaa----%@", hospitalStr);
                cell.textLabel.text = [[hospitalStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0 ? @"请填写执业医院" : hospitalStr;
                cell.textLabel.textColor = [[hospitalStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0 ?  [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0] : [UIColor blackColor];
                self.hospitalStr = [[hospitalStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0 ? @"" : hospitalStr;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                //
                self.submitBtn.enabled = ![NSString isBlankString:self.hospitalStr] && ![NSString isBlankString:self.subjectDict[@"name"]] && ![NSString isBlankString:self.professionDict[@"dictDataName"]] && ![NSString isBlankString:self.nameTextField.text];
            }
                
                break;
            case 1: {
                NSString *subjectStr = self.subjectDict[@"name"];
                cell.textLabel.text = subjectStr == NULL && [[subjectStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0 ? @"请选择学科" : subjectStr;
                cell.textLabel.textColor = subjectStr == NULL && [[subjectStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0 ?  [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0] : [UIColor blackColor];
                self.subjectId = self.subjectDict[@"dictDepartmentId"];
                self.subjectName = subjectStr;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                //
               self.submitBtn.enabled = ![NSString isBlankString:self.hospitalStr] && ![NSString isBlankString:self.subjectDict[@"name"]] && ![NSString isBlankString:self.professionDict[@"dictDataName"]] && ![NSString isBlankString:self.nameTextField.text];
            }
                
                break;
            case 2: {
                NSString *professionStr = self.professionDict[@"dictDataName"];
                cell.textLabel.text = [[professionStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0 ? @"请选择职称" : professionStr;
                cell.textLabel.textColor = [[professionStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0 ? [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0] : [UIColor blackColor];
                self.professionId = self.professionDict[@"dictId"];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                //
                self.submitBtn.enabled = ![NSString isBlankString:self.hospitalStr] && ![NSString isBlankString:self.subjectDict[@"name"]] && ![NSString isBlankString:self.professionDict[@"dictDataName"]] && ![NSString isBlankString:self.nameTextField.text];
            }
                
                break;
            case 3: {
                
                
                NSLog(@"---------***************_________________");
//                [self.nameTextField removeFromSuperview];
                //                [nameTextField addTarget:self action:@selector(nameTextFieldClick:) forControlEvents:UIControlEventEditingChanged];
                [cell.contentView addSubview:self.nameTextField];
            }
                break;
        }

    return cell;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(120);
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(504);
    }
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return kP(34);
    }
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 1) return nil;
    
    UIView *headBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kP(464), kP(464))];
    headBgView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    //    headBgView.backgroundColor = [UIColor redColor];
    // 头像
    CGFloat headImageViewW = kP(234);
    CGFloat headImageViewH = kP(234);
    CGFloat headImageViewX = HZScreenW * 0.5 - headImageViewW * 0.5;
    CGFloat headImageViewY = kP(74);
    UIImageView *headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headImageViewX, headImageViewY, headImageViewW, headImageViewH)];
    headImageView.image = [UIImage imageNamed:@"doctor_head.png"];
    headImageView.layer.cornerRadius = kP(117);
    headImageView.clipsToBounds = YES;
    [headBgView addSubview:headImageView];
    
    // albel1
    UILabel *label1 = [[UILabel alloc] init];
    label1.text = @"完善个人资料";
    label1.font = [UIFont systemFontOfSize:kP(32)];
    [label1 sizeToFit];
    label1.x = HZScreenW * 0.5 - label1.width * 0.5;
    label1.y = headImageView.bottom + kP(42);
    [headBgView addSubview:label1];
    
    // albel2
    UILabel *label2 = [[UILabel alloc] init];
    label2.text = @"一起加入全国最优秀的医生圈子";
    label2.font = [UIFont systemFontOfSize:kP(26)];
    [label2 sizeToFit];
    label2.x = HZScreenW * 0.5 - label2.width * 0.5;
    label2.y = label1.bottom + kP(18);
    [headBgView addSubview:label2];
    
    // headBgView
    return headBgView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        switch (indexPath.row) {
            case 0: {
                
                NSLog(@"---------搜索执业医院");
                HZHospitalSearchViewController *hospitalSearchVCtr = [HZHospitalSearchViewController new];
                hospitalSearchVCtr.delegate = self;
                [self.navigationController pushViewController:hospitalSearchVCtr animated:NO];
            }
                
                break;
            case 1: {
                
                HZSubjectSelectionViewController *subjectSelectionVCtr = [HZSubjectSelectionViewController new];
                subjectSelectionVCtr.delegate = self;
                [self.navigationController pushViewController:subjectSelectionVCtr animated:YES];
            }
                break;
            case 2: {
                HZProfessionSelectionViewController *professionSelectionVCtr = [HZProfessionSelectionViewController new];
                professionSelectionVCtr.delegate = self;
                [self.navigationController pushViewController:professionSelectionVCtr animated:YES];
                NSLog(@"---------搜索执业医院");
            }
                break;
        }
    }
}
#pragma mark -- 提交信息
- (void)saveMyInformation:(UIButton *)submitBtn {
    NSLog(@"---------提交信息");
    
    [self.activityIndicator startAnimating];
    self.view.userInteractionEnabled = NO;
    NSString *phoneNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"phoneNumber"];
    NSLog(@"---------%@-----%@-----%@----%@------%@", self.hospitalStr, self.subjectId, self.professionId, phoneNumber, self.nameTextField.text);
    
    if (self.hospitalStr.length == 0 || self.subjectId.length == 0 || self.professionId.length == 0 || self.nameTextField.text.length == 0 || phoneNumber.length < 11)  {
        
        [self.activityIndicator stopAnimating];
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"资料不完整，请您核查" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.nameTextField resignFirstResponder];
            self.view.userInteractionEnabled = YES;
        }];
        if (kCurrentSystemVersion >= 9.0) {
            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        }
        [alertCtr addAction:ensureAlertAction];
        [self presentViewController:alertCtr animated:YES completion:nil];
    } else {
        
//        [self.activityIndicator startAnimating];
//        NSString *alertStr = [NSString stringWithFormat:@"您要注册账号:%@吗?", phoneNumber];
//        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:alertStr preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //
            [HZInforCompleteTool upLoadUserInforWithHospital:self.hospitalStr subjectIdArr:@[self.subjectId] subjectName:self.subjectName professionId:self.professionId userName:self.nameTextField.text phoneNumber:phoneNumber inviteCode:nil success:^(id responseObject) {
                NSLog(@"---提交成功信息------%@", responseObject);
                [self.activityIndicator stopAnimating];
                self.view.userInteractionEnabled = YES;
                HZCompleteDoneTableViewController *completeDoneTVCtr = [[HZCompleteDoneTableViewController alloc] initWithStyle:UITableViewStylePlain];
                [self.navigationController pushViewController:completeDoneTVCtr animated:YES];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----提交失败-----%@", error);
                [self.activityIndicator stopAnimating];
                //
                
                
                UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"提交失败" preferredStyle:UIAlertControllerStyleAlert];
                
                
                
                UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    self.view.userInteractionEnabled = YES;
                }];
                
                
                if (kCurrentSystemVersion >= 9.0) {
                    [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                }
                [alertCtr addAction:ensureAlertAction];
                [self presentViewController:alertCtr animated:YES completion:nil];
                
            }];

//        }];
//        UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//            [self.navigationController popViewControllerAnimated:YES];
//        }];
//        [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
//        [alertCtr addAction:ensureAlertAction];
//        [alertCtr addAction:cancelAlertAction];
//        [self presentViewController:alertCtr animated:YES completion:nil];
        
    }
}

#pragma mark -- 取消键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    self.completeInformationTableView.y = 0;

     [self.nameTextField resignFirstResponder];

    
    self.submitBtn.enabled = ![NSString isBlankString:self.hospitalStr] && ![NSString isBlankString:self.subjectDict[@"name"]] && ![NSString isBlankString:self.professionDict[@"dictDataName"]] && ![NSString isBlankString:self.nameTextField.text];
    
    return YES;
}


#pragma mark -- keyboardWillChangeFrameNotification
- (void)keyboardWillChangeFrameNotification:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyBoardH = keyboardRect.size.height;
//    NSLog(@"---------sds---:%lf", self.keyBoardH);
    
//    [self textFieldDidBeginEditing:self.nameTextField];
//    [self.completeInformationTableView setContentOffset:CGPointMake(0,kP(50))];
    self.completeInformationTableView.y = 64 - self.keyBoardH;

    
}
#pragma mark -- UITextFieldDelegate
//
- (void)textFieldDidBeginEditing:(UITextField *)textField {
//   NSLog(@"---------sdsssss---:%lf", self.keyBoardH);
    
    
//    if (![NSNotificationCenter defaultCenter]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification:) name:UIKeyboardWillShowNotification object:nil];
//    }
    
    
    
}


#pragma mark -- <#class#>
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    self.completeInformationTableView.y = 0;
    [self.completeInformationTableView reloadData];
    //
   [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    [self.nameTextField resignFirstResponder];
//    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
}
#pragma mark -- HZHospitalSearchViewControllerDeleagte
- (void)passHospitalName:(NSString *)hospitalName {
    self.hospitalStr = hospitalName;
}

#pragma mark -- HZProfessionSelectionViewControllerDelegate
- (void)passProfessionDict:(NSDictionary *)professionDict {
    
    self.professionDict = professionDict;
}

#pragma mark -- HZSubjectSelectionViewControllerDeleagte
- (void)passSubjectDict:(NSDictionary *)subjectDict {
    self.subjectDict = subjectDict;
}

//- (BOOL)textFieldShouldClear:(UITextField *)textField {
//    return YES;
//}
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    if (textField == self.nameTextField){
//
//        if (range.length == 1 && string.length == 0) {
//            return YES;
//        }
//        else if (self.nameTextField.text.length >= 10) {
//            self.nameTextField.text = [textField.text substringToIndex:10];
//            return NO;
//        }
//
//    }
//    return YES;
//}

//- (void)textFieldDidEndEditing:(UITextField *)textField {
//
//    NSLog(@"---------%@", textField.text);
//    if (textField == self.nameTextField) {
//        if (textField.text.length >= 5) {
//            self.nameTextField.userInteractionEnabled = NO;
//        } else {
//            self.nameTextField.userInteractionEnabled = YES;
//        }
//    }
//}

//- (void)limitTextLength {
//    NSLog(@"-----eee----%@", self.nameTextField.text);
//
//    if (self.nameTextField.text.length >= 5) {
//        self.nameTextField.enabled = NO;
//    } else {
//        self.nameTextField.enabled = YES;
//    }
//
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
