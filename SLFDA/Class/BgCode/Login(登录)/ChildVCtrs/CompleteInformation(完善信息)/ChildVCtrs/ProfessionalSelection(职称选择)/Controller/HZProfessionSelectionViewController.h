//
//  HZProfessionSelectionViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZProfessionSelectionViewControllerDelegate <NSObject>

- (void)passProfessionDict:(NSDictionary *)professionDict;

@end
@interface HZProfessionSelectionViewController : HZViewController
@property (nonatomic, weak) id<HZProfessionSelectionViewControllerDelegate>delegate;
@end
