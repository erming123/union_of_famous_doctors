//
//  HZProfessionSelectionViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZProfessionSelectionViewController.h"
#import "HZInforCompleteTool.h"
@interface HZProfessionSelectionViewController ()<UITableViewDataSource, UITableViewDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *professionTableView;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *professionArr;
@end

@implementation HZProfessionSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationItem.title = @"职称选择";
    //
    HZTableView *professionTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    professionTableView.dataSource = self;
    professionTableView.delegate = self;
    professionTableView.tableFooterView = [UIView new];
    [self.view addSubview:professionTableView];
    self.professionTableView = professionTableView;
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    //
    [self getProfessionArr];

}


- (void)getProfessionArr {
    [self.activityIndicator startAnimating];
    [HZInforCompleteTool getProfessionArrWithSuccess:^(id responseObject) {
        NSLog(@"-----获取职称成功----%@", responseObject);
        
        NSDictionary *resultsDict = responseObject[@"results"];
        self.professionArr = resultsDict[@"dictionaryData"];
        //        NSLog(@"-----获取学科成功----%@", [self.professionArr lastObject]);
        [self.professionTableView reloadData];
        [self.activityIndicator stopAnimating];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"-----获取职称失败----%@", error);
    }];
}

- (NSArray *)professionArr {
    if (!_professionArr) {
        _professionArr = [NSArray array];
    }
    return _professionArr;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -- dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.professionArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"professionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    cell.textLabel.text = self.professionArr[indexPath.row][@"dictDataName"];
    cell.textLabel.highlightedTextColor = [UIColor colorWithHexString:@"#21B8C6" alpha:1.0];
    return cell;
}

#pragma mark -- delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(passProfessionDict:)]) {
        [self.delegate passProfessionDict:self.professionArr[indexPath.row]];
    }

    [self.navigationController popViewControllerAnimated:YES];
}
@end
