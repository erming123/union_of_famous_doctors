//
//  HZHospitalSearchViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZHospitalSearchViewController.h"
#import "HZInforCompleteTool.h"
#import "HZSearchBar.h"
@interface HZHospitalSearchViewController ()<HZSearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZSearchBar *searchBar;
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *searchTableView;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *hospitalArr;
@property (nonatomic, assign) BOOL isSearched;
@property (nonatomic, assign) BOOL isChangeTextNil;
@end

@implementation HZHospitalSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGFloat searBarX = kP(20);
    CGFloat searBarY = kP(14);
    CGFloat searBarW = HZScreenW - searBarX - kP(26);
    CGFloat searBarH = kP(56);
    HZSearchBar *searchBar = [[HZSearchBar alloc] initWithFrame:CGRectMake(searBarX, searBarY, searBarW, searBarH)];
    searchBar.delegate = self;
    searchBar.placeholder = @"请输入医院名称";
    self.navigationItem.titleView = searchBar;
    self.searchBar = searchBar;
    
    //
    HZTableView *searchTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    searchTableView.dataSource = self;
    searchTableView.delegate = self;
    searchTableView.tableFooterView = [UIView new];
    [self.view addSubview:searchTableView];
    self.searchTableView = searchTableView;
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
}

- (NSArray *)hospitalArr {
    if (!_hospitalArr) {
        _hospitalArr = [NSArray array];
    }
    
    return _hospitalArr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.isSearched ? self.hospitalArr.count + 1 : self.hospitalArr.count;
//    return self.isChangeTextNil ? 0 : self.hospitalArr.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"hospitalCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    if (indexPath.row == self.hospitalArr.count && self.isSearched == YES) {
    
    NSLog(@"-----123----%ld", self.hospitalArr.count);
//    if (indexPath.row == self.hospitalArr.count) {
        
        cell.textLabel.textColor = kGreenColor;
        cell.textLabel.font = [UIFont systemFontOfSize:kP(32)];
        NSString *creatStr = [NSString stringWithFormat:@"+创建\"%@\"医院", self.searchBar.textFieldText];
        cell.textLabel.text = creatStr;
    } else {
        cell.textLabel.text = self.hospitalArr[indexPath.row][@"name"];
        cell.textLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.highlightedTextColor = kGreenColor;
    return cell;
}

#pragma mark -- delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NSString *hospitalName;
    if (indexPath.row == self.hospitalArr.count) {// 自己创建
        hospitalName = self.searchBar.textFieldText;
    } else {// 点击添加
        hospitalName = self.hospitalArr[indexPath.row][@"name"];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(passHospitalName:)]) {
        [self.delegate passHospitalName:hospitalName];
    }

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -- 去掉返回按钮
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
//    self.searchBar.isBecomeFirstResponder = NO;
    self.navigationItem.hidesBackButton = YES;
}


#pragma mark -- HZSearchBarDelegate

- (void)getSearchResult:(HZSearchBar *)searBar {
    
    
    
    self.searchTableView.height = self.view.height;
    if ([NSString isBlankString:searBar.textFieldText]) return;
    
    [self.activityIndicator startAnimating];
    
    NSLog(@"-----dssdsdsds123----%@", searBar.textFieldText);
    
    
    
    
    [HZInforCompleteTool getHospitalArrWithHospitalName:searBar.textFieldText success:^(id responseObject) {
        self.isSearched = YES;
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            self.hospitalArr = resultsDict[@"hopitals"];
            [self.searchTableView reloadData];
            [self.activityIndicator stopAnimating];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---------dfsfsdfsdfsdfdsf");
    }];
    
}

- (void)cancelBtnClick:(HZSearchBar *)searBar {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changeText:(NSString *)changeText {
    NSLog(@"-----ddd----%@", changeText);
//
    [self getSearchResult:self.searchBar];
//    
    self.isChangeTextNil = [changeText isEqualToString:@""];

//    
    [self changeTableViewHeight:0];
    
}

- (void)changeTableViewHeight:(CGFloat)keyBoardH {
    self.searchTableView.height = self.view.height - kP(600);
    [self.searchTableView reloadData];
}
#pragma mark -- viewWillDisappear
- (void)viewWillDisappear:(BOOL)animated {
    self.isSearched = NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
