//
//  HZSearchBar.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HZSearchBar;
@protocol HZSearchBarDelegate <NSObject>

- (void)cancelBtnClick:(HZSearchBar *)searBar;
- (void)getSearchResult:(HZSearchBar *)searBar;
- (void)changeText:(NSString *)changeText;
- (void)changeTableViewHeight:(CGFloat)keyBoardH;
@end
@interface HZSearchBar : UIView
@property (nonatomic, weak) id<HZSearchBarDelegate> delegate;
@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, assign) BOOL isBecomeFirstResponder;
@property (nonatomic, copy) NSString *textFieldText;
@end
