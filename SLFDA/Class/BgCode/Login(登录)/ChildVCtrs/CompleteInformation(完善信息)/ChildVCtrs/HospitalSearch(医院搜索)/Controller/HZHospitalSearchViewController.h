//
//  HZHospitalSearchViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZHospitalSearchViewControllerDeleagte <NSObject>

- (void)passHospitalName:(NSString *)hospitalName;

@end
@interface HZHospitalSearchViewController : HZViewController
@property (nonatomic, weak) id<HZHospitalSearchViewControllerDeleagte>delegate;
@end
