//
//  HZSubjectSelectionViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZSubjectSelectionViewController.h"
#import "HZInforCompleteTool.h"
@interface HZSubjectSelectionViewController ()<UITableViewDataSource, UITableViewDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *subjectTableView;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *subjectArr;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation HZSubjectSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationItem.title = @"学科选择";
       //
    HZTableView *subjectTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    subjectTableView.dataSource = self;
    subjectTableView.delegate = self;
    subjectTableView.tableFooterView = [UIView new];
    [self.view addSubview:subjectTableView];
    self.subjectTableView = subjectTableView;
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    //
     [self getSubjectArr];
}

- (void)getSubjectArr {
    [self.activityIndicator startAnimating];
    [HZInforCompleteTool getSubjectArrWithSuccess:^(id responseObject) {

        NSDictionary *resultsDict = responseObject[@"results"];
        self.subjectArr = resultsDict[@"dictDepartmentList"];
        NSLog(@"-----获取学科成功----%@", [self.subjectArr lastObject]);
        [self.subjectTableView reloadData];
        [self.activityIndicator stopAnimating];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----获取学科失败-----%@", error);
    }];
}
- (NSArray *)subjectArr {
    if (!_subjectArr) {
        _subjectArr = [NSArray array];
    }
    
    return _subjectArr;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.subjectArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"subjectCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    cell.textLabel.text = self.subjectArr[indexPath.row][@"name"];
    cell.textLabel.highlightedTextColor = [UIColor colorWithHexString:@"#21B8C6" alpha:1.0];
    return cell;
}



#pragma mark -- delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.subjectArr[indexPath.row] forKey:@"subject"];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(passSubjectDict:)]) {
        [self.delegate passSubjectDict:self.subjectArr[indexPath.row]];
    }

    [self.navigationController popViewControllerAnimated:YES];
}
//#pragma mark -- 支持横向
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//
//
//    return UIInterfaceOrientationMaskLandscapeLeft;  //支持横向
//}
//
////设置为允许旋转
//- (BOOL) shouldAutorotate {
//    
//    
//    return YES;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
