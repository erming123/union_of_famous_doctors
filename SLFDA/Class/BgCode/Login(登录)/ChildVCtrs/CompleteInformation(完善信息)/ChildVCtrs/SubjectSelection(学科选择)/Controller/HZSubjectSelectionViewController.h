//
//  HZSubjectSelectionViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZSubjectSelectionViewControllerDeleagte <NSObject>

- (void)passSubjectDict:(NSDictionary *)subjectDict;

@end
@interface HZSubjectSelectionViewController : HZViewController
@property (nonatomic, weak) id<HZSubjectSelectionViewControllerDeleagte>delegate;
@end
