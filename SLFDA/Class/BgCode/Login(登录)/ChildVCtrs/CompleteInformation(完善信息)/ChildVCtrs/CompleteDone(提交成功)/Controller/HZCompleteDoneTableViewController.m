//
//  HZCompleteDoneTableViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/10.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZCompleteDoneTableViewController.h"
#import "HZHomeViewController.h"
#import "HZUserTool.h"
#import "HZTabBarController.h"
@interface HZCompleteDoneTableViewController ()
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation HZCompleteDoneTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setViewPanBack];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationItem.title = @"资料完善";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    //
    CGFloat completeImageViewW = kP(230);
    CGFloat completeImageViewH = kP(230);
    CGFloat completeImageViewX = HZScreenW * 0.5 - completeImageViewW * 0.5;
    CGFloat completeImageViewY = kP(174);
    UIImageView *completeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(completeImageViewX, completeImageViewY, completeImageViewW, completeImageViewH)];
    completeImageView.image = [UIImage imageNamed:@"success.png"];
    completeImageView.layer.cornerRadius = kP(115);
    completeImageView.clipsToBounds = YES;
    [self.tableView addSubview:completeImageView];
    
    
    //
    UILabel *successLabel = [[UILabel alloc] init];
    successLabel.text = @"资料完善成功";
    successLabel.font = [UIFont systemFontOfSize:kP(36)];
    [successLabel sizeToFit];
    successLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    successLabel.x = HZScreenW * 0.5 - successLabel.width * 0.5;
    successLabel.y = completeImageView.bottom + kP(42);
    [self.tableView addSubview:successLabel];
    
    //
    CGFloat enterHomeBtnX = kP(88);
    CGFloat enterHomeBtnY = successLabel.bottom + kP(482);
    CGFloat enterHomeBtnW = HZScreenW - 2 * enterHomeBtnX;
    CGFloat enterHomeBtnH = kP(106);
    UIButton *enterHomeBtn = [[UIButton alloc] initWithFrame:CGRectMake(enterHomeBtnX, enterHomeBtnY, enterHomeBtnW, enterHomeBtnH)];
    enterHomeBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    [enterHomeBtn setTitle:@"进入首页" forState:UIControlStateNormal];
    enterHomeBtn.titleLabel.textColor = [UIColor whiteColor];
    enterHomeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    enterHomeBtn.layer.cornerRadius = kP(16);
    enterHomeBtn.clipsToBounds = YES;
    [enterHomeBtn addTarget:self action:@selector(enterHomeVCtr:) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView addSubview:enterHomeBtn];
    
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
}


#pragma mark -- 进入首页

- (void)enterHomeVCtr:(UIButton *)enterHomeBtn {
  
    [self.activityIndicator startAnimating];
    self.view.userInteractionEnabled = NO;
    //
    [HZUserTool getUserWithSuccess:^(id responseObject) {
        
        NSLog(@"----responseObjec--%@", responseObject);
        if ([responseObject[@"state"] isEqual:@200]) {// 信息完善
            
            NSLog(@"----dddssssaaaaaa-----%@", responseObject);
            
            ;

            
            NSDictionary *dataDict = responseObject;
            NSDictionary *resultDict = dataDict[@"results"];
            NSDictionary *userDict = resultDict[@"user"];
            HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
            user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
            //1
            [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
            
//            HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//            homeVCtr.user = user;
//            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
            
            HZTabBarController *tabBarCtr = [HZTabBarController new];
            [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
            
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hospital"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"subject"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profession"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.view.userInteractionEnabled = YES;
            
        } else {

            
            
            NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
            
            if (!userDict) {
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.view.userInteractionEnabled = YES;

            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
            // 选择页面
            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
        }];
        if (kCurrentSystemVersion >= 9.0) {
            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        }
        [alertCtr addAction:ensureAlertAction];
        [self presentViewController:alertCtr animated:YES completion:nil];
                return;
            }
            
            //
//            HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
//            user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
//            HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
//            homeVCtr.user = user;
//            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//            [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
            HZTabBarController *tabBarCtr = [HZTabBarController new];
            [UIApplication sharedApplication].keyWindow.rootViewController = tabBarCtr;
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hospital"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"subject"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profession"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.view.userInteractionEnabled = YES;

        }
        
        [self.activityIndicator stopAnimating];
    
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        

        

        
        NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
        
        if (!userDict) {
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {



        }];
        if (kCurrentSystemVersion >= 9.0) {
            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        }
        [alertCtr addAction:ensureAlertAction];
        [self presentViewController:alertCtr animated:YES completion:nil];

        //
        [self.activityIndicator stopAnimating];
        self.view.userInteractionEnabled = YES;
            return;
        }
        
        //
        HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
        user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
        HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
        homeVCtr.user = user;
        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
        [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hospital"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"subject"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profession"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.view.userInteractionEnabled = YES;
    }];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
