//
//  HZVersionTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZVersionTool.h"
#import "HZHttpsTool.h"
#define VersionStr @"https://gateway.rubikstack.com/cfdu/v1/openapi/json/versioncontroller/ios"
@implementation HZVersionTool

+ (void)getAppStoreVersionSuccess:(void(^)(NSArray *versionArr))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    [HZHttpsTool GET:VersionStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
       NSArray *versionArr = responseObject[@"results"];
        if (success) {
            success(versionArr);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}
@end
