//
//  HZAppStoreVersion.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZAppStoreVersion : NSObject
@property (nonatomic, copy) NSString *isForceUp;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *version;
@end
