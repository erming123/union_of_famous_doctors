//
//  HZVersionTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HZAppStoreVersion;
@interface HZVersionTool : NSObject // 一括二， 标志和参数
+ (void)getAppStoreVersionSuccess:(void(^)(NSArray *versionArr))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
