//
//  HZDataHelper.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/26.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZDataHelper : NSObject
+ (instancetype)shareInstance;

// 获取
- (void)getUserWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

- (void)getOrdersWithPages:(NSInteger)pages countPerPage:(NSInteger)count stateCode:(NSString *)stateCode success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
// 获取已完成订单列表
- (void)getDoneOrdersWithPages:(NSInteger)pages countPerPage:(NSInteger)count success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
// 订单详情
- (void)getOrderDetailWithOrderID:(NSString *)orderID success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
- (void)getWalletInforWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
- (void)getCapitalListWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

- (void)getOrderStateStringArrWithVersion:(NSString *)version success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failuree;

- (void)getCheckListWithBookingOrderId:(NSString *)bookingOrderId success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
- (void)getBiochemistryCheckListWithCheckOrderId:(NSString *)checkOrderId patientHospitalId:(NSString *)patientHospitalId success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//
- (void)getBankListWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//
- (void)endLocaOrderWithBookingOrderId:(NSString *_Nullable)bookingOrderId success:(void (^_Nullable)(id _Nullable responseObject))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;



@property (nonatomic,strong) NSOperationQueue * _Nullable downLoadImgQueue;


@end
