//
//  HZDataHelper.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/26.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZDataHelper.h"
#import "HZUserTool.h"
#import "HZOrderHttpsTool.h"
#import "HZWalletHttpsTool.h"
#import "HZCheckListTool.h"
//#import "HZDB.h"
#import "HZHttpsTool.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZDataHelper ()
@property (nonatomic, copy) NSString *userInforJson;
@property (nonatomic, copy) NSString *ordersJson;
@property (nonatomic, copy) NSString *stateOrdersJson;
@property (nonatomic, copy) NSString *walletInforJson;
@end
NS_ASSUME_NONNULL_END
@implementation HZDataHelper
+ (instancetype)shareInstance {
    static HZDataHelper *dataHelper;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataHelper = [[HZDataHelper alloc] init];
    });
    
    return dataHelper;
}


-(NSOperationQueue *)downLoadImgQueue{
    
    if (!_downLoadImgQueue) {
        _downLoadImgQueue = [[NSOperationQueue alloc]init];
        _downLoadImgQueue.maxConcurrentOperationCount =4; // 异步队列
    }
    return _downLoadImgQueue;
    
}

- (void)getUserWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {

        
        
        [HZUserTool getUserWithSuccess:^(id responseObject) {
//            self.userInforJson = responseObject;
            
            
//            HZLog(@"-----aaaa----%@", responseObject);
            
            if (success) {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            HZLog(@"-----aaaa----%@", error);

            if (failure) {
                failure(task, error);
            }
        }];
    
}


- (void)getOrdersWithPages:(NSInteger)pages countPerPage:(NSInteger)count stateCode:(NSString *)stateCode success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZOrderHttpsTool getOrdersWithPages:pages countPerPage:count stateCode:stateCode success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];
}

- (void)getDoneOrdersWithPages:(NSInteger)pages countPerPage:(NSInteger)count success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    [HZOrderHttpsTool getOrdersWithPages:pages countPerPage:count stateCode:@"008" success:^(id responseObject) {
        
//        HZLog(@"----erererererre-----%@", responseObject);
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(task, error);
        }
    }];

}

- (void)getOrderDetailWithOrderID:(NSString *)orderID success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    [HZOrderHttpsTool getOrderDetailWithOrderID:orderID success:^(id responseObject) {
        
    
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(task, error);
        }
    }];
}


- (void)getWalletInforWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
   
    [HZWalletHttpsTool getWalletInforWithSuccess:^(id responseObject) {
        
            if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        HZLog(@"---------%@", error);
        if (failure) {
            failure(task, error);
        }
    }];
}

- (void)getOrderStateStringArrWithVersion:(NSString *)version success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZOrderHttpsTool getBookingOrderStateStrWithVersion:@"1" success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        HZLog(@"---------%@", error);
        if (failure) {
            failure(task, error);
        }
    }];
    
}

- (void)getCapitalListWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    [HZWalletHttpsTool getCapitalListWithSuccess:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(task, error);
        }
    }];
}


- (void)getCheckListWithBookingOrderId:(NSString *)bookingOrderId success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    [HZCheckListTool getCheckListWithBookingOrderId:bookingOrderId  patient:nil userHospitalId:nil success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(task, error);
        }
    }];
}

- (void)getBiochemistryCheckListWithCheckOrderId:(NSString *)checkOrderId patientHospitalId:(NSString *)patientHospitalId success:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    [HZCheckListTool getBiochemistryCheckListWithCheckModelId:checkOrderId patientHospitalId:patientHospitalId  success:^(id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(task, error);
        }
    }];
}


- (void)getBankListWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    NSString *bankListStr = kGatewayUrlStr(@"dictionary/dict/batchcommon?params={typeVersion:[{\"type\":\"deposit_bank\",\"version\":\"1\"}]}");
    [HZHttpsTool GET:bankListStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        
        HZLog(@"-----ewewewrerwerwerwerwerwerwerw----%@", responseObject);
        if (success) {
            
            
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(task, error);
        }

    }];
}

- (void)endLocaOrderWithBookingOrderId:(NSString *_Nullable)bookingOrderId success:(void (^_Nullable)(id _Nullable responseObject))success failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure {
    
    
    NSDictionary *paramsDict = @{@"bookingOrderId" : bookingOrderId};
    [HZHttpsTool POST:kGatewayUrlStr(@"cfdu/v2/openapi/json/booking/order/process/finish") headDomainValue:nil parameters:paramsDict progress:nil success:^(id responseObject) {
        HZLog(@"-----结束本地订单成功----%@", responseObject);
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(task, error);
        }
    }];
}
@end
