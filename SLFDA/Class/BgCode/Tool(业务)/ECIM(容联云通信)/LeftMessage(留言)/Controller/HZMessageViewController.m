//
//  HZIMChatViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZMessageViewController.h"
#import "HZKeyboardView.h"
#import "HZMessageDB.h"
#import "HZMessageCell.h"
#import <AVFoundation/AVFoundation.h>
#import "CommonTools.h"
#import "UIImageView+WebCache.h"

//
#import "TZImagePickerController.h"
#import "UIView+Layout.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "TZImageManager.h"
#import "TZVideoPlayerController.h"

//
//
#import "HZOrder.h"
#import "HZUserTool.h"
//
#import "HZUser.h"
//
#import "IQKeyboardManager.h"
//
#import "HZChatHelper.h"
#import "SDPhotoBrowser.h"
#define DefaultThumImageHigth 90.0f
#define DefaultPressImageHigth 960.0f//
@interface HZMessageViewController ()<UITableViewDataSource, UITableViewDelegate, HZKeyboardViewDelegate,HZMessageCellDelegate,TZImagePickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,SDPhotoBrowserDelegate>

//** <#注释#>*/
@property (nonatomic, weak) HZTableView *chatTableView;
@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) HZKeyboardView *keyboardView;
@property (nonatomic, strong) NSMutableArray *messageArr;
//** <#注释#>*/
@property (nonatomic, assign) SystemSoundID receiveSound;
@property (nonatomic, assign) SystemSoundID sendSound;
//** <#注释#>*/
@property (nonatomic, copy) NSString *imagePath;
//** <#注释#>*/
@property (nonatomic, strong) HZMessageDB *messageDB;
//
//** <#注释#>*/
@property (nonatomic, strong) NSString *callID;
//** <#注释#>*/
@property (nonatomic, weak) UIButton *receiveBtn;

//** <#注释#>*/
@property (nonatomic, strong) NSDictionary *photoMessageDict;

@property (nonatomic, assign) long long lastTimeStamp;
@property (nonatomic, assign) BOOL isTimeShow;


//
//** <#注释#>*/
@property (nonatomic, assign) CGFloat cellMaxY;
@property (nonatomic, assign) CGRect keyboardF;
//** <#注释#>*/
@property (nonatomic, strong) NSDictionary *userInfoDict;

/**
 *  ----------------------
 */
@property (nonatomic, strong) TZImagePickerController *imagePickerVc;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *selectedPhotos;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *selectedAssets;


//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *imagePathArr;


@property (nonatomic, copy) NSString *chatterID;

//
@property (nonatomic, copy) NSString *sendUserData;
@property (nonatomic, copy) NSString *saveUserData;

@property (nonatomic,weak) UIActivityIndicatorView*activityIndicator;

@property (nonatomic,strong) UIImage *photoNeedImage;
@end

@implementation HZMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"f4f4f6" alpha:1.0];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    //    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //2. 即时通讯业务
    //    [self makeChat];
    
    //3.播放音效
    [self playSound];
    
    //4.
    self.selectedPhotos = [NSMutableArray array];
    self.selectedAssets = [NSMutableArray array];
    
    
    //6.
    //    [self setUpChatAvatar];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = [UIApplication sharedApplication].keyWindow.center;
    activityIndicator.color = [UIColor grayColor];
    [[UIApplication sharedApplication].keyWindow addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    [self getData];
    
    //7.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendLeftMessageSuccess:) name:KNOTIFICATION_onMesssageSend object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLeftMessageSuccess:) name:KNOTIFICATION_onMesssageReceive object:nil];
    
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLeftMessageSuccess:) name:KNOTIFICATION_onMesssageOffLineReceive object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMyTable) name:kEnterForeground object:nil];
    
    NSLog(@"----sdf--:%@", self.chatter);

    NSLog(@"---qazwsxedc-------:%@----------:%@", self.chatter, self.user);
}


-(void)getData {
    
    if (self.chatter) {
        [self setUpSubViews];
        [self refreshMyTable];
//        _chatTableView.height = self.view.height - kP(101) - SafeAreaTopHeight;
//        _keyboardView.y = self.chatTableView.height;
        return;
    }
    
    
    [self.activityIndicator startAnimating];
    
    [HZUserTool getUserDetailInforWithOpenId:_chatterOpenId success:^(id responseObject) {
        
        
        NSLog(@"-aswwwww--:%@", responseObject);
        
        
        if ([responseObject[@"state"] isEqual:@200]) {
            
            
            NSDictionary *resultDict = responseObject[@"results"];
            NSArray *doctorArr = resultDict[@"doctors"];
            
            //
            NSDictionary *doctorDict;
            if (doctorArr.count) {
               doctorDict = doctorArr[0];
            } else {
                doctorDict = nil;
                [MBProgressHUD showError:@"未获取到IM对象的信息" toView:nil];
            }
            
    
            
            HZUser *expert = [HZUser mj_objectWithKeyValues:doctorDict];
            
            self.chatter = expert;
            
            //
            HZUser *user = [HZUser mj_objectWithKeyValues:[[NSUserDefaults standardUserDefaults] objectForKey:@"user"]];
            self.user = user;
            
            //1. 设置页面
            [self setUpSubViews];
            
            [self refreshMyTable];
            
        }else{
            [MBProgressHUD showError:responseObject[@"subMsg"] toView:nil];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        NSLog(@"-sa--:%@", error);
        [self.activityIndicator stopAnimating];
        [self.activityIndicator removeFromSuperview];
    }];
}


#pragma mark -- chatter

- (void)setChatter:(HZUser *)chatter {
    _chatter = chatter;
}

- (void)setUser:(HZUser *)user {
    _user = user;
}

- (void)sendLeftMessageSuccess:(NSNotification *)notification {
    ECMessage *message = notification.object;
    NSString *imagePath = notification.userInfo[@"imagePath"];
    [self addToMesageDBWithMessage:message withLocalImagePath:imagePath];
}

- (void)receiveLeftMessageSuccess:(NSNotification *)notification {
    
    ECMessage *message = notification.object;
    if ([message.userData isEqualToString:self.saveUserData]) {
        // 收到同一个人
        // [self.messageDB upDateUnReadMsgCountWithOrderId:self.saveUserData];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"offLineMsgCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        ECTextMessageBody *textMsgBody;
        ECImageMessageBody *msgBody;
        NSString *timeStr;
        switch (message.messageBody.messageBodyType) {
            case MessageBodyType_Text:
                textMsgBody = (ECTextMessageBody *)message.messageBody;
                if (message.timestamp) {
                    if (message.timestamp.length > 3) {
                        timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
                    }
                }
                break;
            case MessageBodyType_Image:
                msgBody = (ECImageMessageBody *)message.messageBody;
                if (message.timestamp) {
                    if (message.timestamp.length > 3) {
                        timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
                    }
                }
                break;
            default:
                break;
        }
        
        NSDictionary *dict = @{
                               @"from":message.from,
                               @"to":message.to,
                               @"messageId":message.messageId,
                               @"timestamp":timeStr,
                               @"text":textMsgBody.text?textMsgBody.text:[NSNull null],
                               @"messageBodyType":@(message.messageBody.messageBodyType),
                               @"thumbnailDownloadStatus":msgBody?@(msgBody.thumbnailDownloadStatus):[NSNull null],
                               @"remotePath":msgBody?msgBody.remotePath:[NSNull null],
                               @"thumbnailRemotePath":msgBody?msgBody.thumbnailRemotePath:[NSNull null],
                               @"userData":message.userData,
                               @"sessionId":message.sessionId,
                               @"isGroup":@(message.isGroup),
                               @"messageState":@(message.messageState),
                               @"isRead":@(message.isRead),
                               @"groupSenderName":@"暂无",
                               @"isTimeShow":@(self.isTimeShow)
                               };
        
        [self.messageArr addObject:dict];
        //   self.messageArr = [self.messageArr mutableCopy];
        [self.chatTableView reloadData];
        [self scrollowToBottom];
    }
}
//
//- (void)setIMOrder:(HZOrder *)IMOrder {
//    _IMOrder = IMOrder;
//}


#pragma mark -- refesh
- (void)refreshMyTable {
    
    //1. 未读数全部置为零
    [self.messageDB upDateUnReadMsgCountWithOrderId:self.saveUserData];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"offLineMsgCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSMutableArray *arr = [self.messageDB getMessageArrWithOrderId:self.saveUserData];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.messageArr = [arr mutableCopy];
            NSLog(@"-=-=-=-=-=-=-12=-=-=-=-=-=-=-=-==%@", self.messageArr);
            [self.chatTableView reloadData];
            [self.activityIndicator stopAnimating];
            [self.activityIndicator removeFromSuperview];
            [self scrollowToBottom];
        });
    });
    
    //
    //    NSLog(@"-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==%@", self.messageArr);
    
    
    //    NSMutableArray *unReadMsgCountArr = [NSMutableArray array];
    //    for (NSDictionary *msgDict in self.messageArr) {
    //
    //        if (!msgDict[@"isRead"] && [msgDict[@"from"] isEqualToString:self.IMOrder.remoteAccount]) {
    //            NSDictionary *dict = msgDict;
    //            msgDict[@"isRead"] = YES;
    //            [unReadMsgCountArr addObject:msgDict];
    //        }
    //    }
    
    
    
    //    [[NSUserDefaults standardUserDefaults] setObject:unReadMsgCountArr forKey:kUnReadMsgCount];
    
}

#pragma mark -- 获取信息数组

- (NSMutableArray *)messgeArr {
    if (_messageArr == nil) {
        _messageArr = [NSMutableArray array];
    }
    return _messageArr;
}

#pragma mark -- 初始化数据库
- (HZMessageDB *)messageDB {
    if (_messageDB == nil) {
        _messageDB = [HZMessageDB initMessageDB];
    }
    return _messageDB;
}





#pragma mark -- 设置页面

- (void)setUpSubViews {
    
    
    self.chatterID = self.chatter.remoteAccount;
    
    //    NSString *myChatIdStr = [kMyChatId stringByReplacingOccurrencesOfString:@"sl_" withString:@""];
    //    NSString *chatterIdStr = [self.chatterID stringByReplacingOccurrencesOfString:@"sl_" withString:@""];
    // 从小到大排列
    //    if ([self.chatterID compare:kMyChatId options:NSNumericSearch] == NSOrderedSame || [self.chatterID compare:kMyChatId options:NSNumericSearch] == NSOrderedDescending) {
    
    //        if ([self.chatter.openid compare:self.user.openid options:NSNumericSearch] == NSOrderedSame || [self.chatter.openid compare:self.user.openid options:NSNumericSearch] == NSOrderedDescending) {
    //            self.chatUserData = [NSString stringWithFormat:@"%@@%@", self.user.openid, self.chatter.openid];
    //        } else {
    //            self.chatUserData = [NSString stringWithFormat:@"%@@%@", self.chatter.openid, self.user.openid];
    //        }
    
    self.sendUserData = [NSString stringWithFormat:@"%@,%@,%@", self.user.openid, self.user.userName, self.user.titleName];
    self.saveUserData = [NSString stringWithFormat:@"%@,%@,%@", self.chatter.openid, self.chatter.userName, self.chatter.titleName];
    
    //    } else {
    //
    //        if ([self.chatter.openid compare:self.user.openid options:NSNumericSearch] == NSOrderedSame || [self.chatter.openid compare:self.user.openid options:NSNumericSearch] == NSOrderedDescending) {
    //
    //            self.chatUserData = [NSString stringWithFormat:@"%@@%@@%@@%@", self.chatterID, kMyChatId, self.user.openid, self.chatter.openid];
    //        } else {
    //            self.chatUserData = [NSString stringWithFormat:@"%@@%@@%@@%@", self.chatterID, kMyChatId, self.chatter.openid, self.user.openid];
    //        }
    //
    //    }
    
    
    //
    self.navigationItem.title = self.chatter.userName;
    
    //
    //    self.chatterID = @"sl_null13958";
    
    //self.view.backgroundColor = [UIColor colorWithHexString:@"#eeeeee" alpha:1.0];
    
    HZTableView *chatTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
    
    chatTableView.height = HZScreenH - kP(101) - SafeAreaTopHeight - SafeAreaBottomHeight;
    chatTableView.delegate = self;
    chatTableView.dataSource = self;
    chatTableView.tableFooterView = [UIView new];
    chatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    chatTableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:chatTableView];
    self.chatTableView = chatTableView;
    
    
    HZKeyboardView *keyboardView = [[HZKeyboardView alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, kP(101))];
    
    
    //    keyboardView.isInInquiryCtr = self.chatInInquiryCtr;
    keyboardView.y = self.chatTableView.height;
    // 设置通知改变键盘的位置
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];
    keyboardView.delegate = self;
    [self.view addSubview:keyboardView];
    [self.view bringSubviewToFront:keyboardView];
    self.keyboardView = keyboardView;
    
    //
    self.keyboardView.isLessThanChatTableViewCellMaxY = self.messageDB.DBCellMaxY < kP(600);
}

#pragma mark -- 相册访问页面设置
- (void)keyboardWillChangeFrameNotification:(NSNotification *)notification {
    CGRect keyboardF = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardF = keyboardF;
    
    if (keyboardF.origin.y < HZScreenH ) {// 键盘弹出
        
        //        if (self.messageDB.DBCellMaxY < kP(600)) {
        NSDictionary *userInfoDict = notification.userInfo;
        self.userInfoDict = userInfoDict;
        CGFloat animationDuration = [userInfoDict[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        UIViewAnimationCurve curve = [userInfoDict[UIKeyboardAnimationCurveUserInfoKey] integerValue];
        
        
        NSLog(@"----小于kP(600)-----");
        [UIView animateWithDuration:animationDuration delay:0 options:(curve << 16| UIViewAnimationOptionBeginFromCurrentState) animations:^{
            
            self.keyboardView.y = self.view.height - keyboardF.size.height -self.keyboardView.height;
            self.chatTableView.height = self.keyboardView.y;
            
        } completion:^(BOOL finished) {
            
        }];
        //        } else {
        //
        //            NSLog(@"----大于kP(600)-----");
        //            self.keyboardView.y = self.chatTableView.height;
        //            self.view.y = - keyboardF.size.height;
        //        }
    } else {// 键盘收回
        
        //        if (self.messageDB.DBCellMaxY < kP(600)) {
        
        self.chatTableView.height = HZScreenH - self.keyboardView.height - SafeAreaTopHeight;
        self.keyboardView.y = self.chatTableView.height;
        //        } else {
        //            self.view.y = 0;
        //        }
    }
    [self scrollowToBottom];
}

#pragma mark -- 播放音效
- (void)playSound {
    //1.获得音效文件的全路径
    NSString *receiveSoundPath = [[NSBundle mainBundle] pathForResource:@"receive_msg"
                                                                 ofType:@"caf"];
    NSURL *receiveSoundURL = [NSURL fileURLWithPath:receiveSoundPath];
    
    OSStatus receiveErr = AudioServicesCreateSystemSoundID((__bridge CFURLRef)receiveSoundURL,
                                                           &_receiveSound);
    if (receiveErr != kAudioServicesNoError) {
        NSLog(@"Could not load %@, error code: %d", receiveSoundURL, (int)receiveErr);
    }
    //1.获得音效文件的全路径
    NSString *sendSoundPath = [[NSBundle mainBundle] pathForResource:@"send_msg"
                                                              ofType:@"caf"];
    NSURL *sendSoundURL = [NSURL fileURLWithPath:sendSoundPath];
    
    OSStatus sendErr = AudioServicesCreateSystemSoundID((__bridge CFURLRef)sendSoundURL,
                                                        &_sendSound);
    if (sendErr != kAudioServicesNoError) {
        NSLog(@"Could not load %@, error code: %d", sendSoundURL, (int)sendErr);
    }
    
}

- (void)dealloc {
    AudioServicesDisposeSystemSoundID(_receiveSound);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messgeArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    NSDictionary *modelDict = self.messgeArr[indexPath.row];
    
    //本地图片 路径
    if (modelDict[@"imagePath"]) {
        NSString *path = modelDict[@"imagePath"];
        NSArray *arr = [path componentsSeparatedByString:@"Caches/"];
        NSString* rightPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:arr.lastObject];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:modelDict];
        dict[@"imagePath"] = rightPath;
        NSLog(@"--dddffggg-----------------:%@", rightPath);
        modelDict = dict;
    }
    
    NSString *cellID = [HZMessageCell cellIdentifierWithModel:modelDict];
    HZMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[HZMessageCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    //    NSLog(@"-----sdsdsdsdsdsdsdsdsdsds*****--111--%@", self.IMOrder);
    cell.chatMessageDict = modelDict;
    //    cell.IMOrder = self.IMOrder;
    //    cell.chatter = self.chatter;
    //    cell.user = self.user;
    cell.delegate = self;
    return cell;
    
    // :/var/mobile/Containers/Data/Application/BF81C440-1A92-4C00-8F38-BDA45FBA5088/Library/Caches/20170830102127833.jpg
    
    // :/var/mobile/Containers/Data/Application/BF81C440-1A92-4C00-8F38-BDA45FBA5088/Library/Caches/Caches---------------(null)
    //    static NSString *cellID = @"cell";
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    //    if (!cell) {
    //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    //    }
    //    cell.textLabel.text = @"sdsdsdsdsdds";
    //    return cell;
}


#pragma mark -- UITableViewDelegate
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [self.view endEditing:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *modelDict = self.messageArr[indexPath.row];
    //本地图片 路径
    if (modelDict[@"imagePath"]) {
        NSString *path = modelDict[@"imagePath"];
        NSArray *arr = [path componentsSeparatedByString:@"Caches/"];
        NSString* rightPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:arr.lastObject];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:modelDict];
        dict[@"imagePath"] = rightPath;
        NSLog(@"--dddffggg-----------------:%@", rightPath);
        modelDict = dict;
    }
    return [HZMessageCell cellHeightWithModel:modelDict];
}

#pragma mark -- HZKeyboardViewDelegate
#pragma mark -- 发送文本信息
- (void)sendTextMessage:(NSString *)textMessage{
    
    
    if ([self.chatterID isEqualToString:kMyChatId]) {
        [[UIApplication sharedApplication].keyWindow.rootViewController.view makeToast:@"请不要给自己发信息"
                                                                              duration:1.5f
                                                                              position:CSToastPositionCenter];
        return;
    }
    
    ECTextMessageBody *messageBody = [[ECTextMessageBody alloc] initWithText:textMessage];
    
    //-----------------------------------------
    
    NSString *friendChatId = self.chatterID;
    ECMessage *message = [[ECMessage alloc] initWithReceiver:friendChatId body:messageBody];
    
    //    NSLog(@"-----ddd----%@", self.IMOrder.localAccount);
    // 其本地时间
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval tmp = [date timeIntervalSince1970];
    message.timestamp = [NSString stringWithFormat:@"%lld", (long long)tmp];
    
    // ------------------userData-------------
    message.userData = self.sendUserData;
    
    //
    [[HZChatHelper sharedInstance] sendMessage:message withImagePath:nil];
    
    NSLog(@"---dd--s----to:%@---------from:%@", message.to, message.from);
    
    
    //
    NSDictionary *myDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
    NSString *newsCountStr = myDict[self.saveUserData];
    NSInteger newsCount = [newsCountStr integerValue];
    NSMutableDictionary *newsMutableDict = [NSMutableDictionary dictionaryWithDictionary:myDict];
    [newsMutableDict setValue:[NSString stringWithFormat:@"%ld", (long)newsCount] forKey:self.saveUserData];
    NSDictionary *myNewDict = [NSDictionary dictionaryWithDictionary:newsMutableDict];
    [[NSUserDefaults standardUserDefaults] setObject:myNewDict forKey:@"profileVCtrNews"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"message-set%@",myNewDict);
    //发送后 修改 tableView 高度
    self.chatTableView.height = HZScreenH - kP(101) - SafeAreaTopHeight -self.keyboardF.size.height;
    
}


#pragma mark -- 相册与相机

- (void)pickImageFromAlbum:(UIButton *)albumBtn {
    
    [self pushImagePickerController];
    
}


- (void)pickImageFromCamera:(UIButton *)cameraBtn {
    BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
    
    if (!isCamera) {
        [self setUpAlertViewWithTitle:@"没有摄像头"];
        return;
    }
    
    // 相机权限
    AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied) {
        void(^block)() = authorizationsSettingBlock();
        showMessage(@"您相机权限未打开,是否打开?", self, block);
    }
  
    self.imagePickerController = [[UIImagePickerController alloc] init];
    self.imagePickerController.delegate = self;
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    self.imagePickerController.allowsEditing = YES;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}


- (void)setUpAlertViewWithTitle:(NSString *)title {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -- UIImagePickerControllerDelegate
// 相机拍照发送
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    // 隐藏picker
    [picker dismissViewControllerAnimated:YES completion:nil];
    // 取出图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    // 发送图片
    [self sendSelectedImage:image];
}

// 相册获取发送
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    
    
    for (UIImage *image in photos) {
        [self sendSelectedImage:image];
    }
}


- (void)sendSelectedImage:(UIImage *)image {
    
    
    if ([self.chatterID isEqualToString:kMyChatId]) {
        [[UIApplication sharedApplication].keyWindow.rootViewController.view makeToast:@"请不能给自己发信息"
                                                                              duration:1.5f
                                                                              position:CSToastPositionCenter];
        return;
    }
    NSString *imagePath = [self saveToDocument:image];
    NSMutableArray *imagePathArr = [NSMutableArray array];
    [imagePathArr addObject:imagePath];
    self.imagePathArr = imagePathArr;
    NSMutableArray *messageArr;
    //
    for (NSString *imagePath in self.imagePathArr) {
        NSString *friendChatId = self.chatterID;
        ECImageMessageBody *mediaBody = [[ECImageMessageBody alloc] initWithFile:imagePath displayName:imagePath.lastPathComponent];
        ECMessage *message = [[ECMessage alloc] initWithReceiver:friendChatId body:mediaBody];
        message.userData = self.sendUserData;
        NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
        NSTimeInterval tmp =[date timeIntervalSince1970]*1000;
        message.timestamp = [NSString stringWithFormat:@"%lld", (long long)tmp];
        
        messageArr = [NSMutableArray array];
        [messageArr addObject:message];
    }
    
    
    
    //---------------------------上面执行完毕再执行下面的多线程-----------------------------
#warning 入库前设置本地时间，以本地时间排序和以本地时间戳获取本地数据库缓存数据
    for (NSInteger i = 0; i < messageArr.count; i++) {
        NSString *imagePath = self.imagePathArr[i];
        ECMessage *message = messageArr[i];
        [self sendMessage:message withLocalImagePath:imagePath];
    }
}


- (void)sendMessage:(ECMessage *)message withLocalImagePath:(NSString *)imagePath {
    
    //1. userData
    message.userData = self.sendUserData;
    
    
    //2.
    [[HZChatHelper sharedInstance] sendMessage:message withImagePath:imagePath];
    
    //
    NSDictionary *myDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
    NSString *newsCountStr = myDict[self.saveUserData];
    NSInteger newsCount = [newsCountStr integerValue];
    NSMutableDictionary *newsMutableDict = [NSMutableDictionary dictionaryWithDictionary:myDict];
    [newsMutableDict setValue:[NSString stringWithFormat:@"%ld", newsCount] forKey:self.saveUserData];
    NSDictionary *myNewDict = [NSDictionary dictionaryWithDictionary:newsMutableDict];
    [[NSUserDefaults standardUserDefaults] setObject:myNewDict forKey:@"profileVCtrNews"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"message-set2%@",myDict);
}

-(NSString*)saveToDocument:(UIImage*)image {
    
    //    NSLog(@"=========save=======%@", image);
    UIImage* fixImage = [self fixOrientation:image];
    
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSString* fileName = [NSString stringWithFormat:@"%@.jpg", [formater stringFromDate:[NSDate date]]];
    
    //    NSString* filePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:fileName];
    //
    //    //图片按0.5的质量压缩－》转换为NSData
    CGSize pressSize = CGSizeMake((DefaultPressImageHigth/fixImage.size.height) * fixImage.size.width, DefaultPressImageHigth);
    UIImage * pressImage = [CommonTools compressImage:fixImage withSize:pressSize];
    NSData *imageData = UIImageJPEGRepresentation(pressImage, 0.5);
    // [imageData writeToFile:filePath atomically:YES];
    //
    //    CGSize thumsize = CGSizeMake((kP(253)/fixImage.size.height) * fixImage.size.width, kP(253));
    //    UIImage * thumImage = [CommonTools compressImage:fixImage withSize:thumsize];
    //    NSData * photo = UIImageJPEGRepresentation(thumImage, 0.5);
    //    NSString * thumfilePath = [NSString stringWithFormat:@"%@.jpg_thum", filePath];
    //    [photo writeToFile:thumfilePath atomically:YES];
    
    // [[SDImageCache sharedImageCache] storeImage:fixImage forKey:fileName toDisk:YES];
    NSString *imageCache = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES).firstObject;
    NSString *imageCachePath = [NSString stringWithFormat:@"%@/default/com.hackemist.SDWebImageCache.default/%@",imageCache,fileName];
    [imageData writeToFile:imageCachePath atomically:YES];
    
    return imageCachePath;
    
}



- (UIImage *)fixOrientation:(UIImage *)aImage {
    // No-op if the orientation is already correct
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform     // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,CGImageGetBitsPerComponent(aImage.CGImage), 0,CGImageGetColorSpace(aImage.CGImage),CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
        default:              CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);              break;
    }       // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

#pragma mark -- useImage
- (UIImage *)useImage:(UIImage *)image {
    //实现等比例缩放
    CGFloat hfactor = image.size.width / HZScreenW;
    CGFloat vfactor = image.size.height / HZScreenH;
    CGFloat factor = fmax(hfactor, vfactor);
    //画布大小
    CGFloat newWith = image.size.width / factor;
    CGFloat newHeigth = image.size.height / factor;
    CGSize newSize = CGSizeMake(newWith, newHeigth);
    
    UIGraphicsBeginImageContext(newSize);
    
    //    image.drawInRect(CGRect(x: 0, y: 0, width: newWith, height: newHeigth))
    [image drawInRect:CGRectMake(0, 0, newWith, newHeigth)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //图像压缩
    NSData *newImageData = UIImageJPEGRepresentation(newImage, 0.5);
    return [UIImage imageWithData:newImageData];
}

#pragma mark -- 模型转字典保存到数据库
- (void)addToMesageDBWithMessage:(ECMessage *)message withLocalImagePath:(NSString *)imageLocalPath {
    
    
    
    //1. 设置时间间隔
    NSString *timeStr;
    if (message.messageBody.messageBodyType == MessageBodyType_Image) {
        if (message.timestamp) {
            if (message.timestamp.length > 3) {
                timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
            }
        }
        //        NSLog(@"=========save2=======%@", message);
    } else {
        
        
        if ([message.from isEqualToString:kMyChatId]) {// 本地发的
            
            if (message.timestamp) {
                if (message.timestamp.length > 3) {
                    timeStr = [message.timestamp stringByReplacingCharactersInRange:NSMakeRange(message.timestamp.length - 3, 3) withString:@""];
                }
            }
        } else {// 接收的
            timeStr = message.timestamp;
            
        }
        
    }
    self.isTimeShow = [timeStr integerValue] - self.lastTimeStamp <= 60;
    
    self.lastTimeStamp = [timeStr integerValue];
    
    //2. 设置orderId
    message.userData = self.saveUserData;
    //3. 保存
    NSDictionary *messageDict;
    if (message.messageBody.messageBodyType == MessageBodyType_Text) {
        
        
        if ([message.from isEqualToString:kMyChatId]) {// 本地发的
            ECTextMessageBody *textMsgBody = (ECTextMessageBody *)message.messageBody;
            
            messageDict = @{
                            @"from":message.from,
                            @"to":message.to,
                            @"messageId":message.messageId,
                            @"timestamp":message.timestamp,
                            @"messageBodyType":@(message.messageBody.messageBodyType),
                            @"text":textMsgBody.text,
                            @"userData":message.userData,
                            @"sessionId":message.sessionId,
                            @"isGroup":@(message.isGroup),
                            @"messageState":@(message.messageState),
                            @"isRead":@(message.isRead),
                            @"groupSenderName":@"暂无",
                            @"isTimeShow":@(self.isTimeShow)
                            };
            [self.messageDB addMessageDict:messageDict withUnReadCount:0];
            
        } else {// 接收的
            
            
            ECTextMessageBody *textMsgBody = (ECTextMessageBody *)message.messageBody;
            messageDict = @{
                            @"from":message.from,
                            @"to":message.to,
                            @"messageId":message.messageId,
                            @"timestamp":timeStr,
                            @"messageBodyType":@(message.messageBody.messageBodyType),
                            @"text":textMsgBody.text,
                            @"userData":message.userData,
                            @"sessionId":message.sessionId,
                            @"isGroup":@(message.isGroup),
                            @"messageState":@(message.messageState),
                            @"isRead":@(message.isRead),
                            @"groupSenderName":@"暂无",
                            @"isTimeShow":@(self.isTimeShow)
                            };
            [self.messageDB addMessageDict:messageDict withUnReadCount:0];
        }
        
        
    } else if (message.messageBody.messageBodyType == MessageBodyType_Image) {
        
        ECImageMessageBody *imageMsgBody = (ECImageMessageBody *)message.messageBody;
        
        if ([message.from isEqualToString:kMyChatId]) {// 本地发的
            messageDict = @{
                            @"from":message.from,
                            @"to":message.to,
                            @"messageId":message.messageId,
                            @"timestamp":timeStr,
                            @"messageBodyType":@(message.messageBody.messageBodyType),
                            @"thumbnailDownloadStatus":@(imageMsgBody.thumbnailDownloadStatus),
                            @"imagePath":imageLocalPath,
                            @"userData":message.userData,
                            @"sessionId":message.sessionId,
                            @"isGroup":@(message.isGroup),
                            @"messageState":@(message.messageState),
                            @"isRead":@(message.isRead),
                            @"groupSenderName":@"暂无",
                            @"isTimeShow":@(self.isTimeShow)
                            };
            
            NSLog(@"=========save5=======%@", messageDict[@"userData"]);
            [self.messageDB addMessageDict:messageDict withUnReadCount:0];
            
        } else {
            
            messageDict = @{
                            @"from":message.from,
                            @"to":message.to,
                            @"messageId":message.messageId,
                            @"timestamp":timeStr,
                            @"messageBodyType":@(message.messageBody.messageBodyType),
                            @"thumbnailDownloadStatus":@(imageMsgBody.thumbnailDownloadStatus),
                            @"remotePath":imageMsgBody.remotePath,
                            @"thumbnailRemotePath":imageMsgBody.thumbnailRemotePath,
                            @"userData":message.userData,
                            @"sessionId":message.sessionId,
                            @"isGroup":@(message.isGroup),
                            @"messageState":@(message.messageState),
                            @"isRead":@(message.isRead),
                            @"groupSenderName":@"暂无",
                            @"isTimeShow":@(self.isTimeShow)
                            };
            [self.messageDB addMessageDict:messageDict withUnReadCount:0];
            
        }
        
    }
    
    
    
    if ([message.from isEqualToString:kMyChatId]) {// 本地发的
        AudioServicesPlaySystemSound(_sendSound);// 播放音效
    } else {
        AudioServicesPlaySystemSound(_receiveSound);// 播放音效
    }
    
    [self.messageArr addObject:messageDict];
    // TO DO
    //  self.messageArr = [self.messageArr mutableCopy];
    [self.chatTableView reloadData];
    [self scrollowToBottom];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageComplete" object:nil];
    
    
    
    
    //--------------------------------获取当前订单的数组
    //    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    //        self.messageArr = [self.messageDB getMessageArrWithOrderId:self.saveUserData];
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //
    //            //            NSLog(@"-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==%@", self.messageArr);
    //            [self.chatTableView reloadData];
    //            [self scrollowToBottom];
    //        });
    //    });
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- 字典转模型获取到数组中
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [IQKeyboardManager sharedManager].enable = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    //    [[SDImageCache sharedImageCache] clearDisk];
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    
    // 删除缓存的元素
    NSDictionary *newsDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
    if (newsDict.allKeys.count ==0) return;
    NSString *value = newsDict[self.saveUserData];
    if (value.length == 0) return;
    
    NSMutableDictionary *myDict = [NSMutableDictionary dictionaryWithDictionary:newsDict];
    [myDict setValue:@"" forKey:self.saveUserData];
    
    // 再存元素
    [[NSUserDefaults standardUserDefaults] setObject:myDict forKey:@"profileVCtrNews"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"message-set3%@",myDict);
    
    
    
}


#pragma mark -- 滚到底部
- (void)scrollowToBottom {
    
    //        self.chatTableView.contentSize = CGSizeMake(HZScreenW, 0);
    //        NSIndexPath *bottomIndexPath = [NSIndexPath indexPathForRow:self.messageArr.count - 1 inSection:0];
    //        [self.chatTableView scrollToRowAtIndexPath:bottomIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    NSInteger s = [self.chatTableView numberOfSections];
    if (s<1) return;
    NSInteger r = [self.chatTableView numberOfRowsInSection:s-1];
    if (r<1) return;
    NSIndexPath *ip = [NSIndexPath indexPathForRow:r-1 inSection:s-1];
    [self.chatTableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}


#pragma mark - TZImagePickerController

- (void)pushImagePickerController {
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:5 columnNumber:4 delegate:self];
    imagePickerVc.selectedAssets = nil;
    imagePickerVc.allowTakePicture = NO; // 在内部显示拍照按钮
    imagePickerVc.allowPickingVideo = YES;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingOriginalPhoto = YES;
    imagePickerVc.sortAscendingByModificationDate = YES;// 4. 照片排列按修改时间升序
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if ([picker isKindOfClass:[UIImagePickerController class]]) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}



#pragma mark -- HZChatCellDelegate
- (void)messageCellWithMessageDict:(NSDictionary *)messageDict withImageView:(UIImageView *)imageView sender:(UIGestureRecognizer *)sender {
    
    
    NSLog(@"----dsdsdsdsdsdsds-----%@", messageDict);
    
    if ([imageView.image isEqual:[UIImage imageNamed:@"no_pic"]]) return;
    
    self.photoNeedImage = imageView.image;
    SDPhotoBrowser *photoBrowser = [SDPhotoBrowser new];
    photoBrowser.backgroundColor = [UIColor colorWithHexString:@"#333333"];
    photoBrowser.delegate = self;
    photoBrowser.currentImageIndex = 0;
    photoBrowser.imageCount = 1;
    photoBrowser.sourceImagesContainerView =imageView.superview;
    photoBrowser.showIndexLabel = NO;
    [photoBrowser show];
    
}

- (void)showAndHideStatusBarWithIsBig:(BOOL)isBig {
    //    self.isClicked = !self.isClicked;
    [self.parentViewController prefersStatusBarHidden];
    [self.parentViewController setNeedsStatusBarAppearanceUpdate];
}

#pragma mark photo-Delegate
- (UIImage *)photoBrowser:(SDPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return self.photoNeedImage;
}


@end

