//
//  HZMessageViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/18.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;
@interface HZMessageViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
@property (nonatomic, strong) HZUser *chatter;
@property (nonatomic, assign) BOOL isClicked;
@property (nonatomic,strong) NSString *chatterOpenId;
- (void)addToMesageDBWithMessage:(ECMessage *)message withLocalImagePath:(NSString *)imageLocalPath;
@end
