//
//  HZMessageCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/18.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HZMessageCellDelegate <NSObject>

- (void)messageCellWithMessageDict:(NSDictionary *)messageDict withImageView:(UIImageView *)imageView sender:(UIGestureRecognizer *)sender;

@end
@interface HZMessageCell : UITableViewCell
@property (nonatomic, strong) NSDictionary *chatMessageDict;
//** <#注释#>*/
//@property (nonatomic, strong) HZOrder *IMOrder;
//** <#注释#>*/
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, assign) BOOL isSender;
@property (nonatomic, weak) id<HZMessageCellDelegate>delegate;
+ (NSString *)cellIdentifierWithModel:(NSDictionary *)modelDict;

+ (CGFloat)cellHeightWithModel:(NSDictionary *)modelDict;
@end
