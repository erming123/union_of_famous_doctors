//
//  HZChatCell.m
//  SLIOP
//
//  Created by 季怀斌 on 16/8/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZMessageCell.h"
#import "UIImageView+WebCache.h"
#import <objc/runtime.h>
#import "HZOrder.h"
#import "HZUserTool.h"
#import "HZUser.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZMessageCell ()

@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIImageView *avatarImageView;

@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UILabel *myTextLabel;
@property (nonatomic, strong) UIImageView *picImageView;
@end

//const char KTimeIsShowKey;

NS_ASSUME_NONNULL_END
@implementation HZMessageCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        self.backgroundColor = [UIColor clearColor];
        //
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        [self setUpSubViews];
    }
    
    return self;
}


- (void)setUpSubViews {
    
    //1. 时间
    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.font = [UIFont systemFontOfSize:kP(24)];
   // timeLabel.backgroundColor = [UIColor lightGrayColor];
  //  timeLabel.layer.cornerRadius = kP(5);
  //  timeLabel.clipsToBounds = YES;
    timeLabel.textColor = [UIColor colorWithHexString:@"#C3CDD4"];
    [self addSubview:timeLabel];
    self.timeLabel = timeLabel;
    
    //2. 头像
    UIImageView *avatarImageView = [[UIImageView alloc] init];
    avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:avatarImageView];
    self.avatarImageView = avatarImageView;
    
    //3. 背景图片
    UIImageView *bgImageView = [[UIImageView alloc] init];
//        bgImageView.backgroundColor = [UIColor yellowColor];
    bgImageView.userInteractionEnabled = YES;
    [self addSubview:bgImageView];
    self.bgImageView = bgImageView;
    
    //4. 文本label
    UILabel *myTextLabel = [[UILabel alloc] init];
    //    myTextLabel.backgroundColor = [UIColor redColor];
    myTextLabel.font = [UIFont systemFontOfSize:kP(30)];
    myTextLabel.numberOfLines = 0;
    [self addSubview:myTextLabel];
    self.myTextLabel = myTextLabel;
    
    //5. 图片imageView
    UIImageView *picImageView = [[UIImageView alloc] init];
    picImageView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:picImageView];
    self.picImageView = picImageView;
    
}


- (void)setChatMessageDict:(NSDictionary *)chatMessageDict {
    
    
    //    self.isTimeShow = chatMessageDict[@"isTimeShow"];
    
    self.timeLabel.hidden = [chatMessageDict[@"isTimeShow"] intValue];
    //    NSLog(@"------------hidden-------------------%d", self.timeLabel.hidden);
    _chatMessageDict = chatMessageDict;
    _nickName = chatMessageDict[@"from"];// 发送方
    _isSender = [chatMessageDict[@"from"] isEqualToString:kMyChatId] ? YES : NO; // 是否是发送的消息
    
    
    self.bgImageView.image = _isSender ? [UIImage setAdjustImageWithName:@"bubble_send"] : [UIImage setAdjustImageWithName:@"bubble_receive"];
    NSInteger index = [chatMessageDict[@"messageBodyType"] integerValue];
    
    switch (index) {
            
        case MessageBodyType_Text:{
            
            
            self.myTextLabel.text = chatMessageDict[@"text"];
            //            NSLog(@"-=-=-================================------------%@", self.myTextLabel.text);
            CGSize myTextContentSize = [self.myTextLabel.text getTextSizeWithMaxWidth:HZScreenW * 0.6 textFontSize:kP(31)];
            self.myTextLabel.height = myTextContentSize.height;
            self.myTextLabel.width = myTextContentSize.width;
            //            NSLog(@"-=-=-================================------------%lf", self.myTextLabel.width);
            
            self.bgImageView.height = self.myTextLabel.height + kP(50);
            self.bgImageView.width = self.myTextLabel.width + kP(50)>kP(112)?self.myTextLabel.width + kP(50):kP(112);
        }
            break;
        case MessageBodyType_Image: {

//            self.picImageView.size = CGSizeMake(kP(364), kP(320));
            self.picImageView.layer.cornerRadius = kP(40);
            self.picImageView.clipsToBounds = YES;
            if (_isSender) {

                UIImage *sendImg = [UIImage imageWithContentsOfFile:chatMessageDict[@"imagePath"]];
                
                if (sendImg) {
                    self.picImageView.image = sendImg;
                    
                    CGFloat picImageViewH = kP(320);
                    CGFloat picImageViewW = (sendImg.size.width / sendImg.size.height) * picImageViewH;
                    
                    
                    if (picImageViewW >= HZScreenW - 2 * kP(108)) {
                        picImageViewW = HZScreenW - 2 * kP(108);
                        
                        self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
                    } else {
                        self.picImageView.contentMode = UIViewContentModeScaleAspectFit;
                    }
                    
                    self.picImageView.size = CGSizeMake(picImageViewW, picImageViewH);
                } else {
                    self.picImageView.size = CGSizeMake(kP(132), kP(126));
                    self.picImageView.image = [UIImage imageNamed:@"no_pic"];
                }

            } else {

                NSString *urlStr = chatMessageDict[@"remotePath"];
                
                UIImage *receiveImg = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:urlStr];
                if (receiveImg) {
                    self.picImageView.image = receiveImg;
                    CGFloat picImageViewH = kP(320);
                    CGFloat picImageViewW = (receiveImg.size.width / receiveImg.size.height) * picImageViewH;
                    if (picImageViewW >= HZScreenW - 2 * kP(108)) {
                        picImageViewW = HZScreenW - 2 * kP(108);
                        
                        self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
                    } else {
                        self.picImageView.contentMode = UIViewContentModeScaleAspectFit;
                    }
                    self.picImageView.size = CGSizeMake(picImageViewW, picImageViewH);
                } else {
                    NSURL *url = [NSURL URLWithString:urlStr];
                    UIImage *receiveImg = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                    [[SDImageCache sharedImageCache] storeImage:receiveImg forKey:urlStr toDisk:YES];
                    
                    //
                    CGFloat picImageViewH = kP(320);
                    CGFloat picImageViewW = (receiveImg.size.width / receiveImg.size.height) * picImageViewH;
                    
                    if (picImageViewW >= HZScreenW - 2 * kP(108)) {
                        picImageViewW = HZScreenW - 2 * kP(108);
                        
                        self.picImageView.contentMode = UIViewContentModeScaleAspectFill;
                    } else {
                        self.picImageView.contentMode = UIViewContentModeScaleAspectFit;
                    }
                    
                    self.picImageView.image = receiveImg;
                    self.picImageView.size = CGSizeMake(picImageViewW, picImageViewH);
                }

                
            }
            
        }
            
            break;
    }
    
//    self.bgImageView.layer.cornerRadius = self.bgImageView.height * 0.5;
//    self.bgImageView.clipsToBounds = YES;
    [self layoutSubviews:_isSender chatMessageDict:chatMessageDict chatMessageBodyType:index];
    
}



- (void)layoutSubviews:(BOOL)isSender chatMessageDict:(NSDictionary *)chatMessageDict chatMessageBodyType:(NSInteger)chatMessageBodyType{

   
    
    NSString *openidStr = chatMessageDict[@"userData"];
    NSArray *openIdArr = [openidStr componentsSeparatedByString:@","];
    NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    
    NSString *userOpenId = userDict[@"openid"];
    NSString *chatterOpenId = openIdArr[0];

    
    // 头像
    if (_isSender) {
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:userOpenId];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (imageData) {// 有
                    
                    UIImage *image = [UIImage imageWithData:imageData];
                    self.avatarImageView.image = image;
                    
                } else {// 没有
                    self.avatarImageView.image =  [UIImage imageNamed:@"expertPlaceImg"];
                }

            });
        });
        
        
        
//        NSLog(@"6666------------:%@", imageData);
        
        
           } else {
               
               
        
        UIImage *img = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:chatterOpenId];
        
        if (img) {
            self.avatarImageView.image = img;
        } else {
            self.avatarImageView.image = [UIImage imageNamed:@"Outpatient"];
            //
            [HZUserTool getAvatarWithAvatarOpenId:chatterOpenId success:^(UIImage *image) {
                if (image) {
                    
                    self.avatarImageView.image = image;
                    [[SDImageCache sharedImageCache] storeImage:image forKey:chatterOpenId];
                } else {
                    self.avatarImageView.image = [UIImage imageNamed:@"Outpatient"];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"---------%@", error);
            }];
            
        }
        
    }

    
    //
    // 时间
    NSString *timeStr = [self getDateDisplayString: [chatMessageDict[@"timestamp"] integerValue] * 1000];
    self.timeLabel.text = timeStr;
    [self.timeLabel sizeToFit];
    self.timeLabel.x = HZScreenW * 0.5 - self.timeLabel.width * 0.5;
    self.timeLabel.y = kP(10);
    
    //
    self.avatarImageView.width = self.avatarImageView.height = kP(80);
    self.avatarImageView.x =  _isSender ? HZScreenW - self.avatarImageView.width - kP(28) : kP(28);
    self.avatarImageView.y = [chatMessageDict[@"isTimeShow"] intValue] ?  kP(20) : kP(50);
    self.avatarImageView.layer.borderWidth = kP(1);
    self.avatarImageView.layer.borderColor = [UIColor colorWithHexString:@"#D5D5D5"].CGColor;
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.width * 0.5;
    self.avatarImageView.clipsToBounds = YES;
    
    NSInteger index = [chatMessageDict[@"messageBodyType"] integerValue];
    switch (index) {
        case MessageBodyType_Text: {
            
            self.myTextLabel.y = self.avatarImageView.y + kP(22);
            self.myTextLabel.x = _isSender ? self.avatarImageView.x - self.bgImageView.width: self.avatarImageView.right + kP(38);
            self.myTextLabel.textColor = _isSender ?  [UIColor whiteColor] : [UIColor blackColor];
            self.bgImageView.x = _isSender ? self.avatarImageView.x - self.bgImageView.width - kP(30): self.avatarImageView.right + kP(30);
            self.bgImageView.y = self.avatarImageView.y;
            
            self.avatarImageView.y = self.bgImageView.y + self.bgImageView.height - self.avatarImageView.height - kP(10);
            self.myTextLabel.centerX = self.bgImageView.centerX;
        }
            
            break;
            
        case MessageBodyType_Image: {
            
            self.picImageView.y = self.avatarImageView.y + kP(10);
            self.picImageView.x = _isSender ? self.avatarImageView.x - kP(30) - self.picImageView.width: self.avatarImageView.right + kP(30);
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToBig:)];
            self.picImageView.userInteractionEnabled = YES;
            [self.picImageView addGestureRecognizer:tap];
            
            self.avatarImageView.y = self.bgImageView.y;
            
        }
            
            break;
    }
    
    
    
}


#pragma -----

+ (NSString *)cellIdentifierWithModel:(NSDictionary *)modelDict{
    NSString *cellIdentifier = nil;
    NSInteger index = [modelDict[@"messageBodyType"] integerValue];
    
    if ([modelDict[@"from"] isEqualToString:kMyChatId]) {
        
        
        switch (index) {
            case MessageBodyType_Text:
                cellIdentifier = @"HZLeftMessageCellIdentifierSendText";
                break;
            case MessageBodyType_Image:
                cellIdentifier = @"HZLeftMessageCellIdentifierSendImage";
                break;
                
            default:
                break;
        }
    } else {
        switch (index) {
            case MessageBodyType_Text:
                cellIdentifier = @"HZLeftMessageCellIdentifierRecvText";
                break;
            case MessageBodyType_Image:
                cellIdentifier = @"HZLeftMessageCellIdentifierRecvImage";
                break;
        }
    }
    
    return cellIdentifier;
}



+ (CGFloat)cellHeightWithModel:(NSDictionary *)modelDict {
     BOOL isSend = [modelDict[@"from"] isEqualToString:kMyChatId];
    CGFloat rowHeight = 0.0;
    NSInteger type = [modelDict[@"messageBodyType"] integerValue];
    if (type == MessageBodyType_Text) {
        
        
        CGFloat textHeight = [modelDict[@"text"] getTextHeightWithMaxWidth:HZScreenW * 0.45 textFontSize:kP(30)];
        
        rowHeight = textHeight + 2 * kP(60) > kP(180) ? textHeight + 2 * kP(60) :kP(180);
        
        
        
        if (textHeight + 2 * kP(60) > kP(180)) {
            rowHeight = rowHeight - kP(15);
        } else {
            rowHeight = rowHeight - kP(37);
        }
        
        if ([modelDict[@"isTimeShow"] intValue]) {
            rowHeight = rowHeight - kP(30);
        }
        
    } else if (type ==  MessageBodyType_Image){
        
        if (isSend) {
            UIImage *sendImg = [UIImage imageWithContentsOfFile:modelDict[@"imagePath"]];
            NSLog(@"--sss--:%@", modelDict[@"imagePath"]);
            if (sendImg) {
                rowHeight = kP(500);
            } else {
                rowHeight = kP(300);
            }
        } else {
                rowHeight = kP(500);
        }
        
        
        
        rowHeight = rowHeight - kP(57);
        if ([modelDict[@"isTimeShow"] intValue]) {
            rowHeight = rowHeight - kP(30);
        }
    }
    
    
    
    
    return rowHeight;
}



//时间显示内容
-(NSString *)getDateDisplayString:(long long) miliSeconds{
    
    NSTimeInterval tempMilli = miliSeconds;
    NSTimeInterval seconds = tempMilli/1000.0;
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:seconds];
    
    NSCalendar *calendar = [ NSCalendar currentCalendar ];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear ;
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[ NSDate date ]];
    NSDateComponents *myCmps = [calendar components:unit fromDate:myDate];
    
    NSDateFormatter *dateFmt = [[NSDateFormatter alloc] init];
    if (nowCmps.year != myCmps.year) {
        dateFmt.dateFormat = @"yyyy-MM-dd HH:mm";
    } else {
        if (nowCmps.day==myCmps.day) {
            dateFmt.dateFormat = @"aa KK:mm";
        } else if ((nowCmps.day-myCmps.day)==1) {
            dateFmt.dateFormat = @"昨天 HH:mm";
        } else {
            dateFmt.dateFormat = @"MM-dd HH:mm";
        }
    }
    return [dateFmt stringFromDate:myDate];
}

- (void)tapToBig:(UIGestureRecognizer *)sender {
    
//    UIImage *sendImg = [UIImage imageWithContentsOfFile:self.chatMessageDict[@"imagePath"]];
  //  if (!sendImg) return;
    NSInteger index = [self.chatMessageDict[@"messageBodyType"] integerValue];
    //    self.picImageView.tag = 10000;
    if (index == MessageBodyType_Image) {
        //        EMImageMessageBody *imageBody = body;
        // 显示大图片
        if (self.delegate && [self.delegate respondsToSelector:@selector(messageCellWithMessageDict:withImageView:sender:)]) {
            [self.delegate messageCellWithMessageDict:self.chatMessageDict withImageView:self.picImageView sender:sender];
        }
    }
    
}

@end
