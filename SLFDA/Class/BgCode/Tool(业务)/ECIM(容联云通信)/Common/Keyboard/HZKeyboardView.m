//
//  HZKeyboardView.m
//  SLIOP
//
//  Created by 季怀斌 on 16/8/23.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZKeyboardView.h"
#import <objc/message.h>

NS_ASSUME_NONNULL_BEGIN
@interface HZKeyboardView ()<UITextViewDelegate>
@property (nonatomic, weak) UIView *topLine;
@property (nonatomic, weak) UITextView *textView;
@property (nonatomic, weak) UILabel *placeHolderLabel;
@property (nonatomic, weak) UIButton *albumBtn;
@property (nonatomic, weak) UIButton *cameraBtn;

//
@property (nonatomic, assign) BOOL textViewScrollEnabled;
@property (nonatomic, assign) CGFloat oldTextHeight;
@end

//
static CGFloat maxHeight = 100,constTextViewY, constAlbumBtnY, keyboardHeight, timelyTextHeight;
NS_ASSUME_NONNULL_END
@implementation HZKeyboardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUpSubViews];
        self.backgroundColor = [UIColor colorWithHexString:@"f4f4f6" alpha:1.0];
        //
        //          self.backgroundColor = [UIColor greenColor];
        
    }
    return self;
}


- (void)setUpSubViews {
    
    
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, kP(2))];
    topLine.backgroundColor = [UIColor colorWithHexString:@"d6d6d8" alpha:1.0];
    //    topLine.backgroundColor = [UIColor redColor];

    topLine.layer.shadowColor = [UIColor grayColor].CGColor;
    topLine.layer.shadowOffset = CGSizeMake(0, - 1);
    topLine.layer.shadowOpacity = 0.1;
    topLine.layer.shadowRadius = 1;
    
    //
    [self addSubview:topLine];
    self.topLine = topLine;
    
//    self.layer.shadowColor = [UIColor colorWithHexString:@"#E4E9EC"].CGColor;
//    self.layer.contentsScale = [UIScreen mainScreen].scale;
//    self.layer.shadowOffset = CGSizeMake(0,-1);
//    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
//    self.layer.shadowOpacity = 1.0;
//    self.layer.shouldRasterize = YES;
//    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    UITextView *textView = [[UITextView alloc] init];
    textView.width = HZScreenW - kP(208);
    textView.height = kP(70);
    textView.x = kP(22);
    textView.y = kP(99) * 0.5 - textView.height * 0.5;
    constTextViewY = textView.y;
    textView.font = [UIFont systemFontOfSize:kP(34)];
    textView.returnKeyType = UIReturnKeySend;
   // textView.layer.borderWidth = kP(1);
    //textView.layer.borderColor = [UIColor colorWithHexString:@"dddddd" alpha:1.0].CGColor;
   // textView.layer.cornerRadius = kP(10);
    textView.inputAccessoryView = [[UIView alloc] init];
    textView.delegate = self;
    textView.backgroundColor = [UIColor clearColor];
    [self addSubview:textView];
    
    // _placeholderLabel
    UILabel *placeHolderLabel = [[UILabel alloc] init];
    placeHolderLabel.text = @"请输入内容...";
    placeHolderLabel.numberOfLines = 0;
    placeHolderLabel.textColor = [UIColor lightGrayColor];
    [placeHolderLabel sizeToFit];
    placeHolderLabel.y = textView.height * 0.5 - placeHolderLabel.height * 0.5;
    placeHolderLabel.x = 5.0;
    placeHolderLabel.font = [UIFont systemFontOfSize:kP(34)];
    placeHolderLabel.textColor = [UIColor colorWithHexString:@"#C3CDD4"];
    [textView addSubview:placeHolderLabel];
    self.placeHolderLabel = placeHolderLabel;
//    if (kCurrentSystemVersion >= 9.0) {
//        [textView setValue:placeHolderLabel forKey:@"_placeholderLabel"];
//    }
//    ((void (*) (id , SEL, id)) (void *)objc_msgSend) (textView, sel_registerName("setText:"), @"请输入内容...");

    
    

    self.textView = textView;
    
    UIButton *cameraBtn = [[UIButton alloc] init];
    cameraBtn.width = kP(60);
    cameraBtn.height = kP(60);
    cameraBtn.x = textView.right + kP(22);
    cameraBtn.y = textView.centerY - cameraBtn.height * 0.5;
    constAlbumBtnY = cameraBtn.y;
    [cameraBtn setImage:[UIImage imageWithOriginalName:@"camera"] forState:UIControlStateNormal];
    [cameraBtn addTarget:self action:@selector(cameraBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cameraBtn];
    self.cameraBtn = cameraBtn;
    
    //
    UIButton *albumBtn = [[UIButton alloc] init];
    albumBtn.width = kP(60);
    albumBtn.height = kP(60);
    albumBtn.x = self.cameraBtn.right + kP(30);
    albumBtn.y = self.cameraBtn.y;
    [albumBtn setImage:[UIImage imageWithOriginalName:@"album"] forState:UIControlStateNormal];
    [albumBtn addTarget:self action:@selector(albumBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:albumBtn];
    self.albumBtn = albumBtn;
    
    //
    self.backgroundColor = [UIColor colorWithHexString:@"#FAFBFC"];
}

#pragma mark -- 点击输入框
- (void)textViewDidBeginEditing:(UITextView *)textView{
    self.oldTextHeight = textView.height;
    //    [self scrollRangeToVisibleWithTextView:textView];
    
    NSLog(@"fgdfgdfgdfgdg");
}

#pragma mark -- 文本内容改变
- (void)textViewDidChange:(UITextView *)textView{
    
    
    NSLog(@"---------发送---------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
    // 计算文本高度
    [self countTextHeightWithTextView:textView];
    
    // 发送文本
    [self sendTextMessageWithTextView:textView withBlock:^(UITextView *textView) {
        
        // 再次计算文本高度
        [self countTextHeightWithTextView:textView];
    }];
    
//    if (kCurrentSystemVersion < 9.0) {
//
//    }
    self.placeHolderLabel.hidden = textView.text.length;
    
}

#pragma mark -- // 计算文本高度
- (void)countTextHeightWithTextView:(UITextView *)textView {
    
    CGFloat textH;

    CGSize constraintSize = CGSizeMake(textView.width, MAXFLOAT);
    textH = [textView sizeThatFits:constraintSize].height;
    
    if (textH  >= textView.height) { //计算的高度大于等于空间的高度的时候：40
        timelyTextHeight = textH;
        
        if (textH > maxHeight){// 如果计算的文本高度大于等于最大值80
            
            textView.scrollEnabled = YES;   // 允许滚动
            textH = maxHeight;// 文本高度超过最大高度，输入框的高度／计算的文本高度都不再变化。
            
        }else { // 如果计算的文本高度小于等于最大值80
            textView.scrollEnabled = NO;    // 不允许滚动
        }
        
        // 输入框的高度
        textView.height = textH; // 再变的更高的时候，这里也是不再变化的。不过是可以滚动罢了。
        
    } else {
        
  
        textView.scrollEnabled = NO;// 不允许滚动
        // 输入框的高度，当在第一行的时候输入框高度不发生变化。
        if (textView.height != kP(70)) {
            textView.height = textH;
        }
        
        
        
    }
    
    // 设置底图的高度和y
    [UIView animateWithDuration:0.15 animations:^{
        
        CGFloat oldHeight = self.height;
        self.height = textView.bottom + constTextViewY;
        CGFloat delta = self.height - oldHeight;
        self.y -= delta;
        
//        NSLog(@"---------------------------self.y:000========%lf", self.y);
        
        
//        
//        NSLog(@"----------------BOOL-------------%d", self.isLessThanChatTableViewCellMaxY);
//        
//            NSLog(@"----------------BOOL-------------%d", self.isLessThanChatTableViewCellMaxY);
//            NSLog(@"---------------------------self.y:111========%lf", self.y);
//            if (self.isLessThanChatTableViewCellMaxY) { // < kp(600)
        
                //------------此处有雷，小心设置--------
                
//                NSString *str = [NSString deviceVersion];
//                
////                NSLog(@"-----*******----%@", str);
//                if ([str isEqualToString:@"iPhone 5S"] || [str isEqualToString:@"iPhone 5"]) {
//                    self.y = HZScreenH - (keyboardHeight + self.height) - kP(590);
//                    NSLog(@"---------5");
//                } else {
//                    self.y = HZScreenH - (keyboardHeight + self.height) - kP(578);
//                    NSLog(@"6<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
//                }
        
                 
        
                
//                 NSLog(@"---------------------------self.y:111========%lf", self.y);
//            } else {
//                self.y = HZScreenH - (keyboardHeight + self.height);
//                
////                NSLog(@"eeeeeeeeee11111111102-----self.Hieght === %lf", self.height);
////                
////                NSLog(@"---------------------------self.y:222========%lf", self.y);
//                NSLog(@"6>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//            }
//        
        
        
        
        self.albumBtn.y = self.height -  constAlbumBtnY - self.albumBtn.height;
        self.cameraBtn.y = self.albumBtn.y;
        self.topLine.y = 0;
    }];
    
    // 保存编辑时的输入框高度和是否可以滚动
    self.oldTextHeight = textView.height;
    self.textViewScrollEnabled = textView.scrollEnabled;
    
    //    NSLog(@"--------------------textView.height------------------%lf", textView.height);
    //    NSLog(@"--------------------可否滚动------------------%d", textView.scrollEnabled);
}





#pragma mark -- 发送文本信息
- (void)sendTextMessageWithTextView:(UITextView *)textView withBlock:(void(^)(UITextView *textView))block {
    
    NSString *str = [textView.text componentsSeparatedByString:@"\n"][0];
    
    if (![str isEqualToString:@""] && [textView.text hasSuffix:@"\n"]) {
        
        NSMutableString *mString = [NSMutableString stringWithString:textView.text];
        NSString *tempStr = [mString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        textView.text = tempStr;
        // 调用block, 再次计算文本高度
        if (block) {
            block(textView);
        }
        
        // 发送的操作
        NSLog(@"文本发送");
        if (self.delegate && [self.delegate respondsToSelector:@selector(sendTextMessage:)]) {
            [self.delegate sendTextMessage:textView.text];
        }
        // 清空文本，变为单行
        textView.text = @"";
        [self countTextHeightWithTextView:textView];

        
    }
    
//    NSLog(@"-=-=-=-=-=-=-=-=-=-=-= %lf", self.y);
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    [self scrollRangeToVisibleWithTextView:textView];
}

- (void)scrollRangeToVisibleWithTextView:(UITextView *)textView {
    textView.showsVerticalScrollIndicator = YES;
    
    textView.contentSize = CGSizeMake(0,  timelyTextHeight);
    
    //    NSLog(@"--------------------可视------------------%lf", timelyTextHeight);
    
    textView.layoutManager.allowsNonContiguousLayout = NO;
    [textView scrollRangeToVisible:NSMakeRange(textView.text.length, 1)];
    //    NSLog(@"--------------------可视------------------%ld", textView.text.length);
}


//设置textView的placeholder
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //[text isEqualToString:@""] 表示输入的是退格键
    if (![text isEqualToString:@""])
    {
        self.placeHolderLabel.hidden = YES;
    }
    
    //range.location == 0 && range.length == 1 表示输入的是第一个字符
    if ([text isEqualToString:@""] && range.location == 0 && range.length == 1)
        
    {
        self.placeHolderLabel.hidden = NO;
    }
    return YES;
    
}
#pragma mark -- 弹出相册与相机

- (void)albumBtnClick:(UIButton *)albumBtn {
    if (self.delegate && [self.delegate respondsToSelector:@selector(pickImageFromAlbum:)]) {
        [self.delegate pickImageFromAlbum:albumBtn];
    }
}

- (void)cameraBtnClick:(UIButton *)cameraBtn {
    if (self.delegate && [self.delegate respondsToSelector:@selector(pickImageFromCamera:)]) {
        [self.delegate pickImageFromCamera:cameraBtn];
    }
}


@end
