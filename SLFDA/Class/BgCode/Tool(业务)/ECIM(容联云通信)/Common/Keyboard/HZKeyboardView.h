//
//  HZKeyboardView.h
//  SLIOP
//
//  Created by 季怀斌 on 16/8/23.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZKeyboardViewDelegate <NSObject>

- (void)pickImageFromAlbum:(UIButton *)albumBtn;
- (void)pickImageFromCamera:(UIButton *)cameraBtn;
- (void)sendTextMessage:(NSString *)textMessage;
- (void)chageTableViewOriginYWithNotification:(NSNotification*)notification;
@end
@interface HZKeyboardView : UIView
@property (nonatomic, weak) id<HZKeyboardViewDelegate>delegate;

//@property (nonatomic, assign) BOOL isInInquiryCtr;
@property (nonatomic, assign) BOOL isLessThanChatTableViewCellMaxY;
@end
