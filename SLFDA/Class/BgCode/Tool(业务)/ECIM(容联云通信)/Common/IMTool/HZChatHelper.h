//
//  HZChatHelper.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/12/18.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECDeviceHeaders.h" // 项目header
#import "AppDelegate.h"

#define KNOTIFICATION_onMesssageReceive    @"KNOTIFICATION_onMesssageReceive"
#define KNOTIFICATION_onMesssageOffLineReceive    @"NOTIFICATION_onMesssageOffLineReceive"
#define KNOTIFICATION_onMesssageSend    @"KNOTIFICATION_onMesssageSend"
//#define KNOTIFICATION_onConnected @"KNOTIFICATION_onConnected"
extern NSString *const messageReadCount;
extern NSString *const messageUnReadCount;

@interface HZChatHelper : NSObject<ECDeviceDelegate>
+(HZChatHelper*)sharedInstance;
- (void)sendMessage:(ECMessage *)message withImagePath:(NSString *)imagePath;
@property (nonatomic, copy) NSMutableArray *unReadNewsArr;
@property (nonatomic, assign) BOOL hasUnReadMsg;

/**图片下载队列*/
@property (nonatomic,strong) NSOperationQueue *downLoadImgQueue;
@end
