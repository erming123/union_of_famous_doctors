//
//  HZMessageDB.m
//  SLIOP
//
//  Created by 季怀斌 on 16/9/1.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZMessageDB.h"
#import "FMDB.h"
//#import "MJExtension.h"
@interface HZMessageDB ()
@property (nonatomic, strong) FMDatabaseQueue *dbQueue;

@property (nonatomic, strong) NSMutableArray *unReadMsgArr;
@property (nonatomic, strong) NSMutableArray *magUserDataArr;

@property (nonatomic, strong) NSDictionary *myMessageDict;

@property (nonatomic,strong) NSMutableArray *messageReadArray;

@property (nonatomic,strong) NSMutableArray *pushMessageReadArray;

@end
@implementation HZMessageDB

static HZMessageDB *messageDB = nil;
+ (instancetype)initMessageDB {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        messageDB = [[HZMessageDB alloc] init];
    });
    
    return messageDB;
}

-(NSMutableArray *)pushMessageReadArray{
    if (!_pushMessageReadArray) {
        
        _pushMessageReadArray = [NSMutableArray array];
    }
    return _pushMessageReadArray;
}


-(NSMutableArray *)messageReadArray{
    if (!_messageReadArray) {
        _messageReadArray = [NSMutableArray array];
    }
    return _messageReadArray;
}
-(NSMutableArray *)messageArr{
    if (!_messageArr) {
        _messageArr = [NSMutableArray array];
    }
    return _messageArr;
}

-(NSMutableDictionary *)needUpdateReadStateArray{
    if (!_needUpdateReadStateArray) {
        _needUpdateReadStateArray = [NSMutableDictionary dictionary];
    }
    return _needUpdateReadStateArray;
}

-(NSMutableArray *)PushMessageArr{
    if (!_PushMessageArr) {
        _PushMessageArr = [NSMutableArray array];
    }
    return _PushMessageArr;
}

- (void)setUpDB {
    
    //
//    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
    NSString *firstVersion = @"2.1.0";

    int result =  [kCurrentBundleVersion compare:firstVersion options:NSCaseInsensitiveSearch];
    if (result < 0) { // 当前 < 2.1.0
        [self deleteAllMessage];
    }
    //
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dbQueuePath = [docPath stringByAppendingPathComponent:@"message.sqlite"];
    NSLog(@"dbQueuePath ===setUpDB=== %@", dbQueuePath);
    
    self.GlobSerieaQueue =dispatch_queue_create("customFIFOqueue",NULL);
    
    //2. 创建安全队列
    FMDatabaseQueue *dbQueue = [FMDatabaseQueue databaseQueueWithPath:dbQueuePath];
    self.dbQueue = dbQueue;
    
    //3. 创建表格
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        // 打开数据库
        if (![db open]) return;
        
        // 创建表格
        NSString *creatTableStr = @"CREATE TABLE IF NOT EXISTS t_message (id integer PRIMARY KEY ,messageId text NOT NULL UNIQUE,messageDict text NOT NULL, msgTo text NOT NULL, unReadCount text NOT NULL, msgTime text NOT NULL, orderId text NOT NULL);";
        BOOL result = [db executeUpdate:creatTableStr];
        
        // 判断成功与否
        if (result) {
            NSLog(@"创建表格成功");
        } else {
            NSLog(@"创建表格失败");
        }
        
        // 关闭数据库
        [db close];
        
        
    }];
    
    
    
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUpDB];
    }
    return self;
}

#pragma mark -- 添加数据
- (void)addMessageDict:(NSDictionary *)messageDict withUnReadCount:(int)unReadCount {
    
    NSString *unReadCountStr = [NSString stringWithFormat:@"%d", unReadCount];
        
        [self.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            
            // 打开数据库
            if (![db open]) return;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:messageDict options:NSJSONWritingPrettyPrinted error:nil];
            
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            // 插入数据
            BOOL result = [db executeUpdateWithFormat:@"INSERT OR IGNORE INTO t_message (messageId,messageDict, msgTo, unReadCount, msgTime, orderId) VALUES (%@,%@,%@,%@,%@,%@)",messageDict[@"messageId"],jsonString, messageDict[@"to"],unReadCountStr,messageDict[@"timestamp"],messageDict[@"userData"]];
            NSLog(@"插入数据线程是=%@",[NSThread currentThread]);
            
            // 判断成功与否
            if (result) {
                NSLog(@"插入数据成功");
            } else {
                NSLog(@"插入数据失败");
            }
            // 关闭数据库
            [db close];
        }];
}



- (NSMutableArray *)getMessageArrWithOrderId:(NSString *)orderId {
    
    //
    [self.IMmessageArr removeAllObjects];
    [self.msgImgPathArr removeAllObjects];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        //1. 打开数据库
        if (![db open]) return;
        
        //2. 查询数据
        FMResultSet *resultSet = [db executeQuery:@"SELECT *FROM t_message"];
        //        NSMutableArray *messageArr = ;
        if (self.IMmessageArr) {
            [self.IMmessageArr removeAllObjects];
        } else {
            self.IMmessageArr = [NSMutableArray array];
        }
        
        
        //3. 遍历结果, 如果下一行有数据， 返回YES
        while ([resultSet next]) {
            
            NSString *dictStr = [resultSet objectForColumnName:@"messageDict"];
            NSData *data =[dictStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            if (messageDict == nil) return;
            
            NSLog(@"-----xcv123-q-----:%@--------:%@----------------%@", messageDict[@"userData"], orderId, messageDict);
            
            if ([messageDict[@"userData"] isEqualToString:orderId]) {
                [self.IMmessageArr addObject:messageDict];
                
                NSString* imgPath = messageDict[@"imagePath"];
                NSArray *arr = [imgPath componentsSeparatedByString:@"Caches/"];
                imgPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:arr.lastObject];
                [self.msgImgPathArr addObject:imgPath];
            }
        }
        //4. 关闭数据库
        [db close];
    }];
    
    return self.IMmessageArr;
}

- (NSMutableArray *)getPushMessageArrWithOrderId:(NSString *)orderId {
    [self.PushMessageArr removeAllObjects];
    //[self.msgImgPathArr removeAllObjects];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        //1. 打开数据库
        if (![db open]) return;
        
        //2. 查询数据
        FMResultSet *resultSet = [db executeQuery:@"SELECT *FROM t_message"];
        //        NSMutableArray *messageArr = ;
        if (self.IMmessageArr) {
            [self.IMmessageArr removeAllObjects];
        } else {
            self.IMmessageArr = [NSMutableArray array];
        }
        
        
        //3. 遍历结果, 如果下一行有数据， 返回YES
        while ([resultSet next]) {
            NSString *dictStr = [resultSet objectForColumnName:@"messageDict"];
            NSData *data =[dictStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            if (messageDict == nil) return;
            NSLog(@"-----xcv123-q-----:%@--------:%@----------------%@", messageDict[@"userData"], orderId, messageDict);
            if ([messageDict[@"userData"] isEqualToString:orderId]) {
                [self.PushMessageArr addObject:messageDict];
            }
        }
        //4. 关闭数据库
        [db close];
    }];
    
    return self.PushMessageArr;
}


- (NSMutableArray *)getReceivedMsgUnReadCountArrWithOrderId:(NSString *)orderId {
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        //1. 打开数据库
        if (![db open]) return;
        
        
        //2. 查询数据
        FMResultSet *resultSet = [db executeQuery:[NSString stringWithFormat:@"SELECT *FROM t_message WHERE msgTo='%@' AND orderId='%@'",kMyChatId,orderId]];
        
        NSLog(@"---------dffdsfdfdsfds----$$$$$--%@", resultSet);
        NSMutableArray *unReadMsgArr = [NSMutableArray array];
        self.unReadMsgArr = unReadMsgArr;
        //3. 遍历结果, 如果下一行有数据， 返回YES
        while ([resultSet next]) {
            
            NSString *dictStr = [resultSet objectForColumnName:@"unReadCount"];
            NSLog(@"---dddsssqqqqqq------%@", dictStr);
            
            if (![NSString isBlankString:dictStr]) {
                [self.unReadMsgArr addObject:dictStr];
            }
            
            
        }
        //4. 关闭数据库
        [db close];
    }];
    
    return self.unReadMsgArr;
}


- (NSMutableArray *)getMsgImgPathArrWithOrderId:(NSString *)orderId {
    
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        //1. 打开数据库
        if (![db open]) return;
        
        //2. 查询数据
        FMResultSet *resultSet = [db executeQuery:@"SELECT *FROM t_message"];
        //        NSMutableArray *messageArr = ;
        if (self.msgImgPathArr) {
            [self.msgImgPathArr removeAllObjects];
        } else {
            self.msgImgPathArr = [NSMutableArray array];
        }
        
        
        //3. 遍历结果, 如果下一行有数据， 返回YES
        while ([resultSet next]) {
            //            int myID = [resultSet intForColumn:@"id"];
            //            NSString *myName = [resultSet stringForColumn:@"name"];
            //            int myAge = [resultSet intForColumn:@"age"];
            
            NSString *dictStr = [resultSet objectForColumnName:@"messageDict"];
            NSData *data =[dictStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            if (messageDict == nil) return;
            
            NSLog(@"-----xcv123--b----:%@--------:%@----------------%@", messageDict[@"userData"], orderId, messageDict);
            
            
            if ([messageDict[@"userData"] isEqualToString:orderId]) {
                
                NSString* imgPath = messageDict[@"imagePath"];
                NSArray *arr = [imgPath componentsSeparatedByString:@"Caches/"];
                
                imgPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:arr.lastObject];
                
                [self.msgImgPathArr addObject:imgPath];
                
                
                NSLog(@"-----xcv12--事实上----:%@", self.messageArr);
            }
            //            NSLog(@"-----xxxx----%@", messageDict[@"userData"]);
            
            
        }
        //4. 关闭数据库
        [db close];
    }];
    
    return self.msgImgPathArr;
}


- (void)upDateUnReadMsgCountWithOrderId:(NSString *)orderId {
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [self.dbQueue inDatabase:^(FMDatabase *db) {
            // 打开数据库
            if (![db open]) return;
            
            
            NSString *dbStr = [NSString stringWithFormat:@"UPDATE t_message SET unReadCount = 0 WHERE msgTo='%@' AND orderId = '%@'", kMyChatId, orderId];
            
            BOOL result = [db executeUpdate:dbStr];
            
            // 判断成功与否
            if (result) {
                NSLog(@"修改数据成功");
            } else {
                NSLog(@"修改数据失败");
            }
            // 关闭数据库
            [db close];
        }];
        
        
    });
    //    NSLog(@"dbQueuePath ====== %@", dbQueuePath);
    
}

#pragma mark -- deleteAllMessageWithOrderId

- (void)deleteAllMessageWithOrderId:(NSString *)orderId {
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        // 打开数据库
        if (![db open]) return;
        
        // 清空数据
        BOOL result = [db executeUpdate:@"DELETE FROM t_message WHERE orderId = ?", orderId];
        //        BOOL result1 = [db executeUpdate:@"DROP TABLE IF EXISTS t_message"];
        
        // 判断成功与否
        if (result) {
            [self.messageArr removeAllObjects];
            NSLog(@"删除数据成功");
        } else {
            NSLog(@"删除数据失败");
        }
        
        
        //        // 判断成功与否
        //        if (result1) {
        //            [self.messageArr removeAllObjects];
        //            //            NSLog(@"清空数据数据成功");
        //        } else {
        //            NSLog(@"清空数据失败");
        //        }
        
        // 关闭数据库
        [db close];
    }];
    
}

// 清空数据
- (void)deleteAllMessage {
    
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        // 打开数据库
        if (![db open]) return;
        
        // 清空数据
        BOOL result = [db executeUpdate:@"DELETE FROM t_message"];
        //        BOOL result1 = [db executeUpdate:@"DROP TABLE IF EXISTS t_message"];
        
        // 判断成功与否
        if (result) {
            [self.messageArr removeAllObjects];
            //            NSLog(@"清空数据数据成功");
        } else {
            NSLog(@"清空数据失败");
        }
        
        
        //        // 判断成功与否
        //        if (result1) {
        //            [self.messageArr removeAllObjects];
        //            //            NSLog(@"清空数据数据成功");
        //        } else {
        //            NSLog(@"清空数据失败");
        //        }
        
        // 关闭数据库
        [db close];
    }];
    
}

- (NSMutableArray *)getMyAllMessageUserDataArr:(NSString*)orderId {
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        //1. 打开数据库
        if (![db open]) return;
        
        //2. 查询数据
        FMResultSet *resultSet = [db executeQuery:@"SELECT *FROM t_message ORDER BY msgTime desc"];
        NSMutableArray *magUserDataArr = [NSMutableArray array];
        self.magUserDataArr = magUserDataArr;
        //3. 遍历结果, 如果下一行有数据， 返回YES
        while ([resultSet next]) {
            //            int myID = [resultSet intForColumn:@"id"];
            //            NSString *myName = [resultSet stringForColumn:@"name"];
            //            int myAge = [resultSet intForColumn:@"age"];
            
            NSString *dictStr = [resultSet objectForColumnName:@"messageDict"];
            NSData *data =[dictStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            if (messageDict == nil) return;
            
            //
            NSString *userDataStr = messageDict[@"userData"];
            
            //
            if ([userDataStr containsString:@"push_type_add_pat"]) {
                 [self.magUserDataArr addObject:messageDict[@"userData"]];
            } else {
                //
                NSArray *userDataArr = [userDataStr componentsSeparatedByString:@","];
                
                if (![self.magUserDataArr containsObject:userDataStr] && ([messageDict[@"from"] isEqualToString:kMyChatId] || ([messageDict[@"to"] isEqualToString:kMyChatId])) && [userDataStr rangeOfString:@","].length > 0 && ![kMyChatId isEqualToString:userDataArr[0]]) {
                    
                    [self.magUserDataArr addObject:messageDict[@"userData"]];
                    NSLog(@"-----wsx------:%@--------------:%@", messageDict, kMyChatId);
                    // NSLog(@"-----xxxx----%@", messageDict[@"userData"]);
                }
            }
           
        }
        //4. 关闭数据库
        [db close];
    }];
    
    return self.magUserDataArr;
}


- (void)setDBCellMaxY:(CGFloat)DBCellMaxY {
    _DBCellMaxY = DBCellMaxY;
}

-(void)changeTheMessageReadState:(BOOL)isRead with:(NSString*)orderId andMessageId:(NSString*)messageId{
    
    dispatch_async(self.GlobSerieaQueue, ^{
        
        if (self.messageReadArray.count == 0) {
            
            [self.dbQueue inDatabase:^(FMDatabase *db) {
                // 打开数据库
                if (![db open]) return;
                
                NSString *dbStr = [NSString stringWithFormat:@"SELECT *FROM t_message WHERE msgTo='%@' AND orderId='%@'", kMyChatId, orderId];
                
                FMResultSet *resultSet = [db executeQuery:dbStr];
                while ([resultSet next]) {
                    
                    NSString *dictStr = [resultSet objectForColumnName:@"messageDict"];
                    NSData *data =[dictStr dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                    NSLog(@"添加次数----");
                    [self.messageReadArray addObject:messageDict];
                    
                }
                // 关闭数据库
                [db close];
            }];
   
        }
        
        [self setNeedUpdateReadDictSateWithMessageId:messageId AndState:YES InArray:self.messageReadArray];
        
        
    });
    
    
    
}
/**
 * 获得所有未读消息对象
 */
-(NSMutableArray *)getAllUnReadMessage{
    
    NSMutableArray *messageArray = [NSMutableArray array];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        //1. 打开数据库
        if (![db open]) return;
        //2. 查询数据
        FMResultSet *resultSet = [db executeQuery:@"SELECT *FROM t_message"];
        
        //3. 遍历结果, 如果下一行有数据， 返回YES
        while ([resultSet next]) {
            //            int myID = [resultSet intForColumn:@"id"];
            //            NSString *myName = [resultSet stringForColumn:@"name"];
            //            int myAge = [resultSet intForColumn:@"age"];
            
            NSString *dictStr = [resultSet objectForColumnName:@"messageDict"];
            NSData *data =[dictStr dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            
            if (messageDict == nil) return;
            NSString *read = messageDict[@"isRead"];
            if (!read.boolValue) {
                //[messageArray addObject:messageDict];
                [messageArray addObject:[self createMessageObject:messageDict]];
            }
            
        }
        //4. 关闭数据库
        [db close];
    }];
    
    
    return messageArray;
}


-(ECMessage*)createMessageObject:(NSDictionary*)dict{
      // 创建赋值 只是为了在数据库能找到 相应 id
        ECMessage *subMessage = [[ECMessage alloc]init];
        subMessage.from = dict[@"from"];
        subMessage.to = dict[@"to"];
        subMessage.messageId = dict[@"messageId"];
        subMessage.timestamp = dict[@"timestamp"];
        subMessage.userData = dict[@"userData"];
        subMessage.sessionId = dict[@"sessionId"];

    return subMessage;
    
}
/**
 * 推送消息状态更改
 */
-(void)changePushMessageReadState:(BOOL)isRead withMessageId:(NSString*)messageId{
    
    dispatch_async(self.GlobSerieaQueue, ^{
            
            [self.dbQueue inDatabase:^(FMDatabase *db) {
                // 打开数据库
                if (![db open]) return;
                
                NSString *dbStr = [NSString stringWithFormat:@"SELECT *FROM t_message WHERE messageId='%@' AND orderId='%@'",messageId,kOrganGraftingUserData];
                
                FMResultSet *resultSet = [db executeQuery:dbStr];
                while ([resultSet next]) {
                    
                    NSString *dictStr = [resultSet objectForColumnName:@"messageDict"];
                    NSData *data =[dictStr dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                    NSLog(@"添加次数----");
                    [messageDict setValue:[NSNumber numberWithBool:isRead] forKey:@"isRead"];
                    [self.pushMessageReadArray addObject:messageDict];
                    [self updateThePushMessageState:messageId AndDict:messageDict.mj_JSONString];
                    
                }
                // 关闭数据库
                [db close];
            }];
        
      //  [self setNeedUpdateReadDictSateWithMessageId:messageId AndState:YES InArray:self.pushMessageReadArray];
        
        
    });
    
    
    
}

-(void)updateThePushMessageState:(NSString*)messageId AndDict:(NSString*)dict{
    
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        [self.dbQueue inDatabase:^(FMDatabase *db) {
            // 打开数据库
            if (![db open]) return;
            
            
            NSString *dbStr = [NSString stringWithFormat:@"UPDATE t_message SET messageDict = '%@' WHERE messageId= '%@'", dict, messageId];
            
            BOOL result = [db executeUpdate:dbStr];
            
            // 判断成功与否
            if (result) {
                NSLog(@"修改数据成功");
            } else {
                NSLog(@"修改数据失败");
            }
            // 关闭数据库
            [db close];
        }];
        
        
    });
}


-(void)setNeedUpdateReadDictSateWithMessageId:(NSString*)messageId AndState:(BOOL)isRead InArray:(NSArray*)array{
    
    for (NSDictionary *messageDict in array) {
        
    if ([[messageDict valueForKey:@"messageId"] isEqualToString:messageId]) {
        [messageDict setValue:[NSNumber numberWithBool:isRead] forKey:@"isRead"];
        [self.needUpdateReadStateArray setObject:[NSNumber numberWithBool:isRead] forKey:[messageDict valueForKey:@"messageId"]];
    }
        
    }
    
}

@end

