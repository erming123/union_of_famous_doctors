//
//  HZChatCell.h
//  SLIOP
//
//  Created by 季怀斌 on 16/8/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@protocol HZChatCellDelegate <NSObject>

- (void)chatCellWithMessageDict:(NSDictionary *)messageDict withImageView:(UIImageView *)imageView sender:(UIGestureRecognizer *)sender;

@end
@interface HZChatCell : UITableViewCell
@property (nonatomic, strong) NSDictionary *chatMessageDict;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *IMOrder;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, assign) BOOL isSender;
@property (nonatomic, weak) id<HZChatCellDelegate>delegate;
+ (NSString *)cellIdentifierWithModel:(NSDictionary *)modelDict;

+ (CGFloat)cellHeightWithModel:(NSDictionary *)modelDict;

@end
