//
//  HZIMChatViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@interface HZIMChatViewController : HZViewController
@property (nonatomic, assign) BOOL isClicked;
- (void)addToMesageDBWithMessage:(ECMessage *)message withLocalImagePath:(NSString *)imageLocalPath;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *IMOrder;
@end
