//
//  HZAccount.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/9.
//  Copyright © 2016年 huazhuo. All rights reserved.
//


#import "HZJsonModel.h"
@interface HZAccount : HZJsonModel <NSCoding>
@property (nonatomic, copy) NSString *phoneNumber;

/**
 *  账号的有效期
 */
@property (nonatomic, copy) NSString *expires_in;
/**
 *  用户唯一标识符
 */
@property (nonatomic, copy) NSString *jti;

/**
 *   过期时间 = 当前保存时间+有效期
 */
@property (nonatomic, strong) NSDate *expires_date;

/**c
 *  账号的有效期
 */
@property (nonatomic, copy) NSString *remind_in;

@property (nonatomic, copy) NSString *scope;

@property (nonatomic, copy) NSString *token_type;

+ (instancetype)accountWithDict:(NSDictionary *)dict;
@end
