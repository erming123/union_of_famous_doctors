//
//  HZAccount.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/9.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZAccount.h"
#import "MJExtension.h"


@implementation HZAccount
// 底层便利当前的类的所有属性，一个一个归档和接档
+ (instancetype)accountWithDict:(NSDictionary *)dict {
    HZAccount *account = [[self alloc] init];
    
    [account setValuesForKeysWithDictionary:dict];
    
    return account;
}

//- (void)setExpires_in:(NSString *)expires_in {
//    _expires_in = expires_in;
//    
//    // 计算过期的时间 = 当前时间 + 有效期
//    _expires_date = [NSDate dateWithTimeIntervalSinceNow:[expires_in longLongValue]];
//}

- (NSDate *)expires_date {
    if (_expires_date == nil) {
        
        NSDate *date = [NSDate date];
        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        NSInteger interval = [zone secondsFromGMTForDate: date];
        NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
//        _expires_date = [localeDate dateByAddingTimeInterval:[self.expires_in longLongValue]];
        _expires_date = [localeDate dateByAddingTimeInterval:[@"5" longLongValue]];
    }
    
    return _expires_date;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"acess_Token:%@  \n"];
}


//归档的时候调用：告诉系统哪个属性需要归档，如何归档
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_phoneNumber forKey:@"phoneNumber"];
}

// 解档的时候调用：告诉系统哪个属性需要解档，如何解档

- (id)initWithCoder:(NSCoder *)aDecoder

{
    if (self = [super init]) {

        // 一定要记得赋值
       self.phoneNumber =  [aDecoder decodeObjectForKey:@"phoneNumber"];
    }
    return self;
}
@end
