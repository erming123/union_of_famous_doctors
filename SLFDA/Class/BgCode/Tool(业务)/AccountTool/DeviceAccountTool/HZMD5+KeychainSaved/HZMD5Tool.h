//
//  HZMD5Tool.h
//  sss
//
//  Created by 季怀斌 on 2017/4/19.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZMD5Tool : NSObject
//// 32位小写
//+(NSString *)MD5ForLower32Bate:(NSString *)str;
//// 32位大写
//+(NSString *)MD5ForUpper32Bate:(NSString *)str;
//// 16为大写
//+(NSString *)MD5ForUpper16Bate:(NSString *)str;
//// 16位小写
//+(NSString *)MD5ForLower16Bate:(NSString *)str;


// 注册16位加盐
+(NSString *)MD5ForLower16Bate:(NSString *)UUIDStr saltStr:(NSString *)saltStr;
@end
