//
//  HZDeviceAccountTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/19.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZDeviceAccountTool : NSObject
// 获取盐值
+ (void)getMD5SaltWithAccountKey:(NSString *_Nullable)AccountKey accountPassWordKey:(NSString *_Nullable)accountPassWordKey success:(void(^_Nullable)(NSDictionary * _Nullable saltDict))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//.获取DeviceAccount和deviceAccountPassWord
+ (void)getDeviceAccountAndDeviceAccountPassWordWithDeviceAccountSalt:(NSString *_Nullable)deviceAccountSalt deviceAccountPassWordSalt:(NSString *_Nullable)deviceAccountPassWordSalt;

//1.设备号登录功能是否开启
+ (void)justDeviceAccountLoginFunctionAvailableSuccess:(void(^_Nullable)(BOOL isAvailable))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//2.设备号是否已经注册
+ (void)justDeviceAccountRegistedWithDeviceAccount:(NSString *_Nullable)deviceAccount success:(void(^_Nullable)(NSString * _Nullable userId))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//3.设备号注册
+ (void)registDeviceAccountWithDeviceAccount:(NSString *_Nullable)deviceAccount deviceAccountPassWord:(NSString *_Nullable)deviceAccountPassWord success:(void(^_Nullable)(NSString * _Nullable userId))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//4.设备号绑定
+ (void)associateDeviceAccountWithDeviceAccount:(NSString *_Nullable)deviceAccount deviceAccountPassWord:(NSString *_Nullable)deviceAccountPassWord success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

//5.设备号解绑
+ (void)disassociateDeviceAccountWithDeviceAccount:(NSString *_Nullable)deviceAccount success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
