//
//  HZAccountTool.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/9.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HZAccount;
@interface HZAccountTool : NSObject

+ (void)saveAccount:(HZAccount *)account;

+ (HZAccount *)account;


// 获取basic_token
+ (void)getBasicTokenSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

// 获取动态验证码
+ (void)getVerifyCodeWithUserPhoneNumber:(NSString *)userPhoneNumber success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

// 验证动态验证码
+ (void)justVerifyCodeWithPhoneNumber:(NSString *)phoneNumber verifyCode:(NSString *)verifyCode success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

// 注册
+ (void)registAccountWithPhoneNumber:(NSString *)phoneNumber passWord:(NSString *)passWord type:(NSString *)type success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;


// 登录获取access_token
+ (void)getLoginCodeWithUserPhoneNumber:(NSString *)userPhoneNumber andLoginVerifyCode:(NSString *)loginVerifyCode success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;


// 获取App版本号
+ (void)getAppStoreNewVersionWithSuccess:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

// 刷新access_token
+ (void)getNewAccessTokenWithRefreshToken:(NSString *)refreshToken success:(void(^)(id responseObject))success failure:(void(^)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;

// 用设备账号和密码获取新的Access_token
+ (void)getNewAccessTokenWithDeviceAccount:(NSString *_Nullable)deviceAccount deviceAccountPassWord:(NSString *_Nullable)deviceAccountPassWord success:(void(^_Nullable)(id _Nullable responseObject))success failure:(void(^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error))failure;
@end
