//
//  HZRootVCTool.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZRootVCTool.h"
//#import "HZNewFeatureViewController.h"
//#import "NSLoginViewController.h"
#import "HZHomeViewController.h"
#import "HZTabBarController.h"
#import "HZAlertView.h"
#import "HZVersionTool.h"
#import "HZUserTool.h"
#import "HZLaunchViewController.h"

#import "HZCompleteInformationViewController.h"
@implementation HZRootVCTool
+ (void)chooseRootViewController:(UIWindow *)window withBlock:(void(^)())block{
    
    
    
    //0
    HZLaunchViewController *vCtr = [HZLaunchViewController new];
    window.rootViewController = vCtr;
    // 1. 获取当前版本号
//    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
    
    // 2. 获取上一次的版本号
    NSString *lastVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"version"];
    NSString *access_token = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"access_token"];

    BOOL isLogin = ![NSString isBlankString:access_token];
    if ([kCurrentBundleVersion isEqualToString:lastVersion]) {
        //
        //判断是否已经登录
        // 推出本地服务器, 置空token
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
//        BOOL isLogin = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"access_token"];
        
        NSLog(@"---------BOOL------%@", isLogin == YES ? @"YES":@"NO");
        if (isLogin) {
            NSLog(@"---------伪已经登录");
 
            HZTabBarController *tabBarCtr = [HZTabBarController new];
            //                             tabBarCtr.user = user;
            
            window.rootViewController = tabBarCtr;
            
                        [HZUserTool getUserWithSuccess:^(id responseObject) {
                            if ([responseObject[@"state"] isEqual:@200]) {
//
                                NSDictionary *dataDict = responseObject;
                                NSDictionary *resultDict = dataDict[@"results"];
                                NSDictionary *userDict = resultDict[@"user"];
//
//            
                                NSLog(@"------getUser成功:%@", [userDict getDictJsonStr]);
                                
//                                //1
//                                HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
                                HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                                user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
//                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:user.openid];
                                
                                NSLog(@"----user.remoteAccount-----%@", user.remoteAccount);
                                [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
//                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                                window.rootViewController = navCtr;
                                //
                                if (block) {
                                    block();
                                }

                            } else if ([responseObject[@"state"] isEqual:@4001]) { // 去完善
                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
                                window.rootViewController = navCtr;
                                [navCtr pushViewController:[HZCompleteInformationViewController new] animated:YES];
//                                HZCompleteInformationViewController *completeInformationVCtr = ;
                                
                                if (block) {
                                    block();
                                }
                            } else {// 去登录
                                
                                //
                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
                                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                                //------------- 去掉accesstoken，回到 ------------
                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                //                                HZCompleteInformationViewController *completeInformationVCtr = ;
                                
                                if (block) {
                                    block();
                                }
                            }

            
            
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
                            
                          NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
                            
                            if (!userDict) {
                            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                                //
                                HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
                                [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;

                                //------------- 去掉accesstoken，回到 ------------
                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"access_token"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                            }];
                            if (kCurrentSystemVersion >= 9.0) {
                                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                            }

                            [alertCtr addAction:ensureAlertAction];
                            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
                                return;
                            }
                            
                            //
//                            HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
                            HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                            user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                            //                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:user.openid];
//                            homeVCtr.user = user;
                            
                            NSLog(@"----user.remoteAccount-----%@", user.remoteAccount);
                            
//                            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
//                            window.rootViewController = navCtr;
                            
                            HZTabBarController *tabBarCtr = [HZTabBarController new];
//                            tabBarCtr.user = user;
                            window.rootViewController = tabBarCtr;

                            //
                            if (block) {
                                block();
                            }
                            
                            //
                        }];
   
        } else {
            NSLog(@"---------去登录");
            // 选择登录页面
            HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
            window.rootViewController = navCtr;
            
           
            if (block) {
                block();
            }
        }
        
    } else { //
        
        if (!isLogin) { // 第一次下载
            NSLog(@"---------刚更新");
            
            //进入新特性页面
            window.rootViewController = [NSClassFromString(@"HZNewFeatureViewController") new];
            
            
            // 保存当前版本号
            [[NSUserDefaults standardUserDefaults] setObject:kCurrentBundleVersion forKey:@"version"];
            
            
            if (block) {
                block();
            }

        } else { // 更新版本
            
            NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
            if (userDict)  {
                
                //
                HZTabBarController *tabBarCtr = [HZTabBarController new];
                window.rootViewController = tabBarCtr;
                //
                [HZUserTool getUserWithSuccess:^(id responseObject) {
                    if ([responseObject[@"state"] isEqual:@200]) {
                        
                        //
                        NSDictionary *dataDict = responseObject;
                        NSDictionary *resultDict = dataDict[@"results"];
                        NSDictionary *userDict = resultDict[@"user"];
                        [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                    } else {
                        [window makeToast:@"更新用户信息失败" duration:1.5f position:CSToastPositionCenter];
                    }
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"------34343434343434---");
                }];
            } else {
                
                
                [HZUserTool getUserWithSuccess:^(id responseObject) {
                    if ([responseObject[@"state"] isEqual:@200]) {
                        //
                        NSDictionary *dataDict = responseObject;
                        NSDictionary *resultDict = dataDict[@"results"];
                        NSDictionary *userDict = resultDict[@"user"];
                        //
                        //
                        NSLog(@"------getUser成功:%@", [userDict getDictJsonStr]);
                        
                        //                                //1
                        //                                [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                        
                        
                        //                    HZHomeViewController *homeVCtr = [[HZHomeViewController alloc] init];
                        HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                        user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                        //                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:user.openid];
                        //                    homeVCtr.user = user;
                        
                        NSLog(@"----user.remoteAccount-----%@", user.remoteAccount);
                        [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
                        //                    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:homeVCtr];
                        //                    window.rootViewController = navCtr;
                        
                        HZTabBarController *tabBarCtr = [HZTabBarController new];
                        //                    tabBarCtr.user = user;
                        window.rootViewController = tabBarCtr;
                        
                        //
                        if (block) {
                            block();
                        }
                        
                    } else if ([responseObject[@"state"] isEqual:@4001]) {
                        HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
                        window.rootViewController = navCtr;
                        [navCtr pushViewController:[HZCompleteInformationViewController new] animated:YES];
                        //                                HZCompleteInformationViewController *completeInformationVCtr = ;
                        
                        if (block) {
                            block();
                        }
                    }
                    
                    
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                    //        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                    //        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    //            loginBtn.userInteractionEnabled = YES;
                    //        }];
                    //        [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
                    //        [alertCtr addAction:ensureAlertAction];
                    //        [self presentViewController:alertCtr animated:YES completion:nil];
                    
                    
                    NSLog(@"------34343434343434---");
                }];
            }
            
            
            


        }
        
        
    }

   //


}


@end
