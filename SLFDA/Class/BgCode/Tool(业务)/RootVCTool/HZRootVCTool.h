//
//  HZRootVCTool.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIWindow;
@interface HZRootVCTool : NSObject
+ (void)chooseRootViewController:(UIWindow *)window withBlock:(void(^)())block;
@end
