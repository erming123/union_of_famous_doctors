//
//  UIImageView+SizeFit.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/17.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "UIImageView+SizeFit.h"
#import "HZUserTool.h"
static char loadOperationKey;
static char taskKey;
@implementation UIImageView (SizeFit)
- (void)sizeToFitWithImage {
    self.width = self.image.size.width;
    self.height = self.image.size.height;
}

/**
 * 请求图片的封装base64图像 参数:xxxxxxxx.jpg
 */
-(void)getPictureWithFaceUrl:(NSString*)urlParam AndPlacehoderImage:(NSString*)placehoderName{
    [self cancelTheOtherImageLoad];
    UIImage *cacheImg = nil;
    UIImage *memCacheImg = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:urlParam];
    if (memCacheImg) {
        cacheImg = memCacheImg;
    }else{
        UIImage *diskImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:urlParam];
        if (diskImage) cacheImg = diskImage;
    }
    
    //
    if (cacheImg) {
        self.image = cacheImg;
    }else{
        // 先设置占位图片
        self.image = [UIImage imageNamed:placehoderName];
        NSBlockOperation *operation = [[NSBlockOperation alloc]init];
        __weak typeof(operation)weakOperation = operation;
        //
        [weakOperation addExecutionBlock:^{
            
            if(urlParam.length == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.image = [UIImage imageNamed:placehoderName];
                });
                return;
            }

            //1.构造URL
            NSString *resultUrl = [NSString stringWithFormat:@"cfdu/v1/openapi/json/doctor/photo/get?url=%@",urlParam];
            NSURL *url = [NSURL URLWithString:kGatewayUrlStr(resultUrl)];
            //2.构造Request
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            //(1)设置为POST请求
            [request setHTTPMethod:@"GET"];
            //(2)超时
            [request setTimeoutInterval:60];
            //(3)设置请求头
            NSString *headDomainValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
            [request setValue:headDomainValue forHTTPHeaderField:@"Authorization"];
            
          NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                NSHTTPURLResponse *respon = (NSHTTPURLResponse*)response;
                if (!error && respon.statusCode == 200) {
                    id responseObject = [NSDictionary changeType:data];
                    NSString *avatarStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                    avatarStr = [[avatarStr componentsSeparatedByString:@"base64,"] lastObject];
                    NSData *_decodedImageData   = [[NSData alloc] initWithBase64EncodedString:avatarStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
                    UIImage *img = [UIImage imageWithData:_decodedImageData];
                    [[SDImageCache sharedImageCache] storeImage:img forKey:urlParam];
                    // 回主线程
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // 查看当前 task 是否被cancle 如果被cancle,那么不赋值;
                       self.image = img;
                    });
                    
                }else{
                   // 有错误
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.image = [UIImage imageNamed:placehoderName];
                    });
                    
                }
            }];
           
           [dataTask resume];
           objc_setAssociatedObject(operation, &taskKey, dataTask, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }];
        
        
        
        // 添加操作
        [[HZChatHelper sharedInstance].downLoadImgQueue addOperation:operation];
        [[self myOperationDictionary] setObject:operation forKey:@"imageLoad"];
        
    }
   
}

- (NSMutableDictionary *)myOperationDictionary {
    NSMutableDictionary *operations = objc_getAssociatedObject(self, &loadOperationKey);
    if (operations) {
        return operations;
    }
    operations = [NSMutableDictionary dictionary];
    objc_setAssociatedObject(self, &loadOperationKey, operations, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    return operations;
}

-(void)cancelTheOtherImageLoad {
    
    NSMutableDictionary *operationDictionary = [self myOperationDictionary];
    NSOperation *operation = [operationDictionary objectForKey:@"imageLoad"];
    //NSLog(@"operation=%@",operation);
    NSURLSessionDataTask *task = objc_getAssociatedObject(operation, &taskKey);
      [task cancel];
      [operation cancel];
      [operationDictionary removeObjectForKey:@"imageLoad"];
}
@end
