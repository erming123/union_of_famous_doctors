//
//  HZJsonModel+PropertyNilValueSet.m
//  HZPropertiesDemo
//
//  Created by 季怀斌 on 2017/7/21.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZJsonModel+PropertyNilValueSet.h"
#import <objc/runtime.h>
#import "MJExtension.h"
@implementation HZJsonModel (PropertyNilValueSet)
- (NSDictionary *)propertyValueDict {
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for (i = 0; i<outCount; i++)
    {
        objc_property_t property = properties[i];
        const char* char_f =property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        id propertyValue = [self valueForKey:(NSString *)propertyName];
        
        //        if (propertyValue)
        if (!propertyValue) {
            propertyValue = @"";
        }
        [props setObject:propertyValue forKey:propertyName];
    }
    free(properties);
    return props;
}

@end
