//
//  UIImageView+SizeFit.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/17.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (SizeFit)
- (void)sizeToFitWithImage;
/**
 * 获取图像(返回值是 base64)
 * 参数:xxxxxx.jpg   faceUrl?
 */
-(void)getPictureWithFaceUrl:(NSString*)urlParam AndPlacehoderImage:(NSString*)placehoderName;
@end
