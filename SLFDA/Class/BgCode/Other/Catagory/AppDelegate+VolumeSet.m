//
//  AppDelegate+VolumeSet.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/8/8.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "AppDelegate+VolumeSet.h"
static const void *volumeViewKey = &volumeViewKey;
static const void *sliderKey = &sliderKey;
@implementation AppDelegate (VolumeSet)


#pragma mark -- SET&GET
- (MPVolumeView *)volumeView {
    return objc_getAssociatedObject(self, volumeViewKey);
}

- (void)setVolumeView:(MPVolumeView *)volumeView {
    objc_setAssociatedObject(self, volumeViewKey, volumeView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (UISlider *)slider {
    return objc_getAssociatedObject(self, sliderKey);
}

- (void)setSlider:(UISlider *)slider {
    objc_setAssociatedObject(self, sliderKey, slider, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark -- setVolume:isResignActived:
- (void)setVolume:(NSNumber *)value isResignActived:(BOOL)isResignActived{


//    NSLog(@"---self.volumeView------%@", self.volumeView);
//    [self.volumeView removeFromSuperview];
//    self.volumeView = nil;
    //    if (!self.volumeView) {
    self.volumeView = [[MPVolumeView alloc] init];
    self.volumeView.frame = CGRectMake(-100, -100, 200, 20);
    self.volumeView.center = CGPointMake(-550,370);
    self.volumeView.showsVolumeSlider = YES;
    self.volumeView.hidden = NO;
//    self.slider.hidden = YES;
    [self.window addSubview:self.volumeView];
    //    }


    if (isResignActived) {
        //
        // change system volume, the value is between 0.0f and 1.0f

        //    NSLog(@"---xxxx------%lf", value);
//        self.volumeView.hidden = YES;
//        self.slider.hidden = YES;


        [self.slider setValue:[value floatValue]animated:NO];
        // send UI control event to make the change effect right now.
        [self.slider sendActionsForControlEvents:UIControlEventTouchUpInside];
        [self.volumeView sizeToFit];

        //
        [self.volumeView removeFromSuperview];
        self.volumeView = nil;
        return;
    };
    //    UISlider* volumeViewSlider = nil;
    for (UIView *view in [self.volumeView subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            //            volumeViewSlider = (UISlider*)view;

            //            NSLog(@"------ssaaaQQQ---%lf", volumeViewSlider.value);
            self.slider = (UISlider *)view;

//            self.volumeView.hidden = YES;
//            self.slider.hidden = YES;
            if (!self.slider || self.slider.value == 0) {
                [self performSelector:@selector(setVoice:) withObject:value afterDelay:0.5f];
            }

            break;
        }
    }



}

- (void)setVoice:(NSNumber *)value{
    //    NSLog(@"------ssaaaQQQ---%lf", self.slider.value);
    NSNumber *nowVolum = [NSNumber numberWithFloat:self.slider.value];
//    NSLog(@"当前的音量是------%@",nowVolum);
    [[NSUserDefaults standardUserDefaults] setObject:nowVolum forKey:@"nowVolum"];

    //
    self.volumeView.showsVolumeSlider = NO;
//    self.volumeView.y = - 100;
//    self.volumeView.hidden = YES;
//    self.slider.hidden = YES;
    // change system volume, the value is between 0.0f and 1.0f

    //    NSLog(@"---xxxx------%lf", value);
    [self.slider setValue:[value floatValue] animated:NO];
    // send UI control event to make the change effect right now.
    [self.slider sendActionsForControlEvents:UIControlEventTouchUpInside];
    [self.volumeView sizeToFit];
    [self.volumeView removeFromSuperview];
    self.volumeView = nil;

    NSLog(@"--self.volumeView--%@", self.volumeView);
}

@end
