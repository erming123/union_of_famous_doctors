//
//  UIViewController+PanBack.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/10/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PanBack)<UIGestureRecognizerDelegate>
- (void)setViewPanBack;
@end
