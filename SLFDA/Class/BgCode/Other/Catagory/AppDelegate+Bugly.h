//
//  AppDelegate+Bugly.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/8/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "AppDelegate.h"
#import <Bugly/Bugly.h>
@interface AppDelegate (Bugly) <BuglyDelegate>
- (void)setMyBugly;
@end
