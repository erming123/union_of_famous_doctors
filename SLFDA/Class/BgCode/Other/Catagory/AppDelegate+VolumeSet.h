//
//  AppDelegate+VolumeSet.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/8/8.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
@interface AppDelegate (VolumeSet)
@property (nonatomic, strong) MPVolumeView *volumeView;
//** <#注释#>*/
@property (nonatomic, strong) UISlider *slider;
- (void)setVolume:(NSNumber *)value isResignActived:(BOOL)isResignActived;
@end
