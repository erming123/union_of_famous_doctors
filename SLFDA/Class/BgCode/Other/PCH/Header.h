//
//  Header.h
//  SLIOP
//
//  Created by 季怀斌 on 16/8/17.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#ifndef Header_h
#define Header_h

#pragma mark -- category
#import "UIColor+HexString.h"

#import "UILabel+FontAndTextColor.h"
#import "NSDictionary+JsonStr.h"
#import "NSObject+SetNilStr.h"
#import "UIView+Toast.h"
#import "UITextField+ForbiddenCopyOrPaste.h"
#import "HZJsonModel+PropertyNilValueSet.h"
#import "MBProgressHUD+NHAdd.h"
#import "UIButton+badge.h"
#import "UIViewController+PanBack.h"
#import "UIImageView+SizeFit.h"
#import "NSString+Extension.h"
#import "UIImage+Extension.h"
#import "UIView+Extension.h"
#pragma mark -- 网络数据层
#import "HZAccount.h"
#import "HZAccountTool.h"
//#import "HZDataHelper.h"
#import "HZChatHelper.h"
//#import "HZOrderIdJCYBIdDictTool.h"
#import "HZRefreshHeader.h"

#pragma mark -- 继承
#import "HZNavigationController.h"
#import "HZLabel.h" //label默认黑色
#import "HZTableView.h"
#import "HZTableViewCell.h"
#import "HZTableViewController.h"
#import "HZDashLine.h"
#import "HZViewController.h"

#pragma mark -- 三方控件
#import "MJExtension.h"
#import "UIImageView+WebCache.h"
#import <YYcache/YYCache.h>

#pragma mark -- 容联云

#import "ECLoginInfo.h"
#import "ECDevice.h"
#import "ECTextMessageBody.h"
#import "ECVoiceMessageBody.h"
#endif /* Header_h */
