//
//  AppDelegate.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "AppDelegate.h"
#import "HZAlertView.h"
#import "HZRootVCTool.h"
#import "HZLoginViewController.h"
#import "AppDelegate+Bugly.h"
#import "AppDelegate+VolumeSet.h"
#import "IQKeyboardManager.h"
#import "HZTabBarController.h"

#import "HZOrganGraftingViewController.h"

// 仅 10以后
#import <UserNotifications/UserNotifications.h>

#import "HZMessageDB.h"

@interface AppDelegate ()<ECDeviceDelegate, HZAlertViewDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZAlertView *alertView;
//** <#注释#>*/
@property (nonatomic, strong) UIView *cover;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //
    
    //1. 设置Bugly 120400
    [self setMyBugly];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    //2.设置容联云
    [ECDevice sharedInstance].delegate = [HZChatHelper sharedInstance];
    [[ECDevice sharedInstance] SwitchServerEvn:NO];// NO:生产  YES:开发
    
    //    // 荣连云登录
    //    [self loginChat];
    
    //3.注册通知
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert categories:nil]];
    
    //4.选择rootVC(包含登录)
    [self chooseRootViewController];
    
    
//    //5.音量调节到最大
//    [self setVolume:[NSNumber numberWithFloat:1.0f] isResignActived:NO];
    
    //6.显示
    [self.window makeKeyAndVisible];
    
    //设置缓存 的最大占用空间;
    [SDImageCache sharedImageCache].maxCacheSize = 1024 * 1024 * 250;
    
    return YES;
}


- (void)chooseRootViewController {
    
    [HZRootVCTool chooseRootViewController:self.window withBlock:^{
        //2.显示/更新版本
        //       [self setUpNewVersionWithRootVCtr];
        
        
        [HZAccountTool getAppStoreNewVersionWithSuccess:^(id responseObject) {
            
            NSLog(@"-----AppStoreNewVersion选择rootVC----%@", [responseObject getDictJsonStr]);
            
            if ([responseObject[@"state"] isEqual:@200]) {
                NSDictionary *resultDict = responseObject[@"results"];
                NSDictionary *versionDict = resultDict[@"version"];
                
                
                //
                NSString *forceUpVersion = versionDict[@"forceUpVersion"];
                
                NSString *message = versionDict[@"message"];
                NSString *newVersion = versionDict[@"newVersion"];
                
                
                NSLog(@"---message:%@---newVersion---%@", message, newVersion);
                message = [message stringByReplacingOccurrencesOfString:@"@" withString:@"\n"];
                message = [message stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                //        //
                //        newVersion = @"1.0.1";
                //        forceUpVersion = @"1.0.1";
                //        message = @"本次更新增加了专家钱包的提现功能，钱包里的钱能随时发起提现!钱包里的钱能随";
                // 1. 获取当前版本号
                //                NSString *currentVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
                //               currentVersion = @"1.0";
                //  判断是否更新
                if ([kCurrentBundleVersion compare:newVersion options:NSNumericSearch] == NSOrderedSame || [kCurrentBundleVersion compare:newVersion options:NSNumericSearch] == NSOrderedDescending) return;// currentVersion >= newVersion
                
                
                
                
                // 弹出提示框
                UIWindow *myWindow = [UIApplication sharedApplication].keyWindow;
                UIView *cover = [[UIView alloc] initWithFrame:myWindow.bounds];
                cover.backgroundColor = [UIColor blackColor];
                cover.alpha = 0.3;
                //        cover.hidden = YES;
                [myWindow addSubview:cover];
                self.cover = cover;
                
                HZAlertView *alertView = [[HZAlertView alloc] initWithFrame:CGRectMake(kP(2), kP(0), kP(556), kP(556))];
                alertView.center = myWindow.center;
                alertView.backgroundColor = [UIColor lightGrayColor];
                alertView.layer.cornerRadius = 10;
                alertView.clipsToBounds = YES;
                alertView.delegate = self;
                alertView.alertString = message;
                
                //               NSLog(@"---------currentVersion:%@-----forceUpVersion:%@", currentVersion, forceUpVersion);
                //               NSLog(@"---------BOOL------%@", [currentVersion compare:forceUpVersion options:NSNumericSearch] == NSOrderedAscending == YES ? @"YES":@"NO");
                alertView.isForceUp = [kCurrentBundleVersion compare:forceUpVersion options:NSNumericSearch] == NSOrderedAscending;
                [myWindow addSubview:alertView];
                self.alertView = alertView;
                
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"------AppStoreNewVersion---error:%@", error);
        }];
        
    }];
}
#pragma mark -- HZAlertViewDelegate
- (void)enterToAppStore {
    self.cover.hidden = NO;
    self.alertView.hidden = NO;
    [self loadAppStoreController];
}

- (void)cancelToDismissWithIsForceUp:(BOOL)isForceUp {
    self.cover.hidden = YES;
    self.alertView.hidden = YES;
    
    //
    if (!isForceUp) return;
    //
    exit(0);
}


#pragma mark -- 进入AppStore更新下载
- (void)loadAppStoreController {
    NSString *appStoreUrl = @"https://itunes.apple.com/us/app/ming-yi-lian-meng/id1173508999?l=zh&ls=1&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreUrl] options:@{} completionHandler:nil];
}


#pragma mark -- shareDelegate

+ (AppDelegate *)shareDelegate {
    return [UIApplication sharedApplication].delegate;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // -----容联云推送------
    NSString *token = [NSString stringWithFormat:@"%@", deviceToken];
    NSLog(@"My deviceToken is:%@", token);
    //这里应将device token发送到服务器端
    [[ECDevice sharedInstance] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

// 实现注册APNs失败接口(可选)
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
    
    NSLog(@"--------------------------------------------------------------------------------------------------------");
    
    NSString *error_str = [NSString stringWithFormat: @"%@", error];
    NSLog(@"Failed to get token, error:%@", error_str);
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    
    NSLog(@"----------userInfo-----------:%@", userInfo);
    /**
     ---------oooo----{
     aps =     {
     alert = "sl_19999999999:\U8fd9\U4e48";
     badge = 3;
     sound = default;
     };
     r = "sl_15068720779";
     s = "sl_19999999999";
     }
     */
    
    NSString *senderAccount = userInfo[@"s"];
    [self clickTheNotificationPushCustomVC:senderAccount];
    
    //
    if ([[UIDevice currentDevice].systemVersion floatValue]<10.0 || application.applicationState>0) {
        // 需要加的角标数量
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
    
}

//------------------------------------------------------------------------------------------------------


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
    //    NSLog(@"---------333333");
    //
    //    NSLog(@"--------退出应用------");
    
    //    NSNumber *oldVolume = [[NSUserDefaults standardUserDefaults] objectForKey:@"nowVolum"];
    //    [self setVolume:oldVolume isResignActived:YES];
//     [self setVolume:[NSNumber numberWithFloat:0.5f] isResignActived:YES];
    
    [[ECDevice sharedInstance] setAppleBadgeNumber:0 completion:^(ECError *error) {
        NSLog(@"--------将要进入后台---------------******");
    }];
    
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSLog(@"--------已经进入后台---------------******");
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    NSLog(@"---------将要进入前台----------------******");
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
    
//    //音量调节到最大
//    [self setVolume:[NSNumber numberWithFloat:1.0f] isResignActived:NO];
    
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    NSLog(@"---------已经进入前台----------------******");
    
    NSString *access_token = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"access_token"];
    
    BOOL isLogin = ![NSString isBlankString:access_token];
    if (isLogin) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kEnterForeground object:nil];
    }
    
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// IOS 10+  点击推送
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    NSString *senderAccount = userInfo[@"s"];
    // 来自推送
    [self clickTheNotificationPushCustomVC:senderAccount];
}
/**
 * 点击来自推送的跳转
 */
-(void)clickTheNotificationPushCustomVC:(NSString*)from{
    // 荣连云登录
    [self loginChatWithPushCustomVC:from];
}


- (void)loginChatWithPushCustomVC:(NSString*)from{
    
    ECLoginInfo * loginInfo = [[ECLoginInfo alloc] init];
    loginInfo.username = kMyChatId;
    loginInfo.appKey = kMyAppKey;
    loginInfo.appToken = kMyAppToken;
    loginInfo.authType = LoginAuthType_NormalAuth;//默认方式登录
    loginInfo.mode = LoginMode_InputPassword;
    
    NSLog(@"---------loginInfo------:%@", loginInfo);
    
    if (self.window.rootViewController.childViewControllers.count > 0) { // 后台
        [self pushToMsgVCtr:from];
        return;
    }
    
    // 容联云登录
    [[ECDevice sharedInstance] login:loginInfo completion:^(ECError *error) {
        if (error.errorCode == ECErrorType_NoError) {
            
            NSLog(@"---------容联云登录成功");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIMOnLine"];
            [self pushToMsgVCtr:from];
            //            [self.view makeToast:@"容联云登录成功" duration:1.5f position:CSToastPositionCenter];
        } else if (error.errorCode == 171141){
            
            NSLog(@"---------容联云登录失败");
            
//            NSLog(@"-容联云登录失败:%ld");
            //            [self.view makeToast:str duration:1.5f position:CSToastPositionCenter];
            
        }
    }];
    
}

- (void)pushToMsgVCtr:(NSString *)from {
    // 延时执行跳转
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([from isEqualToString:@"sl_wowjoy"]) {
            NSArray *arr =  [[HZMessageDB initMessageDB] getPushMessageArrWithOrderId:kOrganGraftingUserData];
            HZOrganGraftingViewController *vc = [[HZOrganGraftingViewController alloc]init];
            vc.dataArray = arr;
            UIViewController *rootVC = self.window.rootViewController;
            if ([rootVC isKindOfClass:[UITabBarController class]]) {
                UITabBarController *tabVC = (UITabBarController*)rootVC;
                [tabVC.selectedViewController pushViewController:vc animated:YES];
            }else{
                [rootVC.navigationController pushViewController:vc animated:YES];
            }
            
        }
    });
    
}
@end
