//
//  HZProfileViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZProfileViewController.h"

#import "HZProfileInforCell.h"
#import "HZAptitudeImageGetViewController.h"
#import "HZDoneOrderListViewController.h"
#import "HZMyWalletViewController.h"
#import "HZExpertTeamViewController.h"
#import "HZIntroTableViewController.h"
#import "HZAccountSettingViewController.h"
#import "HZUser.h"
#import "HZUserTool.h"
#import "HZWallet.h"
#import "MJRefresh.h"
#import "HZOrderHttpsTool.h"
#import "HZOrder.h"

//
#import "HZImageClipperViewController.h"
//
#import "HZNewsListViewController.h"
//
#import "HZRoomViewController.h"
#import "HZSettingViewController.h"
#import "HZWebViewController.h"
//
#import "UITabBar+Badge.h"

#import "HZPatientTool.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/PHPhotoLibrary.h>
#define kTestAccountOpenId @"00101b6842294a0d996760ffb893e774"
@interface HZProfileViewController () <UITableViewDataSource, UITableViewDelegate, HZProfileInforCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, HZImageClipperViewControllerDelegate, HZRoomViewControllerDelegate, HZSettingViewControllerDelegate>

//** <#注释#>*/
@property (nonatomic, strong) HZTableView *tableView;

//** <#注释#>*/
@property (nonatomic, strong) NSString *capital;

//
//** <#注释#>*/
@property (nonatomic, strong) UIView *attestationBgView;
//** <#注释#>*/
@property (nonatomic, strong) HZProfileInforCell *profileInforCell;
//
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) UILabel *myNewsLabel;

@property (nonatomic, strong) UIButton *enterVideoBtn;

//
@property (assign, nonatomic) AgoraRtcVideoProfile videoProfile;
@end

@implementation HZProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"个人中心";
//    [self.tabBarController.tabBar showBadgeOnItmIndex:3];
    //
    self.view.backgroundColor = [UIColor whiteColor];

    //
    HZTableView *tableView = [[HZTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - 44 - SafeAreaBottomHeight) style:UITableViewStyleGrouped];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorColor = [UIColor colorWithHexString:@"dfdfdf" alpha:1.0];
    tableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
   // tableView.separatorInset = UIEdgeInsetsMake(0, - 10, 0, 0);
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    
    //
    [self setMyAttestationView];

    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    //
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"hasImageUploadDone"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNewUserInfor) name:@"hasImageUploadDone" object:nil];
    
    //
    //
    //    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMyMessage:) name:KNOTIFICATION_onMesssageReceive object:nil];
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMyMessage:) name:KNOTIFICATION_onMesssageOffLineReceive object:nil];
    NSLog(@"------saw---------:%@", self.navigationController.childViewControllers[0]);
}

- (void)receiveMyMessage:(NSNotification *)notification { // 接受未读数，并存起来
    
      [self setUnreadMsgCount];
}

-(void)setUnreadMsgCount{
    
    NSArray *arr =[[[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"] allValues];
    for (int i = 0; i < arr.count; i++) {
        NSString *unReadNewsCountStr = arr[i];
        
        if (![unReadNewsCountStr isEqualToString:@""] && ![unReadNewsCountStr isEqualToString:@"0"]) {
            [HZChatHelper sharedInstance].hasUnReadMsg = YES;
            self.myNewsLabel.hidden = NO;
            [self.tabBarController.tabBar showBadgeOnItmIndex:3];
            
            break;
        }
        
        [HZChatHelper sharedInstance].hasUnReadMsg = NO;
        [self.tabBarController.tabBar hideBadgeOnItemIndex:3];
        self.myNewsLabel.hidden = YES;
    }

}
#pragma mark -- set

- (void)setIsNewsCome:(BOOL)isNewsCome {
    _isNewsCome = isNewsCome;
}

- (UILabel *)myNewsLabel {
    if (!_myNewsLabel) {
        _myNewsLabel = [UILabel new];
//       _myNewsLabel.text = @" ";
//        _myNewsLabel.font = [UIFont systemFontOfSize:kP(24)];
//        _myNewsLabel.textColor = [UIColor whiteColor];
//        _myNewsLabel.textAlignment = NSTextAlignmentCenter;
//        [_myNewsLabel sizeToFit];
        _myNewsLabel.width = kP(20);
        _myNewsLabel.height = kP(20);
        _myNewsLabel.x = self.view.width - _myNewsLabel.width - kP(100);
        _myNewsLabel.y = kP(100) * 0.5 - _myNewsLabel.height * 0.5;
        _myNewsLabel.backgroundColor = [UIColor redColor];
        _myNewsLabel.layer.cornerRadius = _myNewsLabel.height * 0.5;
        _myNewsLabel.clipsToBounds = YES;
    }
    return _myNewsLabel;
}
- (void)setMyAttestationView {
    
    [self.attestationBgView removeFromSuperview];
    
    UIView *attestationBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, self.view.width, kP(44))];
    attestationBgView.backgroundColor = [UIColor colorWithHexString:@"#F6B751" alpha:1.0];
    
    
    //
     UILabel *attestationStatusLabel = [UILabel new];
     attestationStatusLabel.font = [UIFont systemFontOfSize:kP(26)];
    
    NSLog(@"----authenticationState----%@", self.user.authenticationState);
    
    if ([self.user.authenticationState isEqualToString:@"AUTHENTICATED"] || [self.user.authenticationState isEqualToString:@"AUTHENTICATING"]) {
        attestationBgView.hidden = YES;
        self.tableView.y = 0;
    } else  {
        
        attestationBgView.hidden = NO;
        self.tableView.y = kP(20);
        if ([self.user.authenticationState isEqualToString:@"UNAUTHENTICATED"]) {// 未审核
            attestationStatusLabel.text = @"您的医生资质尚未认证,部分功能将受限制";
            [attestationStatusLabel sizeToFit];
            attestationStatusLabel.x = kP(62);
        } else if ([self.user.authenticationState isEqualToString:@"REJECTED"]){ // 审核失败
            attestationStatusLabel.text = @"您的医生资质认证失败";
            [attestationStatusLabel sizeToFit];
            attestationStatusLabel.x = attestationBgView.width * 0.5 - attestationStatusLabel.width * 0.5;
        } else {
            attestationBgView.hidden = YES;
            self.tableView.y = 0;
        }
        
        
    }
    

//
    
    attestationStatusLabel.textColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    attestationStatusLabel.y = CGRectGetMidY(attestationBgView.bounds) - attestationStatusLabel.height * 0.5;
    attestationStatusLabel.userInteractionEnabled = YES;
    //
    [attestationBgView addSubview:attestationStatusLabel];
    
    
    //
    UILabel *attestationLabel = [UILabel new];
    //    attestationLabel.text = @"您的医生资质尚未认证,无法进行远程接诊     立即认证>";
    if ([self.user.authenticationState isEqualToString:@"AUTHENTICATED"] || [self.user.authenticationState isEqualToString:@"AUTHENTICATING"]) {
        attestationLabel.hidden = YES;
    } else  {
        
        attestationLabel.hidden = NO;
        if ([self.user.authenticationState isEqualToString:@"UNAUTHENTICATED"]) {// 未审核
            attestationLabel.text = @"立即认证>";
        } else { // 审核失败
            attestationLabel.text = @"重新认证>";
        }
        
        
    }
    
    attestationLabel.font = [UIFont systemFontOfSize:kP(26)];
    [attestationLabel sizeToFit];
    attestationLabel.textColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    attestationLabel.y = CGRectGetMidY(attestationBgView.bounds) - attestationLabel.height * 0.5;
    attestationLabel.x = attestationBgView.width - attestationLabel.width - kP(22);
    attestationLabel.userInteractionEnabled = YES;
    //
    [attestationBgView addSubview:attestationLabel];
    
    
    //
    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToAttest:)];
    
    [attestationBgView addGestureRecognizer:tap];
    //
    [self.view addSubview:attestationBgView];
    self.attestationBgView = attestationBgView;
    
    //
    self.myNewsLabel.hidden = ![HZChatHelper sharedInstance].hasUnReadMsg;
    if (self.myNewsLabel.hidden) {
        [self.tabBarController.tabBar hideBadgeOnItemIndex:3];
        
    } else {
        [self.tabBarController.tabBar showBadgeOnItmIndex:3];
    }
}

- (void)tapToAttest:(UITapGestureRecognizer *)tap {
    NSLog(@"tap");
    
//    self.tableView.y = 0;
    
    HZAptitudeImageGetViewController *aptitudeImageGetVCtr = [HZAptitudeImageGetViewController new];
    aptitudeImageGetVCtr.user = self.user;
    [self.navigationController pushViewController:aptitudeImageGetVCtr animated:YES];
}

-(void)setUser:(HZUser *)user {
    _user = user;
}

//
- (void)getNewUserInfor {
    
    NSLog(@"----getNewUserInfor-----");
    
    
    self.view.userInteractionEnabled = NO;
    //
    [HZUserTool getUserWithSuccess:^(id responseObject) {
        
        NSLog(@"----responseObjec--%@", responseObject);
        if ([responseObject[@"state"] isEqual:@200]) {// 信息完善
            
            //            NSLog(@"----dddssssaaaaaa-----%@", responseObject);
            
//            ;
            
            
            NSDictionary *dataDict = responseObject;
            NSDictionary *resultDict = dataDict[@"results"];
            NSDictionary *userDict = resultDict[@"user"];
            //            HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
            
            
            //1
            [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hospital"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"subject"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profession"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.view.userInteractionEnabled = YES;
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                HZUser *user = [HZUser mj_objectWithKeyValues:[[NSUserDefaults standardUserDefaults] objectForKey:@"user"]];
                user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                self.user = user;
                [self setMyAttestationView];
                [self.tableView reloadData];
            });
        } else {
            //
            NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
            
            if (!userDict) {
                UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    self.view.userInteractionEnabled = YES;
                    
                    HZNavigationController *navCtr = [[HZNavigationController alloc] initWithRootViewController:[NSClassFromString(@"HZLoginViewController") new]];
                    // 选择页面
                    [UIApplication sharedApplication].keyWindow.rootViewController = navCtr;
                    
                    
                    
                }];
                if (kCurrentSystemVersion >= 9.0) {
                    [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
                }
                [alertCtr addAction:ensureAlertAction];
                [self presentViewController:alertCtr animated:YES completion:nil];
                return;
            }
            
            //
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hospital"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"subject"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profession"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.view.userInteractionEnabled = YES;
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                HZUser *user = [HZUser mj_objectWithKeyValues:[[NSUserDefaults standardUserDefaults] objectForKey:@"user"]];
                user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                self.user = user;
                [self setMyAttestationView];
                [self.tableView reloadData]; //hasImageUploadDone
            });
            
            
        }
        
        
        
        //        [self.activityIndicator stopAnimating];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        
        
        
        NSDictionary *userDict =  [[NSUserDefaults standardUserDefaults]objectForKey:@"user"];
        
        if (!userDict) {
            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取用户信息" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
                
            }];
            if (kCurrentSystemVersion >= 9.0) {
                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            }
            [alertCtr addAction:ensureAlertAction];
            [self presentViewController:alertCtr animated:YES completion:nil];
            
            //
            //        [self.activityIndicator stopAnimating];
            self.view.userInteractionEnabled = YES;
            return;
        }
        
        //
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hospital"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"subject"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profession"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.view.userInteractionEnabled = YES;
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            HZUser *user = [HZUser mj_objectWithKeyValues:[[NSUserDefaults standardUserDefaults] objectForKey:@"user"]];
            user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
            self.user = user;
            [self setMyAttestationView];
            [self.tableView reloadData];
        });
    }];

}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {

    
    [super viewWillAppear:animated];
    self.myNewsLabel.hidden = ![HZChatHelper sharedInstance].hasUnReadMsg;
    
    if (self.myNewsLabel.hidden) {
         [self.tabBarController.tabBar hideBadgeOnItemIndex:3];
        
    } else {
         [self.tabBarController.tabBar showBadgeOnItmIndex:3];
    }
    
}

//- (void)viewWillDisappear:(BOOL)animated {
//    
//}
- (void)setAvatarImage:(UIImage *)avatarImage {
    _avatarImage = avatarImage;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return [self.user.openid isEqualToString:kTestAccountOpenId] ? 1: 3;
    } else if (section == 2) {
        return 2;
    }
    
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSLog(@"------eeewwwww---%@", self.user.openid);
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:kP(36)];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    UIButton *accessBtn = [cell valueForKeyPath:@"_accessoryView"];
    [accessBtn setImage:[UIImage imageNamed:@"enter"] forState:UIControlStateNormal];
    if (indexPath.section == 0) {
        HZProfileInforCell *profileInforCell = [HZProfileInforCell cellWithTableView:tableView];
        profileInforCell.user = self.user;
        profileInforCell.avatarImage = self.avatarImage;
        profileInforCell.delegate = self;
        self.profileInforCell = profileInforCell;
        return self.profileInforCell;
    } else if (indexPath.section == 1) {
        
        NSLog(@"---qwer-%@", self.user.openid);
        if ([self.user.openid isEqualToString:kTestAccountOpenId]) {
            cell.textLabel.text = @"已接诊";
            cell.imageView.image = [UIImage imageNamed:@"orderList"];
        } else {
            
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"移植管理";
                    cell.imageView.image = [UIImage imageNamed:@"organGraftingInProfileList"];
                    [self addLineTo:cell.contentView];
                    break;
                case 1:
                    
                    cell.textLabel.text = @"我的钱包";
                    cell.imageView.image = [UIImage imageNamed:@"myWallet"];
                    [self addLineTo:cell.contentView];
                    
                    break;
                    
                case 2:
                    cell.textLabel.text = @"已接诊";
                    cell.imageView.image = [UIImage imageNamed:@"orderList"];
                   [self addLineTo:cell.contentView];
                    
                    break;
            }
        }
    } else if (indexPath.section == 2){
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"个人介绍";
                cell.imageView.image = [UIImage imageNamed:@"introduce"];
                break;
            case 1:
                cell.textLabel.text = @"我的消息";
                cell.imageView.image = [UIImage imageNamed:@"greenMsg.png"];
                [self addLineTo:cell.contentView];
                [cell.contentView addSubview:self.myNewsLabel];
                break;
        }
        
        
    } else if (indexPath.section == 3) {
        switch (indexPath.row) {
                case 0: {
                    cell.textLabel.text = @"帐号设置";
                    cell.imageView.image = [UIImage imageNamed:@"settion"];
                    //添加长按手势
                    UILongPressGestureRecognizer * longPressGesture =[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(settionLongPress:)];
                    
                    longPressGesture.minimumPressDuration = 5.0f;//设置长按 时间
                    [cell addGestureRecognizer:longPressGesture];
                    break;
                }
                
            case 1:
                cell.textLabel.text = @"联系我们";
                cell.imageView.image = [UIImage imageNamed:@"contact"];
                [self addLineTo:cell.contentView];
                break;
        }
        
    }
    return cell;
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return kP(184);
    }
    
    return kP(100);
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(0.01);
    }
    return kP(20);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kP(0.05);
}


#pragma mark -- 点击cell进入各个界面
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) return;
    

    switch (indexPath.section) {
        case 1: {
            
            if (indexPath.row == 0) {
                HZWebViewController *webVCtr = [HZWebViewController new];
//                NSString *urlParamStr = [NSString stringWithFormat:@"loginMobile?account=%@", self.user.phone];
//                webVCtr.loadUrlStr = kOrgTrantUrlStr(urlParamStr);
                webVCtr.loadUrlStr = @"https://www.baidu.com";
                webVCtr.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:webVCtr animated:YES];
            } else if (indexPath.row == 1) {
                
                HZMyWalletViewController *myWalletVCtr = [HZMyWalletViewController new];
                NSDictionary *walletDict = [[NSUserDefaults standardUserDefaults] valueForKey:@"walletDict"];
                
                if (walletDict) {
                    HZWallet *wallet = [HZWallet mj_objectWithKeyValues:walletDict];
                    wallet = [HZWallet mj_objectWithKeyValues:[wallet propertyValueDict]];
                    //
                    myWalletVCtr.walllet = wallet;
                }
                
                myWalletVCtr.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:myWalletVCtr animated:YES];
            } else if (indexPath.row == 2) {
                HZDoneOrderListViewController *doneOrderListVCtr = [HZDoneOrderListViewController new];
                doneOrderListVCtr.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:doneOrderListVCtr animated:YES];
                
            }
            
        }
            break;
            
        case 2: {
            if (indexPath.row == 0) {
                HZIntroTableViewController *myIntroVCtr = [[HZIntroTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
                myIntroVCtr.user = self.user;
                myIntroVCtr.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:myIntroVCtr animated:YES];
//
//                mySkillVCtr.user = self.user;
//                [self.navigationController pushViewController:mySkillVCtr animated:YES];
                NSLog(@"个人介绍！！！！！！！");
            } else if (indexPath.row == 1){
                
                
//                NSLog(@"-----xxxxx----%@", self.user);
//                
//                expertTeamVCtr.user = self.user;
//                [self.navigationController pushViewController:expertTeamVCtr animated:YES];
                
                NSLog(@"我的消息！！！！！！！！");


                HZNewsListViewController *newsListVCtr = [HZNewsListViewController new];
                self.isNewsCome = NO;
                newsListVCtr.hidesBottomBarWhenPushed = YES;
                newsListVCtr.user = self.user;
                [self.navigationController pushViewController:newsListVCtr animated:YES];
//                NSLog(@"我的消息！！！！！！！！");
                
            }
        }
            break;
        case 3: {
            if (indexPath.row == 0) {
                HZAccountSettingViewController *accountSettingVCtr = [HZAccountSettingViewController new];
                accountSettingVCtr.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:accountSettingVCtr animated:YES];
            } else {
                NSLog(@"sdfsdfsdfsdfsdfsdfsdfsd");
                [self contactUsAlert];
            }
        }
            break;
    }
    
}

#pragma mark -- contactUsAlert
- (void)contactUsAlert {
    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"联系客服" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:kP(32)],NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:attributedTitle.string message:@"客服时间:周一至周五(法定假日除外) 08:30—17:30" preferredStyle:UIAlertControllerStyleAlert];
    [alertCtr setValue:attributedTitle forKey:@"attributedTitle"];
    
    UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"立即拨号" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"---------拨打");
        
        UIWebView*callWebview = [[UIWebView alloc] init];
        NSURL *telURL = [NSURL URLWithString:@"tel:057156757315"];// 貌似tel:// 或者 tel: 都行
        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
        //记得添加到view上
        [self.view addSubview:callWebview];
        //
     
    }];
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        [cancelAlertAction setValue:[UIColor colorWithHexString:@"878787" alpha:1.0] forKey:@"titleTextColor"];
    }
    
    [alertCtr addAction:ensureAlertAction];
    [alertCtr addAction:cancelAlertAction];
    [self presentViewController:alertCtr animated:YES completion:nil];
    
}


#pragma mark -- HZProfileInforCellDelegate

- (void)tapToGetImage:(UITapGestureRecognizer *)tap {
    
//    NSLog(@"------sdffdsfsfds----");
    
    
    
    
    
    //
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 相机权限
        AVAuthorizationStatus cameraAuthStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (cameraAuthStatus == AVAuthorizationStatusRestricted || cameraAuthStatus ==AVAuthorizationStatusDenied) {
            void(^block)() = authorizationsSettingBlock();
            showMessage(@"您相机权限未打开,是否打开?", self, block);
        }
        //
        UIImagePickerController* imagePick = [[UIImagePickerController alloc] init];
        imagePick.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePick.delegate = self;
        [self presentViewController:imagePick animated:YES completion:nil];
    }];
    
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //相册权限
        PHAuthorizationStatus albumAuthStatus = [PHPhotoLibrary authorizationStatus];
        if (albumAuthStatus == PHAuthorizationStatusRestricted || albumAuthStatus == PHAuthorizationStatusDenied) {
            void(^block)() = authorizationsSettingBlock();
            showMessage(@"您相册权限未打开,是否打开?", self, block);
        }
        //
        UIImagePickerController* imagePick = [[UIImagePickerController alloc] init];
        imagePick.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePick.delegate = self;
        [self presentViewController:imagePick animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    
    [alertCtr addAction:cameraAction];
    [alertCtr addAction:libraryAction];
    [alertCtr addAction:cancelAction];
    
    [self presentViewController:alertCtr animated:YES completion:nil];
    
}

#pragma mark -- UIImagePickerControllerDelegate

// 选中
- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString*, id>*)info {
    
    
    //
    UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    
    HZImageClipperViewController *imageClipperVCtr = [[HZImageClipperViewController alloc] initWithImage:image delegate:self];
//    imageClipperVCtr.roundClip = YES;
    imageClipperVCtr.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:imageClipperVCtr animated:NO];
    

    //
    [picker dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark -- HZImageClipperViewControllerDelegate
- (void)imageClipperViewController:(HZImageClipperViewController *)imageClipper imageClipFinish:(UIImage *)clippedImage {
    // 存储imageData数据
//    NSData *avatarData = UIImageJPEGRepresentation(clippedImage, 1.0);
//    [[NSUserDefaults standardUserDefaults] setObject:avatarData forKey:@"avatarData"];
//    self.profileInforCell.avatarImage = clippedImage;
//    NSLog(@"-----clippedImage----%@", clippedImage);
    // 上传到服务器
    
//    NSData *avatarData = UIImageJPEGRepresentation(clippedImage, 1.0);
//    NSLog(@"-----www----%lu", (unsigned long)avatarData.length);
//    NSLog(@"-----ssaaaaaqq----%@",[clippedImage getCompressedImage]);
    
    NSData *imageData = [clippedImage getCompressedImageDataWithMaxSizeCount:12];
    UIImage *image = [UIImage imageWithData:imageData];
//    UIImage *image = [clippedImage getCompressedImage];
    
    [self.activityIndicator startAnimating];
    [HZUserTool upLoadMyAvatarWithAvatarImage:image userOpenid:self.user.openid progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"--进度----%f",1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
        // 回到主队列刷新UI,用户自定义的进度条
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.progressView.progress = 1.0 *
//            uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
//        });
    } success:^(id responseObject) {
        NSLog(@"上传成功 %@", responseObject);
        
        //
        self.profileInforCell.avatarImage = image;
        
        //
//        NSData *avatarData = UIImageJPEGRepresentation(image, 1.0);
        [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:self.user.openid];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.activityIndicator stopAnimating];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"上传失败 %@", error);
    }];
}


#pragma mark -- settionLongPress
-(void)settionLongPress:(UILongPressGestureRecognizer *)longRecognizer{
    
    
    
    

    if (longRecognizer.state == UIGestureRecognizerStateBegan) {
       
//        //成为第一响应者，需重写该方法
        [self becomeFirstResponder];
        
        // 建立视频通话
        HZRoomViewController *romeVCtr = [[HZRoomViewController alloc] init];
        romeVCtr.delegate = self;
        romeVCtr.videoProfile = AgoraRtc_VideoProfile_360P;
        romeVCtr.roomName = @"999999";
        romeVCtr.isComeFromPodfileVCtr = YES;
        romeVCtr.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:romeVCtr animated:YES];

//        CGPoint location = [longRecognizer locationInView:self.tableView];
//        NSIndexPath * indexPath = [self.tableView indexPathForRowAtPoint:location];
//        
//        NSLog(@"--------:%ld-----------%ld", indexPath.section, indexPath.row);
//
//        CGPoint location = [longRecognizer locationInView:self.tableView];
//        NSIndexPath * indexPath = [self.tableView indexPathForRowAtPoint:location];
//        
//        NSLog(@"--------:%ld", indexPath.row);
//        //可以得到此时你点击的哪一行
//        
//        //在此添加你想要完成的功能
    }
}

#pragma mark  实现成为第一响应者方法
-(BOOL)canBecomeFirstResponder{
    return YES;
}

#pragma mark -加自定义分割线
-(void)addLineTo:(UIView*)view{
    
    for (UIView *lastV in view.subviews) {
        if (lastV.tag == 66) {
            [lastV removeFromSuperview];
        }
    }
    
    UIView *lineV = [[UIView alloc] initWithFrame:CGRectMake(kP(40), 0, HZScreenW - kP(40), 1)];
    lineV.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE" alpha:1.0];
    lineV.tag = 66;
    [view addSubview:lineV];
}

@end
