//
//  HZProfileViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;
@interface HZProfileViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;

//** <#注释#>*/
@property (nonatomic, strong) UIImage *avatarImage;
@property (nonatomic, assign) BOOL isNewsCome;
@end
