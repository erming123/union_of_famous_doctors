//
//  HZProfessionCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/21.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZSubjectCell.h"
#import "HZSubject.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZSubjectCell ()
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) UILabel *professionLabel;
//@property (nonatomic, strong) <#Class#>;
@end
NS_ASSUME_NONNULL_END
@implementation HZSubjectCell

//
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    UIImageView *arrowImageView = [UIImageView new];
    arrowImageView.hidden = YES;
    [self addSubview:arrowImageView];
    self.arrowImageView = arrowImageView;
    
    UILabel *professionLabel = [UILabel new];
    [self addSubview:professionLabel];
    self.professionLabel = professionLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    CGFloat arrowImageViewW = kP(17);
    CGFloat arrowImageViewH = kP(20);
    CGFloat arrowImageViewX = kP(34);
    CGFloat arrowImageViewY = self.height * 0.5 - arrowImageViewH * 0.5;
    self.arrowImageView.frame = CGRectMake(arrowImageViewX, arrowImageViewY, arrowImageViewW, arrowImageViewH);
    
    //3.
    self.professionLabel.x = CGRectGetMaxX(self.arrowImageView.frame) + kP(30);
    self.professionLabel.y = self.height * 0.5 - self.professionLabel.height * 0.5;
}

- (void)setSubViewsContent {
    
    self.arrowImageView.image = [UIImage imageNamed:@"arrow_profession.png"];
    //
    self.professionLabel.text = self.subject.name;
    self.professionLabel.font = [UIFont systemFontOfSize:kP(32)];
    [self.professionLabel sizeToFit];
}

#pragma mark -- set
- (void)setSubject:(HZSubject *)subject {
    _subject = subject;
    self.arrowImageView.hidden = !subject.isSelect;
    self.professionLabel.textColor = subject.isSelect ? [UIColor colorWithHexString:@"34c5b2" alpha:1.0] : [UIColor blackColor];
}
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"professionCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

@end
