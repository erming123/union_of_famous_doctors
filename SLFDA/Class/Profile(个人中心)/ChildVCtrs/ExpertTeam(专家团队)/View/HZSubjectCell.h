//
//  HZSubjectCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/21.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZSubject;
@interface HZSubjectCell : UITableViewCell
//** <#注释#>*/
@property (nonatomic, strong) HZSubject *subject;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
