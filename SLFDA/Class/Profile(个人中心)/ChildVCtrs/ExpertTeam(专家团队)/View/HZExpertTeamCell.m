//
//  HZExpertTeamCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/21.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertTeamCell.h"
#import "HZUser.h"
#import "HZUserTool.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZExpertTeamCell ()
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *jobLabel;
@property (nonatomic, strong) UILabel *subjectLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *skillLabel;
//@property (nonatomic, copy) NSString *Id;
//** <#注释#>*/
//@property (nonatomic, strong) NSCache *cache;
@end
NS_ASSUME_NONNULL_END
@implementation HZExpertTeamCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        //
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    //1.
    UIImageView *avatarImageView = [UIImageView new];
    [self addSubview:avatarImageView];
    self.avatarImageView = avatarImageView;
    
    
    //
    //    UIImageView *avatarCoverImageView = [UIImageView new];
    //    [self.avatarImageView addSubview:avatarCoverImageView];
    //    self.avatarCoverImageView = avatarCoverImageView;
    
    //2.
    UILabel *nameLabel = [UILabel new];
    [self addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    
    //3.
    UILabel *subjectLabel = [UILabel new];
    //    subjectLabel.backgroundColor = [UIColor redColor];
    [self addSubview:subjectLabel];
    self.subjectLabel = subjectLabel;
    
    //4.
    UILabel *jobLabel = [UILabel new];
    //    jobLabel.backgroundColor = [UIColor greenColor];
    [self addSubview:jobLabel];
    self.jobLabel = jobLabel;
    
    
    //5.
    UILabel *priceLabel = [UILabel new];
    //    priceLabel.backgroundColor = [UIColor yellowColor];
    [self addSubview:priceLabel];
    self.priceLabel = priceLabel;
    
    
    //6.
    UILabel *skillLabel = [UILabel new];
    //    skillLabel.backgroundColor = [UIColor grayColor];
    [self addSubview:skillLabel];
    self.skillLabel = skillLabel;
}

// /v1/openapi/json/booking/order/addorupdate, /v1/openapi/json/his/patient/getByJid, /v1/openapi/json/doctor/esearch
- (void)layoutSubviews {
    [super layoutSubviews];
    
    //0.
    [self setSubViewsContent];
    
    //1.
    CGFloat avatarImageViewX = kP(56);
    CGFloat avatarImageViewY = kP(28);
    CGFloat avatarImageViewWH = kP(100);
    self.avatarImageView.frame = CGRectMake(avatarImageViewX, avatarImageViewY, avatarImageViewWH, avatarImageViewWH);
    self.avatarImageView.layer.cornerRadius = avatarImageViewWH * 0.5;
    self.avatarImageView.clipsToBounds = YES;
    
    //
    //    self.avatarCoverImageView.frame = self.avatarImageView.bounds;
    
    
    //2.
    self.nameLabel.y = CGRectGetMaxY(self.avatarImageView.frame) + kP(18);
    self.nameLabel.x = CGRectGetMidX(self.avatarImageView.frame) - self.nameLabel.width * 0.5;
    
    //    //3.---------------------------------
    self.priceLabel.x = self.width - self.priceLabel.width - kP(50);
    self.priceLabel.y = kP(20);
    
    
    //4. 科室
    self.subjectLabel.x = CGRectGetMaxX(self.avatarImageView.frame) + kP(52);
    self.subjectLabel.y = self.avatarImageView.y + kP(10);
    
    
    
    //5. 身份
    self.jobLabel.x = self.subjectLabel.width == 0 ? CGRectGetMaxX(self.subjectLabel.frame) : CGRectGetMaxX(self.subjectLabel.frame) + kP(22);
    self.jobLabel.y = self.subjectLabel.width == 0 ? self.subjectLabel.y : CGRectGetMaxY(self.subjectLabel.frame) - self.jobLabel.height;
    
    
    //6.
    CGFloat skillLabelX = self.subjectLabel.x;
    CGFloat skillLabelY = 0.f;
    if (self.subjectLabel.width == 0 && self.jobLabel.width == 0) {
        skillLabelY = self.subjectLabel.y;
    } else {
        skillLabelY = self.subjectLabel.width == 0 ? CGRectGetMaxY(self.jobLabel.frame) + kP(16) : CGRectGetMaxY(self.subjectLabel.frame) + kP(16);
    }
    
    CGFloat skillLabelW = kP(488);
    CGFloat skillLabelH = kP(102);
    self.skillLabel.frame = CGRectMake(skillLabelX, skillLabelY, skillLabelW, skillLabelH);
}

- (void)setSubViewsContent {
    //1.
    //
    NSString *urlStr = self.expert.facePhotoUrl;
    NSString *myCacheKey = self.expert.openid;
    UIImage *cachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:myCacheKey];
    
    if (cachedImage) {
        self.avatarImageView.image = cachedImage;
    } else {
        self.avatarImageView.image = [UIImage imageNamed:@"photo_profile.png"];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [HZUserTool getAvatarWithFaceUrl:urlStr success:^(UIImage *image) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    HZLog(@"-------cache----%@", image);
                    if (image) {
                        self.avatarImageView.image = image;
                        [[SDImageCache sharedImageCache] storeImage:image forKey:myCacheKey];
                    } else {
                        self.avatarImageView.image = [UIImage imageNamed:@"photo_profile.png"];
                    }
                    
                });
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                self.avatarImageView.image = [UIImage imageNamed:@"photo_profile.png"];
            }];
            
        });
        
    }
    
    
    //2.
    self.nameLabel.text = self.expert.userName;
    self.nameLabel.font = [UIFont systemFontOfSize:kP(32)];
    self.nameLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    [self.nameLabel sizeToFit];
    
    //3. 科室
    //    if (self.expert.departmentList.count == 0 ) {
    HZLog(@"---self.expert.departmentList------%@", self.expert.departmentList);
    HZLog(@"----555555-----%@", self.expert.departmentGeneralName);
    if ([NSString isBlankString:self.expert.departmentGeneralName]) {
        self.subjectLabel.text = @"";
    } else {
        //        self.subjectLabel.text = self.expert.departmentList[0][@"dictDepartmentName"];
        self.subjectLabel.text = self.expert.departmentGeneralName;
        //        HZLog(@"----sss-----%@", self.expert.departmentList[0]);
    }
    self.subjectLabel.font = [UIFont systemFontOfSize:kP(32)];
    self.subjectLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    [self.subjectLabel sizeToFit];
    
    
    //4. 职称
    if (self.expert.titleName.length == 0 || self.expert.titleName == nil) {
        self.expert.titleName = @"";
    } else {
        self.jobLabel.text = self.expert.titleName;
    }
    self.jobLabel.font = [UIFont systemFontOfSize:kP(32)];
    self.jobLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    [self.jobLabel sizeToFit];
    
    
    //5.
    NSString *priceStr = [self getPriceStrWithJobStr:self.expert.remoteExpertLevel];
    
    if (priceStr.length > 0) {
        NSMutableAttributedString *priceText = [[NSMutableAttributedString alloc] initWithString:priceStr];
        [priceText setAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#2DBED8" alpha:1.0] , NSFontAttributeName : [UIFont systemFontOfSize:kP(28)]} range:NSMakeRange(0, 1)];
        [priceText setAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#2DBED8" alpha:1.0] , NSFontAttributeName : [UIFont systemFontOfSize:kP(44)]} range:NSMakeRange(1, priceText.length - 1)];
        [self.priceLabel setAttributedText:priceText];
        [self.priceLabel sizeToFit];
    } else {
        self.priceLabel.width = kP(0);
    }
    
    
    //6.
    self.skillLabel.text = self.expert.speciality;
    self.skillLabel.font = [UIFont systemFontOfSize:kP(28)];
    self.skillLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
    self.skillLabel.numberOfLines = 0;
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"expertCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    
    return cell;
}

- (void)setExpert:(HZUser *)expert {
    _expert = expert;
}

- (void)setPriceList:(NSArray *)priceList {
    _priceList = priceList;
}

//- (void)setUserAvatar:(UIImage *)userAvatar {
//    _userAvatar = userAvatar;
//}


#pragma mark -- getPriceStrWithJobStr

- (NSString *)getPriceStrWithJobStr:(NSString *)remoteExpertLevel {
    
    NSString *priceStr = @"";
    for (NSDictionary *dict in self.priceList) {
        
        if ([remoteExpertLevel isEqualToString:dict[@"levelTypeCode"]]) {
            priceStr = dict[@"totalMoney"];
            priceStr = [NSString stringWithFormat:@"¥%@", priceStr];
        }
        
    }
    return priceStr;
}
@end
