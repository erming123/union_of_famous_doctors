//
//  HZExpertIntroCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HZExpertIntrolCellDelegate <NSObject>

- (void)setCellHeightWithIsIntrolUnFold:(BOOL)isUnFold cellHeight:(CGFloat)cellHeight;

@end
@interface HZExpertIntroCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) id<HZExpertIntrolCellDelegate>delegate;
@property (nonatomic, copy) NSString *expertIntroStr;
@end
