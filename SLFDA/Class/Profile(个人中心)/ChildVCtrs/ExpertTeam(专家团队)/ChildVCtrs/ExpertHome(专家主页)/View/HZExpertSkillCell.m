//
//  HZExpertSkillCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertSkillCell.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZExpertSkillCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIImageView *skillImageView;
@property (nonatomic, strong) UILabel *skillLabel;
@property (nonatomic, strong) UIButton *unFoldBtn;
@property (nonatomic, strong) UILabel *unFoldLabel;
@property (nonatomic, assign) CGFloat cellHeight;
@end
NS_ASSUME_NONNULL_END
@implementation HZExpertSkillCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"f3f3f3" alpha:1.0];
    }
    
    return self;
}

- (void)setUpSubViews {
    
    //1.
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    self.bgView = bgView;
    
    
    //2.
    UIImageView *skillImageView = [UIImageView new];
//    skillImageView.backgroundColor = [UIColor greenColor];
    [bgView addSubview:skillImageView];
    self.skillImageView = skillImageView;
    
    
    //3.
    UILabel *skillLabel = [UILabel new];
//    skillLabel.backgroundColor = [UIColor yellowColor];
    [bgView addSubview:skillLabel];
    self.skillLabel = skillLabel;
    
    
    //4.
    UIButton *unFoldBtn = [UIButton new];
//    unFoldBtn.backgroundColor = [UIColor redColor];
    [unFoldBtn addTarget:self action:@selector(unFoldBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:unFoldBtn];
    self.unFoldBtn = unFoldBtn;
    
    
    //5.
    UILabel *unFoldLabel = [UILabel new];
//    unFoldLabel.backgroundColor = [UIColor yellowColor];
    [bgView addSubview:unFoldLabel];
    self.unFoldLabel = unFoldLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    
    
    CGFloat skillImageViewX = kP(28);
    CGFloat skillImageViewY = kP(28);
    CGFloat skillImageViewWH = kP(40);
    self.skillImageView.frame = CGRectMake(skillImageViewX, skillImageViewY, skillImageViewWH, skillImageViewWH);
    
    self.skillLabel.x = CGRectGetMaxX(self.skillImageView.frame) + kP(14);
    self.skillLabel.y = CGRectGetMidY(self.skillImageView.frame) - self.skillLabel.height * 0.5;
    
    //
    CGFloat unFoldBtnW = kP(100);
    CGFloat unFoldBtnH = kP(100);
    CGFloat unFoldBtnX = self.width - kP(42) - unFoldBtnW;
    CGFloat unFoldBtnY = CGRectGetMidY(self.skillLabel.frame) - unFoldBtnH * 0.5;
    
    self.unFoldBtn.frame = CGRectMake(unFoldBtnX, unFoldBtnY, unFoldBtnW, unFoldBtnH);
    
    //
    self.unFoldLabel.numberOfLines = 0;//5,r
    CGFloat unFoldLabelX = self.skillLabel.x;
    CGFloat unFoldLabelY = CGRectGetMaxY(self.skillLabel.frame) + kP(26);
    
    HZLog(@"----unFoldLabelY-----%lf", unFoldLabelY);
    //
    CGFloat unFoldLabelW = self.width - 2 * unFoldLabelX + kP(25);
    CGFloat unFoldLabelH = [self.unFoldLabel sizeThatFits:CGSizeMake(unFoldLabelW, MAXFLOAT)].height;
    
    //
    HZLog(@"-----wwww----%lf", unFoldLabelH);
    if (unFoldLabelH > kP(32) * 5) {
        HZLog(@"---------wwwweeeeeeooooo大于");
        
        self.unFoldBtn.hidden = NO;
        
        unFoldLabelH = self.unFoldBtn.selected ? unFoldLabelH :  kP(32) * 5;
        self.unFoldLabel.numberOfLines = self.unFoldBtn.selected ? 0 : 4;
        
        unFoldLabelY = self.unFoldBtn.selected ? unFoldLabelY : unFoldLabelY - (kP(4));
        

    } else {
        HZLog(@"---------wwwweeeeeeooooo小于");
        self.unFoldBtn.hidden = YES;
        unFoldLabelY = unFoldLabelY - kP(5);
        self.unFoldLabel.numberOfLines = 0;
    }
    //
    self.unFoldLabel.frame = CGRectMake(unFoldLabelX, unFoldLabelY, unFoldLabelW, unFoldLabelH);
    
    
    //2.
    CGFloat bgViewX = kP(0);
    CGFloat bgViewY = kP(0);
    CGFloat bgViewW = self.width;
    CGFloat bgViewH = CGRectGetMaxY(self.unFoldLabel.frame) + kP(26);
    
//    if (self.unFoldBtn.hidden) {
//        bgViewH = kP(32) * 5 + kP(26);
//    }
    self.bgView.frame = CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH);
    
    //
    self.cellHeight = CGRectGetMaxY(self.bgView.frame) + kP(20);
}

- (void)setSubViewsContent {
    
    
    self.skillImageView.image = [UIImage imageNamed:@"expertSkilled"];
    self.skillLabel.text = @"擅长";
    self.skillLabel.font = [UIFont systemFontOfSize:kP(32)];
    [self.skillLabel sizeToFit];
    [self.unFoldBtn setImage:[UIImage imageNamed:@"subject_down.png"] forState:UIControlStateNormal];
    self.unFoldLabel.text = self.expertSkillStr;
    self.unFoldLabel.font = [UIFont systemFontOfSize:kP(32)];
    
//    HZLog(@"---------333333333");
}

#pragma mark -- set
- (void)setExpertSkillStr:(NSString *)expertSkillStr {
    _expertSkillStr = expertSkillStr;
}
#pragma mark -- unFoldBtnClick
- (void)unFoldBtnClick:(UIButton *)unFoldBtn {
    
    unFoldBtn.selected = !unFoldBtn.selected;
    [self layoutSubviews];
    HZLog(@"---------BOOL------%@", unFoldBtn.selected == YES ? @"YES":@"NO");
    
    HZLog(@"----66666self.cellHeight-----%lf", self.cellHeight);
    
    unFoldBtn.transform = unFoldBtn.selected ? CGAffineTransformMakeRotation(M_PI) : CGAffineTransformMakeRotation(0);
    if (self.delegate && [self.delegate respondsToSelector:@selector(setCellHeightWithIsSkillUnFold: cellHeight:)]) {
        [self.delegate setCellHeightWithIsSkillUnFold:unFoldBtn.selected cellHeight:self.cellHeight];
    }

}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"SkillCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
