//
//  HZExpertHomeViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;
@interface HZExpertHomeViewController : UIViewController
//** <#注释#>*/
@property (nonatomic, strong) HZUser *expert;
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
//** <#注释#>*/
@property (nonatomic, strong) UIImage *expertAvatar;
@property (nonatomic, copy) NSArray *priceArr;
@end
