//
//  HZDoneOrderListViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZDoneOrderListViewController.h"
#import "HZDoneOrderCell.h"
#import "MJRefresh.h"
#import "HZOrder.h"
#import "HZLocalOrderDetailViewController.h"
#import "HZRemoteOrderDetailViewController.h"
#import "HZOrderHttpsTool.h"
#import "HZUserTool.h"
@interface HZDoneOrderListViewController ()<UITableViewDataSource, UITableViewDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *tableView;
//** <#注释#>*/
@property (nonatomic, strong) UIImageView *noOrderImageView;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
//@property (nonatomic, strong) UILabel *errorLabel;
@end

@implementation HZDoneOrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.title = @"已接诊";
    HZTableView *tableView = [[HZTableView alloc] initWithFrame:CGRectMake(0, kP(14), self.view.width, self.view.height) style:UITableViewStylePlain];
    
    if (kCurrentSystemVersion >= 11.0) {
        tableView.y = 64;
        tableView.height -= 64;
    }
    
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
//    tableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    //3. 没有订单的时候
    UIImageView *noOrderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(390), kP(434))];
    noOrderImageView.image = [UIImage imageNamed:@"noDoneOrder"];
    noOrderImageView.center = self.view.center;
    noOrderImageView.y = kP(600);
    noOrderImageView.hidden = YES;
    [self.view addSubview:noOrderImageView];
    self.noOrderImageView = noOrderImageView;
    noOrderImageView.center = self.view.center;
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = noOrderImageView.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
//    //3. 网络出现问题的时候
//    UILabel *errorLabel = [[UILabel alloc] init];
//   // errorLabel.text = @"暂未获取到数据";
//    [errorLabel sizeToFit];
//    errorLabel.center = self.view.center;
//    errorLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
//    errorLabel.hidden = YES;
//    [self.view addSubview:errorLabel];
//    self.errorLabel = errorLabel;


    
    //刷新
    [self pushToRefresh];
    
    // 获取数据
    [self getDoneorderList];
    
    
    
}


- (void)pushToRefresh {
    
    //下拉刷新
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        
        [self refreshOrders];
    }];
    self.tableView.mj_header = refreshHeader;
//    //下拉刷新
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//
//            [self refreshOrders];
//            // 结束刷新
//            [self.tableView.mj_header endRefreshing];
//
//    }];
//
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.tableView.mj_header.automaticallyChangeAlpha = YES;
    
//    // 上拉刷新
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        // 结束刷新
//        [self refreshOrders];
//        [self.tableView.mj_footer endRefreshing];
//    }];

}

- (NSArray *)doneOrderList {
    if (!_doneOrderList) {
        _doneOrderList = [NSArray array];
    }
    return _doneOrderList;
}

#pragma mark -- 获取已完成订单列表
- (void)getDoneorderList {
    
//    self.errorLabel.hidden = YES;
    NSArray *resultArr = [[NSUserDefaults standardUserDefaults] objectForKey:@"doneOrderList"];
    if (resultArr.count != 0) {
        self.doneOrderList = [HZOrder mj_objectArrayWithKeyValuesArray:resultArr];
        [self.tableView reloadData];
        [self.activityIndicator stopAnimating];
        self.noOrderImageView.hidden = self.doneOrderList.count != 0;
    } else {
        [self.activityIndicator startAnimating];
        

        [HZOrderHttpsTool getOrdersWithPages:1 countPerPage:100 stateCode:@"008" success:^(id responseObject) {
            
            
            NSLog(@"----responseObjectqwe-----%@", responseObject);
            NSDictionary *resDict = responseObject;
            NSDictionary *resultDict = resDict[@"results"];
            NSArray *resultArr = resultDict[@"BookingOrderList"];
            self.doneOrderList = [HZOrder mj_objectArrayWithKeyValuesArray:resultArr];
            [[NSUserDefaults standardUserDefaults] setObject:resultArr forKey:@"doneOrderList"];
            
            [self.tableView reloadData];
            self.noOrderImageView.hidden = self.doneOrderList.count != 0;
            [self.activityIndicator stopAnimating];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //        [self.activityIndicator stopAnimating];
            //        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取订单信息" preferredStyle:UIAlertControllerStyleAlert];
            //        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
            //        [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
            //        [alertCtr addAction:ensureAlertAction];
            //        [self presentViewController:alertCtr animated:YES completion:nil];
//            self.errorLabel.hidden = NO;
        }];
        
    }
    
}

#pragma mark -- 进入刷新
- (void)viewWillAppear:(BOOL)animated {
    [self refreshOrders];
}



#pragma mark -- 刷新数据
- (void)refreshOrders{
//    self.errorLabel.hidden = YES;
    [self.activityIndicator startAnimating];
    //
    [HZOrderHttpsTool getOrdersWithPages:1 countPerPage:100 stateCode:@"008" success:^(id responseObject) {
        
        
        NSDictionary *resDict = responseObject;
        NSDictionary *resultDict = resDict[@"results"];
        NSArray *resultArr = resultDict[@"BookingOrderList"];
        self.doneOrderList = [HZOrder mj_objectArrayWithKeyValuesArray:resultArr];
        NSLog(@"----responseObject123-----%@", resultArr);
        [[NSUserDefaults standardUserDefaults] setObject:resultArr forKey:@"doneOrderList"];
        
        [self.tableView reloadData];
        self.noOrderImageView.hidden = self.doneOrderList.count != 0;
        [self.activityIndicator stopAnimating];
        [self.tableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //        [self.activityIndicator stopAnimating];
        //        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取订单信息" preferredStyle:UIAlertControllerStyleAlert];
        //        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        //        [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
        //        [alertCtr addAction:ensureAlertAction];
        //        [self presentViewController:alertCtr animated:YES completion:nil];
//        self.errorLabel.hidden = NO;
        self.noOrderImageView.hidden = NO;
        [self.activityIndicator stopAnimating];
        [self.tableView.mj_header endRefreshing];
    }];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.doneOrderList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HZDoneOrderCell *doneOrderCell = [HZDoneOrderCell cellWithTableView:tableView];
    doneOrderCell.doneOrder = self.doneOrderList[indexPath.row];
    return doneOrderCell;
}


#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(282);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HZOrder *postOrder = self.doneOrderList[indexPath.row];
    if ([postOrder.orderType isEqualToString:@"01"]) { // 本地
        HZLocalOrderDetailViewController *lcalOrderDetailVCtr = [HZLocalOrderDetailViewController new];
        lcalOrderDetailVCtr.localOrder = self.doneOrderList[indexPath.row];
        [self.navigationController pushViewController:lcalOrderDetailVCtr animated:YES];
        
    } else {// 远程
        HZRemoteOrderDetailViewController *remoteOrderDetailVCtr = [HZRemoteOrderDetailViewController new];
        remoteOrderDetailVCtr.remoteOrder = self.doneOrderList[indexPath.row];
        [self.navigationController pushViewController:remoteOrderDetailVCtr animated:YES];
        
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
