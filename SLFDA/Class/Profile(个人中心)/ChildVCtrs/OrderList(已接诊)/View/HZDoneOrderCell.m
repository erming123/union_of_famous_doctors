//
//  HZDoneOrderCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZDoneOrderCell.h"
#import "HZOrder.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZDoneOrderCell ()
@property (nonatomic, strong) UIView *bgView;
//
@property (nonatomic, strong) UIImageView *IDImageView;
@property (nonatomic, strong) UILabel *IDLabel;
@property (nonatomic, strong) UILabel *doneTimeLabel;
//
@property (nonatomic, strong) UIView *sepLine;
//
@property (nonatomic, strong) UILabel *illNameLabel;
@property (nonatomic, strong) UILabel *patientInforLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZDoneOrderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


- (void)setUpSubViews {
    
    
    //
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor colorWithHexString:@"#FAFBFC" alpha:1.0];
    bgView.layer.cornerRadius = kP(16);
    bgView.layer.masksToBounds = NO;
    bgView.layer.shadowColor = [UIColor grayColor].CGColor;
    bgView.layer.shadowOffset = CGSizeMake(kP(2), kP(2));
    bgView.layer.shadowOpacity = 0.3;
    [self.contentView addSubview:bgView];
    self.bgView = bgView;
    
    //
    UIImageView *IDImageView = [[UIImageView alloc] init];
//        IDImageView.backgroundColor = [UIColor redColor];
    IDImageView.image = [UIImage imageNamed:@"remoteOrder"];
    [self.bgView addSubview:IDImageView];
    self.IDImageView = IDImageView;
    
    //
    UILabel *IDLabel = [[UILabel alloc] init];
//        IDLabel.backgroundColor = [UIColor greenColor];
    [IDLabel setTextFont:kP(28) textColor:@"#888888" alpha:1.0];
    [self.bgView addSubview:IDLabel];
    self.IDLabel = IDLabel;
    
    //
    UILabel *doneTimeLabel = [[UILabel alloc] init];
//    doneTimeLabel.backgroundColor = [UIColor redColor];
    [doneTimeLabel setTextFont:kP(30) textColor:@"#9B9B9B" alpha:1.0];
    doneTimeLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:doneTimeLabel];
    self.doneTimeLabel = doneTimeLabel;
    
    
    //
    UIView *sepLine = [[UIView alloc] init];
    sepLine.backgroundColor = [UIColor lightGrayColor];
    sepLine.alpha = 0.5;
    [self.bgView addSubview:sepLine];
    self.sepLine = sepLine;
    
    
    //
    UILabel *illNameLabel = [UILabel new];
//        illNameLabel.backgroundColor = [UIColor greenColor];
    [illNameLabel setTextFont:kP(34) textColor:@"#333333" alpha:1.0];
    [self.bgView addSubview:illNameLabel];
    self.illNameLabel = illNameLabel;
    
    
    
    
    //
    UILabel *patientInforLabel = [UILabel new];
//        patientInforLabel.backgroundColor = [UIColor greenColor];
    [patientInforLabel setTextFont:kP(30) textColor:@"#666666" alpha:1.0];
    [self.bgView addSubview:patientInforLabel];
    self.patientInforLabel = patientInforLabel;
    
    
    
}


- (void)layoutSubviews {
    [super layoutSubviews];
    [self setUpSubViewsContent];
    
    //
    CGFloat bgViewX = kP(30);
    CGFloat bgViewY = kP(20);
    CGFloat bgViewW = self.width - 2 * bgViewX;
    CGFloat bgViewH = self.height - bgViewY * 2;
    self.bgView.frame = CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH);
    
    //
    CGFloat IDImageViewX = kP(30);
    CGFloat IDImageViewY = kP(24);
    CGFloat IDImageViewWH = kP(44);
    self.IDImageView.frame = CGRectMake(IDImageViewX, IDImageViewY, IDImageViewWH, IDImageViewWH);
    
    
    
    CGSize IDSize = [self.IDLabel.text getTextSizeWithMaxWidth:kP(400) textFontSize:kP(28)];
    CGFloat IDLabelW = IDSize.width;
    CGFloat IDLabelH = IDSize.height;
    CGFloat IDLabelX = self.IDImageView.right + kP(20);
    CGFloat IDLabelY = self.IDImageView.centerY - IDLabelH * 0.5;
    self.IDLabel.frame = CGRectMake(IDLabelX, IDLabelY, IDLabelW, IDLabelH);
    
    
    CGSize doneTimeSize = [self.doneTimeLabel.text getTextSizeWithMaxWidth:kP(400) textFontSize:kP(30)];
    CGFloat doneTimeLabelW = doneTimeSize.width;
    CGFloat doneTimeLabelH = doneTimeSize.height;
    CGFloat doneTimeLabelX = self.bgView.width - doneTimeLabelW - kP(30);
    CGFloat doneTimeLabelY = self.IDLabel.centerY - doneTimeLabelH * 0.5;
    self.doneTimeLabel.frame = CGRectMake(doneTimeLabelX, doneTimeLabelY, doneTimeLabelW, doneTimeLabelH);
    
    
    //
    CGFloat sepLineX = 0;
    CGFloat sepLineY = self.IDImageView.bottom + kP(24);
    CGFloat sepLineW = bgViewW;
    CGFloat sepLineH = kP(1);
    self.sepLine.frame = CGRectMake(sepLineX, sepLineY, sepLineW, sepLineH);
    
    
    //
//    self.unFoldLabel.numberOfLines = 0;//5,r
    CGFloat illNameLabelX = kP(94);
    CGFloat illNameLabelY = self.sepLine.bottom + kP(24);
    //
    CGFloat illNameLabelW = kP(520);
    CGFloat illNameLabelH = [self.illNameLabel sizeThatFits:CGSizeMake(illNameLabelW, MAXFLOAT)].height;

    //
    self.illNameLabel.frame = CGRectMake(illNameLabelX, illNameLabelY, illNameLabelW, illNameLabelH);
    
    
    CGSize patientInforSize = [self.patientInforLabel.text getTextSizeWithMaxWidth:kP(500) textFontSize:kP(30)];
    CGFloat patientInforLabelW = patientInforSize.width;
    CGFloat patientInforLabelH = patientInforSize.height;
    CGFloat patientInforLabelX = illNameLabelX;
    CGFloat patientInforLabelY = self.illNameLabel.bottom + kP(18);
    self.patientInforLabel.frame = CGRectMake(patientInforLabelX, patientInforLabelY, patientInforLabelW, patientInforLabelH);
   
    // UI
    [self.illNameLabel sizeToFit];
    self.illNameLabel.width =  self.illNameLabel.width > kP(500)?kP(500):self.illNameLabel.width;
    
    [self.doneTimeLabel sizeToFit];
    self.doneTimeLabel.width = self.doneTimeLabel.width > kP(400)?kP(400):self.doneTimeLabel.width;
    
    [self.IDLabel sizeToFit];
    
    [self.patientInforLabel sizeToFit];
    self.patientInforLabel.width = self.patientInforLabel.width > kP(500)?kP(500):self.patientInforLabel.width;
    
}

- (void)setUpSubViewsContent {
 
    self.IDImageView.image = [self.doneOrder.orderType isEqualToString:@"01"] ? [UIImage imageNamed:@"localOrder"] : [UIImage imageNamed:@"remoteOrder"];
    self.IDLabel.text = [NSString stringWithFormat:@"%@%@", self.doneOrder.hospitalName, [self.doneOrder.orderType isEqualToString:@"01"] ? @"陪诊":@"门诊"];

    
    if ([NSString isBlankString:self.doneOrder.treatmentDateTime]) {
        self.doneTimeLabel.text = @"时间暂无";
    } else {
        //
        NSString *timeStr = [[self.doneOrder.treatmentDateTime componentsSeparatedByString:@" "] firstObject];
        NSArray *subTimeStrArr = [timeStr componentsSeparatedByString:@"-"];
        NSString *yearStr = [NSString stringWithFormat:@"%@年", subTimeStrArr[0]];
        NSString *monStr = [NSString stringWithFormat:@"%@月", subTimeStrArr[1]];
        NSString *dayStr = [NSString stringWithFormat:@"%@日", subTimeStrArr[2]];
        self.doneTimeLabel.text = [NSString stringWithFormat:@"%@%@%@", yearStr, monStr, dayStr];
    }
    
    //
    self.illNameLabel.text = self.doneOrder.icd10Value;
    self.patientInforLabel.text = [NSString stringWithFormat:@"%@  %@  %@岁", self.doneOrder.outpatientName, self.doneOrder.outpatientGenderValue, self.doneOrder.outpatientAge];
}

- (void)setDoneOrder:(HZOrder *)doneOrder {
    _doneOrder = [HZOrder mj_objectWithKeyValues:[doneOrder propertyValueDict]];
}


+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
