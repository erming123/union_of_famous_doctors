//
//  HZWallet.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/25.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZJsonModel.h"
@interface HZWallet : HZJsonModel
@property (nonatomic, copy) NSString *balance;// 总的额度
@property (nonatomic, copy) NSString *incId;
@property (nonatomic, copy) NSString *openid;
@property (nonatomic, copy) NSString *walletId;

@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *bankOwnerName;

@property (nonatomic, copy) NSString *depositBank;// 开户行
@property (nonatomic, copy) NSString *bankAccount;// 银行卡号
@property (nonatomic, copy) NSString *bankAddress;// 开户地址

+ (instancetype)shareInstance;
@end
