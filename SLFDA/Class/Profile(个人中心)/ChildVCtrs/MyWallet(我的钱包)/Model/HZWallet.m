//
//  HZWallet.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/25.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZWallet.h"

@implementation HZWallet
+ (instancetype)shareInstance {
    static HZWallet *wallet;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        wallet = [[self alloc] init];
    });
    
    return wallet;
}


- (NSString *)description {
    return [NSString stringWithFormat:@"---------balance:%@", self.balance];
}
@end
