//
//  HZCapital.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZJsonModel.h"
@interface HZCapital : HZJsonModel
@property (nonatomic, copy) NSString *outpatient_name;// 患者姓名
@property (nonatomic, copy) NSString *expert_openid;// 专家id
@property (nonatomic, copy) NSString *local_doctor_openid;// 本地医生id
@property (nonatomic, copy) NSString *treatment_date_time;// 治疗时间
@property (nonatomic, copy) NSString *sum_of_money;// 单条资金
@property (nonatomic, copy) NSString *bill_type;// 资金类型
@property (nonatomic, copy) NSString *cash_state_code;//
@property (nonatomic, copy) NSString *cash_state_value;//
@property (nonatomic, copy) NSString *cash_time;// 提现时间
@property (nonatomic, copy) NSString *order_type;//01->
@end
