//
//  HZWalletTopView.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/7.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZWalletTopView.h"
#import "HZWallet.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZWalletTopView ()
@property (nonatomic, strong) UIView *backBgView;
@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) UIButton *bankCardManageBtn;
@property (nonatomic, strong) UILabel *moneyLeftLabel;
@property (nonatomic, strong) UILabel *moneyLeftContentLabel;
@property (nonatomic, strong) UIButton *autoDepositBtn;
@end
NS_ASSUME_NONNULL_END
@implementation HZWalletTopView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews {

    UIView *backBgView = [UIView new];
//    backBgView.backgroundColor = [UIColor greenColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeDelegateRebackToLastView:)];
    [backBgView addGestureRecognizer:tap];
    [self addSubview:backBgView];
    self.backBgView = backBgView;
    
    UIImageView *backImageView = [UIImageView new];
    backImageView.image = [UIImage imageNamed:@"back_white"];
//    backImageView.backgroundColor = [UIColor redColor];
    [self.backBgView addSubview:backImageView];
    self.backImageView = backImageView;

    
    UIButton *bankCardManageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bankCardManageBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateNormal];
    [bankCardManageBtn setTitle:@"银行卡管理" forState:UIControlStateNormal];
    bankCardManageBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [bankCardManageBtn addTarget:self action:@selector(bankCardManageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:bankCardManageBtn];
    self.bankCardManageBtn = bankCardManageBtn;
    
    UILabel *moneyLeftLabel = [[UILabel alloc] init];
    moneyLeftLabel.text = @"钱包余额(元)";
    [moneyLeftLabel setTextFont:kP(28) textColor:@"#000000" alpha:0.24];
    [self addSubview:moneyLeftLabel];
    self.moneyLeftLabel = moneyLeftLabel;
    
    UILabel *moneyLeftContentLabel = [[UILabel alloc] init];
    [moneyLeftContentLabel setTextFont:kP(110) textColor:@"#FFFFFF" alpha:1.0];
    moneyLeftContentLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:moneyLeftContentLabel];
    self.moneyLeftContentLabel = moneyLeftContentLabel;
    
    UIButton *autoDepositBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [autoDepositBtn setTitle:@"自动提现" forState:UIControlStateNormal];
    autoDepositBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [autoDepositBtn setImage:[UIImage imageNamed:@"refer"] forState:UIControlStateNormal];
    [autoDepositBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateNormal];
    [autoDepositBtn addTarget:self action:@selector(autoDepositBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:autoDepositBtn];
    self.autoDepositBtn = autoDepositBtn;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    NSLog(@"-----wallet---iiiii----%@", _wallet.balance);
    self.moneyLeftContentLabel.text = ![NSString isBlankString:_wallet.balance] ? _wallet.balance : @"0";
    
    
    CGFloat backBgViewX = kP(0);
    CGFloat backBgViewY = HZScreenH == 812.0 ? kP(60) : kP(40);
    CGFloat backBgViewW = kP(100);
    CGFloat backBgViewH = kP(100);
    self.backBgView.frame = CGRectMake(backBgViewX, backBgViewY, backBgViewW, backBgViewH);
    
    CGFloat backImageViewX = kP(25);
    CGFloat backImageViewY = kP(25);
    CGFloat backImageViewW = kP(18);
    CGFloat backImageViewH = kP(30);
    self.backImageView.frame = CGRectMake(backImageViewX, backImageViewY, backImageViewW, backImageViewH);
    
    
    CGFloat bankCardManageBtnW = kP(180);
    CGFloat bankCardManageBtnH = kP(38);
    CGFloat bankCardManageBtnX = self.width - bankCardManageBtnW - kP(28);
//    CGFloat bankCardManageBtnY = backBgViewY + backImageViewY;
    CGFloat bankCardManageBtnY = [self convertRect:self.backImageView.frame fromView:self.backBgView].origin.y;
    self.bankCardManageBtn.frame = CGRectMake(bankCardManageBtnX, bankCardManageBtnY, bankCardManageBtnW, bankCardManageBtnH);

    CGSize moneyLeftSize = [self.moneyLeftLabel.text getTextSizeWithMaxWidth:self.width textFontSize:kP(30)];
    CGFloat moneyLeftLabelW = moneyLeftSize.width;
    CGFloat moneyLeftLabelH = moneyLeftSize.height;
    CGFloat moneyLeftLabelX = self.width * 0.5 - moneyLeftLabelW * 0.5;
    CGFloat moneyLeftLabelY = self.bankCardManageBtn.bottom + kP(28);
    self.moneyLeftLabel.frame = CGRectMake(moneyLeftLabelX, moneyLeftLabelY, moneyLeftLabelW, moneyLeftLabelH);

    
//    CGSize moneyLeftContentSize = [_wallet.balance getTextSizeWithMaxWidth:self.width textFontSize:kP(115)];
    CGFloat moneyLeftContentLabelW = self.width;
    CGFloat moneyLeftContentLabelH = kP(115);
    CGFloat moneyLeftContentLabelX = self.width * 0.5 - moneyLeftContentLabelW * 0.5;
    CGFloat moneyLeftContentLabelY = self.moneyLeftLabel.bottom + kP(20);
    self.moneyLeftContentLabel.frame = CGRectMake(moneyLeftContentLabelX, moneyLeftContentLabelY, moneyLeftContentLabelW, moneyLeftContentLabelH);


    CGFloat autoDepositBtnW = kP(180);
    CGFloat autoDepositBtnH = kP(50);
    CGFloat autoDepositBtnX = self.width - autoDepositBtnW - kP(28);
    CGFloat autoDepositBtnY = self.height - autoDepositBtnH - kP(18);
    self.autoDepositBtn.frame = CGRectMake(autoDepositBtnX, autoDepositBtnY, autoDepositBtnW, autoDepositBtnH);
}


-(void)setWallet:(HZWallet *)wallet {
    _wallet = wallet;
    self.moneyLeftContentLabel.text = _wallet.balance;
}

//1.
- (void)makeDelegateRebackToLastView:(UIButton *)backBtn {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(rebackToLastView)]) {
        [self.delegate rebackToLastView];
    }
}

//2.
- (void)bankCardManageBtnClick:(UIButton *)bankCardManageBtnClick {
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterToBankCardManageView)]) {
        [self.delegate enterToBankCardManageView];
    }
}

//3.
- (void)autoDepositBtnClick:(UIButton *)autoDepositBtn {
    if (self.delegate && [self.delegate respondsToSelector:@selector(popDepositAlertView)]) {
        [self.delegate popDepositAlertView];
    }
}

@end
