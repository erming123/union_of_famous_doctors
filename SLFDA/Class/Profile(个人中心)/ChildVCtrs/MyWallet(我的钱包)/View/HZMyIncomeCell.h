//
//  HZMyIncomeCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/7.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZCapital;
@interface HZMyIncomeCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//** <#注释#>*/
@property (nonatomic, strong) HZCapital *captital;
@end
