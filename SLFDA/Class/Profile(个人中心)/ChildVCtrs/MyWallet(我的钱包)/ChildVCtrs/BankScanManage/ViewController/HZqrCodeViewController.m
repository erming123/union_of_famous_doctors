//
//  QRCodeViewController.m
//  chartTest
//
//  Created by coder_xu on 2017/8/14.
//  Copyright © 2017年 coder_xu. All rights reserved.
//

#import "HZqrCodeViewController.h"
#import "HZBarScanView.h"

#import "ZBarSDK.h"
#import "HZBankScanView.h"

@interface HZqrCodeViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic,weak) HZBarScanView *barCodeScanV;

@property (nonatomic,weak) UIButton *NavBarRightBtn;

@property (nonatomic,assign) scanType type;

@end

@implementation HZqrCodeViewController


-(instancetype)initWithScanType:(scanType)type{
    if (self = [super init]) {
        
        _type = type;
    }
    
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavBarItem];

    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    self.title = @"扫一扫";

    switch (self.type) {
        case scanTypeBarCode:
             [self scanBarCode];
            break;
        case scanTypeBankCard:
            [self scanBankCard2];
            break;
            
        default:
            break;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 扫描条码
-(void)scanBarCode{
        //暂不支持 从相册里面选 暂时关闭
        self.NavBarRightBtn.hidden = YES;
        HZBarScanView *scanV = [[HZBarScanView alloc]initWithFrame:self.view.bounds];
        scanV.scanWidth = 200;
        scanV.tips = @"条形码放入框内，即可自动扫描";
        [scanV showInView:self.view];
        self.barCodeScanV = scanV;
        WeakSelf
       scanV.successScanBlock = ^(NSString *info) {
        HZLog(@"扫描内容===%@",info);
        if (info) {
            //返回上层
            [weakSelf.navigationController popViewControllerAnimated:YES];
             weakSelf.scanResultInfoBlock(info, nil);
        }
    };
    
    
}



-(void)scanBankCard2{
    
    self.NavBarRightBtn.hidden = YES;
    HZBankScanView *preV = [[HZBankScanView alloc]initWithWithFrame:self.view.bounds ScanSize:CGSizeMake(225, 359) Andtips:@"请将扫描线对准银行卡号并对齐左右"];
    [preV startRunningCamera];
    [self.view addSubview:preV];
    WeakSelf
    preV.successScanBlock = ^(NSString *bankName, NSString *bankNumber) {
        
        //对number处理一下 不能有空格
  //正则  \\s+
        NSString *regx = @"\\s+";
        NSRegularExpression *regExp = [[NSRegularExpression alloc] initWithPattern:regx
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
        // 替换匹配的字符串
        bankNumber = [regExp stringByReplacingMatchesInString:bankNumber
                                                     options:NSMatchingReportProgress
                                                       range:NSMakeRange(0, bankNumber.length)
                                                withTemplate:@""];
        [weakSelf.navigationController popViewControllerAnimated:YES];
        weakSelf.scanResultInfoBlock(nil, @{@"bankName":bankName,@"bankNumber":bankNumber});
    };
    
}


-(void)setNavBarItem{
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"相册" forState:UIControlStateNormal];
    rightBtn.frame = CGRectMake(0, 0, 40, 40);
    [rightBtn addTarget:self action:@selector(jumpTheAlbum) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    UIBarButtonItem *barItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = barItem;
    self.NavBarRightBtn = rightBtn;
    
}

-(void)jumpTheAlbum{
    
    
    ZBarReaderController *imagePicker = [ZBarReaderController new];
    imagePicker.showsHelpOnFail = NO; // 禁止显示读取失败页面
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];

    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    
    for(symbol in results) {
        
        break;
    }
    
    //因为有可能是二维码(过滤)
    //正则匹配(^[0-9]*$) 全是数字
    if (![self TheResultIsBarCodeOrQRcode:symbol.data]) {
        //从相册里面选取的不是条码
        return;
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //二维码字符串
        NSString *QRCodeString =  symbol.data;
        // 触发回调
        NSLog(@"内容是==%@",QRCodeString);
        [self.navigationController popViewControllerAnimated:YES];
        self.scanResultInfoBlock(QRCodeString, nil);
    }];
    
}
// 相册读取失败回调
-(void)readerControllerDidFailToRead:(ZBarReaderController *)reader withRetry:(BOOL)retry{
    
    if (retry) { //retry == 1 选择图片为非二维码。
        [reader dismissViewControllerAnimated:YES completion:^{
            
           self.scanResultInfoBlock(nil, nil);
            
        }];
        
    }
    return;
    
}

- (BOOL)TheResultIsBarCodeOrQRcode:(id)code{
    
    NSString *zhenZeStr = @"^[0-9]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",zhenZeStr];
    return [predicate evaluateWithObject:code];
    
}



-(void)dealloc{
    
    NSLog(@"扫码控制器销毁了");
    
}

@end
