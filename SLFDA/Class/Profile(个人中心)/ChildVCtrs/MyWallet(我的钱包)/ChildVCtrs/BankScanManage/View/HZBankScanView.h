//
//  HZBankScanView.h
//  chartTest
//
//  Created by coder_xu on 2017/8/18.
//  Copyright © 2017年 coder_xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZBankScanView : UIView


- (void)startRunningCamera;

- (void)stopRunningCamera;

-(instancetype)initWithWithFrame:(CGRect)frame ScanSize:(CGSize)size Andtips:(NSString*)tips;

@property (nonatomic,copy) void(^successScanBlock)(NSString *bankName,NSString*bankNumber);

@end
