//
//  HZSelBankNameCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/12.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZSelBankNameCell : UITableViewCell
@property (nonatomic, strong) UILabel *selBankNameLabel;
@property (nonatomic, copy) NSString *selBankName;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
