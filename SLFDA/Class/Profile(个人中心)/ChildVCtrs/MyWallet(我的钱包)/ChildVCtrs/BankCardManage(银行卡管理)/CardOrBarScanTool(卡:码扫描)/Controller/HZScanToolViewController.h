//
//  QRCodeViewController.h
//  chartTest
//
//  Created by coder_xu on 2017/8/14.
//  Copyright © 2017年 coder_xu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    scanTypeBarCode,
    scanTypeBankCard
} scanType;

@interface HZScanToolViewController : HZViewController

@property (nonatomic,copy) void(^scanResultInfoBlock)(NSString*barCode,NSDictionary *bankInfo);

// 请使用此方法初始化
-(instancetype)initWithScanType:(scanType)type;

@end
