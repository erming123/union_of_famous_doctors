//
//  YHScanView.h
//  chartTest
//
//  Created by coder_xu on 2017/8/14.
//  Copyright © 2017年 coder_xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZBarScanView : UIView

@property (nonatomic,assign) int scanWidth;

- (void)showInView:(UIView*)view; //开始扫描
- (void)stopRunning; //停止扫描

@property (nonatomic,strong) NSString *tips;


@property (nonatomic,copy) void(^successScanBlock)(NSString *info);


@end
