//
//  BankCardScanView.m
//  chartTest
//
//  Created by coder_xu on 2017/8/15.
//  Copyright © 2017年 coder_xu. All rights reserved.
//

#import "HZBankCardScanView.h"
#import <AVFoundation/AVFoundation.h>
#import "RectManager.h"
#import "BankCardSearch.h"
#import "exbankcard.h"
#import "UIImage+Extend.h"

#import <Photos/Photos.h>



#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ScreenHeight [UIScreen mainScreen].bounds.size.height


@interface HZBankCardScanView ()<AVCaptureVideoDataOutputSampleBufferDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic,assign) CGSize scanSize;
@property (nonatomic,assign) NSString *tips;

@property (nonatomic,strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer* preView;

@property (nonatomic, strong) AVCaptureDeviceInput *currentDeviceInput;
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoOutput;

@property (nonatomic, strong) dispatch_queue_t sessionQueue;

@property (nonatomic,weak) UIImageView *centerView;
@property (nonatomic,weak) UIImageView *line;

@property (nonatomic,strong) UIView *toolsView;

@property (nonatomic,strong) AVCaptureDevice *device;

@property (nonatomic,assign) BOOL isInProcessing;

@property (nonatomic,assign) BOOL isHasResult;


@end

@implementation HZBankCardScanView

-(instancetype)initWithWithFrame:(CGRect)frame ScanSize:(CGSize)size Andtips:(NSString*)tips{
    
    if (self = [super initWithFrame:frame]) {
        
        self.scanSize = size;
        self.tips = tips;
        [self commonInit];
        
        _preView.frame = self.bounds;
    }
    
    return self;
}

- (void)commonInit {
    
    [self configureCamera];
   // [self requestPermission];
    _preView = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
    _preView.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.layer addSublayer:_preView];
    
    //矩形镂空的部分
    CGRect ovalRect = CGRectMake((ScreenWidth-self.scanSize.width)*0.5+kP(18),(ScreenHeight - self.scanSize.height)*0.5 + kP(26),self.scanSize.width,self.scanSize.height);
    
    UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:0];
    UIBezierPath * cropPath = [UIBezierPath bezierPathWithRoundedRect:ovalRect cornerRadius:0];
    [path appendPath:cropPath];
    
    CAShapeLayer * layer = [[CAShapeLayer alloc] init];
    layer.fillColor = [UIColor colorWithRed:.0 green:.0 blue:.0 alpha:0.5].CGColor;
    //填充规则
    layer.fillRule=kCAFillRuleEvenOdd;
    layer.path = path.CGPath;
    [self.layer addSublayer:layer];
    
    //tips
    UILabel *label = [UILabel new];
    label.text = self.tips;
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:14];
    [label sizeToFit];
    label.transform = CGAffineTransformMakeRotation(M_PI/2);
    label.frame = CGRectMake(ovalRect.origin.x -kP(30) - label.frame.size.width, ovalRect.origin.y + ovalRect.size.height*0.5 -label.frame.size.height*0.5, label.frame.size.width, label.frame.size.height);
    label.textColor = [UIColor whiteColor];
    [self addSubview:label];
    
    
    [self setScanViewImage:ovalRect];
    
    //[self setUpToolsViewSubView];
    
}


//-(void)setUpToolsViewSubView{
//    
//    self.toolsView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.centerView.frame)+6, ScreenWidth, self.bounds.size.height - CGRectGetMaxY(self.centerView.frame) -6)];
//    
//    UIButton *lights = [UIButton buttonWithType:UIButtonTypeCustom];
//    UIButton *takePhoto = [UIButton buttonWithType:UIButtonTypeCustom];
//    UIButton *albumBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    [lights setImage:[UIImage imageNamed:@"dengpao"] forState:UIControlStateNormal];
//    [lights setImage:[UIImage imageNamed:@"dengpao_dianjihou"] forState:UIControlStateSelected];
//    [takePhoto setImage:[UIImage imageNamed:@"paizhaoanniu"] forState:UIControlStateNormal];
//    [takePhoto setImage:[UIImage imageNamed:@"paizhaoanniu_dianjihou"] forState:UIControlStateHighlighted];
//    [albumBtn setImage:[UIImage imageNamed:@"tuku"] forState:UIControlStateNormal];
//    
//    //布局
//    [takePhoto sizeToFit];
//    takePhoto.frame = CGRectMake((self.toolsView.frame.size.width - takePhoto.frame.size.width)*0.5,(self.toolsView.frame.size.height - takePhoto.frame.size.height)*0.5 , takePhoto.frame.size.width, takePhoto.frame.size.height);
//    //灯光
//    //先旋转
//    lights.transform = CGAffineTransformMakeRotation(M_PI/2);
//    lights.frame = CGRectMake(self.toolsView.frame.size.width -50, (self.toolsView.frame.size.height - 40)*0.5, 40, 40);
//    //相册
//    albumBtn.transform = CGAffineTransformMakeRotation(M_PI/2);
//    albumBtn.frame = CGRectMake(10, (self.toolsView.frame.size.height - 40)*0.5, 40, 40);
//    [self.toolsView addSubview:albumBtn];
//    [self.toolsView addSubview:takePhoto];
//    [self.toolsView addSubview:lights];
//    
//    self.toolsView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
//   
//    
//    
//    lights.tag = 1;
//    takePhoto.tag = 2;
//    albumBtn.tag = 3;
//    
//    //Action
//    [lights addTarget:self action:@selector(toolsViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [albumBtn addTarget:self action:@selector(toolsViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [takePhoto addTarget:self action:@selector(toolsViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    
//    
//    // [self addSubview:self.toolsView];
//    
//}


-(void)setScanViewImage:(CGRect)frame{
    
    
    UIImageView *centerView = [[UIImageView alloc]init];
    //扫描框图片的拉伸，拉伸中间一块区域
    UIImage *scanImage = [UIImage imageNamed:@"QR"];
    CGFloat top = 34*0.5-1; // 顶端盖高度
    CGFloat bottom = top ; // 底端盖高度
    CGFloat left = 34*0.5-1; // 左端盖宽度
    CGFloat right = left; // 右端盖宽度
    UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
    scanImage = [scanImage resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
    
    centerView.image = scanImage;
    centerView.backgroundColor = [UIColor clearColor];
    centerView.frame = frame;
    [self addSubview:centerView];
    _centerView = centerView;
    
    
    //扫描线
    UIImageView *line = [[UIImageView alloc]init];
    line.image = [UIImage imageNamed:@"scanline"];
    line.contentMode = UIViewContentModeScaleAspectFill;
    line.backgroundColor = [UIColor clearColor];
    line.clipsToBounds = YES;
    line.frame = CGRectMake(CGRectGetMinX(frame)+ frame.size.width * 21 / 54, CGRectGetMinY(frame)+2, 3, frame.size.height - 4);
    [self addSubview:line];
    _line = line;
    
}
/**
 *  添加扫码动画
 */
- (void)addAnimation{
    
    self.line.hidden = NO;
    CABasicAnimation *animation = [self showAnimationWithTime:0.7 rep:OPEN_MAX];
    [self.line.layer addAnimation:animation forKey:@"LineAnimation"];
    NSLog(@"添加动画");
}

- (CABasicAnimation *)showAnimationWithTime:(float)time rep:(int)rep{
    
    CABasicAnimation *animationMove = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [animationMove setFromValue:@1];
    [animationMove setToValue:@0];
    animationMove.duration = time;
    animationMove.repeatCount  = rep;
    animationMove.removedOnCompletion = NO;
    return animationMove;
    
    
}

/**
 *  去除扫码动画
 */
- (void)removeAnimation{
    
    [self.line.layer removeAnimationForKey:@"LineAnimation"];
    self.line.hidden = YES;
    NSLog(@"移除动画");
}


#pragma mark -
#pragma mark--初始化相机--
- (void)configureCamera {
    
    self.session = [[AVCaptureSession alloc] init];
    
    if ([self.session canSetSessionPreset:AVCaptureSessionPreset1280x720]) {
#if !TARGET_IPHONE_SIMULATOR
        self.session.sessionPreset = AVCaptureSessionPreset1280x720;
#endif
    }
    
    self.sessionQueue = dispatch_queue_create("com.baidu.passport.cameraSessionQueue", DISPATCH_QUEUE_SERIAL);
    
    [self executeSessionConfiguration:^{
        [self configureDeviceInput];
        [self configureVideoOutput];
        
    }];
    
}
- (void)executeSessionConfiguration:(void(^)())action {
    
    if (!self.sessionQueue) {
        return;
    }
    
    dispatch_async(self.sessionQueue, ^{
        
        [self.session beginConfiguration];
        action();
#if !TARGET_IPHONE_SIMULATOR
        [self.session commitConfiguration];
#endif
        
        
    });
    
}

- (void)configureDeviceInput {
    
    NSError *error = nil;
    self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    self.currentDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:_device error:&error];
    if (!error && self.currentDeviceInput) {
        if ([self.session canAddInput:self.currentDeviceInput]) {
            [self.session addInput:self.currentDeviceInput];
        }
    } else {
        NSLog(@"error = %@",error);
    }
    
}

//- (void)configurevideoDataOutput {
//    
//    _videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
//    _videoDataOutput.alwaysDiscardsLateVideoFrames = YES;
//    _videoDataOutput.videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange], (id)kCVPixelBufferPixelFormatTypeKey, nil];
//    if ([self.session canAddOutput:self.videoOutput]) {
//        [self.session addOutput:self.videoOutput];
//    }
//    
//}

- (void)configureVideoOutput {
    
    self.videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    self.videoOutput.alwaysDiscardsLateVideoFrames = YES;
    self.videoOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    [self.videoOutput setSampleBufferDelegate:self queue:dispatch_queue_create("com.baidu.passport.cameraSampleBuffer", DISPATCH_QUEUE_SERIAL)];
    
    if ([self.session canAddOutput:self.videoOutput]) {
        [self.session addOutput:self.videoOutput];
    }
    
}

- (void)requestPermission {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        AVAuthorizationStatus status =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        switch (status) {
            case AVAuthorizationStatusNotDetermined: {
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if (granted) {
                        
                        NSLog(@"已经授权");
                        
                    } else {
                        
                        NSLog(@"拒绝访问");
                    }
                }];
                break;
            }
            case AVAuthorizationStatusRestricted:
            case AVAuthorizationStatusDenied: {
                NSLog(@"未授权访问相机");
                break;
            }
            case AVAuthorizationStatusAuthorized: {
                break;
            }
        }
        
    }
}


- (void)executeAction:(void(^)())action {
    
    if (!self.sessionQueue) {
        return;
    }
    
    dispatch_async(self.sessionQueue, ^{
        action();
    });
}


#pragma mark - Public methods
- (void)startRunningCamera {
    
    [self addAnimation];
    if (!self.session) {
        return;
    }
    
    [self executeAction:^{
#if !TARGET_IPHONE_SIMULATOR
        
        [self.session startRunning];
        
#endif
        
    }];
}
- (void)stopRunningCamera {
    
    [self removeAnimation];
    if (!self.session) {
        return;
    }
    
    [self executeAction:^{
#if !TARGET_IPHONE_SIMULATOR
        
        [self.session stopRunning];
#endif
        
    }];
    
    
    
}

//- (void)captureStillImageWithHandler:(void (^)(NSData *))handler afterDelay:(NSTimeInterval)delay {
//    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), self.sessionQueue, ^{
//        
//        AVCaptureConnection *connection = [self.stillCameraOutput connectionWithMediaType:AVMediaTypeVideo];
//        [self.stillCameraOutput captureStillImageAsynchronouslyFromConnection:connection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
//            
//            if (error == nil) {
//                NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
//                UIImage *img = [UIImage imageWithData:imageData];
//                UIImage *newImage = [self cropTheImageWithFrame:self.centerView.frame WithImage:img];
//                NSData *data = UIImageJPEGRepresentation(newImage, 1.0);
//                //关闭手电筒
//                [self turnOffTheLight];
////                self.takePictureBlock(data,newImage);
//                handler(data);
//                
//            } else {
//                handler(nil);
//            }
//            
//        }];
//        
//    });
//    
//}

#pragma mark -
#pragma mark--btnClick--
//-(void)toolsViewBtnClick:(UIButton*)btn{
//    
//    if (btn.tag == 1) {
//        
//        btn.selected?[self turnOffTheLight]:[self turnOnTheLight];
//        
//    }else if (btn.tag == 2){
//        //拍照传递出去(如果手电打开,关闭)
//        [self captureStillImageWithHandler:^(NSData *imageData) {
//            
//            
//        } afterDelay:0];
//        
//    }else if (btn.tag == 3){
//        //进入相册(如果手电打开,关闭)
//        [self turnOffTheLight];
//        [self openTheAlbumController];
//        
//    }
//    
//}

//打开手电筒
-(void)turnOnTheLight{
    UIButton *lightBtn = [self.toolsView viewWithTag:1];
    lightBtn.selected = YES;
    //有灯
    if ([self.device hasTorch]) {
        [self.device lockForConfiguration:nil];
        [self.device setTorchMode:AVCaptureTorchModeOn];
    }
    
}
//关闭手电筒
-(void)turnOffTheLight{
    UIButton *lightBtn = [self.toolsView viewWithTag:1];
    
    //有灯
    if ([self.device hasTorch] && lightBtn.selected) {
        [self.device setTorchMode:AVCaptureTorchModeOff];
        [self.device unlockForConfiguration];
        lightBtn.selected = NO;
        
    }
    
}


#pragma mark - Memory manage
- (void)dealloc {
    
#if !TARGET_IPHONE_SIMULATOR
    [_session stopRunning];
#endif
    [_session removeInput:_currentDeviceInput];
    [_session removeOutput:_videoOutput];
   // [_session removeOutput:_stillCameraOutput];
    //    NSLog(@"AipCameraController dealloc");
}


#pragma mark -
#pragma mark--AVCaptureVideoDataOutputSampleBufferDelegate--
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    CVPixelBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
//        if(self.isInProcessing == NO) {
//            [self.receiveSubject sendNext:(__bridge id)(imageBuffer)];
//        
//    }
    
    [self doRec:imageBuffer];
    
    
    
    
}

- (void)doRec:(CVImageBufferRef)imageBuffer {
    @synchronized(self) {
        self.isInProcessing = YES;
        if (self.isHasResult) return;
        CVBufferRetain(imageBuffer);
        if(CVPixelBufferLockBaseAddress(imageBuffer, 0) == kCVReturnSuccess) {
         
           [self parseBankImageBuffer:imageBuffer];
              
        }
        CVBufferRelease(imageBuffer);
    }
}

- (void)parseBankImageBuffer:(CVImageBufferRef)imageBuffer {
    size_t width_t= CVPixelBufferGetWidth(imageBuffer);
    size_t height_t = CVPixelBufferGetHeight(imageBuffer);
    CVPlanarPixelBufferInfo_YCbCrBiPlanar *planar = CVPixelBufferGetBaseAddress(imageBuffer);
    size_t offset = NSSwapBigIntToHost(planar->componentInfoY.offset);
    
    unsigned char* baseAddress = (unsigned char *)CVPixelBufferGetBaseAddress(imageBuffer);
    unsigned char* pixelAddress = baseAddress + offset;
    
    size_t cbCrOffset = NSSwapBigIntToHost(planar->componentInfoCbCr.offset);
    uint8_t *cbCrBuffer = baseAddress + cbCrOffset;
    
    CGSize size = CGSizeMake(width_t, height_t);
    CGRect effectRect = [RectManager getEffectImageRect:size];
    CGRect rect = [RectManager getGuideFrame:effectRect];
    
    
    
    int width = ceilf(width_t);
    int height = ceilf(height_t);
    
    
  NSLog(@"effectRect = %@-----rect = %@------size = %@-----width = %i height = %i",NSStringFromCGRect(effectRect),NSStringFromCGRect(rect),NSStringFromCGSize(size),width,height);
    
    unsigned char result [512];
    
    // 模拟器和真机分开
    int resultLen = TARGET_IPHONE_SIMULATOR ? 0 : BankCardNV12(result, 512, pixelAddress, cbCrBuffer, width, height, rect.origin.x, rect.origin.y, rect.origin.x+rect.size.width, rect.origin.y+rect.size.height);
    
    if(resultLen > 0) {
        
       // NSLog(@"识别正确的frame== %@----rect=%@",NSStringFromCGRect(effectRect),NSStringFromCGRect(rect));
        
        int charCount = [RectManager docode:result len:resultLen];
        if(charCount > 0) {
         //   CGRect subRect = [RectManager getCorpCardRect:width height:height guideRect:rect charCount:charCount];
            self.isHasResult = YES;
          //  UIImage *image = [UIImage getImageStream:imageBuffer];
           // __block UIImage *subImg = [UIImage getSubImage:subRect inImage:image];
            char *numbers = [RectManager getNumbers];
            NSString *numberStr = [NSString stringWithCString:numbers encoding:NSASCIIStringEncoding];
            NSString *numberFinal = [self numberHandle:numberStr];
            //bankIO 库 bin查询
            //NSString *bank = [BankCardSearch getBankNameByBin:numbers count:charCount];
            //另一个bin库查询
            NSString *bank = [BankCardSearch getBankName:numberFinal];
             // 数据不全,没找到 在找一次
            if ([bank containsString:@"没找到"]) {
                bank = [BankCardSearch getBankNameByBin:numbers count:charCount];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                
                self.successScanBlock(bank, numberFinal);
            
            });
        }
    }
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    self.isInProcessing = NO;
}



 // 处理空格
- (NSString*)numberHandle:(NSString*)bankNumber{
    
    
    //对number处理一下 不能有空格
    //正则  \\s+
    NSString *regx = @"\\s+";
    NSRegularExpression *regExp = [[NSRegularExpression alloc] initWithPattern:regx
                                                                       options:NSRegularExpressionCaseInsensitive
                                                                         error:nil];
    // 替换匹配的字符串
    bankNumber = [regExp stringByReplacingMatchesInString:bankNumber
                                                  options:NSMatchingReportProgress
                                                    range:NSMakeRange(0, bankNumber.length)
                                             withTemplate:@""];
   
    return bankNumber;
    
}






@end
