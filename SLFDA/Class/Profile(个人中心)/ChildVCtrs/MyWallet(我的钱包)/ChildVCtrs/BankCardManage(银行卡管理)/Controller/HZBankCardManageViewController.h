//
//  HZBankCardManageViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/7.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZWallet, HZBankInfor;
@interface HZBankCardManageViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZWallet *wallet;
//** <#注释#>*/
@property (nonatomic, strong) HZBankInfor *bankInfor;
@end
