//
//  HZMyWalletViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZWallet;
@interface HZMyWalletViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZWallet *walllet;
@end
