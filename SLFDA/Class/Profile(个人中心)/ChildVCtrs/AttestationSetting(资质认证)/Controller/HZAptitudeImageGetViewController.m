//
//  HZAptitudeImageGetViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZAptitudeImageGetViewController.h"
#import "HZAttestImageGetCell.h"
#import "HZImageAttestationUploadViewController.h"
//
#import "HZUser.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/PHPhotoLibrary.h>
@interface HZAptitudeImageGetViewController () <UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *tableView;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *imageGetBtn;
@end

@implementation HZAptitudeImageGetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"资质提交";
    //
    [self setMyTableView];
    
    //
    [self setMyImageGetBtn];
}

#pragma mark -- setMyTableView

- (void)setMyTableView {
    
    UIView *tableHeadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, kP(136))];
    tableHeadView.backgroundColor = [UIColor colorWithHexString:@"#F3F3F3" alpha:1.0];
    UILabel *alertlabel1 = [UILabel new];
    alertlabel1.text = @"请选择一下任意一种证件, 拍照并提交";
    alertlabel1.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
    alertlabel1.font = [UIFont boldSystemFontOfSize:kP(32)];
    alertlabel1.x = kP(34);
    alertlabel1.y = kP(32);
    [alertlabel1 sizeToFit];
    [tableHeadView addSubview:alertlabel1];
    
    //
    UILabel *alertlabel2 = [UILabel new];
    alertlabel2.text = @"您的所有个人信息仅供审核使用, 不会泄露给任何第三方";
    alertlabel2.textColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0];
    alertlabel2.font = [UIFont boldSystemFontOfSize:kP(26)];
    [alertlabel2 sizeToFit];
    alertlabel2.x = alertlabel1.x;
    alertlabel2.y = tableHeadView.height - alertlabel2.height - kP(24);
    
    [tableHeadView addSubview:alertlabel2];
    //
    HZTableView *tableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.tableHeaderView = tableHeadView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor colorWithHexString:@"#F3F3F3" alpha:1.0];
    [self.view addSubview:tableView];
    self.tableView = tableView;
}


#pragma mark -- setMyImageGetBtn

- (void)setMyImageGetBtn {
    //
    CGFloat btnBgViewH = 64;
    CGFloat btnBgViewW = self.view.width;
    CGFloat btnBgViewX = 0;
    CGFloat btnBgViewY = self.view.height - btnBgViewH;
    UIView *btnBgView = [[UIView alloc] initWithFrame:CGRectMake(btnBgViewX, btnBgViewY, btnBgViewW, btnBgViewH)];
    btnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    btnBgView.layer.shadowOffset = CGSizeMake(0, - kP(2));
    btnBgView.layer.shadowOpacity = 0.1;
    btnBgView.layer.shadowRadius = kP(2);
    
    btnBgView.backgroundColor = [UIColor whiteColor];
    
    
    //
    CGFloat imageGetBtnX = kP(34);
    CGFloat imageGetBtnW = self.view.width - 2 * imageGetBtnX;
    CGFloat imageGetBtnY = kP(16);
    CGFloat imageGetBtnH = btnBgViewH - 2 * imageGetBtnY;
    UIButton *imageGetBtn = [[UIButton alloc] initWithFrame:CGRectMake(imageGetBtnX, imageGetBtnY, imageGetBtnW, imageGetBtnH)];
//    [imageGetBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#CBCBCB" alpha:1.0]] forState:UIControlStateDisabled];
    [imageGetBtn setBackgroundImage:[UIImage createImageWithColor:kGreenColor] forState:UIControlStateNormal];
    [imageGetBtn setTitle:@"上传照片" forState:UIControlStateNormal];
    [imageGetBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateNormal];
    imageGetBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    imageGetBtn.layer.cornerRadius = kP(10);
    imageGetBtn.clipsToBounds = YES;
    [imageGetBtn addTarget:self action:@selector(imageGet:) forControlEvents:UIControlEventTouchDown];
    [btnBgView addSubview:imageGetBtn];
    [self.view addSubview:btnBgView];
    self.imageGetBtn = imageGetBtn;

}


#pragma mark -- saveMyInformation
- (void)imageGet:(UIButton *)imageBtn {
    
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 相机权限
        AVAuthorizationStatus cameraAuthStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (cameraAuthStatus == AVAuthorizationStatusRestricted || cameraAuthStatus ==AVAuthorizationStatusDenied) {
            void(^block)() = authorizationsSettingBlock();
            showMessage(@"您相机权限未打开,是否打开?", self, block);
        }
        //
        UIImagePickerController* imagePick = [[UIImagePickerController alloc] init];
        imagePick.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePick.delegate = self;
        [self presentViewController:imagePick animated:YES completion:nil];
    }];
    
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //相册权限
        PHAuthorizationStatus albumAuthStatus = [PHPhotoLibrary authorizationStatus];
        if (albumAuthStatus == PHAuthorizationStatusRestricted || albumAuthStatus == PHAuthorizationStatusDenied) {
            void(^block)() = authorizationsSettingBlock();
            showMessage(@"您相册权限未打开,是否打开?", self, block);
        }
        //
        UIImagePickerController* imagePick = [[UIImagePickerController alloc] init];
        imagePick.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePick.delegate = self;
        [self presentViewController:imagePick animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    
    [alertCtr addAction:cameraAction];
    [alertCtr addAction:libraryAction];
    [alertCtr addAction:cancelAction];
    
    [self presentViewController:alertCtr animated:YES completion:nil];
}


#pragma mark -- UIImagePickerControllerDelegate

// 选中
- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString*, id>*)info
{

    UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
   
    HZImageAttestationUploadViewController *imageAttestationUploadVCtr = [HZImageAttestationUploadViewController new];
    imageAttestationUploadVCtr.attestationImage = image;
    imageAttestationUploadVCtr.user = self.user;
    [self.navigationController pushViewController:imageAttestationUploadVCtr animated:YES];
    //
    [picker dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HZAttestImageGetCell *cell = [HZAttestImageGetCell cellWithTableView:tableView];
   
    return cell;
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(996);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
