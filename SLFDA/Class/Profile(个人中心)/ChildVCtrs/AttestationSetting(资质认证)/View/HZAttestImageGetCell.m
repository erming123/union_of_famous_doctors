//
//  HZAttestImageGetCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZAttestImageGetCell.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZAttestImageGetCell ()
@property (nonatomic, strong) UILabel *professionalCertificateLabel;
@property (nonatomic, strong) UIImageView *professionalCertificateImageView;
@property (nonatomic, strong) UIView *lightSepLine;
@property (nonatomic, strong) UILabel *sepLineLabel;
@property (nonatomic, strong) UIView *rightSepLine;
@property (nonatomic, strong) UILabel *badgesLabel;
@property (nonatomic, strong) UIImageView *badgesImageView;
@end
NS_ASSUME_NONNULL_END
@implementation HZAttestImageGetCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    
    UILabel *professionalCertificateLabel = [UILabel new];
    professionalCertificateLabel.textColor = [UIColor colorWithHexString:@"#21B8C6" alpha:1.0];
    professionalCertificateLabel.font = [UIFont systemFontOfSize:kP(32)];
    [self addSubview:professionalCertificateLabel];
    self.professionalCertificateLabel = professionalCertificateLabel;
    
    //
    UIImageView *professionalCertificateImageView = [UIImageView new];
    [self addSubview:professionalCertificateImageView];
    self.professionalCertificateImageView = professionalCertificateImageView;

    //
    UIView *lightSepLine = [UIView new];
    lightSepLine.backgroundColor = [UIColor colorWithHexString:@"#EDEDED" alpha:1.0];
    [self addSubview:lightSepLine];
    self.lightSepLine = lightSepLine;
    
    UILabel *sepLineLabel = [UILabel new];
    sepLineLabel.textColor = [UIColor colorWithHexString:@"#EDEDED" alpha:1.0];
    sepLineLabel.font = [UIFont systemFontOfSize:kP(32)];
    sepLineLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:sepLineLabel];
    self.sepLineLabel = sepLineLabel;
    
    UIView *rightSepLine = [UIView new];
    rightSepLine.backgroundColor = [UIColor colorWithHexString:@"#EDEDED" alpha:1.0];
    [self addSubview:rightSepLine];
    self.rightSepLine = rightSepLine;
    
    //
    UILabel *badgesLabel = [UILabel new];
    badgesLabel.textColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    [self addSubview:badgesLabel];
    self.badgesLabel = badgesLabel;
    
    //
    UIImageView *badgesImageView = [UIImageView new];
    [self addSubview:badgesImageView];
    self.badgesImageView = badgesImageView;


}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    
    [self.professionalCertificateLabel sizeToFit];
    self.professionalCertificateLabel.x = self.width * 0.5 - self.professionalCertificateLabel.width * 0.5;
    self.professionalCertificateLabel.y = kP(24);
    
    //
    self.professionalCertificateImageView.width = kP(466);
    self.professionalCertificateImageView.height = kP(282);
    self.professionalCertificateImageView.x = self.width * 0.5 - self.professionalCertificateImageView.width * 0.5;
    self.professionalCertificateImageView.y = self.professionalCertificateLabel.bottom + kP(34);
    
    
    //
    self.lightSepLine.x = 0;
    self.lightSepLine.y = self.professionalCertificateImageView.bottom + kP(56);
    self.lightSepLine.width = kP(326);
    self.lightSepLine.height = kP(2);
    
    
    [self.sepLineLabel sizeToFit];
    self.sepLineLabel.x = self.lightSepLine.right + kP(28);
    self.sepLineLabel.y = self.lightSepLine.centerY - self.sepLineLabel.height * 0.5;
    
    
    self.rightSepLine.width = kP(326);
    self.rightSepLine.height = kP(2);
    self.rightSepLine.x = self.width - self.rightSepLine.width;
    self.rightSepLine.y = self.lightSepLine.y;

    //
    [self.badgesLabel sizeToFit];
    self.badgesLabel.x = self.width * 0.5 - self.badgesLabel.width * 0.5;
    self.badgesLabel.y = self.sepLineLabel.bottom + kP(42);
    
    //
    self.badgesImageView.width = kP(470);
    self.badgesImageView.height = kP(290);
    self.badgesImageView.x = self.width * 0.5 - self.badgesImageView.width * 0.5;
    self.badgesImageView.y = self.badgesLabel.bottom + kP(18);
    
}

- (void)setSubViewsContent {
    
    self.professionalCertificateLabel.text = @"医生职业证书";
    self.professionalCertificateImageView.image = [UIImage imageNamed:@"professionalCertificate"];
    self.sepLineLabel.text = @"或";
    
    self.badgesLabel.text = @"胸牌";
    self.badgesImageView.image = [UIImage imageNamed:@"badges"];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

@end
