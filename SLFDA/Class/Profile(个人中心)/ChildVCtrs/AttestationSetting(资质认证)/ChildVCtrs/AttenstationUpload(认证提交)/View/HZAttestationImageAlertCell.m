//
//  HZAttestationImageAlertCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZAttestationImageAlertCell.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZAttestationImageAlertCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel1;
@property (nonatomic, strong) UILabel *contentLabel2;
@property (nonatomic, strong) UILabel *contentLabel3;
@end
NS_ASSUME_NONNULL_END
@implementation HZAttestationImageAlertCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    titleLabel.font = [UIFont boldSystemFontOfSize:kP(30)];
    [self addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    UILabel *contentLabel1 = [UILabel new];
    contentLabel1.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
    contentLabel1.font = [UIFont systemFontOfSize:kP(28)];
    [self addSubview:contentLabel1];
    self.contentLabel1 = contentLabel1;
    
    UILabel *contentLabel2 = [UILabel new];
    contentLabel2.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
    contentLabel2.font = [UIFont systemFontOfSize:kP(28)];
    [self addSubview:contentLabel2];
    self.contentLabel2 = contentLabel2;
    
    
    UILabel *contentLabel3 = [UILabel new];
    contentLabel3.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
    contentLabel3.font = [UIFont systemFontOfSize:kP(28)];
    [self addSubview:contentLabel3];
    self.contentLabel3 = contentLabel3;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    
    [self.titleLabel sizeToFit];
    self.titleLabel.x = kP(44);
    self.titleLabel.y = 0;
    
    //
    [self.contentLabel1 sizeToFit];
    self.contentLabel1.x = self.titleLabel.x;
    self.contentLabel1.y = self.titleLabel.bottom + kP(30);
    
    //
    [self.contentLabel2 sizeToFit];
    self.contentLabel2.x = self.titleLabel.x;
    self.contentLabel2.y = self.contentLabel1.bottom + kP(20);

    //
    [self.contentLabel3 sizeToFit];
    self.contentLabel3.x = self.titleLabel.x;
    self.contentLabel3.y = self.contentLabel2.bottom + kP(20);

    
}

- (void)setSubViewsContent {
    
    self.titleLabel.text = @"注意事项";
    self.contentLabel1.text = @"1.请确保拍摄的证件为原件, 并保持文字清晰可见";
    self.contentLabel2.text = @"2.证件信息请和使用者的信息保持一致";
    self.contentLabel3.text = @"3.审核时间一般为3个工作日";
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
