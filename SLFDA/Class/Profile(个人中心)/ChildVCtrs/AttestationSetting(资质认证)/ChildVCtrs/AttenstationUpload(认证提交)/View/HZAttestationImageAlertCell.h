//
//  HZAttestationImageAlertCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZAttestationImageAlertCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
