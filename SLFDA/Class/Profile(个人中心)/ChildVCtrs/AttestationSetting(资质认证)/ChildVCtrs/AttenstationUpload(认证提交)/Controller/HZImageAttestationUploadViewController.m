//
//  HZImageAttestationUploadViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZImageAttestationUploadViewController.h"
#import "HZAttestationImageAlertCell.h"

//
#import "HZImageScanViewController.h"
//
#import "HZUser.h"
#import "HZUserTool.h"
//
#import "HZActivityIndicatorView.h"
@interface HZImageAttestationUploadViewController () <UITableViewDataSource, UITableViewDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *tableView;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *imageUploadBtn;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) UILabel *progressLabel;
//** <#注释#>*/
@property (nonatomic, strong) HZActivityIndicatorView *uploadActivityIndicatorView;
@end

@implementation HZImageAttestationUploadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F3F3F3" alpha:1.0];
    self.navigationItem.title = @"资质提交";
    
    //
    [self setMyImageAndAlert];
    
    //
    [self setMyImageGetBtn];
    
    //
//    UILabel *progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kP(300), kP(100))];
//    progressLabel.backgroundColor = [UIColor redColor];
////    progressLabel.alpha = 0.0;
//    progressLabel.textAlignment = NSTextAlignmentCenter;
//    progressLabel.center = self.view.center;
//    [self.view addSubview:progressLabel];
//    self.progressLabel = progressLabel;
    
    HZActivityIndicatorView *uploadActivityIndicatorView = [[HZActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, kP(240), kP(240))];
    uploadActivityIndicatorView.center = self.view.center;
    uploadActivityIndicatorView.activityIndicatorText = @"上传中...";
    [self.view addSubview:uploadActivityIndicatorView];
    self.uploadActivityIndicatorView = uploadActivityIndicatorView;
}

- (void)setMyImageAndAlert {
    //
    //    NSData *imageData = [self zipImageWithImage:];
    //    UIImage *attestationImage = [UIImage imageWithData:imageData];
//    UIImage *attestationImage = [self compressImage:self.attestationImage newWidth:kP(362)];
    
    //
//    CGFloat attestationImageViewW = attestationImage.size.width;
//    CGFloat attestationImageViewH = attestationImage.size.height;
    CGFloat attestationImageViewW = kP(362);
    CGFloat attestationImageViewH = kP(540);
    CGFloat attestationImageViewX = self.view.width * 0.5 - attestationImageViewW * 0.5;
    CGFloat attestationImageViewY = kP(62);
    UIView *tableHeadView = [[UIView alloc] initWithFrame:CGRectMake(10, 20, self.view.width, attestationImageViewH + 2 * attestationImageViewY)];
    
    //    tableHeadView.backgroundColor = [UIColor redColor];
    UIImageView *attestationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(attestationImageViewX, attestationImageViewY, attestationImageViewW, attestationImageViewH)];
    attestationImageView.contentMode = UIViewContentModeScaleAspectFit;
    attestationImageView.userInteractionEnabled = YES;
    //
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToScanImage:)];
    
    [attestationImageView addGestureRecognizer:tap];
    //
    
    attestationImageView.image = self.attestationImage;
    [tableHeadView addSubview:attestationImageView];
    
    HZTableView *tableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.tableHeaderView = tableHeadView;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    //
//    //
//    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activityIndicator.center = self.view.center;
//    activityIndicator.y += kP(100);
//    activityIndicator.color = [UIColor grayColor];
////    activityIndicator.layer.shadowOffset = CGSizeMake(5, 5);
////    activityIndicator.layer.shadowColor = (__bridge CGColorRef _Nullable)([UIColor redColor]);
//    [self.view addSubview:activityIndicator];
//    self.activityIndicator = activityIndicator;
}

- (void)setMyImageGetBtn {
    //
    CGFloat btnBgViewH = 64;
    CGFloat btnBgViewW = self.view.width;
    CGFloat btnBgViewX = 0;
    CGFloat btnBgViewY = self.view.height - btnBgViewH;
    UIView *btnBgView = [[UIView alloc] initWithFrame:CGRectMake(btnBgViewX, btnBgViewY, btnBgViewW, btnBgViewH)];
    btnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    btnBgView.layer.shadowOffset = CGSizeMake(0, - kP(2));
    btnBgView.layer.shadowOpacity = 0.1;
    btnBgView.layer.shadowRadius = kP(2);
    
    btnBgView.backgroundColor = [UIColor whiteColor];
    
    
    //
    CGFloat imageUploadBtnX = kP(34);
    CGFloat imageUploadBtnW = self.view.width - 2 * imageUploadBtnX;
    CGFloat imageUploadBtnY = kP(16);
    CGFloat imageUploadBtnH = btnBgViewH - 2 * imageUploadBtnY;
    UIButton *imageUploadBtn = [[UIButton alloc] initWithFrame:CGRectMake(imageUploadBtnX, imageUploadBtnY, imageUploadBtnW, imageUploadBtnH)];
    //    [imageGetBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#CBCBCB" alpha:1.0]] forState:UIControlStateDisabled];
    [imageUploadBtn setBackgroundImage:[UIImage createImageWithColor:kGreenColor] forState:UIControlStateNormal];
    [imageUploadBtn setTitle:@"确认提交" forState:UIControlStateNormal];
    [imageUploadBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateNormal];
    imageUploadBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    imageUploadBtn.layer.cornerRadius = kP(10);
    imageUploadBtn.clipsToBounds = YES;
    [imageUploadBtn addTarget:self action:@selector(imageUpload:) forControlEvents:UIControlEventTouchUpInside];
    [btnBgView addSubview:imageUploadBtn];
    [self.view addSubview:btnBgView];
    self.imageUploadBtn = imageUploadBtn;
    
}

- (void)imageUpload:(UIButton *)imageUploadBtn {
    
//    [self.activityIndicator startAnimating];
    imageUploadBtn.enabled = NO;
    self.uploadActivityIndicatorView.hidden = NO;
    
//    self.progressLabel.alpha = 1.0;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSData *imageData = [self.attestationImage getCompressedImageDataWithMaxSizeCount:32];
        UIImage *myAptitudeImage = [UIImage imageWithData:imageData];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [HZUserTool upLoadMyAptitudeWithAptitudeImage:myAptitudeImage userId:self.user.userId progress:^(NSProgress * _Nonnull uploadProgress) {
//                self.progressLabel.text = [NSString stringWithFormat:@"%lld", uploadProgress.completedUnitCount];
//                CGFloat progress = (CGFloat)uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
//                
//                NSLog(@"----aa-----%@", [NSString stringWithFormat:@"%f%%", roundf(progress * 100)]);
            } success:^(id responseObject) {
                
                
                NSLog(@"-----提交S------%@-----%@", responseObject, self.user.userId);
                
                //        if (<#condition#>) {
                //            <#statements#>
                //        }
                
                [self.navigationController popToRootViewControllerAnimated:YES];
//                [self.activityIndicator stopAnimating];
                self.uploadActivityIndicatorView.hidden = YES;
                imageUploadBtn.enabled = YES;
                
//                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasImageUploadDone"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"hasImageUploadDone" object:nil];
//                self.progressLabel.alpha = 0.0;
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"-----提交F---error:%@", error);
                self.uploadActivityIndicatorView.hidden = YES;
                imageUploadBtn.enabled = YES;
            }];
        });
    });
    
    
    
}


#pragma mark -- tapToScanImage

- (void)tapToScanImage:(UITapGestureRecognizer *)tap {
    
    //
    
    HZImageScanViewController *imageScanViewCtr = [HZImageScanViewController new];
    imageScanViewCtr.scanImage = self.attestationImage;
    [self.navigationController pushViewController:imageScanViewCtr animated:YES];
}


#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HZAttestationImageAlertCell *cell = [HZAttestationImageAlertCell cellWithTableView:tableView];
    return cell;
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(200);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
