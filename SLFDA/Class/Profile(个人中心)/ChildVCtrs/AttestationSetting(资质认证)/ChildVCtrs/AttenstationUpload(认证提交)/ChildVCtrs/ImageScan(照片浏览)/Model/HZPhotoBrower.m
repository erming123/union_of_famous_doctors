//
//  HZPhotoBrower.m
//  HZScrollViewImageDemo
//
//  Created by 季怀斌 on 2017/6/23.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPhotoBrower.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZPhotoBrower () <UIScrollViewDelegate>
@property (nonatomic, assign) BOOL isDoubleTap;
@property (nonatomic, assign) BOOL isSingleTap;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTap;
@property (nonatomic, strong) UITapGestureRecognizer *singleTap;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;
@end
NS_ASSUME_NONNULL_END

@implementation HZPhotoBrower

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setMySubViews];
    }
    return self;
}


#pragma mark -- setMySubViews
- (void)setMySubViews {
    
    self.maximumZoomScale = 10;
    self.minimumZoomScale = 1;
    self.showsVerticalScrollIndicator = NO;
    self.showsHorizontalScrollIndicator = NO;
    self.delegate = self;

    [self addSubview:self.imageView];
    
    
    [self addGestureRecognizer:self.doubleTap];
    
    [self addGestureRecognizer:self.singleTap];
    
    [self addGestureRecognizer:self.longPress];

}

#pragma mark -- get
- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, - 32, self.width, self.height)];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
//        _imageView.contentMode = UIViewContentModeScaleAspectFill;
//        _imageView.contentMode = UIViewContentModeScaleToFill;
        
        _imageView.userInteractionEnabled = YES;
        _imageView.backgroundColor = [UIColor whiteColor];
    }
    
    return _imageView;
}

- (UITapGestureRecognizer *)doubleTap {
    if (!_doubleTap) {
        _doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
        _doubleTap.numberOfTapsRequired = 2;
    }
    
    return _doubleTap;
}

- (UITapGestureRecognizer *)singleTap {
    if (!_singleTap) {
        _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        _singleTap.numberOfTapsRequired = 1;
        
        [_singleTap requireGestureRecognizerToFail:_doubleTap];
    }
    
    return _singleTap;
}

- (UILongPressGestureRecognizer *)longPress {
    if (!_longPress) {
        _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
//        _longPress.numberOfTapsRequired = 1;
        
//        [_longPress requireGestureRecognizerToFail:_doubleTap];
    }

    return _longPress;

}


#pragma mark -- targetAction


- (void)doubleTap:(UITapGestureRecognizer *)doubleTap {
    self.isDoubleTap = !self.isDoubleTap;
    
    //
    self.isDoubleTap ? [self setZoomScale:3 animated:YES] : [self setZoomScale:1 animated:YES];
    //
    if (self.doubleTapBlock) {
        self.doubleTapBlock(doubleTap);
    }
}

- (void)singleTap:(UITapGestureRecognizer *)singleTap {
    self.isSingleTap = !self.isSingleTap;
    
    //
//    self.hidden = self.isSingleTap;
    
    //
    if (self.singleTapBlock) {
        self.singleTapBlock(singleTap);
    }
}

- (void)longPress:(UILongPressGestureRecognizer *)longPress {
    //
    if (self.longPressBlock) {
        self.longPressBlock(longPress);
    }
}

#pragma mark -- scrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}




@end
