//
//  HZPhotoBrower.h
//  HZScrollViewImageDemo
//
//  Created by 季怀斌 on 2017/6/23.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZPhotoBrower : UIScrollView
@property (strong, nonatomic) UIImageView *imageView;

@property (nonatomic, strong) void (^singleTapBlock)(UITapGestureRecognizer *singleTap);
@property (nonatomic, strong) void (^doubleTapBlock)(UITapGestureRecognizer *doubleTap);
@property (nonatomic, strong) void (^longPressBlock)(UILongPressGestureRecognizer *longPress);
@end
