//
//  HZImageScanViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZImageScanViewController.h"
#import "HZPhotoBrower.h"
@interface HZImageScanViewController ()
//** <#注释#>*/
@property (nonatomic, strong) HZPhotoBrower *photoBrower;

//** <#注释#>*/
@property (nonatomic, strong)  UIImageView *imageView;

@property (nonatomic, assign) BOOL isHiddenStatusBar;
@end

@implementation HZImageScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.title = @"图片详情";

    //
    HZPhotoBrower *photoBrower = [[HZPhotoBrower alloc] initWithFrame:self.view.bounds];
    photoBrower.imageView.image = self.scanImage;
    //    photoBrower.hidden = YES;
//    photoBrower.backgroundColor = [UIColor redColor];
    [self.view addSubview:photoBrower];
    self.photoBrower = photoBrower;
    
    // 双击
//    __weak __typeof(self)weakSelf = self;
    WeakSelf(weakSelf)
    
    
    self.photoBrower.doubleTapBlock = ^(UITapGestureRecognizer *doubleTap){
//        __strong __typeof(weakSelf)strongSelf = weakSelf;
        StrongSelf(strongSelf)
        [strongSelf hideBar:doubleTap];
    };
    
    self.photoBrower.longPressBlock = ^(UILongPressGestureRecognizer *longPress) {
//        __strong __typeof(weakSelf)strongSelf = weakSelf;
        StrongSelf(strongSelf)
        [strongSelf longPressToSave:longPress];
    };
    
}


- (void)longPressToSave:(UILongPressGestureRecognizer *)longPress {
    if (longPress.state == UIGestureRecognizerStateBegan) {
        
        
        
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *saveAlertAction = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //
            UIImageWriteToSavedPhotosAlbum(self.scanImage, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
            
        }];
        
        UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        
        [alertCtr addAction:saveAlertAction];
        [alertCtr addAction:cancelAlertAction];
        [self presentViewController:alertCtr animated:YES completion:nil];
    }

}


#pragma mark -- imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:

- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(id)contextInfo {
    
    
    if (!error) {
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:@"提示" message:@"成功保存" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        [alertCtr addAction:ensureAlertAction];
        [self presentViewController:alertCtr animated:YES completion:nil];
        
    } else {
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:@"提示" message:[error description] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        [alertCtr addAction:ensureAlertAction];
        [self presentViewController:alertCtr animated:YES completion:nil];
    }
}


#pragma mark -- hideBar
- (void)hideBar:(UITapGestureRecognizer *)doubleTap {
    self.isHiddenStatusBar = !self.isHiddenStatusBar;
    self.navigationController.navigationBarHidden = self.isHiddenStatusBar;
    [self setNeedsStatusBarAppearanceUpdate];
    
}


- (BOOL)prefersStatusBarHidden {
    return self.isHiddenStatusBar;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
