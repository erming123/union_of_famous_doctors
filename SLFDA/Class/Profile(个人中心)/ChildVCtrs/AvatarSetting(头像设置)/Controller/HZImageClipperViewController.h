//
//  HZImageClipperViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/28.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZImageClipperViewController;
@protocol HZImageClipperViewControllerDelegate <NSObject>
- (void)imageClipperViewController:(HZImageClipperViewController *)imageClipper imageClipFinish:(UIImage *)clippedImage;
@end

@interface HZImageClipperViewController : HZViewController
@property (nonatomic, weak) id<HZImageClipperViewControllerDelegate>delegate;
@property (nonatomic, assign) BOOL roundClip;//圆形裁剪，默认NO;
- (instancetype)initWithImage:(UIImage *)originalImage delegate:(id)delegate;@end
