//
//  HZNormalNewsCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZNormalNewsCell.h"
#import "HZUserTool.h"
//
#import "HZMessageDB.h"
//
#import "HZSampleNews.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZNormalNewsCell ()
@property (nonatomic, strong) UIImageView *chatterImageView;
@property (nonatomic, strong) UILabel *chatterNameLabel;
@property (nonatomic, strong) UILabel *jobLabel;
@property (nonatomic, strong) UILabel *lastNewsLabel;
@property (nonatomic, strong) UILabel *newsTimeLabel;
@property (nonatomic, strong) UILabel *newsCountLabel;
//** <#注释#>*/
@property (nonatomic, strong) HZMessageDB *messageDB;
@end
NS_ASSUME_NONNULL_END
@implementation HZNormalNewsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //
        //
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    UIImageView *chatterImageView = [UIImageView new];
    [self addSubview:chatterImageView];
    self.chatterImageView = chatterImageView;
    
    UILabel *chatterNameLabel = [UILabel new];
    chatterNameLabel.font = [UIFont systemFontOfSize:kP(32)];
    chatterNameLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    //    chatterNameLabel.backgroundColor = [UIColor redColor];
    [self addSubview:chatterNameLabel];
    self.chatterNameLabel = chatterNameLabel;
    
    UILabel *jobLabel = [UILabel new];
    jobLabel.font = [UIFont systemFontOfSize:kP(28)];
    jobLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
    [self addSubview:jobLabel];
    self.jobLabel = jobLabel;
    
    UILabel *lastNewsLabel = [UILabel new];
    lastNewsLabel.font = [UIFont systemFontOfSize:kP(28)];
    lastNewsLabel.textColor = [UIColor colorWithHexString:@"#ABABAB" alpha:1.0];
    //    lastNewsLabel.backgroundColor = [UIColor yellowColor];
    [self addSubview:lastNewsLabel];
    self.lastNewsLabel = lastNewsLabel;
    
    UILabel *newsTimeLabel = [UILabel new];
    newsTimeLabel.font = [UIFont systemFontOfSize:kP(24)];
    newsTimeLabel.textColor = [UIColor colorWithHexString:@"#ABABAB" alpha:1.0];
    [self addSubview:newsTimeLabel];
    self.newsTimeLabel = newsTimeLabel;
    
    UILabel *newsCountLabel = [UILabel new];
    newsCountLabel.font = [UIFont systemFontOfSize:kP(24)];
    newsCountLabel.backgroundColor = [UIColor redColor];
    newsCountLabel.textColor = [UIColor whiteColor];
    newsCountLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:newsCountLabel];
    self.newsCountLabel = newsCountLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    CGFloat chatterImageViewX = kP(32);
    CGFloat chatterImageViewY = kP(32);
    CGFloat chatterImageViewWH = self.height - 2 * chatterImageViewY;
    self.chatterImageView.frame = CGRectMake(chatterImageViewX, chatterImageViewY, chatterImageViewWH, chatterImageViewWH);
    self.chatterImageView.layer.borderWidth = kP(1);
    self.chatterImageView.layer.borderColor = [UIColor colorWithHexString:@"#D5D5D5"].CGColor;
    self.chatterImageView.layer.cornerRadius = chatterImageViewWH * 0.5;
    self.chatterImageView.clipsToBounds = YES;
    
    //3.
    self.chatterNameLabel.x = self.chatterImageView.right + kP(24);
    self.chatterNameLabel.y = self.chatterImageView.centerY - self.chatterNameLabel.height - kP(10);
    
    //
    self.jobLabel.x = self.chatterNameLabel.right + kP(24);
    self.jobLabel.y = self.chatterNameLabel.centerY - self.jobLabel.height * 0.5;
    
    //
    self.lastNewsLabel.width = kP(400);
    self.lastNewsLabel.x = self.chatterNameLabel.x;
    self.lastNewsLabel.y = self.chatterImageView.centerY + kP(10);
    
    //
    self.newsTimeLabel.x = self.width - self.newsTimeLabel.width - kP(32);
    self.newsTimeLabel.y = self.jobLabel.centerY - self.newsTimeLabel.height * 0.5;
    
    //
    self.newsCountLabel.width = kP(40);
    self.newsCountLabel.height = kP(40);
    self.newsCountLabel.x = self.width - self.newsCountLabel.width - kP(32);
    self.newsCountLabel.y = self.lastNewsLabel.y;
    self.newsCountLabel.layer.cornerRadius = self.newsCountLabel.width * 0.5;
    self.newsCountLabel.clipsToBounds = YES;
    self.newsCountLabel.hidden = [NSString isBlankString:self.newsCountLabel.text];
}

- (void)setSubViewsContent {
    
    
    //    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    
    UIImage *avatarImg = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:self.sampleNews.openId];
    if (avatarImg) {
        self.chatterImageView.image = avatarImg;
    } else {
        
        self.chatterImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
        
        [HZUserTool getAvatarWithAvatarOpenId:self.sampleNews.openId success:^(UIImage *image) {
            
            //                dispatch_async(dispatch_get_main_queue(), ^{
            
            if (image) {
                self.chatterImageView.image = image;
                [[SDImageCache sharedImageCache] storeImage:image forKey:self.sampleNews.openId];
            } else {
                self.chatterImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
            }
            
            //                });
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            self.chatterImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
        }];
    }
    
    //    });
    //    NSArray *userDataStrArr = [self.userData componentsSeparatedByString:@"@"];
    //
    //    self.chatterNameLabel.text = userDataStrArr[2];
    //    [self.chatterNameLabel sizeToFit];
    //    self.jobLabel.text = userDataStrArr[2];
    //    [self.jobLabel sizeToFit];
    //    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    //        [HZUserTool getUserDetailInforWithOpenId:_chatterOpenId success:^(id responseObject) {
    //            //        NSLog(@"-as--:%@", responseObject);
    //            if ([responseObject[@"state"] isEqual:@200]) {
    //                NSDictionary *resultDict = responseObject[@"results"];
    //                NSArray *doctorArr = resultDict[@"doctors"];
    //                NSDictionary *doctorDict = doctorArr[0];
    //
    //                //
    //                dispatch_async(dispatch_get_main_queue(), ^{
    
    
    //      NSArray *userDataArr = [self.userData componentsSeparatedByString:@","];
    
    //
    //                    NSLog(@"-wsx--:%@", doctorDict);
    self.chatterNameLabel.text = self.sampleNews.userName;
    [self.chatterNameLabel sizeToFit];
    self.jobLabel.text = self.sampleNews.titleName;
    [self.jobLabel sizeToFit];
    //
    //                    //
    //                    //    self.chatterNameLabel.text = @"李兰娟";
    //                    //    [self.chatterNameLabel sizeToFit];
    //
    //                    //    self.jobLabel.text = @"院士";
    //                    //    [self.jobLabel sizeToFit];
    //
    //                });
    //            }
    //
    //        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    //            NSLog(@"-sa--:%@", error);
    //        }];
    //    });
    //
    
    //    NSArray *newsArr = [self.messageDB getMessageArrWithOrderId:self.userData];
    //
    //    NSDictionary *newsDict = [newsArr lastObject];
    //    NSLog(@"---hahah-------:%@----------------:%@", newsDict, newsDict[@"timestamp"]);
    
    self.lastNewsLabel.text = self.sampleNews.lastNews ? self.sampleNews.lastNews : @"[图片]";
    [self.lastNewsLabel sizeToFit];
    //
    NSString *timeStr = [self getDateDisplayString: [self.sampleNews.newsTimeStr integerValue] * 1000];
    self.newsTimeLabel.text = timeStr;
    [self.newsTimeLabel sizeToFit];
    self.newsCountLabel.text = [self.sampleNews.unReadNewsCountStr isEqualToString:@"0"] ? @"" : [self.sampleNews.unReadNewsCountStr intValue] >= 99 ? @"99" : self.sampleNews.unReadNewsCountStr;
}


//- (void)setChatterOpenId:(NSString *)chatterOpenId {
//
//    _chatterOpenId = chatterOpenId;
//}
//
//- (void)setUnReadCountStr:(NSString *)unReadCountStr {
//    _unReadCountStr = unReadCountStr;
//}


//- (void)setUserData:(NSString *)userData {
//    _userData = userData;
//}


- (void)setSampleNews:(HZSampleNews *)sampleNews {
    _sampleNews = sampleNews;
}

- (void)setNewsLabelhidden {
    self.newsCountLabel.hidden = YES;
}
#pragma mark -- 初始化数据库
- (HZMessageDB *)messageDB {
    if (_messageDB == nil) {
        _messageDB = [HZMessageDB initMessageDB];
    }
    return _messageDB;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"normalNewsCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

//时间显示内容
-(NSString *)getDateDisplayString:(long long) miliSeconds{
    
    NSTimeInterval tempMilli = miliSeconds;
    NSTimeInterval seconds = tempMilli/1000.0;
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:seconds];
    
    NSCalendar *calendar = [ NSCalendar currentCalendar ];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear ;
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[ NSDate date ]];
    NSDateComponents *myCmps = [calendar components:unit fromDate:myDate];
    
    NSDateFormatter *dateFmt = [[ NSDateFormatter alloc ] init];
    if (nowCmps.year != myCmps.year) {
        dateFmt.dateFormat = @"yyyy-MM-dd HH:mm";
    } else {
        if (nowCmps.day==myCmps.day) {
            dateFmt.dateFormat = @"aa KK:mm";
        } else if ((nowCmps.day-myCmps.day)==1) {
            dateFmt.dateFormat = @"昨天 HH:mm";
        } else {
            dateFmt.dateFormat = @"MM-dd HH:mm";
        }
    }
    return [dateFmt stringFromDate:myDate];
}
@end
