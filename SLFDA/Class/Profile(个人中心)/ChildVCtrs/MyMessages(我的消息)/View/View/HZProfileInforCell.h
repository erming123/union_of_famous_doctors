//
//  HZProfileInforCell.h
//  SLIOP
//
//  Created by 季怀斌 on 16/8/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;

@protocol HZProfileInforCellDelegate <NSObject>

- (void)tapToGetImage:(UITapGestureRecognizer *)tap;

@end
@interface HZProfileInforCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, strong) HZUser *user;
//** <#注释#>*/
@property (nonatomic, strong) UIImage *avatarImage;
@property (nonatomic, weak) id<HZProfileInforCellDelegate>delegate;
@end
