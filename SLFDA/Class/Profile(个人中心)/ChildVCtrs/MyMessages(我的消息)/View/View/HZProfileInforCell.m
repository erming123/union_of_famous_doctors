//
//  HZProfileInforCell.m
//  SLIOP
//
//  Created by 季怀斌 on 16/8/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZProfileInforCell.h"
#import "HZUser.h"
#import "HZUserTool.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZProfileInforCell ()
//** <#注释#>*/
@property (nonatomic, strong) UIView *avatarImageBgView;
@property (nonatomic, strong) UIImageView *profileAvatarImageView;
@property (nonatomic, strong) UIImageView *avatarCameraImageView;
@property (nonatomic, strong) UILabel *profileNameLabel;
//** <#注释#>*/
@property (nonatomic, strong) UILabel *attestStatusLabel;
//** <#注释#>*/
@property (nonatomic, strong) UIImageView *attestedImageView;
@property (nonatomic, strong) UILabel *attestedLabel;
//
@property (nonatomic, strong) UILabel *profileJobLabel;
@property (nonatomic, strong) UILabel *profileSubjectLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZProfileInforCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UIView *avatarImageBgView = [UIView new];
    [self addSubview:avatarImageBgView];
    self.avatarImageBgView = avatarImageBgView;
    
    UIImageView *profileAvatarImageView = [[UIImageView alloc] init];
    profileAvatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    // 这里的content设置是不起作用的。
//    profileAvatarImageView.backgroundColor = [UIColor whiteColor];
//    profileAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
    profileAvatarImageView.userInteractionEnabled = YES;
    //
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToGetCameraOrAlbum:)];
    
    [profileAvatarImageView addGestureRecognizer:tap];
    //
    [self addSubview:profileAvatarImageView];
    self.profileAvatarImageView = profileAvatarImageView;
    
    //
    
    UIImageView *avatarCameraImageView = [UIImageView new];
    
//    avatarCameraImageView.backgroundColor = [UIColor redColor];
    [self addSubview:avatarCameraImageView];
    self.avatarCameraImageView = avatarCameraImageView;
    
    
    
    
    //
    
    UILabel *profileNameLabel = [[UILabel alloc] init];
    profileNameLabel.font = [UIFont systemFontOfSize:kP(36)];
    [self addSubview:profileNameLabel];
    self.profileNameLabel = profileNameLabel;
    
    // -----------------------attestStatusLabel
    UILabel *attestStatusLabel = [[UILabel alloc] init];
    attestStatusLabel.font = [UIFont systemFontOfSize:kP(26)];
    attestStatusLabel.layer.cornerRadius = kP(16);
    attestStatusLabel.layer.borderWidth = kP(2);
    attestStatusLabel.clipsToBounds = YES;
    attestStatusLabel.textAlignment = NSTextAlignmentCenter;
    //
    attestStatusLabel.hidden = YES;
    [self addSubview:attestStatusLabel];
    self.attestStatusLabel = attestStatusLabel;
    
    
    
    // 审核通过的imageView
    UIImageView *attestedImageView = [UIImageView new];
    attestedImageView.hidden = YES;
    attestedImageView.image = [UIImage imageNamed:@"attestationSuccess"];
    [self addSubview:attestedImageView];
    self.attestedImageView = attestedImageView;
    // 审核通过的label
    UILabel *attestedLabel = [[UILabel alloc] init];
    attestedLabel.hidden = YES;
    attestedLabel.font = [UIFont systemFontOfSize:kP(26)];
    attestedLabel.textAlignment = NSTextAlignmentCenter;
    attestedLabel.textColor = [UIColor colorWithHexString:@"#09BB07" alpha:1.0];
    attestedLabel.text = @"认证医生";
    [self addSubview:attestedLabel];
    self.attestedLabel = attestedLabel;

    //
    UILabel *profileJobLabel = [[UILabel alloc] init];
    [profileJobLabel setTextFont:kP(32) textColor:@"666666" alpha:1.0];
    [self addSubview:profileJobLabel];
    self.profileJobLabel = profileJobLabel;
    
    UILabel *profileSubjectLabel = [[UILabel alloc] init];
    [profileSubjectLabel setTextFont:kP(32) textColor:@"666666" alpha:1.0];
    [self addSubview:profileSubjectLabel];
    self.profileSubjectLabel = profileSubjectLabel;

}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self setUpSubViewsContent];
    
    // 头像背景
    CGFloat avatarImageBgViewWH = kP(128);
    CGFloat avatarImageBgViewX = kP(28);
    CGFloat avatarImageBgViewY = self.height * 0.5 - avatarImageBgViewWH * 0.5;
    
    self.avatarImageBgView.frame = CGRectMake(avatarImageBgViewX, avatarImageBgViewY, avatarImageBgViewWH, avatarImageBgViewWH);
    self.avatarImageBgView.backgroundColor = [UIColor colorWithHexString:@"999999" alpha:0.1];
    self.avatarImageBgView.layer.cornerRadius = kP(69);
    self.avatarImageBgView.clipsToBounds = YES;
    // 头像
//    CGFloat profileAvatarImageViewWH = kP(130);
//    CGFloat profileAvatarImageViewX = avatarImageBgViewX + kP(4);
//    CGFloat profileAvatarImageViewY = avatarImageBgViewY + kP(4);
    
    
    //
    self.profileAvatarImageView.width = kP(120);
    self.profileAvatarImageView.height = kP(120);
    self.profileAvatarImageView.center = self.avatarImageBgView.center;
//    self.profileAvatarImageView.frame = CGRectMake(profileAvatarImageViewX, profileAvatarImageViewY, profileAvatarImageViewWH, profileAvatarImageViewWH);
    self.profileAvatarImageView.layer.cornerRadius = kP(60);
    self.profileAvatarImageView.clipsToBounds = YES;
    
    
    // 相机
    CGFloat avatarCameraImageViewWH = kP(42);
    CGFloat avatarCameraImageViewXY = self.profileAvatarImageView.right - avatarCameraImageViewWH;
    self.avatarCameraImageView.frame = CGRectMake(avatarCameraImageViewXY, avatarCameraImageViewXY, avatarCameraImageViewWH, avatarCameraImageViewWH);
    
    // 姓名
    [self.profileNameLabel sizeToFit];
    self.profileNameLabel.x = self.profileAvatarImageView.right + kP(30);
    self.profileNameLabel.y = self.profileAvatarImageView.centerY - kP(10) - self.profileNameLabel.height;
    
    // 未审核／审核中
    self.attestStatusLabel.width = kP(110);
    self.attestStatusLabel.height = kP(36);
    self.attestStatusLabel.x = self.profileNameLabel.right + kP(16);
    self.attestStatusLabel.y = self.profileNameLabel.width == 0 ? kP(38) + self.attestStatusLabel.height * 0.5 : self.profileNameLabel.centerY - self.attestStatusLabel.height * 0.5;
    
    // 已审核
    self.attestedImageView.width = kP(22);
    self.attestedImageView.height = kP(26);
    self.attestedImageView.x = self.profileNameLabel.right + kP(20);
    self.attestedImageView.y = self.profileNameLabel.width == 0 ? kP(38) + self.attestedImageView.height * 0.5 : self.profileNameLabel.centerY - self.attestedImageView.height * 0.5;
    //
    [self.attestedLabel sizeToFit];
    self.attestedLabel.x = self.attestedImageView.right + kP(8);
    self.attestedLabel.y = self.profileNameLabel.width == 0 ? kP(38) + self.attestedLabel.height * 0.5 : self.profileNameLabel.centerY - self.attestedLabel.height * 0.5;
    
    // 工作
    [self.profileJobLabel sizeToFit];
    self.profileJobLabel.x = self.profileNameLabel.x;
    self.profileJobLabel.y = self.profileAvatarImageView.centerY + kP(10);

    // 科室
    [self.profileSubjectLabel sizeToFit];
    self.profileSubjectLabel.x = self.profileJobLabel.width == 0 ? self.profileAvatarImageView.right + kP(30) : self.profileJobLabel.right + kP(35);
    self.profileSubjectLabel.y = self.profileJobLabel.centerY - self.profileSubjectLabel.height * 0.5;
}


+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"profileInforCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }

    return cell;
}


- (void)setAvatarImage:(UIImage *)avatarImage {
    _avatarImage = avatarImage;
    
    //
    self.profileAvatarImageView.image = avatarImage;
}

#pragma mark -- 设置内容
- (void)setUpSubViewsContent {
    
    self.profileAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
    NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:self.user.openid];
    if (imageData) {
        UIImage *image = [UIImage imageWithData:imageData];
        self.profileAvatarImageView.image = image;
    } else {
        //
        [HZUserTool getAvatarWithAvatarOpenId:self.user.openid success:^(UIImage *image) {
            
        
            if (image) {
                self.profileAvatarImageView.image = image;
                NSData *imageData = UIImagePNGRepresentation(image);//把image归档为NSData
                [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:self.user.openid];
            } else {
                    self.profileAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"---------%@", error);
        }];
    }


    self.avatarCameraImageView.image = [UIImage imageNamed:@"avatarCamera"];
    
    NSLog(@"----ss222-----%@", self.user.authenticationState);
    
    //
    if ([self.user.authenticationState isEqualToString:@"AUTHENTICATED"]) {// 审核通过
        self.attestStatusLabel.hidden = YES;
        self.attestedImageView.hidden = NO;
        self.attestedLabel.hidden = NO;
        
        //
        
    } else {
        self.attestStatusLabel.hidden = NO;
        self.attestedImageView.hidden = YES;
        self.attestedLabel.hidden = YES;
        if ([self.user.authenticationState isEqualToString:@"UNAUTHENTICATED"] || [self.user.authenticationState isEqualToString:@"REJECTED"]) { // 尚未审核 或者 审核失败
            self.attestStatusLabel.text = @"未认证";
            self.attestStatusLabel.layer.borderColor = [UIColor colorWithHexString:@"#F5A623" alpha:1.0].CGColor;
            self.attestStatusLabel.textColor = [UIColor colorWithHexString:@"#F5A623" alpha:1.0];
        } else  if([self.user.authenticationState isEqualToString:@"AUTHENTICATING"]){ // 审核中
            self.attestStatusLabel.text = @"认证中";
            self.attestStatusLabel.layer.borderColor = [UIColor colorWithHexString:@"#4A90E2" alpha:1.0].CGColor;
            self.attestStatusLabel.textColor = [UIColor colorWithHexString:@"#4A90E2" alpha:1.0];
        } else {
            
            self.attestStatusLabel.hidden = YES;
        }
    }
//    self.attestStatusLabel.text = @"认证中";
//    self.attestStatusLabel.layer.borderColor = [UIColor colorWithHexString:@"#F5A623" alpha:1.0].CGColor;
    
    self.attestedImageView.image = [UIImage imageNamed:@"attestationSuccess"];
    self.attestedLabel.text = @"认证医生";
    self.profileNameLabel.text = self.user.userName;
    self.profileJobLabel.text = self.user.titleName;
    self.profileSubjectLabel.text = self.user.departmentGeneralName;
}

- (void)setUser:(HZUser *)user {
    _user = user;
    
    NSLog(@"------sssss---%@", user);
}


#pragma mark -- tapToGetCameraOrAlbum

- (void)tapToGetCameraOrAlbum:(UITapGestureRecognizer *)tap {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapToGetImage:)]) {
        [self.delegate tapToGetImage:tap];
    }

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
