//
//  HZNormalNewsCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZSampleNews;
@interface HZNormalNewsCell : UITableViewCell
//@property (nonatomic, copy) NSString *chatterOpenId;
//@property (nonatomic, copy) NSString *userData;
//@property (nonatomic, copy) NSString *unReadCountStr;
//** <#注释#>*/
@property (nonatomic, strong) HZSampleNews *sampleNews;
- (void)setNewsLabelhidden;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
