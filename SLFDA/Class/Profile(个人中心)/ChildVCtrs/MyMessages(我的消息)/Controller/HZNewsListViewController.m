//
//  HZNewsListViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/17.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZNewsListViewController.h"
#import "HZMessageViewController.h"
#import "HZOrganGraftingViewController.h"
#import "HZNormalNewsCell.h"
#import "HZOrganGraftingNewsCell.h"
//

//
#import "HZUserTool.h"
#import "HZUser.h"
//
#import "HZMessageDB.h"
//
#import "HZSampleNews.h"
#import "IQKeyboardManager.h"
static NSMutableArray *tempArr;
@interface HZNewsListViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *tempArr;
//@property (nonatomic, copy) NSMutableArray *userDataArr;
@property (nonatomic, strong) NSMutableArray *sampleNewsArr;
@property (nonatomic, strong) NSMutableArray *unReadCountArr;
@property (nonatomic,strong) NSMutableArray *organGraftingNewsArr;
@property (nonatomic,strong) NSMutableArray *resultsArray;
@property (nonatomic,strong) NSMutableArray *organGraftingDataArr;
//** */
@property (nonatomic, weak) HZTableView *tableView;
@property (nonatomic, strong) NSString *userData;

//** <#注释#>*/
@property (nonatomic, strong) HZMessageDB *messageDB;

@property (nonatomic, assign) BOOL isHiddenNewsLabel;

//
//** <#注释#>*/
@property (nonatomic, weak) UIImageView *noNewsListImageView;
//** <#注释#>*/
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;

@property (nonatomic,strong) NSMutableArray *blockArray;
@end

@implementation HZNewsListViewController

-(NSMutableArray *)organGraftingDataArr{
    if (!_organGraftingDataArr) {
        _organGraftingDataArr = [NSMutableArray array];
    }
    return _organGraftingDataArr;
}

-(NSMutableArray *)resultsArray {
    if (!_resultsArray) {
        _resultsArray = [NSMutableArray array];
    }
    return _resultsArray;
}
-(NSMutableArray *)organGraftingNewsArr{
    if (!_organGraftingNewsArr) {
        
        _organGraftingNewsArr = [NSMutableArray array];
    }
    return _organGraftingNewsArr;
}

-(NSMutableArray *)blockArray {
    if (!_blockArray) {
        _blockArray = [NSMutableArray array];
    }
    return _blockArray;
}

- (void)setUser:(HZUser *)user {
    _user = user;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor redColor];
    
    self.navigationItem.title = @"我的信息";
    //
    //
    //    [self.messageDB deleteAllMessage];
    //    self.messageDB = nil;

    //
    HZTableView *tableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.tableFooterView = [UIView new];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    //
    //3. 没有消息列表的时候
    UIImageView *noNewsListImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(390), kP(434))];
    noNewsListImageView.image = [UIImage imageNamed:@"noNewsList"];
    noNewsListImageView.center = self.view.center;
    noNewsListImageView.y = kP(600);
    noNewsListImageView.hidden = YES;
    [self.view addSubview:noNewsListImageView];
    self.noNewsListImageView = noNewsListImageView;
    noNewsListImageView.center = self.view.center;
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = noNewsListImageView.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    // 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMyMessage:) name:KNOTIFICATION_onMesssageReceive object:nil];
    
    //1.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMyMessage:) name:KNOTIFICATION_onMesssageOffLineReceive object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendMsgComplete) name:@"sendMessageComplete" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMyMessage:) name:@"reloadNewsList" object:nil];
    //2.
    [self.resultsArray removeAllObjects];
    [self setDataSource:YES];
    
    if (![HZChatHelper sharedInstance].hasUnReadMsg) {
        [[HZChatHelper sharedInstance].unReadNewsArr removeAllObjects];
    }
}

-(void)sendMsgComplete {
    
    [self cancleTheBlockInQueue];
    
    dispatch_block_t block = dispatch_block_create(DISPATCH_BLOCK_BARRIER, ^{
    
    
    NSArray *arr = [self.messageDB getMyAllMessageUserDataArr:nil];
    
    [self.sampleNewsArr removeAllObjects];
    for (NSString *newsUserDataStr in arr) {
        if ([newsUserDataStr containsString:kMyChatId]) continue;
        if (![newsUserDataStr containsString:kOrganGraftingUserData]) {
            NSArray *userDataArr = [newsUserDataStr componentsSeparatedByString:@","];
            HZSampleNews *sampleNews = [HZSampleNews new];
            [self.messageDB getMessageArrWithOrderId:newsUserDataStr];
            sampleNews.userData = newsUserDataStr;
            sampleNews.openId = userDataArr[0];
            sampleNews.userName = userDataArr[1];
            sampleNews.titleName = userDataArr[2];
            
            NSDictionary *newsDict = [self.messageDB.IMmessageArr lastObject];
            sampleNews.newsTimeStr = newsDict[@"timestamp"];
            sampleNews.lastNews = newsDict[@"text"];
            sampleNews.imgPathArr = [NSArray arrayWithArray:self.messageDB.msgImgPathArr];
            [self.sampleNewsArr addObject:sampleNews];
            
 
        }
        

    }
    
    NSArray *sortArray = [self.sampleNewsArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        HZSampleNews *sampleNews1 = obj1;
        HZSampleNews *sampleNews2 = obj2;
        
        NSInteger newsTime1 = [sampleNews1.newsTimeStr integerValue] * 1000;
        NSInteger newsTime2 = [sampleNews2.newsTimeStr integerValue] * 1000;
        
        //按照大小排序
        if (newsTime1 < newsTime2) { //不使用intValue比较无效
            return NSOrderedDescending;//降序
        }else if (newsTime1 > newsTime2){
            return NSOrderedAscending;//升序
        }else {
            return NSOrderedSame;//相等
        }
        
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.resultsArray removeAllObjects];
        [self.resultsArray addObject:self.organGraftingNewsArr];
        [self.resultsArray addObject:sortArray];
        [self.tableView reloadData];
        
    });
        
    });
    
    [self.blockArray addObject:block];
    // 发送成功
    dispatch_async(self.messageDB.GlobSerieaQueue,block);
}

#pragma mark - 取消任务中的最前面任务
- (void)cancleTheBlockInQueue{
    
    // 只留最后一个任务;
    if (self.blockArray.count < 3 ) return;
    for (int i = 0; i<self.blockArray.count -2; ++i) {
        dispatch_block_t block = self.blockArray[i];
        dispatch_block_cancel(block);
    }
    
}

- (void)receiveMyMessage:(NSNotification *)notification { // 接受未读数，并存起来
    [self cancleTheBlockInQueue];
    [self setDataSource:NO];
}

#pragma mark -- setDataSource

- (void)setDataSource:(BOOL)firstLoad {
    
    firstLoad?[self.activityIndicator startAnimating]:self;
    NSDictionary *myDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
    
    NSLog(@"---qaz-----------:%@", myDict);
    
    
    NSArray *userDataArr;
    if (!myDict) {
        
        userDataArr = [self.messageDB getMyAllMessageUserDataArr:nil];
        NSLog(@"-----dddsssswwwww-------------:%@", userDataArr);
        
        NSMutableDictionary *userDataDict = [NSMutableDictionary dictionary];
        for (NSString *userDataStr in userDataArr) {
            [userDataDict setValue:@"0" forKey:userDataStr];
        }
        NSDictionary *saveUserDataDict = [NSDictionary dictionaryWithDictionary:userDataDict];
        [[NSUserDefaults standardUserDefaults] setObject:saveUserDataDict forKey:@"profileVCtrNews"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"newsList-set--%@",saveUserDataDict);
    }
    
    userDataArr  = [[[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"] allKeys];
    
    dispatch_block_t block = dispatch_block_create(DISPATCH_BLOCK_BARRIER, ^{
        
        [self.resultsArray removeAllObjects];
        [self.organGraftingNewsArr removeAllObjects];
        [self.sampleNewsArr removeAllObjects];
        for (NSString *newsUserDataStr in userDataArr) {
            if ([newsUserDataStr containsString:self.user.openid]) continue;
            if ([newsUserDataStr containsString:@","]) {
                NSArray *userDataArrTemp = [newsUserDataStr componentsSeparatedByString:@","];
                HZSampleNews *sampleNews = [HZSampleNews new];
                sampleNews.userData = newsUserDataStr;
                sampleNews.openId = userDataArrTemp[0];
                sampleNews.userName = userDataArrTemp[1];
                sampleNews.titleName = userDataArrTemp[2];
                //
                [self.messageDB getMessageArrWithOrderId:newsUserDataStr];
                // NSArray *imgPathArr = [self.messageDB getMsgImgPathArrWithOrderId:newsUserDataStr];
                // NSLog(@"----tyttttttt-------------------:%@", imgPathArr);
                NSDictionary *newsDict = [self.messageDB.IMmessageArr lastObject];
                sampleNews.newsTimeStr = newsDict[@"timestamp"];
                sampleNews.lastNews = newsDict[@"text"];
                sampleNews.imgPathArr = [NSArray arrayWithArray:self.messageDB.msgImgPathArr];
                [self.sampleNewsArr addObject:sampleNews];
            } else{
                 HZSampleNews *sampleNews = [HZSampleNews new];
                //
               self.organGraftingDataArr = [self.messageDB getPushMessageArrWithOrderId:kOrganGraftingUserData];
                NSDictionary *newsDict = [self.messageDB.PushMessageArr lastObject];
                //
                sampleNews.userData = kOrganGraftingUserData;
//                sampleNews.openId = @"ddddddd";
//                sampleNews.userName = @"肾脏移植";
//                sampleNews.titleName = @"你好";
                //
                sampleNews.newsTimeStr = newsDict[@"timestamp"];
                sampleNews.lastNews = newsDict[@"text"];
               // sampleNews.imgPathArr = [NSArray arrayWithArray:self.messageDB.msgImgPathArr];
                [self.organGraftingNewsArr addObject:sampleNews];
            }
            
            
           
        }
        
        
        
//        self.unReadCountArr = [NSMutableArray arrayWithArray:[[[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"] allValues]];
//        NSLog(@"--212--:%@", self.unReadCountArr);
       NSDictionary *dict =  [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
//        NSValue *value = dict[kOrganGraftingUserData];
        NSArray *allKeys = [NSMutableArray arrayWithArray:[[[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"] allKeys]];
        
        for (int i=0; i<allKeys.count; ++i) {
            NSString *key = allKeys[i];
            if ([key containsString:@","]) {
                // IM
               // HZSampleNews *sampleNews = self.sampleNewsArr[i];
                for (HZSampleNews *sampleNews in self.sampleNewsArr) {
                    if ([sampleNews.userData isEqualToString:key]) {
                        sampleNews.unReadNewsCountStr = [dict valueForKey:key];
                    }
                }
                
                
            }else{
                for (HZSampleNews *organGrafting in self.organGraftingNewsArr) {
                    if ([organGrafting.userData isEqualToString:key])  {
                        organGrafting.unReadNewsCountStr = [dict valueForKey:key];
                    }
                }
                
            }
        }
        

        //
        NSArray *sortArray = [self.sampleNewsArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            HZSampleNews *sampleNews1 = obj1;
            HZSampleNews *sampleNews2 = obj2;
            
            NSLog(@"--edc---:%@------:%@", sampleNews1.newsTimeStr, sampleNews2.newsTimeStr);
            
            //
            NSInteger newsTime1 = [sampleNews1.newsTimeStr integerValue] * 1000;
            NSInteger newsTime2 = [sampleNews2.newsTimeStr integerValue] * 1000;
            
            //按照大小排序
            if (newsTime1 < newsTime2) { //不使用intValue比较无效
                return NSOrderedDescending;//降序
            }else if (newsTime1 > newsTime2){
                return NSOrderedAscending;//升序
            }else {
                return NSOrderedSame;//相等
            }
            
        }];
        
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
             [self.sampleNewsArr removeAllObjects];
            [self.sampleNewsArr addObjectsFromArray:sortArray];
            
            
            for (int i = 0; i < self.unReadCountArr.count; i++) {
                NSString *unReadNewsCountStr = self.unReadCountArr[i];
                
                if (![unReadNewsCountStr isEqualToString:@""] && ![unReadNewsCountStr isEqualToString:@"0"]) {
                    [HZChatHelper sharedInstance].hasUnReadMsg = YES;
                    break;
                }
                
                [HZChatHelper sharedInstance].hasUnReadMsg = NO;
            }
            
            NSLog(@"----sxc-----BOOL------%@", [HZChatHelper sharedInstance].hasUnReadMsg == YES ? @"YES":@"NO");
            
            
            //
            NSLog(@"--rty--:%@", self.unReadCountArr);
            //
            
            //  dispatch_async(dispatch_get_main_queue(), ^{
            
            //把推送数组 插在最前面
            [self.resultsArray addObject:self.organGraftingNewsArr];
            [self.resultsArray addObject:self.sampleNewsArr];
            
            [self.tableView reloadData];
            
            firstLoad?[self.activityIndicator stopAnimating]:self;
            
            
            self.noNewsListImageView.hidden = self.organGraftingNewsArr.count != 0 || self.sampleNewsArr.count != 0;
            
        });
        
    });
    
    if (!firstLoad) {
        [self.blockArray addObject:block];
    }
    dispatch_async(self.messageDB.GlobSerieaQueue,block);
   
  
}

#pragma mark -- 初始化数据库
- (HZMessageDB *)messageDB {
    if (_messageDB == nil) {
        _messageDB = [HZMessageDB initMessageDB];
    }
    return _messageDB;
}

#pragma mark -- get
- (NSMutableArray *)sampleNewsArr {
    
    if (!_sampleNewsArr) {
        _sampleNewsArr = [NSMutableArray array];
    }
    return _sampleNewsArr;
}

- (NSMutableArray *)unReadCountArr {
    
    if (!_unReadCountArr) {
        _unReadCountArr = [NSMutableArray array];
    }
    return _unReadCountArr;
}

//- (NSMutableArray *)userDataArr {
//    if (!_userDataArr) {
//        _userDataArr = [NSMutableArray array];
//    }
//    return _userDataArr;
//
//}


#pragma mark -- UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"--asd--:%@", self.sampleNewsArr);
    if (section == 0) {
        return self.organGraftingNewsArr.count;
    }else{
         return self.sampleNewsArr.count;
    }
    
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        HZOrganGraftingNewsCell *organGraftingNewsCell = [HZOrganGraftingNewsCell cellWithTableView:tableView];
        organGraftingNewsCell.sampleNews = self.organGraftingNewsArr[indexPath.row];
        return organGraftingNewsCell;
    }else{
        HZNormalNewsCell *newsListCell = [HZNormalNewsCell cellWithTableView:tableView];
        HZSampleNews *sampleNews = self.sampleNewsArr[indexPath.row];
        newsListCell.sampleNews = sampleNews;
        return newsListCell;
    }
    
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(160);
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //
    if (self.resultsArray.count == 0) return;
    if (indexPath.section == 0) {
       
    HZOrganGraftingViewController *organGraftingVCtr = [HZOrganGraftingViewController new];
    organGraftingVCtr.dataArray = self.organGraftingDataArr;
    [self.navigationController pushViewController:organGraftingVCtr animated:YES];
   
    }else{
        
     HZSampleNews *sampleNews = self.sampleNewsArr[indexPath.row];
     HZMessageViewController *messageVCtr = [HZMessageViewController new];
   
    NSString *chatterOpenId = sampleNews.openId;
    NSLog(@"--asxcwsx123--:%@----", chatterOpenId);

     messageVCtr.chatterOpenId = chatterOpenId;
     messageVCtr.title = sampleNews.userName;
     [self.navigationController pushViewController:messageVCtr animated:YES];

    
    HZNormalNewsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setNewsLabelhidden];
    //    self.isHiddenNewsLabel = YES;
    //    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    //
    // 删除缓存的元素
    NSDictionary *newsDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
    NSMutableDictionary *myDict = [NSMutableDictionary dictionaryWithDictionary:newsDict];
    [myDict setValue:@"" forKey:sampleNews.userData];
    
    // 再存元素
    [[NSUserDefaults standardUserDefaults] setObject:myDict forKey:@"profileVCtrNews"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"NEWSlist-set2--%@",myDict);
    
    NSArray *tempArr = [[HZChatHelper sharedInstance].unReadNewsArr copy];
    
    for (ECMessage *message in tempArr) {
        NSString *userData = (NSString *)message.userData;
        if ([userData isEqualToString:sampleNews.userData]) {
            // 发送已读消息
            //  荣连云 免费版 不能改消息回执
            // 去数据库,改变状态;
            [[HZMessageDB initMessageDB] changeTheMessageReadState:YES with:message.userData andMessageId:message.messageId];
            [[HZChatHelper sharedInstance].unReadNewsArr removeObject:message];
        }
    }
    [self setUnreadMsgCount];
  }
}

-(void)setUnreadMsgCount{
    
    NSArray *arr =[[[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"] allValues];
    if (arr.count == 0) {
        [HZChatHelper sharedInstance].hasUnReadMsg = NO;
    }
    for (int i = 0; i < arr.count; i++) {
        NSString *unReadNewsCountStr = arr[i];
        
        if (![unReadNewsCountStr isEqualToString:@""] && ![unReadNewsCountStr isEqualToString:@"0"]) {
            [HZChatHelper sharedInstance].hasUnReadMsg = YES;
             break;
        }
        
        [HZChatHelper sharedInstance].hasUnReadMsg = NO;
    }
    
}



-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return   UITableViewCellEditingStyleDelete;
}
//先要设Cell可编辑
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return NO;
    }
    return YES;
}
//进入编辑模式，按下出现的编辑按钮后
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    WeakSelf(weakSelf);
    [tableView setEditing:NO animated:YES];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        HZSampleNews *sampleNews = self.sampleNewsArr[indexPath.row];
        NSString *userData = sampleNews.userData;
        NSLog(@"---------userData-------------:%@", userData);
        // 删除缓存的元素
        NSDictionary *newsDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
        NSMutableDictionary *myDict = [NSMutableDictionary dictionaryWithDictionary:newsDict];
        [myDict removeObjectForKey:userData];
        
        // 再存元素
        NSDictionary *vcDict = [NSDictionary dictionaryWithDictionary:myDict];
        [[NSUserDefaults standardUserDefaults] setObject:vcDict forKey:@"profileVCtrNews"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"NEWSLIST-SET3-%@",vcDict);
        //
        //        NSLog(@"------sampleNews.imgPathArr----------------:%@", sampleNews.imgPathArr);
        for (NSString *imgPathStr in sampleNews.imgPathArr) {
            NSFileManager *fileMgr = [NSFileManager defaultManager];
            [fileMgr removeItemAtPath:imgPathStr error:nil];
            //            NSData *data = [NSData dataWithContentsOfFile:imgPathStr];
            //            NSLog(@"------sedx----------------:%@",data);
        }
        // 删除当前数组的元素: 数组的性质发生了变化
        //            [userDataDeleteArr removeObject:sampleNews.userData];
        //            [unReadCountDeleteArr ];
        [self.sampleNewsArr removeObject:sampleNews];
        // 删除数据库里的数据
        [self.messageDB deleteAllMessageWithOrderId:userData];
        //
        NSArray *tempArr = [[HZChatHelper sharedInstance].unReadNewsArr copy];
        
        for (ECMessage *message in tempArr) {
            NSString *userData = (NSString *)message.userData;
            if ([userData isEqualToString:sampleNews.userData]) {
                [[HZChatHelper sharedInstance].unReadNewsArr removeObject:message];
            }
        }
        [self setUnreadMsgCount];
        
        // 一定要是最后的操作
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        self.noNewsListImageView.hidden = self.organGraftingNewsArr.count != 0 || self.sampleNewsArr.count != 0;
    }
}
//修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}
//设置进入编辑状态时，Cell不会缩进
- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [IQKeyboardManager sharedManager].enable = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
