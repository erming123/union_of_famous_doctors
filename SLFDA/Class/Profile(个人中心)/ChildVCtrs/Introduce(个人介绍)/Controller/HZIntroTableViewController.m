//
//  HZIntroduceViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZIntroTableViewController.h"
#import "HZUser.h"
#import "HZMyIntroCell.h"
#import "HZMySkillCell.h"
//
#import "HZUserTool.h"
//#import "IQKeyboardManager.h"
@interface HZIntroTableViewController ()<HZMyIntroCellDelegate, HZMySkillCellDelegate>

//** <#注释#>*/
@property (nonatomic, strong) HZMyIntroCell *myIntroCell;
@property (nonatomic, copy) NSString *myIntroStr;
//** <#注释#>*/
@property (nonatomic, strong) HZMySkillCell *mySkillCell;
@property (nonatomic, copy) NSString *mySkillStr;
//** <#注释#>*/
//@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation HZIntroTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setViewPanBack];
    self.navigationItem.title = @"个人介绍";
    //
    self.tableView.showsVerticalScrollIndicator = NO;
    //
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveMyIntroWord)];
    
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#2DBED8" alpha:1.0]} forState:UIControlStateNormal];
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#4A4A4A" alpha:0.5]} forState:UIControlStateDisabled];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    self.tableView.estimatedRowHeight = 0;
    if (@available(iOS 11.0, *)) {
        //当有heightForHeader delegate时设置
        self.tableView.estimatedSectionHeaderHeight = 0;
        //当有heightForFooter delegate时设置
        self.tableView.estimatedSectionFooterHeight = 0;
    }
    //
//    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}



- (void)setUser:(HZUser *)user {
    _user = user;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
    
        HZMySkillCell *mySkillCell = [HZMySkillCell cellWithTableView:tableView];
        mySkillCell.delegate = self;
        //    myIntroCell.honorStr = self.myIntroStr;
        self.mySkillCell = mySkillCell;
        return mySkillCell;
    }

    HZMyIntroCell *myIntroCell = [HZMyIntroCell cellWithTableView:tableView];
    myIntroCell.delegate = self;
    //    myIntroCell.honorStr = self.myIntroStr;
    self.myIntroCell = myIntroCell;
    return myIntroCell;
    
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return kP(340);
    }
    return kP(500);
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return kP(0.01);
    }else{
        return kP(20);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return kP(0.01);
}

//#pragma mark -- 视图滚动的监控
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    [self.view endEditing:YES];
//}

#pragma mark -- HZMyIntroCellDelegate

- (void)setSaveItemWithEnable:(BOOL)enAble{
    self.navigationItem.rightBarButtonItem.enabled = enAble;
}



#pragma mark -- 保存个人介绍
- (void)saveMyIntroWord {
    NSLog(@"保存");
    
    [MBProgressHUD showOnlyTextToView:nil title:@"正在保存.."];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kIsMySkillStrNew]) {
        //    [self.navigationController popViewControllerAnimated:YES];
        self.mySkillCell.mySkillTextViewResignFirstResponder = YES;
        //    self.mySkillCell.activityIndicatorStartAnimating = YES;
        NSString *skillStrOrign = [[NSUserDefaults standardUserDefaults] objectForKey:kMySkillStr];
        // 判断是否含有emoji
        BOOL isSkillStrContainsEmoji = [NSString stringContainsEmoji:skillStrOrign];
        // 转emojiStr成unicodeStr
        NSString *skillStr = skillStrOrign;
        if (isSkillStrContainsEmoji) {
            skillStr = [self getUnicodeStrWithEmojiStr:skillStrOrign];
        }
        
        [HZUserTool upLoadMySkillWithSkillStr:skillStr userOpenid:self.user.openid success:^(id responseObject) {
            NSLog(@"----upLoadSkill成功-----%@", responseObject);
            
            if ([responseObject[@"state"] isEqual:@200]) {
                self.mySkillCell.activityIndicatorStartAnimating = NO;
                [self.navigationController popViewControllerAnimated:YES];
                
                
                // 缓存
                [[YYCache cacheWithName:@"MyInfo_YYcache"] setObject:skillStrOrign forKey:@"MySkill"];
                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsMySkillStrNew];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"-----upLoadSkill失败----%@", error);
            //        self.mySkillCell.activityIndicatorStartAnimating = NO;
        }];
        
        
    }

    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kIsMyIntroStrNew]) {
        NSString *introduceStrOrign = [[NSUserDefaults standardUserDefaults] objectForKey:kMyIntroStr];
        self.myIntroCell.myIntroTextViewResignFirstResponder = YES;
        //    self.myIntroCell.activityIndicatorStartAnimating = YES;
        
        
        // 判断是否含有emoji
        BOOL isIntrolContainsEmoji = [NSString stringContainsEmoji:introduceStrOrign];
        // 转emojiStr成unicodeStr
        NSString *introduceStr = introduceStrOrign;
        if (isIntrolContainsEmoji) {
            introduceStr = [self getUnicodeStrWithEmojiStr:introduceStrOrign];
        }
        
        
        NSLog(@"----introduceStr-----%@", introduceStr);
        [HZUserTool upLoadMyIntroduceWithIntroduceStr:introduceStr userOpenid:self.user.openid success:^(id responseObject) {
            NSLog(@"----upLoadIntroduce成功-----%@", responseObject);
            
            if ([responseObject[@"state"] isEqual:@200]) {
                self.myIntroCell.activityIndicatorStartAnimating = NO;
                [self.navigationController popViewControllerAnimated:YES];
                // 缓存
                [[YYCache cacheWithName:@"MyInfo_YYcache"] setObject:introduceStrOrign forKey:@"MyHonor"];
                

                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsMyIntroStrNew];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"-----upLoadIntroduce失败----%@", error);
            self.myIntroCell.activityIndicatorStartAnimating = NO;
            //         [self.navigationController popViewControllerAnimated:YES];
        }];
        
        

    }
    
   


    
}



#pragma mark -- getUnicodeStrWithEmojiStr 
- (NSString *)getUnicodeStrWithEmojiStr:(NSString *)emojiStr {
    
    return [emojiStr stringByReplacingEmojiUnicodeWithCheatCodes];
}


//#pragma mark -- viewWillDisappear
//
//- (void)viewWillDisappear:(BOOL)animated {
////    self.myIntroCell.isFailLabelHidden = YES;
////    self.myIntroCell.activityIndicatorStartAnimating = NO;
////    self.mySkillCell.activityIndicatorStartAnimating = NO;
////    [self.tableView reloadData];
////    [IQKeyboardManager sharedManager].enable = NO;
//}
//
//- (void)viewWillAppear:(BOOL)animated {
////     self.myIntroCell.isFailLabelHidden = YES;
////    [IQKeyboardManager sharedManager].enable = YES;
//}
@end
