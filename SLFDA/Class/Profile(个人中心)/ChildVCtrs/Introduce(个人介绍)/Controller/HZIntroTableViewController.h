//
//  HZIntroTableViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;
@interface HZIntroTableViewController : UITableViewController
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
@end
