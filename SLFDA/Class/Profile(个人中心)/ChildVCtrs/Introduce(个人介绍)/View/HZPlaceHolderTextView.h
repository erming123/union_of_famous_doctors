//
//  HZPlaceHolderTextView.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/8/11.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZPlaceHolderTextViewDelegate <NSObject>
- (void)myTextViewDidChange:(UITextView *)textView;
- (BOOL)myTextView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
@end
@interface HZPlaceHolderTextView : UIView
@property (nonatomic, weak) id<HZPlaceHolderTextViewDelegate>delegate;
@property (nonatomic, assign) BOOL myEditable;
@property (nonatomic, strong) UIFont *myFont;
@property (nonatomic, copy) NSString *myText;
@property (nonatomic, copy) NSString *myPlaceHolder;
@property (nonatomic, assign) BOOL isMyPlaceHolderHidden;
@end
