//
//  HZMyIntroCell.m
//  SLIOP
//
//  Created by 季怀斌 on 16/8/19.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZMyIntroCell.h"
#import "HZUser.h"
#import "HZUserTool.h"
#import "HZPlaceHolderTextView.h"

#define kIntroStrCount 800
NS_ASSUME_NONNULL_BEGIN
@interface HZMyIntroCell ()<HZPlaceHolderTextViewDelegate>
@property (nonatomic, strong) UILabel *myIntroLabel;
@property (nonatomic, strong) UIView *showLine;
@property (nonatomic, strong) HZPlaceHolderTextView *myIntroTextView;
@property (nonatomic, strong) UILabel *introStrCountLimtLabel;
@property (nonatomic, copy) NSString *introStr;
@property (nonatomic, assign) NSInteger introStrCount;
@end
NS_ASSUME_NONNULL_END
@implementation HZMyIntroCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpAllSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpAllSubViews {
    
    
    UILabel *myIntroLabel = [UILabel new];
    myIntroLabel.text = @"我的简介";
    myIntroLabel.font = [UIFont systemFontOfSize:kP(32)];
    myIntroLabel.textAlignment = NSTextAlignmentRight;
    myIntroLabel.textColor = [UIColor colorWithHexString:@"#777777" alpha:1.0];
//    myIntroLabel.backgroundColor = [UIColor redColor];
    [myIntroLabel sizeToFit];
    [self addSubview:myIntroLabel];
    self.myIntroLabel = myIntroLabel;
    
    
    UIView *showLine = [UIView new];
    showLine.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    [self addSubview:showLine];
    self.showLine = showLine;
    
    HZPlaceHolderTextView *myIntroTextView = [[HZPlaceHolderTextView alloc] init];
    myIntroTextView.delegate = self;
    myIntroTextView.myFont = [UIFont systemFontOfSize:kP(28)];
    myIntroTextView.myEditable = NO;
    myIntroTextView.myPlaceHolder = @"请填写您的个人简介";
//    myIntroTextView.hidden = YES;
//    myIntroTextView.backgroundColor = [UIColor redColor];
    [self addSubview:myIntroTextView];
    self.myIntroTextView = myIntroTextView;
    
//    
//    UIView *screenView = [UIApplication sharedApplication].keyWindow;
//    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    activityIndicator.center = screenView.center;
//    activityIndicator.y += kP(155);
//    activityIndicator.color = [UIColor grayColor];
//    [activityIndicator startAnimating];
//    [screenView addSubview:activityIndicator];
//    self.activityIndicator = activityIndicator;
//    self.screenView = screenView;
    
    
    UILabel *introStrCountLimtLabel = [[UILabel alloc] init];
    introStrCountLimtLabel.text = [NSString stringWithFormat:@"%d", kIntroStrCount];
    introStrCountLimtLabel.font = [UIFont systemFontOfSize:kP(26)];
    introStrCountLimtLabel.textAlignment = NSTextAlignmentRight;
    introStrCountLimtLabel.textColor = [UIColor colorWithHexString:@"#999999" alpha:1.0];
    [introStrCountLimtLabel sizeToFit];
    [self addSubview:introStrCountLimtLabel];
    self.introStrCountLimtLabel = introStrCountLimtLabel;

    
//    UILabel *failGetLabel = [[UILabel alloc] init];
//    failGetLabel.text = @"暂未获取到个人简介";
//    failGetLabel.font = [UIFont systemFontOfSize:kP(40)];
//    failGetLabel.textAlignment = NSTextAlignmentCenter;
//    failGetLabel.textColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0];
//    [failGetLabel sizeToFit];
//    failGetLabel.hidden = YES;
//    failGetLabel.center = [UIApplication sharedApplication].keyWindow.center;
//    [screenView addSubview:failGetLabel];
//    self.failGetLabel = failGetLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    
    [self getMyHonorStr];
    
    self.showLine.x = kP(40);
    self.showLine.y = kP(22);
    self.showLine.width = kP(8);
    self.showLine.layer.cornerRadius = kP(4);
    self.showLine.height = self.myIntroLabel.height;
    
    self.myIntroLabel.x = self.showLine.right + kP(20);
    self.myIntroLabel.y = self.showLine.y;
    //
    self.introStrCountLimtLabel.width = self.width;
    self.introStrCountLimtLabel.x = self.width - self.introStrCountLimtLabel.width - kP(34);
    self.introStrCountLimtLabel.y = self.height - self.introStrCountLimtLabel.height -kP(22);

    CGFloat myIntroTextViewX = self.myIntroLabel.x;
    CGFloat myIntroTextViewY = self.myIntroLabel.bottom + kP(10);
    CGFloat myIntroTextViewW = self.width - myIntroTextViewX -kP(38);
    CGFloat myIntroTextViewH = self.height - self.introStrCountLimtLabel.height - myIntroTextViewY -kP(38);
    self.myIntroTextView.frame = CGRectMake(myIntroTextViewX, myIntroTextViewY, myIntroTextViewW, myIntroTextViewH);
    
//    self.failGetLabel.x = HZScreenW * 0.5 - self.failGetLabel.width * 0.5;
//    self.failGetLabel.y = HZScreenH * 0.5 + kP(100);
}


- (void)getMyHonorStr {
    
    //缓存
  NSString *result = (NSString*)[[YYCache cacheWithName:@"MyInfo_YYcache"] objectForKey:@"MyHonor"];
    if (result) {
        self.myIntroTextView.myText = result;
        self.introStr = result;
        self.introStrCount = result.length;
        
        self.myIntroTextView.myEditable = YES;
        //                self.myIntroTextView.myPlaceHolder = [NSString isBlankString:honorStr] ? @"你好" : @"";
        self.myIntroTextView.isMyPlaceHolderHidden = ![NSString isBlankString:result];
        return;
    }
   
    [HZUserTool getMyIntroduceWithSuccess:^(id responseObject) {
//        NSLog(@"----getMyIntroduce成功-----%@", responseObject);
        
//        NSDictionary *dict = [NSJSONSerialization dataWithJSONObject:responseObject options:kNilOptions error:nil];
        NSLog(@"---getMyIntroduce成功--ss----%@", [responseObject getDictJsonStr]);
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            __block NSString *honorStr = resultsDict[@"honor"];
            
            NSLog(@"----getMyIntroduceStr成功-----%@", honorStr);
            //            [[NSUserDefaults standardUserDefaults] setObject:honorStr forKey:kMyIntroStr];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                honorStr = [NSString isBlankString:honorStr] ? @"" : honorStr;
                honorStr = [honorStr stringByReplacingEmojiCheatCodesWithUnicode];
                self.myIntroTextView.myText = honorStr;
                self.introStr = honorStr;
                self.introStrCount = honorStr.length;
                
                self.myIntroTextView.myEditable = YES;
                self.myIntroTextView.isMyPlaceHolderHidden = ![NSString isBlankString:honorStr];
                
                //缓存
                [[YYCache cacheWithName:@"MyInfo_YYcache"] setObject:honorStr forKey:@"MyHonor"];
            });
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.myIntroTextView.myEditable = YES;
                self.myIntroTextView.isMyPlaceHolderHidden = NO;
            });
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----getMyIntroduceStr失败-----%@", error);
       
        
        
        self.myIntroTextView.myEditable = YES;
        self.myIntroTextView.isMyPlaceHolderHidden = NO;
    }];
    

}



//- (void)setHonorStr:(NSString *)honorStr {
//    _honorStr = honorStr;
//}

- (void)setMyIntroTextViewResignFirstResponder:(BOOL)myIntroTextViewResignFirstResponder {
    
   myIntroTextViewResignFirstResponder ? [self.myIntroTextView resignFirstResponder] : [self.myIntroTextView becomeFirstResponder];
}


+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}



- (void)myTextViewDidChange:(UITextView *)textView {
    //    NSLog(@"textViewDidChange--------------------textView.text.length:%ld", 150 - textView.text.length);
//    self.myIntroTextView.myPlaceHolder = [NSString isBlankString:textView.text] ? @"你好" : @"";
    
    if (textView.markedTextRange != nil) return;
    
    self.introStrCount = textView.text.length;
    NSString *introLeftCountStr = textView.text.length > kIntroStrCount ? [NSString stringWithFormat:@"超%ld", textView.text.length - kIntroStrCount] : [NSString stringWithFormat:@"剩%ld", kIntroStrCount - textView.text.length];
    
    NSLog(@"textViewDidChange--------------------textView.text.length:%@", textView.text);
    if (textView.text.length > 0) {
        self.introStrCountLimtLabel.text = introLeftCountStr;
        
        
        // 设置保存按钮
        if (self.delegate && [self.delegate respondsToSelector:@selector(setSaveItemWithEnable:)]) {
            [self.delegate setSaveItemWithEnable:YES];
        }
        
        
    } else {
        
        self.introStrCountLimtLabel.text = [NSString stringWithFormat:@"限%d", kIntroStrCount];
        // 设置保存按钮
        if (self.delegate && [self.delegate respondsToSelector:@selector(setSaveItemWithEnable:)]) {
            [self.delegate setSaveItemWithEnable:NO];
        }
        
    }
    
    NSLog(@"---qwerty---:%@-----:%@", self.introStr, textView.text);
    if (![textView.text isEqualToString:self.introStr]) {
        
        // 设置保存按钮
        if (self.delegate && [self.delegate respondsToSelector:@selector(setSaveItemWithEnable:)]) {
            [self.delegate setSaveItemWithEnable:YES];
        }

        
        [[NSUserDefaults standardUserDefaults] setObject:textView.text forKey:kMyIntroStr];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsMyIntroStrNew];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else {
        
        // 设置保存按钮
        if (self.delegate && [self.delegate respondsToSelector:@selector(setSaveItemWithEnable:)]) {
            [self.delegate setSaveItemWithEnable:NO];
        }

        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsMyIntroStrNew];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (BOOL)myTextView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSLog(@"--------========--");
    
    if ([NSString isBlankString:text]) {
        
        
        //删除字符肯定是安全的
        return YES;
    } else {
        NSLog(@"--+-----%ld", textView.text.length - range.length + text.length);
        
        if (textView.text.length - range.length + text.length > kIntroStrCount && textView.text.length - range.length + text.length > self.introStrCount) {
        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"超出最大可输入长度" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alert show];
            
                        NSLog(@"超出最大可输入长度");
            return NO;
        }
        
        else {
            return YES;
        }
     }
    
    
    return YES;
}
@end
