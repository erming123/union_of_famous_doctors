//
//  HZMySkillCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZMySkillCell.h"
#import "HZUser.h"
#import "HZUserTool.h"
#import "HZPlaceHolderTextView.h"

#define kSkillStrCount 200
NS_ASSUME_NONNULL_BEGIN
@interface HZMySkillCell ()<HZPlaceHolderTextViewDelegate>
@property (nonatomic, strong) UILabel *mySkillLabel;
@property (nonatomic, strong) UIView *showLine;
@property (nonatomic, strong) HZPlaceHolderTextView *mySkillTextView;
@property (nonatomic, strong) UILabel *skillStrCountLimtLabel;
@property (nonatomic, copy) NSString *skillStr;
@property (nonatomic, assign) NSInteger skillStrCount;
@end
NS_ASSUME_NONNULL_END
@implementation HZMySkillCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpAllSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpAllSubViews {
    
    
    UILabel *mySkillLabel = [UILabel new];
    mySkillLabel.text = @"我的擅长";
    mySkillLabel.font = [UIFont systemFontOfSize:kP(32)];
    mySkillLabel.textAlignment = NSTextAlignmentRight;
    mySkillLabel.textColor = [UIColor colorWithHexString:@"#777777" alpha:1.0];
    [mySkillLabel sizeToFit];
    [self addSubview:mySkillLabel];
    self.mySkillLabel = mySkillLabel;
    
    
    UIView *showLine = [UIView new];
    showLine.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    [self addSubview:showLine];
    self.showLine = showLine;
    
    HZPlaceHolderTextView *mySkillTextView = [[HZPlaceHolderTextView alloc] init];
    mySkillTextView.delegate = self;
    mySkillTextView.myFont = [UIFont systemFontOfSize:kP(28)];
    mySkillTextView.myEditable = NO;
    mySkillTextView.myPlaceHolder = @"请填写您所擅长的医学内容";
    [self addSubview:mySkillTextView];
    self.mySkillTextView = mySkillTextView;

    
    
    UILabel *skillStrCountLimtLabel = [[UILabel alloc] init];
    skillStrCountLimtLabel.text = [NSString stringWithFormat:@"%d", kSkillStrCount];
    skillStrCountLimtLabel.font = [UIFont systemFontOfSize:kP(26)];
    skillStrCountLimtLabel.textAlignment = NSTextAlignmentRight;
    skillStrCountLimtLabel.textColor = [UIColor colorWithHexString:@"#999999" alpha:1.0];
    [skillStrCountLimtLabel sizeToFit];
    [self addSubview:skillStrCountLimtLabel];
    self.skillStrCountLimtLabel = skillStrCountLimtLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    [self getMySkillStr];
    
    //
    self.showLine.x = kP(40);
    self.showLine.y = kP(22);
    self.showLine.width = kP(8);
    self.showLine.layer.cornerRadius = kP(4);
    self.showLine.height = self.mySkillLabel.height;
    
    self.mySkillLabel.x = self.showLine.right + kP(20);
    self.mySkillLabel.y = self.showLine.y;
    
    //
    self.skillStrCountLimtLabel.width = self.width;
    self.skillStrCountLimtLabel.x = self.width - self.skillStrCountLimtLabel.width - kP(34);
    self.skillStrCountLimtLabel.y = self.height - self.skillStrCountLimtLabel.height -kP(22);

//    self.mySkillTextView.text = self.user.speciality;
    
    
    CGFloat mySkillTextViewX = self.mySkillLabel.x;
    CGFloat mySkillTextViewY = self.mySkillLabel.bottom + kP(10);
    CGFloat mySkillTextViewW = self.width - mySkillTextViewX - kP(38);
    CGFloat mySkillTextViewH = self.height - self.skillStrCountLimtLabel.height - mySkillTextViewY - kP(38);
    self.mySkillTextView.frame = CGRectMake(mySkillTextViewX, mySkillTextViewY, mySkillTextViewW, mySkillTextViewH);
}


- (void)getMySkillStr {
    
    NSString *result = (NSString*)[[YYCache cacheWithName:@"MyInfo_YYcache"] objectForKey:@"MySkill"];
    if (result) {
        self.mySkillTextView.myText = result;
        self.skillStr = result;
        self.skillStrCount = result.length;
        //
        self.mySkillTextView.myEditable = YES;
        //
        self.mySkillTextView.isMyPlaceHolderHidden = ![NSString isBlankString:result];
        return;
    }
    
    [HZUserTool getMySkillWithSuccess:^(id responseObject) {
//        NSLog(@"----getMySkill成功-----%@", responseObject);
        NSLog(@"---getMySkill成功------%@", [responseObject getDictJsonStr]);
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSString *skillStr = resultsDict[@"speciality"];
            
            NSLog(@"----getMySkill成功-----%@", skillStr);
            skillStr = [NSString isBlankString:skillStr] ? @"" : skillStr;
            skillStr = [skillStr stringByReplacingEmojiCheatCodesWithUnicode];
            self.mySkillTextView.myText = skillStr;
            self.skillStr = skillStr;
            self.skillStrCount = skillStr.length;
            //
            self.mySkillTextView.myEditable = YES;
            //
            self.mySkillTextView.isMyPlaceHolderHidden = ![NSString isBlankString:skillStr];
    
            //缓存
            [[YYCache cacheWithName:@"MyInfo_YYcache"] setObject:skillStr forKey:@"MySkill"];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.mySkillTextView.myEditable = YES;
                self.mySkillTextView.isMyPlaceHolderHidden = NO;
            });
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----getMySkill失败-----%@", error);
        
        //
        self.mySkillTextView.myEditable = YES;
        self.mySkillTextView.isMyPlaceHolderHidden = NO;
    }];
    

}

//- (void)setUser:(HZUser *)user {
//    _user = user;
//}

- (void)setMySkillTextViewResignFirstResponder:(BOOL)mySkillTextViewResignFirstResponder {
    mySkillTextViewResignFirstResponder ? [self.mySkillTextView resignFirstResponder] : [self.mySkillTextView becomeFirstResponder];
}


+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}


#pragma mark -- myTextViewDidChange
- (void)myTextViewDidChange:(UITextView *)textView {
    

    if (textView.markedTextRange != nil) return;
    //    NSLog(@"textViewDidChange--------------------textView.text.length:%ld", 150 - textView.text.length);
    self.skillStrCount = textView.text.length;
    NSString *skillStrLeftCountStr = textView.text.length > kSkillStrCount ? [NSString stringWithFormat:@"超%ld", textView.text.length - kSkillStrCount] : [NSString stringWithFormat:@"剩%ld", kSkillStrCount - textView.text.length];
    
    
    NSLog(@"textViewDidEndEditing--------------------textView.text.length:%@", textView.text);
    if (textView.text.length > 0) {
        self.skillStrCountLimtLabel.text = skillStrLeftCountStr;
        
        
        // 设置保存按钮
        if (self.delegate && [self.delegate respondsToSelector:@selector(setSaveItemWithEnable:)]) {
            [self.delegate setSaveItemWithEnable:YES];
        }
        
        
    } else {
        self.skillStrCountLimtLabel.text = [NSString stringWithFormat:@"限%d", kSkillStrCount];
        // 设置保存按钮
        if (self.delegate && [self.delegate respondsToSelector:@selector(setSaveItemWithEnable:)]) {
            [self.delegate setSaveItemWithEnable:NO];
        }
        
    }
    
    if (![textView.text isEqualToString:self.skillStr]) {
        
        // 设置保存按钮
        if (self.delegate && [self.delegate respondsToSelector:@selector(setSaveItemWithEnable:)]) {
            [self.delegate setSaveItemWithEnable:YES];
        }
        [[NSUserDefaults standardUserDefaults] setObject:textView.text forKey:kMySkillStr];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsMySkillStrNew];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        
        // 设置保存按钮
        if (self.delegate && [self.delegate respondsToSelector:@selector(setSaveItemWithEnable:)]) {
            [self.delegate setSaveItemWithEnable:NO];
        }
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kIsMySkillStrNew];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}



#pragma mark -- myTextView
- (BOOL)myTextView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {


    if ([NSString isBlankString:text]) {


        //删除字符肯定是安全的
        return YES;
    } else {


        if (textView.text.length - range.length + text.length > kSkillStrCount) {

                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"超出最大可输入长度" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
                        [alert show];

                        NSLog(@"超出最大可输入长度");
            return NO;
        }

        else {
            return YES;
        }
     }


    return YES;
}

@end
