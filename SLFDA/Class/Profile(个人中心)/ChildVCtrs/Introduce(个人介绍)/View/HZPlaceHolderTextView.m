//
//  HZPlaceHolderTextView.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/8/11.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPlaceHolderTextView.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZPlaceHolderTextView ()<UITextViewDelegate>
@property (nonatomic, strong) UITextView *myTextView;
@property (nonatomic, strong) UILabel *placeHolderLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZPlaceHolderTextView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setMySubViews];
    }
    return self;
}

- (UITextView *)myTextView {
    if (!_myTextView) {
        _myTextView = [UITextView new];
        _myTextView.delegate = self;
        //    myTextView.font = [UIFont systemFontOfSize:kP(32)];
        //    myTextView.editable = NO;
        //    myTextView.backgroundColor = [UIColor greenColor];
    }
    
    return _myTextView;
}

- (void)setMySubViews {
    
//    UITextView *myTextView = [UITextView new];
//    myTextView.delegate = self;
////    myTextView.font = [UIFont systemFontOfSize:kP(32)];
////    myTextView.editable = NO;
////    myTextView.backgroundColor = [UIColor greenColor];
//    [self addSubview:myTextView];
//    self.myTextView = myTextView;
    
    [self addSubview:self.myTextView];
    
    //2.
    UILabel *placeHolderLabel = [UILabel new];
    placeHolderLabel.hidden = YES;
    [self.myTextView addSubview:placeHolderLabel];
    self.placeHolderLabel = placeHolderLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //
    [self setSubViewsContent];
    
    //
    self.myTextView.frame = self.bounds;
    
    //
    CGFloat placeHolderLabelX = kP(10);
    CGFloat placeHolderLabelY = kP(16);
    CGFloat placeHolderLabelW = self.width;
    CGFloat placeHolderLabelH = [self.placeHolderLabel.text getTextHeightWithMaxWidth:self.width textFontSize:kP(29)];
    self.placeHolderLabel.frame = CGRectMake(placeHolderLabelX, placeHolderLabelY, placeHolderLabelW, placeHolderLabelH);
}


- (void)setSubViewsContent {
    
    self.placeHolderLabel.text = _myPlaceHolder;
    self.placeHolderLabel.textColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0];
    self.placeHolderLabel.font = [UIFont systemFontOfSize:kP(28)];
    self.placeHolderLabel.numberOfLines = 0;
}

- (void)setMyPlaceHolder:(NSString *)myPlaceHolder {
    _myPlaceHolder = myPlaceHolder;
}

- (void)setIsMyPlaceHolderHidden:(BOOL)isMyPlaceHolderHidden {
   
    self.placeHolderLabel.hidden = isMyPlaceHolderHidden;
//    [self layoutIfNeeded];
}
#pragma mark -- set

- (void)setMyFont:(UIFont *)myFont {
    self.myTextView.font = myFont;
}

- (void)setMyEditable:(BOOL)myEditable {
    self.myTextView.editable = myEditable;
}

- (void)setMyText:(NSString *)myText {
    self.myTextView.text = myText;
}
#pragma mark -- textViewDidChange
- (void)textViewDidChange:(UITextView *)textView {
    
//    self.isMyPlaceHolderHidden = ![NSString isBlankString:textView.text];
    self.isMyPlaceHolderHidden = textView.text.length != 0;
    if (self.delegate && [self.delegate respondsToSelector:@selector(myTextViewDidChange:)]) {
        [self.delegate myTextViewDidChange:textView];
    }

}

#pragma mark -- textView
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    BOOL flag = false;
    if (self.delegate && [self.delegate respondsToSelector:@selector(myTextView:shouldChangeTextInRange:replacementText:)]) {
       flag = [self.delegate myTextView:textView shouldChangeTextInRange:range replacementText:text];
    }

    return flag;
}

@end
