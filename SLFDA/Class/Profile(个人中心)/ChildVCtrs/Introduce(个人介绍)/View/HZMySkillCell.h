//
//  HZMySkillCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kMySkillStr @"mySkillStr"
#define kIsMySkillStrNew @"isMySkillStrNew"
//@class HZUser;
@protocol HZMySkillCellDelegate <NSObject>

- (void)setSaveItemWithEnable:(BOOL)enAble;

@end
@interface HZMySkillCell : UITableViewCell
@property (nonatomic, weak) id<HZMySkillCellDelegate>delegate;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
////** <#注释#>*/
//@property (nonatomic, strong) HZUser *user;

@property (nonatomic, assign) BOOL mySkillTextViewResignFirstResponder;
//** <#注释#>*/
@property (nonatomic, assign) BOOL activityIndicatorStartAnimating;
//** <#注释#>*/
@property (nonatomic, assign) BOOL isFailLabelHidden;
@end
