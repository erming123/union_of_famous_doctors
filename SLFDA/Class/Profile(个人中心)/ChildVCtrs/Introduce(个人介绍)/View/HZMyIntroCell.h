//
//  HZMyIntroCell.h
//  SLIOP
//
//  Created by 季怀斌 on 16/8/19.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kMyIntroStr @"kmyIntro"
#define kIsMyIntroStrNew @"isMyIntroStrNew"
@protocol HZMyIntroCellDelegate <NSObject>

- (void)setSaveItemWithEnable:(BOOL)enAble;
@end
@interface HZMyIntroCell : UITableViewCell
@property (nonatomic, weak) id<HZMyIntroCellDelegate>delegate;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//////** <#注释#>*/
//@property (nonatomic, copy) NSString *honorStr;

@property (nonatomic, assign) BOOL myIntroTextViewResignFirstResponder;
//** <#注释#>*/
@property (nonatomic, assign) BOOL activityIndicatorStartAnimating;
//** <#注释#>*/
@property (nonatomic, assign) BOOL isFailLabelHidden;
@end
