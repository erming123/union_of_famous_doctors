//
//  HZAccountSettingViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/5/18.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZAccountSettingViewController : HZViewController
- (void)clearAppMemory;
@end
