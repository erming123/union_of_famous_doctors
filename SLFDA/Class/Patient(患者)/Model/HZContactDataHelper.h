//
//  HZContactDataHelper.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/5.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, HZOrderType) {
   HZOrderTypeAscending = 0, // 升序
   HZOrderTypeDescending, // 降序
   HZOrderTypeDefault = HZOrderTypeAscending
};

@interface HZContactDataHelper : NSObject

// 按照姓名排序
+ (NSMutableArray *) getStructuredPatientListDataBy:(NSMutableArray *)array;

// 获取姓名分区
+ (NSMutableArray *) getPatientListSectionBy:(NSMutableArray *)array;
// 床号排序
+ (NSMutableArray *) getSampleOrderPatientListBy:(NSMutableArray *)array orderType:(HZOrderType)orderType;

// 获取用户的病人
+ (NSMutableArray *) getUserPatientListWithUserName:(NSString *)userName originDataList:(NSMutableArray *)array;
@end
