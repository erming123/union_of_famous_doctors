//
//  HZPatientListCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/5.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientListCell.h"
#import "HZPatient.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZPatientListCell ()
@property (nonatomic, weak) UIImageView *patientAvatarImageView;
@property (nonatomic, weak) UILabel *patientNameLabel;

@property (nonatomic, weak) UIImageView *patientBedCodeImageView;
@property (nonatomic, weak) UILabel *sickBedCodeContentLabel;
@property (nonatomic, weak) UIImageView *doctorAvtarImageView;
@property (nonatomic, weak) UILabel *doctorNameContentLabel;

@property (nonatomic, weak) UIView *tapBgView;
@property (nonatomic, weak) UIImageView *myFollowPatientImageView;
@property (nonatomic, assign) BOOL starred;
@end
NS_ASSUME_NONNULL_END
@implementation HZPatientListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    
    
    //-- 患者头像图片
    UIImageView *patientAvatarImageView = [UIImageView new];
    patientAvatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:patientAvatarImageView];
    self.patientAvatarImageView = patientAvatarImageView;
    
    //--患者姓名
    UILabel *patientNameLabel = [UILabel new];
    patientNameLabel.font = [UIFont systemFontOfSize:kP(32)];
    patientNameLabel.textColor = [UIColor colorWithHexString:@"#333333" alpha:1.0];
    [self addSubview:patientNameLabel];
    self.patientNameLabel = patientNameLabel;

    //-- 患者床号图片
    UIImageView *patientBedCodeImageView = [UIImageView new];
    patientBedCodeImageView.image = [UIImage imageNamed:@"follow"];
    patientBedCodeImageView.userInteractionEnabled = YES;
    [self addSubview:patientBedCodeImageView];
    self.patientBedCodeImageView = patientBedCodeImageView;
  
    //-- 患者床号
    UILabel *sickBedCodeContentLabel = [UILabel new];
    sickBedCodeContentLabel.font = [UIFont systemFontOfSize:kP(28)];
    sickBedCodeContentLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
    [sickBedCodeContentLabel sizeToFit];
    [self addSubview:sickBedCodeContentLabel];
    self.sickBedCodeContentLabel = sickBedCodeContentLabel;
    
    
    
    //-- 医生头像图片
    UIImageView *doctorAvtarImageView = [UIImageView new];
    doctorAvtarImageView.image = [UIImage imageNamed:@"follow"];
    doctorAvtarImageView.userInteractionEnabled = YES;
    [self addSubview:doctorAvtarImageView];
    self.doctorAvtarImageView = doctorAvtarImageView;
    
    
    //-- 医生姓名
    UILabel *doctorNameContentLabel = [UILabel new];
    doctorNameContentLabel.font = [UIFont systemFontOfSize:kP(28)];
    doctorNameContentLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
    [sickBedCodeContentLabel sizeToFit];
    [self addSubview:doctorNameContentLabel];
    self.doctorNameContentLabel = doctorNameContentLabel;
    

    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToAction:)];
    
    //
    UIView *tapBgView = [UIView new];
    [tapBgView addGestureRecognizer:tap];
    [self addSubview:tapBgView];
    self.tapBgView = tapBgView;
    
    UIImageView *myFollowPatientImageView = [UIImageView new];
    myFollowPatientImageView.image = [UIImage imageNamed:@"follow"];
    myFollowPatientImageView.userInteractionEnabled = YES;
    [self.tapBgView addSubview:myFollowPatientImageView];
    self.myFollowPatientImageView = myFollowPatientImageView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self setContent];
    
    //1.
    CGFloat patientAvatarImageViewWH = kP(80);
    CGFloat patientAvatarImageViewX = kP(28);
    CGFloat patientAvatarImageViewY = self.height * 0.5 - patientAvatarImageViewWH * 0.5;
    self.patientAvatarImageView.frame = CGRectMake(patientAvatarImageViewX, patientAvatarImageViewY, patientAvatarImageViewWH, patientAvatarImageViewWH);
    
    
    //2.
    [self.patientNameLabel sizeToFit];
    self.patientNameLabel.x = self.patientAvatarImageView.right + kP(30);
    self.patientNameLabel.y = self.patientAvatarImageView.centerY - self.patientNameLabel.height - kP(5);
    
    //3.
    [self.patientBedCodeImageView sizeToFitWithImage];
    self.patientBedCodeImageView.x = self.patientNameLabel.x;
    self.patientBedCodeImageView.y = self.patientAvatarImageView.centerY + kP(5);
    
    //4.
    [self.sickBedCodeContentLabel sizeToFit];
    self.sickBedCodeContentLabel.x = self.patientBedCodeImageView.right + kP(20);
    self.sickBedCodeContentLabel.y = self.patientBedCodeImageView.centerY - self.sickBedCodeContentLabel.height * 0.5;
    
    
    //5.
    [self.doctorAvtarImageView sizeToFitWithImage];
    self.doctorAvtarImageView.x = self.patientBedCodeImageView.right + kP(140);
    self.doctorAvtarImageView.y = self.patientBedCodeImageView.centerY - self.doctorAvtarImageView.height * 0.5;
    
    
    //6.
    [self.doctorNameContentLabel sizeToFit];
    self.doctorNameContentLabel.x = self.doctorAvtarImageView.right + kP(12);
    self.doctorNameContentLabel.y = self.doctorAvtarImageView.centerY - self.doctorNameContentLabel.height * 0.5;
    
    if (!self.isShowStarBtn) return;
    
    //5.
    CGFloat tapBgViewWH = self.height;
    CGFloat tapBgViewX = self.width - tapBgViewWH - kP(60);
    CGFloat tapBgViewY = self.height * 0.5 - tapBgViewWH * 0.5;
    self.tapBgView.frame = CGRectMake(tapBgViewX, tapBgViewY, tapBgViewWH, tapBgViewWH);
    
    //6.
    CGFloat myFollowPatientImageViewWH = kP(44);
    CGFloat myFollowPatientImageViewX = self.tapBgView.width * 0.5 - myFollowPatientImageViewWH *0.5;
    CGFloat myFollowPatientImageViewY = self.tapBgView.height * 0.5 - myFollowPatientImageViewWH * 0.5;
    self.myFollowPatientImageView.frame = CGRectMake(myFollowPatientImageViewX, myFollowPatientImageViewY, myFollowPatientImageViewWH, myFollowPatientImageViewWH);
}

- (void)setPatient:(HZPatient *)patient {
    _patient = patient;
}

- (void)setIsShowStarBtn:(BOOL)isShowStarBtn {
    _isShowStarBtn = isShowStarBtn;
}
#pragma mark -- setContent

- (void)setContent {
    self.patientAvatarImageView.image = [self.patient.gender isEqualToString:@"男"] ? [UIImage imageNamed:@"malePatient"] : [UIImage imageNamed:@"femalePatient"];
    self.patientNameLabel.text = self.patient.name;
    self.patientBedCodeImageView.image = [UIImage imageNamed:@"bedCode"];
    self.sickBedCodeContentLabel.text = self.patient.bedNum;
    self.doctorAvtarImageView.image = [UIImage imageNamed:@"patientDoctorAvatar"];
    self.doctorNameContentLabel.text = self.patient.docName;
    self.myFollowPatientImageView.image = self.patient.starred?[UIImage imageNamed:@"followSel"]:[UIImage imageNamed:@"follow"];
    self.starred = self.patient.starred;
}

#pragma mark -- tapToAction
- (void)tapToAction:(UITapGestureRecognizer *)tap {
    self.starred = !self.starred;
    self.myFollowPatientImageView.image = self.starred ? [UIImage imageNamed:@"followSel"] : [UIImage imageNamed:@"follow"];
    
    //
    self.attentionClick(self.starred);
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"patientListCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
