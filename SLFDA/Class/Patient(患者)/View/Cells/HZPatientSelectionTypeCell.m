//
//  HZPatientSelectionTypeCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/16.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientSelectionTypeCell.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZPatientSelectionTypeCell ()
@property (nonatomic, weak) UILabel *selectionTypeLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZPatientSelectionTypeCell

//
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
//    UIImageView *arrowImageView = [UIImageView new];
//    arrowImageView.hidden = YES;
//    [self addSubview:arrowImageView];
//    self.arrowImageView = arrowImageView;
    
    UILabel *selectionTypeLabel = [UILabel new];
    selectionTypeLabel.textColor = [UIColor colorWithHexString:@"#666666"];
    selectionTypeLabel.font = [UIFont systemFontOfSize:kP(28)];
    [self.contentView addSubview:selectionTypeLabel];
    self.selectionTypeLabel = selectionTypeLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
//    //2.
//    CGFloat arrowImageViewW = kP(17);
//    CGFloat arrowImageViewH = kP(20);
//    CGFloat arrowImageViewX = kP(34);
//    CGFloat arrowImageViewY = self.height * 0.5 - arrowImageViewH * 0.5;
//    self.arrowImageView.frame = CGRectMake(arrowImageViewX, arrowImageViewY, arrowImageViewW, arrowImageViewH);
    
    //3.
//    self.professionLabel.x = CGRectGetMaxX(self.arrowImageView.frame) + kP(30);
//    self.professionLabel.y = self.height * 0.5 - self.professionLabel.height * 0.5;
    
    //2.
    [self.selectionTypeLabel sizeToFit];
    self.selectionTypeLabel.x = kP(32);
    self.selectionTypeLabel.y = self.contentView.height * 0.5 - self.selectionTypeLabel.height * 0.5;
}

- (void)setSubViewsContent {
    
//    self.arrowImageView.image = [UIImage imageNamed:@"arrow_selType.png"];
    //
    self.selectionTypeLabel.text = _selTypeStr;
    self.selectionTypeLabel.font = [UIFont systemFontOfSize:kP(32)];
}

//#pragma mark -- set
- (void)setSelTypeStr:(NSString *)selTypeStr {
    _selTypeStr = selTypeStr;
}
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"patientSelectionTypeCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

@end
