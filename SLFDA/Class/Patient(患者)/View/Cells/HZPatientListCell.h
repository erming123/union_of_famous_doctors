//
//  HZPatientListCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/5.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZPatient;
@interface HZPatientListCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//** <#注释#>*/
@property (nonatomic, strong) HZPatient *patient;

@property (nonatomic,copy) void(^attentionClick)(BOOL starred);
@property (nonatomic, assign) BOOL isShowStarBtn;
@end
