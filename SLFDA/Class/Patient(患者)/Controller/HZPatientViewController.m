//
//  HZPatientViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientViewController.h"
#import "HZPatientListCell.h"

#import "HZPatient.h"
#import "HZContactDataHelper.h"
//
#import "HZCheckListViewController.h"
#import "HZPatientSearchViewController.h"
//
#import "HZPatientTool.h"
#import "HZUser.h"
// 患者资料
#import "HZPatientInforViewController.h"
// 资质审核
#import "HZAptitudeImageGetViewController.h"
//
#import "MJRefresh.h"

//
#import "HZButton.h"
#import "HZPatientSelectionTypeCell.h"
#define kIndexH 18

typedef struct sectionY {
    float minY;
    float maxY;
} sectionY;
@interface HZPatientViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) UITableView *patientTableView;


//** 展示第几个的大label*/
@property (nonatomic, weak) UILabel *indexShowLabel;
@property (nonatomic, strong) NSMutableArray *indexArr;

//** 索引的背景视图*/
@property (nonatomic, weak) UIView *indexBarView;
@property (nonatomic, strong) NSMutableArray *indexLabelArr;
//** 索引的label*/
@property (nonatomic, strong) UILabel *indexLabel;


// 数组
@property (nonatomic, strong) NSMutableArray *patientsOriginArr;
// 含有分区的数组
@property (strong,nonatomic) NSMutableArray *patientStructArr;

@property (nonatomic,strong) NSMutableArray *offsetSectionArray;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *bedNumOrderPatientArr;
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
@property (nonatomic, weak) UIImageView *noPatientsImageView;
//
@property (nonatomic, weak) UIImageView *avatarImageView;
@property (nonatomic, weak) UILabel *alertLabel;
@property (nonatomic, weak) UIButton *toAttestationBtn;

//
@property (nonatomic, weak) HZButton *patientSourceTypeBtn;
@property (nonatomic, weak) HZButton *patientPropertTypeBtn;
//
@property (nonatomic, weak) UIButton *patientSearchBtn;
@property (nonatomic, weak) UIView *tapBgView;
@property (nonatomic, weak) UITableView *selTypeTableView;
//
@property (nonatomic, strong) NSArray *sourceTypeStrArr;
@property (nonatomic, strong) NSArray *propertyStrArr;

@property (nonatomic, copy) NSString *oldSourceTypeStr;
@property (nonatomic, copy) NSString *oldPropertyStr;

//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *userPatientArrByPatientName;
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *userPatientArrByBedCodeAscending;
@end
@implementation HZPatientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"患者管理";
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [self setUpSubViews];
    //
    self.patientStructArr = [NSMutableArray array];
    self.patientsOriginArr = [NSMutableArray array];
    self.indexArr = [NSMutableArray array];
    
    //
    if ([self.user.authenticationState isEqualToString:@"AUTHENTICATED"]) {
        [self updateMyDataWithIsShowAlertView:YES];
        [self pullOrPushToRefresh];
    }
    
    
    self.sourceTypeStrArr = @[@"科室患者", @"我的患者"];
    self.propertyStrArr = @[@"名字排序", @"床号排序"];
    
    self.oldSourceTypeStr = @"科室患者";
    self.oldPropertyStr = @"名字排序";
}


#pragma mark -- setUpSubViews
- (void)setUpSubViews {
    
    NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
    self.user = user;
    
    //
    UITableView *patientTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    patientTableView.dataSource = self;
    patientTableView.delegate = self;
    patientTableView.tableFooterView = [UIView new];
    //    patientTableView.separatorInset = UIEdgeInsetsZero;
    patientTableView.showsVerticalScrollIndicator = NO;
    patientTableView.height = self.view.height - SafeAreaBottomHeight;
    if (@available(iOS 11.0, *)) {
        patientTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        patientTableView.contentInset = UIEdgeInsetsMake(SafeAreaTopHeight, 0, 49, 0);
        patientTableView.scrollIndicatorInsets = patientTableView.contentInset;
    }
    [self.view addSubview:patientTableView];
    self.patientTableView = patientTableView;
    
    // 设置下tableHeaderView
    //
    UIView *btnBgView = [self setMyTableHeaderView];
    self.patientTableView.tableHeaderView = btnBgView;
    self.patientTableView.tableHeaderView.hidden = YES;
    //
    //
    if (![self.user.authenticationState isEqualToString:@"AUTHENTICATED"] ) {
        
        CGFloat avatarImageViewWH = kP(190);
        CGFloat avatarImageViewX = self.view.width * 0.5 - avatarImageViewWH * 0.5;
        CGFloat avatarImageViewY = kP(304);
        UIImageView *avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(avatarImageViewX, avatarImageViewY, avatarImageViewWH, avatarImageViewWH)]; //
        avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
        avatarImageView.image = [UIImage imageNamed:@"doctor_head"];
        [self.patientTableView addSubview:avatarImageView];
        self.avatarImageView = avatarImageView;
        
        UILabel *alertLabel = [UILabel new];
        alertLabel.font = [UIFont systemFontOfSize:kP(32)];
        alertLabel.textColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0];
        alertLabel.text = @"您的医生资质尚未认证";
        [alertLabel sizeToFit];
        alertLabel.x = self.avatarImageView.centerX - alertLabel.width * 0.5;
        alertLabel.y = self.avatarImageView.bottom + kP(44);
        [self.patientTableView addSubview:alertLabel];
        self.alertLabel = alertLabel;
        
        
        UIButton *toAttestationBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        toAttestationBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
        [toAttestationBtn setTitle:@"立即认证" forState:UIControlStateNormal];
        [toAttestationBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
        //        toAttestationBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        [toAttestationBtn sizeToFit];
        toAttestationBtn.x = self.alertLabel.centerX - toAttestationBtn.width * 0.5;
        toAttestationBtn.y = self.alertLabel.bottom + kP(20);
        [toAttestationBtn addTarget:self action:@selector(enterToAttestation:) forControlEvents:UIControlEventTouchUpInside];
        [self.patientTableView addSubview:toAttestationBtn];
        self.toAttestationBtn = toAttestationBtn;
        return;
    }
    
    if (self.avatarImageView) {
        [self.avatarImageView removeFromSuperview];
        self.avatarImageView = nil;
    }
    
    if (self.alertLabel) {
        [self.alertLabel removeFromSuperview];
        self.alertLabel = nil;
    }
    
    if (self.toAttestationBtn) {
        [self.toAttestationBtn removeFromSuperview];
        self.alertLabel = nil;
    }
    
    
    
    
    //3. 没有患者的时候
    UIImageView *noPatientsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(340), kP(372))];
    noPatientsImageView.image = [UIImage imageNamed:@"noPatients"];
    noPatientsImageView.center = self.view.center;
    noPatientsImageView.y = kP(600);
    noPatientsImageView.hidden = YES;
    [self.view addSubview:noPatientsImageView];
    self.noPatientsImageView = noPatientsImageView;
    noPatientsImageView.center = self.view.center;
    
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    //
    //
    UIButton *patientSearchBtn = [UIButton new];
    patientSearchBtn.frame = CGRectMake(0, 0, kP(80), kP(80));
    //    patientSearchBtn.backgroundColor = [UIColor redColor];
    [patientSearchBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [patientSearchBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateHighlighted];
    patientSearchBtn.contentHorizontalAlignment  = UIControlContentHorizontalAlignmentRight;
    [patientSearchBtn addTarget:self action:@selector(enterPatientSearchVCtr:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:patientSearchBtn];
    UIBarButtonItem *itemSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    itemSpacer.width = - kP(10);
    patientSearchBtn.hidden = YES;
    self.navigationItem.rightBarButtonItems = @[rightItem, itemSpacer];
    self.patientSearchBtn = patientSearchBtn;
    
    
    //
    UIView *tapBgView = [[UIView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight + kP(80), HZScreenW, HZScreenH)];
    tapBgView.backgroundColor = [UIColor colorWithHexString:@"#191C18" alpha:0.5];
    UITapGestureRecognizer *bgViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenTypeBgView)];
    [tapBgView addGestureRecognizer:bgViewTap];
    tapBgView.hidden = YES;
    [[UIApplication sharedApplication].keyWindow addSubview:tapBgView];
    self.tapBgView = tapBgView;
    
    
    //
    UITableView *selTypeTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    selTypeTableView.dataSource = self;
    selTypeTableView.delegate = self;
    selTypeTableView.tableFooterView = [UIView new];
    //    patientTableView.separatorInset = UIEdgeInsetsZero;
    selTypeTableView.showsVerticalScrollIndicator = NO;
    selTypeTableView.y = SafeAreaTopHeight + kP(80);
    selTypeTableView.height = kP(0);
    selTypeTableView.scrollEnabled = NO;
    [[UIApplication sharedApplication].keyWindow addSubview:selTypeTableView];
    self.selTypeTableView = selTypeTableView;
}


- (NSMutableArray *)bedNumOrderPatientArr {
    
    if (!_bedNumOrderPatientArr) {
        _bedNumOrderPatientArr = [NSMutableArray array];
    }
    
    return _bedNumOrderPatientArr;
}
// 同步更新数据
- (void)updateMyDataWithIsShowAlertView:(BOOL)isShowAlertView {
    
    
    if (isShowAlertView) {
        [self.activityIndicator startAnimating];
    }
    
    self.patientTableView.tableHeaderView.hidden = !self.patientsOriginArr.count;
    
    
    //
    [HZPatientTool getPatientsWithHospitalId:self.user.hospitalId staffNum:self.user.number success:^(id  _Nullable responseObject) {
        
        
        NSLog(@"---------patientsS--------:%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSArray *patientsTempArr = resultsDict[@"patients"];
            
            //
            [[NSUserDefaults standardUserDefaults] setObject:patientsTempArr forKey:@"patientList"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if (patientsTempArr.count == 0) {
                
                
                [self.activityIndicator stopAnimating];
                self.noPatientsImageView.hidden = NO;
                self.patientSearchBtn.hidden = YES;
                self.indexBarView.hidden = YES;
                self.patientTableView.tableHeaderView.hidden = YES;
                [self.view makeToast:@"您在本院尚无对应的患者" duration:1.5 position:CSToastPositionBottom];
                [self.patientTableView reloadData];
                return;
            }
            
            //
            NSArray *patientArr = [HZPatient mj_objectArrayWithKeyValuesArray:patientsTempArr];
            
            
            //
            [self.patientsOriginArr removeAllObjects];
            [self.patientsOriginArr addObjectsFromArray:patientArr];
            
            //
            if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
                
                if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
                    self.patientStructArr = [HZContactDataHelper getStructuredPatientListDataBy:self.patientsOriginArr];
                    
                    self.indexArr = [HZContactDataHelper getPatientListSectionBy:self.patientStructArr];
                    self.noPatientsImageView.hidden = self.patientStructArr.count;
                    [self changeAttentionReloadData];
                    self.indexBarView.hidden = NO;
                } else {
                    
                    //重新计算
                     self.bedNumOrderPatientArr = [HZContactDataHelper getSampleOrderPatientListBy:self.patientsOriginArr orderType:HZOrderTypeDefault];
                    self.noPatientsImageView.hidden = self.bedNumOrderPatientArr.count;
                    [self changeAttentionReloadData];
                    self.indexBarView.hidden = YES;
                }
            } else {
                if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
                    //重新计算
                    NSMutableArray *userNameArr = [HZContactDataHelper getUserPatientListWithUserName:self.user.userName originDataList:self.patientsOriginArr];
                    self.userPatientArrByPatientName = [HZContactDataHelper getStructuredPatientListDataBy:userNameArr];
                     self.indexArr = [HZContactDataHelper getPatientListSectionBy:self.userPatientArrByPatientName];
                    self.noPatientsImageView.hidden = self.userPatientArrByPatientName.count;
                    [self changeAttentionReloadData];
                    self.indexBarView.hidden = NO;
                } else {
                    //重新计算
                    NSMutableArray *userBedArr = [HZContactDataHelper getUserPatientListWithUserName:self.user.userName originDataList:self.patientsOriginArr];
                    self.userPatientArrByBedCodeAscending = [HZContactDataHelper getSampleOrderPatientListBy:userBedArr orderType:HZOrderTypeDefault];
                    self.noPatientsImageView.hidden = self.userPatientArrByBedCodeAscending.count;
                    [self changeAttentionReloadData];
                    self.indexBarView.hidden = YES;
                }
            }
            
            
            self.patientSearchBtn.hidden = NO;
            
            self.patientTableView.tableHeaderView.hidden = NO;
            if (isShowAlertView) {
                [self.activityIndicator stopAnimating];
                [self.view makeToast:@"同步患者数据成功" duration:1.5 position:CSToastPositionBottom];
            }
            
            self.bedNumOrderPatientArr = [HZContactDataHelper getSampleOrderPatientListBy:self.patientsOriginArr orderType:HZOrderTypeDefault];
            NSLog(@"-----sd------%@", self.bedNumOrderPatientArr);
        } else {
            
           
            self.noPatientsImageView.hidden = NO;
            self.patientSearchBtn.hidden = YES;
            self.indexBarView.hidden = YES;
            self.patientTableView.tableHeaderView.hidden = YES;
            
            [self.patientsOriginArr removeAllObjects];
            [self.patientStructArr removeAllObjects];
            [self.bedNumOrderPatientArr removeAllObjects];
            [self.userPatientArrByBedCodeAscending removeAllObjects];
            [self.userPatientArrByPatientName removeAllObjects];
            [self.patientTableView reloadData];
            if (isShowAlertView) {
                [self.activityIndicator stopAnimating];
                [self.view makeToast:responseObject[@"msg"] duration:1.5 position:CSToastPositionBottom];
            }
            
            
        }
       
        // 结束刷新
        [self.patientTableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---------patientsF--------:%@", error);
        // 结束刷新
        [self.patientTableView.mj_header endRefreshing];
        //  self.patientTableView.tableHeaderView.hidden = !self.patientsOriginArr.count;
        if (isShowAlertView) {
            [self.activityIndicator stopAnimating];
            [self.view makeToast:@"同步患者数据失败" duration:1.5 position:CSToastPositionBottom];
        }
        
        self.noPatientsImageView.hidden = self.patientsOriginArr.count;
        
    }];
    
}

// 刷新数据:关注／取消关注
- (void)changeAttentionReloadData {

    if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
        
        if (![self.oldPropertyStr isEqualToString:@"名字排序"]) {
            
            //
            self.bedNumOrderPatientArr = [HZContactDataHelper getSampleOrderPatientListBy:self.patientsOriginArr orderType:HZOrderTypeDefault];
        }
    } else {
        
        if (![self.oldPropertyStr isEqualToString:@"名字排序"]) {
            NSMutableArray *userNameArr = [HZContactDataHelper getUserPatientListWithUserName:self.user.userName originDataList:self.patientsOriginArr];
            //
            self.userPatientArrByBedCodeAscending = [HZContactDataHelper getSampleOrderPatientListBy:userNameArr orderType:HZOrderTypeDefault];
        }
        
    }
    
    
    //
    CGFloat bgViewW = kIndexH;
    CGFloat bgViewH = kIndexH * (self.indexArr.count);
    CGFloat bgViewX = self.view.width - bgViewW;
    CGFloat bgViewY = (self.view.height - SafeAreaTopHeight) * 0.5 - bgViewH * 0.5 + SafeAreaTopHeight;
    
    
    if(!self.indexBarView){
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH)];
        bgView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:bgView];
        self.indexBarView = bgView;
    }
    // 更新 height;和Y值
    self.indexBarView.height = bgViewH;
    self.indexBarView.y = bgViewY;
    [self.indexLabelArr removeAllObjects];
    
    //添加前先移除
    for (UILabel*label in self.indexBarView.subviews) {
        [label removeFromSuperview];
    }
    
    for (int i = 0; i < self.indexArr.count; i++) {
        
        CGFloat indexlabelX = kIndexH * 0.1;
        CGFloat indexlabelY = i * kIndexH;
        CGFloat indexlabelWH = kIndexH - indexlabelX * 2;
        
        UILabel *indexlabel = [[UILabel alloc] initWithFrame:CGRectMake(indexlabelX, indexlabelY, indexlabelWH, indexlabelWH)];
        //        UILabel *indexlabel = [[UILabel alloc] initWithFrame:indexBgView.bounds];
        indexlabel.textAlignment = NSTextAlignmentCenter;
        indexlabel.font = [UIFont boldSystemFontOfSize:kP(20)];
        //        indexlabel.backgroundColor = [UIColor greenColor];
        
        NSString *str = self.indexArr[i];
        //  if ([str isEqualToString:@"{search}"]) continue;
        indexlabel.text = str;
        //        indexlabel.backgroundColor = [UIColor greenColor];
        indexlabel.layer.cornerRadius = indexlabelWH * 0.5;
        indexlabel.clipsToBounds = YES;
        [self.indexBarView addSubview:indexlabel];
        [self.indexLabelArr addObject:indexlabel];
        
    }
    
    
    //
    if (!self.indexShowLabel) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kP(200), kP(200), kP(160), kP(160))];
        label.backgroundColor = [UIColor lightGrayColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:50];
        label.textColor = [UIColor whiteColor];
        label.center = self.view.center;
        label.alpha = 0.0;
        [self.view addSubview:label];
        self.indexShowLabel = label;
    }
    
    
    // 获得偏移y值区间
    [self getIndexOffsetSize];
    
    //
    [self.patientTableView reloadData];
}



- (NSMutableArray *)indexLabelArr {
    if (!_indexLabelArr) {
        _indexLabelArr = [NSMutableArray array];
    }
    return _indexLabelArr;
}


#pragma mark -- touches
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self setIndexViewWithIsTouchesEnd:NO touches:touches];
}
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self setIndexViewWithIsTouchesEnd:NO touches:touches];
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self setIndexViewWithIsTouchesEnd:YES touches:touches];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    if (![self.user.authenticationState isEqualToString:@"AUTHENTICATED"]) {
        return 0;
    }
    
    if ([tableView isEqual:self.selTypeTableView]) {
        return 1;
    }
    
    
    NSInteger sectionCount;
    if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
        
        if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
            sectionCount = self.patientStructArr.count;
            
        } else {
            sectionCount = self.bedNumOrderPatientArr.count;
        }
    } else {
        if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
            sectionCount = self.userPatientArrByPatientName.count;
            NSLog(@"--ssss-----------:%ld", sectionCount);
        } else {
            sectionCount = self.userPatientArrByBedCodeAscending.count;
        }
    }
    

    return sectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (![self.user.authenticationState isEqualToString:@"AUTHENTICATED"]) {
        return 0;
    }
    
    if ([tableView isEqual:self.selTypeTableView]) {
        return 2;
    }
    
    NSMutableArray *tempArr;
    if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
        
        
        if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
            tempArr = self.patientStructArr[section];
        } else {
            tempArr = self.bedNumOrderPatientArr[section];
        }
        
    } else {
        
        if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
            tempArr = self.userPatientArrByPatientName[section];

        } else {
            tempArr = self.userPatientArrByBedCodeAscending[section];
           
        }
    }

     return tempArr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if ([tableView isEqual:self.selTypeTableView]) {
        
        HZPatientSelectionTypeCell *patientSelectionTypeCell = [HZPatientSelectionTypeCell cellWithTableView:tableView];
        patientSelectionTypeCell.selTypeStr = self.patientSourceTypeBtn.selected ? self.sourceTypeStrArr[indexPath.row] : self.propertyStrArr[indexPath.row];
        return patientSelectionTypeCell;
    }
    
    
    
    if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
        
        if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
            
            HZPatientListCell *cell = [self getNameCellWithTableView:tableView atIndexPath:indexPath];
            return cell;
        } else {
            HZPatientListCell *cell = [self getBedNumCellWithTableView:tableView atIndexPath:indexPath];
            return cell;
        }
    } else {
        
        if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
            HZPatientListCell *cell = [self getUserNameCellWithTableView:tableView atIndexPath:indexPath];
            return cell;
        } else {
            HZPatientListCell *cell = [self getUserBedCellWithTableView:tableView atIndexPath:indexPath];
            return cell;
        }
    }
    
}

#pragma mark -- UITableViewDelegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if ([tableView isEqual:self.selTypeTableView]) {
        return nil;
    }
    
    UILabel *label = (UILabel *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerView"];
    if (!label) {
        label = [[UILabel alloc] init];
        //        [label setFont:[UIFont systemFontOfSize:kP(30)]];
        [label setTextColor:[UIColor grayColor]];
        [label setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    }
    
    
    if (![self.oldPropertyStr isEqualToString:@"名字排序"] ) {
        NSArray *attentionArr = (NSArray *)self.bedNumOrderPatientArr[0];
        if (section == 0 && attentionArr.count != 0) {
            label.text = @" 关注的患者";
            [label setFont:[UIFont systemFontOfSize:kP(28)]];
            return label;
        }
        return nil;
    }
    
    
   
   
    
    NSString *headerViewStr = self.indexArr[section];
    
    if ([headerViewStr isEqualToString:@"☆"]) {
        headerViewStr = @"关注的患者";
        [label setFont:[UIFont systemFontOfSize:kP(28)]];
    } else {
        [label setFont:[UIFont systemFontOfSize:kP(32)]];
    }
    
    [label setText:[NSString stringWithFormat:@" %@", headerViewStr]];
    return label;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if ([tableView isEqual:self.selTypeTableView]) {
        return 0.1;
    }
    
    
//    return 30;
//
//    if (self.bedNumOrderPatientArr.count == 0 || !self.bedNumOrderPatientArr) {
//        return 0.1;
//    }
//
    
    
    NSArray *attentionArr;
    if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {
        if (section == 0) {
            return 30;
        } else {
            return 25.5;
        }
    } else if (section == 0){
    
        if ([self.oldPropertyStr isEqualToString:@"科室患者"]) {
            
            if (self.bedNumOrderPatientArr.count == 0 || !self.bedNumOrderPatientArr) {
                return 30;
            }
            
            
            attentionArr = (NSArray *)self.bedNumOrderPatientArr[0];
            return attentionArr.count == 0 ? 0.1 : 30;
            
            
        } else {
            if (self.userPatientArrByBedCodeAscending.count == 0 || !self.userPatientArrByBedCodeAscending) {
                return 30;
            }
            
            attentionArr = (NSArray *)self.userPatientArrByBedCodeAscending[0];
            return attentionArr.count == 0 ? 0.1 : 30;
        }
        
    }
    
    
     return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if ([tableView isEqual:self.selTypeTableView]) {
        
        if (self.patientSourceTypeBtn.selected) {
            [self.patientSourceTypeBtn setTitle:self.sourceTypeStrArr[indexPath.row] forState:UIControlStateNormal];
        } else {
            [self.patientPropertTypeBtn setTitle:self.propertyStrArr[indexPath.row] forState:UIControlStateNormal];
        }
        
        [self recoverPatientTableViewState];
        
        
//        if ([self.oldSourceTypeStr isEqualToString:self.patientSourceTypeBtn.titleLabel.text] && [self.oldPropertyStr isEqualToString:self.patientPropertTypeBtn.titleLabel.text]) {
//            return;
//        }
        
        self.oldSourceTypeStr = self.patientSourceTypeBtn.titleLabel.text;
        self.oldPropertyStr = self.patientPropertTypeBtn.titleLabel.text;
        
        // 根据条件筛选数据，刷新表格。
        NSLog(@"ddddddd-------------------:%@--------------------:%@", self.patientSourceTypeBtn.titleLabel.text, self.patientPropertTypeBtn.titleLabel.text);
        
        [self reloadPatientTableViewWithSourceType:self.patientSourceTypeBtn.titleLabel.text  propertyType:self.patientPropertTypeBtn.titleLabel.text];
        // -----------
        
        return;
    }
    
    
    
    NSArray *rowArr;
    if ([self.oldPropertyStr isEqualToString:@"名字排序"]) {

        
        if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
            rowArr = self.patientStructArr[indexPath.section];

        } else {
            rowArr = self.userPatientArrByPatientName[indexPath.section];
        }

    } else {

        if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
            rowArr = self.bedNumOrderPatientArr[indexPath.section];

        } else {
            rowArr = self.userPatientArrByBedCodeAscending[indexPath.section];
        }

    }
    
    HZPatientInforViewController *patientInforVCtr = [HZPatientInforViewController new];
    patientInforVCtr.patient = rowArr[indexPath.row];
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:self.selTypeTableView]) {
        return kP(88);
    }
    return kP(120);
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self changeTheIndexInBackgroundWithOffsetY:scrollView.contentOffset.y + SafeAreaTopHeight];
    
    NSLog(@"==============:%lf", scrollView.contentOffset.y);
    if (scrollView.contentOffset.y == - 54) {
        [scrollView setContentOffset:CGPointMake(0, - SafeAreaTopHeight) animated:NO];
    }
    NSLog(@"=======1=======:%lf", scrollView.contentOffset.y);
}



#pragma mark -- patientSourceTypeBtnClick:z
- (void)patientSourceTypeBtnClick:(UIButton *)btn {
    NSLog(@"patientSourceTypeBtnClick--");
    
    
    if (self.patientSourceTypeBtn.selected == YES) {
        
        [self recoverPatientTableViewState];
        return;
    }
    btn.selected = YES;
    self.patientPropertTypeBtn.selected = NO;
    //
    self.indexBarView.hidden = NO;

    //
//    [self.patientTableView reloadData];
    [self.selTypeTableView reloadData];
    //
    self.tapBgView.hidden = NO;
    [UIView animateWithDuration:0.2f animations:^{
        self.selTypeTableView.height = 2 * kP(88);
    }];
    
    
    self.patientTableView.scrollEnabled = NO;
}

#pragma mark -- patientPropertTypeBtnClick:
- (void)patientPropertTypeBtnClick:(UIButton *)btn {


    NSLog(@"patientPropertTypeBtnClick--");
    
    if (self.patientPropertTypeBtn.selected == YES) {
        [self recoverPatientTableViewState];
        return;
    }
    btn.selected = YES;
    self.patientSourceTypeBtn.selected = NO;
    //
    self.indexBarView.hidden = YES;
    if (!self.bedNumOrderPatientArr || self.bedNumOrderPatientArr.count == 0) return;
    //
//    [self.patientTableView reloadData];
    [self.selTypeTableView reloadData];
    self.tapBgView.hidden = NO;
    
    [UIView animateWithDuration:0.2f animations:^{
        self.selTypeTableView.height = 2 * kP(88);
    }];
    self.patientTableView.scrollEnabled = NO;
}

#pragma mark -- setIndexViewWithIsTouchesEnd
- (void)setIndexViewWithIsTouchesEnd:(BOOL)isTouchesEnd touches:(NSSet<UITouch *> *)touches {
    
    CGPoint point = [touches.anyObject locationInView:self.indexBarView];
    NSLog(@"point 滑动 = %@",NSStringFromCGPoint(point));
    NSInteger index = (point.y / kIndexH);
    
    
    if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
        if (index >= self.patientStructArr.count && index > 0) {
            index = self.patientStructArr.count - 1;
        }else if (index < 0) {
            index = - 1;
        }
    } else {
        if (index >= self.userPatientArrByPatientName.count && index > 0) {
            index = self.userPatientArrByPatientName.count - 1;
        }else if (index < 0) {
            index = - 1;
        }
    }
    
    
    self.indexShowLabel.alpha = isTouchesEnd ? 0.0 : 1.0;
    
    if (index >= 0) {
        self.indexShowLabel.text = [NSString stringWithFormat:@"%@", self.indexArr[index]];
    }
    
    //
    if (index < 0) {
        
//        NSLog(@"ddddd================");
        [self.patientTableView setContentOffset:CGPointMake(0, - SafeAreaTopHeight) animated:NO];
        return;
    }
    
    [self.patientTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}



-(void)changeTheIndexInBackgroundWithOffsetY:(CGFloat)offsetY{
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        
        NSInteger index = - 1;
        for (int i = 0; i < self.offsetSectionArray.count; ++i) {
            
            sectionY min_max;
            NSValue *value = self.offsetSectionArray[i];
            [value getValue:&min_max];
            
            if (offsetY < min_max.maxY && offsetY >= min_max.minY) {
                index = i;
                break;
            }
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.indexLabel.backgroundColor = [UIColor clearColor];
            if (index >= 0) {
                
                UILabel *indexLabel;
                if (self.indexLabelArr.count != 0) {
                    
                    if ([self.oldSourceTypeStr isEqualToString:@"科室患者"]) {
                        
                        if (self.patientStructArr.count != self.indexLabelArr.count) {
                            
                            return ;
                        }
                    } else {
                        if (self.userPatientArrByPatientName.count != self.indexLabelArr.count) {
                            
                            
                            return ;
                        }

                    }
                    indexLabel = self.indexLabelArr[index];
                    indexLabel.backgroundColor = kGreenColor;
                    self.indexLabel = indexLabel;
                }
                
            }
            
            
        });
        
        
    });
    
    
}



- (void)getIndexOffsetSize {
    
    
    sectionY min_Max;
    CGFloat cellHeight = kP(120);
    CGFloat headerHeight = 22;
    CGFloat initHeight = kP(78);
    NSMutableArray *arr = [NSMutableArray array];
    
    //
    for (int i = 0; i< self.patientStructArr.count; ++i) {
        
        NSArray *modelArr = self.patientStructArr[i];
        min_Max.minY = initHeight;
        min_Max.maxY = (initHeight + headerHeight) +modelArr.count * cellHeight;
        initHeight = min_Max.maxY;
        
        NSValue *min_MaxValue = [NSValue valueWithBytes:&min_Max objCType:@encode(struct sectionY)];
        [arr addObject:min_MaxValue];
    }
    
    self.offsetSectionArray = arr;
    
}


#pragma mark -- enterToAttestation
- (void)enterToAttestation:(UIButton *)btn {
    
    HZAptitudeImageGetViewController *aptitudeImageGetVCtr = [HZAptitudeImageGetViewController new];
    aptitudeImageGetVCtr.user = self.user;
    [self.navigationController pushViewController:aptitudeImageGetVCtr animated:YES];
}


#pragma mark -- enterPatientSearchVCtr
- (void)enterPatientSearchVCtr:(UIButton *)btn {
    //    if (!self.patientsOriginArr || self.patientsOriginArr.count == 0) {
    //        [self.view makeToast:@"您尚无患者" duration:1.5f position:CSToastPositionCenter];
    //        return;
    //    }
    
    self.tapBgView.hidden = YES;
    self.selTypeTableView.height = 0;
    self.patientPropertTypeBtn.selected = NO;
    self.patientSourceTypeBtn.selected = NO;
    self.patientTableView.scrollEnabled = YES;
    //
    HZPatientSearchViewController *patientSearchViewVCtr = [HZPatientSearchViewController new];
    patientSearchViewVCtr.patientOriginArr = self.patientsOriginArr;
    [self.navigationController pushViewController:patientSearchViewVCtr animated:NO];
    
}


#pragma mark -- pullOrPushToRefresh
- (void)pullOrPushToRefresh {
    //下拉刷新
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        
        [self updateMyDataWithIsShowAlertView:YES];
    }];
    [refreshHeader setTitle:@"下拉同步院内患者" forState:MJRefreshStateIdle];
    [refreshHeader setTitle:@"松开同步院内患者" forState:MJRefreshStatePulling];
    [refreshHeader setTitle:@"正在同步院内患者" forState:MJRefreshStateRefreshing];
    self.patientTableView.backgroundColor = [UIColor colorWithHexString:@"#f7f7f7"];
    self.patientTableView.mj_header = refreshHeader;
    
}



#pragma mark -- setMyTableHeaderView:
- (UIView *)setMyTableHeaderView {
    
    
    
        UIView *headBgView = [[UIView alloc] initWithFrame:CGRectMake(kP(0), kP(0), HZScreenW, kP(80))];
        headBgView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
//        headBgView.backgroundColor = [UIColor greenColor];
    
        CGFloat patientSourceTypeBtnX = kP(0);
        CGFloat patientSourceTypeBtnY = kP(0);
        CGFloat patientSourceTypeBtnW = headBgView.width * 0.5;
        CGFloat patientSourceTypeBtnH = headBgView.height;
        HZButton *patientSourceTypeBtn = [[HZButton alloc] initWithFrame:CGRectMake(patientSourceTypeBtnX, patientSourceTypeBtnY, patientSourceTypeBtnW, patientSourceTypeBtnH)];

        [patientSourceTypeBtn addTarget:self action:@selector(patientSourceTypeBtnClick:) forControlEvents:UIControlEventTouchUpInside];

        [patientSourceTypeBtn setTitle:@"科室患者" forState:UIControlStateNormal];
        patientSourceTypeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(30)];
        [patientSourceTypeBtn setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
        [patientSourceTypeBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8"] forState:UIControlStateSelected];
        [patientSourceTypeBtn setImage:[UIImage imageNamed:@"arrowNormal"] forState:UIControlStateNormal];
        [patientSourceTypeBtn setImage:[UIImage imageNamed:@"arrowSel"] forState:UIControlStateSelected];
        patientSourceTypeBtn.selected = NO;
    
        //
        CGSize patientSourceTypeBtnLabelSize = [@"科室患者" getTextSizeWithMaxWidth:HZScreenW textFontSize:kP(31)];
        CGFloat patientSourceTypeBtnLabelW = patientSourceTypeBtnLabelSize.width;
        CGFloat patientSourceTypeBtnLabelX = kP(152);
        CGFloat patientSourceTypeBtnLabelH = patientSourceTypeBtnLabelSize.height;
        CGFloat patientSourceTypeBtnLabelY = patientSourceTypeBtnH * 0.5 - patientSourceTypeBtnLabelH * 0.5;
        patientSourceTypeBtn.titleRect = CGRectMake(patientSourceTypeBtnLabelX, patientSourceTypeBtnLabelY, patientSourceTypeBtnLabelW, patientSourceTypeBtnLabelH);
        
        CGSize patientSourceTypeBtnImageSize = patientSourceTypeBtn.imageView.image.size;
        CGFloat patientSourceTypeBtnImageW = patientSourceTypeBtnImageSize.width;
        CGFloat patientSourceTypeBtnImageX = CGRectGetMaxX(patientSourceTypeBtn.titleRect) + kP(6);
        CGFloat patientSourceTypeBtnImageH = patientSourceTypeBtnImageSize.height;
        CGFloat patientSourceTypeBtnImageY = patientSourceTypeBtnH * 0.5 - patientSourceTypeBtnImageH * 0.5;
        patientSourceTypeBtn.imageRect = CGRectMake(patientSourceTypeBtnImageX, patientSourceTypeBtnImageY, patientSourceTypeBtnImageW, patientSourceTypeBtnImageH);
        //
        [headBgView addSubview:patientSourceTypeBtn];
        self.patientSourceTypeBtn = patientSourceTypeBtn;
        
        
        
        //
        CGFloat patientPropertTypeBtnW = patientSourceTypeBtnW;
        CGFloat patientPropertTypeBtnX = self.patientSourceTypeBtn.right;
        CGFloat patientPropertTypeBtnY = patientSourceTypeBtnY;
        CGFloat patientPropertTypeBtnH = patientSourceTypeBtnH;
        HZButton *patientPropertTypeBtn = [[HZButton alloc] initWithFrame:CGRectMake(patientPropertTypeBtnX, patientPropertTypeBtnY, patientPropertTypeBtnW, patientPropertTypeBtnH)];
        //
        [patientPropertTypeBtn addTarget:self action:@selector(patientPropertTypeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [patientPropertTypeBtn setTitle:@"名字排序" forState:UIControlStateNormal];
        patientPropertTypeBtn.titleLabel.font = [UIFont systemFontOfSize:kP(30)];
        [patientPropertTypeBtn setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
        [patientPropertTypeBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8"] forState:UIControlStateSelected];
        [patientPropertTypeBtn setImage:[UIImage imageNamed:@"arrowNormal"] forState:UIControlStateNormal];
        [patientPropertTypeBtn setImage:[UIImage imageNamed:@"arrowSel"] forState:UIControlStateSelected];
        patientPropertTypeBtn.selected = NO;
        
        //
        CGSize patientPropertTypeBtnImageSize = patientPropertTypeBtn.imageView.image.size;
        CGFloat patientPropertTypeBtnImageW = patientPropertTypeBtnImageSize.width;
        CGFloat patientPropertTypeBtnImageX = headBgView.width * 0.5 - patientPropertTypeBtnImageW - kP(152);
        CGFloat patientPropertTypeBtnImageH = patientPropertTypeBtnImageSize.height;
        CGFloat patientPropertTypeBtnImageY = patientPropertTypeBtnH * 0.5 - patientPropertTypeBtnImageH * 0.5;
        patientPropertTypeBtn.imageRect = CGRectMake(patientPropertTypeBtnImageX, patientPropertTypeBtnImageY, patientPropertTypeBtnImageW, patientPropertTypeBtnImageH);
        //
        CGSize patientPropertTypeBtnLabelSize = [@"名字排序" getTextSizeWithMaxWidth:HZScreenW textFontSize:kP(31)];
        CGFloat patientPropertTypeBtnLabelW = patientPropertTypeBtnLabelSize.width;
        CGFloat patientPropertTypeBtnLabelX = patientPropertTypeBtnImageX - patientPropertTypeBtnLabelW - kP(6);
        CGFloat patientPropertTypeBtnLabelH = patientPropertTypeBtnLabelSize.height;
        CGFloat patientPropertTypeBtnLabelY = patientPropertTypeBtnH * 0.5 - patientPropertTypeBtnLabelH * 0.5;
        patientPropertTypeBtn.titleRect = CGRectMake(patientPropertTypeBtnLabelX, patientPropertTypeBtnLabelY, patientPropertTypeBtnLabelW, patientPropertTypeBtnLabelH);
        
        
        //
        [headBgView addSubview:patientPropertTypeBtn];
        self.patientPropertTypeBtn = patientPropertTypeBtn;
    
    //
    return headBgView;
}


#pragma mark -- hiddenTypeBgView

- (void)hiddenTypeBgView {
    [self recoverPatientTableViewState];
}


- (void)recoverPatientTableViewState {
    self.tapBgView.hidden = YES;
    self.patientPropertTypeBtn.selected = NO;
    self.patientSourceTypeBtn.selected = NO;
    self.patientTableView.scrollEnabled = YES;
    [UIView animateWithDuration:0.2f animations:^{
        self.selTypeTableView.height = 0;
    }];
}


#pragma mark -- class
- (void)reloadPatientTableViewWithSourceType:(NSString *)sourceType  propertyType:(NSString *)propertyType {
    
    
//    NSLog(@"------qqqqwwwww------------:%@", self.patientsOriginArr);
    if ([sourceType isEqualToString:@"科室患者"]) {
        if ([propertyType isEqualToString:@"名字排序"]) {
            NSLog(@"------科室患者---名字排序---------");
            
        
            //重新计算
            self.patientStructArr = [HZContactDataHelper getStructuredPatientListDataBy:self.patientsOriginArr];
            self.indexArr = [HZContactDataHelper getPatientListSectionBy:self.patientStructArr];
            
            [self changeAttentionReloadData];
            self.noPatientsImageView.hidden = self.patientStructArr.count;
            self.indexBarView.hidden = NO;
            [self.patientTableView reloadData];
        } else {
            NSLog(@"------科室患者---床号排序--------");
            self.bedNumOrderPatientArr = [HZContactDataHelper getSampleOrderPatientListBy:self.patientsOriginArr orderType:HZOrderTypeDefault];
            [self changeAttentionReloadData];
            self.noPatientsImageView.hidden = self.bedNumOrderPatientArr.count;
            self.indexBarView.hidden = YES;
            [self.patientTableView reloadData];
        }
    } else {
        
        
        NSMutableArray *userPatientArr = [HZContactDataHelper getUserPatientListWithUserName:self.user.userName originDataList:self.patientsOriginArr];
        
        if ([propertyType isEqualToString:@"名字排序"]) {
            
            
            
            self.userPatientArrByPatientName = [HZContactDataHelper getStructuredPatientListDataBy:userPatientArr];
            NSLog(@"------我的患者---名字排序--------%@", self.userPatientArrByPatientName);
            
            self.noPatientsImageView.hidden = self.userPatientArrByPatientName.count;
            //重新计算
            self.indexArr = [HZContactDataHelper getPatientListSectionBy:self.userPatientArrByPatientName];
            
            [self changeAttentionReloadData];
            self.indexBarView.hidden = NO;
            [self.patientTableView reloadData];
            
            
        } else {
            
            NSMutableArray *userPatientArrByBedCodeAscending = [HZContactDataHelper getSampleOrderPatientListBy:userPatientArr orderType:HZOrderTypeDefault];
            self.userPatientArrByBedCodeAscending = userPatientArrByBedCodeAscending;
            self.noPatientsImageView.hidden = self.userPatientArrByBedCodeAscending.count;
            NSLog(@"------我的患者---床号排序--------%@", userPatientArrByBedCodeAscending);
            [self changeAttentionReloadData];
            self.indexBarView.hidden = YES;
            [self.patientTableView reloadData];
        }
    }
    
    
    
    
}




#pragma mark -- getNameCellWithTableView
- (HZPatientListCell *)getNameCellWithTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    
//    UITableViewCell *cell;
    HZPatientListCell *patientListCell = [HZPatientListCell cellWithTableView:tableView];

//    if (self.patientStructArr.count == 0 || !self.patientStructArr) {
//        cell = patientListCell;
//        return cell;
//    }

    NSArray *rowArr = self.patientStructArr[indexPath.section];

    //
    patientListCell.patient = rowArr[indexPath.row];
    patientListCell.isShowStarBtn = YES;
    //关注与否点击

    WeakSelf(weakSelf)
    patientListCell.attentionClick = ^(BOOL starred) {
        
        StrongSelf(strongSelf)
        // 情况太多 索性 从走一边 数据 修改数据模型 重新刷新数据
        HZPatient *starredPatient = rowArr[indexPath.row];
        for (HZPatient *patient in strongSelf.patientsOriginArr) {
            if ([patient.name isEqual:starredPatient.name]) {
                patient.starred = starred;
                break;
            }
        }
        
        //重新计算
        strongSelf.patientStructArr = [HZContactDataHelper getStructuredPatientListDataBy:self.patientsOriginArr];
        self.indexArr = [HZContactDataHelper getPatientListSectionBy:strongSelf.patientStructArr];
        [strongSelf changeAttentionReloadData];
       
        [strongSelf scrollViewDidScroll:tableView];
        
        //
        if (starredPatient.starred == YES) {
            
            [HZPatientTool starPatientWithStaffNum:strongSelf.user.number patientRecordId:starredPatient.patientRecordId success:^(id  _Nullable responseObject) {
                NSLog(@"----starSSSS-----:%@", responseObject);
                if ([responseObject[@"state"] isEqual:@200]) {
                    [self.view makeToast:@"已将此患者设置为重点关注" duration:1.5 position:CSToastPositionCenter];
                    [tableView setContentOffset:CGPointMake(0, -SafeAreaTopHeight) animated:YES];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----starFFFF-----:%@", error);
            }];
        } else {
            [HZPatientTool unstarPatientWithStaffNum:strongSelf.user.number patientRecordId:starredPatient.patientRecordId success:^(id  _Nullable responseObject) {
                NSLog(@"----unstarSSSS-----:%@", responseObject);
                if ([responseObject[@"state"] isEqual:@200]) {
                    [self.view makeToast:@"已取消此患者的关注" duration:1.5 position:CSToastPositionCenter];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----unstarFFFF-----:%@", error);
            }];
        }
        
    };

    return patientListCell;
}



#pragma mark -- getBedNumCellWithTableView
- (HZPatientListCell *)getBedNumCellWithTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    
    HZPatientListCell *patientListCell = [HZPatientListCell cellWithTableView:tableView];
    if (self.bedNumOrderPatientArr.count == 0 || !self.bedNumOrderPatientArr) {
        return patientListCell;
    }
    
    NSArray *rowArr = self.bedNumOrderPatientArr[indexPath.section];
    
    
    //
    patientListCell.patient = rowArr[indexPath.row];
    patientListCell.isShowStarBtn = YES;
    //关注与否点击
    
    WeakSelf(weakSelf)
    patientListCell.attentionClick = ^(BOOL starred) {
        
        StrongSelf(strongSelf)
        // 情况太多 索性 从走一边 数据 修改数据模型 重新刷新数据
        HZPatient *starredPatient = rowArr[indexPath.row];
        for (HZPatient *patient in strongSelf.patientsOriginArr) {
            if ([patient.name isEqual:starredPatient.name]) {
                patient.starred = starred;
                break;
            }
        }
        
        //重新计算
        strongSelf.bedNumOrderPatientArr = [HZContactDataHelper getSampleOrderPatientListBy:self.patientsOriginArr orderType:HZOrderTypeDefault];
        //        self.indexArr = [HZContactDataHelper getPatientListSectionBy:<#(NSMutableArray *)#>];
        [strongSelf changeAttentionReloadData];
        [strongSelf scrollViewDidScroll:tableView];
        
        //
        if (starredPatient.starred == YES) {
            
            [HZPatientTool starPatientWithStaffNum:strongSelf.user.number patientRecordId:starredPatient.patientRecordId success:^(id  _Nullable responseObject) {
                NSLog(@"----starSSSS-----:%@", responseObject);
                if ([responseObject[@"state"] isEqual:@200]) {
                    [self.view makeToast:@"已将此患者设置为重点关注" duration:1.5 position:CSToastPositionCenter];
                    [tableView setContentOffset:CGPointMake(0, -SafeAreaTopHeight) animated:YES];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----starFFFF-----:%@", error);
            }];
        } else {
            [HZPatientTool unstarPatientWithStaffNum:strongSelf.user.number patientRecordId:starredPatient.patientRecordId success:^(id  _Nullable responseObject) {
                NSLog(@"----unstarSSSS-----:%@", responseObject);
                if ([responseObject[@"state"] isEqual:@200]) {
                    [self.view makeToast:@"已取消此患者的关注" duration:1.5 position:CSToastPositionCenter];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----unstarFFFF-----:%@", error);
            }];
        }
        
    };
    
    return patientListCell;
}

#pragma mark -- getUserNameCellWithTableView
- (HZPatientListCell *)getUserNameCellWithTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    
//    UITableViewCell *cell;
    HZPatientListCell *patientListCell = [HZPatientListCell cellWithTableView:tableView];
    
//    if (self.userPatientArrByPatientName.count == 0 || !self.userPatientArrByPatientName) {
//        cell = patientListCell;
//        return cell;
//    }
    
    
    NSArray *rowArr = self.userPatientArrByPatientName[indexPath.section];
    
    //
    patientListCell.patient = rowArr[indexPath.row];
    patientListCell.isShowStarBtn = YES;
    
    //

    //关注与否点击
    WeakSelf(weakSelf)
    patientListCell.attentionClick = ^(BOOL starred) {
        
        StrongSelf(strongSelf)
        // 情况太多 索性 从走一边 数据 修改数据模型 重新刷新数据
        HZPatient *starredPatient = rowArr[indexPath.row];
        for (HZPatient *patient in strongSelf.patientsOriginArr) {
            if ([patient.name isEqual:starredPatient.name]) {
                patient.starred = starred;
                break;
            }
        }
        

        //重新计算
        NSMutableArray *userNameArr = [HZContactDataHelper getUserPatientListWithUserName:self.user.userName originDataList:self.patientsOriginArr];
        strongSelf.userPatientArrByPatientName = [HZContactDataHelper getStructuredPatientListDataBy:userNameArr];
        strongSelf.indexArr = [HZContactDataHelper getPatientListSectionBy:strongSelf.userPatientArrByPatientName];
        
        
        [strongSelf changeAttentionReloadData];
        [strongSelf scrollViewDidScroll:tableView];
        
        //
        if (starredPatient.starred == YES) {
            
            [HZPatientTool starPatientWithStaffNum:strongSelf.user.number patientRecordId:starredPatient.patientRecordId success:^(id  _Nullable responseObject) {
                NSLog(@"----starSSSS-----:%@", responseObject);
                if ([responseObject[@"state"] isEqual:@200]) {
                    [self.view makeToast:@"已将此患者设置为重点关注" duration:1.5 position:CSToastPositionCenter];
                    [tableView setContentOffset:CGPointMake(0, -SafeAreaTopHeight) animated:YES];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----starFFFF-----:%@", error);
            }];
        } else {
            [HZPatientTool unstarPatientWithStaffNum:strongSelf.user.number patientRecordId:starredPatient.patientRecordId success:^(id  _Nullable responseObject) {
                NSLog(@"----unstarSSSS-----:%@", responseObject);
                if ([responseObject[@"state"] isEqual:@200]) {
                    [self.view makeToast:@"已取消此患者的关注" duration:1.5 position:CSToastPositionCenter];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----unstarFFFF-----:%@", error);
            }];
        }
        
    };
    
    return patientListCell;
}


#pragma mark -- getUserBedCellWithTableView
- (HZPatientListCell *)getUserBedCellWithTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    
    HZPatientListCell *patientListCell = [HZPatientListCell cellWithTableView:tableView];
    if (self.userPatientArrByBedCodeAscending.count == 0 || !self.userPatientArrByBedCodeAscending) {
        return patientListCell;
    }
    
    NSArray *rowArr = self.userPatientArrByBedCodeAscending[indexPath.section];
    //
    patientListCell.patient = rowArr[indexPath.row];
    patientListCell.isShowStarBtn = YES;
    //关注与否点击
    
    WeakSelf(weakSelf)
    patientListCell.attentionClick = ^(BOOL starred) {
        
        StrongSelf(strongSelf)
        // 情况太多 索性 从走一边 数据 修改数据模型 重新刷新数据
        HZPatient *starredPatient = rowArr[indexPath.row];
        for (HZPatient *patient in strongSelf.patientsOriginArr) {
            if ([patient.name isEqual:starredPatient.name]) {
                patient.starred = starred;
                break;
            }
        }
        
     
        
        //重新计算
        NSMutableArray *userBedArr = [HZContactDataHelper getUserPatientListWithUserName:self.user.userName originDataList:self.patientsOriginArr];
        strongSelf.userPatientArrByPatientName = [HZContactDataHelper getStructuredPatientListDataBy:userBedArr];
//        strongSelf.indexArr = [HZContactDataHelper getPatientListSectionBy:strongSelf.userPatientArrByPatientName];
        [strongSelf changeAttentionReloadData];
        [strongSelf scrollViewDidScroll:tableView];
        
        //
        if (starredPatient.starred == YES) {
            
            [HZPatientTool starPatientWithStaffNum:strongSelf.user.number patientRecordId:starredPatient.patientRecordId success:^(id  _Nullable responseObject) {
                NSLog(@"----starSSSS-----:%@", responseObject);
                if ([responseObject[@"state"] isEqual:@200]) {
                    [self.view makeToast:@"已将此患者设置为重点关注" duration:1.5 position:CSToastPositionCenter];
                    [tableView setContentOffset:CGPointMake(0, -SafeAreaTopHeight) animated:YES];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----starFFFF-----:%@", error);
            }];
        } else {
            [HZPatientTool unstarPatientWithStaffNum:strongSelf.user.number patientRecordId:starredPatient.patientRecordId success:^(id  _Nullable responseObject) {
                NSLog(@"----unstarSSSS-----:%@", responseObject);
                if ([responseObject[@"state"] isEqual:@200]) {
                    [self.view makeToast:@"已取消此患者的关注" duration:1.5 position:CSToastPositionCenter];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"----unstarFFFF-----:%@", error);
            }];
        }
        
    };
    
    return patientListCell;
}



//#pragma mark -- updateMyData
//
//- (void)updateMyData {
//    [self updateMyDataWithIsShowAlertView:NO];
//}

//- (void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

