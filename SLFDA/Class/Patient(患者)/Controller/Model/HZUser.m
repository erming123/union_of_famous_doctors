//
//  HZUser.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZUser.h"
#import "MJExtension.h"
@implementation HZUser

- (NSString *)description {
    return [NSString stringWithFormat:@"name:%@------avatar:%@-------gender:%@---------job:%@-----------------sub:%@, deList= %@, openId:%@-------userid:%@-------remoteExpertLevel:%@-----titleId:%@-----totalMoney:%@------departmentGeneralId:%@", self.userName, self.facePhotoUrl, self.gender, self.titleName, self.departmentGeneralName, self.departmentList, self.openid, self.userId, self.remoteExpertLevel, self.titleId, self.totalMoney, self.departmentGeneralId];
}


//- (BOOL)hasDataButted {
//    return ![self.dataButtedVersion isEqualToString:@"PRIMARY"];
//}
@end
