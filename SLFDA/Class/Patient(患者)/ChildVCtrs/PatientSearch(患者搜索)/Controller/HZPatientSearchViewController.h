//
//  HZPatientSearchViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/6.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZPatientSearchViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *patientOriginArr;
@end
