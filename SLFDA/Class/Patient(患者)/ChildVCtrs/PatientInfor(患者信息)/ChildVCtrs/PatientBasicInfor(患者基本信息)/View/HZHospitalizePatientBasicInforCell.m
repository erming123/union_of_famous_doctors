//
//  HZHospitalizePatientBasicInforCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZHospitalizePatientBasicInforCell.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZHospitalizePatientBasicInforCell ()
@property (nonatomic, weak) UILabel *myTitleLabel;
@property (nonatomic, weak) UILabel *myTitleContentLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZHospitalizePatientBasicInforCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    
    //
    UILabel *myTitleLabel = [UILabel new];
    myTitleLabel.font = [UIFont systemFontOfSize:kP(32)];
    myTitleLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B"];
    [self.contentView addSubview:myTitleLabel];
    self.myTitleLabel = myTitleLabel;
    
    //
    UILabel *myTitleContentLabel = [UILabel new];
    myTitleContentLabel.font = [UIFont systemFontOfSize:kP(32)];
    myTitleContentLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
//    myTitleContentLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:myTitleContentLabel];
    self.myTitleContentLabel = myTitleContentLabel;
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    self.myTitleLabel.x = kP(40);
    self.myTitleLabel.y = kP(22);
    [self.myTitleLabel sizeToFit];
    
    //
    self.myTitleContentLabel.numberOfLines = 0;
    self.myTitleContentLabel.x = kP(228);
    self.myTitleContentLabel.y = self.myTitleLabel.y;
    self.myTitleContentLabel.width = HZScreenW - self.myTitleContentLabel.x - self.myTitleLabel.x;
    [self.myTitleContentLabel sizeToFit];
}

- (void)setSubViewsContent {
    self.myTitleLabel.text = self.myTitleStr;
    self.myTitleContentLabel.text = self.myTitleContentStr;
}

- (void)setMyTitleStr:(NSString *)myTitleStr {
    _myTitleStr = myTitleStr;
}

- (void)setMyTitleContentStr:(NSString *)myTitleContentStr {
    _myTitleContentStr = myTitleContentStr;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"HospitalizePatientBasicInforCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
