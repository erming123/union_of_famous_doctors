//
//  HZHospitalizePatientBasicInforCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZHospitalizePatientBasicInforCell : UITableViewCell
@property (nonatomic, copy) NSString *myTitleStr;
@property (nonatomic, copy) NSString *myTitleContentStr;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@end
