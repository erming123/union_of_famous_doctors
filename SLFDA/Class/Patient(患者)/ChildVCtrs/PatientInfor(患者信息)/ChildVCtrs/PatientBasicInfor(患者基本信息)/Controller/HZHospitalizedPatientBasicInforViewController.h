//
//  HZPatientBasicInforViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZViewController.h"
@class HZPatient;
@interface HZHospitalizedPatientBasicInforViewController : HZViewController
@property (nonatomic, strong) HZPatient *patient;
@end
