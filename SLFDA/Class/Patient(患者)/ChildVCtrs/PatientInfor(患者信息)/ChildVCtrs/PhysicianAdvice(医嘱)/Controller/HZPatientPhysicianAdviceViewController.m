//
//  HZPatientPhysicianAdviceViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientPhysicianAdviceViewController.h"
#import "HZPhysicianAdviceCell.h"
#import "HZButton.h"
#import "HZPhysicianAdviceTool.h"
#import "HZPatient.h"
#import "HZPhysicianAdvice.h"
@interface HZPatientPhysicianAdviceViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) HZTableView *physicanAdviceTableView;
@property (nonatomic, weak) UIView *tapBgView;
@property (nonatomic, weak) UIView *siftBgView;
@property (nonatomic, weak) UIButton *siftBtn;
@property (nonatomic, weak) UIView *siftContentBtnBgViewHLine;
@property (nonatomic, weak) UIView *siftBgViewHLine;
@property (nonatomic, weak) UIView *siftContentBgView;
@property (nonatomic, weak) UIButton *resetBtn;
@property (nonatomic, weak) UIButton *ensureBtn;
@property (nonatomic, copy) NSString *adviceOfDateStr;
@property (nonatomic, copy) NSString *adviceOfTypeStr;
@property (nonatomic, copy) NSString *adviceOfDurationTypeStr;
@property (nonatomic, weak) UIImageView *noPhysicianAdviceImageView;
//**数据*/
@property (nonatomic, strong) NSArray *physicianAdviceArr;
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, assign) BOOL ensureBtnClicked;
@end

@implementation HZPatientPhysicianAdviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.isShouldPanToPopBack = NO;;
    [self setUpSubViews];
    [self setUpSiftContentView];
    [self getData];
    [self pushToRefreshData];
}


#pragma mark -- set&get
- (void)setPatient:(HZPatient *)patient {
    _patient = patient;
}

- (NSArray *)physicianAdviceArr {
    if (!_physicianAdviceArr) {
        _physicianAdviceArr = [NSArray array];
    }
    return _physicianAdviceArr;
}

#pragma mark -- setUpSubViews
- (void)setUpSubViews {
    
    // 1.physicanAdviceTableView
    HZTableView *physicanAdviceTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    physicanAdviceTableView.dataSource = self;
    physicanAdviceTableView.delegate = self;
    physicanAdviceTableView.y = kP(76);
    physicanAdviceTableView.height -= (kP(76) + kP(92) + SafeAreaTopHeight);
    physicanAdviceTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    physicanAdviceTableView.backgroundColor = [UIColor colorWithHexString:@"#F6F6F6"];
    [self.view addSubview:physicanAdviceTableView];
    self.physicanAdviceTableView = physicanAdviceTableView;
    
    
    //
    HZButton *siftBtn = [[HZButton alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, kP(76))];
    [siftBtn setTitle:@"筛选" forState:UIControlStateNormal];
    [siftBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    siftBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [siftBtn setImage:[UIImage imageNamed:@"arrowNormal"] forState:UIControlStateNormal];
    [siftBtn setImage:[UIImage imageNamed:@"arrowSel"] forState:UIControlStateSelected];
    
    //
    CGFloat imageRectW = kP(12);
    CGFloat imageRectH = kP(8);
    //
    CGFloat titleRectW = kP(70);
    CGFloat titleRectH = siftBtn.height;
    CGFloat titleRectX = HZScreenW * 0.5 - titleRectW * 0.5 - kP(8);
    CGFloat titleRectY = 0;
    
    siftBtn.titleRect = CGRectMake(titleRectX, titleRectY, titleRectW, titleRectH);
    
    //
    CGFloat imageRectX = titleRectX + titleRectW + kP(8);
    CGFloat imageRectY = siftBtn.height * 0.5 - imageRectH * 0.5;
    
    siftBtn.imageRect = CGRectMake(imageRectX, imageRectY, imageRectW, imageRectH);
    [siftBtn addTarget:self action:@selector(siftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    siftBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:siftBtn];
    self.siftBtn = siftBtn;
    
    
    UIView *btnHLine = [[UIView alloc] initWithFrame:CGRectMake(0, siftBtn.bottom - 0.5, HZScreenW, 0.5)];
    btnHLine.backgroundColor = physicanAdviceTableView.separatorColor;
    [self.view addSubview:btnHLine];
    
    //
    //3. 没有医嘱的时候
    UIImageView *noPhysicianAdviceImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(340), kP(372))];
    noPhysicianAdviceImageView.image = [UIImage imageNamed:@"noPhysicianAdvice"];
    noPhysicianAdviceImageView.center = self.view.center;
    noPhysicianAdviceImageView.y -= kP(76);
    noPhysicianAdviceImageView.hidden = YES;
    [self.view addSubview:noPhysicianAdviceImageView];
    self.noPhysicianAdviceImageView = noPhysicianAdviceImageView;
    
    
    //
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.y -= kP(92);
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
}


- (void)setUpSiftContentView {
    // 赋值先。不要上去就是null。
    self.adviceOfDateStr = @"";
    self.adviceOfTypeStr = @"";
    self.adviceOfDurationTypeStr = @"";
    // 1.遮盖层
    UIView *tapBgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.siftBtn.bottom, HZScreenW, HZScreenH - self.siftBtn.bottom)];
    tapBgView.backgroundColor = [UIColor colorWithHexString:@"#191C18" alpha:0.5];
    UITapGestureRecognizer *bgViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenTapBgView)];
    [tapBgView addGestureRecognizer:bgViewTap];
    tapBgView.hidden = YES;
    [self.view addSubview:tapBgView];
    self.tapBgView = tapBgView;
    
    // 2.siftBgView
    UIView *siftBgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.siftBtn.bottom, HZScreenW, kP(370))];
    siftBgView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    siftBgView.hidden = YES;
    [self.view addSubview:siftBgView];
    self.siftBgView = siftBgView;
    
    //
    UIView *siftContentBtnBgViewHLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.siftBgView.height - kP(90) - 0.5, HZScreenW, 0.5)];
    siftContentBtnBgViewHLine.backgroundColor = self.physicanAdviceTableView.separatorColor;
    [siftBgView addSubview:siftContentBtnBgViewHLine];
    self.siftContentBtnBgViewHLine = siftContentBtnBgViewHLine;
    
    //
    NSArray *siftContentStrArr = @[@"全部", @"今日医嘱", @"", @"全部", @"诊疗医嘱", @"用药医嘱", @"全部", @"长期医嘱", @"临时医嘱"];
    UIView *siftContentBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, HZScreenW, kP(280))];
    
    for (int i = 0; i < siftContentStrArr.count; i++) {
        
        if (i == 2) {
            continue;
        }
        CGFloat btnOriginX = kP(54);
        
        CGFloat btnHMargin = kP(70);
        CGFloat btnW = kP(168);
        CGFloat siftContentBtnX = btnOriginX + (i % 3) * (btnHMargin + btnW);
        //
        CGFloat btnOriginY = kP(32);
        CGFloat btnVMargin = kP(30);
        CGFloat btnH = kP(52);
        CGFloat siftContentBtnY = btnOriginY + (i / 3) * (btnVMargin + btnH);
        UIButton *siftContentBtn = [[UIButton alloc] initWithFrame:CGRectMake(siftContentBtnX, siftContentBtnY, btnW, btnH)];
        siftContentBtn.backgroundColor = [UIColor whiteColor];
        [siftContentBtn setTitle:siftContentStrArr[i] forState:UIControlStateNormal];
        [siftContentBtn setTitleColor:[UIColor colorWithHexString:@"#A2A5A6"] forState:UIControlStateNormal];
        [siftContentBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8"] forState:UIControlStateSelected];
        [siftContentBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [siftContentBtn setBackgroundImage:[UIImage imageNamed:@"siftBtnBgImage"] forState:UIControlStateSelected];
        //
        siftContentBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
        //
        
        //
        [siftContentBtn addTarget:self action:@selector(siftContentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [siftContentBgView addSubview:siftContentBtn];
    }
    [self.siftBgView addSubview:siftContentBgView];
    self.siftContentBgView = siftContentBgView;
    
    //
    UIButton *resetBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, self.siftContentBgView.bottom, HZScreenW * 0.5, kP(90))];
    [resetBtn setTitle:@"重置" forState:UIControlStateNormal];
    [resetBtn setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    resetBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    resetBtn.hidden = YES;
    [resetBtn addTarget:self action:@selector(resetBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.siftBgView addSubview:resetBtn];
    self.resetBtn = resetBtn;
    
    //
    UIButton *ensureBtn = [[UIButton alloc] initWithFrame:CGRectMake(HZScreenW * 0.5, self.siftContentBgView.bottom, HZScreenW * 0.5, kP(90))];
    [ensureBtn setTitle:@"确定" forState:UIControlStateNormal];
    [ensureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    ensureBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    ensureBtn.backgroundColor = kGreenColor;
    ensureBtn.hidden = YES;
    [ensureBtn addTarget:self action:@selector(ensureBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.siftBgView addSubview:ensureBtn];
    self.ensureBtn = ensureBtn;
    
    //
    UIView *siftBgViewHLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.siftBgView.height - 0.5, HZScreenW, 0.5)];
    siftBgViewHLine.backgroundColor = self.physicanAdviceTableView.separatorColor;
    [siftBgView addSubview:siftBgViewHLine];
    self.siftBgViewHLine = siftBgViewHLine;
    
    //
    WeakSelf(weakSelf)
    self.setSiftBtnNormalBlock = ^{
        [weakSelf hiddenTapBgView];
    };
}

#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.physicianAdviceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HZPhysicianAdviceCell *physicianAdviceCell = [HZPhysicianAdviceCell cellWithTableView:tableView];
    physicianAdviceCell.isFirstRow = indexPath.row == 0;
    physicianAdviceCell.isLastRow = indexPath.row == self.physicianAdviceArr.count - 1;
    physicianAdviceCell.physicianAdvice = self.physicianAdviceArr[indexPath.row];
    return physicianAdviceCell;
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return kP(294);
    } else if (indexPath.row == self.physicianAdviceArr.count - 1) {
        return kP(294);
    }
    return kP(284);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -- siftBtnClick
- (void)siftBtnClick:(HZButton *)siftBtn {
    siftBtn.selected = !siftBtn.selected;
    if (siftBtn.selected) {// 当选择页面出现时，
        
        if (self.ensureBtnClicked == NO) {// ensureBtn没有点击的时时候，3个“全部”的按钮选中
            //
            [self resetBtnClick];
        }
    }
    
    [self setSubViewHidden:!siftBtn.selected];
}

#pragma mark -- hiddenTapBgView
- (void)hiddenTapBgView {
    self.siftBtn.selected = NO;
    [self setSubViewHidden:YES];
}

- (void)setSubViewHidden:(BOOL)shouldHidden {
    [UIView animateWithDuration:0.2f animations:^{
        self.tapBgView.hidden = shouldHidden;
        self.siftBgView.hidden = shouldHidden;
        self.siftContentBgView.hidden = shouldHidden;
        self.siftBgViewHLine.hidden = shouldHidden;
        self.siftContentBtnBgViewHLine.hidden = shouldHidden;
        self.resetBtn.hidden = shouldHidden;
        self.ensureBtn.hidden = shouldHidden;
    }];
}
#pragma mark -- siftContentBtnClick
- (void)siftContentBtnClick:(UIButton *)siftContentBtn {
    // 清除同行其他btn的选中状态。
    NSInteger btnIndex = [self.siftContentBgView.subviews indexOfObject:siftContentBtn];
    if (btnIndex >= 0 && btnIndex <= 1) {
        
        // 重复点击自己,取消自己的选中状态。
        if (siftContentBtn.selected == YES) {
            //            self.adviceOfDateStr = @"";
            //            siftContentBtn.selected = NO;
            return;
        }
        
        self.adviceOfDateStr = siftContentBtn.titleLabel.text;
        if (btnIndex == 0) {
            [self setBtnSateNormal:1];
        } else {
            [self setBtnSateNormal:0];
        }
    } else if (btnIndex >= 2 && btnIndex <= 4) {
        if (siftContentBtn.selected == YES) {
            //            self.adviceOfTypeStr = @"";
            //            siftContentBtn.selected = NO;
            return;
        }
        self.adviceOfTypeStr = siftContentBtn.titleLabel.text;
        if (btnIndex == 2) {
            [self setBtnSateNormal:3];
            [self setBtnSateNormal:4];
        } else if (btnIndex == 3) {
            [self setBtnSateNormal:2];
            [self setBtnSateNormal:4];
        } else {
            [self setBtnSateNormal:2];
            [self setBtnSateNormal:3];
        }
    } else if (btnIndex >= 5 && btnIndex <= 7) {
        if (siftContentBtn.selected == YES) {
            //            self.adviceOfDurationTypeStr = @"";
            //            siftContentBtn.selected = NO;
            return;
        }
        self.adviceOfDurationTypeStr = siftContentBtn.titleLabel.text;
        if (btnIndex == 5) {
            [self setBtnSateNormal:6];
            [self setBtnSateNormal:7];
        } else if (btnIndex == 6) {
            [self setBtnSateNormal:5];
            [self setBtnSateNormal:7];
        } else {
            [self setBtnSateNormal:5];
            [self setBtnSateNormal:6];
        }
    }
    
    //
    siftContentBtn.selected = YES;
    //    [self setBackgroundBorder:siftContentBtn];
    NSLog(@"-----:%@", siftContentBtn.titleLabel.text);
}

- (void)setBtnSateNormal:(NSInteger)btnIndex {
    UIButton *siftBtn = self.siftContentBgView.subviews[btnIndex];
    siftBtn.selected = NO;
}

#pragma mark -- resetBtnClick
- (void)resetBtnClick {
    if (self.resetBtn.selected) return;
    
    self.adviceOfDateStr = @"";
    self.adviceOfTypeStr = @"";
    self.adviceOfDurationTypeStr = @"";
    
    for (UIButton *siftBtn in self.siftContentBgView.subviews) {
        
        if ([siftBtn.titleLabel.text isEqualToString:@"全部"]) {
            siftBtn.selected = YES;
            continue;
        }
        siftBtn.selected = NO;
    }
    
    self.ensureBtnClicked = NO;
}


#pragma mark -- ensureBtnClick
- (void)ensureBtnClick:(UIButton *)ensureBtn {
    
    
    //    if (self.adviceOfDateStr.length == 0 && self.adviceOfTypeStr.length == 0 && self.adviceOfDurationTypeStr.length == 0) return;
    //
    self.ensureBtnClicked = YES;
    //
    NSLog(@"---------:%@-----------:%@-----------:%@", self.adviceOfDateStr, self.adviceOfTypeStr, self.adviceOfDurationTypeStr);
    //
    
    
    [self siftData];
    //
    [self hiddenTapBgView];
    //    [self resetBtnClick];
}


#pragma mark -- getData
- (void)getData {
    [self.activityIndicator startAnimating];
    [HZPhysicianAdviceTool getPhysicanAdviceDataWithCardId:self.patient.inHospitalNum success:^(id responseObject) {
        NSLog(@"----responseObject-----%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSArray *doctorAdvicesArr = resultsDict[@"doctorAdvices"];
            
            if ([resultsDict[@"doctorAdvices"] isKindOfClass:[NSArray class]]) {
                if (doctorAdvicesArr.count > 0) {
                    self.noPhysicianAdviceImageView.hidden = YES;
                    [self.view makeToast:@"获取患者医嘱数据成功" duration:1.5 position:CSToastPositionBottom];
                } else {
                    NSLog(@"--------数据为空------");
                    self.noPhysicianAdviceImageView.hidden = NO;
                    [self.view makeToast:@"未找到该患者医嘱数据" duration:1.5 position:CSToastPositionBottom];
                }
                
                self.physicianAdviceArr = [HZPhysicianAdvice mj_objectArrayWithKeyValuesArray:doctorAdvicesArr];
                NSLog(@"---------%@", self.physicianAdviceArr);
            }
            
        } else {
            NSLog(@"--------后台有问题------");
            self.noPhysicianAdviceImageView.hidden = NO;
            [self.view makeToast:responseObject[@"msg"] duration:1.5 position:CSToastPositionBottom];
        }
        [self.activityIndicator stopAnimating];
        [self.physicanAdviceTableView reloadData];
        [self.physicanAdviceTableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"----s-error-------%@", error);
        self.noPhysicianAdviceImageView.hidden = NO;
        [self.activityIndicator stopAnimating];
        [self.physicanAdviceTableView.mj_header endRefreshing];
    }];
    
}

- (void)siftData {
    [self.activityIndicator startAnimating];
    [HZPhysicianAdviceTool getPhysicanAdviceDataWithHodiernalStr:self.adviceOfDateStr adviceTypeStr:self.adviceOfTypeStr adviceCycleTimeStr:self.adviceOfDurationTypeStr cardId:self.patient.inHospitalNum success:^(id  _Nullable responseObject) {
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSArray *doctorAdvicesArr = resultsDict[@"doctorAdvices"];
            
            
            self.physicianAdviceArr = [HZPhysicianAdvice mj_objectArrayWithKeyValuesArray:doctorAdvicesArr];
            NSLog(@"---------%@", self.physicianAdviceArr);
            if (self.physicianAdviceArr.count > 0) {
                self.noPhysicianAdviceImageView.hidden = YES;
                [self.view makeToast:@"筛选患者医嘱数据成功" duration:1.5 position:CSToastPositionBottom];
            } else {
                NSLog(@"--------数据为空------");
                self.noPhysicianAdviceImageView.hidden = NO;
                [self.view makeToast:@"未找到该患者医嘱数据" duration:1.5 position:CSToastPositionBottom];
            }
            
        } else {
            NSLog(@"--------后台有问题------");
            self.noPhysicianAdviceImageView.hidden = NO;
            [self.view makeToast:responseObject[@"msg"] duration:1.5 position:CSToastPositionBottom];
        }
        [self.activityIndicator stopAnimating];
        [self.physicanAdviceTableView reloadData];
        [self.physicanAdviceTableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"----s-error-------%@", error);
        self.noPhysicianAdviceImageView.hidden = NO;
        [self.activityIndicator stopAnimating];
        [self.physicanAdviceTableView.mj_header endRefreshing];
    }];
}

#pragma mark -- 刷新数据
- (void)pushToRefreshData {
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock: ^{
        //            [self getData];
        [self siftData];
    }];
    self.physicanAdviceTableView.mj_header = refreshHeader;
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.physicanAdviceTableView.mj_header.automaticallyChangeAlpha = YES;
}

@end

