//
//  HZPhysicanAdviceCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/11.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZPhysicianAdvice;
@interface HZPhysicianAdviceCell : UITableViewCell
//** 医嘱*/
@property (nonatomic, strong) HZPhysicianAdvice *physicianAdvice;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, assign) BOOL isFirstRow;
@property (nonatomic, assign) BOOL isLastRow;
@end
