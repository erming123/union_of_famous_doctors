//
//  HZPhysicanAdviceCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/11.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPhysicianAdviceCell.h"
#import "HZPhysicianAdvice.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZPhysicianAdviceCell ()
@property (nonatomic, weak) UIView *cellBgView;
@property (nonatomic, weak) UIView *contentBgView;
// 药物名label
@property (nonatomic, weak) UILabel *medicineContentLabel;
// 药物剂量label
@property (nonatomic, weak) UILabel *medicineDosageLabel;
// 药物服用方式label
@property (nonatomic, weak) UILabel *medicineDoseTypeLabel;
// 药物服用频率label
@property (nonatomic, weak) UILabel *medicineDoseFrequencyLabel;
// 医嘱医生头像imageView和名字label
@property (nonatomic, weak) UIImageView *adviceDoctorIconImageView;
@property (nonatomic, weak) UILabel *adviceDoctorNameLabel;
// 医嘱开始时间label
@property (nonatomic, weak) UILabel *adviceBeginTimeLabel;
// 医嘱结束时间label
@property (nonatomic, weak) UILabel *adviceEndTimeLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZPhysicianAdviceCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        
        self.backgroundColor = [UIColor colorWithHexString:@"#F6F6F6"];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UIView *cellBgView = [UIView new];
    [self.contentView addSubview:cellBgView];
    self.cellBgView = cellBgView;
    
    //
    UIView *contentBgView = [UIView new];
    contentBgView.backgroundColor = [UIColor whiteColor];
    [self.cellBgView addSubview:contentBgView];
    self.contentBgView = contentBgView;
    
    //
    UILabel *medicineContentLabel = [UILabel new];
    medicineContentLabel.font = [UIFont systemFontOfSize:kP(32)];
    medicineContentLabel.textColor = [UIColor colorWithHexString:@"#333333"];
//    medicineContentLabel.backgroundColor = [UIColor redColor];
    [self.contentBgView addSubview:medicineContentLabel];
    self.medicineContentLabel = medicineContentLabel;
    //
    UILabel *medicineDosageLabel = [UILabel new];
    medicineDosageLabel.font = [UIFont systemFontOfSize:kP(30)];
    medicineDosageLabel.textColor = [UIColor colorWithHexString:@"#2DBED8"];
//    medicineDosageLabel.backgroundColor = [UIColor greenColor];
    
    [self.contentBgView addSubview:medicineDosageLabel];
    self.medicineDosageLabel = medicineDosageLabel;
    //
    UILabel *medicineDoseTypeLabel = [UILabel new];
    medicineDoseTypeLabel.font = [UIFont systemFontOfSize:kP(30)];
    medicineDoseTypeLabel.textColor = [UIColor colorWithHexString:@"#2DBED8"];
//    medicineDoseTypeLabel.backgroundColor = [UIColor yellowColor];
    [self.contentBgView addSubview:medicineDoseTypeLabel];
    self.medicineDoseTypeLabel = medicineDoseTypeLabel;
    //
    UILabel *medicineDoseFrequencyLabel = [UILabel new];
    medicineDoseFrequencyLabel.font = [UIFont systemFontOfSize:kP(30)];
    medicineDoseFrequencyLabel.textColor = [UIColor colorWithHexString:@"#2DBED8"];
//    medicineDoseFrequencyLabel.backgroundColor = [UIColor blueColor];
    [self.contentBgView addSubview:medicineDoseFrequencyLabel];
    self.medicineDoseFrequencyLabel = medicineDoseFrequencyLabel;
    
    //
    UIImageView *adviceDoctorIconImageView = [UIImageView new];
    adviceDoctorIconImageView.image = [UIImage imageNamed:@"adviceDoctor"];
    [adviceDoctorIconImageView sizeToFitWithImage];
//    adviceDoctorIconImageView.backgroundColor = [UIColor yellowColor];
    [self.contentBgView addSubview:adviceDoctorIconImageView];
    self.adviceDoctorIconImageView = adviceDoctorIconImageView;
    
    //
    UILabel *adviceDoctorNameLabel = [UILabel new];
    adviceDoctorNameLabel.font = [UIFont systemFontOfSize:kP(28)];
    adviceDoctorNameLabel.textColor = [UIColor colorWithHexString:@"#FB7B1D"];
//    adviceDoctorNameLabel.backgroundColor = [UIColor redColor];
    [self.contentBgView addSubview:adviceDoctorNameLabel];
    self.adviceDoctorNameLabel = adviceDoctorNameLabel;
    //
    UILabel *adviceBeginTimeLabel = [UILabel new];
    adviceBeginTimeLabel.font = [UIFont systemFontOfSize:kP(28)];
    adviceBeginTimeLabel.textColor = [UIColor colorWithHexString:@"#707070"];
//    adviceBeginTimeLabel.backgroundColor = [UIColor yellowColor];
    [self.contentBgView addSubview:adviceBeginTimeLabel];
    self.adviceBeginTimeLabel = adviceBeginTimeLabel;
    //
    UILabel *adviceEndTimeLabel = [UILabel new];
    adviceEndTimeLabel.font = [UIFont systemFontOfSize:kP(28)];
    adviceEndTimeLabel.textColor = [UIColor colorWithHexString:@"#707070"];
//    adviceEndTimeLabel.backgroundColor = [UIColor redColor];
    [self.contentBgView addSubview:adviceEndTimeLabel];
    self.adviceEndTimeLabel = adviceEndTimeLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    //2.0
    self.cellBgView.x = kP(30);
    self.cellBgView.width = HZScreenW - 3 * self.cellBgView.x;
    if (self.isFirstRow) {
        self.cellBgView.y = kP(20);
        self.cellBgView.height = self.height - self.cellBgView.y - kP(10);
    }
    else if (self.isLastRow) {
        self.cellBgView.y = kP(10);
        self.cellBgView.height = self.height - 3 * self.cellBgView.y;
    }
    else {
        self.cellBgView.y = kP(10);
        self.cellBgView.height = self.height - 2 * self.cellBgView.y;
    }
    
    self.contentBgView.height = self.cellBgView.height;
    //
    self.cellBgView.layer.cornerRadius = kP(16);
    self.cellBgView.layer.masksToBounds = NO;
    self.cellBgView.backgroundColor = [UIColor colorWithHexString:@"#2DBED8"];
    self.cellBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.cellBgView.layer.shadowOffset = CGSizeMake(kP(2), kP(2));
    self.cellBgView.layer.shadowOpacity = 0.3;
    
    //
    self.contentBgView.x = kP(14);
    self.contentBgView.y = 0;
    self.contentBgView.width = self.cellBgView.width - self.contentBgView.x + self.cellBgView.x;
    [self.contentBgView cornerSideType:UIRectCornerTopRight | UIRectCornerBottomRight withCornerRadius:kP(16)];
    
    //2.1
    
    self.medicineContentLabel.numberOfLines = 2;
    self.medicineContentLabel.x = kP(26);
    self.medicineContentLabel.y = kP(24);
    self.medicineContentLabel.width = self.contentBgView.width - 2 * self.medicineContentLabel.x;
    [self.medicineContentLabel sizeToFit];// 这行放在最后。
    
    //2.2
    self.medicineDosageLabel.numberOfLines = 2;
    self.medicineDosageLabel.x = self.medicineContentLabel.x;
    
    self.medicineDosageLabel.y = kP(120);
    
    self.medicineDosageLabel.width = kP(120);
    [self.medicineDosageLabel sizeToFit];
    
    //2.3
    self.medicineDoseTypeLabel.numberOfLines = 2;
    self.medicineDoseTypeLabel.x = self.medicineContentLabel.x + kP(20) + kP(120);
    
    self.medicineDoseTypeLabel.y = self.medicineDosageLabel.y;
    self.medicineDoseTypeLabel.width = kP(160);
    [self.medicineDoseTypeLabel sizeToFit];
    
    //2.4
    self.medicineDoseFrequencyLabel.numberOfLines = 2;
    self.medicineDoseFrequencyLabel.x = self.medicineDoseTypeLabel.x + kP(20) + kP(160);

    
    self.medicineDoseFrequencyLabel.y = self.medicineDoseTypeLabel.y;
    self.medicineDoseFrequencyLabel.width = kP(160);
    [self.medicineDoseFrequencyLabel sizeToFit];
    
    //2.5
    self.adviceDoctorIconImageView.x = self.medicineDoseFrequencyLabel.x + kP(5) + kP(160);
    self.adviceDoctorIconImageView.y = self.medicineDoseFrequencyLabel.centerY - self.adviceDoctorIconImageView.height * 0.5;
    
    //2.6
    [self.adviceDoctorNameLabel sizeToFit];
    self.adviceDoctorNameLabel.x = self.adviceDoctorIconImageView.right + kP(5);
    self.adviceDoctorNameLabel.y = self.adviceDoctorIconImageView.centerY - self.adviceDoctorNameLabel.height * 0.5;
    
    //2.7
    self.adviceBeginTimeLabel.x = self.medicineContentLabel.x;
    [self.adviceBeginTimeLabel sizeToFit];
    self.adviceBeginTimeLabel.y = self.contentBgView.height - self.adviceBeginTimeLabel.height - kP(20);
    
    //2.8
    [self.adviceEndTimeLabel sizeToFit];
    self.adviceEndTimeLabel.x = kP(348);
    self.adviceEndTimeLabel.y = self.adviceBeginTimeLabel.y;
}

- (void)setSubViewsContent {
    self.medicineContentLabel.text = self.physicianAdvice.adviceItemName;
    self.medicineDosageLabel.text = [NSString stringWithFormat:@"%@%@", self.physicianAdvice.dose, self.physicianAdvice.doseUnit];
    self.medicineDoseTypeLabel.text = self.physicianAdvice.medicineSupplyWayValue;
    self.medicineDoseFrequencyLabel.text = self.physicianAdvice.frequencyValueEN;
    self.adviceDoctorNameLabel.text = self.physicianAdvice.orderCreatorName;
    
    //
    NSString *adviceBeginTimeStr = @"";
    if (self.physicianAdvice.adviceBeginTime.length > 0) {
        adviceBeginTimeStr = [self.physicianAdvice.adviceBeginTime stringByReplacingCharactersInRange:NSMakeRange(self.physicianAdvice.adviceBeginTime.length - 3, 3) withString:@""];
    }
    
    
    self.adviceBeginTimeLabel.text = [NSString stringWithFormat:@"开始:%@", adviceBeginTimeStr];
    
    //
    NSString *adviceEndTimeStr = @"";
    if (self.physicianAdvice.adviceEndTime.length > 0) {
        adviceEndTimeStr = [self.physicianAdvice.adviceEndTime stringByReplacingCharactersInRange:NSMakeRange(self.physicianAdvice.adviceEndTime.length - 3, 3) withString:@""];
    }
    
    self.adviceEndTimeLabel.text = [NSString stringWithFormat:@"结束:%@", adviceEndTimeStr];
}

- (void)setPhysicianAdvice:(HZPhysicianAdvice *)physicianAdvice {
    _physicianAdvice = physicianAdvice;
}

- (void)setIsFirstRow:(BOOL)isFirstRow {
    _isFirstRow = isFirstRow;
}

- (void)setIsLastRow:(BOOL)isLastRow {
    _isLastRow = isLastRow;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"PhysicianAdviceCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

@end
