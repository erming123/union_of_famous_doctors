//
//  HZPhysicianAdvice.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/11.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZPhysicianAdvice : NSObject
// 医嘱主题名称
@property (nonatomic, copy) NSString *adviceItemName;
// 服药剂量和单位
@property (nonatomic, copy) NSString *dose;
@property (nonatomic, copy) NSString *doseUnit;
// 药物服用方式
@property (nonatomic, assign) NSInteger medicineSupplyWayCode;
@property (nonatomic, copy) NSString *medicineSupplyWayValue;
// 药物服用频率
@property (nonatomic, copy) NSString *frequencyValueCN;
@property (nonatomic, copy) NSString *frequencyValueEN;
// 医嘱医生
@property (nonatomic, copy) NSString *orderCreatorName;
// 医嘱开始时间
@property (nonatomic, copy) NSString *adviceBeginTime;
// 医嘱结束时间
@property (nonatomic, copy) NSString *adviceEndTime;
// 医嘱订单ID
@property (nonatomic, copy) NSString *adviceOrderId;
@end
