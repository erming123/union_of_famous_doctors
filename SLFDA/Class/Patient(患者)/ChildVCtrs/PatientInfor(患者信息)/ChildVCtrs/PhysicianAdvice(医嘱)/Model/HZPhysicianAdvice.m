//
//  HZPhysicianAdvice.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/11.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPhysicianAdvice.h"

@implementation HZPhysicianAdvice
- (NSString *)description {
    return [NSString stringWithFormat:@"---------:%@-----------:%@-----------------:%@", self.adviceItemName, self.orderCreatorName, self.adviceBeginTime];
}
@end
