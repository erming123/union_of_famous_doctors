//
//  HZPatientCheckListViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/19.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZPatient;
@interface HZPatientCheckListViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZPatient *patient;
@end
