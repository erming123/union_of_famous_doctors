//
//  HZPatientCheckListViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/19.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientCheckListViewController.h"
#import "HZPatient.h"
#import "MJRefresh.h"
#import "HZCheckListTool.h"
#import "HZUser.h"
//
#import "HZCheckModel.h"
#import "HZCheckListContentCell.h"
#import "HZBiochemistryDetailContentViewController.h"
@interface HZPatientCheckListViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) HZTableView *patientCheckListTableView;
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *patientCheckListArr;
@property (nonatomic, copy) NSString *otherOrderId;
@property (nonatomic, weak) UIImageView *noCheckListImageView;
@end

@implementation HZPatientCheckListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = self.patient.name;
    [self setMySubViews];
    
    //
    [self getMyData];
    //
    [self pushToRefreshData];
}


#pragma mark -- setPatient
- (void)setPatient:(HZPatient *)patient {
    _patient = patient;
}


- (NSArray *)patientCheckListArr {
    if (!_patientCheckListArr) {
        _patientCheckListArr = [NSMutableArray array];
    }
    
    return _patientCheckListArr;
}


- (void)setMySubViews {
    //
    HZTableView *patientCheckListTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    patientCheckListTableView.dataSource = self;
    patientCheckListTableView.delegate = self;
    patientCheckListTableView.height -= (kP(92) + SafeAreaTopHeight);
    patientCheckListTableView.tableFooterView = [UIView new];
    [self.view addSubview:patientCheckListTableView];
    self.patientCheckListTableView = patientCheckListTableView;
    
    //
    //2. 没有订单的时候
    UIImageView *noCheckListImageView = [[UIImageView alloc] init];
    noCheckListImageView.width = kP(340);
    noCheckListImageView.height = kP(382);
    noCheckListImageView.centerX = self.view.centerX;
    noCheckListImageView.centerY = self.view.centerY - kP(150);
    noCheckListImageView.hidden = YES;
    noCheckListImageView.image = [UIImage imageNamed:@"noReport"];
    [self.view addSubview:noCheckListImageView];
    self.noCheckListImageView = noCheckListImageView;
    
    //
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.y -= kP(92);
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.patientCheckListArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HZCheckListContentCell *checkListContentCell = [HZCheckListContentCell cellWithTableView:tableView];
    checkListContentCell.checkModel = self.patientCheckListArr[indexPath.row];
    return checkListContentCell;
}

#pragma mark -- getMyData

- (void)getMyData {
    //
    self.otherOrderId = [NSString stringWithFormat:@"%@%@", self.patient.idCard, self.patient.patientRecordId];
    [self refreshCheckList];
}

#pragma mark -- 刷新数据
- (void)pushToRefreshData {
    
    //下拉刷新
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        
       [self refreshCheckList];
    }];
    self.patientCheckListTableView.mj_header = refreshHeader;
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.patientCheckListTableView.mj_header.automaticallyChangeAlpha = YES;
}


- (void)refreshCheckList {
    
    NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
    [self.activityIndicator startAnimating];
    [HZCheckListTool getCheckListWithBookingOrderId:nil patient:self.patient userHospitalId:user.hospitalId success:^(id responseObject) {
        
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSLog(@"----ddsa-----%@", [responseObject getDictJsonStr]);
            
            NSDictionary *resultsDict = responseObject[@"results"];
            NSArray *localCheckList = resultsDict[@"info"];
            
            if (localCheckList.count) {
                //
                NSLog(@"----dd334455-----%@", localCheckList);
                NSArray *checkModelArr = [HZCheckModel mj_objectArrayWithKeyValuesArray:localCheckList];
                self.patientCheckListArr = checkModelArr;
                //        [[NSUserDefaults standardUserDefaults] setObject:localCheckList forKey:self.otherOrderId];
                self.noCheckListImageView.hidden = self.patientCheckListArr.count != 0;
                [self.patientCheckListTableView reloadData];
                self.noCheckListImageView.hidden = YES;
            } else {
                self.noCheckListImageView.hidden = NO;
                [self.view makeToast:@"未找到检查单数据" duration:1.5 position:CSToastPositionBottom];
            }
            
        } else {
           [self.view makeToast:@"未找到检查单数据" duration:1.5 position:CSToastPositionBottom];
           self.noCheckListImageView.hidden = NO;
        }
        
        //
        [self.activityIndicator stopAnimating];
        [self.patientCheckListTableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------rrrrrr-错--%@", error);
        [self.activityIndicator stopAnimating];
        self.noCheckListImageView.hidden = NO;
        [self.patientCheckListTableView.mj_header endRefreshing];
    }];
    
}

#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(100);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HZBiochemistryDetailContentViewController *asssayDetailContentVCtr = [[HZBiochemistryDetailContentViewController alloc] init];
//    HZImageDetailContentViewController *imageDetailContentVCtr = [HZImageDetailContentViewController new];
    
    HZCheckModel *checkModel = self.patientCheckListArr[indexPath.row];
    BOOL isBiochemistry = [checkModel.flag isEqualToString:@"0"];
    
    
    NSLog(@"-----tutututtututututu----%@", checkModel.jcybid);
    if (isBiochemistry) { // 生化
        
        [self setBiochemistryDetailDataWithCheckId:checkModel.jcybid];// 生化列表
        
        asssayDetailContentVCtr.biochemistryCheckModel = checkModel;// 顶部数据
        
        [self.navigationController pushViewController:asssayDetailContentVCtr animated:YES];
    } else { // 影像
//        imageDetailContentVCtr.imageCheckModel = checkModel;
//        imageDetailContentVCtr.bookingOrderId = self.otherOrderId;
//        [self.navigationController pushViewController:imageDetailContentVCtr animated:YES];
    }
    
}

- (void)setBiochemistryDetailDataWithCheckId:(NSString *)checkId {
    [HZCheckListTool getBiochemistryCheckListWithCheckModelId:checkId patientHospitalId:@"" success:^(id responseObject) {
        NSLog(@"---ddsdsdsdsds0------%@", responseObject);
        [[NSUserDefaults standardUserDefaults] setObject:responseObject forKey:checkId];
    } failure:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
