//
//  HZPatientInforViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientInforViewController.h"
//
#import "HZPatientCheckListViewController.h"
//#import "HZPatientIndexTrendListViewController.h"
#import "HZPatientPhysicianAdviceViewController.h"
//
#import "HZHospitalizedPatientBasicInforViewController.h"
//
#import "HZPatient.h"
#import "MJExtension.h"
#define kVCtrCount 2.0
NS_ASSUME_NONNULL_BEGIN
@interface HZPatientInforViewController ()<UIScrollViewDelegate>
@property (nonatomic, weak) UIButton *patientBasicInforBtn;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UIView *seperateLine;
@property (nonatomic, strong) UIView *indicator;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) UIButton *selectedTitleBtn;
@property (nonatomic, copy) NSMutableArray *titleBtnArr;
@property (nonatomic, strong) HZPatientPhysicianAdviceViewController *physicanAdviceVCtr;
@end
NS_ASSUME_NONNULL_END
@implementation HZPatientInforViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //0
    self.navigationItem.title = [NSString stringWithFormat:@"%@ %@", self.patient.name, self.patient.bedNum];
    //
    //
    UIButton *patientBasicInforBtn = [UIButton new];
    patientBasicInforBtn.frame = CGRectMake(0, 0, kP(80), kP(80));
    //    patientSearchBtn.backgroundColor = [UIColor redColor];
    [patientBasicInforBtn setImage:[UIImage imageNamed:@"hospitalizedPatientBasicInfor"] forState:UIControlStateNormal];
    [patientBasicInforBtn setImage:[UIImage imageNamed:@"hospitalizedPatientBasicInfor"] forState:UIControlStateHighlighted];
    patientBasicInforBtn.contentHorizontalAlignment  = UIControlContentHorizontalAlignmentRight;
    [patientBasicInforBtn addTarget:self action:@selector(enterHospitalizedPatientBasicInforVCtr:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:patientBasicInforBtn];
    UIBarButtonItem *itemSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    itemSpacer.width = - kP(10);
    self.navigationItem.rightBarButtonItems = @[rightItem, itemSpacer];
    self.patientBasicInforBtn = patientBasicInforBtn;

    //1.创建titleView
    [self setUpTitleView];
    
    //2.添加titleView最下面的分割线
    [self setUpSeperateLine];
    
    //3.添加titleView标题指示条
    [self setUpTitleIndicator];
    //
    
    //4.创建ContentScrollView
    [self setUpContentScrollView];
    
    //5.添加子视图控制器
    [self setUpAllChildViewControllers];
    
    //6.添加标题
    [self setUpAllTitles];
    
    //7.解决系统自动处理滚动视图的问题
    self.automaticallyAdjustsScrollViewInsets = NO;
    //
}

- (void)setPatient:(HZPatient *)patient {
    _patient = patient;
}


#pragma mark -- 初始化标题按钮数组
- (NSMutableArray *)titleBtnArr {
    if (_titleBtnArr == nil) {
        _titleBtnArr = [NSMutableArray array];
    }
    
    return _titleBtnArr;
}


#pragma mark -- 创建titleView
- (void)setUpTitleView {
    UIView *titleView = [[UIView alloc] init];
    CGFloat titleViewY = self.parentViewController ? SafeAreaTopHeight: 0;
    titleView.frame = CGRectMake(0, titleViewY, HZScreenW, kP(92));
    [self.view addSubview:titleView];
    self.titleView = titleView;
}

#pragma mark -- 添加titleView最下面的分割线
- (void)setUpSeperateLine {
    UIView *seperateLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.titleView.bottom, HZScreenW, kP(2))];
    seperateLine.backgroundColor = [UIColor colorWithHexString:@"d9d9d9" alpha:1.0];
    [self.view addSubview:seperateLine];
    self.seperateLine = seperateLine;
}
//
#pragma mark -- 添加titleView标题指示条
- (void)setUpTitleIndicator {
    
    UIView *indicator = [[UIView alloc] init];
    indicator.frame = CGRectMake(0, self.seperateLine.bottom - kP(4), HZScreenW / kVCtrCount, kP(4));
    indicator.backgroundColor = kGreenColor;
    [self.view addSubview:indicator];
    self.indicator = indicator;
}

#pragma mark -- 创建ContentScrollView
- (void)setUpContentScrollView {
    
    UIScrollView *contentScrollView = [[UIScrollView alloc] init];
    CGFloat contentScrollViewY = self.seperateLine.bottom;
    contentScrollView.frame = CGRectMake(0, contentScrollViewY, HZScreenW, self.view.height - contentScrollViewY);
    // 设置效果
    contentScrollView.bounces = NO;
    contentScrollView.pagingEnabled = YES;
    contentScrollView.showsHorizontalScrollIndicator = NO;
    
    // 设置代理
    contentScrollView.delegate = self;
    [self.view addSubview:contentScrollView];
    self.contentScrollView = contentScrollView;
}

#pragma mark -- 添加子视图控制器
- (void)setUpAllChildViewControllers {
    // 报告单
    HZPatientCheckListViewController *checkListVCtr = [[HZPatientCheckListViewController alloc] init];
    checkListVCtr.title = @"报告单";
    checkListVCtr.patient = self.patient;
    [self addChildViewController:checkListVCtr];
    
    
//    // 指标趋势
//     HZPatientIndexTrendListViewController *indexTrendVCtr = [HZPatientIndexTrendListViewController new];
//     indexTrendVCtr.title = @"指标趋势";
////    indexTrendVCtr.checkListOrder = self.basicOrder;
//    [self addChildViewController:indexTrendVCtr];

    
    // 医嘱
    HZPatientPhysicianAdviceViewController *physicanAdviceVCtr = [HZPatientPhysicianAdviceViewController new];
    physicanAdviceVCtr.title = @"医嘱";
    physicanAdviceVCtr.patient = self.patient;
    [self addChildViewController:physicanAdviceVCtr];
    self.physicanAdviceVCtr = physicanAdviceVCtr;
}


#pragma mark -- 添加标题
- (void)setUpAllTitles {
    // 添加所有变体按钮
    
    NSInteger btnCount = self.childViewControllers.count;
    
    CGFloat titleBtnW = HZScreenW / kVCtrCount;
    CGFloat titleBtnH = kP(100);
    for (NSInteger i = 0; i < btnCount; i++) {
        UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        titleBtn.frame = CGRectMake(i * titleBtnW, 5, titleBtnW, titleBtnH);
        
        // 添加标题
        UIViewController *VCtr = self.childViewControllers[i];
        [titleBtn setTitle:VCtr.title forState:UIControlStateNormal];
        [titleBtn setTitleColor:[UIColor colorWithHexString:@"262626" alpha:1.0] forState:UIControlStateNormal];
        titleBtn.titleLabel.font = [UIFont systemFontOfSize:kP(31)];

        // 添加tag
        titleBtn.tag = i;
        [self.titleView addSubview:titleBtn];
        
        // 监听按钮的点击
        [titleBtn addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        // 把标题按钮保存到数组
        [self.titleBtnArr addObject:titleBtn];
        
        // 默认选中第一个
        if (i == 0 && self.isReportBtnSel == NO) {
            [self titleBtnClick:titleBtn];
        } else if (i == 1 && self.isReportBtnSel == YES) {
            [self titleBtnClick:titleBtn];
        }
    }
    
    // 让ContentScrollView滚动
    [self.contentScrollView setContentSize:CGSizeMake(HZScreenW * btnCount, 0)];
}


#pragma mark -- 处理titleBtn的点击
- (void)titleBtnClick:(UIButton *)titleBtn {
    
    NSInteger titleIndex = titleBtn.tag;
    // 选中的字体颜色
    [self selectTitleBtn:titleBtn];
    // 横线偏移
    [self reSetIndicatorOrigin:titleIndex];
    
    
    // 添加子视图控制器的View
    [self setUpOneViewController:titleIndex];
    // 子视图控制器的View滚动到可视区域
    CGFloat x = titleIndex * HZScreenW;
    [self.contentScrollView setContentOffset:CGPointMake(x, 0) animated:NO];
}


#pragma mark -- 横线偏移
- (void)reSetIndicatorOrigin:(NSInteger)titleIndex {
    [UIView animateWithDuration:0.2f animations:^{
        self.indicator.x = titleIndex * self.indicator.width;
    }];
}
#pragma mark -- 选中标题
- (void)selectTitleBtn:(UIButton *)titleBtn {
    [self.selectedTitleBtn setTitleColor:[UIColor colorWithHexString:@"262626" alpha:1.0] forState:UIControlStateNormal];
    [titleBtn setTitleColor:kGreenColor forState:UIControlStateNormal];
    self.selectedTitleBtn = titleBtn;
    
    if (titleBtn.tag == 0) {
        
        if (self.physicanAdviceVCtr.setSiftBtnNormalBlock) {
            self.physicanAdviceVCtr.setSiftBtnNormalBlock();
        }
    }
}


#pragma mark -- 添加子视图控制器的View
- (void)setUpOneViewController:(NSInteger)titleIndex{
    
    // 判断是否已经加载，YES->不再加载，NO->加载
    
    UIViewController *VCTr = self.childViewControllers[titleIndex];
    if (VCTr.view.superview) {
        return;
    }
    
    
    UIViewController *VCtr = self.childViewControllers[titleIndex];
    CGFloat x = titleIndex * HZScreenW;
    VCtr.view.frame = CGRectMake(x, 0, HZScreenW, self.contentScrollView.height);
    [self.contentScrollView addSubview:VCtr.view];
    
}

#pragma mark -- UIScrollViewDelegate

// 滚动完成
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    
    // 获取当前角标
    NSInteger i = scrollView.contentOffset.x / HZScreenW;
    
    //1.选中标题
    UIButton *titleBtn = self.titleBtnArr[i];
    [self selectTitleBtn:titleBtn];
    
    //2.横线偏移
    [self reSetIndicatorOrigin:i];
    
    //3.添加子视图控制器的View
    [self setUpOneViewController:i];
}



- (void)rebackToRootViewAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark -- enterPatientBasicInforVCtr
- (void)enterHospitalizedPatientBasicInforVCtr:(UIButton *)patientBasicInBtn {
    HZHospitalizedPatientBasicInforViewController *hospitalizedPatientBasicInforVCtr = [HZHospitalizedPatientBasicInforViewController new];
    hospitalizedPatientBasicInforVCtr.patient = self.patient;
    [self.navigationController pushViewController:hospitalizedPatientBasicInforVCtr animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (BOOL)prefersStatusBarHidden {
//    return self.adviceVCtr.isClicked;
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
