//
//  HZPatientInforViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/11/24.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZViewController.h"
@class HZPatient;
@interface HZPatientInforViewController : HZViewController
@property (nonatomic, assign) CGFloat titleViewY;
//** <#注释#>*/
@property (nonatomic, strong) HZPatient *patient;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *localCheckListArr;

@property (nonatomic, assign) BOOL isReportBtnSel;

@end
