//
//  HZExampleCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HZOrder;
@protocol HZExampleCellDelegate <NSObject>

- (void)enterExampleSickInfoVCtr:(HZOrder *)exampleOrder;
- (void)enterExampleCheckReportVCtr:(HZOrder *)exampleOrder;

@end
@interface HZExampleCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@property (nonatomic, weak) id<HZExampleCellDelegate>delegate;

//** <#注释#>*/
@property (nonatomic, strong) HZOrder *exampleOrder;
@end
