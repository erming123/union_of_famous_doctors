//
//  HZTopHeadView.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;
@protocol HZTopHeadViewDelegate <NSObject>
- (void)enterProfile;
@end
@interface HZTopHeadView : UIView
@property (nonatomic, weak) id <HZTopHeadViewDelegate> delegate;
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;

//** <#注释#>*/
@property (nonatomic, strong) UIImage *avatarImage;
- (void)setUpSubViewsContent;
@end
