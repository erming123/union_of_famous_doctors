//
//  HZTopHeadView.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZTopHeadView.h"
#import "HZUser.h"
#import "HZOrderHttpsTool.h"
#import "HZUserTool.h"
#import "HZUser.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZTopHeadView ()
@property (nonatomic, weak) UIImageView *profileAvatarBgImageView;
@property (nonatomic, weak) UIImageView *profileAvatarImageView;
@property (nonatomic, weak) UILabel *profileNameLabel;
@property (nonatomic, weak) UILabel *profileJobLabel;
@property (nonatomic, weak) UILabel *profileSubjectLabel;
@property (nonatomic, weak) UIImageView *indicatorImageView;
@end
NS_ASSUME_NONNULL_END
@implementation HZTopHeadView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self setUpData];
        [self setUpSubViews];
//        self.backgroundColor = [UIColor yellowColor];
//        [self addTapGusture];
    }
    return self;
}

- (void)setUpSubViews {
    
    // 头像背景
    UIImageView *profileAvatarBgImageView = [[UIImageView alloc] init];
    profileAvatarBgImageView.backgroundColor = [UIColor whiteColor];
    profileAvatarBgImageView.userInteractionEnabled = YES;
    [self addSubview:profileAvatarBgImageView];
    self.profileAvatarBgImageView = profileAvatarBgImageView;
    
    //
    UIImageView *profileAvatarImageView = [[UIImageView alloc] init];
    profileAvatarImageView.backgroundColor = [UIColor greenColor];
    profileAvatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    profileAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
    profileAvatarImageView.userInteractionEnabled = YES;
    [self addSubview:profileAvatarImageView];
    self.profileAvatarImageView = profileAvatarImageView;

    //
    UILabel *profileNameLabel = [[UILabel alloc] init];
//    profileNameLabel.backgroundColor = [UIColor redColor];
    [profileNameLabel setTextFont:kP(36) textColor:@"#FFFFFF" alpha:1.0];
    [self addSubview:profileNameLabel];
    self.profileNameLabel = profileNameLabel;
    
    UILabel *profileJobLabel = [[UILabel alloc] init];
//    profileJobLabel.backgroundColor = [UIColor redColor];
    [profileJobLabel setTextFont:kP(32) textColor:@"#FFFFFF" alpha:1.0];
    [self addSubview:profileJobLabel];
    self.profileJobLabel = profileJobLabel;
    
    UILabel *profileSubjectLabel = [[UILabel alloc] init];
   [profileSubjectLabel setTextFont:kP(28) textColor:@"#FFFFFF" alpha:1.0];
    profileSubjectLabel.backgroundColor = [UIColor colorWithHexString:@"#3BADDA" alpha:1.0];
    profileSubjectLabel.textAlignment = NSTextAlignmentCenter;
    profileSubjectLabel.layer.cornerRadius = kP(20);
    profileSubjectLabel.layer.masksToBounds = YES;
//    profileSubjectLabel.backgroundColor = [UIColor yellowColor];
    [self addSubview:profileSubjectLabel];
    self.profileSubjectLabel = profileSubjectLabel;
    
//    UIImageView *indicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_right"]];
////    indicatorImageView.backgroundColor = [UIColor greenColor];
//    [self addSubview:indicatorImageView];
//    self.indicatorImageView = indicatorImageView;
    
    
//    self.backgroundColor = [UIColor redColor];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self setUpSubViewsContent];
    
    // 头像背景
    CGFloat profileAvatarBgImageViewWH = self.height;
    CGFloat profileAvatarBgImageViewX = kP(0);
    CGFloat profileAvatarBgImageViewY = kP(0);
    self.profileAvatarBgImageView.frame = CGRectMake(profileAvatarBgImageViewX, profileAvatarBgImageViewY, profileAvatarBgImageViewWH, profileAvatarBgImageViewWH);
    self.profileAvatarBgImageView.layer.cornerRadius = profileAvatarBgImageViewWH * 0.5;
    self.profileAvatarBgImageView.clipsToBounds = YES;

    
    
    // 头像
    CGFloat profileAvatarImageViewWH = profileAvatarBgImageViewWH - kP(4);
    CGFloat profileAvatarImageViewX = kP(2);
    CGFloat profileAvatarImageViewY = self.profileAvatarBgImageView.centerY - profileAvatarImageViewWH * 0.5;
    self.profileAvatarImageView.frame = CGRectMake(profileAvatarImageViewX, profileAvatarImageViewY, profileAvatarImageViewWH, profileAvatarImageViewWH);
    self.profileAvatarImageView.layer.cornerRadius = profileAvatarImageViewWH * 0.5;
    self.profileAvatarImageView.clipsToBounds = YES;
    
    // 姓名
    self.profileNameLabel.x = self.profileAvatarBgImageView.right + kP(50);
    self.profileNameLabel.y = self.profileAvatarBgImageView.centerY - kP(11) - self.profileNameLabel.height;
    
    // 工作
    self.profileJobLabel.x = self.profileNameLabel.width == 0 ? self.profileAvatarBgImageView.right + kP(30) : self.profileNameLabel.right + kP(32);
    self.profileJobLabel.y = self.profileAvatarBgImageView.centerY - kP(11) - self.profileJobLabel.height;
//    self.profileJobLabel.y = CGRectGetMidY(self.profileNameLabel.frame) - self.profileJobLabel.height * 0.5;
    
    // 科室
    if (![self.profileSubjectLabel.text isEqualToString:@""]) {
        CGSize profileSubjectSize = [self.profileSubjectLabel.text getTextSizeWithMaxWidth:kP(400) textFontSize:kP(32)];
        self.profileSubjectLabel.width = profileSubjectSize.width + kP(40);
        self.profileSubjectLabel.height = profileSubjectSize.height + kP(4);
        self.profileSubjectLabel.x =  self.profileAvatarBgImageView.right + kP(50);
        self.profileSubjectLabel.y = self.profileAvatarBgImageView.centerY + kP(11);
    }
   
//    // 指示
//    self.indicatorImageView.x = self.width - self.indicatorImageView.width - kP(10);
//    self.indicatorImageView.y = self.height * 0.5 - self.indicatorImageView.height * 0.5;
}

#pragma mark -- <#class#>
- (void)setUpSubViewsContent {
    
    [self setAvatarWithUserOpenidId:self.user.openid];
    
    NSLog(@"----self.user.departmentGeneralName-----%@", self.user.departmentGeneralName);
    //
    self.profileNameLabel.text = self.user.userName;
    self.profileJobLabel.text = self.user.titleName;
    self.profileSubjectLabel.text = self.user.departmentGeneralName;
    //
    [self.profileNameLabel sizeToFit];
    [self.profileJobLabel sizeToFit];
    [self.profileSubjectLabel sizeToFit];
}


- (void)setUser:(HZUser *)user {
    _user = user;
    
    [self setAvatarWithUserOpenidId:_user.openid];
}


- (void)setAvatarImage:(UIImage *)avatarImage {
    self.profileAvatarImageView.image = avatarImage;
}


#pragma mark -- avatar

- (void)setAvatarWithUserOpenidId:(NSString *)userOpenidId{
    NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:userOpenidId];
    
    
//    NSLog(@"----123456-----%@:6666------------:%@", self.user.openid, imageData);
    if (imageData) {// 有
        
        UIImage *image = [UIImage imageWithData:imageData];
        self.profileAvatarImageView.image = image;
        
        [HZUserTool getAvatarWithAvatarOpenId:userOpenidId success:^(UIImage *image) {
            
            
            if (image) {
                self.profileAvatarImageView.image = image;
                NSData *imageData = UIImagePNGRepresentation(image);//把image归档为NSData
                [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:userOpenidId];
                [[NSUserDefaults standardUserDefaults] synchronize];
            } else {
                
                self.profileAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:userOpenidId];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"-----avatarError----%@", error);
        }];
        
        
    } else {// 没有
        
        self.profileAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
        
        //
        [HZUserTool getAvatarWithAvatarOpenId:self.user.openid success:^(UIImage *image) {
            
            
            if (image) {
                self.profileAvatarImageView.image = image;
                NSData *imageData = UIImagePNGRepresentation(image);//把image归档为NSData
                [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:userOpenidId];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //
                //                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"avatarData"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
            } else {
                self.profileAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:userOpenidId];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"---------%@", error);
            self.profileAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
        }];
    }

}

//- (void)addTapGusture {
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeDelegateEnterProfile:)];
//    [self addGestureRecognizer:tap];
//}
//
//- (void)makeDelegateEnterProfile:(UITapGestureRecognizer *)tap {
//
//    if (self.delegate && [self.delegate respondsToSelector:@selector(enterProfile)]) {
//        [self.delegate enterProfile];
//    }
//}
@end
