//
//  HZAppointAlertView.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZAppointAlertView.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZAppointAlertView ()
//@property (nonatomic, strong) UIView *bgView;
//@property (nonatomic, strong) UIView *topBgView;
//@property (nonatomic, strong) UILabel *titleLabel;
//@property (nonatomic, strong) UILabel *alertContentLabel;
//@property (nonatomic, strong) UIButton *alertSetBtn;
//@property (nonatomic, strong) UIView *sepLine;
//** <#注释#>*/
@property (nonatomic, weak) UIImageView *alertImageView;
@property (nonatomic, weak) UIButton *appointCloseBtn;
@property (nonatomic, weak) UIButton *alertEnterBtn;
@end
NS_ASSUME_NONNULL_END
@implementation HZAppointAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}
- (void)setUpSubViews {
    
    //0.
//    UIView *bgView = [UIView new];
//    [self addSubview:bgView];
//    self.bgView = bgView;
    
//    //1.
//    UIView *topBgView = [UIView new];
//    topBgView.backgroundColor = [UIColor whiteColor];
//    [self.bgView addSubview:topBgView];
//    self.topBgView = topBgView;
//    
//    //2.
//    UILabel *titleLabel = [UILabel new];
//    [self.topBgView addSubview:titleLabel];
//    self.titleLabel = titleLabel;
//    
//    //3.
//    UILabel *alertContentLabel = [UILabel new];
//    [self.topBgView addSubview:alertContentLabel];
//    self.alertContentLabel = alertContentLabel;
//    
//    //4.
//    UIButton *alertSetBtn = [UIButton new];
//    [alertSetBtn addTarget:self action:@selector(alertSetBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.topBgView addSubview:alertSetBtn];
//    self.alertSetBtn = alertSetBtn;
//    
//    //5.
//    UIView *sepLine = [UIView new];
//    [self.bgView addSubview:sepLine];
//    self.sepLine = sepLine;
    
    
    
    //
    UIImageView *alertImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"appointHelp"]];
    alertImageView.userInteractionEnabled = YES;
    [self addSubview:alertImageView];
    self.alertImageView = alertImageView;
    
    
    //
    UIButton *appointCloseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    appointCloseBtn.backgroundColor = [UIColor redColor];
    [appointCloseBtn setImage:[UIImage imageNamed:@"appointClose"] forState:UIControlStateNormal];
    [appointCloseBtn addTarget:self action:@selector(appointCloseBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.alertImageView addSubview:appointCloseBtn];
    self.appointCloseBtn = appointCloseBtn;
    
    //6.
    UIButton *alertEnterBtn = [UIButton new];
    [alertEnterBtn setTitle:@"开始预约" forState:UIControlStateNormal];
    [alertEnterBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    alertEnterBtn.titleLabel.font = [UIFont boldSystemFontOfSize:kP(32)];
    alertEnterBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
//    alertEnterBtn.backgroundColor = [UIColor redColor];
    [alertEnterBtn addTarget:self action:@selector(alertEnterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.alertImageView addSubview:alertEnterBtn];
    self.alertEnterBtn = alertEnterBtn;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //0.
//    [self setSubViewsContent];
    
    self.size = [UIScreen mainScreen].bounds.size;
    self.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.5];
    
//    //1.bgView
//    CGFloat bgViewX = kP(90);
//    CGFloat bgViewY = kP(436);
//    CGFloat bgViewW = self.width - 2 * bgViewX;
//    CGFloat bgViewH = kP(400);
//    self.bgView.frame = CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH);
//    
//    self.bgView.layer.cornerRadius = kP(12);
//    self.bgView.clipsToBounds = YES;
//    self.bgView.backgroundColor = [UIColor colorWithHexString:@"#F6F6F6" alpha:1.0];

    
//    //2.topBgView
//    CGFloat topBgViewX = 0;
//    CGFloat topBgViewY = 0;
//    CGFloat topBgViewW = self.bgView.width;
//    CGFloat topBgViewH = kP(296);
//    self.topBgView.frame = CGRectMake(topBgViewX, topBgViewY, topBgViewW, topBgViewH);
//    
//    //3.titleLabel
//    self.titleLabel.x = self.topBgView.width * 0.5 - self.titleLabel.width * 0.5;
//    self.titleLabel.y = kP(36);
//    
//    //4.alertContentLabel
//    CGFloat alertContentLabelX = kP(50);
//    CGFloat alertContentLabelY = CGRectGetMaxY(self.titleLabel.frame) + kP(20);
//    CGFloat alertContentLabelW = self.topBgView.width - 2 * alertContentLabelX;
//    CGFloat alertContentLabelH = [self.alertContentLabel.text getTextHeightWithMaxWidth:alertContentLabelW textFontSize:kP(33)];
//    self.alertContentLabel.frame = CGRectMake(alertContentLabelX, alertContentLabelY, alertContentLabelW, alertContentLabelH);
////    self.alertContentLabel.backgroundColor = [UIColor greenColor];
//
//    //5.alertSetBtn
//    CGFloat alertSetBtnW = kP(260);
//    CGFloat alertSetBtnH = kP(50);
//    CGFloat alertSetBtnX = alertContentLabelX - kP(5);
//    CGFloat alertSetBtnY = self.topBgView.height - kP(20) - alertSetBtnH;
//    self.alertSetBtn.frame = CGRectMake(alertSetBtnX, alertSetBtnY, alertSetBtnW, alertSetBtnH);
//    [self.alertSetBtn setImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
//    [self.alertSetBtn setImage:[UIImage imageNamed:@"checkbox_click"] forState:UIControlStateSelected];
//    [self.alertSetBtn setTitle:@"不再提示" forState:UIControlStateNormal];
//    self.alertSetBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
//    [self.alertSetBtn setTitleColor:[UIColor colorWithHexString:@"#A2A1A1" alpha:1.0] forState:UIControlStateNormal];
//    [self.alertSetBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, kP(70))];
//    [self.alertSetBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, kP(100))];
////    self.alertSetBtn.backgroundColor = [UIColor redColor];
//    
//    //6.sepLine
//    CGFloat sepLineX = 0;
//    CGFloat sepLineY = CGRectGetMaxY(self.topBgView.frame);
//    CGFloat sepLineW = self.bgView.width;
//    CGFloat sepLineH = kP(2);
//    self.sepLine.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.05];
//    self.sepLine.frame = CGRectMake(sepLineX, sepLineY, sepLineW, sepLineH);
//    self.sepLine.layer.shadowColor = [UIColor grayColor].CGColor;
//    self.sepLine.layer.shadowOffset = CGSizeMake(kP(2), 0);
//    self.sepLine.layer.shadowOpacity = 0.3;
    
    
    //
    CGFloat alertImageViewX = kP(52);
    CGFloat alertImageViewY = kP(64);
    CGFloat alertImageViewW = self.width - 2 * alertImageViewX;
    CGFloat alertImageViewH = self.height - 2 * alertImageViewY;
    self.alertImageView.frame = CGRectMake(alertImageViewX, alertImageViewY, alertImageViewW, alertImageViewH);
    
    //
    CGFloat appointCloseBtnWH = kP(80);
    CGFloat appointCloseBtnY = kP(0);
    CGFloat appointCloseBtnX = self.alertImageView.width - appointCloseBtnWH - appointCloseBtnY;
    self.appointCloseBtn.frame = CGRectMake(appointCloseBtnX, appointCloseBtnY, appointCloseBtnWH, appointCloseBtnWH);
    
    //7.alertEnterBtn
    CGFloat alertEnterBtnW = self.alertImageView.width;
    CGFloat alertEnterBtnH = kP(100);
    CGFloat alertEnterBtnX = 0;
    CGFloat alertEnterBtnY = self.alertImageView.height - alertEnterBtnH;
    self.alertEnterBtn.frame = CGRectMake(alertEnterBtnX, alertEnterBtnY, alertEnterBtnW, alertEnterBtnH);
}

//- (void)setSubViewsContent {

//    //1.
//    self.titleLabel.text = @"发起门诊预约";
//    self.titleLabel.font = [UIFont boldSystemFontOfSize:kP(36)];
//    self.titleLabel.textAlignment = NSTextAlignmentCenter;
//    [self.titleLabel sizeToFit];
//    
//    //2.
//    self.alertContentLabel.text = @"帮助患者发起一单新的互联网门诊预约,稍后会由专门的医生助理安排门诊事宜";
//    self.alertContentLabel.font = [UIFont systemFontOfSize:kP(32)];
//    self.alertContentLabel.numberOfLines = 0;
    
    //3.
//    UIImage *norImage = [UIImage createImageWithColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
//    UIImage *selImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#F6F6F6" alpha:1.0]];
//    [self.alertEnterBtn setTitle:@"知道了" forState:UIControlStateNormal];
//    [self.alertEnterBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    self.alertEnterBtn.titleLabel.font = [UIFont boldSystemFontOfSize:kP(32)];
//    self.alertEnterBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
//    [self.alertEnterBtn setBackgroundImage:selImage forState:UIControlStateHighlighted];
//    [self.alertEnterBtn setBackgroundImage:norImage forState:UIControlStateNormal];
//}

//#pragma mark -- alertSetBtnClick
//- (void)alertSetBtnClick:(UIButton *)alertSetBtn {
//    alertSetBtn.selected = !alertSetBtn.selected;
////    [[NSUserDefaults standardUserDefaults] setBool:alertSetBtn.selected forKey:@"alertSetBtnSelected"];
//}


#pragma mark -- appointCloseBtnClick
- (void)appointCloseBtnClick:(UIButton *)appointCloseBtn {
    NSLog(@"---------appointCloseBtnClick");
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeAppointAlertView)]) {
        [self.delegate closeAppointAlertView];
    }
}
#pragma mark -- alertEnterBtnClick
- (void)alertEnterBtnClick:(UIButton *)alertEnterBtn {//
    
    
////    
//    alertEnterBtn.selected = YES;
//    
//    NSLog(@"---------alertEnterBtnClick----%d-----%d--", alertEnterBtn.selected, self.alertSetBtn.selected);
//    BOOL isShowAlert = alertEnterBtn.selected == YES && self.alertSetBtn.selected == YES;
//    
//    [[NSUserDefaults standardUserDefaults] setBool:isShowAlert forKey:@"isShowAlert"];
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterExpertSearchViewController)]) {
        [self.delegate enterExpertSearchViewController];
    }
}


//#pragma mark -- 点击底层蒙板隐藏自己
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    UITouch *touch = [touches anyObject];
//    CGPoint point = [touch locationInView:self];
//    if (!CGRectContainsPoint(self.bgView.frame, point)) {
//        self.hidden = YES;
//    }
//}
@end
