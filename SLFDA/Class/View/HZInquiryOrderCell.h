//
//  HZInquiryOrderCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/10/26.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;

@protocol HZInquiryOrderCellDelegate <NSObject>
- (void)enterSickInfoVCtr:(HZOrder *)order;
- (void)enterCheckReportVCtr:(HZOrder *)order;
@end


@interface HZInquiryOrderCell : HZTableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, assign) int unReadMsgCout;
@property (nonatomic, weak) id<HZInquiryOrderCellDelegate>delegate;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *order;
//
- (void)loadData:(id)data indexPath:(NSIndexPath *)indexPath;
@property (nonatomic, assign) BOOL disPlayed;
@end
