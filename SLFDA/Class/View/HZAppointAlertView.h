//
//  HZAppointAlertView.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZAppointAlertViewDelegate <NSObject>

- (void)enterExpertSearchViewController;
- (void)closeAppointAlertView;
@end
@interface HZAppointAlertView : UIView

@property (nonatomic, weak) id<HZAppointAlertViewDelegate>delegate;
@end
