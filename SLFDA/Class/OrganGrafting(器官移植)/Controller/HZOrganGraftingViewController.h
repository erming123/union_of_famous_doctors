//
//  HZOrganGraftingViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZOrganGraftingViewController : HZViewController

@property (nonatomic,strong) NSArray *dataArray;

@end
