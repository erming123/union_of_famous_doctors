//
//  HZOrganGraftingViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZOrganGraftingViewController.h"
#import "HZOrganGraftingCell.h"
#import "HZWebViewController.h"
#import "HZMessageDB.h"
#import "UITabBar+Badge.h"
@interface HZOrganGraftingViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) HZTableView *organGraftingTableView;
@end

@implementation HZOrganGraftingViewController

-(void)setDataArray:(NSArray *)dataArray{
    
    //倒序 数组
    _dataArray = [[dataArray reverseObjectEnumerator] allObjects];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"新增移植患者";
    //
    HZTableView *organGraftingTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    organGraftingTableView.dataSource = self;
    organGraftingTableView.delegate = self;
    UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        barView.backgroundColor = [UIColor clearColor];
    organGraftingTableView.tableHeaderView = barView;
    organGraftingTableView.tableFooterView = [UIView new];
    organGraftingTableView.backgroundColor = [UIColor colorWithHexString:@"#F1F1F1"];
    //
    UIView *HLine = [[UIView alloc] initWithFrame:CGRectMake(0, 10 - 0.5, HZScreenW, 0.5)];
    HLine.backgroundColor = organGraftingTableView.separatorColor;
    [organGraftingTableView addSubview:HLine];
    //
    [self.view addSubview:organGraftingTableView];
    self.organGraftingTableView = organGraftingTableView;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recievePushShouldReload:) name:KNOTIFICATION_onMesssageReceive object:nil];
}

-(void)recievePushShouldReload:(NSNotification*)notify{
    
    ECMessage *message = notify.object;
    if (![message.userData containsString:kOrganGraftingUserData]) return;
    // 在数组最前面 插入
   // ECTextMessageBody *textMsgBody = (ECTextMessageBody *)message.messageBody;
    id jsonDict = message.userData.mj_JSONObject;
    NSArray *arr = jsonDict[@"patients"];
    NSMutableArray *messageArray = [NSMutableArray array];
    for (int i = 0; i<arr.count; ++i) {
        NSDictionary *textDict = arr[i];
        NSString *jsonText = textDict.mj_JSONString;
        NSDictionary *messageDict = @{
                                      @"from":message.from,
                                      @"to":message.to,
                                      @"messageId":[NSString stringWithFormat:@"%@_%zd",message.messageId,i],
                                      @"timestamp":message.timestamp,
                                      @"messageBodyType":@(message.messageBody.messageBodyType),
                                      @"text":jsonText,
                                      @"userData":message.userData,
                                      @"sessionId":message.sessionId,
                                      @"isGroup":@(message.isGroup),
                                      @"messageState":@(message.messageState),
                                      @"isRead":@(message.isRead),
                                      @"groupSenderName":@"暂无"
                                      };
        [messageArray addObject:messageDict];
  }
    
    NSMutableArray *tempArray = [NSMutableArray array];
    [tempArray addObjectsFromArray:[[messageArray reverseObjectEnumerator] allObjects]];
    [tempArray addObjectsFromArray:self.dataArray];
    // 不要走set 方法
    _dataArray = tempArray;
    [self.organGraftingTableView reloadData];
    
}
#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HZOrganGraftingCell *organGraftingCell = [HZOrganGraftingCell cellWithTableView:tableView];

    NSDictionary *patientAddDict = self.dataArray[indexPath.row];
    organGraftingCell.patientAddDict = patientAddDict;
    return organGraftingCell;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(164);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    HZWebViewController *webVCtr = [HZWebViewController new];
    NSDictionary *newsDict = self.dataArray[indexPath.row];
    NSDictionary *patientDict = [newsDict[@"text"] turnToDict];
    
    
    NSString *patientRecordId = patientDict[@"patientRecordId"];
    NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
//    NSString *patientPhoneNum = [[newsDict[@"messageId"] componentsSeparatedByString:@"|"] firstObject];
    NSString *userPhoneNum = userDict[@"phone"];
    
    
    // 信息未完善
    NSString *urlParamStr = [patientDict[@"dataStatus"] boolValue] ?
    [NSString stringWithFormat:@"loginMobile?account=%@#/patientDetails/%@",userPhoneNum, patientRecordId] :
    [NSString stringWithFormat:@"loginMobile?account=%@#/waitPort/completePatient/%@", userPhoneNum, patientRecordId];
    
    //
    webVCtr.loadUrlStr = kOrgTrantUrlStr(urlParamStr);
    
    // 加载完成的回调
    webVCtr.sucessLoadBlock = ^{
        NSString *read =  newsDict[@"isRead"];
        if (read.boolValue) return ;
        // 去数据库修改状态
        [self changeThePushMessageReadStateWithMessageId:newsDict[@"messageId"]];
        // 发通知给List告知刷新
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadNewsList" object:nil];
    
    // 刷新自己
    NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:newsDict];
    [temp setValue:[NSNumber numberWithBool:YES] forKey:@"isRead"];
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:self.dataArray];
    [tempArray replaceObjectAtIndex:indexPath.row withObject:temp];
    _dataArray = tempArray;
    [self.organGraftingTableView reloadData];
    };
    
    [self.navigationController pushViewController:webVCtr animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}


/**
 * 推送消息 未读数量 --;
 */
- (void)changeThePushMessageReadStateWithMessageId:(NSString*)messageId{
    

    //  应该去数据库中取
     //= [[HZChatHelper sharedInstance].unReadNewsArr copy];
   // if (tempArr.count == 0) {
      NSArray *tempArr = [[HZMessageDB initMessageDB] getAllUnReadMessage];
   // }
    for (ECMessage *message in tempArr) {
        NSString *userData = (NSString *)message.messageId;
        if ([userData isEqualToString:messageId]) {
            // 去数据库,改变状态;
            [[HZMessageDB initMessageDB] changePushMessageReadState:YES withMessageId:message.messageId];
            [[HZChatHelper sharedInstance].unReadNewsArr removeObject:message];
            // 修改
            NSDictionary *newsDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
            NSMutableDictionary *myDict = [NSMutableDictionary dictionaryWithDictionary:newsDict];
            //    [myDict setValue:@"" forKey:sampleNews.userData];
            NSString *value = [myDict valueForKey:kOrganGraftingUserData];
            if (value.integerValue ==0) value= @"1";
            value = [NSString stringWithFormat:@"%zd",value.integerValue-1];
            [myDict setObject:value forKey:kOrganGraftingUserData];
            // 再存元素
            [[NSUserDefaults standardUserDefaults] setObject:myDict forKey:@"profileVCtrNews"];
            
            break;
        }
    }
    
    [self setUnreadMsgCount];
}
// 设置红点
-(void)setUnreadMsgCount{
    
    NSArray *arr =[[[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"] allValues];
    for (int i = 0; i < arr.count; i++) {
        NSString *unReadNewsCountStr = arr[i];
        
        if (![unReadNewsCountStr isEqualToString:@""] && ![unReadNewsCountStr isEqualToString:@"0"]) {
            [HZChatHelper sharedInstance].hasUnReadMsg = YES;
            [self.tabBarController.tabBar showBadgeOnItmIndex:3];
            break;
        }
        
        [HZChatHelper sharedInstance].hasUnReadMsg = NO;
        [self.tabBarController.tabBar hideBadgeOnItemIndex:3];
    }
    
}
    // json转换 字典 or 数组
- (NSArray*)toArrayOrNSDictionary:(NSData *)jsonData{
        NSError *error = nil;
        NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData
                                                              options:NSJSONReadingMutableContainers
                                                                error:&error];
        
        if (jsonObject != nil && error == nil){
            return jsonObject;
        }else{
            // 解析错误
            return nil;
        }
    }

@end
