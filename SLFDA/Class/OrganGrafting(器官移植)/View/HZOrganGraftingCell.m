//
//  HZOrganGraftingCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZOrganGraftingCell.h"

#import "HZUserTool.h"
//
#import "HZMessageDB.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZOrganGraftingCell ()
@property (nonatomic, weak) UIImageView *organGraftingImageView;
@property (nonatomic, weak) UILabel *patientAddLabel;
@property (nonatomic, weak) UILabel *patientAddInforLabel;
@property (nonatomic, weak) UILabel *timeLabel;
@property (nonatomic, weak) UIView *hasReadView;
//@property (nonatomic, weak) UIButton *completeInforBtn;
//** <#注释#>*/
@property (nonatomic, strong) NSDictionary *patientDict;
@property (nonatomic, assign) BOOL isCompleted;
@end
NS_ASSUME_NONNULL_END
@implementation HZOrganGraftingCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //
        //
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    
    UIImageView *organGraftingImageView = [UIImageView new];
    [self.contentView addSubview:organGraftingImageView];
    self.organGraftingImageView = organGraftingImageView;
    
    //
    UILabel *hasReadView = [UILabel new];
    hasReadView.font = [UIFont systemFontOfSize:kP(24)];
    hasReadView.backgroundColor = [UIColor redColor];
    hasReadView.textColor = [UIColor whiteColor];
    hasReadView.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:hasReadView];
    self.hasReadView = hasReadView;
    
    //
    UILabel *patientAddLabel = [UILabel new];
    patientAddLabel.font = [UIFont systemFontOfSize:kP(32)];
    patientAddLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
    //    patientAddLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:patientAddLabel];
    self.patientAddLabel = patientAddLabel;
    
    
    UILabel *patientAddInforLabel = [UILabel new];
    patientAddInforLabel.font = [UIFont systemFontOfSize:kP(26)];
    patientAddInforLabel.textColor = [UIColor colorWithHexString:@"#888888"];
    //    patientAddInforLabel.backgroundColor = [UIColor yellowColor];
    patientAddInforLabel.numberOfLines = 0;
    [self.contentView addSubview:patientAddInforLabel];
    self.patientAddInforLabel = patientAddInforLabel;
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = [UIFont systemFontOfSize:kP(24)];
    timeLabel.textColor = [UIColor colorWithHexString:@"#888888"];
    [self.contentView addSubview:timeLabel];
    self.timeLabel = timeLabel;
    
   
    
//    //
//    UIButton *completeInforBtn = [UIButton new];
//    [completeInforBtn setTitle:@"去完善" forState:UIControlStateNormal];
//    completeInforBtn.titleLabel.font = [UIFont systemFontOfSize:kP(26)];
//    [completeInforBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    completeInforBtn.layer.cornerRadius = kP(5);
//    completeInforBtn.layer.masksToBounds = YES;
//    completeInforBtn.backgroundColor = [UIColor colorWithHexString:@"#2C82FD"];
//    [completeInforBtn addTarget:self action:@selector(completeInforBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentView addSubview:completeInforBtn];
//    self.completeInforBtn = completeInforBtn;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    CGFloat organGraftingImageViewX = kP(24);
    CGFloat organGraftingImageViewY = kP(22);
    CGFloat organGraftingImageViewWH = kP(80);
    self.organGraftingImageView.frame = CGRectMake(organGraftingImageViewX, organGraftingImageViewY, organGraftingImageViewWH, organGraftingImageViewWH);
    self.organGraftingImageView.layer.cornerRadius = organGraftingImageViewWH * 0.5;
    self.organGraftingImageView.clipsToBounds = YES;
    
    //3.
    self.hasReadView.hidden = [self.patientAddDict[@"isRead"] boolValue];
    if (self.hasReadView.hidden == NO) {
        self.hasReadView.width = kP(30);
        self.hasReadView.height = kP(30);
//        self.hasReadView.x = self.organGraftingImageView.centerX + (self.organGraftingImageView.layer.cornerRadius) / sqrtf(2.0) - self.hasReadView.width * 0.5;
//        self.hasReadView.y = self.organGraftingImageView.centerY - (self.organGraftingImageView.layer.cornerRadius) / sqrtf(2.0) - self.hasReadView.width * 0.5;
        self.hasReadView.x = self.organGraftingImageView.right - self.hasReadView.width;
        self.hasReadView.y = self.organGraftingImageView.y;
        self.hasReadView.layer.cornerRadius = self.hasReadView.width * 0.5;
        self.hasReadView.clipsToBounds = YES;
    }
    
    
    //4.
    self.patientAddLabel.x = self.organGraftingImageView.right + kP(24);
    self.patientAddLabel.y = self.organGraftingImageView.centerY - self.patientAddLabel.height - kP(3);
    
    
    //
    self.patientAddInforLabel.x = self.patientAddLabel.x;
    self.patientAddInforLabel.width = self.width - self.patientAddInforLabel.x - kP(170);
    self.patientAddInforLabel.y = self.organGraftingImageView.centerY + kP(7);
    [self.patientAddInforLabel sizeToFit];
    //
    self.timeLabel.x = self.width - self.timeLabel.width - kP(32);
    self.timeLabel.y = self.patientAddLabel.centerY - self.timeLabel.height * 0.5;
    
//    //
//    self.completeInforBtn.hidden = self.isCompleted;
//    if (self.completeInforBtn.hidden == NO) {
//        self.completeInforBtn.width = self.timeLabel.width;
//        self.completeInforBtn.height = kP(60);
//        self.completeInforBtn.x = self.width - self.completeInforBtn.width - kP(32);
//        self.completeInforBtn.y = CGRectGetMaxY(self.timeLabel.frame) + kP(20);
//    }
}

- (void)setSubViewsContent {
    
    self.organGraftingImageView.image = [UIImage imageNamed:@"organGrafting"];
    self.patientAddLabel.text = self.isCompleted ? @"新增成功" : @"完善患者信息";
    [self.patientAddLabel sizeToFit];
    
    NSString *patientNameStr;
    
    if ([self.patientDict isKindOfClass:[NSDictionary class]]) {
        patientNameStr = self.patientDict[@"name"] ? self.patientDict[@"name"] : @"";
    }else{
        patientNameStr = [NSString stringWithFormat:@"%@",self.patientDict];
    }
    NSString *patientAddInforStr = self.isCompleted ? [NSString stringWithFormat:@"患者%@已成功添加到等待移植患者", patientNameStr] : [NSString stringWithFormat:@"患者%@已完成群体反应抗体检查,请完善信息", patientNameStr];
    NSMutableAttributedString *patientAddInforAttribute = [patientAddInforStr changeContentColorWithString:patientNameStr color:kGreenColor];
    self.patientAddInforLabel.attributedText = patientAddInforAttribute;
//    [self.patientAddInforLabel sizeToFit];patientDict
    //
    NSString *timeStr = [self getDateDisplayString: [self.patientAddDict[@"timestamp"] integerValue] * 1000];
    self.timeLabel.text = timeStr;
    [self.timeLabel sizeToFit];
//    self.hasReadView.text = [self.patientAddDict[@"unReadNewsCountStr"] isEqualToString:@"0"] ? @"" : [self.patientAddDict[@"unReadNewsCountStr"] intValue] >= 99 ? @"99" : sself.patientAddDict[@"unReadNewsCountStr"];
}


- (void)setPatientAddDict:(NSDictionary *)patientAddDict {
    _patientAddDict = patientAddDict;
    
    //
    self.patientDict = [_patientAddDict[@"text"] turnToDict];
    self.isCompleted = [self.patientDict[@"dataStatus"] boolValue];
}
//- (void)setNewsLabelhidden {
//    self.hasReadView.hidden = YES;
//}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"organGraftingCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

//时间显示内容
-(NSString *)getDateDisplayString:(long long) miliSeconds{
    
    NSTimeInterval tempMilli = miliSeconds;
    NSTimeInterval seconds = tempMilli/1000.0;
    NSDate *myDate = [NSDate dateWithTimeIntervalSince1970:seconds];
    
    NSCalendar *calendar = [ NSCalendar currentCalendar ];
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth |  NSCalendarUnitYear ;
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[ NSDate date ]];
    NSDateComponents *myCmps = [calendar components:unit fromDate:myDate];
    
    NSDateFormatter *dateFmt = [[ NSDateFormatter alloc ] init];
    if (nowCmps.year != myCmps.year) {
        dateFmt.dateFormat = @"yyyy-MM-dd HH:mm";
    } else {
        if (nowCmps.day==myCmps.day) {
            dateFmt.dateFormat = @"aa KK:mm";
        } else if ((nowCmps.day-myCmps.day)==1) {
            dateFmt.dateFormat = @"昨天 HH:mm";
        } else {
            dateFmt.dateFormat = @"MM-dd HH:mm";
        }
    }
    return [dateFmt stringFromDate:myDate];
}


//- (void)completeInforBtnClick:(UIButton *)completeInforBtn {
//
//    if (self.delegate && [self.delegate respondsToSelector:@selector(enterToCompleteVCtr:)]) {
//        [self.delegate enterToCompleteVCtr:self.patientDict[@"patientRecordId"]];
//    }
//}
@end
