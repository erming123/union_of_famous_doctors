//
//  HZOrganGraftingCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
//@protocol HZOrganGraftingCellDelegate <NSObject>
//@optional
//- (void)enterToCompleteVCtr:(NSString *)patientId;
//@end
@interface HZOrganGraftingCell : UITableViewCell
@property (nonatomic, strong) NSDictionary *patientAddDict;
- (void)setNewsLabelhidden;
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//@property (nonatomic, weak) id<HZOrganGraftingCellDelegate>delegate;
@end
