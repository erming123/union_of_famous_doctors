//
//  HZHomeViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HZUser.h"
@interface HZHomeViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
@end
