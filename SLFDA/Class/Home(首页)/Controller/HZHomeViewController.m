//
//  HZHomeViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/5.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZHomeViewController.h"
#import "HZInquiryOrderCell.h"
#import "HZExampleCell.h"
#import "HZTopHeadView.h"
#import "HZProfileViewController.h"
#import "HZRemoteOrderDetailViewController.h"
#import "HZExampleRemoteOrderDetailViewController.h"
#import "HZLocalOrderDetailViewController.h"
#import "MJRefresh.h"


#import "HZOrder.h"
#import "HZIMChatViewController.h"
#import "HZUserTool.h"
#import "HZUser.h"
#import "HZActivityIndicatorView.h"
#import "HZCommingNewsTon.h"
#import "HZHttpsTool.h"
//
#import "HZTimeModel.h"
#import "HZMessageDB.h"

#import "HZAppointAlertView.h"
#import "HZExpertTeamViewController.h"
//
#import "HZDeviceAccountTool.h"
#import "HZInforCompleteTool.h"
#import "HZHttpsTool.h"

//
#import "HZOrderPatientInforViewController.h"
//

#import "UITabBar+Badge.h"
//
#import "HZOrderHttpsTool.h"
static NSMutableArray *tempArr;
@interface HZHomeViewController ()<UITableViewDataSource, UITableViewDelegate, HZTopHeadViewDelegate, ECDeviceDelegate, ECDelegateBase, HZAppointAlertViewDelegate, HZInquiryOrderCellDelegate, HZExampleCellDelegate>
//** <#注释#>*/
@property (nonatomic, weak) HZTableView *tableView;
//** <#注释#>*/
@property (nonatomic, weak) UIImageView *noOrderImageView;
//** */
@property (nonatomic, weak) UIImageView *headbgImageView;
//** <#注释#>*/
@property (nonatomic, weak) HZTopHeadView *topHeadView;

//** <#注释#>*/
@property (nonatomic, weak) UIButton *exampleBtn;
@property (nonatomic, assign) BOOL isShowExample;

@property (nonatomic, copy) NSString *callID;


@property (nonatomic, assign) NSUInteger offlineCount;

//** <#注释#>*/
@property (nonatomic, strong) NSArray *orderArr;

@property (nonatomic, copy) NSArray *userArr;

//
@property (nonatomic, copy) NSArray *countArr;

//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *postCountArr;
//** <#注释#>*/
@property (nonatomic, strong) NSTimer *timer;
//** <#注释#>*/
@property (nonatomic, weak) HZActivityIndicatorView *activityIndicatorView;
//** <#注释#>*/
@property (nonatomic, strong) HZMessageDB *messageDB;

//** <#注释#>*/
@property (nonatomic, strong) NSMutableArray *orderUnReadCountArr;

//** <#注释#>*/
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;

@property (nonatomic, assign) long long lastTimeStamp;
@property (nonatomic, assign) BOOL isTimeShow;

//** <#注释#>*/
//@property (nonatomic, weak) UILabel *errorLabel;
//** <#注释#>*/
@property (nonatomic, weak) UIButton *appointBtn;

//** <#注释#>*/
@property (nonatomic, strong) HZAppointAlertView *appointAlertView;

@property (nonatomic, copy) NSMutableArray *unReadNewsArr;

@property (nonatomic, copy) NSString *userData;

@end

@implementation HZHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    //1.
    UIImageView *headbgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, HZScreenH == 812.0 ? kP(240) : kP(220))];
    headbgImageView.image = [UIImage imageNamed:@"headBg.png"];
    headbgImageView.userInteractionEnabled = YES;
    headbgImageView.backgroundColor = [UIColor redColor];
    [self.view addSubview:headbgImageView];
    self.headbgImageView = headbgImageView;
    
    //
    [self setHeadView];
    
    //
    HZTableView *tableView = [[HZTableView alloc] initWithFrame:CGRectMake(0, headbgImageView.bottom, self.view.width, self.view.height - headbgImageView.bottom - 44 - SafeAreaBottomHeight) style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    
    //3. 没有订单的时候
    UIImageView *noOrderImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kP(0), kP(0), kP(390), kP(434))];
    noOrderImageView.image = [UIImage imageNamed:@"noOrder"];
    noOrderImageView.center = self.view.center;
    noOrderImageView.y = kP(600);
    noOrderImageView.hidden = YES;
    [self.view addSubview:noOrderImageView];
    self.noOrderImageView = noOrderImageView;
    noOrderImageView.center = self.view.center;
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = noOrderImageView.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    
    //
    //    //3. 网络出现问题的时候
    //    UILabel *errorLabel = [[UILabel alloc] init];
    //    //errorLabel.text = @"暂未获取到数据";
    //    [errorLabel sizeToFit];
    //    errorLabel.center = self.view.center;
    //    errorLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
    //    errorLabel.hidden = YES;
    //    [self.view addSubview:errorLabel];
    //    self.errorLabel = errorLabel;
    
    //4. 查看示例
    UIButton *exampleBtn = [UIButton new];
    exampleBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [exampleBtn setTitle:@"查看示例" forState:UIControlStateNormal];
    [exampleBtn sizeToFit];
    exampleBtn.x = HZScreenW * 0.5 - exampleBtn.width * 0.5;
    exampleBtn.y = self.noOrderImageView.bottom + kP(10);
    [exampleBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    [exampleBtn addTarget:self action:@selector(exampleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    exampleBtn.hidden = YES;
    [self.view addSubview:exampleBtn];
    self.exampleBtn = exampleBtn;
    
    
    //
    [self getOrderList];
    
    
    //    [self refreshOrders];
    //下拉上拉时候才刷新
    [self pushToRefreshData];
    
    
    //    NSLog(@"-----首页个人资料本地化----%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"user"]);
    
    //    NSLog(@"---user------%@", self.user);
    
    [self setUnreadMsgCount];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOrders) name:kEnterForeground object:nil];
    
    
    //    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMessage:) name:KNOTIFICATION_onMesssageReceive object:nil];
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveMessage:) name:KNOTIFICATION_onMesssageOffLineReceive object:nil];
    
    //    NSLog(@"-----xxxxx----%@", self.user);
    //1. 添加预约订单按钮
    CGFloat appointBtnWH = kP(144);
    CGFloat appointBtnX = HZScreenW - kP(60) - appointBtnWH;
    CGFloat appointBtnY = self.view.height - appointBtnWH - 44 - SafeAreaBottomHeight - kP(30);
    UIButton *appointBtn = [[UIButton alloc] initWithFrame:CGRectMake(appointBtnX, appointBtnY, appointBtnWH, kP(142))];
    [appointBtn setImage:[UIImage imageNamed:@"appointBtn.png"] forState:UIControlStateNormal];
    [appointBtn addTarget:self action:@selector(appointBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    appointBtn.hidden =  [NSString isBlankString:self.user.authLocalDoctor] || ![self.user.authLocalDoctor isEqualToString:@"001"];
    [self.view addSubview:appointBtn];
    self.appointBtn = appointBtn;
    
    
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>设备账号登陆
    [self setDeviceAccountAndPassWord];// 网络判断功能是否开启
    
    
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>专家一览
    [self getProfessionArr]; // 专家一览
    
    //
    //    // 荣连云登录
    [self loginChat];
    
    
    //
    UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
    if (UIUserNotificationTypeNone == setting.types) {
        void(^block)() = authorizationsSettingBlock();
        showMessage(@"您消息通知未打开,是否打开?", self, block);
    }
    
}



- (void)loginChat {
    //
    ECLoginInfo * loginInfo = [[ECLoginInfo alloc] init];
    loginInfo.username = kMyChatId;
    loginInfo.appKey = kMyAppKey;
    loginInfo.appToken = kMyAppToken;
    loginInfo.authType = LoginAuthType_NormalAuth;//默认方式登录
    loginInfo.mode = LoginMode_InputPassword;
    
    NSLog(@"---------loginInfo------:%@", loginInfo);
    
    // 容联云登录
    [[ECDevice sharedInstance] login:loginInfo completion:^(ECError *error) {
        if (error.errorCode == ECErrorType_NoError) {
            
            NSLog(@"---------容联云登录成功");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIMOnLine"];
            //            [self.view makeToast:@"容联云登录成功" duration:1.5f position:CSToastPositionCenter];
        } else {
            NSLog(@"---------容联云登录失败");
            //            NSLog(@"-容联云登录失败:%ld");
            //            [self.view makeToast:str duration:1.5f position:CSToastPositionCenter];
        }
    }];
}

#pragma mark -- setDeviceAccountAndPassWord
//1. 判断绑定与开启
- (void)setDeviceAccountAndPassWord {
    // 判断是否绑定
    BOOL isRegisted = [[NSUserDefaults standardUserDefaults] boolForKey:@"deviceAccountIsAssociated"];
    
    if (isRegisted == NO) {// 没有绑定, 判断是否开启
        [HZDeviceAccountTool justDeviceAccountLoginFunctionAvailableSuccess:^(BOOL isAvailable) {
            NSLog(@"---------isAvailable--BOOL------%@", isAvailable == YES ? @"YES":@"NO");
            
            if (isAvailable) {// 已经开启, 判断是否已经注册
                
                // 获取盐值, 用来获取账号和密码
                [HZDeviceAccountTool getMD5SaltWithAccountKey:@"certificate" accountPassWordKey:@"password" success:^(NSDictionary * _Nullable saltDict) {
                    NSLog(@"-----saltDict----%@", saltDict);
                    if (saltDict) {
                        NSString *accountSalt = saltDict[@"certificate"];
                        NSString *accountPassWordSalt = saltDict[@"password"];
                        
                        // 获取账号和密码, 以及剩下的操作(注册, 绑定等)
                        [self setUpDeviceAccountWithAccountSalt:accountSalt accountPassWordSalt:accountPassWordSalt];
                    }
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"-----saltDictError----%@", error);
                }];
                
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"-----设备账号密码功能没有开启----%@", error);
            
            
        }];
    }
}

//2. 获取账号和密码, 以及剩下的操作
- (void)setUpDeviceAccountWithAccountSalt:(NSString *)accountSalt accountPassWordSalt:(NSString *)accountPassWordSalt{
    
    // 根据盐值获取账号和密码
    [HZDeviceAccountTool getDeviceAccountAndDeviceAccountPassWordWithDeviceAccountSalt:accountSalt deviceAccountPassWordSalt:accountPassWordSalt];
    
    NSString *deviceAccount = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceAccount"];
    NSString *deviceAccountPassWord = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceAccountPassWord"];
    
    NSLog(@"----get-----deviceAccount:%@-------deviceAccountPassWord:%@", deviceAccount, deviceAccountPassWord);
    if ([NSString isBlankString:deviceAccount] || [NSString isBlankString:deviceAccountPassWord]) return;
    
    
    NSLog(@"-----判断是否已经注册deviceAccount----%@", deviceAccount);
    [HZDeviceAccountTool justDeviceAccountRegistedWithDeviceAccount:deviceAccount success:^(NSString * _Nullable userId) {
        NSLog(@"-----userId----%@", userId);
        
        if ([NSString isBlankString:userId]) {// 尚未注册， 去注册
            
            [HZDeviceAccountTool registDeviceAccountWithDeviceAccount:deviceAccount deviceAccountPassWord:deviceAccountPassWord success:^(NSString * _Nullable userId) {
                NSLog(@"-----注册userId----%@", userId);
                
                if (![NSString isBlankString:userId]) { // 去绑定
                    
                    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"deviceAccountUserId"];
                    [HZDeviceAccountTool associateDeviceAccountWithDeviceAccount:deviceAccount deviceAccountPassWord:deviceAccountPassWord success:^(id  _Nullable responseObject) {
                        
                        if ([responseObject[@"state"] isEqual:@200]) {// 绑定成功
                            NSLog(@"----绑定--成功---%@", responseObject);
                            // 记录
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"deviceAccountIsAssociated"];
                        } else {
                            NSLog(@"----绑定--未成功---%@", responseObject);
                            // 记录
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"deviceAccountIsAssociated"];
                        }
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        NSLog(@"-----error---失败-%@", error);
                    }];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"-----error----%@", error);
            }];
        } else {// 已经注册,去绑定
            
            
            [HZDeviceAccountTool associateDeviceAccountWithDeviceAccount:deviceAccount deviceAccountPassWord:deviceAccountPassWord success:^(id _Nullable responseObject) {
                if ([responseObject[@"state"] isEqual:@200]) {// 绑定成功
                    NSLog(@"----绑定--成功---%@", responseObject);
                    // 记录
                    [[NSUserDefaults standardUserDefaults] setBool:@YES forKey:@"deviceAccountIsAssociated"];
                } else {
                    NSLog(@"----绑定--未成功---%@", responseObject);
                    // 记录
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"deviceAccountIsAssociated"];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"-----error----%@", error);
            }];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"-----error----%@", error);
    }];
    
    
}

#pragma mark -- get
- (NSMutableArray *)unReadNewsArr {
    if (!_unReadNewsArr) {
        _unReadNewsArr = [NSMutableArray array];
    }
    
    return _unReadNewsArr;
}

#pragma mark -- getdepartMentArr
- (void)getProfessionArr {
    
    [self.activityIndicator startAnimating];
    [HZInforCompleteTool getSubjectArrWithSuccess:^(id responseObject) {
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            [[NSUserDefaults standardUserDefaults] setObject:resultsDict forKey:@"subjectDict"];
            //        NSLog(@"-----获取学科成功----%@", subjectArr);
        }
        
        [self.activityIndicator stopAnimating];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----获取学科失败-----%@", error);
    }];
}


- (void)receiveMessage:(NSNotification *)notification { // 接受未读数，并存起来
    
    ECMessage *message = notification.object;
    NSLog(@"--------首页--ddddddddddssssssssss----%@", message.to);
    
    //    [self.unReadNewsArr addObject:message];
    [self refreshOrdersWithHiddenActivityIndicator:YES];
    // 保存
    [self setUnreadMsgCount];
    //    [self saveWithMessage:message];
    [self.tableView reloadData];
}


-(void)setUnreadMsgCount{
    
    NSArray *arr =[[[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"] allValues];
    for (int i = 0; i < arr.count; i++) {
        NSString *unReadNewsCountStr = arr[i];
        
        if (![unReadNewsCountStr isEqualToString:@""] && ![unReadNewsCountStr isEqualToString:@"0"]) {
            [HZChatHelper sharedInstance].hasUnReadMsg = YES;
            [self.tabBarController.tabBar showBadgeOnItmIndex:3];
            break;
        }
        
        [HZChatHelper sharedInstance].hasUnReadMsg = NO;
        [self.tabBarController.tabBar hideBadgeOnItemIndex:3];
    }
    
}


- (void)receiveOffLineMessage:(NSNotification *)notification {
    NSLog(@"-----offline************首页----%@", notification);
}

#pragma mark -- 初始化数据库
- (HZMessageDB *)messageDB {
    if (_messageDB == nil) {
        _messageDB = [HZMessageDB initMessageDB];
    }
    return _messageDB;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)setUser:(HZUser *)user {
    _user = user;
}

- (void)setHeadView {
    
    //
    HZTopHeadView *topHeadView = [[HZTopHeadView alloc] initWithFrame:CGRectMake(kP(60), self.headbgImageView.height - kP(136) - kP(30), self.headbgImageView.width - kP(80), kP(136))];
    topHeadView.delegate = self;
    
    topHeadView.user = self.user;
    [[NSUserDefaults standardUserDefaults] setValue:self.user.remoteAccount forKeyPath:@"myChatId"];
    [self.headbgImageView addSubview:topHeadView];
    self.topHeadView = topHeadView;
    self.activityIndicatorView.hidden = YES;
}


- (NSArray *)countArr {
    if (!_countArr) {
        _countArr = [NSArray array];
    }
    
    return _countArr;
}

- (NSMutableArray *)postCountArr {
    if (!_postCountArr) {
        _postCountArr = [NSMutableArray array];
    }
    
    return _postCountArr;
}


- (void)setTimeCounter {
    
    
    //   NSLog(@"----首页订单-----%@", self.orderArr);
    //    [self.postCountArr removeAllObjects];
    //
    for (HZOrder *order in self.orderArr) {
        //
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        
        NSDate *newsDateFormatted = [dateFormatter dateFromString:order.treatmentDateTime];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [dateFormatter setTimeZone:timeZone];
        
        //
        NSDate* current_date = [[NSDate alloc] init];
        NSTimeInterval time = [newsDateFormatted timeIntervalSinceDate:current_date];//间隔的秒数
        
        //           NSLog(@"-----time----%f", time);
        
        [self.postCountArr addObject:[HZTimeModel timeModelWithTime:time]];
        
        
        
    }
    
    //    NSLog(@"----countArr-----%@", self.postCountArr);
    
    
    self.countArr = [NSArray arrayWithArray:self.postCountArr];
    
    
    //
    self.timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerEvent) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    
    
}



- (void)reFreshTime {
    
    [self.timer invalidate];
    self.timer = nil;
    //    NSLog(@"----首页订单-----%@", self.orderArr);
    [self.postCountArr removeAllObjects];
    self.countArr = nil;
    //
    for (HZOrder *order in self.orderArr) {
        //
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        
        NSDate *newsDateFormatted = [dateFormatter dateFromString:order.treatmentDateTime];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [dateFormatter setTimeZone:timeZone];
        
        //
        NSDate* current_date = [[NSDate alloc] init];
        NSTimeInterval time = [newsDateFormatted timeIntervalSinceDate:current_date];//间隔的秒数
        
        //        NSLog(@"-----time----%f", time);
        
        [self.postCountArr addObject:[HZTimeModel timeModelWithTime:time]];
    }
    
    //    NSLog(@"----countArr-----%@", self.postCountArr);
    
    
    self.countArr = [NSArray arrayWithArray:self.postCountArr];
    
    
    //
    self.timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerEvent) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    
}






#pragma mark -- 订单数组
- (NSArray *)orderArr {
    if (!_orderArr) {
        _orderArr = [NSArray array];
    }
    
    return _orderArr;
}

#pragma mark -- 进入页面刷新数据
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //    [self setDataBase];
   // self.navigationController.navigationBarHidden = YES;
    // yes ? from tabbar
    if ([self.parentViewController isMovingToParentViewController]) {
        [self.navigationController setNavigationBarHidden:YES];
    }else{
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    self.tabBarController.tabBar.hidden = NO;
    //    [ECDevice sharedInstance].delegate = self;// 这是报错的主要原因
    NSLog(@"---------进入首页面");
    [self refreshOrdersWithHiddenActivityIndicator:YES];
    
    
    NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:self.user.openid];
    if (imageData) {
        self.topHeadView.avatarImage = [UIImage imageWithData:imageData];
    }
    
}


- (void)setDataBase {
    
    //
    [HZOrderHttpsTool getOrdersWithPages:1 countPerPage:100 stateCode:nil success:^(id responseObject) {
        
        NSDictionary *resDict = responseObject;
        NSDictionary *resultDict = resDict[@"results"];
        NSArray *resultArr = resultDict[@"BookingOrderList"];
        NSArray *orderList = [HZOrder mj_objectArrayWithKeyValuesArray:resultArr];
        self.orderArr = [NSArray arrayWithArray:orderList];
        
        self.noOrderImageView.hidden = self.orderArr.count != 0;
        self.exampleBtn.hidden = self.noOrderImageView.hidden;
        if (self.orderArr.count != 0) {
            [self getAllUnReadMsgCountArr];
        }
        
        [self reFreshTime];
        [self.tableView reloadData];
        
    } failure:nil];
    
}

#pragma mark -- getAllUnReadMsgCountArr
- (void)getAllUnReadMsgCountArr {
    
    //1.
    self.orderUnReadCountArr = [NSMutableArray array];
    NSString *unReadCountStr;
    for (HZOrder *order in self.orderArr) {
        NSString *bookingOrderId = order.bookingOrderId;
        
        NSMutableArray *receivedMsgArr = [self.messageDB getReceivedMsgUnReadCountArrWithOrderId:bookingOrderId];
        unReadCountStr = [receivedMsgArr lastObject];
        NSLog(@"--dddsssaaaaa22222-------%@", unReadCountStr);
        
        if (receivedMsgArr.count == 0) {
            unReadCountStr = @"0";
        } else {
            unReadCountStr = [receivedMsgArr lastObject];
        }
        [self.orderUnReadCountArr addObject:unReadCountStr];
    }
    
    NSLog(@"----dsdsdsdsddsds*****-----%@", self.orderUnReadCountArr);
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshUnReadCount" object: self.orderUnReadCountArr];
}


- (NSString *)getCurrentUnReadMsgCountWithOrderId:(NSString *)orderId {
    
    
    //1. 获取之前的所有接受到的消息
    NSMutableArray *receivedMsgArr = [self.messageDB getReceivedMsgUnReadCountArrWithOrderId:orderId];
    
    int lastOneUnReadCount = [[receivedMsgArr lastObject] intValue];
    int unReadCount;
    if (receivedMsgArr.count == 0) {// 第一次接受到
        unReadCount = 1;
    } else {
        unReadCount = lastOneUnReadCount + 1;
    }
    return [NSString stringWithFormat:@"%d", unReadCount];
}
#pragma mark -- 刷新数据
- (void)pushToRefreshData {
    
    
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        
        [self refreshOrdersWithHiddenActivityIndicator:NO];
        
    }];
    self.tableView.mj_header = refreshHeader;
    
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.tableView.mj_header.automaticallyChangeAlpha = YES;
    //    [self.tableView reloadData];
    
}
- (void)refreshOrdersWithHiddenActivityIndicator:(BOOL)hiddenActivityIndicator{
    //    self.errorLabel.hidden = YES;
    
    if (hiddenActivityIndicator == NO) {
        [self.activityIndicator startAnimating];
    }
    
    [self refreshOrders];
}

- (void)refreshOrders {
    
    //
    self.isShowExample = NO;
    self.noOrderImageView.hidden = YES;
    self.exampleBtn.hidden = YES;
    WeakSelf(weakSelf)
    //
    [HZOrderHttpsTool getOrdersWithPages:1 countPerPage:100 stateCode:nil success:^(id responseObject) {
        
        NSLog(@"------------responseObject:123--------------%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resDict = responseObject;
            NSDictionary *resultDict = resDict[@"results"];
            NSArray *resultArr = resultDict[@"BookingOrderList"];
            
            
            //        NSString *str = dict[@"remoteAccount"];
            //        NSLog(@"------------responseObject:321--:%@------------456789:[%@]", [dict getDictJsonStr], str);
            NSArray *orderList = [HZOrder mj_objectArrayWithKeyValuesArray:resultArr];
            weakSelf.orderArr = [NSArray arrayWithArray:orderList];
        } else {
            weakSelf.orderArr = [NSArray array];
        }
        
        //        NSLog(@"------------responseObject:Arr--------------%@", self.orderArr[2]);
        
        __block NSMutableArray *currentOrderIdArr = [NSMutableArray array];
        for (HZOrder *currentOrder in weakSelf.orderArr) {
            
            [currentOrderIdArr addObject:currentOrder.bookingOrderId];
        }
        
        
        
        
        weakSelf.noOrderImageView.hidden = self.orderArr.count != 0;
        weakSelf.exampleBtn.hidden = self.noOrderImageView.hidden;
        if (weakSelf.orderArr.count != 0) {
            [weakSelf getAllUnReadMsgCountArr];
        }
        
        [weakSelf reFreshTime];
        
        [weakSelf.tableView reloadData];
        [weakSelf.activityIndicator stopAnimating];
        [weakSelf.tableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //        self.errorLabel.hidden = NO;
        NSLog(@"-----errorLabel----%@", error);
        [weakSelf.tableView.mj_header endRefreshing];
    }];
    
    
    //
    [HZUserTool getUserWithSuccess:^(id responseObject) {
        if ([responseObject[@"state"] isEqual:@200]) {
            //
            NSLog(@"----启动收取个人信息-----%@", responseObject);
            
            NSDictionary *dataDict = responseObject;
            NSDictionary *resultDict = dataDict[@"results"];
            NSDictionary *userDict = resultDict[@"user"];
            
            //
            //
            [HZUserTool getUserDataButtedVersionWithHospitalId:userDict[@"hospitalId"] success:^(id  _Nullable responseObject) {
                NSLog(@"---------user22222222------:%@", responseObject);
                
                if ([responseObject[@"state"] isEqual:@200]) {
                    NSDictionary *resulDict = responseObject[@"results"];
//                    weakSelf.user.dataButtedVersion = resulDict[@"versions"];
                    NSString *dataButtedVersion = resulDict[@"versions"];
                    [[NSUserDefaults standardUserDefaults] setObject:dataButtedVersion forKey:@"dataButtedVersion"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"-----user111111-------;%@", error);
            }];
            
            // 1.
            [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"user"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        [weakSelf.tableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----启动收取个人信息-----%@", error);
        [weakSelf.tableView.mj_header endRefreshing];
    }];
}
#pragma mark -- 获取订单数据
- (void)getOrderList {
    //    self.errorLabel.hidden = YES;
    [self.activityIndicator startAnimating];
    [HZOrderHttpsTool getOrdersWithPages:1 countPerPage:100 stateCode:nil success:^(id responseObject) {
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resDict = responseObject;
            NSDictionary *resultDict = resDict[@"results"];
            NSArray *resultArr = resultDict[@"BookingOrderList"];
            NSArray *orderList = [HZOrder mj_objectArrayWithKeyValuesArray:resultArr];
            self.orderArr = [NSArray arrayWithArray:orderList];
            self.noOrderImageView.hidden = self.orderArr.count != 0;
            [self setTimeCounter];
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
        } else {
            self.orderArr = [NSArray array];
            self.noOrderImageView.hidden = self.orderArr.count != 0;
            [self setTimeCounter];
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //        [self.activityIndicator stopAnimating];
        //        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"暂无法获取订单信息" preferredStyle:UIAlertControllerStyleAlert];
        //        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        //        [ensureAlertAction setValue:kGreenColor forKey:@"_titleTextColor"];
        //        [alertCtr addAction:ensureAlertAction];
        //        [self presentViewController:alertCtr animated:YES completion:nil];
        //        self.errorLabel.hidden = NO;
    }];
}

#pragma mark -- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.isShowExample ? 1 : self.orderArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isShowExample) {
        HZExampleCell *exampleCell = [HZExampleCell cellWithTableView:tableView];
        exampleCell.delegate = self;
        exampleCell.backgroundColor = [UIColor clearColor];
        return exampleCell;
    }
    
    // ---------------------------
    HZInquiryOrderCell *inquiryOrderCell = [HZInquiryOrderCell cellWithTableView:tableView];
    //
    if (self.orderArr.count == 0) {
        
        return nil;
    }
    inquiryOrderCell.order = self.orderArr[indexPath.row];
    inquiryOrderCell.delegate = self;
    inquiryOrderCell.unReadMsgCout = [self.orderUnReadCountArr[indexPath.row] intValue];
    inquiryOrderCell.backgroundColor = [UIColor clearColor];
    return inquiryOrderCell;
}


#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(388);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (self.isShowExample) {
        HZExampleRemoteOrderDetailViewController *exampleRemoteOrderDetailVCtr = [[HZExampleRemoteOrderDetailViewController alloc] init];
        exampleRemoteOrderDetailVCtr.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:exampleRemoteOrderDetailVCtr animated:YES];
        NSLog(@"--------ssss");
        return;
    }
    // NSLog(@"--qwe--:%@", self.orderArr[indexPath.row]);
    HZInquiryOrderCell *cell = (HZInquiryOrderCell *)[tableView cellForRowAtIndexPath:indexPath];
    BOOL isLocalOrder = [cell.order.orderType isEqualToString:@"01"];
    
    if (isLocalOrder) {
        
        
        HZLocalOrderDetailViewController *localOrderDetailVCtr = [[HZLocalOrderDetailViewController alloc] init];
        localOrderDetailVCtr.localOrder = self.orderArr[indexPath.row];
        localOrderDetailVCtr.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:localOrderDetailVCtr animated:YES];
    } else {
        HZRemoteOrderDetailViewController *remoteOrderDetailVCtr = [[HZRemoteOrderDetailViewController alloc] init];
        remoteOrderDetailVCtr.remoteOrder = self.orderArr[indexPath.row];
        //        int offLineMsgCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"offLineMsgCount"] intValue];
        //        remoteOrderDetailVCtr.remoteUnReadCount = [self.orderUnReadCountArr[indexPath.row] intValue] + offLineMsgCount;
        
        remoteOrderDetailVCtr.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:remoteOrderDetailVCtr animated:YES];
        
    }
}


#pragma mark -- HZTopHeadViewDelegate
- (void)enterProfile {
    self.topHeadView.userInteractionEnabled = NO;
    
    NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    
    if (userDict) {
        HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
        user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
        HZProfileViewController *proFileVCtr = [HZProfileViewController new];
        proFileVCtr.user = user;
        proFileVCtr.isNewsCome = self.unReadNewsArr.count != 0;
        [self.navigationController pushViewController:proFileVCtr animated:YES];
        self.topHeadView.userInteractionEnabled = YES;
    } else {
        [HZUserTool getUserWithSuccess:^(id responseObject) {
            
            if ([responseObject[@"state"] isEqual:@200]) {
                NSDictionary *dataDict = responseObject;
                NSDictionary *resultDict = dataDict[@"results"];
                NSDictionary *userDict = resultDict[@"user"];
                
                HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
                user = [HZUser mj_objectWithKeyValues:[user propertyValueDict]];
                HZProfileViewController *proFileVCtr = [HZProfileViewController new];
                proFileVCtr.user = user;
                //        proFileTVCtr.user = [HZUserTool getUserInfor];
                proFileVCtr.avatarImage = self.topHeadView.avatarImage;
                proFileVCtr.isNewsCome = self.unReadNewsArr.count != 0;
                [self.navigationController pushViewController:proFileVCtr animated:YES];
                
                self.topHeadView.userInteractionEnabled = YES;
            }
       
            
        } failure:nil];
        
    }
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
    //    [self.timer invalidate];
    //    self.timer = nil;
    
    //    [super viewDidDisappear:animated];
    
    
    //    NSLog(@"----qwezxc----------------%@", self.unReadNewsArr);
    if (self.unReadNewsArr.count == 0) {
        return;
    }
    
    
    
    //    NSMutableArray *newsListArr = [NSMutableArray array];
    NSMutableArray *userDataArr = [NSMutableArray array];
    
    for (ECMessage *message in self.unReadNewsArr) {
        
        NSLog(@"----xde------:%@", message.userData);
        
        NSString *userData = (NSString *)message.userData;
        [userDataArr addObject:userData];
    }
    
    NSLog(@"----qas------:%@", userDataArr);
    
    NSStringCompareOptions comparisonOptions = NSCaseInsensitiveSearch|NSNumericSearch|
    NSWidthInsensitiveSearch|NSForcedOrderingSearch;
    NSComparator sort = ^(NSString *obj1,NSString *obj2){
        NSRange range = NSMakeRange(0,obj1.length);
        return [obj1 compare:obj2 options:comparisonOptions range:range];
    };
    NSArray *tempUserDataArr = [userDataArr sortedArrayUsingComparator:sort];
    
    
    NSLog(@"----zxcvb----------------%@", tempUserDataArr);
    
    
    
    NSMutableArray *resultArr = [NSMutableArray array];
    
    NSString *lastC = @"a"; // 随便定义一个字符而已
    NSMutableArray *dataArr;
    
    
    for (NSString *user in tempUserDataArr) {
        
        // 首字母
        
        
        // 是否是英文字母,ewewew, fdffd
        
        
        if (![user isEqualToString:lastC]) { // 是英文字母， 如果字符属性位置不是'1'的， 检索首字母一样的
            lastC = user; //  lastC = 's'
            
            // 不进，此刻没有数组
            if (dataArr && dataArr.count > 0) { // 把上一个数组放进最终大数组里
                [resultArr addObject:dataArr];
            }
            
            // 创建一个新的小数组， 把user放进去
            dataArr = [[NSMutableArray alloc] init];
            [dataArr addObject:user];
            
        } else { // 此时此字符等于上一个字符， 相同的字符。
            [dataArr addObject:user];
        }
        
    }
    
    
    
    // 1. 把最后一个数组放进去
    if (dataArr && dataArr.count > 0) {
        [resultArr addObject:dataArr];
    }
    
    
    NSMutableDictionary *newsDict;
    NSDictionary *myDict  = [[NSUserDefaults standardUserDefaults] objectForKey:@"profileVCtrNews"];
    if (myDict) {
        newsDict = [NSMutableDictionary dictionaryWithDictionary:myDict];
    } else {
        newsDict = [NSMutableDictionary dictionary];
    }
    
    NSLog(@"----rtyu----------------%@", resultArr);
    NSLog(@"---qsc--:%@", newsDict);
    for (NSArray *arr in resultArr) {
        
        NSString *countStr = [NSString stringWithFormat:@"%d", (int)arr.count];
        //        NSString *countStr = @"10";
        NSString *keyStr = (NSString *)[arr firstObject];
        
        
        NSLog(@"--:%lu----:%lu", countStr.length, keyStr.length);
        //        [newsDict setObject:countStr  forKey:keyStr];
        
        [newsDict setValue:countStr forKey:keyStr];
        
        
    }
    
    NSLog(@"HOME-SET:%@", newsDict);
    NSDictionary *myNewDict = [NSDictionary dictionaryWithDictionary:newsDict];
    [[NSUserDefaults standardUserDefaults] setObject:myNewDict forKey:@"profileVCtrNews"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //
    [self.unReadNewsArr removeAllObjects];
}

#pragma mark -- cellDisplay
- (void)timerEvent {
    
    
    //    NSLog(@"---------eeeeee");
    for (int count = 0; count < self.countArr.count; count++) {
        
        HZTimeModel *model = self.countArr[count];
        [model countDown];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notification_timeCell" object:nil];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.countArr.count != 0) {
        HZInquiryOrderCell *tmpCell = (HZInquiryOrderCell *)cell;
        tmpCell.disPlayed = YES;
        //        NSLog(@"------ddddsssssssssssssssss---%@", self.countArr);
        [tmpCell loadData:self.countArr[indexPath.row] indexPath:indexPath];
        
    }
    
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    
    if (self.countArr.count != 0) {
        HZInquiryOrderCell *tmpCell = (HZInquiryOrderCell *)cell;
        
        tmpCell.disPlayed = NO;
    }
    
    
}

#pragma mark -- appointBtnClick

- (HZAppointAlertView *)appointAlertView {
    if (!_appointAlertView) {
        
        //
        _appointAlertView = [HZAppointAlertView new];
        _appointAlertView.delegate = self;
        _appointAlertView.hidden = YES;
        [self.view addSubview:_appointAlertView];
    }
    return _appointAlertView;
}


#pragma mark -- HZOrderCellDelegate
- (void)enterSickInfoVCtr:(HZOrder *)order {
    HZOrderPatientInforViewController *patientInforVCtr = [HZOrderPatientInforViewController new];
    patientInforVCtr.basicOrder = order;
    patientInforVCtr.isReportBtnSel = NO;
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
}


- (void)enterCheckReportVCtr:(HZOrder *)order {
    HZOrderPatientInforViewController *patientInforVCtr = [HZOrderPatientInforViewController new];
    patientInforVCtr.basicOrder = order;
    patientInforVCtr.isReportBtnSel = YES;
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
    
}


#pragma mark -- HZExampleCellDelegate
- (void)enterExampleSickInfoVCtr:(HZOrder *)exampleOrder {
    HZOrderPatientInforViewController *patientInforVCtr = [HZOrderPatientInforViewController new];
    patientInforVCtr.basicOrder = exampleOrder;
    patientInforVCtr.isReportBtnSel = NO;
    exampleOrder.isExampleOrder = YES;
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
}
- (void)enterExampleCheckReportVCtr:(HZOrder *)exampleOrder {
    
    
    HZOrderPatientInforViewController *patientInforVCtr = [HZOrderPatientInforViewController new];
    
    patientInforVCtr.basicOrder = exampleOrder;
    
    patientInforVCtr.isReportBtnSel = YES;
    
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
}

#pragma mark -- appointBtnClick预约按钮点击
- (void)appointBtnClick:(UIButton *)appointBtn {
    //    self.appointAlertView.hidden = [[NSUserDefaults standardUserDefaults] boolForKey:@"isShowAlert"];
    //
    //    if (self.appointAlertView.hidden == YES) {
    //        HZExpertSearchViewController *expertSearchVCtr = [HZExpertSearchViewController new];
    //        expertSearchVCtr.isFromHomeVCtr = YES;
    //        [self.navigationController pushViewController:expertSearchVCtr animated:YES];
    //    }
    
    self.appointAlertView.hidden = NO;
    self.tabBarController.tabBar.hidden = YES;
    
}


#pragma mark --cexampleBtnClick---查看示例
- (void)exampleBtnClick:(UIButton *)exampleBtn {
    NSLog(@"---------exampleBtnClick");
    self.noOrderImageView.hidden = YES;
    exampleBtn.hidden = YES;
    self.isShowExample = YES;
    [self.tableView reloadData];
}
#pragma mark -- HZAppointAlertViewDelegate
- (void)enterExpertSearchViewController {
    self.appointAlertView.hidden = YES;
    HZExpertTeamViewController *expertTeamVCtr = [HZExpertTeamViewController new];
    expertTeamVCtr.user = self.user;
    [self.navigationController pushViewController:expertTeamVCtr animated:YES];
}

- (void)closeAppointAlertView {
    self.appointAlertView.hidden = YES;
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark -- 进入AppStore更新下载

- (void)loadAppStoreController {
    NSString *appStoreUrl = @"https://itunes.apple.com/us/app/ming-yi-lian-meng/id1173508999?l=zh&ls=1&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreUrl] options:@{} completionHandler:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

