//
//  HZOrder.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZOrder.h"
@implementation HZOrder
- (NSString *)description {
    return [NSString stringWithFormat:@"hospitalName:[%@]-----orderType:[%@]-----outpatientName:[%@]--------outpatientGenderValue:[%@]-----bookingStateValue:[%@]----bookingStateCode:[%@]----localAccount:[%@]-------remoteAccount:[%@]", self.hospitalName, self.orderType, self.outpatientName, self.outpatientGenderValue, self.bookingStateValue, self.bookingStateCode, self.localAccount, self.remoteAccount];
}

+ (NSDictionary*)mj_objectClassInArray{
    
    return @{@"examReports":[HZIllnessDataImg class],
             @"laboratoryReports":[HZIllnessDataImg class]};
}


@end
