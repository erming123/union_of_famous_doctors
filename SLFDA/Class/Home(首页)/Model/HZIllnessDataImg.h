//
//  HZIllnessDataImg.h
//  SLFDA
//
//  Created by coder_xu on 2017/10/16.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZIllnessDataImg : NSObject
/** 订单ID*/
@property (nonatomic,strong) NSString *bookingOrderId;
/**Type */
@property (nonatomic,strong) NSString *reportType;
/**图片地址 */
@property (nonatomic,strong) NSString *reportImageUri;
/**创建时间 */
@property (nonatomic,strong) NSString *createTime;
/**更新时间 */
@property (nonatomic,strong) NSString *updateTime;

@end
