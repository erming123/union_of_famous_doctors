//
//  HZRemoteExpert.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZJsonModel.h"
@interface HZRemoteExpert : HZJsonModel
@property (nonatomic, copy) NSString *experDeptCode;
@property (nonatomic, copy) NSString *experDeptValue;
@property (nonatomic, copy) NSString *expertName;
@property (nonatomic, copy) NSString *expertOpenid;
@property (nonatomic, copy) NSString *expertPhone;
@property (nonatomic, copy) NSString *expertTitleCode;
@property (nonatomic, copy) NSString *expertTitleValue;
@end
