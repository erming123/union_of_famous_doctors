//
//  HZTimeModel.m
//  HZTimeDownerDemo
//
//  Created by 季怀斌 on 2016/11/30.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZTimeModel.h"

@implementation HZTimeModel
// 获取
+ (instancetype)timeModelWithTime:(int)time {
    
    HZTimeModel *model = [self new];

    model.timeCount = time;
    
    return model;
}

// 倒计时调用
- (void)countDown {
    _timeCount -= 1;
    
//     NSLog(@"---------ddddddddddd---%d", _timeCount);
}


// 当前时间
- (NSString*)currentTimeString {
    
//    NSLog(@"----dsdsdsdsdsdsdsds-----%d", _timeCount);
    NSString *str;
    if (_timeCount <= 0) {
        str = @"";
        return str;
        
    } else {
        
        
        int day = _timeCount / (3600 * 24); // 天
        int hour = _timeCount % (3600 * 24) /3600; // 小时
        int minite = _timeCount % (3600 * 24) % 3600/ 60;// 分钟
        
        if (_timeCount >= 3600 * 24) {
            str = [NSString stringWithFormat:@"%d天%d小时后", day, hour];
        } else if (_timeCount > 3600 && _timeCount < 24 * 3600) {
            str = [NSString stringWithFormat:@"%d小时%d分后", hour, minite];
        } else if (_timeCount > 60 && _timeCount <= 3600) {
            str = [NSString stringWithFormat:@"%d分后",minite];
        } else if (_timeCount <= 60) {
            str = @"";
        } //        return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",(long)_timeCount/3600,(long)_timeCount%3600/60,(long)_timeCount%60];
        
        
//        str = [NSString stringWithFormat:@"%d", _timeCount];
    }
    
    
    return str;
}
@end
