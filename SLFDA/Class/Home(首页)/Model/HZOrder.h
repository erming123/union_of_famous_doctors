//
//  HZOrder.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/29.
//  Copyright © 2016年 huazhuo. All rights reserved.
//


#import "HZJsonModel.h"
#import "HZIllnessDataImg.h"
@interface HZOrder : HZJsonModel

// 订单
@property (nonatomic, copy) NSString *bookingOrderId;// 订单id
@property (nonatomic, copy) NSString *orderType;//订单类型：本地订单== 01,远程订单==02,
@property (nonatomic, copy) NSString *orderLevelCode;// 订单状态说明
@property (nonatomic, copy) NSString *orderLevelValue;// 订单状态说明
@property (nonatomic, copy) NSString *bookingDatetime;// 预约时间
@property (nonatomic, copy) NSString *treatmentDateTime;// 预约时间
@property (nonatomic, copy) NSString *updateTime;// 预约时间
@property (nonatomic, copy) NSString *bookingStateCode;// 订单状态：
@property (nonatomic, copy) NSString *bookingStateValue;// 订单状态说明

// 支付数据
//@property (nonatomic, copy) NSString *payState;
//@property (nonatomic, copy) NSString *goodsId;
//@property (nonatomic, copy) NSString *price;
//@property (nonatomic, copy) NSString *startTime;
//@property (nonatomic, copy) NSString *endTime;
//
// 专家
@property (nonatomic, copy) NSString *experDeptValue;// 订单状态说明
@property (nonatomic, copy) NSString *expertName;// 订单状态说明
@property (nonatomic, copy) NSString *expertOpenid;// 订单状态说明
@property (nonatomic, copy) NSString *expertPhone;// 订单状态说明
@property (nonatomic, copy) NSString *expertTitleCode;// 订单状态说明
@property (nonatomic, copy) NSString *expertTitleValue;// 订单状态说明

// 本地医生
@property (nonatomic, copy) NSString *localDocotorOpenid;// 订单状态说明
@property (nonatomic, copy) NSString *localDocotorPhone;// 订单状态说明
@property (nonatomic, copy) NSString *localDoctorDeptValue;// 订单状态说明
@property (nonatomic, copy) NSString *localDoctorName;// 订单状态说明
@property (nonatomic, copy) NSString *localDoctorTitleCode;// 订单状态说明
@property (nonatomic, copy) NSString *localDoctorTitleValue;// 订单状态说明

// 患者
@property (nonatomic, copy) NSString *outpatientGenderValue;// 患者性别
@property (nonatomic, copy) NSString *outpatientAge;// 患者年龄
// 疾病
@property (nonatomic, copy) NSString *incId;// 订单状态说明
@property (nonatomic, copy) NSString *icd10Code;// 订单状态说明
@property (nonatomic, copy) NSString *icd10Value;// 疾病名字
@property (nonatomic, copy) NSString *remoteAccount;// 远程医生容联云帐号
@property (nonatomic, copy) NSString *localAccount;// 本地医生容联云帐号
@property (nonatomic, copy) NSString *sicknessRemark;// 订单状态说明
@property (nonatomic, copy) NSString *submitOpenid;// 订单状态说明
//新增字段 图片资料;
@property (nonatomic,strong)NSArray<HZIllnessDataImg*> *examReports;// 影像字段
@property (nonatomic,strong)NSArray<HZIllnessDataImg*> *laboratoryReports;//检验资料
// 医院
@property (nonatomic, copy) NSString *hospitalId;// 订单状态说明
@property (nonatomic, copy) NSString *hospitalName;// 订单医院

// 门诊
@property (nonatomic, copy) NSString *outpatientFee;// 订单状态说明
@property (nonatomic, copy) NSString *outpatientIdcard;// 患者身份证号
@property (nonatomic, copy) NSString *outpatientName;// 患者姓名
@property (nonatomic, copy) NSString *outpatientPhone;// 订单状态说明
@property (nonatomic, copy) NSString *outpatientRecordNumber;// 订单状态说明

// 支付
@property (nonatomic, copy) NSString *paySerialNumber;// 订单状态说明
@property (nonatomic, copy) NSString *payTypeCode;// 订单状态说明
@property (nonatomic, copy) NSString *payTypeValue;// 订单状态说明

// 新增prop
@property (nonatomic, assign) BOOL isExampleOrder;
@property (nonatomic, assign) BOOL isInOrderDetail;
@end
