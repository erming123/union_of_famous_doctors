//
//  HZLocalPatient.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZJsonModel.h"
@interface HZLocalPatient : HZJsonModel
@property (nonatomic, copy) NSString *localDocotorOpenid;
@property (nonatomic, copy) NSString *localDocotorPhone;
@property (nonatomic, copy) NSString *localDoctorName;
@end
