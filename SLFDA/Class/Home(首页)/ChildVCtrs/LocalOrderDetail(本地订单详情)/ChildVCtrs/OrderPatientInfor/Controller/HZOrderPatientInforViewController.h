//
//  HZOrderPatientInforViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@interface HZOrderPatientInforViewController : HZViewController
@property (nonatomic, assign) CGFloat titleViewY;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *basicOrder;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *localCheckListArr;

@property (nonatomic, assign) BOOL isReportBtnSel;
@end
