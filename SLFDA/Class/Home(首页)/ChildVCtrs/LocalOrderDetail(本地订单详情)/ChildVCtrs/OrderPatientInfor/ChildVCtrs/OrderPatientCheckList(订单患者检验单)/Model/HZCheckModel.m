//
//  HZCheckModel.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/12/2.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZCheckModel.h"
//#import <MJExtension.h>
@implementation HZCheckModel
- (NSString *)description {
    return [NSString  stringWithFormat:@"%@-------HZCheckModel----------%@----------------%@, ---%@----------------%@", self.jcxmmc, self.sjbrxm, self.jcybid, self.yblxmc, self.flag];
}

// 也可以在控制器里写
//+ (NSDictionary *)mj_replacedKeyFromPropertyName {
//    
//    return @{@"checkItem" : @"jcxmmc",// 检验项目
//             @"checkDocName" : @"shryxm",// 审核医生姓名， 修改后的
//             @"checkTime" : @"jcdjsj",// 检验时间
//             @"submissionDocName" : @"sjbrxm",// 送检病人姓名
//             @"diagnoseResult" : @"yxzdnr",// 诊断结果
//             @"reportResult" : @"yxsjnr",// 报告结果
//             @"" : @"jcmdmc",// 检验项目
//             @"" : @"kdysxm",// 医生姓名
//             @"" : @"ybjgsj",// 检验时间
//             @"" : @"yblxmc",// 其他
//             @"" : @"kdksmc",// 门诊
//             @"" : @"flag",
//             @"" : @"jcybid"};// 检验单号
//}
@end
