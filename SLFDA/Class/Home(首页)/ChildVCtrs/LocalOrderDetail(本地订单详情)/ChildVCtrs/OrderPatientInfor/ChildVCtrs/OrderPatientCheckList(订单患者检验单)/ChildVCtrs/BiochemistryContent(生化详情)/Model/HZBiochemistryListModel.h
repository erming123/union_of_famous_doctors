//
//  HZBiochemistryListModel.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/12/2.
//  Copyright © 2016年 huazhuo. All rights reserved.
//


#import "HZJsonModel.h"
@interface HZBiochemistryListModel : HZJsonModel
@property (nonatomic, copy) NSString *jcxmzw;// 名称
@property (nonatomic, copy) NSString *ybjcjg;// 结果
@property (nonatomic, copy) NSString *jcjgbz;// 升降符号
@property (nonatomic, copy) NSString *xmjgfw;// 参考范围
@property (nonatomic, copy) NSString *xmjldw;// 单位
@end
