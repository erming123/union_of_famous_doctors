//
//  HZWebImageContentViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2018/1/9.
//  Copyright © 2018年 huazhuo. All rights reserved.
//

#import "HZWebImageContentViewController.h"

@interface HZWebImageContentViewController ()

@end

@implementation HZWebImageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    UIImage *shadowImage = [UIImage createImageWithColor:[UIColor blackColor] withSize:CGSizeMake(self.view.frame.size.width, 1)];
    [self.navigationController.navigationBar setShadowImage:shadowImage];
  
    //
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    UIImage *shadowImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#d9d9d9"] withSize:CGSizeMake(self.view.frame.size.width, 1)];
    [self.navigationController.navigationBar setShadowImage:shadowImage];
    //
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
