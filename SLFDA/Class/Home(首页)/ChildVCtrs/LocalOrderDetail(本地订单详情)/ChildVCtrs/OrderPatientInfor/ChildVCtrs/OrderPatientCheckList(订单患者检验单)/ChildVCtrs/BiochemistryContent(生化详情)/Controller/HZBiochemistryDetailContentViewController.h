//
//  HZBiochemistryDetailContentTableViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZCheckModel, HZBiochemistryListModel, HZOrder;
@interface HZBiochemistryDetailContentViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZCheckModel *biochemistryCheckModel;// 顶部数据
//** <#注释#>*/
@property (nonatomic, strong) HZBiochemistryListModel *biochemistryListModel;
@property (nonatomic, strong) HZOrder *biochemistryDetailPassOrder;
@end
