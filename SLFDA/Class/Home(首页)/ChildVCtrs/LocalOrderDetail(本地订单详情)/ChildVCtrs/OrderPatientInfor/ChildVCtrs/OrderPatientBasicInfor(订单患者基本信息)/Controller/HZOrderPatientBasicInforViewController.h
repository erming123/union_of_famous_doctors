//
//  HZOrderPatientBasicInforViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/6.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@interface HZOrderPatientBasicInforViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *basicOrder;
@end
