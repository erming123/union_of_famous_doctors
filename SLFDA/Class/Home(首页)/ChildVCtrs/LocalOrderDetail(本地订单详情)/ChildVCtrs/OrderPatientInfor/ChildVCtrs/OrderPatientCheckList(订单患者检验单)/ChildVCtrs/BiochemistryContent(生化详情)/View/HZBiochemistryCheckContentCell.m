//
//  HZBiochemistryCheckContentCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZBiochemistryCheckContentCell.h"
#import "HZCheckModel.h"
#import "HZBiochemistryListModel.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZBiochemistryCheckContentCell ()
@property (nonatomic, strong) UILabel *biochemistryCheckNameLabel;
@property (nonatomic, strong) UILabel *biochemistryCheckResultLabel;
@property (nonatomic, strong) UILabel *biochemistryCheckStandardLabel;
@property (nonatomic, strong) UILabel *biochemistryCheckUnitLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZBiochemistryCheckContentCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UILabel *biochemistryCheckNameLabel = [UILabel new];
    [biochemistryCheckNameLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    biochemistryCheckNameLabel.numberOfLines = 0;
    [self addSubview:biochemistryCheckNameLabel];
    self.biochemistryCheckNameLabel= biochemistryCheckNameLabel;
    
    UILabel *biochemistryCheckResultLabel = [UILabel new];
    [biochemistryCheckResultLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    [self addSubview:biochemistryCheckResultLabel];
    self.biochemistryCheckResultLabel= biochemistryCheckResultLabel;

    UILabel *biochemistryCheckStandardLabel = [UILabel new];
    [biochemistryCheckStandardLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:biochemistryCheckStandardLabel];
    self.biochemistryCheckStandardLabel= biochemistryCheckStandardLabel;

    UILabel *biochemistryCheckUnitLabel = [UILabel new];
    [biochemistryCheckUnitLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:biochemistryCheckUnitLabel];
    self.biochemistryCheckUnitLabel= biochemistryCheckUnitLabel;

}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //
    [self setSubViewsContent];
    
    
    // 内容
    CGSize biochemistryCheckNameSize = [self.biochemistryCheckNameLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(35)];
    CGFloat biochemistryCheckNameLabelW = biochemistryCheckNameSize.width;
    CGFloat biochemistryCheckNameLabelH = biochemistryCheckNameSize.height;
    CGFloat biochemistryCheckNameLabelX = kP(28);
    CGFloat biochemistryCheckNameLabelY = self.height * 0.5 - biochemistryCheckNameLabelH * 0.5;
    self.biochemistryCheckNameLabel.frame = CGRectMake(biochemistryCheckNameLabelX, biochemistryCheckNameLabelY, biochemistryCheckNameLabelW, biochemistryCheckNameLabelH);
    
    // 结果
    CGSize biochemistryCheckResultSize = [self.biochemistryCheckResultLabel.text getTextSizeWithMaxWidth:kP(400) textFontSize:kP(35)];
    CGFloat biochemistryCheckResultLabelW = biochemistryCheckResultSize.width;
    CGFloat biochemistryCheckResultLabelH = biochemistryCheckResultSize.height;
    CGFloat biochemistryCheckResultLabelX = self.width * 0.5 - biochemistryCheckResultLabelW * 0.5 + kP(10);
    CGFloat biochemistryCheckResultLabelY = self.height * 0.5 - biochemistryCheckResultLabelH * 0.5;
    self.biochemistryCheckResultLabel.frame = CGRectMake(biochemistryCheckResultLabelX, biochemistryCheckResultLabelY, biochemistryCheckResultLabelW, biochemistryCheckResultLabelH);
    
    // 参考范围
    CGSize biochemistryCheckStandardSize = [self.biochemistryCheckStandardLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(38)];
    CGFloat biochemistryCheckStandardLabelW = biochemistryCheckStandardSize.width;
    CGFloat biochemistryCheckStandardLabelH = biochemistryCheckStandardSize.height;
    CGFloat biochemistryCheckStandardLabelX = self.width - biochemistryCheckStandardLabelW - kP(28);
    CGFloat biochemistryCheckStandardLabelY = kP(3);
    self.biochemistryCheckStandardLabel.frame = CGRectMake(biochemistryCheckStandardLabelX, biochemistryCheckStandardLabelY, biochemistryCheckStandardLabelW, biochemistryCheckStandardLabelH);
    
    // 单位
    CGSize biochemistryCheckUnitdSize = [self.biochemistryCheckUnitLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(38)];
    CGFloat biochemistryCheckUnitLabelW = biochemistryCheckUnitdSize.width;
    CGFloat biochemistryCheckUnitLabelH = biochemistryCheckUnitdSize.height;
    CGFloat biochemistryCheckUnitLabelX = self.width - biochemistryCheckUnitLabelW - kP(28);
    CGFloat biochemistryCheckUnitLabelY = self.biochemistryCheckStandardLabel.bottom + kP(5);
    self.biochemistryCheckUnitLabel.frame = CGRectMake(biochemistryCheckUnitLabelX, biochemistryCheckUnitLabelY, biochemistryCheckUnitLabelW, biochemistryCheckUnitLabelH);
    
}

- (void)setSubViewsContent {
    
    //1.
    self.biochemistryCheckNameLabel.text = self.biochemistryListModel.jcxmzw;// 名称
    //2.---------------------
    NSString *bioStateStr;
    if ([self.biochemistryListModel.jcxmzw isEqualToString:@"说明"]) {
        bioStateStr = @"";
    } else {
        bioStateStr = self.biochemistryListModel.jcjgbz;
    }
    //
    NSString *preStr = [NSString stringWithFormat:@"%@ %@", self.biochemistryListModel.ybjcjg, bioStateStr];
    //
    if ([self.biochemistryListModel.jcjgbz isEqualToString:@"正常"] || self.biochemistryListModel.jcjgbz == nil) {
        
        //
        self.biochemistryCheckResultLabel.text = self.biochemistryListModel.ybjcjg;// 结果
    } else {
               
       NSMutableAttributedString *resultAttributedStr = [[NSMutableAttributedString alloc] initWithString:preStr];
       [resultAttributedStr addAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} range:NSMakeRange(preStr.length - 1, 1)];
       self.biochemistryCheckResultLabel.attributedText = resultAttributedStr;// 结果
    }
    
    
    
    //3. 参考范围和单位
    self.biochemistryCheckStandardLabel.text = self.biochemistryListModel.xmjgfw;
    self.biochemistryCheckUnitLabel.text = self.biochemistryListModel.xmjldw;
}


- (void)setBiochemistryListModel:(HZBiochemistryListModel *)biochemistryListModel {
    _biochemistryListModel = biochemistryListModel;
}
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"biochemistryListCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

+ (CGFloat)cellHeightWithBiochemistryCheckModel:(HZBiochemistryListModel *)biochemistryListModel {
    CGFloat height1 = [biochemistryListModel.jcxmzw getTextHeightWithMaxWidth:kP(300) textFontSize:kP(35)];
    //
    CGFloat height3 = [biochemistryListModel.xmjgfw getTextHeightWithMaxWidth:kP(300) textFontSize:kP(38)];
    CGFloat height4 = [biochemistryListModel.xmjldw getTextHeightWithMaxWidth:kP(300) textFontSize:kP(38)];
    CGFloat height5 = height3 + height4 + kP(10);
    
    //
    
    
    return MAX(height1, height5) + kP(10);
}

@end
