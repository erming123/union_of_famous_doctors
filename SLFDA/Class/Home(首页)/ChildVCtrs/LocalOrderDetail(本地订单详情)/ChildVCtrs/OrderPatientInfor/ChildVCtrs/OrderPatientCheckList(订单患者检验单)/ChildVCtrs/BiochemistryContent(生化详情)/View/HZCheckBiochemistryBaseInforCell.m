//
//  HZCheckBiochemistryBaseInforCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZCheckBiochemistryBaseInforCell.h"
#import "HZCheckModel.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZCheckBiochemistryBaseInforCell ()
@property (nonatomic, strong) UILabel *orderNameLabel;
@property (nonatomic, strong) UILabel *orderNameContentLabel;
@property (nonatomic, strong) UILabel *itemNameLabel;
@property (nonatomic, strong) UILabel *itemNameContentLabel;
@property (nonatomic, strong) UILabel *checkDoctorNameLabel;
@property (nonatomic, strong) UILabel *checkDoctorNameContentLabel;
@property (nonatomic, strong) UILabel *checkTimeLabel;
@property (nonatomic, strong) UILabel *checkTimeContentLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZCheckBiochemistryBaseInforCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    
    //1.
    UILabel *orderNameLabel = [UILabel new];
    orderNameLabel.text = @"检验单号";
    [orderNameLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:orderNameLabel];
    self.orderNameLabel = orderNameLabel;
    
    UILabel *orderNameContentLabel = [UILabel new];
    [orderNameContentLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    orderNameContentLabel.numberOfLines = 0;
    [self addSubview:orderNameContentLabel];
    self.orderNameContentLabel = orderNameContentLabel;
    
    //2.
    UILabel *itemNameLabel = [UILabel new];
    itemNameLabel.text = @"检验项目";
    [itemNameLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:itemNameLabel];
    self.itemNameLabel = itemNameLabel;
    
    UILabel *itemNameContentLabel = [UILabel new];
    [itemNameContentLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    itemNameContentLabel.numberOfLines = 0;
    [self addSubview:itemNameContentLabel];
    self.itemNameContentLabel = itemNameContentLabel;
    
    //3.
    UILabel *checkDoctorNameLabel = [UILabel new];
    checkDoctorNameLabel.text = @"审核医生";
    [checkDoctorNameLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:checkDoctorNameLabel];
    self.checkDoctorNameLabel = checkDoctorNameLabel;
    
    UILabel *checkDoctorNameContentLabel = [UILabel new];
    [checkDoctorNameContentLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    checkDoctorNameContentLabel.numberOfLines = 0;
    [self addSubview:checkDoctorNameContentLabel];
    self.checkDoctorNameContentLabel = checkDoctorNameContentLabel;
    
    
    //4.
    UILabel *checkTimeLabel = [UILabel new];
    checkTimeLabel.text = @"检验时间";
    [checkTimeLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:checkTimeLabel];
    self.checkTimeLabel = checkTimeLabel;
    
    UILabel *checkTimeContentLabel = [UILabel new];
    [checkTimeContentLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    checkTimeContentLabel.numberOfLines = 0;
    [self addSubview:checkTimeContentLabel];
    self.checkTimeContentLabel = checkTimeContentLabel;
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //
    [self setSubViewsContent];
    
    //1.
    CGSize orderNameSize = [self.orderNameLabel.text getTextSizeWithMaxWidth:kP(150) textFontSize:kP(35)];
    CGFloat orderNameLabelW = orderNameSize.width;
    CGFloat orderNameLabelH = orderNameSize.height;
    CGFloat orderNameLabelX = kP(28);
    CGFloat orderNameLabelY = kP(28);
    self.orderNameLabel.frame = CGRectMake(orderNameLabelX, orderNameLabelY, orderNameLabelW, orderNameLabelH);
    
    CGFloat orderNameContentLabelX = self.orderNameLabel.right + kP(20);
    CGFloat orderNameContentLabelY = orderNameLabelY;
    
    CGFloat maxWidth = self.width - orderNameContentLabelX - kP(28);//----------------------maxWidth
    
    CGSize orderNameContentSize = [self.orderNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(35)];
    CGFloat orderNameContentLabelW = orderNameContentSize.width;
    CGFloat orderNameContentLabelH = orderNameContentSize.height;
    self.orderNameContentLabel.frame = CGRectMake(orderNameContentLabelX, orderNameContentLabelY, orderNameContentLabelW, orderNameContentLabelH);
    
    //2.
    CGSize itemNameSize = [self.itemNameLabel.text getTextSizeWithMaxWidth:kP(150) textFontSize:kP(35)];
    CGFloat itemNameLabelW = itemNameSize.width;
    CGFloat itemNameLabelH = itemNameSize.height;
    CGFloat itemNameLabelX = orderNameLabelX;
    CGFloat itemNameLabelY = MAX(self.orderNameLabel.bottom, self.orderNameContentLabel.bottom) + kP(20);
    self.itemNameLabel.frame = CGRectMake(itemNameLabelX, itemNameLabelY, itemNameLabelW, itemNameLabelH);
    
    CGSize itemNameContentSize = [self.itemNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(35)];
    CGFloat itemNameContentLabelW = itemNameContentSize.width;
    CGFloat itemNameContentLabelH = itemNameContentSize.height;
    CGFloat itemNameContentLabelX = orderNameContentLabelX;
    CGFloat itemNameContentLabelY = itemNameLabelY;
    self.itemNameContentLabel.frame = CGRectMake(itemNameContentLabelX, itemNameContentLabelY, itemNameContentLabelW, itemNameContentLabelH);
    
    //3.
    CGSize checkDoctorNameSize = [self.checkDoctorNameLabel.text getTextSizeWithMaxWidth:kP(150) textFontSize:kP(35)];
    CGFloat checkDoctorNameLabelW = checkDoctorNameSize.width;
    CGFloat checkDoctorNameLabelH = checkDoctorNameSize.height;
    CGFloat checkDoctorNameLabelX = itemNameLabelX;
    CGFloat checkDoctorNameLabelY = MAX(self.itemNameLabel.bottom, self.itemNameContentLabel.bottom) + kP(20);
    self.checkDoctorNameLabel.frame = CGRectMake(checkDoctorNameLabelX, checkDoctorNameLabelY, checkDoctorNameLabelW, checkDoctorNameLabelH);
    
    CGSize checkDoctorNameContentSize = [self.checkDoctorNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(35)];
    CGFloat checkDoctorNameContentLabelW = checkDoctorNameContentSize.width;
    CGFloat checkDoctorNameContentLabelH = checkDoctorNameContentSize.height;
    CGFloat checkDoctorNameContentLabelX = itemNameContentLabelX;
    CGFloat checkDoctorNameContentLabelY = checkDoctorNameLabelY;
    self.checkDoctorNameContentLabel.frame = CGRectMake(checkDoctorNameContentLabelX, checkDoctorNameContentLabelY, checkDoctorNameContentLabelW, checkDoctorNameContentLabelH);
    
    
    //4.
    CGSize checkTimeSize = [self.checkTimeLabel.text getTextSizeWithMaxWidth:kP(150) textFontSize:kP(35)];
    CGFloat checkTimeLabelW = checkTimeSize.width;
    CGFloat checkTimeLabelH = checkTimeSize.height;
    CGFloat checkTimeLabelX = checkDoctorNameLabelX;
    CGFloat checkTimeLabelY = MAX(self.checkDoctorNameLabel.bottom, self.checkDoctorNameContentLabel.bottom) + kP(20);
    self.checkDoctorNameLabel.frame = CGRectMake(checkDoctorNameLabelX, checkDoctorNameLabelY, checkDoctorNameLabelW, checkDoctorNameLabelH);;
    self.checkTimeLabel.frame = CGRectMake(checkTimeLabelX, checkTimeLabelY, checkTimeLabelW, checkTimeLabelH);
    
    CGSize checkTimeContentSize = [self.checkTimeContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(35)];
    CGFloat checkTimeContentLabelW = checkTimeContentSize.width;
    CGFloat checkTimeContentLabelH = checkTimeContentSize.height;
    CGFloat checkTimeContentLabelX = checkDoctorNameContentLabelX;
    CGFloat checkTimeContentLabelY = checkTimeLabelY;
    self.checkTimeContentLabel.frame = CGRectMake(checkTimeContentLabelX, checkTimeContentLabelY, checkTimeContentLabelW, checkTimeContentLabelH);
    
    NSLog(@"---xxxx------%f", self.checkTimeContentLabel.bottom);
}


- (void)setSubViewsContent {
    
    //1.
    self.orderNameContentLabel.text = self.biochemistryCheckModel.jcybid;
    //2. 项目名
    NSString *itemNamePostStr = self.biochemistryCheckModel.jcmdmc;
    
    if (itemNamePostStr) {
        if (itemNamePostStr.length > 0) {
            if ([[itemNamePostStr substringFromIndex:itemNamePostStr.length - 1] isEqualToString:@","]) {
                itemNamePostStr = [itemNamePostStr stringByReplacingCharactersInRange:NSMakeRange(itemNamePostStr.length - 1, 1) withString:@""];
            }
        }
    }
    
    self.itemNameContentLabel.text = itemNamePostStr;
    
    //3.
    self.checkDoctorNameContentLabel.text = self.biochemistryCheckModel.kdysxm;
    //4. 时间
    NSString *timePostStr = self.biochemistryCheckModel.ybjgsj;
    
    NSString *timeStr;
    if (timePostStr) {
        if (timePostStr.length >= 11) {
            timeStr = [timePostStr stringByReplacingCharactersInRange:NSMakeRange(10, timePostStr.length - 10) withString:@""];
        }
        
    }
    self.checkTimeContentLabel.text = timeStr;
}

- (void)setBiochemistryCheckModel:(HZCheckModel *)biochemistryCheckModel {
    _biochemistryCheckModel = biochemistryCheckModel;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"biochemistryCheckCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}


+ (CGFloat)cellHeightWithBiochemistryCheckModel:(HZCheckModel *)biochemistryCheckModel {
    CGFloat height1 = [biochemistryCheckModel.jcybid getTextHeightWithMaxWidth:HZScreenW - 2 * kP(28) textFontSize:kP(32)];
    CGFloat height2 = [biochemistryCheckModel.jcmdmc getTextHeightWithMaxWidth:HZScreenW - 2 * kP(28) textFontSize:kP(32)];
    CGFloat height3 = [biochemistryCheckModel.kdysxm getTextHeightWithMaxWidth:HZScreenW - 2 * kP(28) textFontSize:kP(32)];
    CGFloat height4 = [biochemistryCheckModel.ybjgsj getTextHeightWithMaxWidth:HZScreenW - 2 * kP(28) textFontSize:kP(32)];
    return height1 +height2 + height3 + height4 + kP(150);
}
@end

