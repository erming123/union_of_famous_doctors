//
//  HZCheckListViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HZOrder;
@interface HZCheckListViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *checkListOrder;
@end
