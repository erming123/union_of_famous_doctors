//
//  HZImageCheckContentCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZImageCheckContentCell.h"
#import "HZCheckModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZImageCheckContentCell ()
@property (nonatomic, strong) UILabel *reportResultLabel;
@property (nonatomic, strong) UILabel *reportResultContentLabel;
@property (nonatomic, strong) UILabel *diagnoseResultLabel;
@property (nonatomic, strong) UILabel *diagnoseResultContentLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZImageCheckContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UILabel *reportResultLabel = [UILabel new];
//    reportResultLabel.backgroundColor = [UIColor redColor];
    reportResultLabel.text = @"报告结果";
    [reportResultLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:reportResultLabel];
    self.reportResultLabel = reportResultLabel;
    
    UILabel *reportResultContentLabel = [UILabel new];
//    reportResultContentLabel.backgroundColor = [UIColor redColor];
    
    [reportResultContentLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    reportResultContentLabel.numberOfLines = 0;
    [self addSubview:reportResultContentLabel];
    self.reportResultContentLabel = reportResultContentLabel;

    
    UILabel *diagnoseResultLabel = [UILabel new];
//    diagnoseResultLabel.backgroundColor = [UIColor redColor];
    diagnoseResultLabel.text = @"诊断结果";
    [diagnoseResultLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self addSubview:diagnoseResultLabel];
    self.diagnoseResultLabel = diagnoseResultLabel;
    
    UILabel *diagnoseResultContentLabel = [UILabel new];
//    diagnoseResultContentLabel.backgroundColor = [UIColor redColor];
    [diagnoseResultContentLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    diagnoseResultContentLabel.numberOfLines = 0;
    [self addSubview:diagnoseResultContentLabel];
    self.diagnoseResultContentLabel = diagnoseResultContentLabel;


}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    [self setUpSubViewsContent];
    
    //1.
    CGSize reportResultSize = [self.reportResultLabel.text getTextSizeWithMaxWidth:kP(150) textFontSize:kP(33)];
    CGFloat reportResultLabelW = kP(150);
    CGFloat reportResultLabelH = reportResultSize.height;
    CGFloat reportResultLabelX = kP(28);
    CGFloat reportResultLabelY = kP(28);
    self.reportResultLabel.frame = CGRectMake(reportResultLabelX, reportResultLabelY, reportResultLabelW, reportResultLabelH);
    
    //
    CGFloat maxWidth = self.width - 2 * reportResultLabelX;
    //

    
    CGSize  reportResultContentSize = [self.reportResultContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(33)];
    CGFloat reportResultContentLabelW = maxWidth;
    CGFloat reportResultContentLabelH = reportResultContentSize.height;
    CGFloat reportResultContentLabelX = reportResultLabelX;
    CGFloat reportResultContentLabelY = self.reportResultLabel.bottom + kP(16);
    self.reportResultContentLabel.frame = CGRectMake(reportResultContentLabelX, reportResultContentLabelY, reportResultContentLabelW, reportResultContentLabelH);
    
    //2.
    CGSize diagnoseResultSize = [self.diagnoseResultLabel.text getTextSizeWithMaxWidth:kP(150) textFontSize:kP(33)];
    CGFloat diagnoseResultLabelW = reportResultLabelW;
    CGFloat diagnoseResultLabelH = diagnoseResultSize.height;
    CGFloat diagnoseResultLabelX = reportResultLabelX;
    CGFloat diagnoseResultLabelY = MAX(self.reportResultLabel.bottom, self.reportResultContentLabel.bottom) + kP(16);
    self.diagnoseResultLabel.frame = CGRectMake(diagnoseResultLabelX, diagnoseResultLabelY, diagnoseResultLabelW, diagnoseResultLabelH);
    
    CGSize diagnoseResultContentSize = [self.diagnoseResultContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(33)];
    CGFloat diagnoseResultContentLabelW = maxWidth;
    CGFloat diagnoseResultContentLabelH = diagnoseResultContentSize.height;
    CGFloat diagnoseResultContentLabelX = diagnoseResultLabelX;
    CGFloat diagnoseResultContentLabelY = self.diagnoseResultLabel.bottom + kP(16);;
    self.diagnoseResultContentLabel.frame = CGRectMake(diagnoseResultContentLabelX, diagnoseResultContentLabelY, diagnoseResultContentLabelW, diagnoseResultContentLabelH);
    
    //3.----------------------------------
    
}


- (void)setUpSubViewsContent {
    self.reportResultContentLabel.text = self.imageCheckModel.yxsjnr;
    self.diagnoseResultContentLabel.text = self.imageCheckModel.yxzdnr;
}


- (void)setImageCheckModel:(HZCheckModel *)imageCheckModel {
    _imageCheckModel = imageCheckModel;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"cell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

+ (CGFloat)cellHeightWithImageCheckModel:(HZCheckModel *)imageCheckModel {
   CGFloat height1 = [imageCheckModel.yxsjnr getTextHeightWithMaxWidth:HZScreenW - 2 * kP(28) textFontSize:kP(33)];
   CGFloat height2 = [imageCheckModel.yxzdnr getTextHeightWithMaxWidth:HZScreenW - 2 * kP(28) textFontSize:kP(33)];
    
    return height1 +height2 + kP(200);
}
@end
