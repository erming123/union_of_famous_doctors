//
//  HZSectionHeadView.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/10.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZSectionHeadView.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZSectionHeadView ()
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *midLabell;
@property (nonatomic, strong) UILabel *rightLabel;
@property (nonatomic, weak) UIView *vBottomLine;
@end
NS_ASSUME_NONNULL_END
@implementation HZSectionHeadView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setUpSubViews {
    UILabel *leftLabel = [UILabel new];
    [leftLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    leftLabel.textAlignment = NSTextAlignmentLeft;
//    leftLabel.backgroundColor = [UIColor redColor];
    [self addSubview:leftLabel];
    self.leftLabel = leftLabel;
    
    UILabel *midLabell = [UILabel new];
    [midLabell setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    midLabell.textAlignment = NSTextAlignmentCenter;
//    midLabell.backgroundColor = [UIColor redColor];
    [self addSubview:midLabell];
    self.midLabell = midLabell;
    
    
    UILabel *rightLabel = [UILabel new];
    [rightLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    rightLabel.textAlignment = NSTextAlignmentRight;
//    rightLabel.backgroundColor = [UIColor redColor];
    [self addSubview:rightLabel];
    self.rightLabel = rightLabel;
    
    UIView *vBottomLine = [UIView new];
    vBottomLine.backgroundColor = [UIColor colorWithHexString:@"dfdfdf" alpha:1.0];
    [self addSubview:vBottomLine];
    self.vBottomLine = vBottomLine;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    //
    [self setSubViewsContent];
    
    
    //
    CGFloat leftLabelW = (self.width - kP(28) * 4) / 3.;
    CGFloat leftLabelH = [self.leftLabel.text getTextHeightWithMaxWidth:leftLabelW textFontSize:kP(35)];
    CGFloat leftLabelX = kP(28);
    CGFloat leftLabelY = self.height * 0.5 - leftLabelH * 0.5;
    self.leftLabel.frame = CGRectMake(leftLabelX, leftLabelY, leftLabelW, leftLabelH);
    
    
    CGFloat midLabellW = leftLabelW;
    CGFloat midLabellH = [self.midLabell.text getTextHeightWithMaxWidth:midLabellW textFontSize:kP(35)];
    CGFloat midLabellX = self.width * 0.5 - midLabellW * 0.5;
    CGFloat midLabellY = leftLabelY;
    self.midLabell.frame = CGRectMake(midLabellX, midLabellY, midLabellW, midLabellH);
    
    
    CGFloat rightLabelW = midLabellW;
    CGFloat rightLabelH = [self.rightLabel.text getTextHeightWithMaxWidth:midLabellW textFontSize:kP(35)];
    CGFloat rightLabelX = self.width - rightLabelW - kP(28);
    CGFloat rightLabelY = midLabellY;
    self.rightLabel.frame = CGRectMake(rightLabelX, rightLabelY, rightLabelW, rightLabelH);
    
    self.vBottomLine.x = 0;
    self.vBottomLine.width = HZScreenW;
    self.vBottomLine.height = 0.5;
    self.vBottomLine.y = self.height - self.vBottomLine.height;
}

- (void)setSubViewsContent {
    self.leftLabel.text = self.leftStr;
    self.midLabell.text = self.midStr;
    self.rightLabel.text = self.rightStr;
}

- (void)setLeftStr:(NSString *)leftStr {
    _leftStr = leftStr;
}

- (void)setMidStr:(NSString *)midStr {
    _midStr = midStr;
}

- (void)setRightStr:(NSString *)rightStr {
    _rightStr = rightStr;
}
@end
