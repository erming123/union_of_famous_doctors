//
//  HZCheckModel.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/12/2.
//  Copyright © 2016年 huazhuo. All rights reserved.
//


#import "HZJsonModel.h"
@interface HZCheckModel : HZJsonModel

// 影像
@property (nonatomic, copy) NSString *jcxmmc;// 检验项目
@property (nonatomic, copy) NSString *shryxm;// 审核医生姓名， 修改后的
@property (nonatomic, copy) NSString *jcdjsj;// 检验时间
@property (nonatomic, copy) NSString *sjbrxm;// 送检病人姓名
@property (nonatomic, copy) NSString *yxzdnr;// 诊断结果
@property (nonatomic, copy) NSString *yxsjnr;// 报告结果


// 生化
@property (nonatomic, copy) NSString *jcmdmc;// 检验项目
@property (nonatomic, copy) NSString *kdysxm;// 医生姓名
@property (nonatomic, copy) NSString *ybjgsj;// 检验时间
//
@property (nonatomic, copy) NSString *yblxmc;// 其他
@property (nonatomic, copy) NSString *kdksmc;// 门诊
//
@property (nonatomic, copy) NSString *flag;
@property (nonatomic, copy) NSString *jcybid;// 检验单号

//
@property (nonatomic, assign) BOOL isExample;
@end
