//
//  HZImageDetailContentViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/2/15.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZImageDetailContentViewController.h"
#import "HZCheckImageBaseInforCell.h"
#import "HZImageCheckContentCell.h"
#import "HZCheckModel.h"
//
#import "HZWebImageContentViewController.h"
//
#import "SSZipArchive.h"
#import "AFNetworking.h"
//
#import "HZImageDataTool.h"

//
@interface HZImageDetailContentViewController ()<UITableViewDataSource, UITableViewDelegate, SSZipArchiveDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *imageTableView;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *imageBtn;

//** 下载任务*/
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
//** 大小数组*/
@property (nonatomic, copy) NSMutableArray *mutableBigArray;
@property (nonatomic, copy) NSMutableArray *mutableSmallArray;
@property (nonatomic, assign) BOOL isSampleImage;
@end

@implementation HZImageDetailContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.title = @"影像检验详情";
    
    self.view.backgroundColor = [UIColor whiteColor];
    //    self.imageCheckModel.jcybid = @"CTMZ6616477";
    self.isSampleImage = [self.imageCheckModel.jcybid isEqualToString:@"648860CT"];
    
    HZTableView *imageTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    imageTableView.dataSource = self;
    imageTableView.delegate = self;
    imageTableView.showsVerticalScrollIndicator = NO;
    imageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:imageTableView];
    self.imageTableView = imageTableView;
    
    //
    [self setMyImageLoadBtn];

}


#pragma mark -- getImageBaseInfor
- (void)getImageBaseInfor {
    
    //    self.imageCheckModel.jcybid = @"648860CT";
    
    NSLog(@"---------------------wer-%@", self.imageCheckModel.jcybid);
   
}

#pragma mark -- setMyImageLoadBtn

- (void)setMyImageLoadBtn {
    
    self.imageTableView.height -= kP(130);
    //
    CGFloat imageBtnBgViewX = kP(0);
    CGFloat imageBtnBgViewH = kP(130);
    CGFloat imageBtnBgViewY = HZScreenH - imageBtnBgViewH - SafeAreaBottomHeight;
    CGFloat imageBtnBgViewW = HZScreenW;
    
    UIView *imageBtnBgView = [[UIView alloc] initWithFrame:CGRectMake(imageBtnBgViewX, imageBtnBgViewY, imageBtnBgViewW, imageBtnBgViewH)];
    //
    imageBtnBgView.backgroundColor = [UIColor whiteColor];
    imageBtnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    imageBtnBgView.layer.shadowOffset = CGSizeMake(kP(0), - kP(2));
    imageBtnBgView.layer.shadowOpacity = 0.1;
    //
    imageBtnBgView.layer.shadowRadius = kP(2);
    [self.view addSubview:imageBtnBgView];
   
    //
    CGFloat imageBtnX = kP(32);
    CGFloat imageBtnY = kP(15);
    CGFloat imageBtnW = HZScreenW - 2 * imageBtnX;
    CGFloat imageBtnH = imageBtnBgViewH - 2 * imageBtnY;
    
    UIButton *imageBtn = [[UIButton alloc] initWithFrame:CGRectMake(imageBtnX, imageBtnY, imageBtnW, imageBtnH)];
    imageBtn.backgroundColor = kGreenColor;
    
    [imageBtn setTitle:@"查看影像" forState:UIControlStateNormal];
    
    
    [imageBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    imageBtn.layer.cornerRadius = kP(10);
    imageBtn.clipsToBounds = YES;
    [imageBtn addTarget:self action:@selector(imageBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [imageBtnBgView addSubview:imageBtn];
    self.imageBtn = imageBtn;
    
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        HZCheckImageBaseInforCell *checkImageBaseInforCell = [HZCheckImageBaseInforCell cellWithTableView:tableView];
        checkImageBaseInforCell.imageCheckModel = self.imageCheckModel;
        return checkImageBaseInforCell;
    } else {
        HZImageCheckContentCell *imageCheckContentCell = [HZImageCheckContentCell cellWithTableView:tableView];
        imageCheckContentCell.imageCheckModel = self.imageCheckModel;
        return imageCheckContentCell;
        
    }
}



-(void)setImageCheckModel:(HZCheckModel *)imageCheckModel {
    _imageCheckModel = imageCheckModel;
}

- (void)setBookingOrderId:(NSString *)bookingOrderId {
    _bookingOrderId = bookingOrderId;
}

- (void)setPatientHospitalId:(NSString *)patientHospitalId {
    _patientHospitalId = patientHospitalId;
}
#pragma mark -- delgate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return [HZCheckImageBaseInforCell cellHeightWithImageCheckModel:self.imageCheckModel];
    }
    
    return [HZImageCheckContentCell cellHeightWithImageCheckModel:self.imageCheckModel];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kP(0.1);
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kP(1);
}



#pragma mark -- imageBtnClick
- (void)imageBtnClick:(UIButton *)imageBtn {

    if (self.isSampleImage) {
        HZWebImageContentViewController *webImageContentVCtr = [HZWebImageContentViewController new];
        webImageContentVCtr.loadUrlStr = [NSString stringWithFormat:@"https://viewer-wpacs.rubikstack.com/series.html?accessionNumber=%@", @"CTMZ6616477"];
        [self.navigationController pushViewController:webImageContentVCtr animated:YES];
        return;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

