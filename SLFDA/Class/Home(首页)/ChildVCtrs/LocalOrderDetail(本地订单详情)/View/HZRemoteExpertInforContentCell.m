//
//  HZRemoteExpertInforContentCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZRemoteExpertInforContentCell.h"
#import "HZOrder.h"
#import "HZUserTool.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZRemoteExpertInforContentCell ()
@property (nonatomic, strong) UIImageView *doctorInforImageView;
@property (nonatomic, strong) UILabel *doctorInforLabel;
//
@property (nonatomic, strong) UIImageView *doctorAvatarImageView;
@property (nonatomic, strong) UILabel *doctorNameLabel;
@property (nonatomic, strong) UILabel *doctorJobLabel;
@property (nonatomic, strong) UILabel *doctorSubjectLabel;
@property (nonatomic, strong) UIButton *IMBtn;
@property (nonatomic, strong) UILabel *newsCountLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZRemoteExpertInforContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
//        self.backgroundColor = [UIColor blueColor];
    }
    
    return self;
}

- (void)setUpSubViews {
    
    
    //
    
    
    //1.
    UIImageView *doctorInforImageView = [[UIImageView alloc] init];
    doctorInforImageView.contentMode = UIViewContentModeScaleAspectFill;
    doctorInforImageView.image = [UIImage imageNamed:@"localDoctor"];
    [self addSubview:doctorInforImageView];
    self.doctorInforImageView = doctorInforImageView;
    
    //2.
    UILabel *doctorInforLabel = [[UILabel alloc] init];
    doctorInforLabel.text = @"专家信息";
    [doctorInforLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
//    doctorInforLabel.backgroundColor = [UIColor redColor];
    [doctorInforLabel sizeToFit];
    [self addSubview:doctorInforLabel];
    self.doctorInforLabel = doctorInforLabel;
    
    //
    
    UIImageView *doctorAvatarImageView = [[UIImageView alloc] init];
    doctorAvatarImageView.backgroundColor = [UIColor greenColor];
    doctorAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
    doctorAvatarImageView.layer.borderWidth = kP(2);
    doctorAvatarImageView.layer.borderColor = [UIColor colorWithRed:223/255.0 green:223/255.0 blue:223/255.0 alpha:1.0].CGColor;
    [self addSubview:doctorAvatarImageView];
    self.doctorAvatarImageView = doctorAvatarImageView;
    
    UILabel *doctorNameLabel = [[UILabel alloc] init];
    doctorNameLabel.font = [UIFont systemFontOfSize:kP(32)];
    doctorNameLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
    [self addSubview:doctorNameLabel];
    self.doctorNameLabel = doctorNameLabel;
    
    UILabel *doctorJobLabel = [[UILabel alloc] init];
    [doctorJobLabel setTextFont:kP(28) textColor:@"#666666" alpha:1.0];
    [self addSubview:doctorJobLabel];
    self.doctorJobLabel = doctorJobLabel;
    
    UILabel *doctorSubjectLabel = [[UILabel alloc] init];
    [doctorSubjectLabel setTextFont:kP(28) textColor:@"#666666" alpha:1.0];
    [self addSubview:doctorSubjectLabel];
    self.doctorSubjectLabel = doctorSubjectLabel;
    
    UIButton *IMBtn = [[UIButton alloc] init];
    [IMBtn setTitleColor:[UIColor colorWithHexString:@"2dbed8" alpha:1.0] forState:UIControlStateNormal];
    IMBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [IMBtn setTitle:@"图文交流" forState:UIControlStateNormal];
    [IMBtn setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [IMBtn sizeToFit];
    [IMBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:kP(20)];
    [IMBtn addTarget:self action:@selector(makeDelegateEnterIMVCtr:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:IMBtn];
    self.IMBtn = IMBtn;
    
    //
    //  newsLabel
    UILabel *newsCountLabel = [[UILabel alloc] init];
    newsCountLabel.backgroundColor = [UIColor redColor];
    newsCountLabel.textColor = [UIColor whiteColor];
    newsCountLabel.font = [UIFont systemFontOfSize:kP(26)];
    newsCountLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:newsCountLabel];
    self.newsCountLabel = newsCountLabel;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self setUpSubViewsContent];
    
    
    //
    // 图标
    CGFloat doctorInforImageViewWH = kP(40);
    CGFloat doctorInforImageViewXY = kP(32);
    self.doctorInforImageView.frame = CGRectMake(doctorInforImageViewXY, doctorInforImageViewXY, doctorInforImageViewWH, doctorInforImageViewWH);
    
    
    // 文本：专家信息
    self.doctorInforLabel.x = self.doctorInforImageView.right + kP(20);
    self.doctorInforLabel.y = doctorInforImageViewXY;
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    //
    [self.doctorNameLabel sizeToFit];
    [self.doctorJobLabel sizeToFit];
    [self.doctorSubjectLabel sizeToFit];
    [self.newsCountLabel sizeToFit];
    
    // 头像
//    CGFloat doctorAvatarImageViewWH = kP(130);
//    CGFloat doctorAvatarImageViewX = self.doctorInforLabel.x;
//    CGFloat doctorAvatarImageViewY = self.height * 0.5 - doctorAvatarImageViewWH * 0.5;
    CGFloat doctorAvatarImageViewWH = kP(128);
    CGFloat doctorAvatarImageViewX = self.doctorInforLabel.x;
    CGFloat doctorAvatarImageViewY = self.doctorInforLabel.bottom + kP(28);
    self.doctorAvatarImageView.frame = CGRectMake(doctorAvatarImageViewX, doctorAvatarImageViewY, doctorAvatarImageViewWH, doctorAvatarImageViewWH);
    self.doctorAvatarImageView.layer.borderWidth = kP(1);
    self.doctorAvatarImageView.layer.borderColor = [UIColor colorWithHexString:@"#D5D5D5"].CGColor;
    self.doctorAvatarImageView.layer.cornerRadius = kP(65);
    self.doctorAvatarImageView.clipsToBounds = YES;
    
//    // 姓名
//    self.doctorNameLabel.x = CGRectGetMaxX(self.doctorAvatarImageView.frame) + kP(30);
//    self.doctorNameLabel.y = kP(38);
//    
//    // 工作
//    self.doctorJobLabel.x = CGRectGetMaxX(self.doctorNameLabel.frame) + kP(35);
//    self.doctorJobLabel.y = self.doctorNameLabel.y;
//    
//    // 科室
//    self.doctorSubjectLabel.x = self.doctorNameLabel.x;
//    self.doctorSubjectLabel.y = self.height - self.doctorSubjectLabel.height - self.doctorNameLabel.y;
    
    // 姓名
    self.doctorNameLabel.x = self.doctorAvatarImageView.right + kP(40);
    self.doctorNameLabel.y = self.doctorAvatarImageView.centerY - self.doctorNameLabel.height - kP(5);
    
    // 工作
    self.doctorJobLabel.x = self.doctorNameLabel.width == 0 ?  self.doctorAvatarImageView.right + kP(40) : self.doctorNameLabel.right + kP(20);
    self.doctorJobLabel.y = self.doctorNameLabel.width == 0 ? self.doctorAvatarImageView.centerY - self.doctorJobLabel.height - kP(5) : self.doctorNameLabel.centerY - self.doctorJobLabel.height * 0.5;
    // 科室
    self.doctorSubjectLabel.x = self.doctorAvatarImageView.right + kP(40);
    self.doctorSubjectLabel.y = self.doctorAvatarImageView.centerY + kP(15);
    
    //
    self.IMBtn.x = self.width - self.IMBtn.width - kP(32);
    self.IMBtn.y = self.doctorInforLabel.centerY - self.IMBtn.height * 0.5;
    
    //    CGFloat IMBtnW = kP(300);
    //    CGFloat IMBtnH = kP(100);
    //    CGFloat IMBtnX = self.width - self.IMBtn.width;
    //    CGFloat IMBtnY = self.doctorNameLabel.y;
    //    self.IMBtn.frame = CGRectMake(IMBtnX, IMBtnY, self.IMBtn.width, IMBtnH);
    
//    //
//    self.IMBtn.x = self.width - self.IMBtn.width - kP(15);
//    self.IMBtn.y = self.height * 0.5 - self.IMBtn.height * 0.5;

    
    //
    //  newsLabel
//    CGSize newsCountSize = [self.newsCountLabel.text getTextSizeWithMaxWidth:kP(400) textFontSize:kP(32)];
//    CGFloat WH = MAX(newsCountSize.width, newsCountSize.height);
//    self.newsCountLabel.width = self.newsCountLabel.height = WH;
//    self.newsCountLabel.layer.cornerRadius = WH * 0.5;
//    self.newsCountLabel.layer.masksToBounds = YES;
//    self.newsCountLabel.x = CGRectGetMaxX(self.IMBtn.frame) - self.newsCountLabel.width * 0.8;
//    self.newsCountLabel.y = self.IMBtn.y - self.newsCountLabel.height * 0.3;
    
    //  newsLabel
    CGSize newsCountSize = [self.newsCountLabel.text getTextSizeWithMaxWidth:kP(400) textFontSize:kP(32)];
    CGFloat WH = MAX(newsCountSize.width, newsCountSize.height);
    self.newsCountLabel.width = self.newsCountLabel.height = WH;
    self.newsCountLabel.layer.cornerRadius = WH * 0.5;
    self.newsCountLabel.layer.masksToBounds = YES;
    self.newsCountLabel.x = self.IMBtn.right - self.newsCountLabel.width * 0.8;
    self.newsCountLabel.y = self.IMBtn.y - self.newsCountLabel.height * 0.3;
    
    
    if (!self.newsCountLabel.hidden) {
        // 重新布局
        _IMBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -kP(120), 0, 0);
        self.newsCountLabel.centerY = _IMBtn.centerY;
        self.newsCountLabel.x = self.width - self.newsCountLabel.width - kP(50);
        
    }

}


- (void)setUpSubViewsContent {
    
    NSData *imageData = [[NSUserDefaults standardUserDefaults] objectForKey:self.myLocalOrder.expertOpenid];
    if (imageData) {
        UIImage *image = [UIImage imageWithData:imageData];
        self.doctorAvatarImageView.image = image;
    } else {
        
        self.doctorAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
        //
        [HZUserTool getAvatarWithAvatarOpenId:self.myLocalOrder.expertOpenid success:^(UIImage *image) {
            if (image) {
                self.doctorAvatarImageView.image = image;
                NSData *imageData = UIImagePNGRepresentation(image);//把image归档为NSData
                [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:self.myLocalOrder.expertOpenid];
            } else {
                self.doctorAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
            }

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"---------%@", error);
        }];
        
        
    }

    
    self.doctorNameLabel.text = self.myLocalOrder.expertName;
    self.doctorJobLabel.text = self.myLocalOrder.expertTitleValue;
    self.doctorSubjectLabel.text = self.myLocalOrder.experDeptValue;
    
    //
    self.IMBtn.hidden = [self.myLocalOrder.bookingStateCode isEqualToString:@"008"];
    //
    self.newsCountLabel.hidden = [self.myLocalOrder.bookingStateCode isEqualToString:@"008"] || self.unReadMsgCount == 0;
    self.newsCountLabel.text = [NSString stringWithFormat:@"%d", self.unReadMsgCount];
    NSLog(@"---adsasasasaasassasa******------%d", self.unReadMsgCount);
}

-(void)setUnReadMsgCount:(int)unReadMsgCount {
    _unReadMsgCount = unReadMsgCount;
}

//- (void)setLocalOrder:(HZOrder *)localOrder {
//    _localOrder = localOrder;
//    
//    NSLog(@"-------sdf----------:%@", localOrder);
//}

- (void)setMyLocalOrder:(HZOrder *)myLocalOrder {
    _myLocalOrder = myLocalOrder;
    NSLog(@"-------sdf----------:%@", myLocalOrder);
}
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"remoteExpertInforContentCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

#pragma mark -- <#class#>

- (void)makeDelegateEnterIMVCtr:(UIButton *)IMBtn {
    self.newsCountLabel.hidden = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterIMVCtr)]) {
        [self.delegate enterIMVCtr];
    }
}

#pragma mark -- <#class#>

- (void)setIsIMBtnHidden:(BOOL)isIMBtnHidden {
    _IMBtn.hidden = isIMBtnHidden;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
