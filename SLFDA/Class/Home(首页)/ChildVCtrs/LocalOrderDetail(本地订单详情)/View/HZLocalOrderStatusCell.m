//
//  HZLocalOrderStatusCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZLocalOrderStatusCell.h"
#import "HZOrder.h"
#import "HZOrderHttpsTool.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZLocalOrderStatusCell ()
@property (nonatomic, strong) UIImageView *orderStatusImageView;
@property (nonatomic, strong) UILabel *orderStatusLabel;
@property (nonatomic, strong) UILabel *orderStatusContentLabel;
@property (nonatomic, strong) UIView *VSepLine;
@property (nonatomic, strong) UIButton *enterToPayBtn;
@end
NS_ASSUME_NONNULL_END
@implementation HZLocalOrderStatusCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    
    
    UIImageView *orderStatusImageView = [UIImageView new];
    [self addSubview:orderStatusImageView];
    self.orderStatusImageView = orderStatusImageView;
    
    //
    UILabel *orderStatusLabel = [[UILabel alloc] init];
    [orderStatusLabel setTextFont:kP(34) textColor:@"#2DBED8" alpha:1.0];
    [self addSubview:orderStatusLabel];
    self.orderStatusLabel = orderStatusLabel;
    
    UILabel *orderStatusContentLabel = [[UILabel alloc] init];
    [orderStatusContentLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    orderStatusContentLabel.numberOfLines = 0;
//        orderStatusContentLabel.backgroundColor = [UIColor redColor];
    [self addSubview:orderStatusContentLabel];
    self.orderStatusContentLabel = orderStatusContentLabel;
    
    //
    //
    UIView *VSepLine = [UIView new];
    VSepLine.backgroundColor = [UIColor colorWithHexString:@"#e6e6e6" alpha:1.0];
    [self addSubview:VSepLine];
    self.VSepLine = VSepLine;
    
    //
    UIButton *enterToPayBtn = [UIButton new];
    [enterToPayBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    enterToPayBtn.titleLabel.font = [UIFont systemFontOfSize:kP(28)];
    [enterToPayBtn addTarget:self action:@selector(makeDelegateEnterToPay:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:enterToPayBtn];
    self.enterToPayBtn = enterToPayBtn;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    //
    [self setSubViewsContent];
    
    //
    //
    CGFloat orderStatusImageViewWH = kP(40);
    CGFloat orderStatusImageViewXY = kP(32);
    self.orderStatusImageView.frame = CGRectMake(orderStatusImageViewXY, orderStatusImageViewXY, orderStatusImageViewWH, orderStatusImageViewWH);
    //
    //
    [self.orderStatusLabel sizeToFit];
    self.orderStatusLabel.x = self.orderStatusImageView.right + kP(20);
//    self.orderStatusLabel.width = HZScreenW - self.orderStatusLabel.x;
    self.orderStatusLabel.y = self.orderStatusImageView.centerY - self.orderStatusLabel.height * 0.5;
    
    //
    if ([self.localOrder.bookingStateCode isEqualToString:@"001"]) {
        self.enterToPayBtn.width = kP(180);
        //    self.enterToPayBtn.width = kP(180);
        self.enterToPayBtn.height = self.height * 0.75;
        //    self.enterToPayBtn.backgroundColor = [UIColor redColor];
        self.enterToPayBtn.x = self.width - self.enterToPayBtn.width - kP(32);
        self.enterToPayBtn.y = self.height * 0.5 - self.enterToPayBtn.height * 0.5;
        [self.enterToPayBtn setTitle:@"打开支付页面" forState:UIControlStateNormal];
    } else {
        [self.enterToPayBtn setTitle:@"" forState:UIControlStateNormal];
        self.enterToPayBtn.frame = CGRectZero;
    }
    
    
    NSLog(@"-----nmb--------%@", self.localOrder.bookingStateCode);
    //
    self.VSepLine.x = self.enterToPayBtn.x - kP(32);
    self.VSepLine.y = kP(45);
    self.VSepLine.width = [self.localOrder.bookingStateCode isEqualToString:@"001"] ? kP(2) : kP(0);
    //    self.VSepLine.width = kP(2);
    self.VSepLine.height = self.height - 2 * self.VSepLine.y;
    
    //
    self.orderStatusContentLabel.x = self.orderStatusLabel.x;
    self.orderStatusContentLabel.y = self.orderStatusLabel.bottom + kP(20);
    self.orderStatusContentLabel.width = self.width - self.orderStatusContentLabel.x - self.enterToPayBtn.width - self.VSepLine.width - 3 * kP(32);
    self.orderStatusContentLabel.height = [self.orderStatusContentLabel.text getTextHeightWithMaxWidth:self.orderStatusContentLabel.width textFontSize:kP(32)];
}



- (void)setSubViewsContent {
    self.orderStatusImageView.image = [UIImage imageNamed:@"statusAlert"];
    self.orderStatusLabel.text = self.localOrder.bookingStateValue;
    
    self.orderStatusContentLabel.text = [HZOrderHttpsTool getBookingOrderStateAlertStringWithVersion:@"1" order:self.localOrder];
    
    NSLog(@"-------789-------%@--------------%@",self.localOrder, [HZOrderHttpsTool getBookingOrderStateAlertStringWithVersion:@"1" order:self.localOrder]);
    
    
    //    [self.orderStatusLabel sizeToFit];
    //    [self.orderStatusContentLabel sizeToFit];
}

- (void)setLocalOrder:(HZOrder *)localOrder {
    _localOrder = localOrder;
}
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"localOrderStatusCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

#pragma mark -- <#class#>
+ (CGFloat)cellHeightWithLocalOrder:(HZOrder *)localOrder {
    CGFloat maxWidth = HZScreenW - kP(180) - 4 * kP(32) - kP(60);
    CGFloat height = [[HZOrderHttpsTool getBookingOrderStateAlertStringWithVersion:@"1" order:localOrder] getTextHeightWithMaxWidth:maxWidth textFontSize:kP(32)];
    return [localOrder.bookingStateCode isEqualToString:@"001"] ? height + 3 * kP(32) + kP(20) + kP(30): height + 2 * kP(32) + kP(20) + kP(10);
    //    return height + 3 * kP(32) + kP(20);
}

- (void)makeDelegateEnterToPay:(UIButton *)enterToPayBtn {
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterToPay)]) {
        [self.delegate enterToPay];
    }
}
@end
