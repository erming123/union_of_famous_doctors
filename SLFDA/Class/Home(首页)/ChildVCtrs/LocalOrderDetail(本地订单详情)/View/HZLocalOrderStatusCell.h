//
//  HZLocalOrderStatusCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HZOrder;
@protocol HZLocalOrderStatusCellDelegate <NSObject>
- (void)enterToPay;
@end
@interface HZLocalOrderStatusCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *localOrder;
+ (CGFloat)cellHeightWithLocalOrder:(HZOrder *)localOrder;
@property (nonatomic, weak) id<HZLocalOrderStatusCellDelegate>delegate;
@end
