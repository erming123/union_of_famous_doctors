//
//  HZLocalPatientInforContentCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZOrderPatientInforContentCell.h"
#import "HZOrder.h"
#import "SDPhotoBrowser.h"
#import <objc/message.h>
#import "HZUserTool.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZOrderPatientInforContentCell ()<SDPhotoBrowserDelegate>
@property (nonatomic, weak) UILabel *patientNameLabel;
@property (nonatomic, weak) UILabel *patientNameContentLabel;
@property (nonatomic, weak) UILabel *illNameLabel;
@property (nonatomic, weak) UILabel *illNameContentLabel;
@property (nonatomic, weak) UILabel *illDetailLabel;
@property (nonatomic, weak) UILabel *illDetailContentLabel;
@property (nonatomic,strong) NSMutableDictionary *PicViewDict;
@property (nonatomic,assign) NSInteger type;
@end
NS_ASSUME_NONNULL_END
@implementation HZOrderPatientInforContentCell

-(NSMutableDictionary *)PicViewDict{
    
    if (!_PicViewDict) {
        
        _PicViewDict = [NSMutableDictionary dictionary];
    }
    return _PicViewDict;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
        self.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
        //        self.backgroundColor = [UIColor yellowColor];
    }
    
    return self;
}

- (void)setUpSubViews {
    //
    UILabel *patientNameLabel = [UILabel new];
    [patientNameLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self.contentView addSubview:patientNameLabel];
    self.patientNameLabel = patientNameLabel;
    
    
    UILabel *patientNameContentLabel = [UILabel new];
    [patientNameContentLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    patientNameContentLabel.numberOfLines = 0;
    [self.contentView addSubview:patientNameContentLabel];
    self.patientNameContentLabel = patientNameContentLabel;
    
    
    UILabel *illNameLabel = [UILabel new];
    [illNameLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self.contentView addSubview:illNameLabel];
    self.illNameLabel = illNameLabel;
    
    
    UILabel *illNameContentLabel = [UILabel new];
    [illNameContentLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    illNameContentLabel.numberOfLines = 0;
    [self.contentView addSubview:illNameContentLabel];
    self.illNameContentLabel = illNameContentLabel;
    
    
    UILabel *illDetailLabel = [UILabel new];
    [illDetailLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    [self.contentView addSubview:illDetailLabel];
    self.illDetailLabel = illDetailLabel;
    
    
    
    UILabel *illDetailContentLabel = [UILabel new];
    [illDetailContentLabel setTextFont:kP(32) textColor:@"#9B9B9B" alpha:1.0];
    illDetailContentLabel.numberOfLines = 0;
    [self.contentView addSubview:illDetailContentLabel];
    self.illDetailContentLabel = illDetailContentLabel;
    
    
    
}

-(void)setUpSubViwsFrameWithIsInOrderDetail:(BOOL)isInOrderDetail {
    
    [self setSubViewsContent];
    
    
    
    CGSize patientNameSize = [self.patientNameLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(33)];
    CGFloat patientNameLabelW = kP(150);
    CGFloat patientNameLabelH = patientNameSize.height;
    CGFloat patientNameLabelX = isInOrderDetail ? kP(92) : kP(30);
    CGFloat patientNameLabelY = kP(28);
    
    self.patientNameLabel.frame = CGRectMake(patientNameLabelX, patientNameLabelY, patientNameLabelW, patientNameLabelH);
    
    //
    CGFloat maxWidth =  kP(430);
    
    //
    //    CGSize patientNameContentSize = [self.patientNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(32)];
    //    CGFloat patientNameContentLabelX = CGRectGetMaxX(self.patientNameLabel.frame) + kP(20);
    //    CGFloat patientNameContentLabelY = patientNameLabelY;
    //    CGFloat patientNameContentLabelW = maxWidth;
    //    CGFloat patientNameContentLabelH = patientNameContentSize.height;
    //    self.patientNameContentLabel.frame = CGRectMake(patientNameContentLabelX, patientNameContentLabelY, patientNameContentLabelW, patientNameContentLabelH);
    CGSize patientNameContentSize = [self.patientNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(32)];
    CGFloat patientNameContentLabelX = self.patientNameLabel.right + kP(20);
    CGFloat patientNameContentLabelY = patientNameLabelY;
    CGFloat patientNameContentLabelW = maxWidth;
    CGFloat patientNameContentLabelH = patientNameContentSize.height;
    self.patientNameContentLabel.frame = CGRectMake(patientNameContentLabelX, patientNameContentLabelY, patientNameContentLabelW, patientNameContentLabelH);
    
    //2.
    
    //    CGSize illNameSize = [self.illNameLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(32)];
    //    CGFloat illNameLabelW = patientNameLabelW;
    //    CGFloat illNameLabelH = illNameSize.height;
    //    CGFloat illNameLabelX = patientNameLabelX;
    //    CGFloat illNameLabelY =  MAX(CGRectGetMaxY(self.patientNameLabel.frame), CGRectGetMaxY(self.patientNameContentLabel.frame)) + kP(20);
    //    self.illNameLabel.frame = CGRectMake(illNameLabelX, illNameLabelY, illNameLabelW, illNameLabelH);
    CGSize illNameSize = [self.illNameLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(32)];
    CGFloat illNameLabelW = patientNameLabelW;
    CGFloat illNameLabelH = illNameSize.height;
    CGFloat illNameLabelX = patientNameLabelX;
    CGFloat illNameLabelY =  MAX(self.patientNameLabel.bottom, self.patientNameContentLabel.bottom) + kP(20);
    self.illNameLabel.frame = CGRectMake(illNameLabelX, illNameLabelY, illNameLabelW, illNameLabelH);
    
    
    //    CGSize illNameContentSize = [self.illNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(32)];
    //    CGFloat illNameContentLabelW = maxWidth;
    //    CGFloat illNameContentLabelH = illNameContentSize.height;
    //    CGFloat illNameContentLabelX = CGRectGetMaxX(self.illNameLabel.frame) + kP(20);
    //    CGFloat illNameContentLabelY = illNameLabelY;
    //    self.illNameContentLabel.frame = CGRectMake(illNameContentLabelX, illNameContentLabelY, illNameContentLabelW, illNameContentLabelH);
    CGSize illNameContentSize = [self.illNameContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(33)];
    CGFloat illNameContentLabelW = maxWidth;
    CGFloat illNameContentLabelH = illNameContentSize.height;
    CGFloat illNameContentLabelX = self.illNameLabel.right + kP(20);
    CGFloat illNameContentLabelY = illNameLabelY;
    self.illNameContentLabel.frame = CGRectMake(illNameContentLabelX, illNameContentLabelY, illNameContentLabelW, illNameContentLabelH);
    
    //3.
    //    CGSize illDetailSize = [self.illDetailLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(32)];
    //    CGFloat illDetailLabelW = patientNameLabelW;
    //    CGFloat illDetailLabelH = illDetailSize.height;
    //    CGFloat illDetailLabelX = illNameLabelX;
    //    CGFloat illDetailLabelY = MAX(CGRectGetMaxY(self.illNameLabel.frame), CGRectGetMaxY(self.illNameContentLabel.frame)) + kP(20);
    //    self.illDetailLabel.frame = CGRectMake(illDetailLabelX, illDetailLabelY, illDetailLabelW, illDetailLabelH);
    CGSize illDetailSize = [self.illDetailLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(33)];
    CGFloat illDetailLabelW = patientNameLabelW;
    CGFloat illDetailLabelH = illDetailSize.height;
    CGFloat illDetailLabelX = illNameLabelX;
    CGFloat illDetailLabelY = MAX(self.illNameLabel.bottom, self.illNameContentLabel.bottom) + kP(20);
    self.illDetailLabel.frame = CGRectMake(illDetailLabelX, illDetailLabelY, illDetailLabelW, illDetailLabelH);
    
    //    CGSize illDetailContentSize = [self.illDetailContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(32)];
    //    CGFloat illDetailContentLabelW = maxWidth;
    //    CGFloat illDetailContentLabelH =  illDetailContentSize.height;
    //    CGFloat illDetailContentLabelX = CGRectGetMaxX(self.illDetailLabel.frame) + kP(20);
    //    CGFloat illDetailContentLabelY = illDetailLabelY;
    //    self.illDetailContentLabel.frame = CGRectMake(illDetailContentLabelX, illDetailContentLabelY, illDetailContentLabelW, illDetailContentLabelH);
    CGSize illDetailContentSize = [self.illDetailContentLabel.text getTextSizeWithMaxWidth:maxWidth textFontSize:kP(32)];
    CGFloat illDetailContentLabelW = maxWidth;
    CGFloat illDetailContentLabelH =  illDetailContentSize.height;
    CGFloat illDetailContentLabelX = self.illDetailLabel.right + kP(20);
    CGFloat illDetailContentLabelY = illDetailLabelY;
    self.illDetailContentLabel.frame = CGRectMake(illDetailContentLabelX, illDetailContentLabelY, illDetailContentLabelW, illDetailContentLabelH);
    
    
}

- (void)setSubViewsContent {

    self.patientNameLabel.text = @"患者姓名:";
    self.patientNameContentLabel.text = self.order.outpatientName;
    self.illNameLabel.text = @"疾病名称:";
    self.illNameContentLabel.text = self.order.icd10Value;
    self.illDetailLabel.text = @"病情描述:";
    self.order.sicknessRemark = [self.order.sicknessRemark stringByReplacingEmojiCheatCodesWithUnicode];
    self.illDetailContentLabel.text = self.order.sicknessRemark;
}

- (void)setIsInOrderDetail:(BOOL)isInOrderDetail {
    _isInOrderDetail = isInOrderDetail;
    
    //
    if (isInOrderDetail) return;
    
    [self setUpSubViwsFrameWithIsInOrderDetail:isInOrderDetail];
    self.patientNameLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
    self.patientNameContentLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
    //
    self.illNameLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
    self.illNameContentLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
    //
    self.illDetailLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
    self.illDetailContentLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
}

- (void)setOrder:(HZOrder *)order {
    _order = order;
    
    [self setUpSubViwsFrameWithIsInOrderDetail:YES];
    
//    if (_isInOrderDetail) return;
    
    //检验资料
    CGFloat shouldOffset = 0;
    
    // 创建之前先删除
    [self clearCustomAdd];
    if (order.laboratoryReports.count >0) {
        shouldOffset = [self createCheckPicView:@"检验资料:" offset:0 picArray:order.laboratoryReports type:0];
    }
    //
    //影像资料
    if (order.examReports.count > 0) {
        
        [self createCheckPicView:@"影像资料:" offset:shouldOffset picArray:order.examReports type:1];
    }
    
}

-(void)clearCustomAdd{
    for (UIView *picV in self.PicViewDict.allValues) {
        [picV removeFromSuperview];
    }
    [self.PicViewDict removeAllObjects];
    for (UIView *label in self.contentView.subviews) {
        if (label.tag == 6) [label removeFromSuperview];
    }
}

- (CGFloat)createCheckPicView:(NSString*)title offset:(CGFloat)offset picArray:(NSArray*)array type:(NSInteger)type{
    
    UILabel *label = [UILabel new];
    label.font = [UIFont systemFontOfSize:kP(32)];
    label.tag = 6;
    label.textColor = self.order.isInOrderDetail ? [UIColor colorWithHexString:@"#9B9B9B"]: [UIColor colorWithHexString:@"#4A4A4A"];
    label.text = title;
    [label sizeToFit];
    

    label.x = self.order.isInOrderDetail ? kP(92) : kP(30);;
    label.y = self.illDetailContentLabel.bottom + kP(36) + offset;
    
    
    //
    CGFloat picVX = self.order.isInOrderDetail ? self.illDetailContentLabel.x :  self.illDetailContentLabel.x - kP(62);
    CGFloat picVW = HZScreenW - kP(28) - picVX - kP(8);
    
    CGFloat imageMargin = kP(16);
    // 一行四个
//    CGFloat imageWH = (picVW - 3 * imageMargin) / 4.0;
    CGFloat imageWH = kP(120);
    NSInteger totleNum = ((array.count -1) / 4 + 1);
    CGFloat picVH = totleNum * imageWH + (totleNum -1) * imageMargin;
    
    //
    UIView *picV = [[UIView alloc]initWithFrame:CGRectMake(picVX, label.y, picVW, picVH)];
//    picV.backgroundColor = [UIColor redColor];
    //
    for (int i = 0; i<array.count; ++i) {
        int section = i / 4;
        int row = i % 4;
        
        //
        UIImageView * imgV = [[UIImageView alloc]initWithFrame:CGRectMake(row*(imageMargin +imageWH), section*(imageWH+imageMargin), imageWH, imageWH)];
        imgV.layer.borderWidth = kP(2);
        imgV.layer.borderColor = [UIColor colorWithHexString:@"#d9d9d9"].CGColor;
        imgV.userInteractionEnabled = YES;
        imgV.layer.cornerRadius = kP(12);
        imgV.layer.masksToBounds = YES;
        imgV.tag = i;
        [imgV addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTouch:)]];
        
        // 设置图片
        HZIllnessDataImg *model = array[i];
//        [self getImageFromNetWork:model.reportImageUri andImageView:imgV];
        [imgV getPictureWithFaceUrl:model.reportImageUri AndPlacehoderImage:nil];
        [picV addSubview:imgV];
        objc_setAssociatedObject(imgV, "Type", @(type), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        
    }
    
    [self.contentView addSubview:label];
    [self.contentView addSubview:picV];
    [self.PicViewDict setObject:picV forKey:@(type)];
    
    // 返回高度; (间距);
    return picVH + kP(36);
    
}
//  imageView分类中添加方法 废弃这个
//- (void) getImageFromNetWork:(NSString*)url andImageView:(UIImageView*)imageV{
//
//   UIImage *cacheImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:url];
//    if (cacheImage) {
//        imageV.image = cacheImage;
//        return;
//    }
//
//    [HZUserTool getAvatarWithFaceUrl:url success:^(UIImage *image,NSString*urlKey) {
//        [[SDImageCache sharedImageCache] storeImage:image forKey:url toDisk:YES];
//        imageV.image = image;
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"获取病情资料图片错误:%@", error);
//        //占位图片
//    }];
//
//}

#pragma mark -- class
//
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"localPatientInforContentCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

+ (CGFloat)cellHeightWithOrder:(HZOrder *)order {
    CGFloat maxWidth = kP(430);
    CGFloat height1 = [order.outpatientName getTextHeightWithMaxWidth:maxWidth textFontSize:kP(32)];
    CGFloat height2 = [order.icd10Value getTextHeightWithMaxWidth:maxWidth textFontSize:kP(32)];
    CGFloat height3 = [order.sicknessRemark getTextHeightWithMaxWidth:maxWidth textFontSize:kP(32)];
    CGFloat height4 = [HZOrderPatientInforContentCell caclueTheHeightForPicArray:order.laboratoryReports];
    CGFloat height5 = [HZOrderPatientInforContentCell caclueTheHeightForPicArray:order.examReports];
    return height1 +height2 + height3 + height4 +height5 + 6 * kP(28);
}


+(CGFloat)caclueTheHeightForPicArray:(NSArray*)array{
    
    if (array.count == 0) {
        return 0.0;
    }
    
    CGFloat imageMargin = kP(16);
    CGFloat picVX = kP(92) + kP(150) + kP(8);
    CGFloat picVW = HZScreenW - kP(28) - picVX;
    // 一行四个
    CGFloat imageWH = (picVW - 3*imageMargin) / 4.0;
    NSInteger totleNum = ((array.count -1) / 4 + 1);
    CGFloat picVH = totleNum * imageWH + (totleNum -1)*imageMargin;
    picVH += kP(28);
    return picVH;
    
}

-(void)imageTouch:(UITapGestureRecognizer*)tap {
    
    
    UIImageView *imageV = (UIImageView*)tap.view;
    NSNumber *type = objc_getAssociatedObject(imageV, "Type");
    self.type = type.integerValue;
    //1.创建图片浏览器
    SDPhotoBrowser *photoBrowser = [SDPhotoBrowser new];
    photoBrowser.backgroundColor = [UIColor colorWithHexString:@"#333333"];
    photoBrowser.delegate = self;
    photoBrowser.currentImageIndex = imageV.tag;
    photoBrowser.imageCount = type.boolValue?self.order.examReports.count:self.order.laboratoryReports.count;
    photoBrowser.showIndexLabel = photoBrowser.imageCount > 1;
    UIView *sourceView = [self.PicViewDict objectForKey:type];
    photoBrowser.sourceImagesContainerView = sourceView;
    [photoBrowser show];
}

#pragma mark - SDPhotoBrowserDelegate
- (UIImage *)photoBrowser:(SDPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    NSNumber *type = [NSNumber numberWithInteger:self.type];
    UIView *fatherV = [self.PicViewDict objectForKey:type];
    UIImageView *imageView = fatherV.subviews[index];
    return imageView.image;
}

@end

