//
//  HZLocalPatientInforCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@protocol HZLocalPatientInforCellDelegate <NSObject>

- (void)enterMorePatientInfor;

@end
@interface HZLocalPatientInforCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) id <HZLocalPatientInforCellDelegate> delegate;
//** */
@property (nonatomic, strong) HZOrder *localOrder;
@end
