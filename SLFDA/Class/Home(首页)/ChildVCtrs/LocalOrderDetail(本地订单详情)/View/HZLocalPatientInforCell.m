//
//  HZLocalPatientInforCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZLocalPatientInforCell.h"
#import "HZOrder.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZLocalPatientInforCell ()
@property (nonatomic, strong) UIImageView *patientInforImageView;
@property (nonatomic, strong) UILabel *patientInforLabel;
//** <#注释#>*/
@property (nonatomic, strong) UIView *enterBgView;
@property (nonatomic, strong) UILabel *morePatientInforLabel;
@property (nonatomic, strong) UIImageView *enterIndicatorImageView;
@end
NS_ASSUME_NONNULL_END
@implementation HZLocalPatientInforCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
//        self.backgroundColor = [UIColor greenColor];
    }
    
    return self;
}

- (void)setUpSubViews {
    UIImageView *patientInforImageView = [[UIImageView alloc] init];
    [self addSubview:patientInforImageView];
    self.patientInforImageView = patientInforImageView;
    
    UILabel *patientInforLabel = [[UILabel alloc] init];
    [patientInforLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    [self addSubview:patientInforLabel];
    self.patientInforLabel = patientInforLabel;
    
    
    
    UIView *enterBgView = [[UIView alloc] init];
    [self addSubview:enterBgView];
    self.enterBgView = enterBgView;
    
    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeDelegateEnterMorePatientInfor:)];
    [self.enterBgView addGestureRecognizer:tap];
    
    
    //
    UILabel *morePatientInforLabel = [[UILabel alloc] init];
    [morePatientInforLabel setTextFont:kP(32) textColor:@"#2DBED8" alpha:1.0];
    [self.enterBgView addSubview:morePatientInforLabel];
    self.morePatientInforLabel = morePatientInforLabel;
    
    UIImageView *enterIndicatorImageView = [[UIImageView alloc] init];
    [self.enterBgView addSubview:enterIndicatorImageView];
    self.enterIndicatorImageView = enterIndicatorImageView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    
    //2.
    CGFloat patientInforImageViewWH = kP(40);
    CGFloat patientInforImageViewX = kP(32);
    CGFloat patientInforImageViewY = kP(32);
    
    self.patientInforImageView.frame = CGRectMake(patientInforImageViewX, patientInforImageViewY, patientInforImageViewWH, patientInforImageViewWH);
    
    
    //    CGSize patientInforSize = [self.patientInforLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(30)];
    //    CGFloat patientInforLabelW = patientInforSize.width;
    //    CGFloat patientInforLabelH = patientInforSize.height;
    //    CGFloat patientInforLabelX = CGRectGetMaxX(self.patientInforImageView.frame) + kP(20);
    //    CGFloat patientInforLabelY = self.patientInforImageView.y;
    //    self.patientInforLabel.frame = CGRectMake(patientInforLabelX, patientInforLabelY, patientInforLabelW, patientInforLabelH);
    
    [self.patientInforLabel sizeToFit];
    self.patientInforLabel.x = self.patientInforImageView.right + kP(20);
    self.patientInforLabel.y = self.patientInforImageView.centerY - self.patientInforLabel.height * 0.5;
    
    //
    if ([self.localOrder.bookingStateCode isEqualToString:@"008"]) return;
    CGFloat enterBgViewW = kP(160);
    CGFloat enterBgViewH = kP(40);
    CGFloat enterBgViewX = self.width - enterBgViewW - kP(38);
    CGFloat enterBgViewY = self.patientInforImageView.centerY - enterBgViewH * 0.5;
    self.enterBgView.frame = CGRectMake(enterBgViewX, enterBgViewY, enterBgViewW, enterBgViewH);
    
    
    //    CGSize  morePatientInforSize = [self.morePatientInforLabel.text getTextSizeWithMaxWidth:kP(300) textFontSize:kP(30)];
    //    CGFloat morePatientInforLabelW = morePatientInforSize.width;
    //    CGFloat morePatientInforLabelH = enterBgViewH;
    //    CGFloat morePatientInforLabelX = 0;
    //    CGFloat morePatientInforLabelY = 0;
    //    self.morePatientInforLabel.frame = CGRectMake(morePatientInforLabelX, morePatientInforLabelY, morePatientInforLabelW, morePatientInforLabelH);
    
    
    [self.morePatientInforLabel sizeToFit];
    self.morePatientInforLabel.x = 0;
    self.morePatientInforLabel.y = 0;
    
    CGFloat enterIndicatorImageViewW = kP(20);
    CGFloat enterIndicatorImageViewH = kP(32);
    CGFloat enterIndicatorImageViewX = self.morePatientInforLabel.right + kP(20);
    CGFloat enterIndicatorImageViewY = self.morePatientInforLabel.centerY -  enterIndicatorImageViewH * 0.5;
    self.enterIndicatorImageView.frame = CGRectMake(enterIndicatorImageViewX, enterIndicatorImageViewY, enterIndicatorImageViewW, enterIndicatorImageViewH);
    self.enterIndicatorImageView.contentMode = UIViewContentModeCenter;
    self.enterIndicatorImageView.clipsToBounds = NO;
}




#pragma mark -- <#class#>

- (void)setSubViewsContent {
    self.patientInforImageView.image = [UIImage imageWithOriginalName:@"patientInfor"];
    self.patientInforLabel.text = @"患者资料";
    self.morePatientInforLabel.text = @"更多资料";
    self.enterIndicatorImageView.image = [UIImage imageNamed:@"arrow"];
}
+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"localPatientInforCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

- (void)setLocalOrder:(HZOrder *)localOrder {
    _localOrder = localOrder;
}

- (void)makeDelegateEnterMorePatientInfor:(UITapGestureRecognizer *)tap {
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterMorePatientInfor)]) {
        [self.delegate enterMorePatientInfor];
    }
}
@end
