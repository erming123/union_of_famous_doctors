//
//  HZLocalOrderDetailViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/14.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZLocalOrderDetailViewController.h"
#import "HZLocalOrderStatusCell.h"
#import "HZLocalOrderStartTimeCell.h"
#import "HZRemoteExpertInforContentCell.h"
#import "HZLocalPatientInforCell.h"
#import "HZOrderPatientInforContentCell.h"
#import "MJRefresh.h"
#import "HZOrder.h"
#import "HZOrderPatientInforViewController.h"
#import "HZRoomViewController.h"
#import "HZIMChatViewController.h"
#import "HZUser.h"
#import "HZMessageDB.h"

//
#import "HZOrder.h"
#import "HZPayOrder.h"
//
#import "HZAlertPayView.h"
//
#import "HZOrderHttpsTool.h"
#import "HZCheckListTool.h"
#define kLocalUnReadCountStr @"localUnReadCountStr"
@interface HZLocalOrderDetailViewController ()<UITableViewDataSource, UITableViewDelegate, HZLocalPatientInforCellDelegate, HZRemoteExpertInforContentCellDelegate, HZRoomViewControllerDelegate, HZLocalOrderStatusCellDelegate, HZAlertPayViewDelegate>

//** <#注释#>*/
@property (nonatomic, strong) HZTableView *localOrderDetailTableView;
@property (nonatomic, assign) BOOL hasBegan;
//** <#注释#>*/
@property (nonatomic, strong) HZMessageDB *messageDB;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *endOutpatientBtn;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *enterVideoBtn;
@property (nonatomic, strong) UIView *btnBgView;

//
//** <#注释#>*/
@property (nonatomic, strong) HZPayOrder *payOrder;
//** <#注释#>*/
@property (nonatomic, strong) HZAlertPayView *alertPayView;
//
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation HZLocalOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    self.tableView.showsVerticalScrollIndicator = NO;
//    


    
    //1.
    self.navigationItem.title = @"订单详情";
    
    //2.
    HZTableView *localOrderDetailTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    localOrderDetailTableView.dataSource = self;
    localOrderDetailTableView.delegate = self;
    localOrderDetailTableView.showsVerticalScrollIndicator = NO;
    localOrderDetailTableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    localOrderDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:localOrderDetailTableView];
    self.localOrderDetailTableView = localOrderDetailTableView;
    
    
    //
    [self setUpVideoBtn];
    
    self.hasBegan = self.localOrder.expertOpenid != nil;
    
    
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLocalMessageSuccess:) name:KNOTIFICATION_onMesssageReceive object:nil];
    //
//    [self getPayParamsDict];
    
    //
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;

    
    NSLog(@"-------xcv-self.localOrder---------%@", self.localOrder);
}


- (void)setUpVideoBtn {
    
    [self.btnBgView removeFromSuperview];
    UIView *btnBgView = [[UIView alloc] initWithFrame:CGRectMake(kP(0), self.view.height - kP(130), self.view.width, kP(130))];
    btnBgView.backgroundColor = [UIColor whiteColor];
    btnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    btnBgView.layer.shadowOffset = CGSizeMake(0, - kP(2));
    btnBgView.layer.shadowOpacity = 0.1;
    btnBgView.layer.shadowRadius = kP(2);
    
    //
    CGFloat btnWidth = (btnBgView.width - 3 * kP(32)) * 0.5;
    CGFloat btnHeight = btnBgView.height - 2 * kP(15);
    //
//    UIButton *endOutpatientBtn = [[UIButton alloc] initWithFrame:CGRectMake(kP(32), kP(15), btnWidth, btnHeight)];
    UIButton *endOutpatientBtn = [UIButton new];
    //
//    UIButton *enterVideoBtn = [[UIButton alloc] initWithFrame:CGRectMake(kP(32), kP(15), btnBgView.width - 2 * kP(32), btnBgView.height - 2 * kP(15))];
    UIButton *enterVideoBtn = [UIButton new];
    
    if ([self.localOrder.bookingStateCode isEqualToString:@"003"] || [self.localOrder.bookingStateCode isEqualToString:@"004"]) {
        // 隐藏endOutpatientBtn, 显示enterVideoBtn
        endOutpatientBtn.hidden = YES;
        enterVideoBtn.hidden = NO;
        
        //
        enterVideoBtn.x = kP(32);
        enterVideoBtn.width = btnBgView.width - 2 * enterVideoBtn.x;
        //
        self.localOrderDetailTableView.height = self.view.height - kP(130);
    } else if ([self.localOrder.bookingStateCode isEqualToString:@"006"]) {
        
        // 显示endOutpatientBtn和enterVideoBtn
        endOutpatientBtn.hidden = NO;
        enterVideoBtn.hidden = NO;
        
        //
        endOutpatientBtn.x = kP(32);
        endOutpatientBtn.width = btnWidth;
        enterVideoBtn.x = endOutpatientBtn.right + kP(32);
        enterVideoBtn.width = btnWidth;
        //
       self.localOrderDetailTableView.height = self.view.height - kP(130);
    } else if ([self.localOrder.bookingStateCode isEqualToString:@"001"] || [self.localOrder.bookingStateCode isEqualToString:@"002"] || [self.localOrder.bookingStateCode isEqualToString:@"005"] || [self.localOrder.bookingStateCode isEqualToString:@"007"] || [self.localOrder.bookingStateCode isEqualToString:@"008"]) {
        //
        endOutpatientBtn.hidden = YES;
        enterVideoBtn.hidden = YES;
        btnBgView.hidden = YES;
        self.localOrderDetailTableView.height = self.view.height;
    }
    
    
    endOutpatientBtn.y = kP(15);
    endOutpatientBtn.height = btnHeight;
    [endOutpatientBtn setTitle:@"结束本次门诊" forState:UIControlStateNormal];
    [endOutpatientBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    //
    endOutpatientBtn.backgroundColor = [UIColor whiteColor];
    endOutpatientBtn.layer.borderColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0].CGColor;
    endOutpatientBtn.layer.borderWidth = kP(2);
    endOutpatientBtn.layer.cornerRadius = kP(10);
    endOutpatientBtn.clipsToBounds = YES;
    
    //
    [endOutpatientBtn addTarget:self action:@selector(endOutpatient:) forControlEvents:UIControlEventTouchUpInside];
    [btnBgView addSubview:endOutpatientBtn];
    [self.view addSubview:btnBgView];
    self.endOutpatientBtn = endOutpatientBtn;
    
    
    enterVideoBtn.y = kP(15);
    enterVideoBtn.height = btnHeight;
    enterVideoBtn.backgroundColor = kGreenColor;
    enterVideoBtn.layer.cornerRadius = kP(10);
    enterVideoBtn.clipsToBounds = YES;
    [enterVideoBtn setTitle:@"进入视频诊间" forState:UIControlStateNormal];
    
    [enterVideoBtn addTarget:self action:@selector(enterVideoRoom:) forControlEvents:UIControlEventTouchUpInside];
    [btnBgView addSubview:enterVideoBtn];
    self.enterVideoBtn = enterVideoBtn;
    self.btnBgView = btnBgView;
}

- (void)receiveLocalMessageSuccess:(NSNotification *)notifucation {
    NSLog(@"-----dsdsds2222222----%@", notifucation.object);
    [self refreshCheckList];
}


- (HZMessageDB *)messageDB {
    if (!_messageDB) {
        _messageDB = [HZMessageDB initMessageDB];
    }
    
    return _messageDB;
}



#pragma mark -- <#class#>
- (void)refreshCheckList {
    
    
    if (![self.localOrder.bookingStateCode isEqualToString:@"006"]) {
        [self reFreshLocalOrderDetail];
    }
    
    
    
    [HZCheckListTool getCheckListWithBookingOrderId:self.localOrder.bookingOrderId  patient:nil userHospitalId:nil success:^(id responseObject) {
        
        
        NSLog(@"---ssddgg--:%@", responseObject);
        NSDictionary *resultsDict = responseObject[@"results"];
        NSArray *localCheckList = resultsDict[@"info"];
        [[NSUserDefaults standardUserDefaults] setObject:localCheckList forKey:self.localOrder.bookingOrderId];
        //
        NSMutableArray *receivedMsgArr = [self.messageDB getReceivedMsgUnReadCountArrWithOrderId:self.localOrder.bookingOrderId];
        NSString *unReadCountStr = [receivedMsgArr lastObject];
        [[NSUserDefaults standardUserDefaults] setObject:unReadCountStr forKey:kLocalUnReadCountStr];
        [self.localOrderDetailTableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------rrrrrr-错误--%@", error);
    }];
    
    
    
    NSLog(@"--------2233");
    
    if ([self.localOrder.bookingStateCode isEqualToString:@"006"]) return;
    [HZOrderHttpsTool getOrderDetailWithOrderID:self.localOrder.bookingOrderId success:^(id responseObject) {
        NSLog(@"------getOrderDetailWithOrderID---%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSDictionary *bookingOrderDict = resultsDict[@"bookingOrder"];
            
            HZOrder *localOrder = [HZOrder mj_objectWithKeyValues:bookingOrderDict];
            localOrder.orderType = @"01";
            self.localOrder = [HZOrder mj_objectWithKeyValues:[localOrder propertyValueDict]];
            [self setUpVideoBtn];
            [self.localOrderDetailTableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"--------2233----wwww");
    }];

}

#pragma mark -- getPayParamsDictToImageData
- (void)getPayParamsDictToImageData {
    
    NSLog(@"---------sdffgh------:%@", self.localOrder.bookingOrderId);
    [self.activityIndicator startAnimating];
    [HZOrderHttpsTool getPayParamsWithBookingOrderId:self.localOrder.bookingOrderId success:^(id responseObject) {
        NSLog(@"--getPayParamsWithBookingOrderId---成功:----%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *payDict = responseObject[@"results"];
            
           HZPayOrder *payOrder = [HZPayOrder mj_objectWithKeyValues:payDict];
            payOrder = [HZPayOrder mj_objectWithKeyValues:[payOrder propertyValueDict]];
            NSLog(@"---------cdf--123-------------:%@", payOrder);
//            
//            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//            dispatch_group_t group = dispatch_group_create();
//            
//            dispatch_group_async(group, queue, ^{
//                // 获取微信二维码图片
                [self getWeChatQRImageWith:payOrder];
//            });
//            dispatch_group_async(group, queue, ^{
                // 获取支付宝二维码图片
                [self getAliPayQRImageWith:payOrder];
//                self.alertPayView.hidden = NO;
//            });

//            //等group里的task都执行完后执行notify方法里的内容
//            dispatch_group_notify(group, queue, ^{
//               
//            });
           
           
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----失败:----%@", error);
    }];
}

- (void)getWeChatQRImageWith:(HZPayOrder *)payOrder {
    
    //1.0
//    if (![self.localOrder.bookingStateCode isEqualToString:@"001"]) return;
    
    //2.0
//    NSDate *senddate = [NSDate date];
//    NSInteger currentTime = (long)[senddate timeIntervalSince1970];
//    
//    NSInteger weChatOldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"WeChatQRImageOldTime%@", payOrder.bookingOrderId]] integerValue];
    
//    NSString *weChatQRImageId = [NSString stringWithFormat:@"WeChatQRImage%@", payOrder.bookingOrderId];
//    NSData *weChatQRImageData = [[NSUserDefaults standardUserDefaults] objectForKey:weChatQRImageId];
    
    
//    if ((currentTime - weChatOldTime) <= 60 && weChatQRImageData) return;

   
    // 3.0
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{

        [HZOrderHttpsTool getWeChatPayIdWithPayOrder:payOrder success:^(id responseObject) {
            NSLog(@"-----clickToWeChatPayY----%@",  [(NSDictionary *)responseObject getDictJsonStr]);
            
            if ([responseObject[@"state"] isEqual:@200]) {
                NSDictionary *resultsDict = responseObject[@"results"];
                
                //
                NSString *relationId = resultsDict[@"relationId"];
                //
                NSLog(@"---------WeChatPayrelationId:%@", relationId);
                //
                [HZOrderHttpsTool getQRImageWithRelationId:relationId success:^(NSData *QRImageData) {
                    
                    
                    NSLog(@"--------Wy-------%@", QRImageData);
                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        //
                        NSDate *senddate = [NSDate date];
                        NSString *dateStr = [NSString stringWithFormat:@"%ld", (long)[senddate timeIntervalSince1970]];
                        [[NSUserDefaults standardUserDefaults] setObject:dateStr forKey:[NSString stringWithFormat:@"WeChatQRImageOldTime%@", payOrder.bookingOrderId]];
                        //
                        NSString *QRImageId = [NSString stringWithFormat:@"WeChatQRImage%@", payOrder.bookingOrderId];
                        [[NSUserDefaults standardUserDefaults] setObject:QRImageData forKey:QRImageId];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        //
                    
                    self.payOrder = payOrder;
                    
                    //
                    dispatch_async(dispatch_get_main_queue(), ^{
//                        //
//                        [self.alertPayView removeFromSuperview];
//                        [self.alertPayView.timer invalidate];
//                        self.alertPayView.timer = nil;
//                        
//                        HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:payOrder];
//                        alertPayView.delegate = self;
//                        [self.navigationController.view addSubview:alertPayView];
//                        self.alertPayView = alertPayView;
//                        
//                        //
//                        self.alertPayView.hidden = NO;
                        
                        
                        //
                        [self setMyAlertPayViewWithPayOrder:payOrder];
                        //
                        [self.activityIndicator stopAnimating];
                    });

                    
                    //
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"--------Wf-------%@", error);
                }];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"------clickToWeChatPayF-----%@", error);
        }];
//    });
    
   
}


- (void)getAliPayQRImageWith:(HZPayOrder *)payOrder {
    
//    //2.0
//    NSDate *senddate = [NSDate date];
//    NSInteger currentTime = (long)[senddate timeIntervalSince1970];
//    
//    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//    
//    NSInteger AliOldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"AliQRImageOldTime%@", payOrder.bookingOrderId]] integerValue];
//    NSString *AliQRImageId = [NSString stringWithFormat:@"AliQRImage%@", payOrder.bookingOrderId];
//    NSData *AliQRImageData = [[NSUserDefaults standardUserDefaults] objectForKey:AliQRImageId];
    
    //2.0
//    if ((currentTime - AliOldTime) <= 60 && AliQRImageData) return;
    
    //    //3.0
    //    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    
    NSLog(@"---sdf-%@", payOrder);
    
    [HZOrderHttpsTool getAliPayIdWithPayOrder:payOrder success:^(id responseObject) {
        NSLog(@"-----clickToAliPayY----%@", [(NSDictionary *)responseObject getDictJsonStr]);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            
            //
            NSString *relationId = resultsDict[@"relationId"];
            
            
            //
            NSLog(@"---------AliPayrelationId:%@", relationId);
            
            
            //
            [HZOrderHttpsTool getQRImageWithRelationId:relationId success:^(NSData *QRImageData) {
                NSLog(@"--------Ay-------%@", QRImageData);
                //
                NSDate *senddate = [NSDate date];
                NSString *dateStr = [NSString stringWithFormat:@"%ld", (long)[senddate timeIntervalSince1970]];
                [[NSUserDefaults standardUserDefaults] setObject:dateStr forKey:[NSString stringWithFormat:@"AliQRImageOldTime%@", payOrder.bookingOrderId]];
                //
                NSString *QRImageId = [NSString stringWithFormat:@"AliQRImage%@", payOrder.bookingOrderId];
                [[NSUserDefaults standardUserDefaults] setObject:QRImageData forKey:QRImageId];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //
                self.payOrder = payOrder;
                
                dispatch_async(dispatch_get_main_queue(), ^{
//                    //
//                    [self.alertPayView removeFromSuperview];
//                    [self.alertPayView.timer invalidate];
//                    self.alertPayView.timer = nil;
//
//                    HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:payOrder];
//                    alertPayView.delegate = self;
//                    [self.navigationController.view addSubview:alertPayView];
//                    self.alertPayView = alertPayView;
//                    
//                    //
//                    self.alertPayView.hidden = NO;
                    
                    //
                    [self setMyAlertPayViewWithPayOrder:payOrder];
                    //
                    [self.activityIndicator stopAnimating];
                });
               
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"--------Af-------%@", error);
                
            }];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------clickToAliPayF-----%@", error);
        
    }];
    //    });
    
    
}


#pragma mark -- getPayParamsDict

- (void)getPayParamsDict {
   
    [HZOrderHttpsTool getPayParamsWithBookingOrderId:self.localOrder.bookingOrderId success:^(id responseObject) {
        NSLog(@"--getPayParamsWithBookingOrderId---成功:----%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *payDict = responseObject[@"results"];
            
            HZPayOrder *payOrder = [HZPayOrder mj_objectWithKeyValues:payDict];
            payOrder = [HZPayOrder mj_objectWithKeyValues:[payOrder propertyValueDict]];
            NSLog(@"---------cdf--123-------------:%@", payOrder);
            
            self.payOrder = payOrder;
            dispatch_async(dispatch_get_main_queue(), ^{
//                //
//                [self.alertPayView removeFromSuperview];
//                [self.alertPayView.timer invalidate];
//                self.alertPayView.timer = nil;
//                
//                HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:payOrder];
//                alertPayView.delegate = self;
//                [self.navigationController.view addSubview:alertPayView];
//                self.alertPayView = alertPayView;
//                
//                //
//                self.alertPayView.hidden = NO;
                
                [self setMyAlertPayViewWithPayOrder:payOrder];
            });

        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----失败:----%@", error);
    }];
    
}

#pragma mark -- reFreshLocalOrderDetail
- (void)reFreshLocalOrderDetail {
    [HZOrderHttpsTool getOrderDetailWithOrderID:self.localOrder.bookingOrderId success:^(id responseObject) {
        NSLog(@"------getOrderDetailWithOrderID---%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            NSDictionary *bookingOrderDict = resultsDict[@"bookingOrder"];
          
            HZOrder *localOrder = [HZOrder mj_objectWithKeyValues:bookingOrderDict];
            localOrder.orderType = @"01";
            self.localOrder = [HZOrder mj_objectWithKeyValues:[localOrder propertyValueDict]];
            NSLog(@"--------asd-------:%@", self.localOrder);
            [self setUpVideoBtn];
            [self.localOrderDetailTableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"--------2233----wwww");
    }];
}

//
- (void)setUnReadMsgCountStr:(NSString *)unReadMsgCountStr {
    _unReadMsgCountStr = unReadMsgCountStr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self refreshCheckList];
}


//- (HZOrder *)localOrder {
//    if (!_localOrder) {
//        _localOrder = [HZOrder new];
//    }
//    return _localOrder;
//}

- (void)setLocalOrder:(HZOrder *)localOrder {
    _localOrder = localOrder;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    
    
    if (self.hasBegan) {
        return 4;
    } else {
        return 3;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    
    if (self.hasBegan) {
        if (section == 3) {
            return 2;
        }
        return 1;
    } else {
        if (section == 2) {
            return 2;
        }
        return 1;
    }

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    //
    [cell.textLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
        //
        switch (indexPath.section) {
            case 0: {
    
                HZLocalOrderStatusCell *localOrderStatusCell = [HZLocalOrderStatusCell cellWithTableView:tableView];
                localOrderStatusCell.localOrder = self.localOrder;
                localOrderStatusCell.delegate = self;
                return localOrderStatusCell;
            }
                break;
            case 1: {
                
//                if (indexPath.row == 0) {
    
                    HZLocalOrderStartTimeCell *localOrderStartTimeCell = [HZLocalOrderStartTimeCell cellWithTableView:tableView];
                    return localOrderStartTimeCell;
//                } else {
//                    cell.textLabel.text = self.localOrder.treatmentDateTime;
//                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//                    return cell;
//                }
    
            }
                break;
    
            case 2: {
    
                if (self.hasBegan) {
//                    if (indexPath.row == 0) {
//    
//                        HZRemoteExpertInforCell *remoteExpertInforCell = [HZRemoteExpertInforCell cellWithTableView:tableView];
//    
//                        return remoteExpertInforCell;
//                    } else {
                        HZRemoteExpertInforContentCell *remoteExpertInforContentCell = [HZRemoteExpertInforContentCell cellWithTableView:tableView];
                        remoteExpertInforContentCell.delegate = self;
                    
                        remoteExpertInforContentCell.myLocalOrder = self.localOrder;
                        NSLog(@"________1_________ert:%@", self.localOrder);
                    
                        //
//                        int offLineMsgCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"offLineMsgCount"] intValue];
                        int unReadMsgCount = [[[NSUserDefaults standardUserDefaults] objectForKey:kLocalUnReadCountStr] intValue];
                        
//                        NSLog(@"------1111112222222---%d------%d", offLineMsgCount, unReadMsgCount);
                        remoteExpertInforContentCell.unReadMsgCount = unReadMsgCount;

                        return remoteExpertInforContentCell;
    
//                    }
    
                } else {
    
                    if (indexPath.row == 0) {
                        HZLocalPatientInforCell *localPatientInforCell = [HZLocalPatientInforCell cellWithTableView:tableView];
                        localPatientInforCell.delegate = self;
                        return localPatientInforCell;
                    } else {
                        HZOrderPatientInforContentCell *localPatientInforContentCell = [HZOrderPatientInforContentCell cellWithTableView:tableView];
                        self.localOrder.isInOrderDetail = YES;
                        localPatientInforContentCell.order = self.localOrder;
                        localPatientInforContentCell.isInOrderDetail = YES;
                        return localPatientInforContentCell;
                    }
    
    
//                }
    
    
    
            }
    
    
            case 3: {
    
                if (indexPath.row == 0) {
                    HZLocalPatientInforCell *localPatientInforCell = [HZLocalPatientInforCell cellWithTableView:tableView];
                    localPatientInforCell.delegate = self;
                    localPatientInforCell.localOrder = self.localOrder;
                    return localPatientInforCell;
                } else {
                HZOrderPatientInforContentCell *localPatientInforContentCell = [HZOrderPatientInforContentCell cellWithTableView:tableView];
                self.localOrder.isInOrderDetail = YES;
                localPatientInforContentCell.order = self.localOrder;
                localPatientInforContentCell.isInOrderDetail = YES;
                return localPatientInforContentCell;
                }
            }
    
    
            }
    
    
        }
    
    return cell;
}

#pragma mark -- viewForHeaderInSection
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0 || section == 1) return 0;
    
    HZDashLine *dashLine = [[HZDashLine alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 1)];
    dashLine.backgroundColor = [UIColor whiteColor];
    return dashLine;
}

#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = 0;
    
        switch (indexPath.section) {
            case 0:
                cellHeight = [HZLocalOrderStatusCell cellHeightWithLocalOrder:self.localOrder];
                break;
            case 1:
//            {
    
//                if (indexPath.row == 0) {
                    cellHeight = kP(158);
//                } else {
//                    cellHeight = [HZLocalOrderStatusCell cellHeightWithLocalOrder:self.localOrder];
//                }
    
//            }
                break;
            case 2:
                if (self.hasBegan) {// 当前的
//                    if (indexPath.row == 0) {
//                        cellHeight = kP(72);
//                    } else {
                        cellHeight = 3 * kP(32) + kP(40) + kP(128);
//                    }
    
                } else {
                    if (indexPath.row == 0) {
                        return kP(72);
                    } else {
                        cellHeight = [HZOrderPatientInforContentCell cellHeightWithOrder:self.localOrder];
                    }
                }
                
                break;
                
            case 3:{
    
                if (indexPath.row == 0) {
                    return kP(72);
                } else {
                    cellHeight = [HZOrderPatientInforContentCell cellHeightWithOrder:self.localOrder];
                }
    
            }
                break;
        }
    return cellHeight;
}

#pragma mark -- UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 0.001;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(20);
    }
    return kP(0.1);
}

#pragma mark -- endOutpatient
- (void)endOutpatient:(UIButton *)btn {
    [self alertWithAlertStr:@"是否确认结束本次门诊" hasCancelAction:YES];
}


- (void)alertWithAlertStr:(NSString *)alertStr hasCancelAction:(BOOL)hasCancelAction {
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:alertStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (hasCancelAction == NO) return;
        
        [HZOrderHttpsTool endLocaOrderWithBookingOrderId:self.localOrder.bookingOrderId success:^(id  _Nullable responseObject) {
            if ([responseObject[@"state"] isEqual:@200]) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self alertWithAlertStr:[NSString stringWithFormat:@"结束订单失败:%@", responseObject[@"state"]] hasCancelAction:NO];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"---------结束订单失败");
        }];
    }];
    
    if (hasCancelAction) {
        UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        if (kCurrentSystemVersion >= 9.0) {
            [cancelAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        }
        [alertCtr addAction:cancelAlertAction];
    }
    
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
    }
    [alertCtr addAction:ensureAlertAction];
    
    [self presentViewController:alertCtr animated:YES completion:nil];
}
#pragma mark -- 进入Video
- (void)enterVideoRoom:(UIButton *)btn {
    HZRoomViewController *romeVCtr = [[HZRoomViewController alloc] init];
    romeVCtr.delegate = self;
    romeVCtr.videoProfile = AgoraRtc_VideoProfile_360P;
    romeVCtr.roomName = self.localOrder.bookingOrderId;
    romeVCtr.remoteOrder = self.localOrder;
    romeVCtr.isComeFromPodfileVCtr = NO;
    [self.navigationController pushViewController:romeVCtr animated:YES];
}

#pragma mark -- HZPatientInforCellDelegate
- (void)enterMorePatientInfor {

    HZOrderPatientInforViewController *patientInforVCtr = [HZOrderPatientInforViewController new];
    patientInforVCtr.basicOrder = self.localOrder;
    
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
}

#pragma mark -- HZDoctorInforCellDelegate
- (void)enterIMVCtr {
    [self pushToIMVCWithAnimated:YES];
}


- (void)pushToIMVCWithAnimated:(BOOL)animated{
    HZIMChatViewController *IMChatVCtr = [HZIMChatViewController new];
    IMChatVCtr.IMOrder = self.localOrder;
    
    [self.navigationController pushViewController:IMChatVCtr animated:animated];
}

#pragma mark -- pay

- (void)enterToPay {
    
    if (self.alertPayView) {
        [self.alertPayView setMyTimerStart];
    }
    
    NSLog(@"enterToPay");
    
    NSDate *senddate = [NSDate date];
    NSInteger currentTime = (long)[senddate timeIntervalSince1970];
    
    NSInteger weChatOldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"WeChatQRImageOldTime%@", self.localOrder.bookingOrderId]] integerValue];
    NSInteger AliOldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"AliQRImageOldTime%@", self.localOrder.bookingOrderId]] integerValue];
    
    
    NSLog(@"weChatOldTime:%ld------AliOldTime:%ld--------currentTime:%ld------payOrder:%@------ currentTime - weChatOldTime:%ld------currentTime - AliOldTime:%ld", weChatOldTime, AliOldTime, currentTime, self.payOrder, currentTime -  weChatOldTime, currentTime - AliOldTime);
    
    if (currentTime - weChatOldTime > 2 * 3600 || currentTime - AliOldTime > 2 * 3600 ) {
        [self getPayParamsDictToImageData];
        
        NSLog(@"--qwweeee---------------------");
        //        self.alertPayView.isShowWeChat = YES;
        return;
    } else if (self.payOrder == nil) {
        NSLog(@"---------------uio-----------------");
        [self getPayParamsDict];
        
        return;
    }
    
    NSLog(@"---------------rty-----------------");
    
//    if (currentTime - weChatOldTime > 2 * 3600 || currentTime - AliOldTime > 2 * 3600 || weChatOldTime == 0 || AliOldTime == 0) {
//        [self getPayParamsDict];
//        
//        //        self.alertPayView.isShowWeChat = YES;
//        return;
//    }
    
//    //
//    [self.alertPayView removeFromSuperview];
//    [self.alertPayView.timer invalidate];
//    self.alertPayView.timer = nil;
//    
//    HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:nil];
//    alertPayView.delegate = self;
//    [self.navigationController.view addSubview:alertPayView];
//    self.alertPayView = alertPayView;
    
    [self setMyAlertPayViewWithPayOrder:self.payOrder];
}

- (void)setMyAlertPayViewWithPayOrder:(HZPayOrder *)payOrder {
    //
    [self.alertPayView removeFromSuperview];
    [self.alertPayView.timer invalidate];
    self.alertPayView.timer = nil;
    
    HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:payOrder];
    alertPayView.delegate = self;
    alertPayView.isFromAppointSuccess = NO;
    [self.navigationController.view addSubview:alertPayView];
    self.alertPayView = alertPayView;
    
    //
    self.alertPayView.hidden = NO;
}

#pragma mark -- HZAlertPayViewDelegate

- (void)refreshOrderStatus {
    NSLog(@"refreshOrderStatus");
    
    NSString *userId = [self.localOrder.orderType isEqualToString:@"01"] ? self.localOrder.localDocotorOpenid : self.localOrder.expertOpenid;
    //
    [HZOrderHttpsTool getOrderPayDetailWithOrderId:self.localOrder.bookingOrderId openOrderId:userId success:^(id responseObject) {
        NSLog(@"---------ssss-------:%@", responseObject);
        if ([responseObject[@"state"] isEqual:@200]) {
            
            NSDictionary *resultsDict = responseObject[@"results"];
            NSDictionary *bookingOrderDict = resultsDict[@"bookingOrder"];
            
            //
            HZOrder *localOrder = [HZOrder mj_objectWithKeyValues:bookingOrderDict];
            localOrder.orderType = @"01";
            self.localOrder = [HZOrder mj_objectWithKeyValues:[localOrder propertyValueDict]];
            //
            [self.localOrderDetailTableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------rrrr----------:%@", error);
    }];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    //
    [self.alertPayView.timer invalidate];
    self.alertPayView.timer = nil;
    //
    //    self.timeCount = 0;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
