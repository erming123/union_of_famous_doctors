//
//  HZLocalOrderDetailViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/14.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@interface HZLocalOrderDetailViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *localOrder;
@property (nonatomic, copy) NSString *unReadMsgCountStr;
- (void)pushToIMVCWithAnimated:(BOOL)animation;
@end
