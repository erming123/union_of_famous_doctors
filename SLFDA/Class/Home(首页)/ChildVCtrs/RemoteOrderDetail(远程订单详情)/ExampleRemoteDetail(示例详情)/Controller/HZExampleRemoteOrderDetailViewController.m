//
//  HZRemoteOrderDetailViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZExampleRemoteOrderDetailViewController.h"
#import "HZExampleRemoteOrderStatusCell.h"
#import "HZExampleRemoteOrderStartTimeCell.h"
#import "HZExampleLocalDoctorInforContentCell.h"
#import "HZLocalPatientInforCell.h"
//#import "HZExampleRemotePatientInforCell.h"
#import "HZOrderPatientInforContentCell.h"
//#import "HZExampleRemotePatientInforContentCell.h"
#import "MJRefresh.h"
#import "HZOrder.h"
#import "HZOrderPatientInforViewController.h"
#import "HZUser.h"
#import "HZIMChatViewController.h"
#import "HZCheckListTool.h"

//
#import "HZRoomViewController.h"
#import "HZSettingViewController.h"
#import "HZMessageDB.h"
//
#import "HZDashLine.h"
//
#define kRemoteUnReadCountStr @"remoteUnReadCountStr"
@interface HZExampleRemoteOrderDetailViewController ()<UITableViewDataSource, UITableViewDelegate,HZExampleLocalDoctorInforContentCellDelegate, HZSettingViewControllerDelegate, HZLocalPatientInforCellDelegate>
// HZExampleRemotePatientInforCellDelegate
//
@property (assign, nonatomic) AgoraRtcVideoProfile videoProfile;

//** <#注释#>*/
@property (nonatomic, strong) HZMessageDB *messageDB;

//@property (nonatomic, copy) NSString *unReadCountStr;
//** <#注释#>*/
@property (nonatomic, strong) UIView *btnBgView;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *exampleRemoteOrder;
@end

@implementation HZExampleRemoteOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //1.
    self.navigationItem.title = @"订单详情";

    //2.
    HZTableView *remoteOrderDetailTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    remoteOrderDetailTableView.dataSource = self;
    remoteOrderDetailTableView.delegate = self;
    remoteOrderDetailTableView.showsVerticalScrollIndicator = NO;
    remoteOrderDetailTableView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7" alpha:1.0];
    remoteOrderDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:remoteOrderDetailTableView];
    self.remoteOrderDetailTableView = remoteOrderDetailTableView;

    //1.
//    [self setUpVideoBtn];
    
    //
    HZOrder *exampleRemoteOrder = [HZOrder new];
    exampleRemoteOrder.remoteAccount = @"iOSTestRemoteAccount:example123456";
    exampleRemoteOrder.localAccount = @"iOSTestLocalAccount:example123456";
    exampleRemoteOrder.bookingOrderId = @"iOSTestBookingOrderId:example123456";
    exampleRemoteOrder.expertOpenid = @"iOSTestExpertOpenid:example123456";
    exampleRemoteOrder.orderType = @"02";
//    exampleRemoteOrder.localAccount = @"";
    exampleRemoteOrder.localDoctorName = @"夏惠民";
    exampleRemoteOrder.outpatientName = @"陈某某(男, 38岁)";
    exampleRemoteOrder.icd10Value = @"系膜增生性肾炎";
    exampleRemoteOrder.sicknessRemark = @"面部及两下肢浮肿2个月，伴腰膝酸软，体重增加，纳呆，恶心及重度蛋白尿。无血尿及尿路感染刺激征。发病前一月内无发热、咽痛或关节痛史。收缩期血压稍增高，心肺正常，肝脾不大；轻度贫血，尿素氮及肌酐不增加，总胆固醇显著增多";
    exampleRemoteOrder.isExampleOrder = YES;
    HZIllnessDataImg *illnessDataImg = [HZIllnessDataImg new];
    illnessDataImg.reportImageUri = @"9eb95dd6bab349f3b16642fe4e36f0de.png";
    exampleRemoteOrder.laboratoryReports = @[illnessDataImg];
    HZIllnessDataImg *illnessDataImg1 = [HZIllnessDataImg new];
    illnessDataImg1.reportImageUri = @"7f660bef8f694e0991632d532a65e014.png";
    exampleRemoteOrder.examReports = @[illnessDataImg1];
    exampleRemoteOrder.isInOrderDetail = YES;
    self.exampleRemoteOrder = exampleRemoteOrder;
}


- (void)enterVideoRoom:(UIButton *)btn {
    NSLog(@"--------dfsfsdfsd--3333");
}
#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 3) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    [cell.textLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    //
    switch (indexPath.section) {
        case 0: {
            
            HZExampleRemoteOrderStatusCell *remoteOrderStatusCell = [HZExampleRemoteOrderStatusCell cellWithTableView:tableView];
            return remoteOrderStatusCell;
        }
            break;
        case 1: {
                HZExampleRemoteOrderStartTimeCell *remoteOrderStartTimeCell = [HZExampleRemoteOrderStartTimeCell cellWithTableView:tableView];
                return remoteOrderStartTimeCell;
        }
            break;
        case 2: {
                HZExampleLocalDoctorInforContentCell *doctorInforContentCell = [HZExampleLocalDoctorInforContentCell cellWithTableView:tableView];
                doctorInforContentCell.delegate = self;
                //
                return doctorInforContentCell;
        }
        case 3: {
            
            if (indexPath.row == 0) {
                
                HZLocalPatientInforCell *localPatientInforCell = [HZLocalPatientInforCell cellWithTableView:tableView];
                localPatientInforCell.delegate = self;
                return localPatientInforCell;
            } else {
                
                HZOrderPatientInforContentCell *localPatientInforContentCell = [HZOrderPatientInforContentCell cellWithTableView:tableView];
                localPatientInforContentCell.order = self.exampleRemoteOrder;
                localPatientInforContentCell.isInOrderDetail = YES;
                return localPatientInforContentCell;
            }
            
        }
            
    }
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0 || section == 1) return 0;
    
    HZDashLine *dashLine = [[HZDashLine alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 1)];
    dashLine.backgroundColor = [UIColor whiteColor];
    return dashLine;
}
#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CGFloat cellHeight = 0.0;
    switch (indexPath.section) {
        case 0:
            cellHeight = [HZExampleRemoteOrderStatusCell cellHeightWithRemoteOrder:nil];
            break;
        case 1:
            cellHeight = kP(158);
            break;
        case 2:
            return kP(260);
            break;
        case 3: {
            
            if (indexPath.row == 0) {
                cellHeight = kP(72);
            } else {
                cellHeight = [HZOrderPatientInforContentCell cellHeightWithOrder:self.exampleRemoteOrder];
            }
            
        }
            break;
    }
    return cellHeight;
}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(0.01);
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return kP(30);
    }
    return kP(0.01);
}


#pragma mark -- HZRemotePatientInforCellDelegate
- (void)enterMorePatientInfor {
    HZOrderPatientInforViewController *patientInforVCtr = [HZOrderPatientInforViewController new];
    patientInforVCtr.basicOrder = self.exampleRemoteOrder;
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
}

#pragma mark -- HZLocalDoctorInforContentCellDelegate
// --- 进入IM页面-----
- (void)enterExampleIMVCtr {
    
    [self pushToExampleIMVCWithAnimated:YES];
}



- (void)pushToExampleIMVCWithAnimated:(BOOL)animation {
    //
    HZIMChatViewController *IMChatVCtr = [HZIMChatViewController new];
    IMChatVCtr.IMOrder = self.exampleRemoteOrder;
    [self.navigationController pushViewController:IMChatVCtr animated:animation];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
