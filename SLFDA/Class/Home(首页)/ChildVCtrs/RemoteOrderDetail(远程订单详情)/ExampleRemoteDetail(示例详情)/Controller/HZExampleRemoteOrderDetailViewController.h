//
//  HZExampleRemoteOrderDetailViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZExampleRemoteOrderDetailViewController : HZViewController
@property (nonatomic, assign) NSUInteger commingNewsCount;
@property (nonatomic, weak) HZTableView *remoteOrderDetailTableView;
//@property (nonatomic, assign) int remoteUnReadCount;
- (void)pushToIMVCWithAnimated:(BOOL)animation;
@end
