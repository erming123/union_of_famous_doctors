//
//  HZExampleRemotePatientInforCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@protocol HZExampleRemotePatientInforCellDelegate <NSObject>
- (void)enterExampleMorePatientInfor;
@end
@interface HZExampleRemotePatientInforCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
+ (CGFloat)cellHeightWithRemoteOrder:(HZOrder *)order;
@property (nonatomic, weak) id<HZExampleRemotePatientInforCellDelegate>delegate;
@end
