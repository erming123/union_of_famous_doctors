//
//  HZExampleLocalDoctorInforContentCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HZExampleLocalDoctorInforContentCellDelegate <NSObject>
- (void)enterExampleIMVCtr;
@end
@interface HZExampleLocalDoctorInforContentCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) id<HZExampleLocalDoctorInforContentCellDelegate>delegate;
@end
