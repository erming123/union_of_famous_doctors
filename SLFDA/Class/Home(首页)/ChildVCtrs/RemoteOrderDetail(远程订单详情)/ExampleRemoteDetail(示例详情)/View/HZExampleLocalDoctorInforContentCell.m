//
//  HZLocalDoctorInforContentCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZExampleLocalDoctorInforContentCell.h"
#import "HZCommingNewsTon.h"
#import "HZOrder.h"
#import "HZUserTool.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZExampleLocalDoctorInforContentCell ()
@property (nonatomic, strong) UIImageView *doctorLogolImageView;
@property (nonatomic, strong) UILabel *doctorInforLabel;
@property (nonatomic, strong) UIImageView *doctorAvatarImageView;
@property (nonatomic, strong) UILabel *doctorNameLabel;
@property (nonatomic, strong) UILabel *doctorJobLabel;
@property (nonatomic, strong) UILabel *doctorSubjectLabel;
@property (nonatomic, strong) UIImageView *IMImageView;
@property (nonatomic, strong) UIButton *IMBtn;
@property (nonatomic, strong) UILabel *newsCountLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZExampleLocalDoctorInforContentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    //1.
    UIImageView *doctorLogolImageView = [[UIImageView alloc] init];
    doctorLogolImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:doctorLogolImageView];
    self.doctorLogolImageView = doctorLogolImageView;
    
    //2.
    UILabel *doctorInforLabel = [[UILabel alloc] init];
    [doctorInforLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    [self addSubview:doctorInforLabel];
    self.doctorInforLabel = doctorInforLabel;
    
    //
    UIImageView *doctorAvatarImageView = [[UIImageView alloc] init];
    doctorAvatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    doctorAvatarImageView.backgroundColor = [UIColor greenColor];
    [self addSubview:doctorAvatarImageView];
    self.doctorAvatarImageView = doctorAvatarImageView;
    
    UILabel *doctorNameLabel = [[UILabel alloc] init];
    [doctorNameLabel setTextFont:kP(32) textColor:@"#666666" alpha:1.0];
    [self addSubview:doctorNameLabel];
    self.doctorNameLabel = doctorNameLabel;
    
    UILabel *doctorJobLabel = [[UILabel alloc] init];
    [doctorJobLabel setTextFont:kP(28) textColor:@"#666666" alpha:1.0];
    [doctorJobLabel sizeToFit];
    [self addSubview:doctorJobLabel];
    self.doctorJobLabel = doctorJobLabel;
    
    UILabel *doctorSubjectLabel = [[UILabel alloc] init];
    [doctorSubjectLabel setTextFont:kP(28) textColor:@"#666666" alpha:1.0];
    [doctorSubjectLabel sizeToFit];
    [self addSubview:doctorSubjectLabel];
    self.doctorSubjectLabel = doctorSubjectLabel;
    
    UIButton *IMBtn = [[UIButton alloc] init];
    [IMBtn setTitleColor:[UIColor colorWithHexString:@"2dbed8" alpha:1.0] forState:UIControlStateNormal];
    IMBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
    [IMBtn setTitle:@"图文交流" forState:UIControlStateNormal];
    [IMBtn setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [IMBtn sizeToFit];
    [IMBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:kP(20)];
    [IMBtn addTarget:self action:@selector(makeDelegateEnterIMVCtr:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:IMBtn];
    self.IMBtn = IMBtn;
    
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeDelegateEnterIMVCtr:)];
    //    UIImageView *IMImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithOriginalName:@"IM"]];
    //    [IMImageView addGestureRecognizer:tap];
    //    IMImageView.userInteractionEnabled = YES
    //    [self addSubview:IMImageView];
    //    self.IMImageView = IMImageView;
    
    
    
    UILabel *newsCountLabel = [[UILabel alloc] init];
    newsCountLabel.backgroundColor = [UIColor redColor];
    newsCountLabel.textColor = [UIColor whiteColor];
    newsCountLabel.font = [UIFont systemFontOfSize:kP(26)];
    newsCountLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:newsCountLabel];
    self.newsCountLabel = newsCountLabel;
    
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    //
    [self setUpSubViewsContent];
    
    
    //
    [self.doctorNameLabel sizeToFit];
    [self.doctorJobLabel sizeToFit];
    [self.doctorSubjectLabel sizeToFit];
    [self.newsCountLabel sizeToFit];
    
    //
    // 图标
    CGFloat doctorLogolImageViewWH = kP(40);
    CGFloat doctorLogolImageViewXY = kP(32);
    self.doctorLogolImageView.frame = CGRectMake(doctorLogolImageViewXY, doctorLogolImageViewXY, doctorLogolImageViewWH, doctorLogolImageViewWH);
    
    
    // 文本： 陪诊医生
    [self.doctorInforLabel sizeToFit];
    self.doctorInforLabel.x = self.doctorLogolImageView.right + kP(20);
    self.doctorInforLabel.y = self.doctorLogolImageView.centerY - self.doctorInforLabel.height * 0.5;
    
    // 头像
    CGFloat doctorAvatarImageViewWH = kP(128);
    CGFloat doctorAvatarImageViewX = self.doctorInforLabel.x;
    CGFloat doctorAvatarImageViewY = self.doctorInforLabel.bottom + kP(28);
    self.doctorAvatarImageView.frame = CGRectMake(doctorAvatarImageViewX, doctorAvatarImageViewY, doctorAvatarImageViewWH, doctorAvatarImageViewWH);
    self.doctorAvatarImageView.layer.borderWidth = kP(1);
    self.doctorAvatarImageView.layer.borderColor = [UIColor colorWithHexString:@"#D5D5D5"].CGColor;
    self.doctorAvatarImageView.layer.cornerRadius = kP(65);
    self.doctorAvatarImageView.clipsToBounds = YES;
    
    // 姓名
    self.doctorNameLabel.x = self.doctorAvatarImageView.right + kP(40);
    self.doctorNameLabel.y = self.doctorAvatarImageView.centerY - self.doctorNameLabel.height - kP(5);
    
    // 工作
    self.doctorJobLabel.x = [NSString isBlankString:self.doctorNameLabel.text] ? self.doctorAvatarImageView.right + kP(40) : self.doctorNameLabel.right + kP(20);
    self.doctorJobLabel.y = self.doctorNameLabel.y;
    
    // 科室
    self.doctorSubjectLabel.x = self.doctorNameLabel.x;
    self.doctorSubjectLabel.y = self.doctorAvatarImageView.centerY + kP(15);
    
    //
    self.IMBtn.x = self.width - self.IMBtn.width - kP(32);
    self.IMBtn.y = self.doctorInforLabel.centerY - self.IMBtn.height * 0.5;
    
    
    
    //
    //  newsLabel
    CGSize newsCountSize = [self.newsCountLabel.text getTextSizeWithMaxWidth:kP(400) textFontSize:kP(32)];
    CGFloat WH = MAX(newsCountSize.width, newsCountSize.height);
    self.newsCountLabel.width = self.newsCountLabel.height = WH;
    self.newsCountLabel.layer.cornerRadius = WH * 0.5;
    self.newsCountLabel.layer.masksToBounds = YES;
    self.newsCountLabel.x = self.IMBtn.right - self.newsCountLabel.width * 0.8;
    self.newsCountLabel.y = self.IMBtn.y - self.newsCountLabel.height * 0.3;
    
}

- (void)setUpSubViewsContent {
    
    //
    self.doctorLogolImageView.image = [UIImage imageNamed:@"localDoctor"];
    self.doctorInforLabel.text = @"陪诊医生";
    
    self.doctorAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
        
    //    self.doctorAvatarImageView.image = [UIImage imageNamed:@"expertPlaceImg"];
    self.doctorNameLabel.text = @"夏惠民";
    self.doctorJobLabel.text = @"主任医师";
    self.doctorSubjectLabel.text = @"内分泌科";
    
    //
    self.IMBtn.hidden = NO;
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"localDoctorInforContentCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}

#pragma mark -- makeDelegateEnterIMVCtr

- (void)makeDelegateEnterIMVCtr:(UIButton *)IMBtn {
    
    self.newsCountLabel.hidden = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(enterExampleIMVCtr)]) {
        [self.delegate enterExampleIMVCtr];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
