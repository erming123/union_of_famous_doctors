//
//  HZRemoteOrderStartTimeCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZExampleRemoteOrderStartTimeCell.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZExampleRemoteOrderStartTimeCell ()
@property (nonatomic, strong) UIImageView *orderStartImageView;
@property (nonatomic, strong) UILabel *orderStartLabel;
@property (nonatomic, strong) UILabel *orderStartContentLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZExampleRemoteOrderStartTimeCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UIImageView *orderStartImageView = [[UIImageView alloc] init];
    orderStartImageView.image = [UIImage imageWithOriginalName:@"startTime"];
    [self addSubview:orderStartImageView];
    self.orderStartImageView = orderStartImageView;
    
    UILabel *orderStartLabel = [[UILabel alloc] init];
    orderStartLabel.text = @"开诊时间";
    [orderStartLabel setTextFont:kP(32) textColor:@"#414141" alpha:1.0];
    [self addSubview:orderStartLabel];
    self.orderStartLabel = orderStartLabel;
    
    UILabel *orderStartContentLabel = [[UILabel alloc] init];
    orderStartContentLabel.text = @"2017-12-01 13:30";
    [orderStartContentLabel setTextFont:kP(32) textColor:@"#666666" alpha:1.0];
    [self addSubview:orderStartContentLabel];
    self.orderStartContentLabel = orderStartContentLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //
    CGFloat orderStartImageViewWH = kP(40);
    CGFloat orderStartImageViewX = kP(32);
    CGFloat orderStartImageViewY = kP(28);
    
    self.orderStartImageView.frame = CGRectMake(orderStartImageViewX, orderStartImageViewY, orderStartImageViewWH, orderStartImageViewWH);
    
    //
    [self.orderStartLabel sizeToFit];
    self.orderStartLabel.x = self.orderStartImageView.right + kP(20);
    self.orderStartLabel.centerY = self.orderStartImageView.centerY;
    
    //
    [self.orderStartContentLabel sizeToFit];
    self.orderStartContentLabel.x = CGRectGetMinX(self.orderStartLabel.frame);
    self.orderStartContentLabel.y = self.orderStartLabel.bottom+kP(22);
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"remoteOrderStartTimeCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
