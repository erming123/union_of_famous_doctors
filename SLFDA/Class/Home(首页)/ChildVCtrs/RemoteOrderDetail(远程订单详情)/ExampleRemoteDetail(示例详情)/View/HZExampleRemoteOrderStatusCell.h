//
//  HZExampleRemoteOrderStatusCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/6/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@interface HZExampleRemoteOrderStatusCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
+ (CGFloat)cellHeightWithRemoteOrder:(HZOrder *)order;
@end
