//
//  HZRemotePatientInforCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@protocol HZRemotePatientInforCellDelegate <NSObject>

- (void)enterMorePatientInfor;

@end
@interface HZRemotePatientInforCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) id <HZRemotePatientInforCellDelegate> delegate;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *remoteOrder;
@end
