//
//  HZRemoteOrderStatusCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;

@protocol HZRemoteOrderStatusCellDelegate <NSObject>
- (void)enterToPay;
@end

@interface HZRemoteOrderStatusCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *remoteOrder;
+ (CGFloat)cellHeightWithRemoteOrder:(HZOrder *)remoteOrder;
@property (nonatomic, weak) id<HZRemoteOrderStatusCellDelegate>delegate;
@end
