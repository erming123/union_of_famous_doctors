//
//  HZRemoteOrderStartTimeCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZRemoteOrderStartTimeCell.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZRemoteOrderStartTimeCell ()
@property (nonatomic, strong) UIImageView *orderStartImageView;
@property (nonatomic, strong) UILabel *orderStartLabel;
@end
NS_ASSUME_NONNULL_END
@implementation HZRemoteOrderStartTimeCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    UIImageView *orderStartImageView = [[UIImageView alloc] init];
    orderStartImageView.image = [UIImage imageWithOriginalName:@"startTime"];
    [self addSubview:orderStartImageView];
    self.orderStartImageView = orderStartImageView;
    
    UILabel *orderStartLabel = [[UILabel alloc] init];
    orderStartLabel.text = @"开诊时间";
    [orderStartLabel setTextFont:kP(32) textColor:@"#4A4A4A" alpha:1.0];
    [self addSubview:orderStartLabel];
    self.orderStartLabel = orderStartLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //
    CGFloat orderStartImageViewWH = kP(40);
    CGFloat orderStartImageViewX = kP(28);
    CGFloat orderStartImageViewY = self.height * 0.5 - orderStartImageViewWH * 0.5;
    
    self.orderStartImageView.frame = CGRectMake(orderStartImageViewX, orderStartImageViewY, orderStartImageViewWH, orderStartImageViewWH);
    
    //
    [self.orderStartLabel sizeToFit];
    self.orderStartLabel.x = self.orderStartImageView.right + kP(20);
    self.orderStartLabel.centerY = self.orderStartImageView.centerY;
    
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"remoteOrderStartTimeCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}
@end
