//
//  HZRemoteOrderDetailViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/11.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZOrder;
@interface HZRemoteOrderDetailViewController : HZViewController
@property (nonatomic, assign) NSUInteger commingNewsCount;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *remoteOrder;
@property (nonatomic, strong) UITableView *remoteOrderDetailTableView;
//@property (nonatomic, assign) int remoteUnReadCount;
- (void)pushToIMVCWithAnimated:(BOOL)animation;
@end
