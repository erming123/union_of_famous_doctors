//
//  HZSettingViewController.m
//  HZAgoraDemo
//
//  Created by 季怀斌 on 2016/12/14.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZSettingViewController.h"
#import "ProfileCell.h"
#import <AgoraRtcEngineKit/AgoraRtcEngineKit.h>
@interface HZSettingViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) HZTableView *profileTableView;
@property (strong, nonatomic) NSArray *profiles;
@end

@implementation HZSettingViewController


- (void)viewDidLoad {
    
    self.view.backgroundColor = [UIColor yellowColor];
    HZTableView *profileTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    profileTableView.delegate = self;
    profileTableView.dataSource = self;
    [self.view addSubview:profileTableView];
    self.profileTableView = profileTableView;
    
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"OK" style:UIBarButtonItemStylePlain target:self action:@selector(doConfirmPressed:)];
    
    self.profileTableView.tableFooterView = [UIView new];
}
- (NSArray *)profiles {
    if (!_profiles) {
        _profiles = @[@(AgoraRtc_VideoProfile_120P),
                      @(AgoraRtc_VideoProfile_180P),
                      @(AgoraRtc_VideoProfile_240P),
                      @(AgoraRtc_VideoProfile_360P),
                      @(AgoraRtc_VideoProfile_480P),
                      @(AgoraRtc_VideoProfile_720P)];
    }
    return _profiles;
}


- (void)doConfirmPressed:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(settingsVC:didSelectProfile:)]) {
        [self.delegate settingsVC:self didSelectProfile:self.videoProfile];
    }
}
- (void)setVideoProfile:(AgoraRtcVideoProfile)videoProfile {
    _videoProfile = videoProfile;
    [self.profileTableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.profiles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProfileCell *cell = [ProfileCell cellWithTableView:tableView];
    AgoraRtcVideoProfile selectedProfile = [self.profiles[indexPath.row] integerValue];
    
    
    [cell updateWithProfile:selectedProfile isSelected:(selectedProfile == self.videoProfile)];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AgoraRtcVideoProfile selectedProfile = [self.profiles[indexPath.row] integerValue];
    NSLog(@"--------------------%ld", selectedProfile);
    self.videoProfile = selectedProfile;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
