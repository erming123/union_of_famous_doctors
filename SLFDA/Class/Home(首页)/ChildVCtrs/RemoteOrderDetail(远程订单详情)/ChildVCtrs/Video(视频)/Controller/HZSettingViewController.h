//
//  HZSettingViewController.h
//  HZAgoraDemo
//
//  Created by 季怀斌 on 2016/12/14.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AgoraRtcEngineKit/AgoraRtcEngineKit.h>

@class HZSettingViewController;

@protocol HZSettingViewControllerDelegate <NSObject>

- (void)settingsVC:(HZSettingViewController *)settingsVC didSelectProfile:(AgoraRtcVideoProfile)profile;

@end
@interface HZSettingViewController : HZViewController
@property (assign, nonatomic) AgoraRtcVideoProfile videoProfile;
@property (weak, nonatomic) id<HZSettingViewControllerDelegate> delegate;
@end
