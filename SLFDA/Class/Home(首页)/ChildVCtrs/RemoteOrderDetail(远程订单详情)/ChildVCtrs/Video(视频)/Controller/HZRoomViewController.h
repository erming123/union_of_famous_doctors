//
//  HZRoomViewController.h
//  HZAgoraDemo
//
//  Created by 季怀斌 on 2016/12/14.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AgoraRtcEngineKit/AgoraRtcEngineKit.h>

@class HZRoomViewController, HZOrder;

@protocol HZRoomViewControllerDelegate <NSObject>

- (void)roomVCNeedClose:(HZRoomViewController *)roomVC;

@end
@interface HZRoomViewController : HZViewController
@property (copy, nonatomic) NSString *roomName;
@property (assign, nonatomic) AgoraRtcVideoProfile videoProfile;
@property (weak, nonatomic) id<HZRoomViewControllerDelegate> delegate;
//** <#注释#>*/
@property (nonatomic, strong) HZOrder *remoteOrder;
@property (nonatomic, assign) BOOL isComeFromPodfileVCtr;
@end
