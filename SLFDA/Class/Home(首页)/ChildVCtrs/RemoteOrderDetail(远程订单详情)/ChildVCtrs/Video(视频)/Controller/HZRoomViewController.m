//
//  HZRoomViewController.m
//  HZAgoraDemo
//
//  Created by 季怀斌 on 2016/12/14.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZRoomViewController.h"
#import "VideoSession.h"
#import "VideoViewLayouter.h"
#import "KeyCenter.h"
#import "HZIMChatViewController.h"
#import "HZOrderPatientInforViewController.h"
#import "HZHttpsTool.h"
#import "HZOrder.h"
//
#import <AVFoundation/AVFoundation.h>
@interface HZRoomViewController ()
@property (weak, nonatomic) UIView *containerView;
@property (copy, nonatomic) NSArray *flowViews;
//@property (strong, nonatomic) UILabel *roomNameLabel;// 房间名字

@property (weak, nonatomic) UIView *controlView;

@property (weak, nonatomic) UIButton *muteVideoButton;// 视频挂断按钮
//** <#注释#>*/
@property (nonatomic, weak) UILabel *alertLabel;
//
@property (nonatomic, weak) UIButton *messageBtn;// IM按钮
//** <#注释#>*/
@property (nonatomic, weak)  UILabel *messageLabel;
@property (weak, nonatomic) UIButton *muteAudioButton;// 暂停
@property (weak, nonatomic) UIButton *cameraButton;// 摄像头反转
@property (weak, nonatomic) UIButton *patientInforButton;// 患者资料按钮
//** <#注释#>*/
@property (nonatomic, weak) UILabel *patientInforLabel;

@property (strong, nonatomic) UITapGestureRecognizer *backgroundTap;
@property (strong, nonatomic) UITapGestureRecognizer *backgroundDoubleTap;


//------------------------------

@property (strong, nonatomic) AgoraRtcEngineKit *agoraKit;
@property (strong, nonatomic) NSMutableArray<VideoSession *> *videoSessions;
@property (strong, nonatomic) VideoSession *doubleClickFullSession;
@property (strong, nonatomic) VideoViewLayouter *videoViewLayouter;

@property (assign, nonatomic) BOOL shouldHideFlowViews;
@property (assign, nonatomic) BOOL audioMuted;
@property (assign, nonatomic) BOOL videoMuted;
@property (assign, nonatomic) BOOL speakerEnabled;
@end

@implementation HZRoomViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
    [self setUpSubViews];
    self.videoSessions = [[NSMutableArray alloc] init];
    [self loadAgoraKit];
    //相机和麦克风权限
    AVAuthorizationStatus cameraAuthStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    AVAuthorizationStatus voiceAuthStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
//        if (cameraAuthStatus == AVAuthorizationStatusRestricted || cameraAuthStatus ==AVAuthorizationStatusDenied || voiceAuthStatus == AVAuthorizationStatusNotDetermined || voiceAuthStatus == AVAuthorizationStatusDenied) {
//            void(^block)() = authorizationsSettingBlock();
//            showMessage(@"您麦克风或相机权限未打开,是否打开?", self, block);
//        }
    if (cameraAuthStatus == AVAuthorizationStatusRestricted || cameraAuthStatus ==AVAuthorizationStatusDenied  || voiceAuthStatus == AVAuthorizationStatusDenied) {
        void(^block)() = authorizationsSettingBlock();
        showMessage(@"您麦克风或相机权限未打开,是否打开?", self, block);
    }
    
    
}





- (void)setRemoteOrder:(HZOrder *)remoteOrder {
    _remoteOrder = remoteOrder;
}

- (void)setShouldHideFlowViews:(BOOL)shouldHideFlowViews {
    _shouldHideFlowViews = shouldHideFlowViews;
    if (self.flowViews.count) {
        for (UIView *view in self.flowViews) {
            view.hidden = shouldHideFlowViews;
        }
    }
}

- (void)setDoubleClickFullSession:(VideoSession *)doubleClickFullSession {
    _doubleClickFullSession = doubleClickFullSession;
    if (self.videoSessions.count >= 3) {
        [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:YES];
    }
}


- (void)setIsComeFromPodfileVCtr:(BOOL)isComeFromPodfileVCtr {
    _isComeFromPodfileVCtr = isComeFromPodfileVCtr;
}

- (VideoViewLayouter *)videoViewLayouter {
    if (!_videoViewLayouter) {
        _videoViewLayouter = [[VideoViewLayouter alloc] init];
    }
    return _videoViewLayouter;
}

- (void)setAudioMuted:(BOOL)audioMuted {
    _audioMuted = audioMuted;
    [self.muteAudioButton setImage:[UIImage imageNamed:(audioMuted ? @"btn_mute_blue" : @"btn_mute")] forState:UIControlStateNormal];
    [self.agoraKit muteLocalAudioStream:audioMuted];
}

- (void)setVideoMuted:(BOOL)videoMuted {
    _videoMuted = videoMuted;
    [self.muteVideoButton setImage:[UIImage imageNamed:(videoMuted ? @"btn_video" : @"btn_voice")] forState:UIControlStateNormal];
    self.cameraButton.hidden = videoMuted;
    self.patientInforButton.hidden = !videoMuted;
    
    [self.agoraKit muteLocalVideoStream:videoMuted];
    
    [self setVideoMuted:videoMuted forUid:0];
    [self updateSelfViewVisiable];
}


- (void)setSpeakerEnabled:(BOOL)speakerEnabled {
    //_speakerEnabled = speakerEnabled;
    // [self.patientInforButton setImage:[UIImage imageNamed:(speakerEnabled ? @"btn_speaker_blue" : @"btn_speaker")] forState:UIControlStateNormal];
    //[self.patientInforButton setImage:[UIImage imageNamed:(speakerEnabled ? @"btn_speaker" : @"btn_speaker_blue")] forState:UIControlStateHighlighted];
    
    //[self.agoraKit setEnableSpeakerphone:speakerEnabled];
    
    
}



//
- (void)setUpSubViews {
    
    UIView *containerView = [[UIView alloc] initWithFrame:self.view.bounds];
//    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, -kP(200), HZScreenW, HZScreenH)];
    [self.view addSubview:containerView];
    self.containerView = containerView;
    
    //
    //UILabel *roomNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 100, 50)];
    //roomNameLabel.x = self.view.width * 0.5 - roomNameLabel.width * 0.5;
    //roomNameLabel.backgroundColor = [UIColor yellowColor];
    //[self.view addSubview:roomNameLabel];
    
    //
    UIView *controlView = [[UIView alloc] initWithFrame:CGRectMake(kP(0), self.view.height - kP(300), self.view.width, kP(210))];
//    controlView.backgroundColor = [UIColor greenColor];
    //    controlView.alpha = 0.3;
    [self.view addSubview:controlView];
    self.controlView = controlView;
    
    // 摄像头切换
    CGFloat cameraButtonX = kP(10);
    CGFloat cameraButtonY = kP(20);
    CGFloat cameraButtonWH = kP(164);
    UIButton *cameraButton = [[UIButton alloc] initWithFrame:CGRectMake(cameraButtonX, cameraButtonY, cameraButtonWH, cameraButtonWH)];
    //cameraButton.backgroundColor = [UIColor redColor];
    //[cameraButton setTitle:@"反转" forState:UIControlStateNormal];
    [cameraButton setImage:[UIImage imageNamed:@"btn_cutaways"] forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(doCameraPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cameraButton];
    self.cameraButton = cameraButton;
    
    
    
    
    //
    CGFloat alertLabelW = kP(400);
    CGFloat alertLabelH = kP(70);
    CGFloat alertLabelX = self.view.width * 0.5 - alertLabelW * 0.5;
    CGFloat alertLabelY = self.cameraButton.centerY - alertLabelH * 0.5;;
    UILabel *alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(alertLabelX, alertLabelY, alertLabelW, alertLabelH)];
    //    alertLabel.text = [NSString stringWithFormat:@"请等待患者:%@进入诊间", self.remoteOrder.outpatientName];
    alertLabel.text = [NSString stringWithFormat:@"请等待对方进入视频诊间"];
    alertLabel.font = [UIFont systemFontOfSize:kP(34)];
    alertLabel.textColor = [UIColor whiteColor];
    alertLabel.backgroundColor = [UIColor blackColor];
    alertLabel.alpha = 0.3;
    alertLabel.layer.cornerRadius = kP(10);
    alertLabel.clipsToBounds = YES;
    alertLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:alertLabel];
    self.alertLabel = alertLabel;
    
    
    
    //1.图文
    CGFloat btnWH = kP(164);
    CGFloat btnFX = kP(100);
    CGFloat btnMargin = (self.view.width - 3 * btnWH - btnFX * 2) * 0.5;
    //1.
    CGFloat messageBtnX = btnFX;
    CGFloat messageBtnY = kP(0);
    UIButton *messageBtn = [[UIButton alloc] initWithFrame:CGRectMake(messageBtnX, messageBtnY, btnWH, btnWH)];
    [messageBtn setImage:[UIImage imageNamed:@"enterIM"] forState:UIControlStateNormal];
    [messageBtn addTarget:self action:@selector(enterIM:) forControlEvents:UIControlEventTouchUpInside];
    messageBtn.hidden = self.isComeFromPodfileVCtr;
    [self.controlView addSubview:messageBtn];
    self.messageBtn = messageBtn;
    //
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnFX, self.messageBtn.bottom + kP(10), btnWH, kP(25))];
    messageLabel.text = @"图文交流";
    messageLabel.textColor = [UIColor whiteColor];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.hidden = self.isComeFromPodfileVCtr;
    [self.controlView addSubview:messageLabel];
    self.messageLabel = messageLabel;
    ///2.
    /**
     UIButton *muteAudioButton = [[UIButton alloc] initWithFrame:CGRectMake(120, 80, 50, 50)];
     muteAudioButton.backgroundColor = [UIColor redColor];
     [muteAudioButton setTitle:@"暂停" forState:UIControlStateNormal];
     [muteAudioButton addTarget:self action:@selector(doMuteVideoPressed:) forControlEvents:UIControlEventTouchUpInside];
     [self.controlView addSubview:muteAudioButton];
     self.muteAudioButton = muteAudioButton;*/
    
    
    
    //2.
    CGFloat muteVideoButtonX = self.messageBtn.right + btnMargin;
    CGFloat muteVideoButtonY = messageBtnY;
    UIButton *muteVideoButton = [[UIButton alloc] initWithFrame:CGRectMake(muteVideoButtonX, muteVideoButtonY, kP(164), kP(164))];
    //    muteVideoButton.backgroundColor = [UIColor redColor];
    //[muteVideoButton setTitle:@"挂断" forState:UIControlStateNormal];
    [muteVideoButton setImage:[UIImage imageNamed:@"hangup"] forState:UIControlStateNormal];
    [muteVideoButton addTarget:self action:@selector(doClosePressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.controlView addSubview:muteVideoButton];
    self.muteVideoButton = muteVideoButton;
    
    
    
    //3.
    CGFloat patientInforButtonX = self.muteVideoButton.right + btnMargin;
    CGFloat patientInforButtonY = messageBtnY;
    
    UIButton *patientInforButton = [[UIButton alloc] initWithFrame:CGRectMake(patientInforButtonX, patientInforButtonY, btnWH, btnWH)];
    //patientInforButton.backgroundColor = [UIColor redColor];
    //[patientInforButton setTitle:@"患者资料" forState:UIControlStateNormal];
    [patientInforButton setImage:[UIImage imageNamed:@"enterPatientInfor"] forState:UIControlStateNormal];
    [patientInforButton addTarget:self action:@selector(enterPatientInfor:) forControlEvents:UIControlEventTouchUpInside];
    patientInforButton.hidden = self.isComeFromPodfileVCtr;
    [self.controlView addSubview:patientInforButton];
    self.patientInforButton = patientInforButton;
    
    
    
    //
    UILabel *patientInforLabel = [[UILabel alloc] initWithFrame:CGRectMake(patientInforButtonX, messageLabel.y, btnWH, kP(25))];
    patientInforLabel.text = @"患者资料";
    patientInforLabel.textColor = [UIColor whiteColor];
    patientInforLabel.textAlignment = NSTextAlignmentCenter;
    patientInforLabel.hidden = self.isComeFromPodfileVCtr;
    [self.controlView addSubview:patientInforLabel];
    self.patientInforLabel = patientInforLabel;
    
    
    ///
    UITapGestureRecognizer *backgroundTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doBackTapped:)];
    [self.view addGestureRecognizer:backgroundTap];
    self.backgroundTap = backgroundTap;
    
    //    UITapGestureRecognizer *backgroundDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doBackDoubleTapped:)];
    //    [self.view addGestureRecognizer:backgroundDoubleTap];
    //    self.backgroundDoubleTap = backgroundDoubleTap;
}


// 点击挂断按钮
- (void)doClosePressed:(UIButton *)btn {
    [self leaveChannel];
    
    [self.navigationController popViewControllerAnimated:YES];
}


// 进入IM
- (void)enterIM:(UIButton *)btn{
    HZIMChatViewController *IMChatVCtr = [HZIMChatViewController new];
    IMChatVCtr.IMOrder = self.remoteOrder;
    [self.navigationController pushViewController:IMChatVCtr animated:YES];
}


// 暂停视频
- (void)doMuteVideoPressed:(UIButton *)btn {
    self.videoMuted = !self.videoMuted;
}



// 摄像头反转
- (void)doCameraPressed:(UIButton *)btn {
    [self.agoraKit switchCamera];
}

// 患者资料
- (void)enterPatientInfor:(UIButton *)btn {
    //self.speakerEnabled = !self.speakerEnabled;
    HZOrderPatientInforViewController *patientInforVCtr = [HZOrderPatientInforViewController new];
    patientInforVCtr.basicOrder = self.remoteOrder;
    [self.navigationController pushViewController:patientInforVCtr animated:YES];
}




//
//- (void)doMuteAudioPressed:(UIButton *)sender {
// self.audioMuted = !self.audioMuted;
//}

- (void)doBackTapped:(UITapGestureRecognizer *)sender {
    //    self.shouldHideFlowViews = !self.shouldHideFlowViews;
    
    self.muteVideoButton.hidden = !self.muteVideoButton.hidden;
    self.messageBtn.hidden = !self.messageBtn.hidden;
    self.cameraButton.hidden = !self.cameraButton.hidden;
    self.patientInforButton.hidden = !self.patientInforButton.hidden;
    self.messageLabel.hidden = !self.messageLabel.hidden;
    self.patientInforLabel.hidden = !self.patientInforLabel.hidden;
}


//- (void)doBackDoubleTapped:(UITapGestureRecognizer *)sender {
//    if (!self.doubleClickFullSession) {
//        NSInteger tappedIndex = [self.videoViewLayouter responseIndexOfLocation:[sender locationInView:self.containerView]];
//        if (tappedIndex >= 0 && tappedIndex < self.videoSessions.count) {
//            self.doubleClickFullSession = self.videoSessions[tappedIndex];
//        }
//    } else {
//        self.doubleClickFullSession = nil;
//    }
//}


//
- (void)updateInterfaceWithSessions:(NSArray *)sessions targetSize:(CGSize)targetSize animation:(BOOL)animation {
    if (animation) {
        [UIView animateWithDuration:0.3 animations:^{
            [self updateInterfaceWithSessions:sessions targetSize:targetSize];
            [self.view layoutIfNeeded];
        }];
    } else {
        [self updateInterfaceWithSessions:sessions targetSize:targetSize];
    }
}

- (void)updateInterfaceWithSessions:(NSArray *)sessions targetSize:(CGSize)targetSize {
    if (!sessions.count) {
        return;
    }
    
    VideoSession *selfSession = sessions.firstObject;
    self.videoViewLayouter.selfView = selfSession.hostingView;
    self.videoViewLayouter.selfSize = selfSession.size;
    self.videoViewLayouter.targetSize = targetSize;
    
    NSMutableArray *peerVideoViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 1; i < sessions.count; ++i) {
        VideoSession *session = sessions[i];
        [peerVideoViews addObject:session.hostingView];
    }
    self.videoViewLayouter.videoViews = peerVideoViews;
    self.videoViewLayouter.fullView = self.doubleClickFullSession.hostingView;
    self.videoViewLayouter.containerView = self.containerView;
    
    [self.videoViewLayouter layoutVideoViews];
    [self updateSelfViewVisiable];
    
    if (sessions.count >= 3) {
        self.backgroundDoubleTap.enabled = YES;
    } else {
        self.backgroundDoubleTap.enabled = NO;
        self.doubleClickFullSession = nil;
    }
}

- (void)setIdleTimerActive:(BOOL)active {
    [UIApplication sharedApplication].idleTimerDisabled = !active;
}

- (void)addLocalSession {
    VideoSession *localSession = [VideoSession localSession];
    [self.videoSessions addObject:localSession];
    [self.agoraKit setupLocalVideo:localSession.canvas];
    
    // 屏幕大小
    [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:YES];
}

- (VideoSession *)fetchSessionOfUid:(NSUInteger)uid {
    for (VideoSession *session in self.videoSessions) {
        if (session.uid == uid) {
            return session;
        }
    }
    return nil;
}

- (VideoSession *)videoSessionOfUid:(NSUInteger)uid {
    VideoSession *fetchedSession = [self fetchSessionOfUid:uid];
    if (fetchedSession) {
        return fetchedSession;
    } else {
        VideoSession *newSession = [[VideoSession alloc] initWithUid:uid];
        [self.videoSessions addObject:newSession];
        [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:YES];
        return newSession;
    }
}

- (void)setVideoMuted:(BOOL)muted forUid:(NSUInteger)uid {
    VideoSession *fetchedSession = [self fetchSessionOfUid:uid];
    fetchedSession.isVideoMuted = muted;
}

- (void)updateSelfViewVisiable {
    UIView *selfView = self.videoSessions.firstObject.hostingView;
    if (self.videoSessions.count == 2) {
        selfView.hidden = self.videoMuted;
    } else {
        selfView.hidden = false;
    }
}

- (void)alertString:(NSString *)string {
    if (!string.length) {
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:string preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)leaveChannel {
    [self.agoraKit setupLocalVideo:nil];
    [self.agoraKit leaveChannel:nil];
    [self.agoraKit stopPreview];
    
    for (VideoSession *session in self.videoSessions) {
        [session.hostingView removeFromSuperview];
    }
    [self.videoSessions removeAllObjects];
    
    [self setIdleTimerActive:YES];
    
    if ([self.delegate respondsToSelector:@selector(roomVCNeedClose:)]) {
        [self.delegate roomVCNeedClose:self];
    }
}

#pragma mark -- <#class#>
- (void)loadAgoraKit {
    self.agoraKit = [AgoraRtcEngineKit sharedEngineWithAppId:[KeyCenter AppId] delegate:self];
    [self.agoraKit setChannelProfile:AgoraRtc_ChannelProfile_Communication];
    [self.agoraKit enableVideo];
    [self.agoraKit setVideoProfile:self.videoProfile swapWidthAndHeight:NO];
    
    [self addLocalSession];
    [self.agoraKit startPreview];
    
    int code = [self.agoraKit joinChannelByKey:nil channelName:self.roomName info:nil uid:0 joinSuccess:nil];
    if (code == 0) {
        [self setIdleTimerActive:NO];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self alertString:[NSString stringWithFormat:@"Join channel failed: %d", code]];
        });
    }
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine firstRemoteVideoDecodedOfUid:(NSUInteger)uid size:(CGSize)size elapsed:(NSInteger)elapsed {
    
    self.alertLabel.hidden = YES;
    
    NSString *orderStateStr = [NSString stringWithFormat:@"cfdu/v1/openapi/json/booking/order/start?bookingOrderId=%@", self.remoteOrder.bookingOrderId];
    orderStateStr = kGatewayUrlStr(orderStateStr);
    [HZHttpsTool POST:orderStateStr headDomainValue:nil parameters:nil progress:nil success:^(id responseObject) {
        NSLog(@"-----订单改变状态成功----%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"-----订单改变状态失败----%@", error);
    }];
    VideoSession *userSession = [self videoSessionOfUid:uid];
    userSession.size = size;
    [self.agoraKit setupRemoteVideo:userSession.canvas];
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine firstLocalVideoFrameWithSize:(CGSize)size elapsed:(NSInteger)elapsed {
    if (self.videoSessions.count) {
        VideoSession *selfSession = self.videoSessions.firstObject;
        selfSession.size = size;
        [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:NO];
    }
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didOfflineOfUid:(NSUInteger)uid reason:(AgoraRtcUserOfflineReason)reason {
    VideoSession *deleteSession;
    for (VideoSession *session in self.videoSessions) {
        if (session.uid == uid) {
            deleteSession = session;
        }
    }
    
    if (deleteSession) {
        [self.videoSessions removeObject:deleteSession];
        [deleteSession.hostingView removeFromSuperview];
        [self updateInterfaceWithSessions:self.videoSessions targetSize:self.containerView.frame.size animation:YES];
        
        if (deleteSession == self.doubleClickFullSession) {
            self.doubleClickFullSession = nil;
        }
    }
}

- (void)rtcEngine:(AgoraRtcEngineKit *)engine didVideoMuted:(BOOL)muted byUid:(NSUInteger)uid {
    [self setVideoMuted:muted forUid:uid];
}

#pragma mark -- <#class#>
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
