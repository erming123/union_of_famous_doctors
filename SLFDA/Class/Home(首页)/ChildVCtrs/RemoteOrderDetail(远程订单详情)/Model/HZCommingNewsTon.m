//
//  HZCommingNewsTon.m
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/23.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZCommingNewsTon.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZCommingNewsTon ()
@end
NS_ASSUME_NONNULL_END
@implementation HZCommingNewsTon

static HZCommingNewsTon *commingNewsTon = nil;
+ (instancetype)shareCommingNewsTon {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        commingNewsTon = [HZCommingNewsTon new];
    });
    
    return commingNewsTon;
}

- (void)setCommingNewsCount:(NSUInteger)commingNewsCount {
    _commingNewsCount = commingNewsCount;
}
@end
