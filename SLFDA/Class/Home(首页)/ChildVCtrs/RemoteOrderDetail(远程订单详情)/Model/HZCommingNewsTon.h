//
//  HZCommingNewsTon.h
//  SLFDA
//
//  Created by 季怀斌 on 2016/11/23.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HZCommingNewsTon : NSObject
+ (instancetype)shareCommingNewsTon;
@property (nonatomic, assign) NSUInteger commingNewsCount;
@end
