//
//  HZSickDescribCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HZSickDescribCellDelegate <NSObject>

- (void)upTableView:(CGFloat)keyBoardH;
- (void)revertTableView:(NSString *)textViewText;
@end
@interface HZSickDescribCell : UITableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, weak) id<HZSickDescribCellDelegate>delegate;
@property (nonatomic, assign) BOOL becomeFirstResponder;
@end
