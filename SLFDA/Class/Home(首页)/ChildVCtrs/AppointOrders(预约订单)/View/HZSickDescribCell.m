//
//  HZSickDescribCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZSickDescribCell.h"
#import "HZTextView.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZSickDescribCell ()<HZTextViewDelegate>
@property (nonatomic, strong) UILabel *sickDesLabel;
@property (nonatomic, strong) HZTextView *sickDesTextView;
@end
NS_ASSUME_NONNULL_END
@implementation HZSickDescribCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
    }
    
    return self;
}

- (void)setUpSubViews {
    
    //0.
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //1.
    UILabel *sickDesLabel = [UILabel new];
    [self addSubview:sickDesLabel];
    self.sickDesLabel = sickDesLabel;
    
    //2.
    HZTextView *sickDesTextView = [HZTextView new];
    sickDesTextView.delegate = self;
    [self addSubview:sickDesTextView];
    self.sickDesTextView = sickDesTextView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    
    //2.
    self.sickDesLabel.x = kP(28);
    self.sickDesLabel.y = kP(32);
    //
    CGFloat sickDesTextViewX = self.sickDesLabel.x;
    CGFloat sickDesTextViewY = self.sickDesLabel.bottom + kP(32);
    CGFloat sickDesTextViewW = self.width - 2 * sickDesTextViewX;
    CGFloat sickDesTextViewH = kP(240);
    self.sickDesTextView.frame = CGRectMake(sickDesTextViewX, sickDesTextViewY, sickDesTextViewW, sickDesTextViewH);
}

- (void)setSubViewsContent {
    
    //
    self.sickDesLabel.text = @"疾病描述";
    self.sickDesLabel.font = [UIFont systemFontOfSize:kP(32)];
    [self.sickDesLabel sizeToFit];
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"sickDescribCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}


- (void)setBecomeFirstResponder:(BOOL)becomeFirstResponder {
    self.sickDesTextView.becomeFirstResponder = becomeFirstResponder;
}


#pragma mark -- HZTextViewDelegate
- (void)passTextViewStateDidBegin:(CGFloat)keyBoardH{
    if (self.delegate && [self.delegate respondsToSelector:@selector(upTableView:)]) {
        [self.delegate upTableView:keyBoardH];
    }
}

- (void)passTextViewStateClickReturn:(NSString *)textViewText {
    if (self.delegate && [self.delegate respondsToSelector:@selector(revertTableView:)]) {
        [self.delegate revertTableView:textViewText];
    }
}

@end
