//
//  HZAlertTextField.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZAlertTextField;
@protocol HZAlertTextFieldDelegate <NSObject>

- (void)hideAlertTextField:(HZAlertTextField *)alertTextField;
- (void)getPatientInfor:(HZAlertTextField *)alertTextField;
- (void)scanBtnClickAction;
@end
@interface HZAlertTextField : UIView
@property (nonatomic, assign) BOOL isBecomeFirstResponder;
@property (nonatomic, weak) id<HZAlertTextFieldDelegate>delegate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *textFieldText;
@property (nonatomic, assign) BOOL isKeyboardNumberType;
@property (nonatomic, assign) BOOL needScan;
@end
