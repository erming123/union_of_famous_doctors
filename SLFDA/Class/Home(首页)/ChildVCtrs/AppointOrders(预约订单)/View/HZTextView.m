//
//  HZTextView.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZTextView.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZTextView ()<UITextViewDelegate>
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) UILabel *placeHolderLabel;
//** <#注释#>*/
@property (nonatomic, assign) CGFloat keyBoardH;
@end
NS_ASSUME_NONNULL_END
@implementation HZTextView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}
- (void)setUpSubViews {
    
    //0.
    self.backgroundColor = [UIColor redColor];
    
    //1.
    UITextView *textView = [UITextView new];
    textView.delegate = self;
    textView.font = [UIFont systemFontOfSize:kP(32)];
    textView.backgroundColor = [UIColor colorWithHexString:@"#F0F0F0" alpha:1.0];
    textView.returnKeyType= UIReturnKeyDone;
    [self addSubview:textView];
    self.textView = textView;
    
    //2.
    UILabel *placeHolderLabel = [UILabel new];
    [self.textView addSubview:placeHolderLabel];
    self.placeHolderLabel = placeHolderLabel;
    
    //3.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //1.
    [self setSubViewsContent];
    self.layer.cornerRadius = kP(10);
    self.clipsToBounds = YES;
    //2.
    self.textView.frame = self.bounds;
    
    //
    CGFloat placeHolderLabelX = kP(10);
    CGFloat placeHolderLabelY = kP(16);
    CGFloat placeHolderLabelW = self.width;
    CGFloat placeHolderLabelH = [self.placeHolderLabel.text getTextHeightWithMaxWidth:self.width textFontSize:kP(33)];
    self.placeHolderLabel.frame = CGRectMake(placeHolderLabelX, placeHolderLabelY, placeHolderLabelW, placeHolderLabelH);
}

- (void)setSubViewsContent {
    self.placeHolderLabel.text = @"请描述疾病的症状,发病时间和就诊人的身体状况";
    self.placeHolderLabel.textColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0];
    self.placeHolderLabel.font = [UIFont systemFontOfSize:kP(32)];
    self.placeHolderLabel.numberOfLines = 0;
}

- (void)setBecomeFirstResponder:(BOOL)becomeFirstResponder {
    becomeFirstResponder ? [self.textView becomeFirstResponder] : [self.textView resignFirstResponder];
}

#pragma mark -- UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    self.placeHolderLabel.text = textView.text.length > 0 ? @"" : @"请描述疾病的症状,发病时间和就诊人的身体状况";

}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(passTextViewStateDidBegin:)]) {
        [self.delegate passTextViewStateDidBegin:self.keyBoardH];
    }
}


- (void)keyboardWillChangeFrameNotification:(NSNotification *)notification {
    
    
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyBoardH = keyboardRect.size.height;
}

// 点击return键
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
        if ([text isEqualToString:@"\n"]) {
            
            [textView resignFirstResponder];
            //
            if (self.delegate && [self.delegate respondsToSelector:@selector(passTextViewStateClickReturn:)]) {
                [self.delegate passTextViewStateClickReturn:textView.text];
            }
            //
            return NO;

    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    NSLog(@"--------textViewDidEndEditing-----");
    [textView resignFirstResponder];
    //
    if (self.delegate && [self.delegate respondsToSelector:@selector(passTextViewStateClickReturn:)]) {
        [self.delegate passTextViewStateClickReturn:textView.text];
    }

}

- (void)dealloc {
//    self.textView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//- (void)dealloc {
//    
//}
@end
