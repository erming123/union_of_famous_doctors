//
//  HZPatientSummitInforCell.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZPatient, HZPatientSummitInforCell;

@protocol HZPatientSummitInforCellDelegate <NSObject>
@optional
- (void)revertMyTableView;
- (void)showGenderSelectView;
@end
@interface HZPatientSummitInforCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
@property (nonatomic, strong) UIColor *seprateHLineColor;
@property (nonatomic, weak) id<HZPatientSummitInforCellDelegate>delegate;
@property (nonatomic, assign) BOOL isTextFieldEnable;
@property (nonatomic, copy) NSString *patientGenderStr;
@end
