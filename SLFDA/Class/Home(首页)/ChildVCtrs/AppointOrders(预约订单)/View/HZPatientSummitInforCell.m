//
//  HZPatientSummitInforCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/12/13.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPatientSummitInforCell.h"
#import "HZPatient.h"
NS_ASSUME_NONNULL_BEGIN
@interface HZPatientSummitInforCell () <UITextFieldDelegate>
@property (nonatomic, weak) UILabel *patientNameLabel;
@property (nonatomic, weak) UITextField *patientNameTextField;
@property (nonatomic, weak) UIView *firstHLine;
@property (nonatomic, weak) UILabel *patientGenderLabel;
//@property (nonatomic, weak) UITextField *patientGenderTextField;
@property (nonatomic, weak) UILabel *patientGenderContentLabel;
@property (nonatomic, weak) UIView *secondHLine;
@property (nonatomic, weak) UILabel *patientAgeLabel;
@property (nonatomic, weak) UITextField *patientAgeTextField;
@end
NS_ASSUME_NONNULL_END
@implementation HZPatientSummitInforCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUpSubViews];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)setUpSubViews {
    
    //
    UILabel *patientNameLabel = [UILabel new];
    patientNameLabel.attributedText = [@"姓名 *" changeContentColorWithString:@"*" color:[UIColor redColor]];
//    patientNameLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:patientNameLabel];
    self.patientNameLabel = patientNameLabel;
    //
    UITextField *patientNameTextField = [UITextField new];
//    patientNameTextField.backgroundColor = [UIColor greenColor];
    patientNameTextField.textAlignment = NSTextAlignmentRight;
    patientNameTextField.placeholder = @"请输入患者的姓名";
    patientNameTextField.delegate = self;
    
    [patientNameTextField addTarget:self action:@selector(typeInTextField:) forControlEvents:UIControlEventEditingChanged];
    [self.contentView addSubview:patientNameTextField];
    patientNameTextField.returnKeyType = UIReturnKeyNext;
    self.patientNameTextField = patientNameTextField;
    
    //
    UIView *firstHLine = [UIView new];
    [self.contentView addSubview:firstHLine];
    self.firstHLine = firstHLine;
    
    //
    UILabel *patientGenderLabel = [UILabel new];
    patientGenderLabel.attributedText = [@"性别 *" changeContentColorWithString:@"*" color:[UIColor redColor]];
//    patientGenderLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:patientGenderLabel];
    self.patientGenderLabel = patientGenderLabel;
    
//    //
//    UITextField *patientGenderTextField = [UITextField new];
////    patientGenderTextField.backgroundColor = [UIColor greenColor];
//    patientGenderTextField.textAlignment = NSTextAlignmentRight;
//    patientGenderTextField.returnKeyType = UIReturnKeyNext;
//    patientGenderTextField.placeholder = @"请输入患者的性别";
//    patientGenderTextField.delegate = self;
//
//    [patientGenderTextField addTarget:self action:@selector(typeInTextField:) forControlEvents:UIControlEventEditingChanged];
//    [self.contentView addSubview:patientGenderTextField];
//    self.patientGenderTextField = patientGenderTextField;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToShowGenderSelectView:)];
    
    //
    UILabel *patientGenderContentLabel = [UILabel new];
    patientGenderContentLabel.text = @"请选择性别";
    patientGenderContentLabel.userInteractionEnabled = YES;
//    patientGenderContentLabel.font = [UIFont systemFontOfSize:kP(28)];
//    patientGenderContentLabel.textColor = self.detailTextLabel.textColor;
    patientGenderContentLabel.textColor = [UIColor colorWithHexString:@"#C1C1C4"];
    patientGenderContentLabel.textAlignment = NSTextAlignmentRight;
    [patientGenderContentLabel addGestureRecognizer:tap];
    [self.contentView addSubview:patientGenderContentLabel];
    self.patientGenderContentLabel = patientGenderContentLabel;
    
    UIView *secondHLine = [UIView new];
    [self.contentView addSubview:secondHLine];
    self.secondHLine = secondHLine;
    
    //
    UILabel *patientAgeLabel = [UILabel new];
    patientAgeLabel.attributedText = [@"年龄 *" changeContentColorWithString:@"*" color:[UIColor redColor]];
//    patientAgeLabel.backgroundColor = [UIColor redColor];
    [self.contentView addSubview:patientAgeLabel];
    self.patientAgeLabel = patientAgeLabel;
    //
    UITextField *patientAgeTextField = [UITextField new];
//    patientAgeTextField.backgroundColor = [UIColor greenColor];
    patientAgeTextField.textAlignment = NSTextAlignmentRight;
    patientAgeTextField.placeholder = @"请输入患者的年龄";
    patientAgeTextField.returnKeyType = UIReturnKeyDone;
    patientAgeTextField.delegate = self;
    
    [patientAgeTextField addTarget:self action:@selector(typeInTextField:) forControlEvents:UIControlEventEditingChanged];
    [self.contentView addSubview:patientAgeTextField];
    self.patientAgeTextField = patientAgeTextField;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //2.
    self.patientNameLabel.x = kP(28);
    self.patientNameLabel.y = kP(29);
    [self.patientNameLabel sizeToFit];
    
    self.patientNameTextField.x = self.patientNameLabel.right + kP(30);
    self.patientNameTextField.y = self.patientNameLabel.y;
    self.patientNameTextField.width = HZScreenW - self.patientNameTextField.x - self.patientNameLabel.x;
    self.patientNameTextField.height = self.patientNameLabel.height;
    
    //
    self.firstHLine.x = kP(28);
    self.firstHLine.y = kP(99);
    self.firstHLine.width = self.width - self.firstHLine.x;
    self.firstHLine.height = 0.5;
    
    
    //3.
    self.patientGenderLabel.x = self.patientNameLabel.x;
    self.patientGenderLabel.y = self.patientNameLabel.bottom + 2 * self.patientNameTextField.y;
    [self.patientGenderLabel sizeToFit];
    
//    self.patientGenderTextField.x = self.patientNameTextField.x;
//    self.patientGenderTextField.y = self.patientGenderLabel.y;
//    self.patientGenderTextField.width = self.patientNameTextField.width;
//    self.patientGenderTextField.height = self.patientNameTextField.height;
    
    self.patientGenderContentLabel.x = self.patientNameTextField.x;
    self.patientGenderContentLabel.y = self.firstHLine.y;
    self.patientGenderContentLabel.width = self.patientNameTextField.width;
    self.patientGenderContentLabel.height = kP(100);
    
    self.secondHLine.x = kP(28);
    self.secondHLine.y = kP(198);
    self.secondHLine.width = self.width - self.firstHLine.x;
    self.secondHLine.height = 0.5;
    
    //4.
    self.patientAgeLabel.x = self.patientGenderLabel.x;
    self.patientAgeLabel.y = self.secondHLine.bottom +  self.patientNameLabel.y;
    [self.patientAgeLabel sizeToFit];
    
    self.patientAgeTextField.x = self.patientNameTextField.x;
    self.patientAgeTextField.y = self.patientAgeLabel.y;
    self.patientAgeTextField.width = self.patientNameTextField.width;
    self.patientAgeTextField.height = self.patientNameTextField.height;
}


- (void)setSeprateHLineColor:(UIColor *)seprateHLineColor {
    _seprateHLineColor = seprateHLineColor;
    
    //
    self.firstHLine.backgroundColor = self.seprateHLineColor;
    self.secondHLine.backgroundColor = self.seprateHLineColor;
}


+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *cellID = @"PatientSummitInforCell";
    id cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    return cell;
}


- (void)typeInTextField:(UITextField *)textField {
    
    
    NSLog(@"----textField.text-----%@", textField.text);
    //
    [self sendSubmitBtnEnable];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (self.patientNameTextField.text.length == 0){
        [self.patientNameTextField becomeFirstResponder];
    } else if (self.patientAgeTextField.text.length == 0){
        [self.patientAgeTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        if (self.delegate && [self.delegate respondsToSelector:@selector(revertMyTableView)]) {
            [self.delegate revertMyTableView];
        }
    }
    
    //
    [self sendSubmitBtnEnable];
    
    return YES;
}


#pragma mark ----------------- tapToShowGenderSelectView ------------------
- (void)tapToShowGenderSelectView:(UITapGestureRecognizer *)tap {
    NSLog(@"--------sfdsfsdf-----------");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(showGenderSelectView)]) {
        [self.delegate showGenderSelectView];
    }
}


- (BOOL)isTextFieldEnable {
    return self.patientNameTextField.text.length != 0 && self.patientAgeTextField.text.length != 0;
}

- (void)setPatientGenderStr:(NSString *)patientGenderStr {
    _patientGenderStr = patientGenderStr;
    self.patientGenderContentLabel.text = patientGenderStr;
    self.patientGenderContentLabel.textColor = [UIColor blackColor];
    //
    [self sendSubmitBtnEnable];
}


#pragma mark ----------------- sendSubmitBtnEnable ------------------
- (void)sendSubmitBtnEnable {
    //
    if (self.patientNameTextField.text.length != 0 && ![self.patientGenderContentLabel.text isEqualToString:@"请选择性别"] && self.patientAgeTextField.text.length != 0) {
        
        NSString *genderCode = @"";
        if ([self.patientGenderContentLabel.text isEqualToString:@"男"] || [self.patientGenderContentLabel.text isEqualToString:@"male"]) {
            genderCode = @"1";
        } else if ([self.patientGenderContentLabel.text isEqualToString:@"女"] || [self.patientGenderContentLabel.text isEqualToString:@"female"]){
            genderCode = @"0";
        }
        
        NSDictionary *paramDict = @{@"patientName" : self.patientNameTextField.text,
                                    @"patientGender" : genderCode,
                                    @"patientAge" : [NSString stringWithFormat:@"%@岁", self.patientAgeTextField.text]
                                    };
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"submitBtnShouldEnable" object:paramDict];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"submitBtnShouldEnable" object:nil];
    }
}

@end
