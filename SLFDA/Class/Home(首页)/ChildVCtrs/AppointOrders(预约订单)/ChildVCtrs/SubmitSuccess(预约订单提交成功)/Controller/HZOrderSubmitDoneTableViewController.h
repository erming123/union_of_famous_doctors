//
//  HZOrderSubmitDoneTableViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface HZOrderSubmitDoneTableViewController : UITableViewController
@property (nonatomic, copy) NSString *appointBookingOrderId;
@end
