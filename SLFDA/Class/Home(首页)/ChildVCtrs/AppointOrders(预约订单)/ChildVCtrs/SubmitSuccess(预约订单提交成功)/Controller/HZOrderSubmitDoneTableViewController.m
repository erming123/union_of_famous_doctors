//
//   HZOrderSubmitDoneTableView.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/2.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZOrderSubmitDoneTableViewController.h"
#import "HZHomeViewController.h"

//
#import "HZPayOrder.h"
//
#import "HZAlertPayView.h"
//
//
#import "HZOrderHttpsTool.h"
@interface HZOrderSubmitDoneTableViewController () <HZAlertPayViewDelegate>
//** <#注释#>*/
@property (nonatomic, weak) UIImageView *doneImgeView;

//** <#注释#>*/
@property (nonatomic, strong) HZPayOrder *payOrder;
//** <#注释#>*/
@property (nonatomic, weak) HZAlertPayView *alertPayView;
//
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation HZOrderSubmitDoneTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setViewPanBack];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationItem.title = @"预约成功";
    
//    //
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(returnToHome)];
//    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#21b8c6" alpha:1.0], NSFontAttributeName : [UIFont systemFontOfSize:kP(32)]} forState:UIControlStateNormal];

    
    //
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CGFloat doneImgeViewX = kP(284);
    CGFloat doneImgeViewY = kP(148);
    CGFloat doneImgeViewWH = self.view.width - 2 * doneImgeViewX;
    UIImageView *doneImgeView = [[UIImageView alloc] initWithFrame:CGRectMake(doneImgeViewX, doneImgeViewY, doneImgeViewWH, doneImgeViewWH)];
    doneImgeView.image = [UIImage imageNamed:@"done.png"];
    [self.tableView addSubview:doneImgeView];
    self.doneImgeView = doneImgeView;
    
    //
    UILabel *doneAlertLabel = [[UILabel alloc] init];
    doneAlertLabel.text = @"专家预约成功";
    doneAlertLabel.font = [UIFont systemFontOfSize:kP(40)];
    doneAlertLabel.textColor = [UIColor colorWithHexString:@"#333333" alpha:1.0];
    doneAlertLabel.textAlignment = NSTextAlignmentCenter;
    [doneAlertLabel sizeToFit];
    doneAlertLabel.x = self.view.width * 0.5 - doneAlertLabel.width * 0.5;
    doneAlertLabel.y = doneImgeView.bottom + kP(60);
    [self.tableView addSubview:doneAlertLabel];
    
    //
    UILabel *payAlertLabel = [[UILabel alloc] init];
    payAlertLabel.text = @"请让患者通过微信或者支付宝进行扫码支付";
    payAlertLabel.font = [UIFont systemFontOfSize:kP(32)];
    payAlertLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
    payAlertLabel.textAlignment = NSTextAlignmentCenter;
    [payAlertLabel sizeToFit];
    payAlertLabel.x = self.view.width * 0.5 - payAlertLabel.width * 0.5;
    payAlertLabel.y = doneAlertLabel.bottom + kP(35);
    [self.tableView addSubview:payAlertLabel];
    
    //
    CGFloat laterToPayBtnW = kP(574);
    CGFloat laterToPayBtnH = kP(100);
    CGFloat laterToPayBtnX = self.view.width * 0.5 - laterToPayBtnW * 0.5;
    CGFloat laterToPayBtnY = self.view.height - laterToPayBtnH - kP(98) - 50;
    UIButton *laterToPayBtn = [[UIButton alloc] initWithFrame:CGRectMake(laterToPayBtnX, laterToPayBtnY, laterToPayBtnW, laterToPayBtnH)];
    laterToPayBtn.backgroundColor = [UIColor whiteColor];
    laterToPayBtn.layer.borderColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0].CGColor;
    laterToPayBtn.layer.borderWidth = kP(2);
    laterToPayBtn.layer.cornerRadius = kP(16);
    laterToPayBtn.clipsToBounds = YES;
    [laterToPayBtn setTitle:@"稍后支付" forState:UIControlStateNormal];
    laterToPayBtn.titleLabel.font = [UIFont systemFontOfSize:kP(36)];
    [laterToPayBtn setTitleColor:[UIColor colorWithHexString:@"#2DBED8" alpha:1.0] forState:UIControlStateNormal];
    [laterToPayBtn addTarget:self action:@selector(returnBackHome) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:laterToPayBtn];
    
    
    //
    CGFloat scanToPayBtnW = laterToPayBtnW;
    CGFloat scanToPayBtnH = laterToPayBtnH;
    CGFloat scanToPayBtnX = laterToPayBtnX;
    CGFloat scanToPayBtnY = laterToPayBtnY - scanToPayBtnH - kP(50);
    UIButton *scanToPayBtn = [[UIButton alloc] initWithFrame:CGRectMake(scanToPayBtnX, scanToPayBtnY, scanToPayBtnW, scanToPayBtnH)];
    scanToPayBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
//    scanToPayBtn.layer.borderColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0].CGColor;
//    scanToPayBtn.layer.borderWidth = kP(2);
    scanToPayBtn.layer.cornerRadius = kP(16);
    scanToPayBtn.clipsToBounds = YES;
    [scanToPayBtn setTitle:@"打开二维码让患者支付" forState:UIControlStateNormal];
    scanToPayBtn.titleLabel.font = [UIFont systemFontOfSize:kP(36)];
    [scanToPayBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scanToPayBtn addTarget:self action:@selector(scanToPayBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scanToPayBtn];
    
    //
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
}



#pragma mark -- set
- (void)setAppointBookingOrderId:(NSString *)appointBookingOrderId {
    _appointBookingOrderId = appointBookingOrderId;
}
#pragma mark -- 去掉返回按钮

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationItem.hidesBackButton = YES;
}


#pragma mark -- scanToPayBtnClick
- (void)scanToPayBtnClick:(UIButton *)scanToPayBtn {
    NSLog(@"------scanToPayBtnClick--------");
    
    if (self.alertPayView) {
        [self.alertPayView setMyTimerStart];
    }
    
    NSLog(@"enterToPay");
    
    NSDate *senddate = [NSDate date];
    NSInteger currentTime = (long)[senddate timeIntervalSince1970];
    
    NSInteger weChatOldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"WeChatQRImageOldTime%@", self.appointBookingOrderId]] integerValue];
    NSInteger AliOldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"AliQRImageOldTime%@", self.appointBookingOrderId]] integerValue];
    
    
    NSLog(@"weChatOldTime:%ld------AliOldTime:%ld--------currentTime:%ld------payOrder:%@------ currentTime - weChatOldTime:%ld------currentTime - AliOldTime:%ld", weChatOldTime, AliOldTime, currentTime, self.payOrder, currentTime -  weChatOldTime, currentTime - AliOldTime);
    
    if (currentTime - weChatOldTime > 2 * 3600 || currentTime - AliOldTime > 2 * 3600 ) {
        [self getPayParamsDictToImageData];
        
        NSLog(@"--qwweeee---------------------");
        //        self.alertPayView.isShowWeChat = YES;
        return;
    } else if (self.payOrder == nil) {
        NSLog(@"---------------uio-----------------");
        [self getPayParamsDict];
        
        return;
    }
    
    NSLog(@"---------------rty-----------------");
    
    //    if (currentTime - weChatOldTime > 2 * 3600 || currentTime - AliOldTime > 2 * 3600 || weChatOldTime == 0 || AliOldTime == 0) {
    //        [self getPayParamsDict];
    //
    //        //        self.alertPayView.isShowWeChat = YES;
    //        return;
    //    }
    
    //    //
    //    [self.alertPayView removeFromSuperview];
    //    [self.alertPayView.timer invalidate];
    //    self.alertPayView.timer = nil;
    //
    //    HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:nil];
    //    alertPayView.delegate = self;
    //    [self.navigationController.view addSubview:alertPayView];
    //    self.alertPayView = alertPayView;
    
    [self setMyAlertPayViewWithPayOrder:self.payOrder];
}

#pragma mark -- returnToHome
- (void)returnBackHome {
    
//    HZHomeViewController *homeVCtr = [HZHomeViewController new];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark -- setMyAlertPayViewWithPayOrder
- (void)setMyAlertPayViewWithPayOrder:(HZPayOrder *)payOrder {
    //
    [self.alertPayView removeFromSuperview];
    [self.alertPayView.timer invalidate];
    self.alertPayView.timer = nil;
    
    HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:payOrder];
    alertPayView.delegate = self;
    alertPayView.isFromAppointSuccess = YES;
    [self.navigationController.view addSubview:alertPayView];
    self.alertPayView = alertPayView;
    
    //
    self.alertPayView.hidden = NO;
}

#pragma mark -- getPayParamsDictToImageData
- (void)getPayParamsDictToImageData {
    
//    NSLog(@"---------sdffgh------:%@", self.appointOrder.bookingOrderId);
    [self.activityIndicator startAnimating];
    [HZOrderHttpsTool getPayParamsWithBookingOrderId:self.appointBookingOrderId success:^(id responseObject) {
        NSLog(@"--getPayParamsWithBookingOrderId---成功:----%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *payDict = responseObject[@"results"];
            
            HZPayOrder *payOrder = [HZPayOrder mj_objectWithKeyValues:payDict];
            payOrder = [HZPayOrder mj_objectWithKeyValues:[payOrder propertyValueDict]];
            NSLog(@"---------cdf--123-------------:%@", payOrder);
            //
            //            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            //            dispatch_group_t group = dispatch_group_create();
            //
            //            dispatch_group_async(group, queue, ^{
            //                // 获取微信二维码图片
            [self getWeChatQRImageWith:payOrder];
            //            });
            //            dispatch_group_async(group, queue, ^{
            // 获取支付宝二维码图片
            [self getAliPayQRImageWith:payOrder];
            //                self.alertPayView.hidden = NO;
            //            });
            
            //            //等group里的task都执行完后执行notify方法里的内容
            //            dispatch_group_notify(group, queue, ^{
            //
            //            });
            
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----失败:----%@", error);
    }];
}

- (void)getWeChatQRImageWith:(HZPayOrder *)payOrder {
    
    //1.0
    //    if (![self.localOrder.bookingStateCode isEqualToString:@"001"]) return;
    
    //2.0
    //    NSDate *senddate = [NSDate date];
    //    NSInteger currentTime = (long)[senddate timeIntervalSince1970];
    //
    //    NSInteger weChatOldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"WeChatQRImageOldTime%@", payOrder.bookingOrderId]] integerValue];
    
    //    NSString *weChatQRImageId = [NSString stringWithFormat:@"WeChatQRImage%@", payOrder.bookingOrderId];
    //    NSData *weChatQRImageData = [[NSUserDefaults standardUserDefaults] objectForKey:weChatQRImageId];
    
    
    //    if ((currentTime - weChatOldTime) <= 60 && weChatQRImageData) return;
    
    
    // 3.0
    //    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    
    [HZOrderHttpsTool getWeChatPayIdWithPayOrder:payOrder success:^(id responseObject) {
        NSLog(@"-----clickToWeChatPayY----%@",  [(NSDictionary *)responseObject getDictJsonStr]);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            
            //
            NSString *relationId = resultsDict[@"relationId"];
            //
            NSLog(@"---------WeChatPayrelationId:%@", relationId);
            //
            [HZOrderHttpsTool getQRImageWithRelationId:relationId success:^(NSData *QRImageData) {
                
                
                NSLog(@"--------Wy-------%@", QRImageData);
                
                //                    dispatch_async(dispatch_get_main_queue(), ^{
                
                //
                NSDate *senddate = [NSDate date];
                NSString *dateStr = [NSString stringWithFormat:@"%ld", (long)[senddate timeIntervalSince1970]];
                [[NSUserDefaults standardUserDefaults] setObject:dateStr forKey:[NSString stringWithFormat:@"WeChatQRImageOldTime%@", payOrder.bookingOrderId]];
                //
                NSString *QRImageId = [NSString stringWithFormat:@"WeChatQRImage%@", payOrder.bookingOrderId];
                [[NSUserDefaults standardUserDefaults] setObject:QRImageData forKey:QRImageId];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                //
                
                self.payOrder = payOrder;
                
                //
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                        //
                    //                        [self.alertPayView removeFromSuperview];
                    //                        [self.alertPayView.timer invalidate];
                    //                        self.alertPayView.timer = nil;
                    //
                    //                        HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:payOrder];
                    //                        alertPayView.delegate = self;
                    //                        [self.navigationController.view addSubview:alertPayView];
                    //                        self.alertPayView = alertPayView;
                    //
                    //                        //
                    //                        self.alertPayView.hidden = NO;
                    
                    
                    //
                    [self setMyAlertPayViewWithPayOrder:payOrder];
                    //
                    [self.activityIndicator stopAnimating];
                });
                
                
                //
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"--------Wf-------%@", error);
            }];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------clickToWeChatPayF-----%@", error);
    }];
    //    });
    
    
}


- (void)getAliPayQRImageWith:(HZPayOrder *)payOrder {
    
    //    //2.0
    //    NSDate *senddate = [NSDate date];
    //    NSInteger currentTime = (long)[senddate timeIntervalSince1970];
    //
    //    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    //
    //    NSInteger AliOldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"AliQRImageOldTime%@", payOrder.bookingOrderId]] integerValue];
    //    NSString *AliQRImageId = [NSString stringWithFormat:@"AliQRImage%@", payOrder.bookingOrderId];
    //    NSData *AliQRImageData = [[NSUserDefaults standardUserDefaults] objectForKey:AliQRImageId];
    
    //2.0
    //    if ((currentTime - AliOldTime) <= 60 && AliQRImageData) return;
    
    //    //3.0
    //    dispatch_async(dispatch_get_global_queue(0, 0), ^{
    
    NSLog(@"---sdf-%@", payOrder);
    
    [HZOrderHttpsTool getAliPayIdWithPayOrder:payOrder success:^(id responseObject) {
        NSLog(@"-----clickToAliPayY----%@", [(NSDictionary *)responseObject getDictJsonStr]);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *resultsDict = responseObject[@"results"];
            
            //
            NSString *relationId = resultsDict[@"relationId"];
            
            
            //
            NSLog(@"---------AliPayrelationId:%@", relationId);
            
            
            //
            [HZOrderHttpsTool getQRImageWithRelationId:relationId success:^(NSData *QRImageData) {
                NSLog(@"--------Ay-------%@", QRImageData);
                //
                NSDate *senddate = [NSDate date];
                NSString *dateStr = [NSString stringWithFormat:@"%ld", (long)[senddate timeIntervalSince1970]];
                [[NSUserDefaults standardUserDefaults] setObject:dateStr forKey:[NSString stringWithFormat:@"AliQRImageOldTime%@", payOrder.bookingOrderId]];
                //
                NSString *QRImageId = [NSString stringWithFormat:@"AliQRImage%@", payOrder.bookingOrderId];
                [[NSUserDefaults standardUserDefaults] setObject:QRImageData forKey:QRImageId];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //
                self.payOrder = payOrder;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    //
                    //                    [self.alertPayView removeFromSuperview];
                    //                    [self.alertPayView.timer invalidate];
                    //                    self.alertPayView.timer = nil;
                    //
                    //                    HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:payOrder];
                    //                    alertPayView.delegate = self;
                    //                    [self.navigationController.view addSubview:alertPayView];
                    //                    self.alertPayView = alertPayView;
                    //
                    //                    //
                    //                    self.alertPayView.hidden = NO;
                    
                    //
                    [self setMyAlertPayViewWithPayOrder:payOrder];
                    //
                    [self.activityIndicator stopAnimating];
                });
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"--------Af-------%@", error);
                
            }];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------clickToAliPayF-----%@", error);
        
    }];
    //    });
    
    
}


#pragma mark -- getPayParamsDict

- (void)getPayParamsDict {
    
    [HZOrderHttpsTool getPayParamsWithBookingOrderId:self.appointBookingOrderId success:^(id responseObject) {
        NSLog(@"---getPayParamsDict--成功:----%@", responseObject);
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSDictionary *payDict = responseObject[@"results"];
            
            HZPayOrder *payOrder = [HZPayOrder mj_objectWithKeyValues:payDict];
            payOrder = [HZPayOrder mj_objectWithKeyValues:[payOrder propertyValueDict]];
            NSLog(@"---------cdf--123-------------:%@", payOrder);
            
            self.payOrder = payOrder;
            dispatch_async(dispatch_get_main_queue(), ^{
                //                //
                //                [self.alertPayView removeFromSuperview];
                //                [self.alertPayView.timer invalidate];
                //                self.alertPayView.timer = nil;
                //
                //                HZAlertPayView *alertPayView = [[HZAlertPayView alloc] initWithPayOrder:payOrder];
                //                alertPayView.delegate = self;
                //                [self.navigationController.view addSubview:alertPayView];
                //                self.alertPayView = alertPayView;
                //
                //                //
                //                self.alertPayView.hidden = NO;
                
                [self setMyAlertPayViewWithPayOrder:payOrder];
            });
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----失败:----%@", error);
    }];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    //
    [self.alertPayView.timer invalidate];
    self.alertPayView.timer = nil;
    //
    //    self.timeCount = 0;
}

//#pragma mark -- reFreshLocalOrderDetail
//- (void)reFreshLocalOrderDetail {
//    [[HZDataHelper shareInstance] getOrderDetailWithOrderID:self.appointOrder.bookingOrderId success:^(id responseObject) {
//        NSLog(@"------getOrderDetailWithOrderID---%@", responseObject);
//        
//        if ([responseObject[@"state"] isEqual:@200]) {
//            NSDictionary *resultsDict = responseObject[@"results"];
//            NSDictionary *bookingOrderDict = resultsDict[@"BookingOrder"];
//            
//            HZOrder *localOrder = [HZOrder mj_objectWithKeyValues:bookingOrderDict];
//            self.appointOrder = [HZOrder mj_objectWithKeyValues:[localOrder propertyValueDict]];
//            
//            NSLog(@"--------asd-------:%@", self.appointOrder);
//            [self setUpVideoBtn];
//            [self.localOrderDetailTableView reloadData];
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"--------2233----wwww");
//    }];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 0;
}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
