//
//  HZSickNameSearchViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/3.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZSickNameSearchViewController.h"
#import "HZSearchBar.h"
#import "HZOrderAppointTool.h"
#import "HZSickNess.h"
@interface HZSickNameSearchViewController ()<HZSearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZSearchBar *searchBar;
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *searchTableView;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *patientSickNameArr;
@property (nonatomic, assign) BOOL isSearched;
@property (nonatomic, assign) BOOL isChangeTextNil;
@end

@implementation HZSickNameSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGFloat searBarX = kP(20);
    CGFloat searBarY = kP(14);
    CGFloat searBarW = HZScreenW - searBarX - kP(26);
    CGFloat searBarH = kP(56);
    HZSearchBar *searchBar = [[HZSearchBar alloc] initWithFrame:CGRectMake(searBarX, searBarY, searBarW, searBarH)];
    searchBar.delegate = self;
    searchBar.placeholder = @"请输入疾病名";
    self.navigationItem.titleView = searchBar;
    self.searchBar = searchBar;
    
    //
    HZTableView *searchTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    searchTableView.dataSource = self;
    searchTableView.delegate = self;
    searchTableView.tableFooterView = [UIView new];
    [self.view addSubview:searchTableView];
    self.searchTableView = searchTableView;
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    //
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrameNotification1:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

#pragma mark -- keyboardWillChangeFrameNotification1
- (void)keyboardWillChangeFrameNotification1:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    NSLog(@"------wo---%lf", keyboardRect.size.height);
    
    
    if (keyboardRect.origin.y == HZScreenH) {
        self.searchTableView.height = HZScreenH;
    } else {
        self.searchTableView.height = HZScreenH - keyboardRect.size.height;
    }
    
}

- (NSArray *)patientSickNameArr {
    if (!_patientSickNameArr) {
        _patientSickNameArr = [NSArray array];
    }
    return _patientSickNameArr;
}

#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.patientSickNameArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    HZSickNess *sickNess = self.patientSickNameArr[indexPath.row];
    cell.textLabel.text = sickNess.diseaseName;
    return cell;
}

#pragma mark -- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(passPatientSickNess:)]) {
        HZSickNess *sickNess = self.patientSickNameArr[indexPath.row];
        [self.delegate passPatientSickNess:sickNess];
    }

    [self cancelBtnClick:self.searchBar];
}

#pragma mark -- 去掉返回按钮
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationItem.hidesBackButton = YES;
    self.searchBar.isBecomeFirstResponder = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    self.searchBar.isBecomeFirstResponder = NO;
}
#pragma mark -- HZSearchBarDelegate
- (void)cancelBtnClick:(HZSearchBar *)searBar {
    [self.navigationController popViewControllerAnimated:YES];
}

//动态搜索
- (void)changeText:(NSString *)changeText {
    [self getSearchResult:self.searchBar];
}

- (void)getSearchResult:(HZSearchBar *)searBar {
    
//    NSLog(@"-----aa----%@", searBar.textFieldText);
    
    [HZOrderAppointTool getSickNameWithSickNameKeyStr:searBar.textFieldText success:^(id responseObject) {
//        NSLog(@"-----we----%@", responseObject);
        
        NSDictionary *resultsDict = responseObject[@"results"];
        NSArray *resultArr = resultsDict[@"result"];
        
        self.patientSickNameArr = [HZSickNess mj_objectArrayWithKeyValuesArray:resultArr];
        
//        NSLog(@"-----dd----%@", self.patientSickNameArr);
        [self.searchTableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"-------er--%@", error);
    }];
    
}


- (void)dealloc {
//    [self.searchBar removeFromSuperview];
//    self.searchBar = nil;
    self.searchBar.delegate = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
