//
//  HZSickNameSearchViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/3.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HZSickNess;
@protocol HZSickNameSearchViewControllerDelegate <NSObject>

- (void)passPatientSickNess:(HZSickNess *)patientSickNess;

@end
@interface HZSickNameSearchViewController : HZViewController
@property (nonatomic, weak) id<HZSickNameSearchViewControllerDelegate>delegate;
@end
