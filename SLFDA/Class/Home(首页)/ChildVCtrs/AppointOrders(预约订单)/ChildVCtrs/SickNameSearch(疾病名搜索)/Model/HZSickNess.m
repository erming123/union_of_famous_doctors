//
//  HZSickNess.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/6.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZSickNess.h"
@implementation HZSickNess
- (NSString *)description {
    return [NSString stringWithFormat:@"diseaseCode:%@----diseaseName:%@-------diseaseType:%@, icd10:%@, icd9=====%@, orderby=====%@", self.diseaseCode, self.diseaseName, self.diseaseType, self.icd10, self.icd9, self.orderby];
}
@end
