//
//  HZSickNess.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/6.
//  Copyright © 2017年 huazhuo. All rights reserved.
//


#import "HZJsonModel.h"
@interface HZSickNess : HZJsonModel
@property (nonatomic, copy) NSString *diseaseCode;//
@property (nonatomic, copy) NSString *diseaseName;
@property (nonatomic, copy) NSString *diseaseType;
@property (nonatomic, copy) NSString *icd10;
@property (nonatomic, copy) NSString *icd9;
@property (nonatomic, copy) NSString *orderby;
@end
