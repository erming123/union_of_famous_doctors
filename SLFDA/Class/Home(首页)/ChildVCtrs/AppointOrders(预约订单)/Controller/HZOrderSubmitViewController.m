//
//  HZOrderSubmitViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZOrderSubmitViewController.h"
#import "HZSickDescribCell.h"
#import "HZPatientSummitInforCell.h"
#import "HZAlertTextField.h"
#import "HZOrderSubmitDoneTableViewController.h"
#import "HZSickNameSearchViewController.h"
#import "HZUser.h"

//
//
#import "HZOrderAppointTool.h"
#import "HZPatient.h"// 基本可不用
#import "HZSickNess.h"

//条码控制器
#import "HZScanToolViewController.h"
//
#import "IQKeyboardManager.h"
@interface HZOrderSubmitViewController ()<UITableViewDataSource, UITableViewDelegate, HZAlertTextFieldDelegate, HZSickDescribCellDelegate, HZSickNameSearchViewControllerDelegate, HZPatientSummitInforCellDelegate>
//** <#注释#>*/
@property (nonatomic, strong) HZTableView *submitTabaleView;
//** <#注释#>*/
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *submitBtn;
//** <#注释#>*/
@property (nonatomic, strong) UITextView *sickDescribTextView;
//** 卡号*/
@property (nonatomic, weak) HZAlertTextField *outPatientCodeAlertTextField;
//** 优惠码*/
@property (nonatomic, strong) HZAlertTextField *privilegeNumberAlertTextField;

//** <#注释#>*/
@property (nonatomic, strong) HZPatient *patient;
//** <#注释#>*/
@property (nonatomic, strong) HZSickNess *patientSickNess;
@property (nonatomic, copy) NSString *patientInforStr;
@property (nonatomic, copy) NSString *outPatientCodeStr;
@property (nonatomic, copy) NSString *patientSickNameStr;
@property (nonatomic, copy) NSString *expertPrice;
@property (nonatomic, copy) NSString *sickNessDesStr;
@property (nonatomic, copy) NSString *privilegeNumberStr;

@property (nonatomic, assign) BOOL isPrivilegeNumberEnAbled;

//** <#注释#>*/
@property (nonatomic, strong) HZSickNameSearchViewController *sickNameSearchVCtr;
//** <#注释#>*/
@property (nonatomic, strong) HZSickDescribCell *sickDescribCell;
//** <#注释#>*/
@property (nonatomic, strong) HZPatient *paramPatient;
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
@property (nonatomic, assign) BOOL userHasDataButted;
//** <#注释#>*/
@property (nonatomic, strong) UIColor *seprateLineColor;
@end

@implementation HZOrderSubmitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //
    NSString *dataButtedVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"dataButtedVersion"];
    self.userHasDataButted = ![dataButtedVersion isEqualToString:@"PRIMARY"];
    
    if (self.userHasDataButted == NO) {
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.view = scrollView;
    }
    
    //
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"预约单提交";
    
    //
    //
    self.expertPrice = [self getPriceStrWithJobStr:self.expert.remoteExpertLevel];
    self.outPatientCodeStr = @"输入就诊卡号";
    //
    HZTableView *submitTabaleView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    submitTabaleView.height = self.userHasDataButted ? self.view.height - SafeAreaBottomHeight : self.view.height - SafeAreaTopHeight - SafeAreaBottomHeight;
    submitTabaleView.showsVerticalScrollIndicator = NO;
    submitTabaleView.dataSource = self;
    submitTabaleView.delegate = self;
    [self.view addSubview:submitTabaleView];
    self.submitTabaleView = submitTabaleView;
    
   
    
    //
    CGFloat btnBgViewH = kP(128);
    CGFloat btnBgViewW = self.view.width;
    CGFloat btnBgViewX = 0;
//    if (self.userHasDataButted == NO) {
//        //
        [self.submitTabaleView layoutIfNeeded];
//    }
    CGFloat btnBgViewY = self.submitTabaleView.contentSize.height - btnBgViewH;
    UIView *btnBgView = [[UIView alloc] initWithFrame:CGRectMake(btnBgViewX, btnBgViewY, btnBgViewW, btnBgViewH)];
    btnBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    btnBgView.layer.shadowOffset = CGSizeMake(0, - kP(2));
    btnBgView.layer.shadowOpacity = 0.1;
    btnBgView.layer.shadowRadius = kP(2);
    btnBgView.backgroundColor = [UIColor whiteColor];
    [self.submitTabaleView addSubview:btnBgView];

    //
    CGFloat submitBtnX = kP(34);
    CGFloat submitBtnW = self.view.width - 2 * submitBtnX;
    CGFloat submitBtnY = kP(16);
    CGFloat submitBtnH = btnBgViewH - 2 * submitBtnY;
    UIButton *submitBtn = [[UIButton alloc] initWithFrame:CGRectMake(submitBtnX, submitBtnY, submitBtnW, submitBtnH)];
    [submitBtn setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#CBCBCB" alpha:1.0]] forState:UIControlStateDisabled];
    [submitBtn setBackgroundImage:[UIImage createImageWithColor:kGreenColor] forState:UIControlStateNormal];
    [submitBtn setTitle:@"提交预约单" forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateNormal];
    submitBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
    submitBtn.layer.cornerRadius = kP(10);
    submitBtn.clipsToBounds = YES;
    [submitBtn addTarget:self action:@selector(submitBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    submitBtn.enabled = NO;
    [btnBgView addSubview:submitBtn];
    self.submitBtn = submitBtn;
//
    //
    HZAlertTextField *outPatientCodeAlertTextField = [HZAlertTextField new];
    outPatientCodeAlertTextField.hidden = YES;
    outPatientCodeAlertTextField.delegate = self;
    outPatientCodeAlertTextField.title = @"请输入或扫描就诊卡号";
    outPatientCodeAlertTextField.isKeyboardNumberType = NO;
    outPatientCodeAlertTextField.needScan = YES;
    //此处修改 加在控制器的View上
    [self.view addSubview:outPatientCodeAlertTextField];
    self.outPatientCodeAlertTextField = outPatientCodeAlertTextField;
    
    //
    HZAlertTextField *privilegeNumberAlertTextField = [HZAlertTextField new];
    privilegeNumberAlertTextField.hidden = YES;
    privilegeNumberAlertTextField.delegate = self;
    privilegeNumberAlertTextField.title = @"优惠码";
    privilegeNumberAlertTextField.needScan = NO;
    [self.navigationController.view addSubview:privilegeNumberAlertTextField];
    self.privilegeNumberAlertTextField = privilegeNumberAlertTextField;
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    //
    //    self.patientSickNess.diseaseName = @"疾病名"
    
    NSLog(@"-----expert----%@", self.expert);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(turnSubmitBtnEnable:) name:@"submitBtnShouldEnable" object:nil];
    //
    //
    self.paramPatient = [HZPatient new];
    self.patientSickNess = [HZSickNess new];
}

- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)turnSubmitBtnEnable:(NSNotification *)notification {
    
    NSDictionary *paramDict = notification.object;
    if (paramDict) {
        
        self.paramPatient.BRDAXM = paramDict[@"patientName"];
        self.paramPatient.BRDAXB = paramDict[@"patientGender"];
        self.paramPatient.BRCSRQ = paramDict[@"patientAge"];
        self.submitBtn.enabled = YES;
    } else {
        self.submitBtn.enabled = NO;
    }
}
#pragma mark -- expert
- (void)setExpert:(HZUser *)expert {
    _expert = expert;
}

- (void)setPriceList:(NSArray *)priceList {
    
    _priceList = priceList;
}

#pragma mark -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 3 || section == 4) {
        return 1;
    } else if (section == 1) {
        return  self.userHasDataButted ? 2 : 1;
    }
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"submitTabaleViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:kP(32)];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:kP(32)];
    switch (indexPath.section) {
        case 0: {
            
            if (indexPath.row == 0) {
                cell.textLabel.text = @"已选择专家";
                cell.detailTextLabel.text = self.expert.userName;
            } else {
                cell.textLabel.text = @"价格";
                cell.detailTextLabel.text = self.expertPrice;
            }
        }
            break;
        case 1: {
            if (indexPath.row == 0) {// 0.
                if (self.userHasDataButted) {
                    cell.textLabel.text = @"就诊卡号";
                    cell.detailTextLabel.text = self.outPatientCodeStr;
                    return cell;
                } else {
                    
                    HZPatientSummitInforCell *patientSummitInforCell = [HZPatientSummitInforCell cellWithTableView:tableView];
//                    patientSummitInforCell.patient = self.patient;
                    patientSummitInforCell.seprateHLineColor = tableView.separatorColor;
                    patientSummitInforCell.delegate = self;
                    return patientSummitInforCell;
                }
            } else {// 1.
                if (self.userHasDataButted) {
                    
                    cell.textLabel.text = @"患者信息";
                    cell.detailTextLabel.text = self.patientInforStr;
                    return cell;
                } else {
                    
                    HZPatientSummitInforCell *patientSummitInforCell = [HZPatientSummitInforCell cellWithTableView:tableView];
//                    patientSummitInforCell.patient = self.patient;
                    patientSummitInforCell.seprateHLineColor = tableView.separatorColor;
                    patientSummitInforCell.delegate = self;
                    return patientSummitInforCell;
                }
                
            }
        }
            break;
        case 2: {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"疾病名";
                cell.detailTextLabel.text = self.patientSickNess.diseaseName ? self.patientSickNess.diseaseName : @"疾病名";
            } else {
                
                NSLog(@"---------33332222222-----");
                HZSickDescribCell *sickDescribCell = [HZSickDescribCell cellWithTableView:tableView];
                sickDescribCell.delegate = self;
                self.sickDescribCell = sickDescribCell;
                return sickDescribCell;
            }
        }
            break;
            
        case 3: {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"优惠码";
                cell.detailTextLabel.text = self.isPrivilegeNumberEnAbled ? @"本单已免费" : @"非必填";
            }
            
        }
            break;
            
        case 4: {
                cell.textLabel.text = @"";
                cell.detailTextLabel.text = @"";
        }
            break;
            
    }
    
    return cell;
    
}
#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2 && indexPath.row == 1) {
        return kP(374);
    } else if (indexPath.section == 1 && !self.userHasDataButted) {
        return kP(300);
    } else if (indexPath.section == 4) {
        return kP(128);
    }
    return kP(100);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 4) {
        return 0.1;
    }
    return kP(24);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 4) {
        return 0.1;
    }
    return kP(52);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *title;
    switch (section) {
        case 0:
            title = @"";
            break;
        case 1:
            title = @"就诊人信息";
            break;
        case 2:
            title = @"疾病信息";
            break;
        case 3:
            title = @"免单优惠";
            break;
    }
    return title;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {
            if (self.userHasDataButted) {
                self.outPatientCodeAlertTextField.hidden = NO;
                self.outPatientCodeAlertTextField.isBecomeFirstResponder = YES;
            } 
        }
    } else if(indexPath.section == 2 && indexPath.row == 0){
        HZSickNameSearchViewController *sickNameSearchVCtr = [HZSickNameSearchViewController new];
        sickNameSearchVCtr.delegate = self;
        [self.navigationController pushViewController:sickNameSearchVCtr animated:NO];
//        self.sickNameSearchVCtr = sickNameSearchVCtr;
    } else if(indexPath.section == 3) {
        self.privilegeNumberAlertTextField.hidden = NO;
        self.privilegeNumberAlertTextField.isBecomeFirstResponder = YES;
    }
}
#pragma mark -- hideAlertTextField
- (void)hideAlertTextField:(HZAlertTextField *)alertTextField {
    alertTextField.hidden = YES;
    self.submitTabaleView.y = 0;
}

- (void)getPatientInfor:(HZAlertTextField *)alertTextField {
    
    [self.activityIndicator startAnimating];
    self.view.userInteractionEnabled = NO;
    if (alertTextField == self.outPatientCodeAlertTextField) {
        self.outPatientCodeAlertTextField.hidden = YES;
        
        NSLog(@"---------点击了outPatientCodeAlertTextField=====%@", alertTextField.textFieldText);
        NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
        
        NSLog(@"--eeeeeee222222--%@",userDict);
        [HZOrderAppointTool getPatientInforWithOutPatientCode:alertTextField.textFieldText hospitalId:userDict[@"hospitalId"] success:^(id responseObject) {
            NSLog(@"-----patientInfor--11--%@", [responseObject getDictJsonStr]);

            
            
            if ([responseObject[@"state"] isEqual:@200]) {// 原来是msg
                [self.activityIndicator stopAnimating];
                
                NSLog(@"-----patientInfor----%@", responseObject);
                
                NSDictionary *resultsDict = responseObject[@"results"];
                
                if ([[resultsDict[@"patientInfo"] class] isSubclassOfClass:NSArray.class]) {
                    NSLog(@"---------ssss---%@", [resultsDict getDictJsonStr]);
                    
                    
                    NSArray *patientInfoArr = resultsDict[@"patientInfo"];
                    
                    if (patientInfoArr == nil || patientInfoArr.count == 0) {
                        [self setAlertWithAlertStr:@"未找到对应患者, 请手动收入"];
                        self.view.userInteractionEnabled = YES;
                        return;
                    }
                    
                    NSDictionary *patientDict = patientInfoArr[0];
                    HZPatient *patient = [HZPatient mj_objectWithKeyValues:patientDict];
                    patient = [HZPatient mj_objectWithKeyValues:[patient propertyValueDict]];
                    self.patient = patient;
                    
                    //2.
                    self.outPatientCodeStr = alertTextField.textFieldText;
                    
                    if (self.userHasDataButted) {
                    self.patientInforStr = [NSString stringWithFormat:@"%@  %@  %@", self.patient.BRDAXM, [self.patient.BRDAXB isEqualToString:@"1"] ? @"男" : @"女", [self getAgeStr]];
                    }
                    NSIndexPath *indexPath1= [NSIndexPath indexPathForRow:0 inSection:1];  //你需要更新的组数中的cell
                    NSIndexPath *indexPath2= [NSIndexPath indexPathForRow:1 inSection:1];
                    [self.submitTabaleView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath1,indexPath2,nil] withRowAnimation:UITableViewRowAnimationNone];
                    
                    
                    self.submitBtn.enabled = ![NSString isBlankString:self.patient.BRDAXM] && ![NSString isBlankString:self.patient.BRDAXB] && ![NSString isBlankString:self.patient.BRCSRQ];
                    if (self.submitBtn.enabled) {
                        self.paramPatient = self.patient;
                    }
                    
                    
                    NSLog(@"------ssaa---%@", self.patientSickNess.diseaseName);
                    self.view.userInteractionEnabled = YES;
                } else {
                    NSLog(@"---------ssss1111");
                    
                    [self setAlertWithAlertStr:@"未找到对应患者，请手动输入"];
                    self.view.userInteractionEnabled = YES;
                    
                }
                
                
                
            } else {
                [self setAlertWithAlertStr:@"未找到对应患者，请手动输入"];
                self.view.userInteractionEnabled = YES;
            }
            
            [self.activityIndicator stopAnimating];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"-----333----%@", error);
            self.view.userInteractionEnabled = YES;
            [self.activityIndicator stopAnimating];
        }];
        
        
    } else if (alertTextField == self.privilegeNumberAlertTextField) {
        self.privilegeNumberAlertTextField.hidden = YES;
        NSLog(@"---------点击了privilegeNumberAlertTextField========%@", alertTextField.textFieldText);
        
        self.submitTabaleView.y = 0;
        [HZOrderAppointTool justPrivilegeNumberEnAbledWithPrivilegeNumber:alertTextField.textFieldText success:^(id responseObject) {
            NSLog(@"-----y----%@", responseObject);
            
            
            if ([responseObject[@"state"] isEqual:@200]) {
                self.isPrivilegeNumberEnAbled = YES;
                NSIndexPath *indexPath= [NSIndexPath indexPathForRow:0 inSection:3];  //你需要更新的组数中的cell
                [self.submitTabaleView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
                self.privilegeNumberStr = alertTextField.textFieldText;
            } else {
                self.isPrivilegeNumberEnAbled = NO;
                
                [self setAlertWithAlertStr:@"优惠码不可用"];
            }
            
            [self.activityIndicator stopAnimating];
            self.view.userInteractionEnabled = YES;
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"------x---%@", error);
            self.view.userInteractionEnabled = YES;
            [self.activityIndicator stopAnimating];
        }];
    }
    
    [self revertMyTableView];
}

//跳转条码扫描
- (void)scanBtnClickAction{
    HZScanToolViewController *barCodeVC= [[HZScanToolViewController alloc]initWithScanType:scanTypeBarCode];
    [self.navigationController pushViewController:barCodeVC animated:YES];
    barCodeVC.scanResultInfoBlock = ^(NSString *barCode, NSDictionary *bankInfo) {
        
        self.outPatientCodeAlertTextField.textFieldText = barCode;
        [self revertMyTableView];
    };
}

#pragma mark -- setAlertWithAlertStr
- (void)setAlertWithAlertStr:(NSString *)alertStr {
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:alertStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.activityIndicator stopAnimating];
    }];
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
    }
    [alertCtr addAction:ensureAlertAction];
    [self presentViewController:alertCtr animated:YES completion:nil];
}

#pragma mark -- HZSickDescribCellDelegate
- (void)upTableView:(CGFloat)keyBoardH {
    
    [self.submitTabaleView setContentOffset:CGPointMake(0,kP(50))];
    self.submitTabaleView.y -= keyBoardH - kP(128);
}

- (void)revertTableView:(NSString *)textViewText {
    self.submitTabaleView.y = 0;
//    [self.submitTabaleView setContentOffset:CGPointMake(0, kP(128))];
    [self.submitTabaleView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    NSLog(@"-----dd----%@", textViewText);
    
    self.sickNessDesStr = textViewText;
}

- (void)revertMyTableView {
    self.submitTabaleView.y = 0;
    //    [self.submitTabaleView setContentOffset:CGPointMake(0, kP(128))];
    [self.submitTabaleView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark -- 取消键盘第一相应
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    self.sickDescribCell.becomeFirstResponder = NO;
}

#pragma mark -- submitBtnClick
- (void)submitBtnClick:(UIButton *)submitBtn {
    if (![self.paramPatient.BRCSRQ containsNumberString]) { //没有数字
        showToastMessage(@"患者年龄输入有误");
        return;
    }

    [self.activityIndicator startAnimating];
    self.expert.totalMoney = self.expertPrice;
    NSLog(@"-----ddss----%@", self.sickNessDesStr);
    
    // 判断是否含有emoji
    BOOL isContainsEmoji = [NSString stringContainsEmoji:self.sickNessDesStr];
    // 转emojiStr成unicodeStr
    if (isContainsEmoji) {
        self.sickNessDesStr = [self getUnicodeStrWithEmojiStr:self.sickNessDesStr];
    }
    
    
    [HZOrderAppointTool creatOrderWithExpert:self.expert patient:self.paramPatient sickNess:self.patientSickNess sickNessDes:self.sickNessDesStr  privilegeNumberStr:self.privilegeNumberStr success:^(id responseObject) {
        
        
        [self.activityIndicator stopAnimating];
        if ([responseObject[@"state"] isEqual:@200]) {
            
            NSLog(@"-----提交预约单成功----%@", responseObject);
            
            NSDictionary *resultsDict = responseObject[@"results"];
            NSString *bookingOrderId = resultsDict[@"bookingOrderId"];
            if ([resultsDict[@"payState"] isEqual:@004]) {
                HZOrderSubmitDoneTableViewController *orderSubmitDoneTVCtr = [[HZOrderSubmitDoneTableViewController alloc] initWithStyle:UITableViewStylePlain];
                orderSubmitDoneTVCtr.appointBookingOrderId = bookingOrderId;
                [self.navigationController pushViewController:orderSubmitDoneTVCtr animated:YES];
                return;
            }
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        } else if([responseObject[@"state"] isEqual:@401]){
            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"请重新登录" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            if (kCurrentSystemVersion >= 9.0) {
                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            }
            [alertCtr addAction:ensureAlertAction];
            [self presentViewController:alertCtr animated:YES completion:nil];
            
        } else {
            
            UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:responseObject[@"msg"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            if (kCurrentSystemVersion >= 9.0) {
                [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
            }
            [alertCtr addAction:ensureAlertAction];
            [self presentViewController:alertCtr animated:YES completion:nil];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self.activityIndicator stopAnimating];
        UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:@"连接服务器失败，请重试" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        if (kCurrentSystemVersion >= 9.0) {
            [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
        }
        [alertCtr addAction:ensureAlertAction];
        [self presentViewController:alertCtr animated:YES completion:nil];
    }];
}



#pragma mark -- getExpertPrice
- (NSString *)getPriceStrWithJobStr:(NSString *)remoteExpertLevel {
    
    NSString *priceStr;
    
    for (NSDictionary *dict in self.priceList) {
        
        //            NSLog(@"-----sssaaa----%@-----%@", remoteExpertLevel, dict[@"levelTypeCode"]);
        if ([remoteExpertLevel isEqual:dict[@"levelTypeCode"]]) {
            priceStr = dict[@"totalMoney"];
            priceStr = [NSString stringWithFormat:@"¥%@", priceStr];
        }
        
    }
    return priceStr;
}


#pragma mark -- HZSickNameSearchViewControllerDelegate
- (void)passPatientSickNess:(HZSickNess *)patientSickNess {
    self.patientSickNess = patientSickNess;
    
    //
    NSIndexPath *indexPath= [NSIndexPath indexPathForRow:0 inSection:2];  //你需要更新的组数中的cell
    [self.submitTabaleView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
   
    
    // 修改了一个bug！
    self.submitBtn.enabled = ![NSString isBlankString:self.paramPatient.BRDAXM] && ![NSString isBlankString:self.paramPatient.BRDAXB] && ![NSString isBlankString:self.paramPatient.BRCSRQ];
    
    //
    if (self.submitBtn.enabled) {
        self.paramPatient = self.patient;
    }
    
    if (self.userHasDataButted) {
    self.patientInforStr = [NSString stringWithFormat:@"%@  %@  %@", self.patient.BRDAXM, [self.patient.BRDAXB isEqualToString:@"1"] ? @"男" : @"女", [self getAgeStr]];
    }
}

#pragma mark -- getUnicodeStrWithEmojiStr
- (NSString *)getUnicodeStrWithEmojiStr:(NSString *)emojiStr {
    
    return [emojiStr stringByReplacingEmojiUnicodeWithCheatCodes];
}

#pragma mark -- getAgeStr
- (NSString *)getAgeStr {
    //1.
    NSDate *now = [NSDate date];
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"yyyy"];//
    
    NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
    [df2 setDateFormat:@"MMdd"];//MMdd
    
    NSString *yearStrNew = [df1 stringFromDate:now];
    NSString *monthStrNew = [df2 stringFromDate:now];
    
    //
    NSString *ageStr;
    NSInteger year = 0;
    NSInteger month = 0;
    if (self.patient.BRCSRQ.length == 0) {
        ageStr = @"";
    } else {
        NSString *yearStr = self.patient.BRCSRQ.length > 0 ? [self.patient.BRCSRQ substringWithRange:NSMakeRange(0, 4)] : @"";
        NSString *monthStr = self.patient.BRCSRQ.length > 0 ? [self.patient.BRCSRQ substringWithRange:NSMakeRange(5, 2)] : @"";
        NSString *dayStr = self.patient.BRCSRQ.length > 0 ? [self.patient.BRCSRQ substringWithRange:NSMakeRange(8, 2)] : @"";
        NSString *MDStr = [NSString stringWithFormat:@"%@%@", monthStr, dayStr];
        year = [yearStrNew intValue] - [yearStr intValue];
        month = [monthStrNew intValue] - [MDStr intValue];
        if (month < 0) {
            year -= 1;
        }
        ageStr = [NSString stringWithFormat:@"%ld岁", year];
    }
    
    return ageStr;
}


#pragma mark ----------------- contactUsAlert ------------------
- (void)showGenderSelectView {

    //
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
      
   
        HZPatientSummitInforCell *patientSummitInforCell = [self.submitTabaleView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        
        patientSummitInforCell.patientGenderStr = @"男";
        NSLog(@"---------BOOL------%@", patientSummitInforCell.isTextFieldEnable == YES ? @"YES":@"NO");
    }];
    
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        HZPatientSummitInforCell *patientSummitInforCell = [self.submitTabaleView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        patientSummitInforCell.patientGenderStr = @"女";
        NSLog(@"---------BOOL------%@", patientSummitInforCell.isTextFieldEnable == YES ? @"YES":@"NO");
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    
    [alertCtr addAction:cameraAction];
    [alertCtr addAction:libraryAction];
    [alertCtr addAction:cancelAction];
    
    //
    [self presentViewController:alertCtr animated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
