//
//  HZPatient.m
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "HZPatient.h"

@implementation HZPatient
- (NSString *)description {


    return [NSString stringWithFormat:@"name:%@----gender:%@-------idCard:%@,----phone：%@------------bedNum:%@",  self.name, self.gender, self.idCard, self.phone, self.bedNum];
}

- (NSString *)namePinyinStr {
    
    if (!_namePinyinStr) {
        
        if (self.name) {
            _namePinyinStr = [self transform:self.name];
        } else if(self.BRDAXM) {
            _namePinyinStr = [self transform:self.BRDAXM];
        }
        
    }
    
    return _namePinyinStr;
}

- (NSString *)transform:(NSString *)chinese {
    //        kCFStringTransformStripCombiningMarks 去掉音标
    //        kCFStringTransformMandarinLatin  有音标
    NSMutableString *pinyin = [chinese mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripCombiningMarks, NO);
    NSLog(@"-----pinyin-----------%@", pinyin);
    return [[pinyin uppercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
}

@end
