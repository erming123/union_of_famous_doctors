//
//  HZPatient.h
//  HZHttpsSqliteDemo
//
//  Created by 季怀斌 on 2016/11/16.
//  Copyright © 2016年 huazhuo. All rights reserved.
//


#import "HZJsonModel.h"
@interface HZPatient : HZJsonModel
@property (nonatomic, copy) NSString *BRDAXM;// 姓名
@property (nonatomic, copy) NSString *BRDAXB;// 性别
@property (nonatomic, copy) NSString *BRSFZH;// 身份证号解析出年龄
@property (nonatomic, copy) NSString *BRDAMZ;// 民族
@property (nonatomic, copy) NSString *BRDHZC;// 手机
@property (nonatomic, copy) NSString *BRJZHM;// 手机
@property (nonatomic, copy) NSString *BRCSRQ;// 出生年月
@property (nonatomic, copy) NSString *BRJZLX;// jz类型：门诊／住院...
//@property (nonatomic, copy) NSString *str;
//@property (nonatomic, copy) NSString *str;

//是否是关注的病人
@property (nonatomic,assign) BOOL starred;

//
@property (nonatomic, copy) NSString *age;// 年龄
@property (nonatomic, copy) NSString *bedNum;// 床号
@property (nonatomic, copy) NSString *docJobNum;// 医生公号
@property (nonatomic, copy) NSString *docName;// 医生名字
@property (nonatomic, copy) NSString *gender;// 性别
@property (nonatomic, copy) NSString *hisInHospitalNum;//
@property (nonatomic, copy) NSString *idCard;// 身份证号
@property (nonatomic, copy) NSString *inHospitalNum;//
@property (nonatomic, copy) NSString *name;// 患者姓名
@property (nonatomic, copy) NSString *patientRecordId;//
@property (nonatomic, copy) NSString *patientStatus;// 1:住院
@property (nonatomic, copy) NSString *phone;// 患者手机号
@property (nonatomic, copy) NSString *clinicId;// 诊所id
//
@property (nonatomic, copy) NSString *namePinyinStr;
@end
