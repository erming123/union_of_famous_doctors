//
//  HZPayOrder.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/7/18.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZPayOrder.h"

@implementation HZPayOrder

- (NSString *)description
{
    return [NSString stringWithFormat:@"bookingOrderId:%@-----endTime:%@-----goodsId:%@-----payState:%@-----price:%@-----startTime:%@", self.bookingOrderId, self.endTime, self.goodsId, self.payState, self.price, self.startTime];
}
@end
