//
//  HZAlertPayView.h
//  HZPayDemo
//
//  Created by 季怀斌 on 2017/7/17.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HZPayOrder;

@protocol HZAlertPayViewDelegate <NSObject>

- (void)refreshOrderStatus;
- (void)returnBackHome;
@end
@interface HZAlertPayView : UIView

- (instancetype)initWithPayOrder:(HZPayOrder *)payOrder;

- (void)setMyTimerStart;
//- (void)setMyTimerStop;
@property (nonatomic, assign) BOOL isShowWeChat;
@property (nonatomic, assign) BOOL isFromAppointSuccess;
//** <#注释#>*/
@property (nonatomic, strong) NSTimer *timer;
//
@property (nonatomic, weak) id<HZAlertPayViewDelegate>delegate;
@end
