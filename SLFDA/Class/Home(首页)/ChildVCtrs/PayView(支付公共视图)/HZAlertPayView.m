//
//  HZAlertPayView.m
//  HZPayDemo
//
//  Created by 季怀斌 on 2017/7/17.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZAlertPayView.h"
#import "HZPayOrder.h"
#import "HZOrderHttpsTool.h"

NS_ASSUME_NONNULL_BEGIN
@interface HZAlertPayView ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *payCountLabel;
@property (nonatomic, strong) UIButton *weChatPayBtn;
@property (nonatomic, strong) UIButton *aliPayBtn;
@property (nonatomic, strong) UILabel *payStyleLabel;
@property (nonatomic, strong) UIImageView *QRCodeImageView;
@property (nonatomic, strong) UILabel *timeAlertLabel;
//
@property (nonatomic, strong) UIImageView *paySuccessImageView;
@property (nonatomic, strong) UILabel *paySuccessLabel;
@property (nonatomic, strong) UILabel *payAlertLabel;
@property (nonatomic, strong) UIButton *paySuccessBtn;
//** <#注释#>*/
@property (nonatomic, strong) UIButton *hiddenBtn;

//
@property (nonatomic, strong) HZPayOrder *payOrder;
//


@property (nonatomic, assign) NSInteger timeCount;
@end
NS_ASSUME_NONNULL_END
@implementation HZAlertPayView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}


- (UIView *)bgView {
    if (!_bgView) {
        CGFloat bgViewX = kP(40);
        CGFloat bgViewY = kP(194);
        CGFloat bgViewW = kP(674);
        CGFloat bgViewH = kP(1010);
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(bgViewX, bgViewY, bgViewW, bgViewH)];
        _bgView.layer.cornerRadius = kP(6);
        _bgView.clipsToBounds = YES;
    }
    
    return _bgView;
}


- (UIButton *)hiddenBtn {
    
    if (!_hiddenBtn) {
        _hiddenBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.bgView.right - kP(64), 30, kP(64), kP(64))];
        [_hiddenBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [_hiddenBtn addTarget:self action:@selector(clickToHiden:) forControlEvents:UIControlEventTouchUpInside];
        //        _hiddenBtn.backgroundColor = [UIColor redColor];
    }
    
    return _hiddenBtn;
}



- (UILabel *)payCountLabel {
    
    if (!_payCountLabel) {
        _payCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(kP(0), kP(60), self.bgView.width, kP(55))];
        _payCountLabel.font = [UIFont systemFontOfSize:kP(32)];
        _payCountLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _payCountLabel;
}

- (UIButton *)weChatPayBtn {
    
    if (!_weChatPayBtn) {
        CGFloat weChatPayBtnW = kP(316);
        CGFloat weChatPayBtnH = kP(88);
        CGFloat weChatPayBtnX = self.bgView.width * 0.5 - weChatPayBtnW * 0.5;
        CGFloat weChatPayBtnY = self.payCountLabel.bottom + kP(68);
        _weChatPayBtn = [[UIButton alloc] initWithFrame:CGRectMake(weChatPayBtnX, weChatPayBtnY, weChatPayBtnW, weChatPayBtnH)];
        _weChatPayBtn.adjustsImageWhenHighlighted = NO;
        [_weChatPayBtn setImage:[UIImage imageNamed:@"wechatPay"] forState:UIControlStateNormal];
        [_weChatPayBtn setTitle:@"微信支付" forState:UIControlStateNormal];
        _weChatPayBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
        [_weChatPayBtn setTitleColor:[UIColor colorWithHexString:@"#4A4A4A" alpha:1.0] forState:UIControlStateNormal];
        [_weChatPayBtn setTitleColor:[UIColor colorWithHexString:@"#00A4CA" alpha:1.0] forState:UIControlStateSelected];
        _weChatPayBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, kP(35));
        _weChatPayBtn.titleEdgeInsets = UIEdgeInsetsMake(0, kP(20), 0, 0);
        _weChatPayBtn.backgroundColor = [UIColor whiteColor];
        _weChatPayBtn.layer.borderWidth = kP(2);
        _weChatPayBtn.layer.borderColor = [UIColor colorWithHexString:@"#00A4CA" alpha:1.0].CGColor;
        _weChatPayBtn.layer.cornerRadius = kP(10);
        _weChatPayBtn.clipsToBounds = YES;
        [_weChatPayBtn addTarget:self action:@selector(clickToWeChatPay:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _weChatPayBtn;
}


- (UIButton *)aliPayBtn {
    
    if (!_aliPayBtn) {
        CGFloat aliPayBtnW = kP(316);
        CGFloat aliPayBtnH = kP(88);
        CGFloat aliPayBtnX = self.bgView.width * 0.5 - aliPayBtnW * 0.5;
        CGFloat aliPayBtnY = self.weChatPayBtn.bottom + kP(42);
        _aliPayBtn = [[UIButton alloc] initWithFrame:CGRectMake(aliPayBtnX, aliPayBtnY, aliPayBtnW, aliPayBtnH)];
        _aliPayBtn.adjustsImageWhenHighlighted = NO;
        [_aliPayBtn setImage:[UIImage imageNamed:@"alipay"] forState:UIControlStateNormal];
        [_aliPayBtn setTitle:@"支付宝支付" forState:UIControlStateNormal];
        //        _aliPayBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, kP(50));
        _aliPayBtn.titleEdgeInsets = UIEdgeInsetsMake(0, kP(40), 0, 0);
        _aliPayBtn.titleLabel.font = [UIFont systemFontOfSize:kP(32)];
        [_aliPayBtn setTitleColor:[UIColor colorWithHexString:@"#4A4A4A" alpha:1.0] forState:UIControlStateNormal];
        [_aliPayBtn setTitleColor:[UIColor colorWithHexString:@"#00A4CA" alpha:1.0] forState:UIControlStateSelected];
        _aliPayBtn.backgroundColor = [UIColor whiteColor];
        _aliPayBtn.layer.borderWidth = kP(2);
        _aliPayBtn.layer.borderColor = [UIColor colorWithHexString:@"#00A4CA" alpha:1.0].CGColor;
        _aliPayBtn.layer.cornerRadius = kP(10);
        _aliPayBtn.clipsToBounds = YES;
        [_aliPayBtn addTarget:self action:@selector(clickToAliPay:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _aliPayBtn;
}


- (UILabel *)payStyleLabel {
    
    if (!_payStyleLabel) {
        _payStyleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kP(0), self.aliPayBtn.bottom + kP(68), self.bgView.width, kP(38))];
        _payStyleLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
        _payStyleLabel.font = [UIFont systemFontOfSize:kP(32)];
        _payStyleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _payStyleLabel;
}

- (UIImageView *)QRCodeImageView {
    
    if (!_QRCodeImageView) {
        
        CGFloat QRCodeImageViewWH = kP(352);
        CGFloat QRCodeImageViewX = self.bgView.width * 0.5 - QRCodeImageViewWH * 0.5;
        CGFloat QRCodeImageViewY = self.payStyleLabel.bottom + kP(26);
        _QRCodeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(QRCodeImageViewX, QRCodeImageViewY, QRCodeImageViewWH, QRCodeImageViewWH)];
        
        //
        _QRCodeImageView.backgroundColor = [UIColor whiteColor];
        
    }
    
    return _QRCodeImageView;
}



- (UILabel *)timeAlertLabel {
    if (!_timeAlertLabel) {
        _timeAlertLabel = [[UILabel alloc] initWithFrame:CGRectMake(kP(0), self.QRCodeImageView.bottom + kP(20), self.bgView.width, kP(38))];
        _timeAlertLabel.text = @"此二维码有效期为2小时";
        _timeAlertLabel.font = [UIFont systemFontOfSize:kP(28)];
        _timeAlertLabel.textColor = [UIColor colorWithHexString:@"#9B9B9B" alpha:1.0];
        _timeAlertLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _timeAlertLabel;
}




- (UIImageView *)paySuccessImageView {
    
    if (!_paySuccessImageView) {
        
        CGFloat paySuccessImageViewWH = kP(194);
        CGFloat paySuccessImageViewY = kP(176);
        CGFloat paySuccessImageViewX = self.bgView.width * 0.5 - paySuccessImageViewWH * 0.5;
        _paySuccessImageView = [[UIImageView alloc] initWithFrame:CGRectMake(paySuccessImageViewX, paySuccessImageViewY, paySuccessImageViewWH, paySuccessImageViewWH)];
        _paySuccessImageView.image = [UIImage imageNamed:@"paySuccess"];
        _paySuccessImageView.hidden = YES;
    }
    
    return _paySuccessImageView;
}

- (UILabel *)paySuccessLabel {
    if (!_paySuccessLabel) {
        _paySuccessLabel = [UILabel new];
        _paySuccessLabel.font = [UIFont systemFontOfSize:kP(36)];
        _paySuccessLabel.textColor = [UIColor colorWithHexString:@"#000000" alpha:1.0];
        _paySuccessLabel.text = @"支付成功";
        [_paySuccessLabel sizeToFit];
        _paySuccessLabel.y = self.paySuccessImageView.bottom + kP(74);
        _paySuccessLabel.x = self.bgView.width * 0.5 - _paySuccessLabel.width * 0.5;
        _paySuccessLabel.hidden = YES;
    }
    
    return _paySuccessLabel;
}


- (UILabel *)payAlertLabel {
    if (!_payAlertLabel) {
        _payAlertLabel = [UILabel new];
        _payAlertLabel.font = [UIFont systemFontOfSize:kP(32)];
        _payAlertLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
        _payAlertLabel.text = @"请等待工作人员安排专家";
        [_payAlertLabel sizeToFit];
        _payAlertLabel.y = self.paySuccessLabel.bottom + kP(28);
        _payAlertLabel.x = self.bgView.width * 0.5 - _payAlertLabel.width * 0.5;
        _payAlertLabel.hidden = YES;
    }
    
    return _payAlertLabel;
}


- (UIButton *)paySuccessBtn {
    if (!_paySuccessBtn) {
        
        
        CGFloat paySuccessBtnW = kP(574);
        CGFloat paySuccessBtnH = kP(106);
        CGFloat paySuccessBtnX = self.bgView.width * 0.5 - paySuccessBtnW * 0.5;
        CGFloat paySuccessBtnY = self.bgView.height - paySuccessBtnH - kP(72);
        _paySuccessBtn = [[UIButton alloc] initWithFrame:CGRectMake(paySuccessBtnX, paySuccessBtnY, paySuccessBtnW, paySuccessBtnH)];
        [_paySuccessBtn setTitle:@"完成" forState:UIControlStateNormal];
        [_paySuccessBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateNormal];
        _paySuccessBtn.titleLabel.font = [UIFont systemFontOfSize:kP(40)];
        _paySuccessBtn.layer.cornerRadius = kP(5);
        _paySuccessBtn.clipsToBounds = YES;
        [_paySuccessBtn addTarget:self action:@selector(paySuccessBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _paySuccessBtn.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
        _paySuccessBtn.hidden = YES;
    }
    
    return _paySuccessBtn;
}

//
- (void)setIsShowWeChat:(BOOL)isShowWeChat {
    
    _isShowWeChat = isShowWeChat;
    
    if (isShowWeChat) {
        [self clickToWeChatPay:self.weChatPayBtn];
    }
}

//
- (void)setIsFromAppointSuccess:(BOOL)isFromAppointSuccess {
    _isFromAppointSuccess = isFromAppointSuccess;
}
#pragma mark -- setUpSubViews
- (void)setUpSubViews {
    
    
    CGSize size = [UIScreen mainScreen].bounds.size;
    self.size = size;
    self.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.5];
    //    self.hidden = YES;
    //
    //    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:@"需要支付金额:  ¥199.00"];
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"需要支付金额:  ¥%@", self.payOrder.price]];
    [AttributedStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:kP(40)] range:NSMakeRange(8, AttributedStr.length - 8)];
    
    [AttributedStr addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(8, AttributedStr.length - 8)];
    self.payCountLabel.attributedText = AttributedStr;
    [self.bgView addSubview:self.payCountLabel];
    
    //
    [self.bgView addSubview:self.weChatPayBtn];
    
    //
    [self.bgView addSubview:self.aliPayBtn];
    
    //
    [self.bgView addSubview:self.payStyleLabel];
    
    //
    [self.bgView addSubview:self.QRCodeImageView];
    
    //
    [self.bgView addSubview:self.timeAlertLabel];
    
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    [self.bgView addSubview:self.paySuccessImageView];
    
    //
    [self.bgView addSubview:self.paySuccessLabel];
    
    //
    [self.bgView addSubview:self.payAlertLabel];
    
    
    //
    [self.bgView addSubview:self.paySuccessBtn];
    
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    //
    
    [self addSubview:self.hiddenBtn];
    //
    //    if (<#condition#>) {
    //        <#statements#>
    //    }
    [self clickToWeChatPay:self.weChatPayBtn];
    // 创建一个定时器
    self.timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(getPayOrderState) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (instancetype)initWithPayOrder:(HZPayOrder *)payOrder {
    self.payOrder = payOrder;
    return [self init];
}
#pragma mark -- class
- (void)clickToHiden:(UIButton *)cancelBtn {
    self.hidden = YES;
    [self.timer invalidate];
    self.timer = nil;
    self.timeCount = 0;
}


#pragma mark -- payCount

#pragma mark -- clickToWeChatPay
- (void)clickToWeChatPay:(UIButton *)weChatPayBtn { // 根据已经获取到的imageData显示二维码
    
    
    NSString *QRImageId = [NSString stringWithFormat:@"WeChatQRImage%@", self.payOrder.bookingOrderId];
    NSData *QRImageData = [[NSUserDefaults standardUserDefaults] objectForKey:QRImageId];
    
    if (!weChatPayBtn.selected) { // 防止多次点击自己
        NSLog(@"weChatPayBtn");
        NSLog(@"QRImageData:%@", QRImageData);
        weChatPayBtn.layer.borderColor = [UIColor colorWithHexString:@"#05B1C2" alpha:1.0].CGColor;
        self.aliPayBtn.layer.borderColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0].CGColor;
        self.QRCodeImageView.image = [UIImage imageWithData:QRImageData];
        weChatPayBtn.selected = YES;
        
        //
        NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:@"请让患者打开微信扫码支付"];
        [AttributedStr addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(6, 2)];
        self.payStyleLabel.attributedText = AttributedStr;
        
        //
//        // 创建一个定时器
//        self.timer = [NSTimer timerWithTimeInterval:3.0 target:self selector:@selector(getPayOrderState) userInfo:nil repeats:YES];
//        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
    
    self.aliPayBtn.selected = NO;
    
//    //
//    [self.timer invalidate];
//    //    self.timer = nil;
//    self.timeCount = 0;
   
    
}

- (void)clickToAliPay:(UIButton *)aliPayBtn {
    
    NSString *QRImageId = [NSString stringWithFormat:@"AliQRImage%@", self.payOrder.bookingOrderId];
    NSData *QRImageData = [[NSUserDefaults standardUserDefaults] objectForKey:QRImageId];
    
    if (!aliPayBtn.selected) {
        NSLog(@"alliPayBtn");
        aliPayBtn.layer.borderColor = [UIColor colorWithHexString:@"#05B1C2" alpha:1.0].CGColor;
        self.weChatPayBtn.layer.borderColor = [UIColor colorWithHexString:@"#CBCBCB" alpha:1.0].CGColor;
        self.QRCodeImageView.image = [UIImage imageWithData:QRImageData];
        aliPayBtn.selected = YES;
        
        //
        NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:@"请让患者打开支付宝扫码支付"];
        [AttributedStr addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(6, 3)];
        self.payStyleLabel.attributedText = AttributedStr;
        
        //
//        // 创建一个定时器
//        self.timer = [NSTimer timerWithTimeInterval:3.0 target:self selector:@selector(getAliPayOrderState) userInfo:nil repeats:YES];
//        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
    
    self.weChatPayBtn.selected = NO;
    
//    [self.timer invalidate];
//    //    self.timer = nil;
//    self.timeCount = 0;
//    // 创建一个定时器
//    self.timer = [NSTimer timerWithTimeInterval:3.0 target:self selector:@selector(getAliPayOrderState) userInfo:nil repeats:YES];
//    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    
}
#pragma mark -- getPayOrderState

- (void)getPayOrderState {
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_group_t group = dispatch_group_create();
////
//    dispatch_group_async(group, queue, ^{
//        // 获取微信二维码图片
        [self getWeChatPayOrderState];
//    });
//    dispatch_group_async(group, queue, ^{
//        // 获取支付宝二维码图片
        [self getAliPayOrderState];
//    });
////
////    //等group里的task都执行完后执行notify方法里的内容
//    dispatch_group_notify(group, queue, ^{
        self.timeCount += 2;
//    });

}

#pragma mark -- getOrderState
- (void)getWeChatPayOrderState {
    
    
    
    if (self.timeCount > 2 * 60) { // 两分钟
        
        NSLog(@"支付超时!");
        [self.timer invalidate];
        //        self.timer = nil;
        self.timeCount = 0;
        
        //
        //2.0
        
        self.hidden = YES;
        
        //
        // 3.0, 设置老的时间为0， 这样退出的时候就会再次重新获取新的二维码。
        
        
        
        // 4.0
        [self setAlertWithAlertStr:@"支付超时,请重新打开支付页面"];
        return;
    }
    
    
    [HZOrderHttpsTool getOrderPayStateWithPayWay:@"WEIXIN" OutTradeNo:self.payOrder.bookingOrderId success:^(id responseObject) {
//        NSLog(@"WEIXIN-----------%@", [(NSDictionary *)responseObject getDictJsonStr]);
        
        NSLog(@"---------WEIXIN--------%ld", self.timeCount);
        if ([responseObject[@"state"] isEqual:@200]) {
            //            [self.timer invalidate];
            ////            self.timer = nil;
            //            self.timeCount = 0;
            
            
            //
            NSDictionary *resultsDict =  responseObject[@"results"];
            NSDictionary *orderDict =  resultsDict[@"order"];
            NSString *payOrderStatus = orderDict[@"tradeStatus"];
            
            //
            [self setUpPayOrderStatusWithStatus:payOrderStatus];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"WEIXIN-----------%@", error);

    }];
}

- (void)getAliPayOrderState {
    
    
    
    if (self.timeCount > 2 * 60) { // 两分钟
        
        NSLog(@"支付超时!");
        
        // 1.0
        [self.timer invalidate];
        //        self.timer = nil;
        self.timeCount = 0;
        
        //2.0
        
        self.hidden = YES;
        // 3.0
        
        [self setAlertWithAlertStr:@"支付超时,请重新打开支付页面"];
        return;
    }
    
    
    [HZOrderHttpsTool getOrderPayStateWithPayWay:@"ALIPAY" OutTradeNo:self.payOrder.bookingOrderId success:^(id responseObject) {
//        NSLog(@"ALIPAY-----------%@", [(NSDictionary *)responseObject getDictJsonStr]);
        NSLog(@"---------ALIPAY--------%ld", self.timeCount);
        if ([responseObject[@"state"] isEqual:@200]) {
            //            [self.timer invalidate];
            ////            self.timer = nil;
            //            self.timeCount = 0;
            
            //
            NSDictionary *resultsDict =  responseObject[@"results"];
            NSDictionary *orderDict =  resultsDict[@"order"];
            NSString *payOrderStatus = orderDict[@"tradeStatus"];
            
            //
            [self setUpPayOrderStatusWithStatus:payOrderStatus];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ALIPAY-----------%@", error);
    }];
    
    
}


#pragma mark -- setUpPayOrderStatusWithStatus

- (void)setUpPayOrderStatusWithStatus:(NSString *)status {
    
    //    if ([status isEqualToString:@"SUCCESS"]) {// 支付成功
    //
    //    } else if ([status isEqualToString:@"FAILED"]) { // 交易失败
    //
    //    //
    //    } else if ([status isEqualToString:@"CREATED"]) { // 订单已创建
    //
    //    } else if ([status isEqualToString:@"PAYING"]) { // 正在支付
    //
    //    } else if ([status isEqualToString:@"CANCELED"]) { // 订单已取消
    //
    //    } else if ([status isEqualToString:@"REFUNDED"]) { // 退款成功
    //
    //    }
    
    
    NSLog(@"----------支付--------:%@", status);
    if ([status isEqualToString:@"SUCCESS"]) {// 支付成功
        //1.0
        [self.timer invalidate];
        //            self.timer = nil;
        self.timeCount = 0;
        
        // 2.0
        self.hiddenBtn.hidden = YES;
        self.payCountLabel.hidden = YES;
        self.weChatPayBtn.hidden = YES;
        self.aliPayBtn.hidden = YES;
        self.payStyleLabel.hidden = YES;
        self.QRCodeImageView.hidden = YES;
        self.timeAlertLabel.hidden = YES;
        
        //
        self.paySuccessImageView.hidden = NO;
        self.paySuccessLabel.hidden = NO;
        self.payAlertLabel.hidden = NO;
        self.paySuccessBtn.hidden = NO;
        
        //3.0
       
        
    } else if ([status isEqualToString:@"FAILED"]) { // 交易失败
        
        //1.0
        [self.timer invalidate];
        //            self.timer = nil;
        self.timeCount = 0;
        
        //2.0
        //
        self.hidden = YES;
        
        //
        //
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"WeChatQRImageOldTime%@", self.payOrder.bookingOrderId]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"AliQRImageOldTime%@", self.payOrder.bookingOrderId]];
        
        //
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"WeChatQRImage%@", self.payOrder.bookingOrderId]];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"AliQRImage%@", self.payOrder.bookingOrderId]];
        //
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //
        [self setAlertWithAlertStr:@"交易失败"];
    } else if ([status isEqualToString:@"CREATED"]) {
        
        //        // 2.0
        //        self.hiddenBtn.hidden = YES;
        //        self.payCountLabel.hidden = YES;
        //        self.weChatPayBtn.hidden = YES;
        //        self.aliPayBtn.hidden = YES;
        //        self.payStyleLabel.hidden = YES;
        //        self.QRCodeImageView.hidden = YES;
        //        self.timeAlertLabel.hidden = YES;
        //
        //        //
        //        self.paySuccessImageView.hidden = NO;
        //        self.paySuccessLabel.hidden = NO;
        //        self.payAlertLabel.hidden = NO;
        //        self.paySuccessBtn.hidden = NO;
        
        //3.0
        //
        //        self.hidden = YES;
        
        //
        //        NSString *alertStr = [status isEqualToString:@"FAILED"] ? @"交易失败" : @"订单已取消";
        //        [self setAlertWithAlertStr:alertStr];
        
    }
}

#pragma mark -- setAlertWithAlertStr
- (void)setAlertWithAlertStr:(NSString *)alertStr {
    UIAlertController *alertCtr = [UIAlertController alertControllerWithTitle:nil message:alertStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ensureAlertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        

    }];
    if (kCurrentSystemVersion >= 9.0) {
        [ensureAlertAction setValue:kGreenColor forKey:@"titleTextColor"];
    }
    [alertCtr addAction:ensureAlertAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCtr animated:YES completion:nil];
}



#pragma mark -- paySuccessBtnClick

- (void)paySuccessBtnClick:(UIButton *)paySuccessBtn {
    
    self.hidden = YES;
    
    
    if (self.isFromAppointSuccess) {
        // 回去刷新订单状态
        if (self.delegate && [self.delegate respondsToSelector:@selector(returnBackHome)]) {
            [self.delegate returnBackHome];
        }
    } else {
        // 回去刷新订单状态
        if (self.delegate && [self.delegate respondsToSelector:@selector(refreshOrderStatus)]) {
            [self.delegate refreshOrderStatus];
        }
    }
    
    
}

- (void)setMyTimerStart {

    //
    [self.timer invalidate];
    //    self.timer = nil;
    self.timeCount = 0;
    // 创建一个定时器
    self.timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(getWeChatPayOrderState) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];

}

//
//- (void)setMyTimerStop {
//    //
//    [self.timer invalidate];
//    self.timer = nil;
//    self.timeCount = 0;
//}
@end
