//
//  HZWebViewController.m
//  HZAppBackJSDemo
//
//  Created by 季怀斌 on 2017/11/10.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZWebViewController.h"
#import <WebKit/WebKit.h>
//
#import "HZUser.h"
@interface HZWebViewController () <WKUIDelegate, WKNavigationDelegate, UIScrollViewDelegate>
@property (nonatomic, weak) WKWebView *webView;
@property (nonatomic, weak) UIProgressView *progressView;
@property (nonatomic, copy) NSString *webTitleStr;
@property (nonatomic, assign) BOOL shouldStatusBarStyleLightContent;
@end

@implementation HZWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    

    //
    self.shouldStatusBarStyleLightContent = [self.loadUrlStr containsString:@"viewer-wpacs"];
    //
    if (self.shouldStatusBarStyleLightContent) {// pacs
        self.view.backgroundColor = [UIColor blackColor];
        [self preferredStatusBarStyle];
    } else {
        self.view.backgroundColor = [UIColor whiteColor];
    }

    
    self.title = @"加载中...";
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, HZScreenW, HZScreenH - SafeAreaTopHeight - SafeAreaBottomHeight)];
    NSLog(@"--------loadUrlStr======:%@", _loadUrlStr);
    //
    NSURL *url = [NSURL URLWithString:_loadUrlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //
    if (self.shouldStatusBarStyleLightContent) {
        NSString *accessTokenValue = [NSString stringWithFormat:@"Bearer %@",kAccess_token];
        [request setValue:accessTokenValue forHTTPHeaderField:@"Authorization"];
        webView.allowsBackForwardNavigationGestures = NO;
        webView.scrollView.bounces = YES;
    } else {
        webView.allowsBackForwardNavigationGestures = YES;
        webView.scrollView.bounces = YES;
    }
   
    webView.UIDelegate = self;
    webView.navigationDelegate = self;
//            webView.scrollView.scrollEnabled = NO;// 屏蔽滑动
    
    webView.scrollView.showsVerticalScrollIndicator = NO;
    webView.scrollView.delegate = self;
//    webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    //        webView.scrollView.contentSize =  CGSizeMake(0, 200);
//    webView.allowsBackForwardNavigationGestures = YES;
    //
    //        [webView loadHTMLString:htmlCont baseURL:baseURL];
    
   NSLog(@"----ffffffffff-----%@", self.webView.scrollView.subviews);
    [webView loadRequest:request];
    [self.view addSubview:webView];
    self.webView = webView;
    
    //
    //进度条初始化
    UIProgressView *progressView;
    if (self.shouldStatusBarStyleLightContent) {
       progressView  = [[UIProgressView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, HZScreenW, 0.5)];
    } else {
       progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight + 1, HZScreenW, 0.5)];
    }

    //    progressView.backgroundColor = [UIColor blueColor];
    progressView.trackTintColor = [UIColor clearColor];
    //    progressView.tintColor = [UIColor greenColor];
    progressView.progressTintColor = kGreenColor;
    progressView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    [self.view addSubview:progressView];
    self.progressView = progressView;
    
    //
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    
    //
    [self setNaviBackItemBlock];
    
    [self testWebViewGes];
}


#pragma mark -- loadUrlStr
- (void)setLoadUrlStr:(NSString *)loadUrlStr {
    _loadUrlStr = loadUrlStr;
}

-(void)testWebViewGes {
    if (self.shouldStatusBarStyleLightContent) return;
    id gesDelegate = self.webView.gestureRecognizers.firstObject.delegate;
    UIPanGestureRecognizer *webPan = [[UIPanGestureRecognizer alloc]initWithTarget:gesDelegate action:@selector(handleNavigationTransition:)];
    webPan.delegate = self;
    [self.view addGestureRecognizer:webPan];
}


#pragma mark -- setNaviBackItemBlock
- (void)setNaviBackItemBlock {
    
    if (self.shouldStatusBarStyleLightContent) return;
    
    WeakSelf(weakSelf);
    HZNavigationController *navCtr = (HZNavigationController *)self.navigationController;
    [navCtr returnViewController:^{
        if ([weakSelf.webView canGoBack]) {
            [weakSelf.webView goBack];
            return;
        }
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}


#pragma mark ----------------- 注释掉 ------------------
//#pragma mark -- 让webview的内容一直居中显示
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    scrollView.contentOffset = CGPointMake((scrollView.contentSize.width - HZScreenW) * 0.5, scrollView.contentOffset.y);
//}



#pragma mark - WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"-----------开始加载------------:%@", webView.title);
    self.navigationItem.title = [NSString isBlankString:webView.title] ? @"加载中..." : webView.title;
    //
    //开始加载网页时展示出progressView
    self.progressView.hidden = NO;
    //开始加载网页的时候将progressView的Height恢复为1.5倍
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    //防止progressView被网页挡住
    [self.view bringSubviewToFront:self.progressView];
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    
    NSString *titleStr = [NSString isBlankString:webView.title] ? ([NSString isBlankString:self.webTitleStr] ? @"加载中..." : self.webTitleStr) : webView.title;
   
    self.navigationItem.title = titleStr;
    
    NSLog(@"------------开始返回-----------:%@", webView.title);
}
// 页面加载完成
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    self.navigationItem.title = [NSString isBlankString:webView.title] ? @"加载中..." : webView.title;
    NSLog(@"------------加载完成-----------:%@", webView.title);
    
    
    self.webTitleStr = webView.title;
        NSLog(@"--33:%@",navigation);

    
    if ([NSString isBlankString:webView.title]) {
        
        
       self.navigationItem.title = @"加载失败";
        
    }
    
    //加载完成后隐藏progressView
    //self.progressView.hidden = YES;
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    //加载失败同样需要隐藏progressView
    //self.progressView.hidden = YES;
    
    if ([NSString isBlankString:webView.title]) {
       
    self.navigationItem.title = @"加载中失败";
        
        
    }
}
// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    //    self.navigationItem.title = [NSString isBlankString:webView.title] ? @"加载中..." : webView.title;
}



// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    
    
    //    NSLog(@"--33:%@",navigationResponse.response.URL.absoluteString);
    NSLog(@"------------收到响应后----------:%@", webView.title);
    
    NSString *titleStr = [NSString isBlankString:webView.title] ? @"加载中..." : webView.title;
   
    self.navigationItem.title = titleStr;
    
    //
    if (((NSHTTPURLResponse *)navigationResponse.response).statusCode == 200) {
        
        // 就第一个地址加载完成回调,其他不回调
        if (webView.backForwardList.backList.count == 0 && self.sucessLoadBlock) {
            self.sucessLoadBlock();
            self.sucessLoadBlock = nil;
        }
        
        decisionHandler (WKNavigationResponsePolicyAllow);
    } else {
        
        decisionHandler(WKNavigationResponsePolicyCancel);
    }
    
    
    //    //允许跳转
    //    decisionHandler(WKNavigationResponsePolicyAllow);
    //    //不允许跳转
    //    //decisionHandler(WKNavigationResponsePolicyCancel);
    
    
}




// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    NSLog(@"------------发送请求之前----------:%@", webView.title);
    
    //    NSLog(@"--22:%@",navigationAction.request.URL.absoluteString);
    NSString *titleStr = [NSString isBlankString:webView.title] ? @"加载中..." : webView.title;
   
    self.navigationItem.title = titleStr;

    
    self.webTitleStr = webView.title;
    
    NSURL *URL = navigationAction.request.URL;
    NSString *scheme = [URL scheme];
    if ([scheme isEqualToString:@"tel"]) {
        NSString *resourceSpecifier = [URL resourceSpecifier];
        NSString *callPhone = [NSString stringWithFormat:@"telprompt://%@", resourceSpecifier];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone]];
    }
    
    
    // 控制手势 返回
    self.view.gestureRecognizers.firstObject.delegate = webView.backForwardList.backList.count < 1 ? nil:self;
    
    self.isShouldPanToPopBack = webView.backForwardList.backList.count < 1;
    
    //允许跳转
    decisionHandler(WKNavigationActionPolicyAllow);
}

#pragma mark -- observeValueForKeyPath
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        self.progressView.progress = self.webView.estimatedProgress;
        NSLog(@"----------sss--------:%lf", self.progressView.progress);
        if (self.progressView.progress == 1) {
            WeakSelf(weakSelf)
            [UIView animateWithDuration:0.25f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                weakSelf.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
            } completion:^(BOOL finished) {
                weakSelf.progressView.hidden = YES;
                
            }];
        }
    }else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


- (void)dealloc {
    
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
}

-(void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{
    
    NSLog(@"GGGGGGGGG");
}


//#pragma mark - WKUIDelegate
//// 创建一个新的WebView
//- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
//
//
//    return [[WKWebView alloc]init];
//}
//// 输入框
//- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * __nullable result))completionHandler{
//
//
//    completionHandler(@"http");
//}
//// 确认框
//- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler{
//    completionHandler(YES);
//
//
//}
//// 警告框
//- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
//    NSLog(@"%@",message);
//
//    completionHandler();
//}


- (void)viewWillDisappear:(BOOL)animated {
    HZNavigationController *navCtr = (HZNavigationController *)self.navigationController;
    navCtr.backItemBlock = nil;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    if (self.webView.backForwardList.backList.count < 1) {
        
        return NO;
    }
    
    return YES;
}



@end
