//
//  HZWebViewController.h
//  HZAppBackJSDemo
//
//  Created by 季怀斌 on 2017/11/10.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HZWebViewController : HZViewController
@property (nonatomic, copy) NSString *loadUrlStr;
@property (nonatomic,copy) void(^sucessLoadBlock)();
@property (nonatomic,copy) void(^setStatusBarStyleNormalBlock)();
@end
