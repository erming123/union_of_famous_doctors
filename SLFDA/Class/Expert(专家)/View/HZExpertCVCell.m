//
//  HZExpertCVCell.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/5.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertCVCell.h"
#import "HZUser.h"
#import "HZUserTool.h"

@interface HZExpertCVCell ()
@property (nonatomic, weak) UIImageView *expertAvatarImageView;
@property (nonatomic, weak) UIImageView *expertGradeImageView;
@property (nonatomic, weak) UILabel *expertNameLabel;
@property (nonatomic, weak) UILabel *expertSubjectLabel;
@property (nonatomic, weak) UILabel *expertJobLabel;
@property (nonatomic, weak) UILabel *expertSkillLabel;
@end

@implementation HZExpertCVCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
        self.backgroundColor = [UIColor colorWithHexString:@"#FAFBFC" alpha:1.0];
        
        self.layer.cornerRadius = kP(16);
        self.layer.shadowColor = [UIColor colorWithRed:12/255.0 green:46/255.0 blue:53/255.0 alpha:1].CGColor;
        self.layer.contentsScale = [UIScreen mainScreen].scale;
        self.layer.shadowOpacity = 0.2f;
        self.layer.shadowRadius = 4.0f;
        self.layer.shadowOffset = CGSizeMake(0,5);
        self.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, frame.size.width, frame.size.height)].CGPath;
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    }
    return self;
}



- (void)setUpSubViews {
    
    
    // 头像
    UIImageView *expertAvatarImageView = [UIImageView new];
    expertAvatarImageView.backgroundColor = [UIColor whiteColor];
    [self addSubview:expertAvatarImageView];
    expertAvatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.expertAvatarImageView = expertAvatarImageView;
    
    
    // 级别
    UIImageView *expertGradeImageView = [UIImageView new];
    [self addSubview:expertGradeImageView];
    self.expertGradeImageView = expertGradeImageView;
    
    
    // 姓名
    UILabel *expertNameLabel = [UILabel new];
    expertNameLabel.textColor = [UIColor colorWithHexString:@"#5E6066" alpha:1.0];
    [self addSubview:expertNameLabel];
    self.expertNameLabel = expertNameLabel;
    
    // 科室
    UILabel *expertSubjectLabel = [UILabel new];
    expertSubjectLabel.textAlignment = NSTextAlignmentCenter;
    expertSubjectLabel.font = [UIFont systemFontOfSize:kP(24)];
    expertSubjectLabel.textColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    expertSubjectLabel.backgroundColor = [UIColor colorWithHexString:@"#F5A623" alpha:1.0];
    [self addSubview:expertSubjectLabel];
    self.expertSubjectLabel = expertSubjectLabel;
    
    // 职称
    UILabel *expertJobLabel = [UILabel new];
    expertJobLabel.textAlignment = NSTextAlignmentCenter;
    expertJobLabel.font = [UIFont systemFontOfSize:kP(24)];
    expertJobLabel.textColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:1.0];
    expertJobLabel.backgroundColor = [UIColor colorWithHexString:@"#239BF5" alpha:1.0];
    [self addSubview:expertJobLabel];
    self.expertJobLabel = expertJobLabel;
    
    // 擅长
    UILabel *expertSkillLabel = [UILabel new];
    expertSkillLabel.numberOfLines = 0;
    expertSkillLabel.font = [UIFont systemFontOfSize:kP(24)];
    expertSkillLabel.textColor = [UIColor colorWithHexString:@"#999999" alpha:1.0];
    [self addSubview:expertSkillLabel];
    self.expertSkillLabel = expertSkillLabel;
    
    //
    //
    CGFloat expertAvatarImageViewY = kP(32);
    CGFloat expertAvatarImageViewWH = kP(130);
    CGFloat expertAvatarImageViewX = self.width * 0.5 - expertAvatarImageViewWH * 0.5;
    self.expertAvatarImageView.frame = CGRectMake(expertAvatarImageViewX, expertAvatarImageViewY, expertAvatarImageViewWH, expertAvatarImageViewWH);
    self.expertAvatarImageView.layer.cornerRadius = expertAvatarImageViewWH * 0.5;
    self.expertAvatarImageView.layer.borderColor = [UIColor colorWithHexString:@"#D5D5D5" alpha:1.0].CGColor;
    self.expertAvatarImageView.layer.borderWidth = kP(1);
    self.expertAvatarImageView.clipsToBounds = YES;
    
    
    //
    self.expertNameLabel.font = [UIFont systemFontOfSize:kP(32)];
    self.expertNameLabel.textColor = [UIColor colorWithHexString:@"#5E6066" alpha:1.0];
    
    //
    self.expertGradeImageView.y = self.expertAvatarImageView.bottom + kP(20);
    self.expertGradeImageView.width = kP(26);
    self.expertGradeImageView.height = kP(35);
    

    //
    self.expertSubjectLabel.y = self.expertGradeImageView.bottom + kP(20);

    //
    self.expertJobLabel.y = self.expertGradeImageView.bottom + kP(20);
//    self.expertJobLabel.layer.cornerRadius = expertJobLabelH * 0.5;
//    self.expertJobLabel.clipsToBounds = YES;
    
    //
    self.expertSkillLabel.x = kP(20);
    self.expertSkillLabel.width = self.width - 2 * self.expertSkillLabel.x;
    self.expertSkillLabel.height = kP(60);
}

- (void)setExpert:(HZUser *)expert {
    
   _expert = expert;

    [self.expertAvatarImageView getPictureWithFaceUrl:expert.facePhotoUrl AndPlacehoderImage:@"expertPlaceImg.png"];
    self.expertGradeImageView.image = [UIImage imageNamed:@"expertGrade"];
    
    //
    self.expertNameLabel.text = expert.userName;
    [self.expertNameLabel sizeToFit];
    
    self.expertGradeImageView.x = self.width * 0.5 - (self.expertGradeImageView.width + self.expertNameLabel.width + kP(11)) * 0.5;
    self.expertNameLabel.x = self.expertGradeImageView.right + kP(11);
    self.expertNameLabel.y = self.expertGradeImageView.centerY - self.expertNameLabel.height * 0.5;
    
    //
    self.expertSubjectLabel.text = [NSString stringWithFormat:@"%@",expert.departmentGeneralName];
    [self.expertSubjectLabel sizeToFit];
    if (expert.departmentGeneralName.length != 0) {
    self.expertSubjectLabel.width += kP(20);
    self.expertSubjectLabel.width = self.expertSubjectLabel.width > kP(120) ? kP(120) : kP(120);
    self.expertSubjectLabel.height += kP(10);
    }

    self.expertSubjectLabel.layer.cornerRadius = self.expertSubjectLabel.height * 0.5;
    self.expertSubjectLabel.clipsToBounds = YES;
    
    
    
    //
    self.expertJobLabel.text = [NSString stringWithFormat:@"%@",expert.titleName];
    [self.expertJobLabel sizeToFit];
    if (expert.titleName.length != 0) {
    self.expertJobLabel.width +=kP(20);
    self.expertJobLabel.width = self.expertJobLabel.width > kP(120)?kP(140):kP(120);
    self.expertJobLabel.height += kP(10);
    }
    self.expertJobLabel.layer.cornerRadius = self.expertJobLabel.height * 0.5;
    self.expertJobLabel.clipsToBounds = YES;
    self.expertSubjectLabel.x = kP(20);
    self.expertJobLabel.x = self.width - self.expertJobLabel.width - kP(20);
   
    
    
    //
    self.expertSkillLabel.y = self.expertJobLabel.bottom + kP(20);
    if ([NSString isBlankString:expert.speciality]) {
        self.expertSkillLabel.textAlignment = NSTextAlignmentCenter;
        self.expertSkillLabel.text = @"暂无介绍信息";
    } else {
        self.expertSkillLabel.textAlignment = NSTextAlignmentLeft;
        self.expertSkillLabel.text = [NSString stringWithFormat:@"擅长:%@", expert.speciality];
    }
    
    
}

@end
