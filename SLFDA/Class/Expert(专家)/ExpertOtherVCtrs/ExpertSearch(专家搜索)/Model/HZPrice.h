//
//  HZPrice.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/3.
//  Copyright © 2017年 huazhuo. All rights reserved.
//


#import "HZJsonModel.h"
@interface HZPrice : HZJsonModel
//
@property (nonatomic, copy) NSString *levelTypeCode;// 
@property (nonatomic, copy) NSString *levelTypeValue;// job,比如院士
//
@property (nonatomic, copy) NSString *totalMoney;// 2000
@property (nonatomic, copy) NSString *hisBookingTypeCode;
//
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *updateTime;
//
@property (nonatomic, copy) NSString *proportionId;
@property (nonatomic, copy) NSString *state;
//
@property (nonatomic, copy) NSString *incId;
@end
