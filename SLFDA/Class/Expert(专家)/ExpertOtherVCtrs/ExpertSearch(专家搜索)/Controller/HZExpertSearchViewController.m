//
//  HZExpertSearchViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/3/1.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertSearchViewController.h"
#import "HZSearchBar.h"
#import "HZOrderSubmitViewController.h"
#import "HZExpertTeamCell.h"
#import "HZExpertSearchTool.h"
#import "HZUser.h"
#import "HZUserTool.h"
//
#import "HZExpertHomeViewController.h"

//
#import "MJRefresh.h"
//
#import "IQKeyboardManager.h"
@interface HZExpertSearchViewController ()<HZSearchBarDelegate, UITableViewDataSource, UITableViewDelegate, NSCacheDelegate>
//** <#注释#>*/
@property (nonatomic, weak) HZSearchBar *searchBar;
//** <#注释#>*/
@property (nonatomic, weak) HZTableView *searchTableView;
//** <#注释#>*/
@property (nonatomic, weak) HZExpertTeamCell *expertTeamCell;
//** <#注释#>*/
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
//** <#注释#>*/
@property (nonatomic, strong) NSArray *expertArr;
@property (nonatomic, assign) BOOL isSearched;
@property (nonatomic, assign) BOOL isChangeTextNil;
//** <#注释#>*/
@property (nonatomic, weak) UIImageView *noDoctImageView;
@end

@implementation HZExpertSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    self.view.backgroundColor = [UIColor whiteColor];
    
    CGFloat searBarX = kP(20);
    CGFloat searBarY = kP(14);
    CGFloat searBarW =HZScreenW - 2*searBarX;
    CGFloat searBarH = kP(56);
    HZSearchBar *searchBar = [[HZSearchBar alloc] initWithFrame:CGRectMake(searBarX, searBarY, searBarW, searBarH)];
    searchBar.delegate = self;
    searchBar.placeholder = @"请输入专家姓名或学科";
    self.navigationItem.titleView = searchBar;
    self.searchBar = searchBar;
    //
    HZTableView *searchTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    if (kCurrentSystemVersion >= 11.0) {
        searchTableView.y = SafeAreaTopHeight;
        searchTableView.height -= SafeAreaTopHeight;
    }
    searchTableView.showsVerticalScrollIndicator = NO;
    searchTableView.dataSource = self;
    searchTableView.delegate = self;
    searchTableView.tableFooterView = [UIView new];
    [self.view addSubview:searchTableView];
    self.searchTableView = searchTableView;
    
    //
    CGFloat noDoctImageViewW = kP(340);
    CGFloat noDoctImageViewH = kP(376);
    CGFloat noDoctImageViewX = self.view.width * 0.5 - noDoctImageViewW * 0.5;
    CGFloat noDoctImageViewY = kP(600);
    UIImageView *noDoctImageView = [[UIImageView alloc] initWithFrame:CGRectMake(noDoctImageViewX, noDoctImageViewY, noDoctImageViewW, noDoctImageViewH)];
    noDoctImageView.image = [UIImage imageNamed:@"noDoct.png"];
    noDoctImageView.hidden = YES;
    [self.view addSubview:noDoctImageView];
    self.noDoctImageView = noDoctImageView;
    noDoctImageView.center = self.view.center;
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    //    [self getPriceList];
    //5.
    [self pushToRefresh];
}


- (NSArray *)expertArr {
    if (!_expertArr) {
        _expertArr = [NSArray array];
    }
    
    return _expertArr;
}


- (void)setPriceArr:(NSArray *)priceArr {
    _priceArr = priceArr;
}

#pragma mark -- set

- (void)setUser:(HZUser *)user {
    _user = user;
}



#pragma mark -- pushToRefresh
- (void)pushToRefresh {
    
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        

//        [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
//
//        [[SDImageCache sharedImageCache] clearMemory];//可不写
        [self getSearchResult:self.searchBar];
        
    }];
    self.searchTableView.mj_header = refreshHeader;
    
//    //下拉刷新
//    self.searchTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//
//        //
//        [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
//
//        [[SDImageCache sharedImageCache] clearMemory];//可不写
//        [self getSearchResult:self.searchBar];
//        // 结束刷新
//        [self.searchTableView.mj_header endRefreshing];
//    }];
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.searchTableView.mj_header.automaticallyChangeAlpha = YES;
}
#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.expertArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HZExpertTeamCell *expertTeamCell = [HZExpertTeamCell cellWithTableView:tableView];
    
    HZUser *expert = self.expertArr[indexPath.row];
    expertTeamCell.priceList = self.priceArr;
    
    //
    expertTeamCell.expert = expert;
    return expertTeamCell;
}




#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kP(230);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //
    HZExpertHomeViewController *expertHomeVCtr = [HZExpertHomeViewController new];
    HZUser *expert = self.expertArr[indexPath.row];
    expertHomeVCtr.expert = expert;
    expertHomeVCtr.user = self.user;
    
    
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:expert.openid];
    expertHomeVCtr.expertAvatar = image;
    expertHomeVCtr.priceArr = self.priceArr;
    [self.navigationController pushViewController:expertHomeVCtr animated:YES];
}

#pragma mark -- 去掉返回按钮
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
//    UIBarButtonItem *backItem =  self.navigationItem.leftBarButtonItems.lastObject;
//    backItem.customView.hidden = YES;
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.backBarButtonItem.title = @"";
    if (self.expertArr.count == 0) {
        self.searchBar.isBecomeFirstResponder = YES;
    }

    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    self.searchBar.isBecomeFirstResponder = NO;
//    UIBarButtonItem *backItem =  self.navigationItem.leftBarButtonItems.lastObject;
//    backItem.customView.hidden = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    
   
}

#pragma mark -- HZSearchBarDelegate
- (void)cancelBtnClick:(HZSearchBar *)searBar {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)getSearchResult:(HZSearchBar *)searBar {
    
//    //
//    [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
//
//    [[SDImageCache sharedImageCache] clearMemory];//可不写
    
    if ([NSString isBlankString:searBar.textFieldText] && self.expertArr.count != 0) {
        searBar.isBecomeFirstResponder = NO;
        // 结束刷新
        [self.searchTableView.mj_header endRefreshing];
        //        return;
    } else if (![NSString isBlankString:searBar.textFieldText]){
        searBar.isBecomeFirstResponder = NO;
        [self.activityIndicator startAnimating];
        
        NSLog(@"---------%@", searBar.textFieldText);
        [HZExpertSearchTool getExpertArrWithDoctorName:searBar.textFieldText dictDepartmentId:nil isHotExpertSearch:NO page:1 countPerPage:1000 success:^(id responseObject) {
            [self.activityIndicator stopAnimating];
            
            NSLog(@"-------dsf-----:%@", responseObject);
            
            if ([responseObject[@"msg"] isEqualToString:@"success"]) {
                NSDictionary *resultDict = responseObject[@"results"];
                self.expertArr = [HZUser mj_objectArrayWithKeyValuesArray:resultDict[@"doctors"]];
                
                //            HZUser *user = self.expertArr;
                //                            NSLog(@"---------搜索成功----%@", self.expertArr);
                //            NSLog(@"---------getDictJsonStr搜索成功----%@", self.expertArr[0]);
                
                self.noDoctImageView.hidden = self.expertArr.count != 0;
//                [self.searchTableView reloadData];
//                // 结束刷新
//                [self.searchTableView.mj_header endRefreshing];
            } else {
                self.noDoctImageView.hidden = NO;
                self.expertArr = [NSArray array];
                [self.view makeToast:responseObject[@"msg"] duration:1.5 position:CSToastPositionBottom];
                NSLog(@"---------搜索不成功----%@", responseObject[@"msg"]);
            }
            
            [self.searchTableView reloadData];
            // 结束刷新
            [self.searchTableView.mj_header endRefreshing];
            [self.activityIndicator stopAnimating];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"-----搜索失败专家搜索----%@", error);
            // 结束刷新
            [self.searchTableView.mj_header endRefreshing];
            [self.activityIndicator stopAnimating];
            
        }];
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
