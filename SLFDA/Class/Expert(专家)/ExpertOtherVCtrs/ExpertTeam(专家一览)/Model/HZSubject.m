//
//  HZSubject.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/21.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZSubject.h"

@implementation HZSubject
- (NSString *)description {
    return [NSString stringWithFormat:@"code:%@-----dictDepartmentId:%@, englishName:%@-----incId:%@-----, index:%@-----name:%@-----parentId:%@-----pinyinCode:%@, property:%@-----state:%@-----, wubiCode:%@", self.code, self.dictDepartmentId, self.englishName, self.incId, self.index, self.name, self.parentId, self.pinyinCode, self.property, self.state, self.wubiCode];
}
@end
