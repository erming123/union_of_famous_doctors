//
//  HZSubject.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/21.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZJsonModel.h"
@interface HZSubject : HZJsonModel
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *dictDepartmentId;
@property (nonatomic, copy) NSString *englishName;
@property (nonatomic, copy) NSString *incId;
@property (nonatomic, copy) NSString *index;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *parentId;
@property (nonatomic, copy) NSString *pinyinCode;
@property (nonatomic, copy) NSString *property;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *wubiCode;

@property (nonatomic, assign) BOOL isSelect;
@end
