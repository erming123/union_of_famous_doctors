//
//  HZExpertTeamViewController.h
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HZUser;
@interface HZExpertTeamViewController : HZViewController
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
@end
