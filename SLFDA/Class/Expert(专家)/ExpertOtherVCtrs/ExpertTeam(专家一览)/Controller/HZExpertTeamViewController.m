//
//  HZExpertTeamViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/4/14.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertTeamViewController.h"
#import "HZSubjectCell.h"
#import "HZExpertTeamCell.h"
#import "HZExpertHomeViewController.h"
#import "HZNewsListViewController.h"

//
#import "HZSubject.h"
#import "HZUser.h"
#import "HZInforCompleteTool.h"
#import "HZExpertSearchTool.h"
#import "HZUserTool.h"

//
#import "MJRefresh.h"
//
#import "HZExpertSearchViewController.h"
@interface HZExpertTeamViewController () <UITableViewDataSource, UITableViewDelegate, NSCacheDelegate>
//** <#注释#>*/
@property (nonatomic, weak) UIImageView *arrowImageView;
//** <#注释#>*/
@property (nonatomic, weak) UIView *titleBgView;
//** <#注释#>*/
@property (nonatomic, weak) UILabel *subjectLabel;
@property (nonatomic, assign) BOOL isTapped;
//** <#注释#>*/
@property (nonatomic, weak) UIView *bgView;
//** <#注释#>*/
@property (nonatomic, weak) HZTableView *subjectTableView;
//** <#注释#>*/
@property (nonatomic, weak) UITableView *expertTeamTableView;
@property (nonatomic, assign) NSInteger indexPath;
//
@property (nonatomic, strong) NSArray *subjectArr;
@property (nonatomic, strong) NSArray *expertTeamArr;
//** <#注释#>*/
@property (nonatomic, weak) HZSubjectCell *subjectCell;
//** <#注释#>*/
@property (nonatomic, weak) HZExpertTeamCell *expertTeamCell;
//** <#注释#>*/
@property (nonatomic, strong) HZSubject *currentSubject;
//** <#注释#>*/
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;

//** <#注释#>*/
@property (nonatomic, weak) UIImageView *noDoctImageView;

//
@property (nonatomic, strong) NSArray *priceArr;
@end

@implementation HZExpertTeamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor redColor];
    
    //
    NSDictionary *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"subjectDict"];
    if (dictionaryData) {
         self.subjectArr = [HZSubject mj_objectArrayWithKeyValuesArray:dictionaryData[@"dictDepartmentList"]];
    }
   
    
    //1.
    [self setUpHeadTopSubViews];
    
    //2.
    [self setUpExpertTeamSubViews];
    
    //3.
    [self setUpDepartMentSubViews];
    
    //
    [self getMyPriceList];
    
        //
    //4.
//    [self getProfessionArr];
    [self getExpertArrWithSubjectDictDepartmentId:self.user.departmentGeneralId];
    
    
    
    //5.
//    [self pushToRefresh];
    
    
}

- (NSArray *)priceArr {
    
    if (!_priceArr) {
        _priceArr = [NSArray array];
    }
    
    return _priceArr;
}

- (NSArray *)subjectArr {
    if (!_subjectArr) {
        _subjectArr = [NSArray array];
    }
    
    return _subjectArr;
}

- (void)setUser:(HZUser *)user {
    _user = user;
}


#pragma mark -- getPriceList
- (void)getMyPriceList {
    [HZExpertSearchTool getPriceListSuccess:^(id responseObject) {
        self.priceArr = responseObject[@"results"][@"PriceList"];
        NSLog(@"-----sssaaaqqqq-qwe---%@", self.priceArr);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---------%@", error);
    }];
}
#pragma mark -- getExpertArrWithProfession
- (void)getExpertArrWithSubjectDictDepartmentId:(NSString *)dictDepartmentId {
    
    
    self.titleBgView.userInteractionEnabled = NO;
    [self.activityIndicator startAnimating];
    
    [HZExpertSearchTool getExpertArrWithDoctorName:nil dictDepartmentId:dictDepartmentId isHotExpertSearch:NO page:1 countPerPage:1000 success:^(id responseObject) {
        
        NSLog(@"---axc---:%@", responseObject);
        
        if ([responseObject[@"msg"] isEqualToString:@"success"]) {
            NSDictionary *resultDict = responseObject[@"results"];
            NSArray *resultArr = [HZUser mj_objectArrayWithKeyValuesArray:resultDict[@"doctors"]];
            
            //            HZUser *user = self.expertArr;
                        NSLog(@"-----resultDictdd----%@", [resultDict getDictJsonStr]);
            
            NSMutableArray *expertArr0 = [NSMutableArray array];
            NSMutableArray *expertArr1 = [NSMutableArray array];
            NSMutableArray *expertArr2 = [NSMutableArray array];
            NSMutableArray *expertArr3 = [NSMutableArray array];
            NSMutableArray *expertArr4 = [NSMutableArray array];
            NSMutableArray *expertArr5 = [NSMutableArray array];
            NSMutableArray *expertArr6 = [NSMutableArray array];
            
            for (HZUser *expertTeam in resultArr) {
                
                
                if ([expertTeam.departmentGeneralId isEqualToString:dictDepartmentId]) {
                    
                    if ([expertTeam.titleName isEqualToString:@"院士"]) {
                        [expertArr0 addObject:expertTeam];
                    } else if ([expertTeam.titleName isEqualToString:@"主任医师"]) {
                        [expertArr1 addObject:expertTeam];
                    } else if ([expertTeam.titleName isEqualToString:@"副主任医师"]) {
                        [expertArr2 addObject:expertTeam];
                    } else if ([expertTeam.titleName isEqualToString:@"主治医师"]) {
                        [expertArr3 addObject:expertTeam];
                    } else if ([expertTeam.titleName isEqualToString:@"住院医生"]) {
                        [expertArr4 addObject:expertTeam];
                    }
                    else if ([NSString isBlankString:expertTeam.titleName]) {
                        [expertArr5 addObject:expertTeam];
                    }
                    else {
                        [expertArr6 addObject:expertTeam];
                    }
                    
                }
            }
//
            
            NSArray *arrArr = @[expertArr0, expertArr1, expertArr2, expertArr3, expertArr4 ,expertArr5, expertArr6];
            NSMutableArray *expertLastArr = [NSMutableArray array];
            
            for (int i = 0; i < arrArr.count; i++) {
                [expertLastArr addObjectsFromArray:arrArr[i]];
            }
            
            self.expertTeamArr = [NSArray arrayWithArray:expertLastArr];
           NSLog(@"---------q搜索成功----%@", self.expertTeamArr);
            
//            self.expertTeamArr = [NSArray arrayWithArray:resultArr];
            
            self.noDoctImageView.hidden = self.expertTeamArr.count != 0;
//            [self.expertTeamTableView reloadData];
//            [self.activityIndicator stopAnimating];
            // 结束刷新
            [self.expertTeamTableView.mj_header endRefreshing];
        } else {
            self.noDoctImageView.hidden = NO;
            self.expertTeamArr = [NSArray array];
            
            [self.view makeToast:responseObject[@"msg"] duration:1.5 position:CSToastPositionBottom];
            NSLog(@"---------搜索不成功----%@", responseObject[@"msg"]);
        }
        
        [self.expertTeamTableView  reloadData];
        [self.activityIndicator stopAnimating];
        self.titleBgView.userInteractionEnabled = YES;
        [self.expertTeamTableView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"-----搜索失败专家团队----%@", error);
        // 结束刷新
        [self.expertTeamTableView.mj_header endRefreshing];
        [self.activityIndicator stopAnimating];
        self.titleBgView.userInteractionEnabled = YES;
    }];

}
#pragma mark -- setUpHeadTopSubViews
- (void)setUpHeadTopSubViews {
    
    //1.
    UILabel *subjectLabel = [[UILabel alloc] init];
    subjectLabel.text = self.user.departmentGeneralName;
    
    NSLog(@"---12vv------%@", self.user.departmentGeneralName);
    
    
    for (HZSubject *subject in self.subjectArr) {
        
//        NSLog(@"---self.professionArr11222223333------%@", self.subjectArr);
        if ([subject.name isEqualToString:self.user.departmentGeneralName]) {
            
//            subject.isSelect = YES;
            self.indexPath = [self.subjectArr indexOfObject:subject];
            self.currentSubject = subject;
            [[NSUserDefaults standardUserDefaults] setInteger:self.indexPath forKey:@"currentProfessionIndexPath"];
        }
        //            else {
        //            ward.isSelect = NO;
        //        }
    }
    subjectLabel.font = [UIFont boldSystemFontOfSize:kP(36)];
    subjectLabel.textColor = [UIColor colorWithHexString:@"#4A4A4A" alpha:1.0];
    [subjectLabel sizeToFit];
    subjectLabel.y = kP(0);
    subjectLabel.x = kP(0);
//    subjectLabel.backgroundColor = [UIColor redColor];
    subjectLabel.textAlignment = NSTextAlignmentCenter;
    subjectLabel.userInteractionEnabled = YES;
    
    
    //2.
    UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"subject_down.png"]];
    arrowImageView.x = subjectLabel.right + kP(16);
    arrowImageView.y = subjectLabel.height * 0.5 - arrowImageView.height * 0.5;
    //    arrowImageView.backgroundColor = [UIColor yellowColor];
    //3.
    CGFloat titleBgViewX = self.view.width * 0.5 - subjectLabel.width * 0.5;
    CGFloat titleBgViewY = 32;
    CGFloat titleBgViewW = subjectLabel.width + kP(50);
    CGFloat titleBgViewH = subjectLabel.height;
    UIView *titleBgView = [[UIView alloc] initWithFrame:CGRectMake(titleBgViewX, titleBgViewY, titleBgViewW, titleBgViewH)];
//    titleBgView.backgroundColor = [UIColor greenColor];
    [titleBgView addSubview:subjectLabel];
    [titleBgView addSubview:arrowImageView];
    self.titleBgView = titleBgView;
    
    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(departMentSearch)];
    [titleBgView addGestureRecognizer:tap];
    //    self.navigationItem.titleView = titleBgView;
    self.navigationItem.titleView = titleBgView;
    
    
    self.subjectLabel = subjectLabel;
    self.arrowImageView = arrowImageView;
    
    //
    UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kP(36), kP(36))];
//    searchBtn.backgroundColor = [UIColor redColor];
    [searchBtn setImage:[UIImage imageNamed:@"doctorSearch"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(turnToSearchVCtr:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
}


- (void)setUpDepartMentSubViews {
    //
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, self.view.width, self.view.height)];
    bgView.backgroundColor = [UIColor lightGrayColor];
    UITapGestureRecognizer *bgViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showBgView)];
    [bgView addGestureRecognizer:bgViewTap];
    bgView.hidden = YES;
    [self.view addSubview:bgView];
    self.bgView = bgView;
    
    //
    HZTableView *subjectTableView = [[HZTableView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, self.view.width, 0) style:UITableViewStylePlain];
    subjectTableView.dataSource = self;
    subjectTableView.delegate = self;
    subjectTableView.tableFooterView = [UIView new];
    subjectTableView.separatorInset = UIEdgeInsetsMake(kP(0), kP(35), kP(0), kP(0));
    //    departMentTableView.separatorColor = kSepLineColor;
//    subjectTableView.backgroundColor = [UIColor greenColor];
    subjectTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:subjectTableView];
    self.subjectTableView = subjectTableView;

    //
//    NSIndexPath * selIndex = [NSIndexPath indexPathForRow:self.indexPath inSection:0];
//    [self.subjectTableView selectRowAtIndexPath:selIndex animated:YES scrollPosition:UITableViewScrollPositionTop];
    NSIndexPath * path = [NSIndexPath indexPathForItem:self.indexPath inSection:0];
    [self tableView:self.subjectTableView didSelectRowAtIndexPath:path];
    [self subjectSearch];

    //
    NSLog(@"---------444555566666777");

}
#pragma mark -- setUpExpertTeamSubViews
- (void)setUpExpertTeamSubViews {
    HZTableView *expertTeamTableView = [[HZTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    expertTeamTableView.dataSource = self;
    expertTeamTableView.delegate = self;
    expertTeamTableView.tableFooterView = [UIView new];
    expertTeamTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:expertTeamTableView];
    self.expertTeamTableView = expertTeamTableView;
    
    //
    CGFloat noDoctImageViewW = kP(340);
    CGFloat noDoctImageViewH = kP(376);
    CGFloat noDoctImageViewX = self.view.width * 0.5 - noDoctImageViewW * 0.5;
    CGFloat noDoctImageViewY = kP(600);
    UIImageView *noDoctImageView = [[UIImageView alloc] initWithFrame:CGRectMake(noDoctImageViewX, noDoctImageViewY, noDoctImageViewW, noDoctImageViewH)];
    noDoctImageView.image = [UIImage imageNamed:@"noDoct.png"];
    noDoctImageView.hidden = YES;
    [self.view addSubview:noDoctImageView];
    self.noDoctImageView = noDoctImageView;
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;

}



#pragma mark -- newsBtnClick
- (void)newsBtnClick:(UIButton *)newsBtn {
//    NSLog(@"---------ssssss");
    HZNewsListViewController *NewsListVCtr = [HZNewsListViewController new];
    [self.navigationController pushViewController:NewsListVCtr animated:YES];
}
#pragma mark -- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    NSLog(@"---self.departMentArr------%@", self.subjectArr);
    return [tableView isEqual:self.subjectTableView] ? self.subjectArr.count : self.expertTeamArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:self.subjectTableView]) {
        
        HZSubjectCell *subjectCell = [HZSubjectCell cellWithTableView:tableView];
        subjectCell.subject = self.subjectArr[indexPath.row];
        return subjectCell;
    }
    
    HZExpertTeamCell *expertTeamCell = [HZExpertTeamCell cellWithTableView:tableView];
    
    HZUser *expert = self.expertTeamArr[indexPath.row];
    
    expertTeamCell.priceList = self.priceArr;
   
    expertTeamCell.expert = expert;
    self.expertTeamCell = expertTeamCell;
    return expertTeamCell;

}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if ([tableView isEqual:self.subjectTableView]) {
        
        NSLog(@"--123456789--:%@", self.subjectArr);
        
//        //
//        [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
//
//        [[SDImageCache sharedImageCache] clearMemory];//可不写
        
        //
        HZSubject *subject0 = self.subjectArr[self.indexPath];
        subject0.isSelect = NO;
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];

        //
        [self subjectSearch];
        
        //
        HZSubject *subject = self.subjectArr[indexPath.row];
        subject.isSelect = YES;
        self.subjectLabel.text = subject.name;
        [self.subjectLabel sizeToFit];
        self.titleBgView.width = self.subjectLabel.width + kP(50);
        self.arrowImageView.x = self.subjectLabel.right + kP(16);
        self.indexPath = indexPath.row;
        
        //
        [self.subjectTableView reloadData];
        [self getExpertArrWithSubjectDictDepartmentId:subject.dictDepartmentId];

    } else {
        
        HZExpertHomeViewController *expertHomeVCtr = [HZExpertHomeViewController new];
        HZUser *expert = self.expertTeamArr[indexPath.row];
        expertHomeVCtr.expert = expert;
        
        NSLog(@"-----qwedcf--------:%@", expert.openid);
        expertHomeVCtr.user = self.user;
        
        
        UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:expert.openid];
        expertHomeVCtr.expertAvatar = image;
        expertHomeVCtr.priceArr = self.priceArr;
        [self.navigationController pushViewController:expertHomeVCtr animated:YES];
       
    }
}

#pragma mark -- lesionSearch
- (void)subjectSearch{
    
    //    self.navigationController.navigationBarHidden = NO;
    self.isTapped = !self.isTapped;
    self.arrowImageView.transform = self.isTapped ? CGAffineTransformMakeRotation(M_PI) : CGAffineTransformMakeRotation(0);
    //
    self.bgView.hidden = !self.bgView.hidden;
    [UIView animateWithDuration:0.2f animations:^{
        self.subjectTableView.height = self.isTapped ? MIN(self.view.height, self.subjectArr.count * kP(90))  : 0;
    }];
    
}


#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [tableView isEqual:self.subjectTableView] ? kP(90) : kP(230);
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>数据交互>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

#pragma mark -- departMentSearch
- (void)departMentSearch {
    self.isTapped = !self.isTapped;
    self.arrowImageView.transform = self.isTapped ? CGAffineTransformMakeRotation(M_PI) : CGAffineTransformMakeRotation(0);
    //
    self.bgView.hidden = !self.bgView.hidden;
    [UIView animateWithDuration:0.2f animations:^{
        
        self.subjectTableView.height = self.isTapped ? MIN(self.view.height - SafeAreaTopHeight - 49, self.subjectArr.count * kP(90))  : 0;
        
        NSLog(@"----222-----%@", self.subjectTableView);
    }];

}

#pragma mark -- showBgView

- (void)showBgView {
    [self departMentSearch];
}


#pragma mark -- turnToSearchVCtr
- (void)turnToSearchVCtr:(UIButton *)searchBtn {
    HZExpertSearchViewController *expertSearchVCtr = [HZExpertSearchViewController new];
    expertSearchVCtr.priceArr = self.priceArr;
    expertSearchVCtr.user = self.user;
    [self.navigationController pushViewController:expertSearchVCtr animated:NO];
}

#pragma mark -- pushToRefresh
- (void)pushToRefresh {
    
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        
//        //
//        [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
//
//        [[SDImageCache sharedImageCache] clearMemory];//可不写
        HZSubject *subject = self.subjectArr[self.indexPath];
        NSString *dictDepartmentId = subject.dictDepartmentId;
        [self getExpertArrWithSubjectDictDepartmentId:dictDepartmentId];
        
    }];
    self.expertTeamTableView.mj_header = refreshHeader;
    
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.expertTeamTableView.mj_header.automaticallyChangeAlpha = YES;
}

#pragma mark -- barHidden
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.alpha = 1.f;
    //    self.navigationController.navigationBarHidden = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
