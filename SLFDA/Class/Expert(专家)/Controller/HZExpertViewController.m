//
//  HZExpertViewController.m
//  SLFDA
//
//  Created by 季怀斌 on 2017/9/4.
//  Copyright © 2017年 huazhuo. All rights reserved.
//

#import "HZExpertViewController.h"
#import "HZExpertCVCell.h"
#import "HZExpertSearchTool.h"
#import "HZUser.h"
//
#import "MJRefresh.h"
//
#import "HZExpertHomeViewController.h"
//
#import "HZExpertTeamViewController.h"
static NSString *collectionViewCellID = @"expertCVCell";
@interface HZExpertViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
//** <#注释#>*/
@property (nonatomic, strong) HZUser *expert;
//** <#注释#>*/
@property (nonatomic, copy) NSArray *expertArr;
//** <#注释#>*/
@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, copy) NSArray *priceArr;
//** <#注释#>*/
@property (nonatomic, strong) HZUser *user;
@property (nonatomic, weak) UIImageView *noDoctImageView;
@end

@implementation HZExpertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"专家";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpSubViews];
    
    
    //
    [self refreshMyExperts];
    
    //
    [self pullOrPushToRefresh];
    
    //
    [self getMyPriceList];
}

- (void)setUpSubViews {
    
    //-------------------------------
    CGFloat headBgViewW = self.view.width;
    CGFloat headBgViewH = kP(94);
    CGFloat headBgViewX = 0;
    CGFloat headBgViewY = SafeAreaTopHeight;
    UIView *headBgView = [[UIView alloc] initWithFrame:CGRectMake(headBgViewX, headBgViewY, headBgViewW, headBgViewH)];
    headBgView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4" alpha:1.0];
    [self.view addSubview:headBgView];
    //
    UIView *vSepLine = [[UIView alloc] initWithFrame:CGRectMake(kP(36), kP(34), kP(8), kP(32))];
    vSepLine.layer.cornerRadius = kP(8) * 0.5;
    vSepLine.clipsToBounds = YES;
    vSepLine.backgroundColor = [UIColor colorWithHexString:@"#2DBED8" alpha:1.0];
    [headBgView addSubview:vSepLine];
    
    //
    UILabel *hotExpertLabel = [UILabel new];
    hotExpertLabel.text = @"相关热门专家";
    hotExpertLabel.textColor = [UIColor colorWithHexString:@"#777777" alpha:1.0];
    hotExpertLabel.font = [UIFont systemFontOfSize:kP(32)];
    [hotExpertLabel sizeToFit];
    hotExpertLabel.x = vSepLine.right + kP(20);
    hotExpertLabel.y = vSepLine.centerY - hotExpertLabel.height * 0.5;
    
    [headBgView addSubview:hotExpertLabel];
    
    
    //
    CGFloat tapViewW = self.view.width * 0.3;
    CGFloat tapViewH = kP(94);
    CGFloat tapViewX = self.view.width - tapViewW;
    CGFloat tapViewY = 0;
    UIView *tapView = [[UIView alloc] initWithFrame:CGRectMake(tapViewX, tapViewY, tapViewW, tapViewH)];
//    tapView.backgroundColor = [UIColor redColor];
    
    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToAction:)];
    [tapView addGestureRecognizer:tap];
    [headBgView addSubview:tapView];

    //
    CGFloat allExperEnterImgViewW = kP(16);
    CGFloat allExperEnterImgViewH = kP(24);
    CGFloat allExperEnterImgViewX = tapView.width - kP(32) - allExperEnterImgViewW;
    CGFloat allExperEnterImgViewY = tapView.height * 0.5 - allExperEnterImgViewH * 0.5;
    UIImageView *allExperEnterImgView = [[UIImageView alloc] initWithFrame:CGRectMake(allExperEnterImgViewX, allExperEnterImgViewY, allExperEnterImgViewW, allExperEnterImgViewH)];
    allExperEnterImgView.image = [UIImage imageNamed:@"allExpertEnter"];
    allExperEnterImgView.userInteractionEnabled = YES;
    [tapView addSubview:allExperEnterImgView];
    
    //
    CGFloat allExpertLabelW = kP(150);
    CGFloat allExpertLabelH = kP(35);
    CGFloat allExpertLabelX = allExperEnterImgViewX - kP(10) - allExpertLabelW;
    CGFloat allExpertLabelY = tapView.height * 0.5 - allExpertLabelH * 0.5;
    UILabel *allExpertLabel = [[UILabel alloc] initWithFrame:CGRectMake(allExpertLabelX, allExpertLabelY, allExpertLabelW, allExpertLabelH)];
    allExpertLabel.text = @"所有专家";
    allExpertLabel.textAlignment = NSTextAlignmentRight;
    allExpertLabel.textColor = [UIColor colorWithHexString:@"#666666" alpha:1.0];
    allExpertLabel.font = [UIFont systemFontOfSize:kP(28)];
    //    allExpertLabel.backgroundColor = [UIColor greenColor];
    allExpertLabel.userInteractionEnabled = YES;
    [tapView addSubview:allExpertLabel];
    
    //
   
    
    //---- collectionView  ----------------
    UICollectionViewFlowLayout *flowLayout = [UICollectionViewFlowLayout new];
    flowLayout.itemSize = CGSizeMake(kP(324), kP(380));
//    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.sectionInset = UIEdgeInsetsMake(kP(10), kP(36), kP(10), kP(36));
    flowLayout.minimumLineSpacing = kP(30);
    CGFloat collectionViewX = kP(0);
    CGFloat collectionViewY = kP(94) + SafeAreaTopHeight;
    CGFloat collectionViewW = HZScreenW;
    CGFloat collectionViewH = self.view.height - collectionViewY - 44;
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(collectionViewX, collectionViewY, collectionViewW, collectionViewH) collectionViewLayout:flowLayout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4" alpha:1.0];
    collectionView.showsVerticalScrollIndicator = NO;
//    collectionView.backgroundColor = [UIColor redColor];
    [self.view addSubview:collectionView];
    
    
    // 注册cell
    [collectionView registerClass:[HZExpertCVCell class] forCellWithReuseIdentifier:collectionViewCellID];
    
//    self.automaticallyAdjustsScrollViewInsets = NO; //头部空白处理
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

    self.collectionView = collectionView;
    
    //
    //
    CGFloat noDoctImageViewW = kP(340);
    CGFloat noDoctImageViewH = kP(376);
    CGFloat noDoctImageViewX = self.view.width * 0.5 - noDoctImageViewW * 0.5;
    CGFloat noDoctImageViewY = kP(600);
    UIImageView *noDoctImageView = [[UIImageView alloc] initWithFrame:CGRectMake(noDoctImageViewX, noDoctImageViewY, noDoctImageViewW, noDoctImageViewH)];
    noDoctImageView.image = [UIImage imageNamed:@"noDoct.png"];
    noDoctImageView.hidden = YES;
    [self.view addSubview:noDoctImageView];
    noDoctImageView.center = self.view.center;
    self.noDoctImageView = noDoctImageView;
    
    //
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.view.center;
    activityIndicator.color = [UIColor grayColor];
    [self.view addSubview:activityIndicator];
    self.activityIndicator = activityIndicator;
    
    //
    NSDictionary *userDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    HZUser *user = [HZUser mj_objectWithKeyValues:userDict];
    self.user = user;
}



- (void)refreshMyExperts {
    [self.activityIndicator startAnimating];
    //
    [HZExpertSearchTool getExpertArrWithDoctorName:nil dictDepartmentId:self.user.departmentGeneralId isHotExpertSearch:YES page:1 countPerPage:1000 success:^(id responseObject) {
        
        if ([responseObject[@"state"] isEqual:@200]) {
            NSLog(@"------sdssS---------:%@", responseObject);
            
            NSDictionary *resultsDict = responseObject[@"results"];
            NSLog(@"-----ddddsssss---------:%@", [responseObject getDictJsonStr]);
            NSArray *expertArr = resultsDict[@"doctors"];
            
            if (expertArr.count != 0) {
                [[NSUserDefaults standardUserDefaults] setObject:expertArr forKey:@"MyExpertArr"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                //
                self.expertArr = [NSArray array];
                self.expertArr = [HZUser mj_objectArrayWithKeyValuesArray:expertArr];
            } else {
                self.noDoctImageView.hidden = NO;
                self.expertArr = [NSArray array];
            }
           
            
            
//            [self.collectionView  reloadData];
//            [self.activityIndicator stopAnimating];
        } else {
            self.noDoctImageView.hidden = NO;
            self.expertArr = [NSArray array];
//            [self.collectionView reloadData];
//            [self.activityIndicator stopAnimating];
            [self.view makeToast:responseObject[@"msg"] duration:1.5 position:CSToastPositionBottom];
        }
        [self.collectionView  reloadData];
        [self.activityIndicator stopAnimating];
        [self.collectionView.mj_header endRefreshing];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"------sdssF---------:%@", error);
        [self.activityIndicator stopAnimating];
        [self.collectionView.mj_header endRefreshing];
    }];

}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.expertArr.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HZExpertCVCell *expertCVCell = (HZExpertCVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellID forIndexPath:indexPath];
    expertCVCell.expert = self.expertArr[indexPath.row];
    return expertCVCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSLog(@"-----------%ld--------%ld", indexPath.section, indexPath.item);
    
    HZUser *expert = self.expertArr[indexPath.row];
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:expert.openid];
    HZExpertHomeViewController *expertHomeVCtr = [HZExpertHomeViewController new];
    expertHomeVCtr.expertAvatar = image;
    expertHomeVCtr.priceArr = self.priceArr;
    expertHomeVCtr.expert = expert;
    expertHomeVCtr.user = self.user;
    [self.navigationController pushViewController:expertHomeVCtr animated:YES];
}

- (void)pullOrPushToRefresh {
    
    HZRefreshHeader *refreshHeader = [HZRefreshHeader headerWithRefreshingBlock:^{
        
        [self refreshMyExperts];
        
    }];
    self.collectionView.mj_header = refreshHeader;
    
    
//    //下拉刷新
//    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//
//        [self refreshMyExperts];
//        // 结束刷新
//        [self.collectionView.mj_header endRefreshing];
//    }];
//
//   // 上拉刷新
//    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//
//       [self refreshMyExperts];
//        // 结束刷新
//        [self.collectionView.mj_footer endRefreshing];
//    }];

}

#pragma mark -- tapToAction
- (void)tapToAction:(UITapGestureRecognizer *)tap {
    
    NSLog(@"-----%s", __FUNCTION__);
    
    HZExpertTeamViewController *expertTeamVCtr = [HZExpertTeamViewController new];
    expertTeamVCtr.user = self.user;
    [self.navigationController pushViewController:expertTeamVCtr animated:YES];
}


#pragma mark -- getPriceList
- (void)getMyPriceList {
    [HZExpertSearchTool getPriceListSuccess:^(id responseObject) {
        self.priceArr = responseObject[@"results"][@"PriceList"];
        NSLog(@"-----sssaaaqqqq-qwe---%@", self.priceArr);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"---------%@", error);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
