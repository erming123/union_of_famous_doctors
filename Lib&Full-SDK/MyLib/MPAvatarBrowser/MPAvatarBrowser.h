//
//  MPAvatarBrowser.h
//  HZImageBrowserDemo
//
//  Created by 季怀斌 on 16/9/27.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol MPAvatarBrowserDelegate <NSObject>

- (void)showAndHideStatusBarWithIsBig:(BOOL)isBig;

@end
@interface MPAvatarBrowser : NSObject
- (void)showImage:(UIImageView *)avatarImageView;

@property (nonatomic, weak) id<MPAvatarBrowserDelegate>delegate;
@property (nonatomic, assign) BOOL isBig;// 变大
@end
