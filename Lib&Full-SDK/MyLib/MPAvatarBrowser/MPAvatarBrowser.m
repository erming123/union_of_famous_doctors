//
//  MPAvatarBrowser.m
//  HZImageBrowserDemo
//
//  Created by 季怀斌 on 16/9/27.
//  Copyright © 2016年 huazhuo. All rights reserved.
//

#import "MPAvatarBrowser.h"
static CGRect oldframe;
NS_ASSUME_NONNULL_BEGIN
@interface MPAvatarBrowser ()
@property (nonatomic, assign)  CGFloat lastScale;//保存图片原来的大小
@property (nonatomic, assign) CGRect oldFrame;
@property (nonatomic, assign) CGRect largeFrame;  //确定图片放大最大的程度
//** <#注释#>*/
@property (nonatomic, strong) UIView *backgroundView;
@end
NS_ASSUME_NONNULL_END
@implementation MPAvatarBrowser
- (void)showImage:(UIImageView *)avatarImageView{ // 变大
    
    // 1. 取出当前图片和window
    UIImage *image = avatarImageView.image;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    //2. backgroundView
    UIView *backgroundView = [[UIView alloc]initWithFrame:window.bounds];
    
    //3. 图片view 转成 window
    oldframe = [avatarImageView convertRect:avatarImageView.bounds toView:window];
    
    //
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0;
    
    //3.
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:oldframe];
    imageView.image = image;
    imageView.tag = 1;
    
    //4.window 上放置图片view
    [backgroundView addSubview:imageView];
    [window addSubview:backgroundView];
    
    //5.
    [UIView animateWithDuration:0.3 animations:^{
        
        imageView.frame = CGRectMake(0,([UIScreen mainScreen].bounds.size.height-image.size.height*[UIScreen mainScreen].bounds.size.width/image.size.width)/2, [UIScreen mainScreen].bounds.size.width, image.size.height*[UIScreen mainScreen].bounds.size.width/image.size.width);
        
        backgroundView.alpha = 1;

        self.isBig = YES;
        if (self.delegate && [self.delegate respondsToSelector:@selector(showAndHideStatusBarWithIsBig:)]) {
            [self.delegate showAndHideStatusBarWithIsBig:YES];
        }
    } completion:^(BOOL finished) {
        
    }];
    
    
    //
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideImage:)];
    [backgroundView addGestureRecognizer: tap];
    
    //
    // 缩放手势
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchView:)];
    [backgroundView addGestureRecognizer:pinchGestureRecognizer];
    backgroundView.multipleTouchEnabled = YES;
    
    self.oldFrame = backgroundView.frame;
    self.largeFrame = CGRectMake(0 - 10000 *backgroundView.width, 0 - 10000 * backgroundView.height, 10000 * self.oldFrame.size.width, 10000 * self.oldFrame.size.height);
    
    //
    self.backgroundView = backgroundView;
}

- (void)hideImage:(UITapGestureRecognizer*)tap{ // 复原
    UIView *backgroundView=tap.view;
    UIImageView *imageView=(UIImageView*)[tap.view viewWithTag:1];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        imageView.frame = oldframe;
        backgroundView.alpha = 0;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(showAndHideStatusBarWithIsBig:)]) {
            [self.delegate showAndHideStatusBarWithIsBig:NO];
        }

        self.isBig = NO;
    } completion:^(BOOL finished) {
        [backgroundView removeFromSuperview];
    }];
    
}


// 处理缩放手势
- (void) pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    
    
    UIView *view = pinchGestureRecognizer.view;
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan || pinchGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        view.transform = CGAffineTransformScale(view.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
        if (self.backgroundView.frame.size.width <= self.oldFrame.size.width) {
            self.backgroundView.frame = self.oldFrame;
            //让图片无法缩得比原图小
        }
        if (self.backgroundView.frame.size.width > 10000 * self.oldFrame.size.width) {
            self.backgroundView.frame = self.largeFrame;
        }
        pinchGestureRecognizer.scale = 1;
    }
}

@end
